:relfilesuffix: .adoc
:adoc-main: https://asciidoctor.org/
:docker-main: https://docs.docker.com/
:gitlab-main: https://docs.gitlab.com/ee/ci/
:protobuf-main: https://developers.google.com/protocol-buffers
:grpc-main: https://grpc.io/docs/what-is-grpc/introduction/
:kubernetes-main: https://kubernetes.io/de/docs/concepts/overview/what-is-kubernetes/

= Pokémon Webapp Project

This project is an attempt to learn a lot of new, exciting web development technologies in a "Real World" Setting.
All code is inside this mono-repo (aside from a few extracted generic libraries that are now available open source).

== Product Vision

The user can play a Pokémon Game with his friends where he can
roam around on a map, catch Pokémon, battle and look at their collection and team.

== Goals

* Build a complete real-world web application by myself
* Use <<on_dependencies,state-of-the-art dependencies>>
* High <<on_compile_time_guarantees, compile-time guarantees>>
* Good <<on_performance, performance>>
* Good <<on_dev_exp, developer experience>>

== Non-Goals

* Choosing dependencies just because they are "in"
* Perfect test coverage
* Complicated Git Flows

== Footnotes

[#on_dependencies]
=== On Dependencies

We currently have a few dependencies which can be considered state-of-the-art:

* Kubernetes
* Rust
* Nginx
* Postgres
* Prometheus

Other dependencies are more debatable but have good synergy:

* Elm (focus on compile-time safety just like Rust)
* Protobuf/gRPC (performance, compile-time safety)

[#on_compile_time_guarantees]
=== Compile-Time Guarantees vs Tests

Building working software is a very complex task.
We want to help ourselves as much as possible to not be stuck with errors or even security issues in production.

Tests are very important for that. They actually run parts of our code to see if anything goes unexpectedly.
In many projects, a test coverage minimum of 80% or similar is set to enforce tests. However, I find that this often leads to poor tests instead of actually avoiding real errors.

Test data is often unrealistic/outdated, Usage of mocks means that the actual software is seldomly tested integratively.

So how can we fix some of these problems?
With strong typing. If we use https://en.wikipedia.org/wiki/Algebraic_data_type[Algebraic data types] to our advantage, we can make many https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/[impossible states unrepresentable] - so they cannot even be tested! If there https://doc.rust-lang.org/book/ch06-01-defining-an-enum.html?highlight=null[is no null], then it does not need to be tested.
Similarily, code generation helps with keeping code in sync.
For example, with {grpc-main}[gRPC], we ensure type-safety across different programming languages. We avoid a whole class of 4xx response problems this way.

This does not mean we don't need tests, but we need drastically less of them, and the ones we write are much closer to the code that actually runs in production. 

The general approach we want to use here is:

- Do not write unit tests for trivial functionality. It will harm iteration speed.
- Write unit tests for complex functions that are hard to get right or hard to understand.
- Write integration tests otherwise. Cover real use-cases and do not forget testing what should NOT work (e.g. role-based permissions)
- Test the impact of the code, not what method has been called (e.g. use a real postgres database for integration testing, spin up a HTTP server for mocking an external server)
- Write e2e tests for real user flows and in this case, only cover the happy path (crashing your own server to test error handling is unrealistic). The error cases should be covered by integration tests.

[#on_performance]
=== Performance

Many software projects emphasize https://www.computerenhance.com/p/clean-code-horrible-performance[clean code but ignore performance] until it is unbearable - pages load for a considerable amount of time, visible lag etc.
We don't want to do that. Of course we don't want to write raw C either. We rely on https://doc.rust-lang.org/beta/embedded-book/static-guarantees/zero-cost-abstractions.html[zero cost abstractions] and https://guide.elm-lang.org/optimization/asset_size[compiler optimizations].
In general, my opinion is "anything goes" for performance as long as I can keep a type-safe interface. So I won't expose raw pointer/byte operations from a module, but if I can keep it within the module and it is considerably faster than the alternatives, why not?

Measuring performance automatically is hard though. 
We should rely on metrics from production to track code paths that seem slower than they need to be (this is still a TODO currently).

[#on_dev_exp]
=== Developer Experience

There are certain things that make for a great developer experience:

1. Well-documented, mostly automated setup to get the project running
2. Local environment is very close to production
3. It should be easy to write a test to reproduce an issue
4. (Time-Travel-)Debugging & Stacktraces
5. Useful logs
6. Fast Feedback Loop

This project tries to accomplish most of these points.

1. You need to install a few executables on your own, but after that https://docs.npmjs.com/cli/v10/using-npm/scripts[npm scripts] and https://github.com/matklad/cargo-xtask[cargo xtask] got your back and work cross-platform
2. The production configuration has very few differences to the development configuration - most are related to connection strings/passwords. Passwords/Secrets are in git by using git-crypt so you should be able to reproduce production completely locally.
3. Since we do not rely on mocking for tests and do not use dependency injection, it should be easy to create an integrative test for some code path.
4. On the frontend, we take advantage of https://elm-lang.org/news/time-travel-made-easy[Elms time travelling debugger], on the backend we can debug with the https://code.visualstudio.com/docs/languages/rust[VSCode Rust Language Extension].
5. We use the https://github.com/tokio-rs/tracing[tracing library] for https://www.sumologic.com/glossary/structured-logging/[structured logging] - the log messages are mostly static but they are enriched with extra metadata and formatted as JSON. Tests are not spammed with logs since logs are only written if a https://docs.rs/tracing-subscriber/latest/tracing_subscriber/[subscriber] exists.
6. The https://vitejs.dev/guide/[Vite Dev Server] results in pretty quick automatic frontend reloads. On the backend we need to manually restart the server, but in general we can rely on tests here a lot more, since we have no visuals (the browser) we need to check. 
A CI/CD pipeline makes sure the code keeps its quality and is automatically deployed to production. With various tricks, we try to optimize its time.

== Features

* Accounts
** Creating accounts for this website ✅
*** Passwords are stored securely (hash with salt) ✅
** Using your andrena account as your preferred authentication instead ✅
** Using your GitHub account as your preferred authentication instead ✅
** Refreshing the page or closing and reopening the browser will remember you and automatically log you in ✅
*** Logout button to opt out of this behaviour ✅

* Multiplayer
** Chatting 🚧
*** Non-persistent global chat 🚧
*** Friends 🚧
**** Add friends by their id or username 🚧
**** Remove friends 🚧
**** Non-persistent private chat 🚧
*** Groups 🚧
**** Creating groups 🚧
**** Adding users to groups 🚧
**** Elevating them to administrators (allowing them to add more members) 🚧
**** Deleting groups (only by the original creator) 🚧
**** Non-persistent group chat 🚧
**** Add friends to groups 🚧
**** Add group members to friends 🚧

** See other players run across the map ✅

* Gameplay
** Configurable map layout ✅
** Configurable trainer sprites ✅
** Randomized, randomly moving, visible pokemon encounters ✅
** Ground animations (e.g. footsteps in sand) ✅
** Catch pokemon 🚧
** Pokedex ✅
*** View your progress ✅
*** View individual species stats ✅

* Configuration
** Dark Mode ✅
** Internationalized Texts ✅
*** English ✅
*** German ✅
*** Other languages? 🚧
** Persistence of configuration to browser storage ✅


== Local setup

include::SETUP.adoc

== Technologies

=== Backend

include::backend/README.adoc[tags=vars;tech]

=== Frontend

include::frontend/README.adoc[tags=vars;tech] 

=== Integration Tests

* Cypress

=== General

* {protobuf-main}[Protobuf]

=== DevOps

* {adoc-main}[AsciiDoc]
* {gitlab-main}[Gitlab CI]
* {docker-main}[Docker]
* {docker-compose-main}[Docker-Compose]

=== Monitoring 

include::monitoring/README.adoc[tags=vars;tech]
