import { GrpcStatus } from '../../utils/grpc-status';
import {
  GetDexEntriesResponse,
  GetDexEntryDetailRequest,
  GetDexEntryDetailResponse,
  GetPokemonRequest,
  GetPokemonResponse,
  Type,
} from '../../generated/kangaskhan';

const POKEMON = {
  1: {
    name: 'Bulbasaur',
    amountNormal: 2,
    amountShiny: 1,
    height: 7.0,
    weight: 69.0,
    primaryType: Type.Grass,
    secondaryType: Type.Poison,
    description:
      'A strange seed was planted on its back at birth. The plant sprouts and grows with this Pokémon.',
    moveAbility: 1,
    moveDurationInMs: 500,
  },
  19: {
    name: 'Rattata',
    amountNormal: 4,
    amountShiny: 0,
    height: 3.0,
    weight: 35.0,
    primaryType: Type.Normal,
    description:
      'Bites anything when it attacks. Small and very quick, it is a common sight in many places.',
    moveAbility: 1,
    moveDurationInMs: 400,
  },
  25: {
    name: 'Pikachu',
    amountNormal: 1,
    amountShiny: 0,
    primaryType: Type.Electric,
    height: 0.5,
    weight: 5.2,
    description: 'Pikachu is a cute fellow',
    moveAbility: 1,
    moveDurationInMs: 450,
  },
  35: {
    name: 'Clefairy',
    amountNormal: 0,
    amountShiny: 1,
    height: 6.0,
    weight: 75.0,
    primaryType: Type.Fairy,
    description:
      'Its magical and cute appeal has many admirers. It is rare and found only in certain areas.',
    moveAbility: 1,
    moveDurationInMs: 400,
  },
  149: {
    name: 'Dragonite',
    amountNormal: 1,
    amountShiny: 0,
    height: 22.0,
    weight: 2100.0,
    primaryType: Type.Dragon,
    secondaryType: Type.Flying,
    description:
      'An extremely rarely seen marine Pokémon. Its intelligence is said to match that of humans.',
    moveAbility: 7,
    moveDurationInMs: 300,
  },
};

export function getDexEntryDetail(
  request: GetDexEntryDetailRequest
): GetDexEntryDetailResponse | { grpcStatus: GrpcStatus } {
  const pokemon = POKEMON[request.number as keyof typeof POKEMON];
  if (pokemon) {
    return pokemon;
  }
  return { grpcStatus: GrpcStatus.NotFound };
}

export function getDexEntries(
  filter?: (keyof typeof POKEMON)[]
): GetDexEntriesResponse {
  const entries = Object.entries(POKEMON).map(([number, pokemon]) => ({
    number: +number,
    pokemonName: pokemon.name,
    ...pokemon,
  }));
  if (!filter) {
    return { entries };
  }

  return {
    entries: entries.filter((entry) =>
      filter.includes(entry.number as keyof typeof POKEMON)
    ),
  };
}

export function getPokemon(
  request: GetPokemonRequest
): GetPokemonResponse | { grpcStatus: GrpcStatus } {
  const pokemon = POKEMON[request.pokemonNumber as keyof typeof POKEMON];
  if (pokemon) {
    return { ...pokemon, number: request.pokemonNumber };
  }
  return { grpcStatus: GrpcStatus.NotFound };
}
