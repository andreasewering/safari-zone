export const trainers = {
  trainers: Array.from({ length: 14 }).map((_, i) => ({
    trainerNumber: i + 1,
    tagIds: [0, 1],
    displayName: '',
  })),
  tags: [
    { tagId: 0, displayName: 'Male' },
    { tagId: 1, displayName: 'Female' },
  ],
};
