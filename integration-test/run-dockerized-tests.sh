#!/bin/bash

source ../.env

ARCH=$(node -e 'console.log(process.arch)')

echo "Running on arch $ARCH"

# Function to stop containers and exit script
cleanup() {
  echo "Stopping containers..."
  docker stop int-test-nginx int-test-runner
  exit
}

start_frontend() {
  # Build frontend static files
  (cd ../frontend && npm run build:int-test)
  # Build frontend docker container
  docker build -t int-test-nginx --build-arg BASE_IMAGE=registry.gitlab.com/andreasewering/safari-zone/nginx ../frontend

  # Run frontend docker container
  docker run --rm -d --name int-test-nginx \
      -p 4173:443 \
      -e GRPC_SERVERS='server host.docker.internal:4444;' \
      -e HTTP_SERVERS='server host.docker.internal:4444;' \
      -e DEBUG_HEADERS='' \
      -e SSL_CERT_NAME=integration-test \
      int-test-nginx
}

start_frontend

copy_frontend() {
  # Remove existing static frontend files
  docker exec -it int-test-nginx /bin/sh -c "rm -rf /usr/share/nginx/*"
  # Build frontend static files
  (cd ../frontend && npm run build:int-test && docker cp dist/. int-test-nginx:/usr/share/nginx)
  # Restart nginx
  docker exec -it int-test-nginx nginx -s reload
}

# Run playwright container
docker run --rm -d --name int-test-runner --memory 2GB -v "$(pwd):/tests" -w /tests \
    -p 4321:4321 \
    -e PRIVATE_KEY="$PRIVATE_KEY" \
    -e ARCH="$ARCH" \
    mcr.microsoft.com/playwright:v1.49.1-noble-amd64 \
    sh ./start-test-ui.sh
# For when you need to debug:
# -e DEBUG=pw:api,pw:channel:command,pw:channel:response \

echo "Open http://localhost:4321 to run the tests interactively"

# Listen for Ctrl-C
trap cleanup SIGINT

# Keep the script running
while true; do
   echo "Enter one of the following commands:"
   echo "'r' - to rebuild/start the frontend docker container"
   echo "'q' - to quit, alternatively just ctrl + c"
   read input

   if [ "$input" = "r" ]; then
        copy_frontend
    elif [ "$input" = "q" ]; then
        cleanup
        echo "Goodbye"
        exit 0
    else
        echo "Unknown command: $input"
    fi
done
