import { expect } from '@playwright/test';
import { randomUUID } from 'node:crypto';
import { GetUserDataResponse, Type } from '../generated/kangaskhan.js';
import { generateToken } from '../utils/auth.js';
import { Direction } from '../generated/direction.js';
import { test } from '../utils/test-extensions.js';
import { GrpcStatus } from '../utils/grpc-status.js';

const userData: GetUserDataResponse = { name: 'Tiberius Estor' };

const trainers = [{ trainerNumber: 1, displayName: 'Captain', tagIds: [] }];

const startTiles = [{ startTileId: 1n, x: 0, y: 0, layerNumber: 0, mapId: 1n }];

test('account creation happy case', async ({
  page,
  rpc,
  trainerOverview,
  trainerDetail,
  pokemonSelect,
  mapSelect,
}) => {
  await rpc.mock('gardevoir.GardevoirService/Refresh', {
    grpcStatus: GrpcStatus.Unauthenticated,
  });
  await rpc.mock('gardevoir.GardevoirService/GetConfig', {
    githubClientId: 'Iv23limbK9Ob4xBi1ncr',
    andrenaRealm: 'test',
    andrenaClientId: 'testclient',
  });
  await page.goto('/');

  // Get redirected due to not being logged in
  await expect(page).toHaveTitle(/Login/);
  await expect(page).toHaveScreenshot();

  await page.getByText('Sign up').click();

  await expect(page).toHaveTitle(/Sign up/);

  // Create a new account with a random username to avoid conflicts
  const uuid = randomUUID();
  const username = `e2e-${uuid}`;
  const password = 'Test1234';
  await page.getByPlaceholder('Username').fill(username);
  await page.getByPlaceholder('Password').fill(password);
  await page.getByPlaceholder('Email').fill(`${uuid}@e2e`);

  await rpc.mock('gardevoir.GardevoirService/Register', {});

  await page.getByText('Sign up', { exact: true }).click();
  await expect(page.getByText(/created successfully/)).toBeVisible();

  // Navigate back to login page
  await page.getByText('Sign in', { exact: true }).click();
  await expect(page).toHaveTitle(/Login/);

  // Login using previously created account
  await page.getByPlaceholder('Username').fill(username);
  await page.getByPlaceholder('Password').fill(password);
  const accessToken = generateToken({ userId: 1234n, permissions: [] });
  await rpc.mock('gardevoir.GardevoirService/Login', {
    accessToken,
    validForSeconds: 1000,
  });
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntries');
  await rpc.mock('kangaskhan.KangaskhanService/GetUserData', () => userData);
  await rpc.mock('kangaskhan.KangaskhanService/GetTrainers', {
    trainers,
    tags: [],
  });
  await rpc.mock('smeargle.SmeargleService/GetTextureMetadata');
  await rpc.mock('smeargle.SmeargleService/GetTilesetConfig');
  await rpc.mock('smeargle.SmeargleService/GetTiles');

  await page.getByText('Sign in', { exact: true }).click();

  // Since we created a new account, we first have to choose our avatar
  await trainerOverview.waitForSuccessTitle();
  await trainerOverview.clickTrainer('Captain');
  await rpc.mock(
    'kangaskhan.KangaskhanService/SetTrainerSprite',
    ({ trainerNumber }) => {
      userData.trainer = { trainerNumber };
      return {};
    }
  );

  await rpc.mock('kangaskhan.KangaskhanService/GetStarterPokemon', {
    pokemon: [
      {
        number: 1,
        name: 'Bulbasaur',
        primaryType: Type.Grass,
        secondaryType: Type.Poison,
        height: 0.3,
        weight: 8.5,
        description: 'Bulbasaur Description',
      },
    ],
  });
  await expect(trainerDetail.confirmTrainerChoice()).resolves.toEqual('1');

  // Choose our starter pokémon
  await pokemonSelect.waitForSuccessTitle();
  await rpc.mock(
    'kangaskhan.KangaskhanService/SetPartnerPokemon',
    ({ pokemonNumber }) => {
      userData.partnerPokemon = { pokemonNumber, isShiny: false };
      return {};
    }
  );
  await rpc.mock('smeargle.SmeargleService/GetAllStartTiles', {
    tiles: startTiles,
    maps: {
      '1': {
        path: '',
        layers: [],
        offsetX: 0,
        offsetY: 0,
        name: 'Hidden Woods',
      },
    },
  });
  await rpc.mock(
    'kangaskhan.KangaskhanService/ChooseStartTile',
    ({ startTileId }) => {
      const startTile = startTiles.find(
        (startTile) => startTile.startTileId === startTileId
      );
      if (!startTile) {
        throw new Error(
          `Non existing start tile ${startTileId.toString()} chosen`
        );
      }
      userData.position = {
        x: startTile.x,
        y: startTile.y,
        layerNumber: startTile.layerNumber,
        mapId: startTile.mapId,
        direction: Direction.D,
      };
    }
  );

  await pokemonSelect.choosePokemon('Bulbasaur');

  // Choose the map to start on
  await mapSelect.waitForSuccessTitle();
  await rpc.mock('kangaskhan.KangaskhanService/GetUserItems');
  await rpc.mock('smeargle.SmeargleService/GetAnimationConfig');
  await rpc.mock('kangaskhan.KangaskhanService/GetPokemon');
  await mapSelect.chooseMap('Hidden Woods');

  await expect(page).toHaveTitle(/Discover the world/);
});

test('automatic redirect to trainer selection', async ({
  page,
  rpc,
  trainerOverview,
}) => {
  await rpc.mock('gardevoir.GardevoirService/Refresh', {
    accessToken: generateToken({ userId: 1n, permissions: [] }),
    validForSeconds: 1000,
  });
  await rpc.mock('kangaskhan.KangaskhanService/GetUserData', {
    name: 'No trainer',
  });
  await rpc.mock('kangaskhan.KangaskhanService/GetTrainers');
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntries');
  await rpc.mock('smeargle.SmeargleService/GetTilesetConfig');
  await page.goto('/');

  await trainerOverview.waitForSuccessTitle();
});

test('automatic redirect to pokemon selection', async ({
  page,
  rpc,
  pokemonSelect,
}) => {
  await rpc.mock('gardevoir.GardevoirService/Refresh', {
    accessToken: generateToken({ userId: 1n, permissions: [] }),
    validForSeconds: 1000,
  });
  await rpc.mock('kangaskhan.KangaskhanService/GetUserData', {
    name: 'Trainer but no pokemon',
    trainer: { trainerNumber: 1 },
  });
  await rpc.mock('kangaskhan.KangaskhanService/GetStarterPokemon');
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntries');
  await page.goto('/');

  await pokemonSelect.waitForSuccessTitle();
});

test('automatic redirect to map selection', async ({
  page,
  rpc,
  auth,
  mapSelect,
}) => {
  await auth.login();
  await rpc.mock('kangaskhan.KangaskhanService/GetUserData', {
    name: 'Trainer, pokemon but no map',
    trainer: { trainerNumber: 1 },
    partnerPokemon: { pokemonNumber: 1, isShiny: false },
  });
  await rpc.mock('smeargle.SmeargleService/GetAllStartTiles');
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntries');
  await rpc.mock('smeargle.SmeargleService/GetTilesetConfig');
  await page.goto('/');

  await mapSelect.waitForSuccessTitle();
});
