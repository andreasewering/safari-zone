import { Item } from '../generated/item';
import { Direction } from '../generated/direction';
import { test } from '../utils/test-extensions';
import { expect } from '@playwright/test';

test('safari single player', async ({ safari, canvasCapture, eventWaiter }) => {
  const mapId = 4321n;
  await safari.mockRpcs({ mapId, x: 5, y: 3, direction: Direction.D });
  await safari.goto();

  await eventWaiter.waitForMapLoad(mapId);
  await canvasCapture.screenshotCanvas();
});

test('edge of map is blocked', async ({
  safari,
  canvasCapture,
  eventWaiter,
  page,
}) => {
  const mapId = 4321n;
  const userId = 1234n;
  await safari.mockRpcs({ mapId, userId, x: 10, y: 5, direction: Direction.D });

  const mapLoad = eventWaiter.waitForMapLoad(mapId);
  await safari.goto();
  await mapLoad;

  let moveComplete = eventWaiter.waitForPlayerMoveComplete(
    userId,
    {
      x: 10,
      y: 5,
      layerNumber: 0,
    },
    'R'
  );
  await page.keyboard.press('ArrowRight', { delay: 20 });
  await moveComplete;

  await canvasCapture.screenshotCanvas();

  moveComplete = eventWaiter.waitForPlayerMoveComplete(
    userId,
    {
      x: 10,
      y: 5,
      layerNumber: 0,
    },
    'D'
  );
  await page.keyboard.press('ArrowDown', { delay: 20 });
  await moveComplete;
  await canvasCapture.screenshotCanvas();
});

test.describe('mobile', () => {
  test.skip(({ isMobile }) => !isMobile);

  test('dpad single', async ({ safari, canvasCapture, eventWaiter }) => {
    const mapId = 4321n;
    const userId = 1234n;
    await safari.mockRpcs({
      mapId,
      userId,
      x: 0,
      y: -2,
      direction: Direction.D,
    });

    const mapLoad = eventWaiter.waitForMapLoad(mapId);
    await safari.goto();
    await mapLoad;

    const boundingBox = await safari.dpad().boundingBox();
    if (!boundingBox) {
      throw new Error('Expected Dpad to be visible and have a size');
    }

    const moveComplete = eventWaiter.waitForPlayerMoveComplete(
      userId,
      {
        x: 0,
        y: -3,
        layerNumber: 0,
      },
      'U'
    );
    await safari.dpad().click({
      // this should essentially be the same as an "ArrowUp"
      position: { x: boundingBox.width / 2, y: boundingBox.height / 2 - 1 },
      delay: 10,
    });
    await moveComplete;
    await canvasCapture.screenshotCanvas();
  });

  test('dpad spin', async ({ safari, canvasCapture, eventWaiter, page }) => {
    const mapId = 4321n;
    const userId = 1234n;
    await safari.mockRpcs({
      mapId,
      userId,
      x: 0,
      y: -2,
      direction: Direction.D,
    });

    const mapLoad = eventWaiter.waitForMapLoad(mapId);
    await safari.goto();
    await mapLoad;

    const boundingBox = await safari.dpad().boundingBox();
    if (!boundingBox) {
      throw new Error('Expected Dpad to be visible and have a size');
    }

    let moveComplete = eventWaiter.waitForPlayerMoveComplete(
      userId,
      {
        x: -1,
        y: -2,
        layerNumber: 0,
      },
      'L'
    );
    await page.mouse.move(
      // left
      boundingBox.x + boundingBox.width / 2 - 1,
      boundingBox.y + boundingBox.height / 2
    );
    await page.mouse.down();
    await page.mouse.up();
    await moveComplete;

    moveComplete = eventWaiter.waitForPlayerMoveComplete(
      userId,
      {
        x: -1,
        y: -1,
        layerNumber: 0,
      },
      'D'
    );
    await page.mouse.move(
      // down
      boundingBox.x + boundingBox.width / 2,
      boundingBox.y + boundingBox.height / 2 + 1
    );
    await page.mouse.down();
    await page.mouse.up();
    await moveComplete;

    await canvasCapture.screenshotCanvas();
  });
});

test('safari two players', async ({
  safari,
  page,
  websocket,
  canvasCapture,
  eventWaiter,
}) => {
  const mapId = 4321n;
  const userId = 1123n;
  await safari.mockRpcs({
    mapId,
    userId,
    x: 5,
    y: 3,
    direction: Direction.D,
  });
  const mapLoad = eventWaiter.waitForMapLoad(mapId);
  await safari.goto();
  await mapLoad;

  const otherUserId = 1111n;
  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'playerEnteredMap',
      playerEnteredMap: {
        coordX: 3,
        coordY: 1,
        layerNumber: 0,
        userName: 'OtherPlayer',
        partnerPokemonNumber: 2,
        partnerIsShiny: false,
        userId: otherUserId,
        trainerNumber: 2,
        direction: Direction.L,
      },
    },
  });
  await page.waitForResponse('**/trainer/2.png');

  await canvasCapture.screenshotCanvas();

  const moveComplete = eventWaiter.waitForPlayerMoveComplete(
    otherUserId,
    {
      x: 3,
      y: 0,
      layerNumber: 0,
    },
    'U'
  );
  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'playerMoved',
      playerMoved: {
        coordX: 3,
        coordY: 0,
        layerNumber: 0,
        direction: Direction.U,
        userId: otherUserId,
      },
    },
  });
  await moveComplete;

  await canvasCapture.screenshotCanvas();
});

test('collect spawned item', async ({
  safari,
  page,
  canvasCapture,
  websocket,
  eventWaiter,
}) => {
  const mapId = 4321n;
  const userId = 1123n;
  await safari.mockRpcs({
    mapId,
    userId,
    x: 5,
    y: 3,
    direction: Direction.D,
  });
  const mapLoad = eventWaiter.waitForMapLoad(mapId);
  await safari.goto();
  await mapLoad;

  const itemId = 123n;
  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'itemSpawned',
      itemSpawned: {
        itemId,
        item: Item.Superball,
        x: 4,
        y: 3,
        layerNumber: 0,
        amount: 1,
      },
    },
  });
  await canvasCapture.screenshotCanvas();

  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'collectedItem',
      collectedItem: {
        item: Item.Superball,
        amount: 1,
      },
    },
  });

  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'itemDespawned',
      itemDespawned: itemId,
    },
  });
  await canvasCapture.screenshotCanvas();

  await safari.backpack().click();
  expect(safari.superball()).toBeVisible();
});
