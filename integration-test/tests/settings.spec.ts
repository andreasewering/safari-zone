import { expect } from '@playwright/test';
import { test } from '../utils/test-extensions';

test('switching from light to dark mode', async ({
  settings,
  rpc,
  page,
  mouse,
}) => {
  await rpc.mockFullyCreatedUser();
  await settings.goto();
  await mouse.moveToMiddle();

  await settings.toggleDarkmode();
  await expect(page.getByText('Current theme: Dark')).toBeVisible();
  await expect(page).toHaveScreenshot();
});

test('switching from dark to light mode', async ({
  settings,
  rpc,
  page,
  mouse,
}) => {
  await page.emulateMedia({ colorScheme: 'dark' });
  await rpc.mockFullyCreatedUser();
  await settings.goto();
  await mouse.moveToMiddle();

  await settings.toggleDarkmode();
  await expect(page.getByText('Current theme: Light')).toBeVisible();
  await expect(page).toHaveScreenshot();
});

test('switch to german', async ({ settings, rpc, page, mouse }) => {
  await rpc.mockFullyCreatedUser();
  await settings.goto();
  await mouse.moveToMiddle();

  await page
    .locator('select')
    .filter({ hasText: 'English' })
    .selectOption({ label: 'German' });
  await expect(page).toHaveScreenshot();
});
