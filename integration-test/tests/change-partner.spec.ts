import { expect } from '@playwright/test';
import { Direction } from '../generated/direction';
import { test } from '../utils/test-extensions';
import { GrpcStatus } from '../utils/grpc-status';

test('change partner pokemon success', async ({
  dexDetail,
  safari,
  rpc,
  page,
  canvasCapture,
  eventWaiter,
  websocket,
}) => {
  const mapId = 1337n;
  const userId = 3456n;
  await dexDetail.mockRpcs();
  const { setPartnerPokemon } = await safari.mockRpcs({
    mapId,
    userId,
    x: 0,
    y: 0,
    direction: Direction.D,
  });
  await dexDetail.goto(35);

  await rpc.mock('kangaskhan.KangaskhanService/SetPartnerPokemon');
  const partnerImageLoad = page.waitForResponse('**/overworld/35.png');
  setPartnerPokemon(35);
  await dexDetail.setPartnerButton().click();

  const mapLoadEvent = eventWaiter.waitForMapLoad(mapId);
  await safari.expectToBeOnPage();
  await mapLoadEvent;
  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'movePartnerPokemon',
      movePartnerPokemon: {
        coordX: 1,
        coordY: 0,
        layerNumber: 0,
        direction: Direction.L,
        userId,
      },
    },
  });
  await partnerImageLoad;
  await canvasCapture.screenshotCanvas();
});

test('loading state', async ({ dexDetail, rpc, page }) => {
  await dexDetail.mockRpcs();
  await dexDetail.goto(35);

  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/SetPartnerPokemon');
  await rpc.mock('kangaskhan.KangaskhanService/GetPokemon');
  await dexDetail.setPartnerButton().click();

  await expect(page).toHaveScreenshot();
});

test('error state', async ({ dexDetail, rpc, page }) => {
  await dexDetail.mockRpcs();
  await dexDetail.goto(35);

  await rpc.mock('kangaskhan.KangaskhanService/SetPartnerPokemon', {
    grpcStatus: GrpcStatus.InvalidArgument,
  });
  await rpc.mock('kangaskhan.KangaskhanService/GetPokemon');
  await dexDetail.setPartnerButton().click();

  await expect(page).toHaveScreenshot();
});

test('via keyboard', async ({ dexDetail, rpc, keyboard }) => {
  await dexDetail.mockRpcs();
  await dexDetail.goto(35);

  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/SetPartnerPokemon');
  await rpc.mock('kangaskhan.KangaskhanService/GetPokemon');
  await keyboard.findAndPressEnter(dexDetail.setPartnerButton());

  expect(
    rpc.mocks['kangaskhan.KangaskhanService/SetPartnerPokemon']?.requests
  ).toEqual([{ pokemonNumber: 35 }]);
});

test('other player changes partner', async ({
  safari,
  canvasCapture,
  page,
  websocket,
}) => {
  const mapId = 1234n;
  const userId = 4321n;
  await safari.mockRpcs({ mapId, userId, x: 1, y: 1, direction: Direction.L });
  await safari.goto();

  await websocket.waitForConnect();

  const otherUserId = 1111n;
  const trainerTexture = page.waitForResponse('**/trainer/2.png');
  const initialPartnerPokemonTexture =
    page.waitForResponse('**/overworld/1.png');

  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'playerEnteredMap',
      playerEnteredMap: {
        userId: otherUserId,
        coordX: 3,
        coordY: 0,
        layerNumber: 0,
        direction: Direction.R,
        userName: 'Other player',
        trainerNumber: 2,
        partnerIsShiny: false,
        partnerPokemonNumber: 1,
      },
    },
  });
  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'movePartnerPokemon',
      movePartnerPokemon: {
        coordX: 3,
        coordY: -1,
        layerNumber: 0,
        direction: Direction.D,
        userId: otherUserId,
      },
    },
  });
  await initialPartnerPokemonTexture;
  await trainerTexture;

  const newPartnerPokemonTexture = page.waitForResponse('**/overworld/149.png');
  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'playerChangedPartnerPokemon',
      playerChangedPartnerPokemon: {
        userId: otherUserId,
        pokemonNumber: 149,
        isShiny: true,
      },
    },
  });
  await newPartnerPokemonTexture;
  await canvasCapture.screenshotCanvas();
});
