import { expect, Locator } from '@playwright/test';
import { test } from '../utils/test-extensions';
import { getDexEntryDetail } from '../fixtures/pokemon/pokemon';
import { GrpcStatus } from '../utils/grpc-status';

test('dex detail page', async ({ dexDetail, mouse, page }) => {
  await dexDetail.mockRpcs();
  await dexDetail.goto(25);
  await mouse.moveToMiddle();
  await expect(page).toHaveTitle(/Dex: Pikachu/);
  await dexDetail.expectToBeOnPage(25);

  await getSingleVisible(page.getByText(/Height.*0.5 m/, { exact: true }));
  await getSingleVisible(page.getByText(/Weight.*5.2 kg/, { exact: true }));
  await getSingleVisible(page.getByText(/Caught.*1/, { exact: true }));
  await getSingleVisible(page.getByText(/Shiny.*0/, { exact: true }));
  await getSingleVisible(page.getByText('Electric', { exact: true }));
  await getSingleVisible(
    page.getByText('Pikachu is a cute fellow', { exact: true })
  );
  await expect(page).toHaveScreenshot();
});

test.describe('german locale', () => {
  test.use({
    locale: 'de-DE',
  });

  test('dex detail page', async ({ dexDetail, page, rpc }) => {
    await dexDetail.mockRpcs();
    await rpc.mock('kangaskhan.KangaskhanService/GetDexEntryDetail', {
      ...getDexEntryDetail({ number: 25 }),
      description: 'Eine deutsche Beschreibung',
    });

    await page.goto('/dex/25');
    await expect(page).toHaveTitle(/Dex: Pikachu/);
    expect(new URL(page.url()).pathname).toBe('/dex/25');

    await getSingleVisible(page.getByText(/Größe.*0,5 m/, { exact: true }));
    await getSingleVisible(page.getByText(/Gewicht.*5,2 kg/, { exact: true }));
    await getSingleVisible(page.getByText(/Gefangen.*1/, { exact: true }));
    await getSingleVisible(page.getByText(/Schillernd.*0/, { exact: true }));
    await getSingleVisible(page.getByText('Elektro', { exact: true }));
    await getSingleVisible(
      page.getByText('Eine deutsche Beschreibung', { exact: true })
    );
  });
});

test('dex detail loading', async ({ dexDetail, rpc, page, mouse }) => {
  await dexDetail.mockRpcs();
  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/GetDexEntryDetail');
  await dexDetail.goto(25);
  await mouse.moveToMiddle();

  await expect(page).toHaveScreenshot();
});

test('dex entry loading', async ({ dexDetail, rpc, page, mouse }) => {
  await dexDetail.mockRpcs();
  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/GetDexEntries');
  await dexDetail.goto(25);
  await mouse.moveToMiddle();

  await expect(page).toHaveScreenshot();
});

test('dex detail error', async ({ dexDetail, rpc, page, mouse }) => {
  await dexDetail.mockRpcs();
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntryDetail', {
    grpcStatus: GrpcStatus.Internal,
  });
  await dexDetail.goto(25);
  await mouse.moveToMiddle();

  await expect(page).toHaveScreenshot();
});

test('dex entry error', async ({ dexDetail, rpc, page, mouse }) => {
  await dexDetail.mockRpcs();
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntries', {
    grpcStatus: GrpcStatus.Internal,
  });
  await dexDetail.goto(25);
  await mouse.moveToMiddle();

  await expect(page).toHaveScreenshot();
});

test('dex detail token expired', async ({
  dexDetail,
  dexOverview,
  rpc,
  page,
}) => {
  await dexDetail.mockRpcs();
  await dexOverview.goto();

  await rpc.setTokenExpired(['kangaskhan.KangaskhanService/GetDexEntryDetail']);
  await dexOverview.pokemonDetailLink('Pikachu').click();

  await expect(page).toHaveTitle(/Dex: Pikachu/);
  await dexDetail.expectToBeOnPage(25);

  await getSingleVisible(page.getByText(/Height.*0.5 m/, { exact: true }));
});

test('navigates back to dex on back click', async ({
  dexDetail,
  dexOverview,
}) => {
  await dexDetail.mockRpcs();
  await dexDetail.goto(19);

  await dexDetail.backButton().click();
  await expect(dexDetail.backButton()).not.toBeVisible();
  await dexOverview.expectToBeOnPage();
});

test.describe('desktop only', () => {
  test.skip(({ isMobile }) => isMobile);

  test('navigate between dex details via arrow right', async ({
    dexDetail,
    page,
    mouse,
  }) => {
    await dexDetail.mockRpcs();
    await dexDetail.goto(19);
    await mouse.moveToMiddle();

    const nextButton = dexDetail.nextButton();
    await expect
      .poll(() => isVisible(nextButton), {
        message: 'Make sure next arrow is visible',
      })
      .toBe(true);
    await expect(page.getByText('Rattata', { exact: true })).toBeInViewport();
    await nextButton.click();

    await page.getByText('Pikachu', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"
    await nextButton.click();

    await page.getByText('Clefairy', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"
    await nextButton.click();

    await page.getByText('Dragonite', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"
    await nextButton.click();

    await expect(page.getByText('Bulbasaur', { exact: true })).toBeInViewport();
  });

  test('navigate between dex details via arrow left', async ({
    dexDetail,
    page,
    mouse,
  }) => {
    await dexDetail.mockRpcs();
    await dexDetail.goto(19);
    await mouse.moveToMiddle();

    const previousButton = dexDetail.previousButton();
    await expect
      .poll(() => isVisible(previousButton), {
        message: 'Make sure previous arrow is visible',
      })
      .toBe(true);
    await expect(page.getByText('Rattata', { exact: true })).toBeInViewport();
    await previousButton.click();

    await page.getByText('Bulbasaur', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"
    await previousButton.click();

    await page.getByText('Dragonite', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"
    await previousButton.click();

    await page.getByText('Clefairy', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"
    await previousButton.click();

    await expect(page.getByText('Pikachu', { exact: true })).toBeInViewport();
  });

  test('navigate using keyboard', async ({ dexDetail, dexOverview, page }) => {
    await dexDetail.mockRpcs();
    await dexDetail.goto(25);

    await expect
      .poll(() => isVisible(dexDetail.previousButton()), {
        message: 'Make sure previous arrow is visible',
      })
      .toBe(true);

    await page.keyboard.down('ArrowLeft');
    await page.keyboard.up('ArrowLeft');
    await dexDetail.expectToBeOnPage(19);
    await page.getByText('Rattata', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"

    await page.keyboard.press('ArrowRight');
    await dexDetail.expectToBeOnPage(25);
    await page.getByText('Pikachu', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"

    await page.keyboard.press('Escape');
    await dexOverview.expectToBeOnPage();
  });

  test('scrolls overview to selected number on back button', async ({
    dexDetail,
    dexOverview,
    page,
  }) => {
    await dexDetail.mockRpcs();
    await dexOverview.goto();

    await dexOverview.pokemonDetailLink('Clefairy').click();
    await dexDetail.expectToBeOnPage(35);
    await dexDetail.nextButton().click();

    await dexDetail.expectToBeOnPage(149);
    await page.getByText('Dragonite', { exact: true }).scrollIntoViewIfNeeded(); // workaround for "waiting for this to be stable"

    await dexDetail.backButton().click();
    await dexOverview.expectToBeOnPage();

    await expect(dexOverview.pokemonDetailLink('Dragonite')).toBeInViewport();
    await expect(
      dexOverview.pokemonDetailLink('Clefairy')
    ).not.toBeInViewport();
  });

  test('click outside of dialog closes the dialog', async ({
    dexDetail,
    dexOverview,
    page,
  }) => {
    await dexDetail.mockRpcs();
    await dexDetail.goto(19);

    const boundingBox = await dexDetail.modal().boundingBox();
    if (!boundingBox) {
      throw new Error('Expected dialog to be visible');
    }

    await page.mouse.click(boundingBox.x - 1, boundingBox.y);
    await expect(dexDetail.modal()).not.toBeVisible();
    await dexOverview.expectToBeOnPage();
  });
});

test.describe('mobile only', () => {
  test.skip(({ isMobile }) => !isMobile);

  test('navigate between dex details via swipe right', async ({
    dexDetail,
    page,
    gesture,
  }) => {
    await dexDetail.mockRpcs();
    await dexDetail.goto(19);

    // right, slightly upwards to be more realistic
    await gesture.swipe(dexDetail.modal(), { x: 102, y: 5 });
    await expect(page.getByText('Bulbasaur', { exact: true })).toBeInViewport();
  });

  test('navigate between dex details via swipe left', async ({
    dexDetail,
    page,
    gesture,
  }) => {
    await dexDetail.mockRpcs();
    await dexDetail.goto(19);

    // left, slightly downwards to be more realistic
    await gesture.swipe(dexDetail.modal(), { x: -102, y: -5 });
    await expect(page.getByText('Pikachu', { exact: true })).toBeInViewport();
  });
});

async function getSingleVisible(locator: Locator): Promise<Locator> {
  const all = await locator.all();
  const [visible, ...rest] = (
    await Promise.all(
      all.map(async (one) => [await isVisible(one), one] as const)
    )
  )
    .filter(([isInViewport]) => isInViewport)
    .map(([, locator]) => locator);

  if (!visible) {
    throw new Error(
      `Expected element to be visible but none of ${all.length.toString()} matching elements were visible`
    );
  }

  if (rest.length > 0) {
    throw new Error(
      `Expected exactly one element to be in viewport but ${rest.length.toString()} extra elements of ${all.length.toString()} matching elements were visible`
    );
  }

  return visible;
}

// Playwright does not consider things like aria-hidden or opacity, so we add this functionality ourselves
async function isVisible(element: Locator): Promise<boolean> {
  const isVisibleAccordingToPlaywright = await element.isVisible();
  const isVisibleAccordingToViewport = await isInViewport(element);
  const isVisibleAccordingToScreenReader =
    (await element.getAttribute('aria-hidden')) !== 'true';
  const isVisibleAccordingToOpacity = await element.evaluate(
    (node: HTMLElement | SVGElement | null) => {
      while (node) {
        const opacity = window.getComputedStyle(node).opacity;
        if (opacity === '0') return false;
        node = node.parentElement;
      }
      return true;
    }
  );

  return (
    isVisibleAccordingToPlaywright &&
    isVisibleAccordingToScreenReader &&
    isVisibleAccordingToOpacity &&
    isVisibleAccordingToViewport
  );
}

async function isInViewport(element: Locator): Promise<boolean> {
  const viewportSize = element.page().viewportSize();
  const boundingBox = await element.boundingBox();

  if (!viewportSize || !boundingBox) {
    return false;
  }

  const isBoundingBoxVisible = boundingBox.x >= 0 && boundingBox.y >= 0;
  const isBoundingBoxInViewport =
    boundingBox.x + boundingBox.width <= viewportSize.width &&
    boundingBox.y + boundingBox.height <= viewportSize.height;

  return isBoundingBoxVisible && isBoundingBoxInViewport;
}
