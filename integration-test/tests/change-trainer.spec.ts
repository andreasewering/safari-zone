import { expect } from '@playwright/test';
import { Direction } from '../generated/direction';
import { test } from '../utils/test-extensions';
import { GrpcStatus } from '../utils/grpc-status';

test('change trainer success', async ({
  trainerDetail,
  safari,
  rpc,
  page,
  canvasCapture,
  eventWaiter,
}) => {
  const mapId = 1337n;
  const userId = 3456n;
  await trainerDetail.mockRpcs();
  const { setTrainerNumber } = await safari.mockRpcs({
    mapId,
    userId,
    x: 0,
    y: 0,
    direction: Direction.D,
  });
  await trainerDetail.goto(5);

  await rpc.mock('kangaskhan.KangaskhanService/SetTrainerSprite');
  const trainerImageLoad = page.waitForResponse('**/trainer/5.png');
  setTrainerNumber(5);

  await trainerDetail.confirmTrainerChoice();

  const mapLoadEvent = eventWaiter.waitForMapLoad(mapId);
  await safari.expectToBeOnPage();
  await trainerImageLoad;
  await mapLoadEvent;
  await canvasCapture.screenshotCanvas();
});

test('loading state', async ({ trainerDetail, rpc, page }) => {
  await page.emulateMedia({ reducedMotion: 'reduce' });
  await trainerDetail.mockRpcs();
  await trainerDetail.goto(5);

  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/SetTrainerSprite');
  await trainerDetail.confirmTrainerChoice();

  await expect(page).toHaveScreenshot();
});

test('error state', async ({ trainerDetail, rpc, page }) => {
  await page.emulateMedia({ reducedMotion: 'reduce' });
  await trainerDetail.mockRpcs();
  await trainerDetail.goto(5);

  await rpc.mock('kangaskhan.KangaskhanService/SetTrainerSprite', {
    grpcStatus: GrpcStatus.InvalidArgument,
  });
  await trainerDetail.confirmTrainerChoice();

  await expect(page).toHaveScreenshot();
});

test('via keyboard', async ({ trainerDetail, rpc, keyboard }) => {
  await trainerDetail.mockRpcs();
  await trainerDetail.goto(5);

  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/SetTrainerSprite');
  await keyboard.findAndPressEnter(trainerDetail.confirmButton());

  expect(
    rpc.mocks['kangaskhan.KangaskhanService/SetTrainerSprite']?.requests
  ).toEqual([{ trainerNumber: 5 }]);
});

test('other player changes trainer', async ({
  safari,
  canvasCapture,
  page,
  websocket,
}) => {
  const mapId = 1234n;
  const userId = 4321n;
  await safari.mockRpcs({ mapId, userId, x: 1, y: 1, direction: Direction.L });
  await safari.goto();

  await websocket.waitForConnect();

  const otherUserId = 1111n;
  const trainerTexture = page.waitForResponse('**/trainer/2.png');

  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'playerEnteredMap',
      playerEnteredMap: {
        userId: otherUserId,
        coordX: 3,
        coordY: 0,
        layerNumber: 0,
        direction: Direction.R,
        userName: 'Other player',
        trainerNumber: 2,
        partnerIsShiny: false,
        partnerPokemonNumber: 1,
      },
    },
  });
  await trainerTexture;

  const newTrainerTexture = page.waitForResponse('**/trainer/5.png');
  await websocket.sendKangaskhan({
    message: {
      oneofKind: 'playerChangedTrainerSprite',
      playerChangedTrainerSprite: {
        userId: otherUserId,
        trainerNumber: 5,
      },
    },
  });
  await newTrainerTexture;
  await canvasCapture.screenshotCanvas();
});
