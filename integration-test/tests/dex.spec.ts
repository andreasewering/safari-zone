import { expect } from '@playwright/test';
import { GrpcStatus } from '../utils/grpc-status.js';
import { test } from '../utils/test-extensions.js';

test('dex page', async ({ dexOverview, mouse, page }) => {
  await dexOverview.mockRpcs([25]);
  await dexOverview.goto();
  await mouse.moveToMiddle();

  await expect(page).toHaveTitle(/Pokédex/);
  await expect(dexOverview.pokemonDetailLink('Pikachu')).toBeVisible();
  await expect(page).toHaveScreenshot();

  const assertions: Promise<void>[] = [];
  for (let i = 1; i <= 151; i += 1) {
    if (i == 25) {
      // Pikachu is rendered at position 25 and should replace the number
      const assertion = expect(
        page.getByText(i.toString(), { exact: true })
      ).not.toBeVisible();
      assertions.push(assertion);
      continue;
    }
    // All other entries are represented as numbers since they are not in the list
    // of dex entries
    const assertion = expect(
      page.getByText(i.toString(), { exact: true })
    ).toBeVisible();
    assertions.push(assertion);
  }
  await Promise.all(assertions);
});

test('click on dex entry', async ({ dexOverview, dexDetail }) => {
  await dexDetail.mockRpcs();
  await dexOverview.goto();

  await dexOverview.pokemonDetailLink('Pikachu').click();
  await dexDetail.expectToBeOnPage(25);
});

test('navigate via keyboard', async ({ dexOverview, dexDetail, keyboard }) => {
  await dexDetail.mockRpcs();
  await dexOverview.goto();

  await keyboard.findAndPressEnter(dexOverview.pokemonDetailLink('Pikachu'));
  await dexDetail.expectToBeOnPage(25);
});

test('dark mode', async ({ dexOverview, page, mouse }) => {
  await page.emulateMedia({ colorScheme: 'dark' });
  await dexOverview.mockRpcs();
  await dexOverview.goto();
  await mouse.moveToMiddle();

  await expect(dexOverview.pokemonDetailLink('Pikachu')).toBeInViewport();
  await expect(page).toHaveScreenshot();
});

test('dex entries loading', async ({ dexOverview, rpc, mouse, page }) => {
  await rpc.mockFullyCreatedUser();
  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/GetDexEntries');
  await dexOverview.goto();
  await mouse.moveToMiddle();

  await expect(dexOverview.headline()).toBeVisible();
  await expect(page).toHaveScreenshot();
});

test('dex entries error', async ({ dexOverview, rpc, page, mouse }) => {
  await rpc.mockFullyCreatedUser();
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntries', {
    grpcStatus: GrpcStatus.Internal,
  });
  await dexOverview.goto();
  await mouse.moveToMiddle();

  await expect(dexOverview.headline()).toBeVisible();
  await expect(page).toHaveScreenshot();
});

test('dex entries token expired', async ({ rpc, dexOverview }) => {
  await dexOverview.mockRpcs();
  await rpc.setTokenExpired(['kangaskhan.KangaskhanService/GetDexEntries']);
  await dexOverview.goto();

  await expect(dexOverview.pokemonDetailLink('Pikachu')).toBeVisible();
});

test('navigate to appraisal', async ({ dexOverview, dexAppraisal }) => {
  await dexOverview.mockRpcs();
  await dexOverview.goto();

  await dexOverview.appraisalLink().click();
  await dexAppraisal.expectToBeOnPage();
});
