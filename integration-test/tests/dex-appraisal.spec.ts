import { expect } from '@playwright/test';
import { test } from '../utils/test-extensions';
import { GrpcStatus } from '../utils/grpc-status';

test('appraisal success', async ({ page, dexAppraisal, mouse }) => {
  await dexAppraisal.mockRpcs();
  await dexAppraisal.goto();
  await mouse.moveToMiddle();

  await expect(dexAppraisal.headline()).toBeVisible();
  await dexAppraisal.waitForDialog();
  await expect(page).toHaveScreenshot();
});

test('appraisal loading', async ({ page, dexAppraisal, rpc, mouse }) => {
  await rpc.mockFullyCreatedUser();
  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/GetDexEntries');
  await dexAppraisal.goto();
  await mouse.moveToMiddle();

  await expect(dexAppraisal.headline()).toBeVisible();
  await dexAppraisal.waitForDialog();
  await expect(page).toHaveScreenshot();
});

test('appraisal error', async ({ dexAppraisal, rpc, page, mouse }) => {
  await rpc.mockFullyCreatedUser();
  await rpc.mock('kangaskhan.KangaskhanService/GetDexEntries', {
    grpcStatus: GrpcStatus.Internal,
  });
  await dexAppraisal.goto();
  await mouse.moveToMiddle();

  await expect(dexAppraisal.headline()).toBeVisible();
  await dexAppraisal.waitForDialog();
  await expect(page).toHaveScreenshot();
});

test('appraisal click outside', async ({ page, dexAppraisal, dexOverview }) => {
  await dexAppraisal.mockRpcs();
  await dexAppraisal.goto();
  const boundingBox = await dexAppraisal.modal().boundingBox();
  if (!boundingBox) {
    throw new Error('Expected dialog to be visible');
  }

  await page.mouse.click(boundingBox.x - 1, boundingBox.y);
  await expect(dexAppraisal.modal()).not.toBeVisible();
  await dexOverview.expectToBeOnPage();
});
