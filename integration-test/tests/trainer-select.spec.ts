import { expect } from '@playwright/test';
import { test } from '../utils/test-extensions';
import { GrpcStatus } from '../utils/grpc-status';

test('success', async ({ trainerOverview, page, mouse }) => {
  await trainerOverview.mockRpcs();
  await trainerOverview.goto();
  await mouse.moveToMiddle();

  await expect(page).toHaveScreenshot();
});

test('loading states', async ({ trainerOverview, rpc, page, mouse }) => {
  await trainerOverview.mockRpcs();
  await rpc.waitIndefinitely('kangaskhan.KangaskhanService/GetTrainers');
  await trainerOverview.goto();
  await mouse.moveToMiddle();

  await expect(page).toHaveScreenshot();
});

test('error states', async ({ trainerOverview, rpc, page, mouse }) => {
  await trainerOverview.mockRpcs();
  await rpc.mock('kangaskhan.KangaskhanService/GetTrainers', {
    grpcStatus: GrpcStatus.Internal,
  });
  await trainerOverview.goto();
  await mouse.moveToMiddle();

  await expect(page).toHaveScreenshot();
});
