import { AccessTokenClaims, Permission } from '../generated/gardevoir.js';
import { Signer } from 'protobuf-web-token';
import crypto from 'node:crypto';
import nacl from 'tweetnacl';
import { MockRpc } from './mock-server.js';

function setup() {
  if (!process.env.PRIVATE_KEY) {
    throw new Error('PRIVATE_KEY env variable not set in .env file');
  }
  const privateKey = pemToPrivateKey(process.env.PRIVATE_KEY);
  return new Signer(AccessTokenClaims, privateKey);
}

export function generateToken(
  claims: AccessTokenClaims,
  validForMilliseconds = 60_000
): string {
  const signer = setup();
  return signer.sign(claims, validForMilliseconds);
}

export class Auth {
  constructor(private readonly rpc: MockRpc) {}

  async login(
    options: Partial<{
      userId: bigint;
      permissions: Permission[];
      mapId: bigint;
    }> = {}
  ): Promise<void> {
    const userId = options.userId ?? 1337n;
    const permissions = options.permissions ?? [];
    const mapId = options.mapId;
    await this.rpc.mock('gardevoir.GardevoirService/Refresh', {
      accessToken: generateToken({ userId, permissions, mapId }, 1000),
      validForSeconds: 1000,
    });
  }
}

function pemToPrivateKey(pem: string): Uint8Array {
  const cryptoPK = crypto.createPrivateKey({
    key: `-----BEGIN PRIVATE KEY-----\n${pem}\n-----END PRIVATE KEY-----`,
    format: 'pem',
  });
  const buf = cryptoPK.export({ format: 'der', type: 'pkcs8' });
  const seed = new Uint8Array(buf).slice(buf.length - 32);
  return nacl.sign.keyPair.fromSeed(seed).secretKey;
}
