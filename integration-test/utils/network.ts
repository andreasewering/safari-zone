import { Page } from '@playwright/test';

export function recordNetwork(page: Page): Network {
  const network = new Network();
  page.on('response', (response) => {
    network.responses.push({ status: response.status(), url: response.url() });
  });
  return network;
}

export class Network {
  responses: Response[] = [];

  getNon2xxResponses(): Response[] {
    return this.responses.filter((response) => response.status > 299);
  }
}

type Response = {
  status: number;
  url: string;
};
