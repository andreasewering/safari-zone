import { test as base, expect } from '@playwright/test';
import { MockRpc } from './mock-server';
import { Network, recordNetwork } from './network';
import { TrainerOverviewPage } from '../pages/trainer-overview';
import { TrainerDetailPage } from '../pages/trainer-detail';
import { PokemonSelectPage } from '../pages/pokemon-select';
import { MapSelectPage } from '../pages/map-select';
import { Auth } from './auth';
import { MockTime } from './time';
import { WebsocketMock } from './websocket';
import { StaticFiles } from './static-files';
import { CanvasCapture } from './webgl';
import { EventWaiter } from './event';
import { Gesture } from './gesture';
import { Mouse } from './mouse';
import { DexOverviewPage } from '../pages/dex-overview';
import { DexDetailPage } from '../pages/dex-detail';
import { DexAppraisalPage } from '../pages/dex-appraisal';
import { Keyboard } from './keyboard';
import { SettingsPage } from '../pages/settings';
import { SafariPage } from '../pages/safari';

type Extensions = {
  rpc: MockRpc;
  network: Network;
  auth: Auth;
  safari: SafariPage;
  trainerOverview: TrainerOverviewPage;
  trainerDetail: TrainerDetailPage;
  pokemonSelect: PokemonSelectPage;
  mapSelect: MapSelectPage;
  dexOverview: DexOverviewPage;
  dexDetail: DexDetailPage;
  dexAppraisal: DexAppraisalPage;
  settings: SettingsPage;
  time: MockTime;
  websocket: WebsocketMock;
  staticFiles: StaticFiles;
  canvasCapture: CanvasCapture;
  eventWaiter: EventWaiter;
  gesture: Gesture;
  mouse: Mouse;
  keyboard: Keyboard;
};

export const test = base.extend<Extensions>({
  page: async ({ page }, use) => {
    await use(page);
  },
  time: [
    async ({ page }, use) => {
      const mockTime = new MockTime(page);
      await mockTime.resume();
      await use(mockTime);
    },
    { auto: true },
  ],
  websocket: [
    async ({ page }, use) => {
      const websocket = new WebsocketMock(page);
      await websocket.register();
      await use(websocket);
    },
    { auto: true },
  ],
  rpc: async ({ page }, use) => {
    const rpc = new MockRpc(page);
    await use(rpc);
    expect(rpc.getUnusedMocks()).toEqual(new Set());
  },
  gesture: async ({ page }, use) => {
    await use(new Gesture(page));
  },
  network: [
    async ({ page }, use) => {
      const network = recordNetwork(page);
      await use(network);
      expect(network.getNon2xxResponses()).toEqual([]);
    },
    { auto: true },
  ],
  auth: async ({ rpc }, use) => {
    const auth = new Auth(rpc);
    await use(auth);
  },
  mouse: async ({ page }, use) => {
    await use(new Mouse(page));
  },
  keyboard: async ({ page }, use) => {
    await use(new Keyboard(page));
  },
  eventWaiter: async ({ page }, use) => {
    const eventWaiter = await EventWaiter.create(page);
    await use(eventWaiter);
  },
  staticFiles: [
    async ({ page }, use) => {
      const staticFiles = new StaticFiles(page);
      await staticFiles.serve();
      await use(staticFiles);
    },
    { auto: true },
  ],
  canvasCapture: async ({ page }, use, testInfo) => {
    const canvasCapture = new CanvasCapture(page, testInfo);
    await use(canvasCapture);
    canvasCapture.failTestIfAnySnapshotsWereCreated();
  },
  safari: async ({ page, rpc }, use) => {
    await use(new SafariPage(page, rpc));
  },
  trainerOverview: async ({ page, rpc }, use) => {
    await use(new TrainerOverviewPage(page, rpc));
  },
  trainerDetail: async ({ page, rpc }, use) => {
    await use(new TrainerDetailPage(page, rpc));
  },
  pokemonSelect: async ({ page }, use) => {
    await use(new PokemonSelectPage(page));
  },
  mapSelect: async ({ page }, use) => {
    await use(new MapSelectPage(page));
  },
  dexOverview: async ({ page, rpc }, use) => {
    await use(new DexOverviewPage(page, rpc));
  },
  dexDetail: async ({ page, rpc }, use) => {
    await use(new DexDetailPage(page, rpc));
  },
  dexAppraisal: async ({ page, rpc, eventWaiter }, use) => {
    await use(new DexAppraisalPage(page, rpc, eventWaiter));
  },
  settings: async ({ page }, use) => {
    await use(new SettingsPage(page));
  },
});
