import { IGardevoirServiceClient } from '../generated/gardevoir.client.js';
import { ISmeargleServiceClient } from '../generated/smeargle.client.js';
import { IArceusServiceClient } from '../generated/arceus.client.js';
import { IChatotServiceClient } from '../generated/chatot.client.js';
import { IKangaskhanServiceClient } from '../generated/kangaskhan.client.js';
import { Page } from '@playwright/test';
import { MessageType } from '@protobuf-ts/runtime';
import {
  CreateAreaRequest,
  CreateAreaResponse,
  DeleteAreaRequest,
  DeleteAreaResponse,
  EditAreaRequest,
  EditAreaResponse,
  GetAreaDetailRequest,
  GetAreaDetailResponse,
  GetAreasRequest,
  GetAreasResponse,
  GetAudioTracksRequest,
  GetAudioTracksResponse,
  TranslateRequest,
  TranslateResponse,
} from '../generated/arceus.js';
import {
  AddFriendRequest,
  AddFriendResponse,
  AddToGroupRequest,
  AddToGroupResponse,
  CreateGroupRequest,
  CreateGroupResponse,
  GetFriendRequestsRequest,
  GetFriendRequestsResponse,
  GetFriendsRequest,
  GetFriendsResponse,
  GetGroupDetailRequest,
  GetGroupDetailResponse,
  GetGroupsRequest,
  GetGroupsResponse,
  MarkFriendAsFavoriteRequest,
  MarkFriendAsFavoriteResponse,
  MarkGroupAsFavoriteRequest,
  MarkGroupAsFavoriteResponse,
  RemoveFriendRequest,
  RemoveFriendResponse,
  RemoveFromGroupRequest,
  RemoveFromGroupResponse,
  RemoveGroupRequest,
  RemoveGroupResponse,
  RequestGroupMembershipRequest,
  RequestGroupMembershipResponse,
} from '../generated/chatot.js';
import {
  AccessTokenData,
  GetConfigRequest,
  GetConfigResponse,
  LoginRequest,
  RegistrationRequest,
  VerificationRequest,
} from '../generated/gardevoir.js';
import { Empty } from '../generated/google/protobuf/empty.js';
import {
  AddWarpRequest,
  AddWarpResponse,
  ChooseStartTileRequest,
  ChooseStartTileResponse,
  DeleteWarpRequest,
  DeleteWarpResponse,
  GetAllPokemonRequest,
  GetAllPokemonResponse,
  GetDexEntriesRequest,
  GetDexEntriesResponse,
  GetDexEntryDetailRequest,
  GetDexEntryDetailResponse,
  GetPokemonRequest,
  GetPokemonResponse,
  GetStarterPokemonRequest,
  GetStarterPokemonResponse,
  GetTrainersRequest,
  GetTrainersResponse,
  GetUserDataRequest,
  GetUserDataResponse,
  GetUserItemsRequest,
  GetUserItemsResponse,
  GetWarpsRequest,
  GetWarpsResponse,
  SetPartnerPokemonRequest,
  SetPartnerPokemonResponse,
  SetTrainerSpriteRequest,
  SetTrainerSpriteResponse,
} from '../generated/kangaskhan.js';
import {
  AddStartTileRequest,
  AddStartTileResponse,
  CreateMapRequest,
  CreateMapResponse,
  DeleteStartTileRequest,
  DeleteStartTileResponse,
  EditMapRequest,
  EditMapResponse,
  FindFittingTilesRequest,
  FindFittingTilesResponse,
  GetAllStartTilesRequest,
  GetAllStartTilesResponse,
  GetAnimationConfigRequest,
  GetAnimationConfigResponse,
  GetMapsRequest,
  GetMapsResponse,
  GetStartTilesRequest,
  GetStartTilesResponse,
  GetTextureMetadataRequest,
  GetTextureMetadataResponse,
  GetTileConfigRequest,
  GetTileConfigResponse,
  GetTileDetailsRequest,
  GetTileDetailsResponse,
  GetTilesetConfigRequest,
  GetTilesetConfigResponse,
  GetTilesRequest,
  GetTilesResponse,
} from '../generated/smeargle.js';
import { GrpcStatus } from './grpc-status.js';
import { generateToken } from './auth.js';
import { Direction } from '../generated/direction.js';

type CapitalizeFirst<T> = T extends `${infer F}${infer R}`
  ? `${Uppercase<F>}${R}`
  : never;

type Services = {
  'gardevoir.GardevoirService': IGardevoirServiceClient;
  'smeargle.SmeargleService': ISmeargleServiceClient;
  'arceus.ArceusService': IArceusServiceClient;
  'kangaskhan.KangaskhanService': IKangaskhanServiceClient;
  'chatot.ChatotService': IChatotServiceClient;
};

type ServiceDeclaration<S extends keyof Services> = {
  [K in keyof Services[S]]: {
    url: `${S}/${CapitalizeFirst<K>}`;
    responseType: Awaited<
      ReturnType<
        // Somehow typescript cannot infer that Services[S][K] extends "(...args: any) => any" anyways
        // so this is a workaround which produces the right autocompletes and type hints
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        Services[S][K] extends (...args: any) => any ? Services[S][K] : never
      >
    >['response'];
    requestType: Services[S][K] extends (input: infer I) => unknown ? I : never;
  };
};

type AllServicesDeclarations = {
  [S in keyof Services]: ServiceDeclaration<S>;
};

// New Flatten type to correctly extract each service and method into a flat structure
type FlattenServices = {
  [S in keyof AllServicesDeclarations]: {
    [M in keyof AllServicesDeclarations[S]]: {
      url: AllServicesDeclarations[S][M]['url'];
      responseType: AllServicesDeclarations[S][M]['responseType'];
      requestType: AllServicesDeclarations[S][M]['requestType'];
    };
  }[keyof AllServicesDeclarations[S]];
}[keyof AllServicesDeclarations];

type URLToTypes = {
  [Item in FlattenServices as Item['url']]: {
    response: Item['responseType'];
    request: Item['requestType'];
  };
};

export type RequestType<RPC extends keyof URLToTypes> =
  URLToTypes[RPC]['request'];
export type ResponseType<RPC extends keyof URLToTypes> =
  URLToTypes[RPC]['response'];

type UrlToCodecs = {
  [U in keyof URLToTypes]: [
    MessageType<RequestType<U>>,
    MessageType<ResponseType<U>>
  ];
};

const urlToCodec: UrlToCodecs = {
  'arceus.ArceusService/CreateArea': [CreateAreaRequest, CreateAreaResponse],
  'arceus.ArceusService/DeleteArea': [DeleteAreaRequest, DeleteAreaResponse],
  'arceus.ArceusService/EditArea': [EditAreaRequest, EditAreaResponse],
  'arceus.ArceusService/GetAreaDetail': [
    GetAreaDetailRequest,
    GetAreaDetailResponse,
  ],
  'arceus.ArceusService/GetAreas': [GetAreasRequest, GetAreasResponse],
  'arceus.ArceusService/GetAudioTracks': [
    GetAudioTracksRequest,
    GetAudioTracksResponse,
  ],
  'arceus.ArceusService/Translate': [TranslateRequest, TranslateResponse],
  'chatot.ChatotService/AddFriend': [AddFriendRequest, AddFriendResponse],
  'chatot.ChatotService/AddToGroup': [AddToGroupRequest, AddToGroupResponse],
  'chatot.ChatotService/CreateGroup': [CreateGroupRequest, CreateGroupResponse],
  'chatot.ChatotService/GetFriendRequests': [
    GetFriendRequestsRequest,
    GetFriendRequestsResponse,
  ],
  'chatot.ChatotService/GetFriends': [GetFriendsRequest, GetFriendsResponse],
  'chatot.ChatotService/GetGroupDetail': [
    GetGroupDetailRequest,
    GetGroupDetailResponse,
  ],
  'chatot.ChatotService/GetGroups': [GetGroupsRequest, GetGroupsResponse],
  'chatot.ChatotService/MarkFriendAsFavorite': [
    MarkFriendAsFavoriteRequest,
    MarkFriendAsFavoriteResponse,
  ],
  'chatot.ChatotService/MarkGroupAsFavorite': [
    MarkGroupAsFavoriteRequest,
    MarkGroupAsFavoriteResponse,
  ],
  'chatot.ChatotService/RemoveFriend': [
    RemoveFriendRequest,
    RemoveFriendResponse,
  ],
  'chatot.ChatotService/RemoveFromGroup': [
    RemoveFromGroupRequest,
    RemoveFromGroupResponse,
  ],
  'chatot.ChatotService/RemoveGroup': [RemoveGroupRequest, RemoveGroupResponse],
  'chatot.ChatotService/RequestGroupMembership': [
    RequestGroupMembershipRequest,
    RequestGroupMembershipResponse,
  ],
  'gardevoir.GardevoirService/GetConfig': [GetConfigRequest, GetConfigResponse],
  'gardevoir.GardevoirService/Login': [LoginRequest, AccessTokenData],
  'gardevoir.GardevoirService/Logout': [Empty, Empty],
  'gardevoir.GardevoirService/Refresh': [Empty, AccessTokenData],
  'gardevoir.GardevoirService/Register': [RegistrationRequest, Empty],
  'gardevoir.GardevoirService/Verify': [VerificationRequest, Empty],
  'kangaskhan.KangaskhanService/AddWarp': [AddWarpRequest, AddWarpResponse],
  'kangaskhan.KangaskhanService/ChooseStartTile': [
    ChooseStartTileRequest,
    ChooseStartTileResponse,
  ],
  'kangaskhan.KangaskhanService/DeleteWarp': [
    DeleteWarpRequest,
    DeleteWarpResponse,
  ],
  'kangaskhan.KangaskhanService/GetAllPokemon': [
    GetAllPokemonRequest,
    GetAllPokemonResponse,
  ],
  'kangaskhan.KangaskhanService/GetDexEntries': [
    GetDexEntriesRequest,
    GetDexEntriesResponse,
  ],
  'kangaskhan.KangaskhanService/GetDexEntryDetail': [
    GetDexEntryDetailRequest,
    GetDexEntryDetailResponse,
  ],
  'kangaskhan.KangaskhanService/GetPokemon': [
    GetPokemonRequest,
    GetPokemonResponse,
  ],
  'kangaskhan.KangaskhanService/GetStarterPokemon': [
    GetStarterPokemonRequest,
    GetStarterPokemonResponse,
  ],
  'kangaskhan.KangaskhanService/GetTrainers': [
    GetTrainersRequest,
    GetTrainersResponse,
  ],
  'kangaskhan.KangaskhanService/GetUserData': [
    GetUserDataRequest,
    GetUserDataResponse,
  ],
  'kangaskhan.KangaskhanService/GetUserItems': [
    GetUserItemsRequest,
    GetUserItemsResponse,
  ],
  'kangaskhan.KangaskhanService/GetWarps': [GetWarpsRequest, GetWarpsResponse],
  'kangaskhan.KangaskhanService/SetPartnerPokemon': [
    SetPartnerPokemonRequest,
    SetPartnerPokemonResponse,
  ],
  'kangaskhan.KangaskhanService/SetTrainerSprite': [
    SetTrainerSpriteRequest,
    SetTrainerSpriteResponse,
  ],
  'smeargle.SmeargleService/AddStartTile': [
    AddStartTileRequest,
    AddStartTileResponse,
  ],
  'smeargle.SmeargleService/CreateMap': [CreateMapRequest, CreateMapResponse],
  'smeargle.SmeargleService/DeleteStartTile': [
    DeleteStartTileRequest,
    DeleteStartTileResponse,
  ],
  'smeargle.SmeargleService/EditMap': [EditMapRequest, EditMapResponse],
  'smeargle.SmeargleService/FindFittingTiles': [
    FindFittingTilesRequest,
    FindFittingTilesResponse,
  ],
  'smeargle.SmeargleService/GetAllStartTiles': [
    GetAllStartTilesRequest,
    GetAllStartTilesResponse,
  ],
  'smeargle.SmeargleService/GetAnimationConfig': [
    GetAnimationConfigRequest,
    GetAnimationConfigResponse,
  ],
  'smeargle.SmeargleService/GetMaps': [GetMapsRequest, GetMapsResponse],
  'smeargle.SmeargleService/GetStartTiles': [
    GetStartTilesRequest,
    GetStartTilesResponse,
  ],
  'smeargle.SmeargleService/GetTextureMetadata': [
    GetTextureMetadataRequest,
    GetTextureMetadataResponse,
  ],
  'smeargle.SmeargleService/GetTileConfig': [
    GetTileConfigRequest,
    GetTileConfigResponse,
  ],
  'smeargle.SmeargleService/GetTileDetails': [
    GetTileDetailsRequest,
    GetTileDetailsResponse,
  ],
  'smeargle.SmeargleService/GetTiles': [GetTilesRequest, GetTilesResponse],
  'smeargle.SmeargleService/GetTilesetConfig': [
    GetTilesetConfigRequest,
    GetTilesetConfigResponse,
  ],
};

export class MockRpc {
  mocks: UrlToMocks = {};
  isTokenExpired = false;

  constructor(private readonly page: Page) {}

  async waitIndefinitely<const RPC extends keyof URLToTypes>(rpc: RPC) {
    await this.mock(rpc, () => new Promise(() => {}));
  }

  async setTokenExpired(forRpcs: (keyof URLToTypes)[]) {
    this.isTokenExpired = true;
    const previousHandlers: any = {};

    for (const rpc of forRpcs) {
      const mock = this.mocks[rpc];
      if (mock) {
        previousHandlers[rpc] = mock.handler;
        mock.handler = () => {
          return {
            grpcStatus: GrpcStatus.Unauthenticated,
          };
        };
      }
    }

    const previousRefreshMock = await this.mock(
      'gardevoir.GardevoirService/Refresh',
      async (
        request: RequestType<'gardevoir.GardevoirService/Refresh'>
      ): Promise<ResponseType<'gardevoir.GardevoirService/Refresh'>> => {
        for (const [url, handler] of Object.entries(previousHandlers) as [
          keyof URLToTypes,
          RpcHandler<any>
        ][]) {
          const mock = this.mocks[url];
          if (mock) {
            mock.handler = handler;
          }
        }

        return this.evaluateHandler(
          'gardevoir.GardevoirService/Refresh',
          request,
          previousRefreshMock
        ) as Promise<ResponseType<'gardevoir.GardevoirService/Refresh'>>;
      }
    );
  }

  async mockFullyCreatedUser(
    options: {
      mapId?: bigint;
      userId?: bigint;
      x?: number;
      y?: number;
      direction?: Direction;
    } = {}
  ): Promise<{
    setPartnerPokemon: (pokemonNumber: number) => void;
    setTrainerNumber: (trainerNumber: number) => void;
  }> {
    const mapId = options.mapId ?? 11111n;
    const userId = options.userId ?? 12345n;
    const x = options.x ?? 0;
    const y = options.y ?? 0;
    const direction = options.direction ?? Direction.L;

    let trainerNumber = 1;
    let pokemonNumber = 25;
    const accessToken = generateToken({
      userId,
      mapId,
      permissions: [],
    });
    await this.mock('gardevoir.GardevoirService/Refresh', {
      accessToken,
      validForSeconds: 1000,
    });
    await this.mock('kangaskhan.KangaskhanService/GetUserData', () => ({
      name: 'Tiberius Estor',
      trainer: { trainerNumber },
      position: {
        x,
        y,
        layerNumber: 0,
        mapId,
        direction,
      },
      partnerPokemon: {
        pokemonNumber,
        isShiny: false,
      },
    }));

    return {
      setPartnerPokemon: (number) => {
        pokemonNumber = number;
      },
      setTrainerNumber: (number) => {
        trainerNumber = number;
      },
    };
  }

  async mock<const RPC extends keyof URLToTypes>(
    rpc: RPC,
    respond?: RpcHandler<RPC>
  ): Promise<RpcHandler<RPC> | undefined> {
    const previousMock = this.mocks[rpc];
    if (previousMock) {
      const previousHandler = previousMock.handler;
      previousMock.handler = respond;
      return previousHandler;
    }

    // This is fine, since RPC will never be a union of more than one string
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-explicit-any
    this.mocks[rpc] = { requests: [], handler: respond } as any;

    await this.page.route(`**/${rpc}`, async (route) => {
      console.log('intercepted request', route.request().url());
      const mock = this.mocks[rpc];
      if (!mock) {
        throw new Error('To get here, mocks[rpc] should have always been set');
      }

      const requestBinary = route.request().postDataBuffer();
      if (!requestBinary) {
        throw new Error(
          'Rpc request did not include request body in binary format'
        );
      }
      const [requestCodec, responseCodec] = urlToCodec[rpc];
      const request = requestCodec.fromBinary(
        unframeGrpcMessage(requestBinary)
      );
      mock.requests.push(request);
      const response = await this.evaluateHandler(rpc, request, mock.handler);
      if (
        response &&
        'grpcStatus' in response &&
        typeof response.grpcStatus === 'number'
      ) {
        return route.fulfill({
          status: 200,
          contentType: 'application/grpc-web+proto',
          headers: {
            'grpc-status': response.grpcStatus.toString(),
          },
        });
      }
      if (
        response &&
        'httpStatus' in response &&
        typeof response.httpStatus === 'number'
      ) {
        return route.fulfill({
          status: response.httpStatus,
        });
      }
      const binary = responseCodec.toBinary(response);
      const body = frameGrpcMessage(binary);

      await route.fulfill({
        status: 200,
        contentType: 'application/grpc-web+proto',
        body,
      });
    });
  }

  getUnusedMocks(): Set<keyof URLToTypes> {
    const calledUrls = Object.entries(this.mocks).flatMap(([url, mock]) =>
      mock.requests.length === 0 ? [url as keyof URLToTypes] : []
    );
    return new Set(calledUrls);
  }

  private async evaluateHandler<RPC extends keyof URLToTypes>(
    rpc: RPC,
    request: RequestType<RPC>,
    handler?: RpcHandler<RPC>
  ): Promise<
    ResponseType<RPC> | { httpStatus: number } | { grpcStatus: GrpcStatus }
  > {
    const [, responseCodec] = urlToCodec[rpc];
    let response = responseCodec.fromJson({});
    if (handler !== undefined) {
      response = await handler;
    }
    if (typeof handler === 'function') {
      response = (await handler(request)) as
        | ResponseType<RPC>
        | { httpStatus: number }
        | { grpcStatus: GrpcStatus };
    }
    return response;
  }
}

type RpcHandler<RPC extends keyof URLToTypes> =
  | ((
      input: RequestType<RPC>
    ) =>
      | ResponseType<RPC>
      | { httpStatus: number }
      | { grpcStatus: GrpcStatus }
      | Promise<ResponseType<RPC>>
      | Promise<{ httpStatus: number }>
      | Promise<{ grpcStatus: GrpcStatus }>)
  | ResponseType<RPC>
  | Promise<ResponseType<RPC>>
  | { httpStatus: number }
  | { grpcStatus: GrpcStatus };

type UrlToMocks = {
  [U in keyof URLToTypes]?: {
    requests: URLToTypes[U]['request'][];
    handler?: RpcHandler<U>;
  };
};

function frameGrpcMessage(binary: Uint8Array): Buffer {
  const buffer = Buffer.from(binary.buffer);
  const trailers = Buffer.from('grpc-status:0', 'utf-8');

  // Shape: 0          a a a a     b b b b b b b b b b b b     c c c c d d d d d d d     e f
  //        ^               ^                     ^             ^              ^         ^ ^
  // compression flag  content-length     actual message    trailers-length  trailers    \r\n
  const body = Buffer.alloc(5 + binary.length + 5 + trailers.length + 2);
  body[0] = 0; // Assuming the message is uncompressed
  body.writeUInt32BE(binary.length, 1);
  buffer.copy(body, 5);
  body[5 + binary.length] = 128;
  body.writeUInt32BE(trailers.length, 5 + binary.length + 1);
  trailers.copy(body, 5 + binary.length + 5);
  body.writeUInt8(13, 5 + binary.length + 5 + trailers.length);
  body.writeUInt8(10, 5 + binary.length + 5 + trailers.length + 1);

  return body;
}

function unframeGrpcMessage(buffer: Buffer): Uint8Array {
  const binary = new Uint8Array(buffer);
  const dataView = new DataView(binary.buffer, 0, 5);
  // not sure why protobuf-ts does this
  const isPaddingEncoded = dataView.getUint8(0);
  if (isPaddingEncoded) {
    return new Uint8Array();
  }
  const length = dataView.getUint32(1, false);
  return binary.slice(5, 5 + length);
}
