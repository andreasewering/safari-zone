import { Locator, Page } from '@playwright/test';

export class Keyboard {
  constructor(private readonly page: Page) {}

  async findAndPressEnter(locator: Locator) {
    await this.tabUntilInFocus(locator);
    await this.page.keyboard.press('Enter');
  }

  private async tabUntilInFocus(
    locator: Locator,
    maxTries = 20
  ): Promise<Locator> {
    let leftoverTries = maxTries;
    while (
      !(await locator.evaluate((node) => {
        return document.activeElement === node;
      })) &&
      leftoverTries > 0
    ) {
      await locator.page().keyboard.press('Tab');
      leftoverTries -= 1;
    }
    if (leftoverTries <= 0) {
      throw new Error(
        `Expected to focus element within ${maxTries.toString()} tries but failed.`
      );
    }

    return locator;
  }
}
