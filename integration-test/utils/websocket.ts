// Idea: use MockSocket to override Websocket global via init script.

import { Page, WebSocketRoute } from '@playwright/test';
import { WebsocketMessageToClient } from '../generated/kangaskhan';

export class WebsocketMock {
  private ws: WebSocketRoute[] = [];
  private connectCallbacks: Array<() => void> = [];
  constructor(private readonly page: Page) {}

  async sendKangaskhan(web: WebsocketMessageToClient) {
    const msg = WebsocketMessageToClient.toBinary(web);
    await this.sendBinary(msg);
  }

  async waitForConnect(): Promise<void> {
    return new Promise<void>((resolve) => {
      this.connectCallbacks.push(resolve);
    });
  }

  async sendBinary(binary: Uint8Array) {
    for (const ws of this.ws) {
      ws.send(Buffer.from(binary.buffer));
    }
  }

  async register() {
    this.page.routeWebSocket(/kangaskhan\/ws/, (ws) => {
      this.ws.push(ws);
      for (const callback of this.connectCallbacks) {
        callback();
      }
      this.connectCallbacks = [];
    });
  }
}
