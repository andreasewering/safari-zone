import { Page } from '@playwright/test';

export class StaticFiles {
  constructor(private readonly page: Page) {}

  async serve(): Promise<void> {
    await this.page.route('**/static/**', (route) => {
      const { pathname } = new URL(route.request().url());
      route.fulfill({
        status: 200,
        path: pathname.substring(1), // remove leading slash
      });
    });
  }
}
