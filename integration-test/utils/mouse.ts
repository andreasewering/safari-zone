import { Page } from 'playwright/test';

export class Mouse {
  constructor(private readonly page: Page) {}

  async moveToMiddle() {
    const viewport = this.page.viewportSize();
    if (viewport) {
      await this.page.mouse.move(viewport.width / 2, viewport.height / 2);
    }
  }
}
