import { Locator, Page } from '@playwright/test';

export class Gesture {
  constructor(private readonly page: Page) {}

  async swipe(locator: Locator, relativeDistance: { x: number; y: number }) {
    const stats = await locator.evaluate((target) => {
      const { top, left, right, bottom } = target.getBoundingClientRect();
      const touch = new Touch({
        identifier: 0,
        target,
        clientX: (left + right) / 2,
        clientY: (top + bottom) / 2,
      });
      const touchEvent = new TouchEvent('touchstart', {
        bubbles: true,
        cancelable: true,
        touches: [touch],
        changedTouches: [touch],
      });
      target.dispatchEvent(touchEvent);
      return { x: touch.clientX, y: touch.clientY, top, left, right, bottom };
    });

    for (let i = 1; i <= 10; i++) {
      await locator.evaluate(
        (target, { clientX, clientY }) => {
          const touch = new Touch({
            identifier: 0,
            target,
            clientX,
            clientY,
          });
          const touchEvent = new TouchEvent('touchmove', {
            bubbles: true,
            cancelable: true,
            touches: [touch],
            changedTouches: [touch],
          });
          target.dispatchEvent(touchEvent);
        },
        {
          clientX: stats.x + (i * relativeDistance.x) / 10,
          clientY: stats.y + (i * relativeDistance.y) / 10,
        }
      );
    }
    await locator.evaluate(
      (target, { clientX, clientY }) => {
        const touch = new Touch({
          identifier: 0,
          target,
          clientX,
          clientY,
        });
        const touchEvent = new TouchEvent('touchend', {
          bubbles: true,
          cancelable: true,
          touches: [touch],
          changedTouches: [touch],
        });
        target.dispatchEvent(touchEvent);
      },
      {
        clientX: stats.x + relativeDistance.x,
        clientY: stats.y + relativeDistance.y,
      }
    );
  }
}
