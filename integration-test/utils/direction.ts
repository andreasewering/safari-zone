export type Direction = 'L' | 'U' | 'R' | 'D';

export function directionToVector(direction: Direction): {
  x: number;
  y: number;
} {
  switch (direction) {
    case 'L':
      return { x: -1, y: 0 };
    case 'U':
      return { x: 0, y: -1 };
    case 'R':
      return { x: 1, y: 0 };
    case 'D':
      return { x: 0, y: 1 };
  }
}
