import { Page } from '@playwright/test';

export class MockTime {
  // addInitScript ordering is not defined so we define it ourselves.
  // Later calls to resume will get a higher applicationOrder and
  // our code makes sure that the higher applicationOrder results in the offset with that order winning.
  private applicationOrder = 0;
  constructor(private readonly page: Page) {}

  /**
   * Control the time which the page is navigated on.
   * After that, time will resume "normally", e.g. after 1 minute Date.now() will
   * return the given start time + 1 minute.
   *
   * @param start Time on which the page is navigated on
   */
  async resume(start: Date = new Date(2020, 1, 1, 12, 0)) {
    this.applicationOrder += 1;
    const offset = Date.now() - start.getTime();

    if (this.applicationOrder <= 1) {
      await this.page.addInitScript(`
      const oldDate = window.Date;

      function newDate(...args) {
        if (args.length === 0) {
          const oldTime = oldDate.now();
          return new oldDate(oldTime - window.mockTime.offset);
        } else {
          return new oldDate(...args);
        }
      }

      newDate.now = function() {
        return oldDate.now() - window.mockTime.offset;
      }

      newDate.UTC = oldDate.UTC;
      newDate.parse = oldDate.parse;

      window.Date = newDate;
    `);
    }

    await this.page.addInitScript(`
          const previousOrder = window.mockTime?.order ?? 0;
          if (previousOrder < ${this.applicationOrder}) {
            window.mockTime = { offset: ${offset}, order: ${this.applicationOrder} };
          }
    `);
  }
}
