import { Page } from '@playwright/test';
import { Direction, directionToVector } from './direction';

export class EventWaiter {
  static async create(page: Page) {
    const waiter = new EventWaiter(page);
    await waiter.exposeFunctionPromise;
    return waiter;
  }

  private exposeFunctionPromise: Promise<void>;

  private callbacks = new Map<string, Array<() => void>>();

  private constructor(private readonly page: Page) {
    this.exposeFunctionPromise = this.page.exposeFunction(
      'safarizoneEvent',
      (...args: unknown[]) => {
        const key = JSON.stringify(args);
        const cbs = this.callbacks.get(key);
        if (!cbs) {
          console.log(`Received event without waiting on it.
Expected one of: ${this.renderCallbacks()}
Actual: ${key}`);
          return;
        }
        for (const cb of cbs) {
          cb();
        }
        this.callbacks.delete(key);
      }
    );
  }

  async waitForMapLoad(mapId: BigInt) {
    return this.waitFor('mapLoaded', mapId.toString());
  }

  async waitForTypewriter(name: string) {
    return this.waitFor('typewriterFinished', name);
  }

  async waitForPlayerMoveComplete(
    userId: BigInt,
    { x, y, layerNumber }: { x: number; y: number; layerNumber: number },
    direction: Direction
  ) {
    return this.waitFor('playerMoveComplete', userId.toString(), {
      x,
      y,
      layerNumber,
      direction: directionToVector(direction),
    });
  }

  private renderCallbacks(): string {
    return this.callbacks.keys().toArray().join(',');
  }

  private async waitFor(...args: [string, ...unknown[]]) {
    const key = JSON.stringify(args);
    await new Promise<void>((resolve) => {
      const cbs = this.callbacks.get(key) ?? [];
      cbs.push(resolve);
      this.callbacks.set(key, cbs);
    });
  }
}
