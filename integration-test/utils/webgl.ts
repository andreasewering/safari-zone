import { Page, TestInfo } from '@playwright/test';
import { base64decode } from '@protobuf-ts/runtime';
import fs from 'fs/promises';
import os from 'node:os';

import { compare } from 'odiff-bin';
import path from 'path';

export class CanvasCapture {
  private counter = 1;
  // Track number of created snapshots per test to fail the test at the end if any were created
  private newSnapshots = 0;

  constructor(
    private readonly page: Page,
    private readonly testInfo: TestInfo
  ) {}

  async screenshotCanvas(): Promise<void> {
    const base64 = await this.getCanvasContentsBase64();
    const image = base64decode(base64);

    const snapshotPath = this.snapshotPath();
    const snapshotDir = path.dirname(snapshotPath);
    try {
      await fs.access(snapshotPath, fs.constants.F_OK);
    } catch (e) {
      await fs.mkdir(snapshotDir, { recursive: true });
      await fs.writeFile(snapshotPath, image);
      console.log(`No snapshot existed yet, created ${snapshotPath}.`);
      this.newSnapshots += 1;
      return;
    }

    const actualPath = this.actualPath();
    const diffPath = this.diffPath();
    await fs.writeFile(actualPath, image);
    const compareResult = await compare(snapshotPath, actualPath, diffPath, {
      antialiasing: true,
      threshold: 0.01,
    });
    this.counter += 1;

    if (compareResult.match) {
      return;
    }
    switch (compareResult.reason) {
      case 'file-not-exists': {
        throw new Error(
          `The file ${compareResult.file} did not exist. Well shoot, time to debug ${CanvasCapture.name}`
        );
      }
      case 'layout-diff': {
        throw new Error(
          `Screenshot comparison resulted in layout difference. Compare ${actualPath} with ${snapshotPath} to debug.`
        );
      }
      case 'pixel-diff': {
        throw new Error(
          `The new screenshot did not match the base image. 

Diff Percentage: ${compareResult.diffPercentage}
Diff Pixels: ${compareResult.diffCount}

Check ${diffPath} for a comparison.`
        );
      }
      default: {
        return unreachableCompareResult(compareResult);
      }
    }
  }

  failTestIfAnySnapshotsWereCreated() {
    if (this.newSnapshots > 0) {
      throw new Error(
        `New snapshots were created. Please run the test '${this.testInfo.title}' again after validating the screenshots manually.`
      );
    }
  }

  private snapshotPath() {
    return this.testInfo.snapshotPath(
      `${this.testInfo.title}-${this.counter.toString()}.png`
    );
  }

  private actualPath() {
    return this.testInfo.outputPath(
      `${this.testInfo.title}-${this.counter.toString()}.actual.png`
    );
  }

  private diffPath() {
    return this.testInfo.outputPath(
      `${this.testInfo.title}-${this.counter.toString()}.diff.png`
    );
  }

  private async getCanvasContentsBase64() {
    const canvas = this.page.locator('canvas');
    if (!canvas) {
      throw new Error('No canvas found');
    }
    const base64 = await canvas.evaluate((canvas) => {
      // Remove the metadata (base64;image+png or something like that) prefix
      return (canvas as HTMLCanvasElement).toDataURL().split(',')[1];
    });
    if (typeof base64 !== 'string') {
      throw new Error(
        `Expected JS to evaluate to a base64 string, but received ${base64} instead`
      );
    }
    return base64;
  }
}

function unreachableCompareResult(value: never) {
  throw new Error(`The impossible happened! ${JSON.stringify(value)}`);
}
