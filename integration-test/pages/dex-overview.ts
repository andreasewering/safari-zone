import { Locator, Page } from '@playwright/test';
import { MockRpc } from '../utils/mock-server';
import { getDexEntries, getDexEntryDetail } from '../fixtures/pokemon/pokemon';

export class DexOverviewPage {
  constructor(private readonly page: Page, private readonly rpc: MockRpc) {}

  async goto(): Promise<void> {
    await this.page.goto('/dex');
  }

  async expectToBeOnPage(): Promise<void> {
    await this.page.waitForURL('/dex');
  }

  async mockRpcs(filter?: Parameters<typeof getDexEntries>[0]): Promise<void> {
    await this.rpc.mockFullyCreatedUser();
    await this.rpc.mock(
      'kangaskhan.KangaskhanService/GetDexEntries',
      getDexEntries(filter)
    );
  }

  headline(): Locator {
    return this.page.getByText('Pokédex').and(this.page.getByRole('heading'));
  }

  pokemonDetailLink(name: string): Locator {
    return this.page.locator(`a:has(img[alt="${name}"])`);
  }

  appraisalLink(): Locator {
    return this.page.getByRole('link', { name: '/ 151' });
  }
}
