import { Locator, Page } from '@playwright/test';
import { MockRpc } from '../utils/mock-server';
import { getPokemon } from '../fixtures/pokemon/pokemon';
import testTiles from '../fixtures/maps/tiles.json';
import { Direction } from '../generated/direction';

export class SafariPage {
  constructor(private readonly page: Page, private readonly rpc: MockRpc) {}

  async goto(): Promise<void> {
    await this.page.goto('/safari');
  }

  async expectToBeOnPage() {
    if (new URL(this.page.url()).pathname == '/safari') {
      return;
    }
    await this.page.waitForURL('/safari');
  }

  dpad(): Locator {
    return this.page.getByAltText('Dpad');
  }

  superball(): Locator {
    return this.page.getByTitle('Super Ball');
  }

  backpack(): Locator {
    return this.page.getByAltText('Backpack');
  }

  async mockRpcs({
    mapId,
    userId,
    x,
    y,
    direction,
  }: {
    mapId: bigint;
    userId?: bigint;
    x: number;
    y: number;
    direction: Direction;
  }): Promise<{
    setPartnerPokemon: (pokemonNumber: number) => void;
    setTrainerNumber: (trainerNumber: number) => void;
  }> {
    const userMocks = await this.rpc.mockFullyCreatedUser({
      mapId,
      userId,
      x,
      y,
      direction,
    });
    await this.rpc.mock('kangaskhan.KangaskhanService/GetUserItems');
    await this.rpc.mock('kangaskhan.KangaskhanService/GetPokemon', getPokemon);
    await this.setupMapRelatedMocks();

    return userMocks;
  }

  async setupMapRelatedMocks(): Promise<void> {
    await this.rpc.mock('smeargle.SmeargleService/GetTilesetConfig', {
      tilesPerRow: 8,
      tileSizeInPx: 64,
      tilesetPath: '/static/tileset.png',
    });
    await this.rpc.mock('smeargle.SmeargleService/GetAnimationConfig', {
      animations: [],
    });
    await this.rpc.mock('arceus.ArceusService/GetAreas');
    await this.rpc.mock('smeargle.SmeargleService/GetTiles', {
      tiles: testTiles.map((tile) => ({
        ...tile,
        animationIds: [],
      })),
    });
    await this.rpc.mock('smeargle.SmeargleService/GetTextureMetadata', {
      path: '/static/tilemap.png',
      layers: [10, 20],
      offsetX: -10,
      offsetY: -5,
    });
  }
}
