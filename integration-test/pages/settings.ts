import { Page } from 'playwright/test';

export class SettingsPage {
  constructor(private readonly page: Page) {}

  async goto(): Promise<void> {
    await this.page.goto('/settings');
  }

  async toggleDarkmode(): Promise<void> {
    await this.page.locator('theme-switcher').getByRole('button').click();
  }
}
