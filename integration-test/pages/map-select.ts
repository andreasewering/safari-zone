import { expect, Page } from '@playwright/test';

export class MapSelectPage {
  constructor(private readonly page: Page) {}

  async goto(): Promise<void> {
    await this.page.goto('/map-select');
  }

  async chooseMap(name: string): Promise<void> {
    await this.verifyUrl();
    await this.page.getByText(name).click();
    await this.page.getByText("Let's go!").click();
  }

  async waitForSuccessTitle(): Promise<void> {
    await this.verifyUrl();
    await expect(this.page).toHaveTitle(/Starting point selection/);
  }

  private async verifyUrl() {
    await this.page.waitForURL('**/map-select');
  }
}
