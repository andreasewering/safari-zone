import { Locator, Page } from '@playwright/test';
import { MockRpc } from '../utils/mock-server';
import { getDexEntries } from '../fixtures/pokemon/pokemon';
import { EventWaiter } from '../utils/event';

export class DexAppraisalPage {
  constructor(
    private readonly page: Page,
    private readonly rpc: MockRpc,
    private readonly eventWaiter: EventWaiter
  ) {}

  async goto(): Promise<void> {
    await this.page.goto('/dex/appraisal');
  }

  async expectToBeOnPage(): Promise<void> {
    await this.page.waitForURL('/dex/appraisal');
  }

  async mockRpcs(filter?: Parameters<typeof getDexEntries>[0]): Promise<void> {
    await this.rpc.mockFullyCreatedUser();
    await this.rpc.mock(
      'kangaskhan.KangaskhanService/GetDexEntries',
      getDexEntries(filter)
    );
  }

  backButton(): Locator {
    return this.modal().getByRole('button', { name: 'Back' });
  }

  modal(): Locator {
    return this.page.getByRole('dialog');
  }

  headline(): Locator {
    return this.page.getByText('Professor Oaks Appraisal');
  }

  async waitForDialog(): Promise<void> {
    const typewriterEvent = this.eventWaiter.waitForTypewriter('appraisal');
    const done = await this.page.locator('type-writer').getAttribute('done');
    if (done === null) {
      await typewriterEvent;
    }
  }
}
