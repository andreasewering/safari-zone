import { Locator, Page } from '@playwright/test';
import { MockRpc } from '../utils/mock-server';
import { trainers } from '../fixtures/trainers/trainers';
import testTiles from '../fixtures/maps/tiles.json';

export class TrainerDetailPage {
  constructor(private readonly page: Page, private readonly rpc: MockRpc) {}

  async goto(trainerNumber: number): Promise<void> {
    await this.page.goto(`/trainer/${trainerNumber}`);
  }

  async mockRpcs(): Promise<void> {
    await this.rpc.mockFullyCreatedUser();
    await this.rpc.mock('kangaskhan.KangaskhanService/GetTrainers', trainers);
    await this.setupMapRelatedMocks();
  }

  async confirmTrainerChoice(): Promise<string> {
    const trainerNumber = await this.verifyUrl();
    await this.confirmButton().click();
    return trainerNumber;
  }

  confirmButton(): Locator {
    return this.page.getByText(/Confirm/);
  }

  private async verifyUrl(): Promise<string> {
    let retrievedNumber = '';
    const regex = /\/trainer\/(\d+)/;
    await this.page.waitForURL((url) => {
      const matches = regex.exec(url.pathname);
      if (!matches?.[1]) {
        return false;
      }
      retrievedNumber = matches[1];
      return true;
    });

    return retrievedNumber;
  }

  private async setupMapRelatedMocks(): Promise<void> {
    await this.rpc.mock('smeargle.SmeargleService/GetTilesetConfig', {
      tilesPerRow: 8,
      tileSizeInPx: 64,
      tilesetPath: '/static/tileset.png',
    });
    await this.rpc.mock('smeargle.SmeargleService/GetTiles', {
      tiles: testTiles.map((tile) => ({
        ...tile,
        animationIds: [],
      })),
    });
    await this.rpc.mock('smeargle.SmeargleService/GetTextureMetadata', {
      path: '/static/tilemap.png',
      layers: [10, 20],
      offsetX: -10,
      offsetY: -5,
    });
  }
}
