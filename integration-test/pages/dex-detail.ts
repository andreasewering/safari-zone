import { Locator, Page } from '@playwright/test';
import { getDexEntries, getDexEntryDetail } from '../fixtures/pokemon/pokemon';
import { MockRpc } from '../utils/mock-server';

export class DexDetailPage {
  constructor(private readonly page: Page, private readonly rpc: MockRpc) {}

  async goto(pokemonNumber: number) {
    await this.page.goto(`/dex/${pokemonNumber}`);
  }

  backButton(): Locator {
    return this.modal().getByRole('button', { name: 'Back' });
  }

  nextButton(): Locator {
    return this.page.getByAltText('To next dex entry');
  }

  previousButton(): Locator {
    return this.page.getByAltText('To previous dex entry');
  }

  setPartnerButton(): Locator {
    return this.modal().getByText('Choose as partner');
  }

  modal(): Locator {
    return this.page.getByRole('dialog');
  }

  async expectToBeOnPage(pokemonNumber: number) {
    await this.page.waitForURL(`/dex/${pokemonNumber}`);
  }

  async mockRpcs(filter?: Parameters<typeof getDexEntries>[0]): Promise<void> {
    await this.rpc.mockFullyCreatedUser();
    await this.rpc.mock(
      'kangaskhan.KangaskhanService/GetDexEntries',
      getDexEntries(filter)
    );
    await this.rpc.mock(
      'kangaskhan.KangaskhanService/GetDexEntryDetail',
      getDexEntryDetail
    );
  }
}
