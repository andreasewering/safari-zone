import { expect, Page } from '@playwright/test';
import { MockRpc } from '../utils/mock-server';
import { trainers } from '../fixtures/trainers/trainers';

export class TrainerOverviewPage {
  constructor(private readonly page: Page, private readonly rpc: MockRpc) {}

  async goto(): Promise<void> {
    await this.page.goto('/trainer');
  }

  async mockRpcs(): Promise<void> {
    await this.rpc.mockFullyCreatedUser();
    await this.rpc.mock('kangaskhan.KangaskhanService/GetTrainers', trainers);
    await this.rpc.mock('smeargle.SmeargleService/GetTilesetConfig');
  }

  async clickTrainer(name: string): Promise<void> {
    await this.verifyUrl();
    await this.page.getByRole('link', { name }).click();
  }

  async waitForSuccessTitle(): Promise<void> {
    await this.verifyUrl();
    await expect(this.page).toHaveTitle(/Choose trainer/);
  }

  private async verifyUrl() {
    await this.page.waitForURL('**/trainer');
  }
}
