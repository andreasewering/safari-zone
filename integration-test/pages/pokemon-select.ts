import { expect, Page } from '@playwright/test';

export class PokemonSelectPage {
  constructor(private readonly page: Page) {}

  async goto(): Promise<void> {
    await this.page.goto('/starter');
  }

  async choosePokemon(name: string): Promise<void> {
    await this.verifyUrl();
    await this.page.getByText(`I choose ${name}!`).click();
  }

  async waitForSuccessTitle(): Promise<void> {
    await this.verifyUrl();
    await expect(this.page).toHaveTitle(/Pokémon Selection/);
  }

  private async verifyUrl() {
    await this.page.waitForURL('**/starter');
  }
}
