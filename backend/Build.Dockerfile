FROM rust:1.85-slim-bookworm
RUN apt-get update && apt-get upgrade -y \
  && apt-get install -y libssl-dev pkg-config curl unzip postgresql-client
RUN cargo install sccache --locked
RUN rustup component add clippy

# Install Elm to verify Elm/Rust code compatability
RUN curl -L -o elm.gz https://github.com/elm/compiler/releases/download/0.19.1/binary-for-linux-64-bit.gz \
  && gunzip elm.gz \
  && chmod +x elm \
  && mv elm /usr/local/bin/
# Add protoc to generate rust code
RUN curl -L -o protoc.zip https://github.com/protocolbuffers/protobuf/releases/download/v21.9/protoc-21.9-linux-x86_64.zip \
  && unzip -o protoc.zip -d /usr/local bin/protoc \
  && unzip -o protoc.zip -d /usr/local include/*  
# Install cargo-udeps for finding unused dependencies
RUN cargo install cargo-udeps --locked
RUN cargo install sqlx-cli --locked --no-default-features --features native-tls,postgres
