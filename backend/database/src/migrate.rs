use sqlx::Connection as _;
use sqlx::Executor as _;
use std::collections::BTreeSet;
use std::path::PathBuf;
use std::{collections::BTreeMap, ffi::OsString, fs, num::ParseIntError, path::Path};

#[derive(Debug, Clone)]
pub struct Migration {
    pub timestamp: u64,
    pub description: String,
    pub up_sql: String,
    pub down_sql: String,
}

#[derive(sqlx::FromRow, Debug, Clone)]
struct ExecutedMigration {
    version: i64,
    description: String,
    up_sql: String,
    down_sql: String,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum MigrationDirection {
    Up,
    Down,
}

#[derive(Debug)]
pub enum Error {
    FileOrDirectoryNotFound {
        error: std::io::Error,
        path: PathBuf,
    },
    InvalidUnicode(OsString),
    TimestampFormat {
        error: ParseIntError,
        string: String,
    },
    NonRevertableMigration(u64),
    SqlExecution {
        error: sqlx::Error,
        context: SqlExecutionContext,
    },
    Inconsistency(Inconsistency),
}

#[derive(Debug)]
pub enum SqlExecutionContext {
    Migration { version: u64, sql: String },
    MigrationTableManagement { sql: String },
    Locking,
    TransactionManagement,
}

pub fn read_migrations(path: &Path) -> Result<Vec<Migration>, Error> {
    tracing::debug!(message = "Reading migrations", path = ?path);
    let migration_dir = fs::read_dir(path).map_err(|error| Error::FileOrDirectoryNotFound {
        error,
        path: path.to_owned(),
    })?;
    let mut migrations: BTreeMap<u64, Migration> = BTreeMap::new();
    for dir_entry in migration_dir {
        let dir_entry = dir_entry.map_err(|error| Error::FileOrDirectoryNotFound {
            error,
            path: path.to_owned(),
        })?;
        let file_name = dir_entry
            .file_name()
            .into_string()
            .map_err(Error::InvalidUnicode)?;
        let Some((direction, file_name)) = determine_migration_direction(&file_name) else {
            continue;
        };

        let Some((timestamp, name)) = file_name.split_once('_') else {
            continue;
        };
        let timestamp = str::parse::<u64>(timestamp).map_err(|error| Error::TimestampFormat {
            error,
            string: timestamp.to_owned(),
        })?;
        tracing::debug!(message = "Found migration file", name = %name, version = %timestamp, direction = ?direction);
        let sql = fs::read_to_string(dir_entry.path()).map_err(|error| {
            Error::FileOrDirectoryNotFound {
                error,
                path: dir_entry.path(),
            }
        })?;
        let sql = remove_sql_comments(sql);
        let name: String = name.into();

        migrations
            .entry(timestamp)
            .and_modify(|entry| entry.merge(sql.clone(), direction))
            .or_insert(Migration::one_way(timestamp, name, sql, direction));
    }

    let migrations: Vec<Migration> = migrations
        .into_values()
        .map(|migration| {
            if migration.validate_revertible() {
                Ok(migration)
            } else {
                Err(Error::NonRevertableMigration(migration.timestamp))
            }
        })
        .collect::<Result<_, _>>()?;

    Ok(migrations)
}

pub async fn execute_migrations(
    conn: &mut sqlx::PgConnection,
    migrations: Vec<Migration>,
) -> Result<(), Error> {
    lock(conn).await?;

    let migration_result = execute_migrations_without_locking(conn, migrations).await;

    unlock(conn).await?;

    migration_result?;
    Ok(())
}

async fn execute_migrations_without_locking(
    conn: &mut sqlx::PgConnection,
    migrations: Vec<Migration>,
) -> Result<(), Error> {
    ensure_migrations_table(&mut *conn).await?;
    let previously_executed_migrations = executed_migrations(&mut *conn).await?;

    let plan = compute_plan(migrations, previously_executed_migrations)?;
    execute_plan(&mut *conn, plan).await?;

    Ok(())
}

/// Given a ordered list of all migrations and an ordered list of previously executed migrations,
/// compute the steps necessary to have all migrations from the former list executed (and no more).
/// In most cases this means either:
/// - Do absolutely nothing since all migrations have already been applied on the last application startup (migrations == executed migrations)
/// - Apply a few migrations that have been added since the last application startup (migrations starts with executed migrations)
///
/// The interesting part comes in when a "go forward" path is not possible.
/// If an old migration has been changed or deleted, we want to fail - we need all migrations to still be there in case we want
/// to deploy to a different cluster with a fresh database.
///
/// If the latest executed migrations have been deleted, we are in a rollback scenario. This could happen if we deploy a new version to
/// our cluster which executed some migrations and we trigger a rollback to the previous version afterwards
/// In local development, this might also happen when you switch branches.
/// The production branch possibly has less migrations than the master branch for instance.
/// There is an easy way to deal with this: we execute the "down_sql" commands stored in the executed migrations in reverse order
/// until the executed migrations match the list of all migrations.
///
/// The most complex scenario that we want to tackle here is the one of two diverging branches.
/// Imagine the following executed migrations:
///     A -> B -> C -> D
/// And the following migration files:
///     A -> B -> C -> E
/// We can actually solve this: Revert D and apply E.
///
/// The issues is - how do we differentiate this from a deleted old migration:
/// Imagine the following executed migrations:
///     A -> B -> C -> D
/// And the following migration files:
///     A -> C -> D
/// We could revert D, C and B here and then apply C and D, but this is very likely not what we want.
/// In the worst case, this behaviour could accidentally delete and recreate a lot of tables in our database, losing a ton of data.
/// The difference between this and the previous example is, that we are applying migrations that we just reverted (here: C and D).
/// In this case we return an error to avoid the data loss.
fn compute_plan(
    migrations: Vec<Migration>,
    previously_executed_migrations: Vec<ExecutedMigration>,
) -> Result<MigrationPlan, Inconsistency> {
    let mut migrations_iter = migrations.into_iter();
    let mut executed_migrations_iter = previously_executed_migrations.into_iter();
    let mut migration_plan = MigrationPlan::default();

    loop {
        match (migrations_iter.next(), executed_migrations_iter.next()) {
            (None, None) => break,
            (Some(migration), None) => migration_plan.execute.push(migration),
            (None, Some(executed_migration)) => migration_plan.revert.push(executed_migration),
            (Some(migration), Some(executed_migration))
                if migration.timestamp == executed_migration.version as u64 =>
            {
                if migration.up_sql != executed_migration.up_sql {
                    // The SQL to execute for the forward migration has changed. That's illegal!
                    return Err(Inconsistency::UpSqlMutation {
                        version: migration.timestamp,
                        executed_sql: executed_migration.up_sql,
                        current_sql: migration.up_sql,
                    });
                }
                if migration.description != executed_migration.description
                    || migration.down_sql != executed_migration.down_sql
                {
                    // The metadata (name, down_sql) has changed. That's probably fine.
                    migration_plan.update_metadata.push(migration);
                } else {
                    migration_plan.keep.push(executed_migration);
                }
            }
            (Some(migration), Some(executed_migration)) => {
                // There is a diverging migration. Attempt to salvage by reverting the executed migration and then
                // executing the new migration.
                migration_plan.revert.push(executed_migration);
                migration_plan.execute.push(migration);
            }
        }
    }

    // Reverts have to be applied in reverse order
    migration_plan.revert.reverse();

    // We need to check that we are not undoing and redoing the same migrations
    let planned_reverts: BTreeSet<u64> = migration_plan
        .revert
        .iter()
        .map(|migration| migration.version as u64)
        .collect();
    let planned_executes: BTreeSet<u64> = migration_plan
        .execute
        .iter()
        .map(|migration| migration.timestamp)
        .collect();

    if planned_executes.is_disjoint(&planned_reverts) {
        return Ok(migration_plan);
    }
    Err(Inconsistency::OldMigrationChange {
        needed_reverts: planned_reverts,
        needed_executes: planned_executes,
    })
}

async fn execute_plan(
    conn: &mut sqlx::PgConnection,
    MigrationPlan {
        update_metadata,
        execute,
        revert,
        keep,
    }: MigrationPlan,
) -> Result<(), Error> {
    let mut transaction = conn.begin().await.map_err(add_transaction_context)?;
    for migration in update_metadata {
        tracing::info!(message = "Updating metadata for migration", description = %migration.description, version = %migration.timestamp);
        let query = "UPDATE _migrations SET description = $1, down_sql = $2 WHERE version = $3";
        sqlx::query(query)
            .bind(migration.description)
            .bind(migration.down_sql)
            .bind(migration.timestamp as i64)
            .execute(&mut *transaction)
            .await
            .map_err(add_migration_context(migration.timestamp, query))?;
    }
    transaction
        .commit()
        .await
        .map_err(add_transaction_context)?;

    for migration in keep {
        tracing::debug!(message = "Leaving migration untouched", description = %migration.description, version = %migration.version as u64);
    }

    for migration in revert {
        tracing::info!(message = "Reverting migration", description = %migration.description, version = %migration.version as u64);

        let mut transaction = conn.begin().await.map_err(add_transaction_context)?;
        transaction
            .execute(migration.down_sql.as_ref())
            .await
            .map_err(add_migration_context(
                migration.version as u64,
                &migration.down_sql,
            ))?;
        let query = "DELETE FROM _migrations WHERE version = $1";
        sqlx::query(query)
            .bind(migration.version)
            .execute(&mut *transaction)
            .await
            .map_err(add_migration_context(migration.version as u64, query))?;
        transaction
            .commit()
            .await
            .map_err(add_transaction_context)?;
    }
    for migration in execute {
        tracing::info!(message = "Executing migration", description = %migration.description, version = %migration.timestamp);
        let mut transaction = conn.begin().await.map_err(add_transaction_context)?;
        transaction
            .execute(migration.up_sql.as_ref())
            .await
            .map_err(add_migration_context(
                migration.timestamp,
                &migration.up_sql,
            ))?;
        let query = "INSERT INTO _migrations (version, description, up_sql, down_sql) VALUES ($1, $2, $3, $4)";
        sqlx::query(query)
            .bind(migration.timestamp as i64)
            .bind(&migration.description)
            .bind(migration.up_sql)
            .bind(migration.down_sql)
            .execute(&mut *transaction)
            .await
            .map_err(add_migration_context(migration.timestamp, query))?;

        transaction
            .commit()
            .await
            .map_err(add_transaction_context)?;

        tracing::info!(message = "Migration successful", description = %migration.description, version = %migration.timestamp);
    }

    Ok(())
}

#[derive(Default, Debug)]
struct MigrationPlan {
    update_metadata: Vec<Migration>,
    execute: Vec<Migration>,
    revert: Vec<ExecutedMigration>,
    keep: Vec<ExecutedMigration>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Inconsistency {
    UpSqlMutation {
        version: u64,
        executed_sql: String,
        current_sql: String,
    },
    OldMigrationChange {
        needed_reverts: BTreeSet<u64>,
        needed_executes: BTreeSet<u64>,
    },
}

async fn lock(conn: &mut sqlx::PgConnection) -> Result<(), Error> {
    let database_name = current_database(conn).await.map_err(add_locking_context)?;
    let lock_id = generate_lock_id(&database_name);

    tracing::info!(message = "Acquiring database lock", lock_id = %lock_id);

    // create an application lock over the database
    // this function will not return until the lock is acquired

    // https://www.postgresql.org/docs/current/explicit-locking.html#ADVISORY-LOCKS
    // https://www.postgresql.org/docs/current/functions-admin.html#FUNCTIONS-ADVISORY-LOCKS-TABLE

    // language=SQL
    let _ = sqlx::query("SELECT pg_advisory_lock($1)")
        .bind(lock_id)
        .execute(conn)
        .await
        .map_err(add_locking_context)?;

    tracing::info!(message = "Successfully acquired database lock", lock_id = %lock_id);

    Ok(())
}

async fn unlock(conn: &mut sqlx::PgConnection) -> Result<(), Error> {
    let database_name = current_database(conn).await.map_err(add_locking_context)?;
    let lock_id = generate_lock_id(&database_name);
    tracing::info!(message = "Releasing database lock", lock_id = %lock_id);

    // language=SQL
    let _ = sqlx::query("SELECT pg_advisory_unlock($1)")
        .bind(lock_id)
        .execute(conn)
        .await
        .map_err(add_locking_context)?;

    tracing::info!(message = "Successfully released database lock", lock_id = %lock_id);

    Ok(())
}

async fn executed_migrations(
    conn: &mut sqlx::PgConnection,
) -> Result<Vec<ExecutedMigration>, Error> {
    tracing::debug!("Fetching executed migrations");
    let query = "SELECT version, description, up_sql, down_sql FROM _migrations ORDER BY version";
    let rows = sqlx::query_as(query)
        .fetch_all(conn)
        .await
        .map_err(add_migration_table_management_context(query))?;
    Ok(rows)
}

pub async fn ensure_migrations_table<'a, E>(executor: E) -> Result<(), Error>
where
    E: sqlx::PgExecutor<'a>,
{
    let query = r#"
    CREATE TABLE IF NOT EXISTS _migrations (
    version BIGINT PRIMARY KEY,
    description TEXT NOT NULL,
    installed_on TIMESTAMPTZ NOT NULL DEFAULT now(),
    up_sql TEXT NOT NULL,
    down_sql TEXT NOT NULL
    );"#;
    let result = executor
        .execute(query)
        .await
        .map_err(add_migration_table_management_context(query))?;
    if result.rows_affected() > 0 {
        tracing::info!("Created fresh _migrations table");
    } else {
        tracing::debug!("Ensured _migrations table exists");
    }

    Ok(())
}

async fn current_database(conn: &mut sqlx::PgConnection) -> sqlx::Result<String> {
    sqlx::query_scalar("SELECT current_database()")
        .fetch_one(conn)
        .await
}

fn generate_lock_id(database_name: &str) -> i64 {
    let bytes = database_name.as_bytes();
    let (int_bytes, _) = bytes.split_at(std::mem::size_of::<i64>());
    i64::from_le_bytes(int_bytes.try_into().unwrap())
}

fn determine_migration_direction(file_name: &str) -> Option<(MigrationDirection, &str)> {
    if let Some(name) = file_name.strip_suffix(".down.sql") {
        return Some((MigrationDirection::Down, name));
    }
    if let Some(name) = file_name.strip_suffix(".up.sql") {
        return Some((MigrationDirection::Up, name));
    }
    None
}

impl Migration {
    fn one_way(
        timestamp: u64,
        description: String,
        sql: String,
        direction: MigrationDirection,
    ) -> Self {
        match direction {
            MigrationDirection::Up => Self {
                timestamp,
                description,
                up_sql: sql,
                down_sql: "".to_owned(),
            },
            MigrationDirection::Down => Self {
                timestamp,
                description,
                up_sql: "".to_owned(),
                down_sql: sql,
            },
        }
    }

    fn merge(&mut self, sql: String, direction: MigrationDirection) {
        match direction {
            MigrationDirection::Up => self.up_sql = sql,
            MigrationDirection::Down => self.down_sql = sql,
        }
    }

    fn validate_revertible(&self) -> bool {
        !self.down_sql.is_empty() && !self.up_sql.is_empty()
    }
}

fn remove_sql_comments(sql: String) -> String {
    let without_multiline = remove_multiline_comments(&sql);
    without_multiline
        .lines()
        .map(str::trim)
        .filter(|line| !line.is_empty())
        .collect::<Vec<&str>>()
        .join("\n")
}

#[derive(Clone, Copy)]
enum CommentState {
    NoComment,
    SingleLine,
    MultiLine,
}

/// Remove all multiline comments from the SQL content
///
/// # Arguments
/// * `sql_content` - A string slice that holds the content of the SQL file
fn remove_multiline_comments(sql_content: &str) -> String {
    // A multiline comment is a classical example of balanced parenthesis.
    // We can use this to our advantage to remove them from the SQL content,
    // where the parenthesis in question is \* and *\. These are two characters
    // and not one, so we need to keep track of the last two characters we've seen
    // to determine if we're in a comment or not.
    let mut output = String::new();

    let mut last_char = char::default();
    let mut state = CommentState::NoComment;

    for mut c in sql_content.chars() {
        match (state, last_char, c) {
            (CommentState::MultiLine, '*', '/') => {
                state = CommentState::NoComment;
                // avoid the last / being treated as the potential beginning of a new comment
                c = char::default();
            }
            (CommentState::MultiLine, _, _) => {
                // remove chars inside of comment
            }
            (CommentState::SingleLine, _, '\n') => {
                state = CommentState::NoComment;
                output.push('\n');
            }
            (CommentState::SingleLine, _, _) => {
                // remove chars inside of comment
            }
            (CommentState::NoComment, '/', '*') => {
                state = CommentState::MultiLine;
            }
            (CommentState::NoComment, '/', _) => {
                output.push('/');
                output.push(c);
            }
            (CommentState::NoComment, '-', '-') => {
                state = CommentState::SingleLine;
            }
            (CommentState::NoComment, '-', _) => {
                output.push('-');
                output.push(c);
            }
            (CommentState::NoComment, _, '/') => {
                // do not push '/' since this might be the start of a comment
            }
            (CommentState::NoComment, _, '-') => {
                // do not push '-' since this might be the start of a comment
            }
            (CommentState::NoComment, _, _) => {
                output.push(c);
            }
        }
        last_char = c;
    }

    output
}

impl From<Inconsistency> for Error {
    fn from(value: Inconsistency) -> Self {
        Self::Inconsistency(value)
    }
}

fn add_transaction_context(error: sqlx::Error) -> Error {
    Error::SqlExecution {
        error,
        context: SqlExecutionContext::TransactionManagement,
    }
}

fn add_locking_context(error: sqlx::Error) -> Error {
    Error::SqlExecution {
        error,
        context: SqlExecutionContext::Locking,
    }
}

fn add_migration_context(version: u64, sql: &str) -> impl FnOnce(sqlx::Error) -> Error {
    let sql = sql.to_owned();
    move |error: sqlx::Error| Error::SqlExecution {
        error,
        context: SqlExecutionContext::Migration { version, sql },
    }
}

fn add_migration_table_management_context(sql: &str) -> impl FnOnce(sqlx::Error) -> Error {
    let sql = sql.to_owned();
    move |error: sqlx::Error| Error::SqlExecution {
        error,
        context: SqlExecutionContext::MigrationTableManagement { sql },
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::FileOrDirectoryNotFound { error, path } => {
                f.write_fmt(format_args!("File IO Error {error}: {path:?}"))
            }
            Error::InvalidUnicode(os_string) => {
                f.write_fmt(format_args!("Invalid unicode: {os_string:?}"))
            }
            Error::TimestampFormat { error, string } => f.write_fmt(format_args!(
                "Expected timestamp in u64 format but got {string}, {error}"
            )),
            Error::NonRevertableMigration(version) => f.write_fmt(format_args!(
                "Migration with version {} does not have an associated .up or .down file",
                version
            )),
            Error::SqlExecution { error, context } => f.write_fmt(format_args!(
                "SQL Execution Error: {error}. Context: {context}"
            )),
            Error::Inconsistency(inconsistency) => {
                f.write_fmt(format_args!("Migration inconsistency: {inconsistency}"))
            }
        }
    }
}

impl std::fmt::Display for SqlExecutionContext {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SqlExecutionContext::Migration { version, sql } => f.write_fmt(format_args!(
                "Error happened during migration of version: {version} via SQL {sql}."
            )),
            SqlExecutionContext::MigrationTableManagement { sql } => f.write_fmt(format_args!(
                "Error happened during migration table management via SQL {sql}."
            )),
            SqlExecutionContext::Locking => {
                f.write_str("Error happened around locking the database.")
            }
            SqlExecutionContext::TransactionManagement => {
                f.write_str("Error happened when managing a transaction.")
            }
        }
    }
}

impl std::fmt::Display for Inconsistency {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Inconsistency::UpSqlMutation {
                version,
                executed_sql,
                current_sql,
            } => f.write_fmt(format_args!(
                "Migration with version {} has changed. Previous migration SQL script: 

{}

Current migration SQL script: 

{}",
                version, executed_sql, current_sql
            )),
            Inconsistency::OldMigrationChange {
                needed_reverts,
                needed_executes,
            } => f.write_fmt(format_args!(
                "An already executed migration seems to have been removed or re-ordered.
This is the minimal migration path possible:
    Revert: {needed_reverts:?}                
    Execute: {needed_executes:?}
Since this re-applies reverted migrations, this is considered incorrect."
            )),
        }
    }
}

impl std::error::Error for Error {}
