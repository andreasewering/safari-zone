use std::collections::BTreeSet;

pub async fn create_snapshot(pool: &sqlx::PgPool) -> anyhow::Result<DatabaseSnapshot> {
    let tables = sqlx::query_as(
        r#"SELECT table_schema, table_name
    FROM information_schema.tables
    WHERE table_schema NOT IN ('information_schema', 'pg_catalog')
    ORDER BY table_name
    "#,
    )
    .fetch_all(pool)
    .await?;

    let columns = sqlx::query_as(
        r#"SELECT
        table_schema,
        table_name,
        column_name,
        data_type,
        character_maximum_length,
        is_nullable,
        column_default
    FROM information_schema.columns
    WHERE table_schema NOT IN ('information_schema', 'pg_catalog')
    ORDER BY table_name, ordinal_position
    "#,
    )
    .fetch_all(pool)
    .await?;

    let constraints = sqlx::query_as(
        r#"
        SELECT
            tc.table_schema,
            tc.table_name,
            tc.constraint_name,
            tc.constraint_type,
            kcu.column_name,
            ccu.table_schema AS foreign_table_schema,
            ccu.table_name AS foreign_table_name,
            ccu.column_name AS foreign_column_name
        FROM information_schema.table_constraints AS tc
        JOIN information_schema.key_column_usage AS kcu
          ON tc.constraint_name = kcu.constraint_name
          AND tc.table_schema = kcu.table_schema
        LEFT JOIN information_schema.constraint_column_usage AS ccu
          ON ccu.constraint_name = tc.constraint_name
          AND ccu.table_schema = tc.table_schema
        WHERE tc.table_schema NOT IN ('information_schema', 'pg_catalog')
        ORDER BY tc.table_name
        "#,
    )
    .fetch_all(pool)
    .await?;

    let indices = sqlx::query_as(
        r#"
    SELECT
        t.relname as table_name,
        i.relname as index_name,
        a.attname as column_name
    FROM
        pg_class t,
        pg_class i,
        pg_index ix,
        pg_attribute a
    WHERE
        t.oid = ix.indrelid
        AND i.oid = ix.indexrelid
        AND a.attrelid = t.oid
        AND a.attnum = ANY(ix.indkey)
        AND t.relkind = 'r'
        AND t.relname NOT IN ('pg_catalog', 'information_schema')
        AND t.relname NOT LIKE 'pg_%'
    ORDER BY
        t.relname,
        i.relname;
    "#,
    )
    .fetch_all(pool)
    .await?;

    let custom_types = sqlx::query_as(
        r#"SELECT
    n.nspname AS schema,
    t.typname AS type_name,
    pg_catalog.format_type(t.oid, NULL) AS display_name,
    CASE
        WHEN t.typtype = 'b' THEN 'base type'
        WHEN t.typtype = 'c' THEN 'composite type'
        WHEN t.typtype = 'd' THEN 'domain'
        WHEN t.typtype = 'e' THEN 'enum type'
        WHEN t.typtype = 'p' THEN 'pseudo-type'
        ELSE 'other'
    END AS type,
    pg_catalog.obj_description(t.oid, 'pg_type') as description
FROM pg_catalog.pg_type t
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
WHERE (t.typrelid = 0 OR (SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid))
      AND NOT EXISTS(SELECT 1 FROM pg_catalog.pg_type el WHERE el.oid = t.typelem AND el.typarray = t.oid)
      AND n.nspname NOT IN ('pg_catalog', 'information_schema')
ORDER BY t.typname;"#,
    ).fetch_all(pool).await?;

    let sequences = sqlx::query_as(
        r#"
    SELECT
        sequence_schema,
        sequence_name,
        start_value,
        increment
    FROM
        information_schema.sequences
    WHERE
        sequence_schema NOT IN ('pg_catalog', 'information_schema');
    "#,
    )
    .fetch_all(pool)
    .await?;

    let extensions = sqlx::query_as(
        r#"SELECT extname, extversion
        FROM pg_extension
        ORDER BY extname;"#,
    )
    .fetch_all(pool)
    .await?;

    let functions = sqlx::query_as(
        r#"SELECT
    n.nspname AS "schema",
    p.proname AS "function_name",
    pg_catalog.pg_get_function_result(p.oid) AS "return_type",
    pg_catalog.pg_get_function_arguments(p.oid) AS "args",
    CASE
        WHEN p.prokind = 'a' THEN 'Aggregate'
        WHEN p.prokind = 'w' THEN 'Window'
        WHEN p.prokind = 'p' THEN 'Procedure'
        WHEN p.prorettype = 'pg_catalog.trigger'::pg_catalog.regtype THEN 'Trigger'
        ELSE 'Normal'
    END AS "kind",
    pg_catalog.obj_description(p.oid, 'pg_proc') AS "description"
FROM
    pg_catalog.pg_proc p
    INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
WHERE
    n.nspname NOT IN ('pg_catalog', 'information_schema')
    AND pg_catalog.pg_function_is_visible(p.oid)
ORDER BY
    "schema", "function_name";
"#,
    )
    .fetch_all(pool)
    .await?;

    Ok(DatabaseSnapshot {
        tables,
        columns,
        constraints,
        indices,
        sequences,
        custom_types,
        extensions,
        functions,
    })
}

pub fn diff_snapshot(a: DatabaseSnapshot, b: DatabaseSnapshot) -> DatabaseSnapshotDiff {
    DatabaseSnapshotDiff {
        tables: calc_diff(a.tables, b.tables),
        columns: calc_diff(a.columns, b.columns),
        constraints: calc_diff(a.constraints, b.constraints),
        indices: calc_diff(a.indices, b.indices),
        sequences: calc_diff(a.sequences, b.sequences),
        custom_types: calc_diff(a.custom_types, b.custom_types),
        extensions: calc_diff(a.extensions, b.extensions),
        functions: calc_diff(a.functions, b.functions),
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DatabaseSnapshot {
    tables: Vec<Table>,
    columns: Vec<Column>,
    constraints: Vec<Constraint>,
    indices: Vec<Index>,
    sequences: Vec<Sequence>,
    custom_types: Vec<CustomType>,
    extensions: Vec<Extension>,
    functions: Vec<PgFunction>,
}

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct DatabaseSnapshotDiff {
    tables: Vec<Diff<Table>>,
    columns: Vec<Diff<Column>>,
    constraints: Vec<Diff<Constraint>>,
    indices: Vec<Diff<Index>>,
    sequences: Vec<Diff<Sequence>>,
    custom_types: Vec<Diff<CustomType>>,
    extensions: Vec<Diff<Extension>>,
    functions: Vec<Diff<PgFunction>>,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Diff<T> {
    New(T),
    Missing(T),
}
fn calc_diff<T, I, J>(a: I, b: J) -> Vec<Diff<T>>
where
    T: Ord + Clone,
    I: IntoIterator<Item = T>,
    J: IntoIterator<Item = T>,
{
    let a_set: BTreeSet<T> = a.into_iter().collect();
    let b_set: BTreeSet<T> = b.into_iter().collect();
    b_set
        .difference(&a_set)
        .cloned()
        .map(Diff::New)
        .chain(a_set.difference(&b_set).cloned().map(Diff::Missing))
        .collect()
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct Table {
    table_schema: Option<String>,
    table_name: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct Column {
    table_schema: Option<String>,
    table_name: Option<String>,
    column_name: Option<String>,
    data_type: Option<String>,
    character_maximum_length: Option<i32>,
    is_nullable: Option<String>,
    column_default: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct Constraint {
    table_schema: Option<String>,
    table_name: Option<String>,
    constraint_name: Option<String>,
    constraint_type: Option<String>,
    column_name: Option<String>,
    foreign_column_name: Option<String>,
    foreign_table_schema: Option<String>,
    foreign_table_name: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct Index {
    table_name: String,
    index_name: String,
    column_name: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct Sequence {
    sequence_schema: Option<String>,
    sequence_name: Option<String>,
    start_value: Option<String>,
    increment: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct PgFunction {
    schema: Option<String>,
    function_name: Option<String>,
    return_type: Option<String>,
    args: Option<String>,
    kind: Option<String>,
    description: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct Extension {
    extname: Option<String>,
    extversion: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, sqlx::FromRow)]
struct CustomType {
    schema: Option<String>,
    type_name: Option<String>,
    display_name: Option<String>,
    r#type: Option<String>,
    description: Option<String>,
}
