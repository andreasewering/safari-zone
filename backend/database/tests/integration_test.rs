use std::collections::BTreeSet;
use std::path::Path;

use database::Error;
use database::Inconsistency;
use database::SqlExecutionContext;
use sqlx::Executor as _;
use sqlx::Postgres;

async fn migrate(path: &str, conn: &mut sqlx::PgConnection) -> Result<(), database::Error> {
    let migrations = database::read_migrations(Path::new(path))?;
    database::execute_migrations(conn, migrations).await
}

#[sqlx::test]
async fn forward_migration(mut conn: sqlx::pool::PoolConnection<Postgres>) -> anyhow::Result<()> {
    migrate("tests/fixtures/forward_migration_1", &mut conn).await?;

    conn.execute("SELECT * FROM table_1").await?;
    conn.execute("SELECT * FROM table_2").await?;
    assert!(conn.execute("SELECT * FROM table_3").await.is_err());

    migrate("tests/fixtures/forward_migration_2", &mut conn).await?;
    conn.execute("SELECT * FROM table_3").await?;
    conn.execute("SELECT * FROM table_4").await?;

    Ok(())
}

#[sqlx::test]
async fn backward_migration(mut conn: sqlx::pool::PoolConnection<Postgres>) -> anyhow::Result<()> {
    migrate("tests/fixtures/forward_migration_2", &mut conn).await?;

    conn.execute("SELECT * FROM table_1").await?;
    conn.execute("SELECT * FROM table_2").await?;
    conn.execute("SELECT * FROM table_3").await?;
    conn.execute("SELECT * FROM table_4").await?;

    migrate("tests/fixtures/forward_migration_1", &mut conn).await?;

    conn.execute("SELECT * FROM table_1").await?;
    conn.execute("SELECT * FROM table_2").await?;
    assert!(conn.execute("SELECT * FROM table_3").await.is_err());
    assert!(conn.execute("SELECT * FROM table_4").await.is_err());
    Ok(())
}

#[sqlx::test]
async fn diverging_migrations_happy_case(
    mut conn: sqlx::pool::PoolConnection<Postgres>,
) -> anyhow::Result<()> {
    migrate("tests/fixtures/diverging_migrations_happy_1", &mut conn).await?;

    conn.execute("SELECT * FROM table_1").await?;
    conn.execute("SELECT * FROM table_2").await?;
    conn.execute("SELECT * FROM table_3").await?;
    conn.execute("SELECT * FROM table_4").await?;
    assert!(conn.execute("SELECT * FROM table_5").await.is_err());

    migrate("tests/fixtures/diverging_migrations_happy_2", &mut conn).await?;

    conn.execute("SELECT * FROM table_1").await?;
    conn.execute("SELECT * FROM table_2").await?;
    conn.execute("SELECT * FROM table_5").await?;
    assert!(conn.execute("SELECT * FROM table_3").await.is_err());
    assert!(conn.execute("SELECT * FROM table_4").await.is_err());

    Ok(())
}

#[sqlx::test]
async fn diverging_migrations_happy_case_backwards(
    mut conn: sqlx::pool::PoolConnection<Postgres>,
) -> anyhow::Result<()> {
    migrate("tests/fixtures/diverging_migrations_happy_2", &mut conn).await?;

    conn.execute("SELECT * FROM table_1").await?;
    conn.execute("SELECT * FROM table_2").await?;
    conn.execute("SELECT * FROM table_5").await?;
    assert!(conn.execute("SELECT * FROM table_3").await.is_err());
    assert!(conn.execute("SELECT * FROM table_4").await.is_err());

    migrate("tests/fixtures/diverging_migrations_happy_1", &mut conn).await?;

    conn.execute("SELECT * FROM table_1").await?;
    conn.execute("SELECT * FROM table_2").await?;
    conn.execute("SELECT * FROM table_3").await?;
    conn.execute("SELECT * FROM table_4").await?;
    assert!(conn.execute("SELECT * FROM table_5").await.is_err());

    Ok(())
}

#[sqlx::test]
async fn diverging_migrations_deleted_old(
    mut conn: sqlx::pool::PoolConnection<Postgres>,
) -> anyhow::Result<()> {
    migrate(
        "tests/fixtures/diverging_migrations_deleted_old_1",
        &mut conn,
    )
    .await?;
    let result = migrate(
        "tests/fixtures/diverging_migrations_deleted_old_2",
        &mut conn,
    )
    .await;
    let inconsistency = match result {
        Err(Error::Inconsistency(inconsistency)) => inconsistency,
        _ => panic!("Expected inconsistency error but got: {result:?}"),
    };
    let expected_inconsistency = Inconsistency::OldMigrationChange {
        needed_reverts: BTreeSet::from([20240703063524, 20240703063506]),
        needed_executes: BTreeSet::from([20240703063524]),
    };
    assert_eq!(inconsistency, expected_inconsistency);
    Ok(())
}

#[sqlx::test]
async fn changed_comments_and_newlines_do_not_matter(
    mut conn: sqlx::pool::PoolConnection<Postgres>,
) -> anyhow::Result<()> {
    migrate("tests/fixtures/comments_1", &mut conn).await?;
    conn.execute("SELECT * FROM table_1").await?;

    migrate("tests/fixtures/comments_2", &mut conn).await?;
    conn.execute("SELECT * FROM table_1").await?;

    Ok(())
}

#[sqlx::test]
async fn late_sql_error_does_not_rollback_previous_changes(
    mut conn: sqlx::pool::PoolConnection<Postgres>,
) -> anyhow::Result<()> {
    let result = migrate("tests/fixtures/late_sql_error", &mut conn).await;

    let context = match result {
        Err(Error::SqlExecution { context, .. }) => context,
        _ => panic!("Expected SqlExecution Error but received {result:?}"),
    };
    match context {
        SqlExecutionContext::Migration { version, .. } => {
            assert_eq!(version, 20240703063524);
        }
        _ => panic!("Expected Migration Context but received {context}"),
    }
    // Creation of table_1 was not rolled back
    assert!(conn.execute("SELECT * FROM table_1").await.is_ok());
    Ok(())
}

#[sqlx::test]
async fn migrations_run_in_seperate_transactions(
    mut conn: sqlx::pool::PoolConnection<Postgres>,
) -> anyhow::Result<()> {
    migrate("tests/fixtures/seperate_transactions", &mut conn).await?;
    conn.execute("SELECT * FROM table_1").await?;
    Ok(())
}
