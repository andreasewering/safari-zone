CREATE TABLE table_1 (
    id BIGINT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL
);

CREATE TABLE table_2 (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    foo INT NOT NULL
);
