ALTER TABLE table_1
    ADD COLUMN new_column INT NOT NULL;

ALTER TABLE table_2
    -- Intentional error. It should be DROP instead of DELETE
    DELETE COLUMN foo; 
   