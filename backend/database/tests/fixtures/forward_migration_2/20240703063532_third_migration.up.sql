ALTER TABLE table_1
    ADD COLUMN new_column_2 INT NOT NULL;

CREATE TABLE table_3 (
    id BIGINT PRIMARY KEY,
    FOREIGN KEY (id) REFERENCES table_1(id) ON DELETE CASCADE
);   
