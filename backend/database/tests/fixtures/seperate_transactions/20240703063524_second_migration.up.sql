CREATE TABLE table_1 (
    id SERIAL PRIMARY KEY,
    -- This will not work if it runs in the same transaction as the sql that created the type DIRECTION 
    direction DIRECTION NOT NULL
);
   