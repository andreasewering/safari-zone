
CREATE TABLE table_1 (
    id BIGINT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL -- arbitrary line comment not at the start of the line
);

-- arbitrary line comment at the start of the line
CREATE TABLE table_2 (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    foo INT NOT NULL /* Single line comment using multiline syntax */
); /* Actual multiline comment
Blabla
*/

