pub fn merge(base: &mut figment::value::Dict, extension: figment::value::Dict) {
    for (key, value) in extension {
        let base_value = base.get_mut(&key);
        match (base_value, value) {
            (None, value) => {
                base.insert(key, value);
            }
            (
                Some(figment::value::Value::Bool(base_tag, base_bool)),
                figment::value::Value::Bool(tag, bool),
            ) => {
                *base_bool = bool;
                *base_tag = tag;
            }
            (
                Some(figment::value::Value::Num(base_tag, base_num)),
                figment::value::Value::Num(tag, num),
            ) => {
                *base_num = num;
                *base_tag = tag;
            }
            (
                Some(figment::value::Value::Char(base_tag, base_char)),
                figment::value::Value::Char(tag, char),
            ) => {
                *base_char = char;
                *base_tag = tag;
            }
            (
                Some(figment::value::Value::String(base_tag, base_str)),
                figment::value::Value::String(tag, str),
            ) => {
                *base_str = str;
                *base_tag = tag;
            }
            (
                Some(figment::value::Value::Array(base_tag, base_arr)),
                figment::value::Value::Array(tag, arr),
            ) => {
                *base_arr = arr;
                *base_tag = tag;
            }
            (
                Some(figment::value::Value::Dict(_base_tag, base_map)),
                figment::value::Value::Dict(_tag, map),
            ) => merge(base_map, map),
            (x, y) => {
                panic!("Inconsistent structure. Base Value: {x:?} Value to merge: {y:?}")
            }
        }
    }
}
