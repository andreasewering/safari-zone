mod implicit_profile_provider;
mod merge;

use std::{env, fmt::Display, path::PathBuf};

use figment::{providers, Figment, Profile};
use figment_directory::FormatExt as _;
use implicit_profile_provider::ImplicitProfileProvider;
use serde::Deserialize;

pub type Error = figment::Error;

pub fn read_config<'a, C>() -> Result<C, figment::Error>
where
    C: Deserialize<'a>,
{
    let config_dir =
        env::var("SZ_CONFIG").map_err(|_err| "Environment Variable 'SZ_CONFIG' not found.")?;
    read_config_from_dir(config_dir)
}

pub fn read_config_from_dir<'a, C, P>(config_dir: P) -> Result<C, figment::Error> where C: Deserialize<'a>, P: Into<PathBuf> + Display {
    let profile = Profile::from_env_or("SZ_ENV", Profile::Default);
    tracing::info!(message = "Reading and parsing configuration directory", %config_dir, %profile);
    Figment::new()
        .merge(ImplicitProfileProvider::from(providers::Toml::directory(
            config_dir,
        )))
        .merge(providers::Env::prefixed("SZ_").split("__"))
        .select(profile)
        .extract()
}
