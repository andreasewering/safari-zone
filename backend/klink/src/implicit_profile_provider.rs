use figment::{
    value::{Dict, Map, Value},
    Profile, Provider,
};

use crate::merge;

pub struct ImplicitProfileProvider<P> {
    provider: P,
}

impl<P> From<P> for ImplicitProfileProvider<P> {
    fn from(provider: P) -> ImplicitProfileProvider<P> {
        ImplicitProfileProvider { provider }
    }
}

impl<P> Provider for ImplicitProfileProvider<P>
where
    P: Provider,
{
    fn metadata(&self) -> figment::Metadata {
        self.provider.metadata()
    }

    fn data(
        &self,
    ) -> Result<figment::value::Map<figment::Profile, figment::value::Dict>, figment::Error> {
        let mut data = self.provider.data()?;
        traverse_default(&mut data);
        Ok(data)
    }
}

pub fn traverse_default(map: &mut Map<Profile, Dict>) {
    let Some(default) = map.remove(&Profile::Default) else {
        return;
    };
    let default = help_traverse(map, default, Vec::new());
    map.insert(Profile::Default, default);
}

fn help_traverse(map: &mut Map<Profile, Dict>, dict: Dict, path: Vec<String>) -> Dict {
    dict.into_iter()
        .filter_map(|(key, value)| {
            let Some(profile) = key
                .strip_prefix('_')
                .and_then(|k| k.strip_suffix('_'))
                .map(Profile::new)
            else {
                let value = match value {
                    Value::Dict(tag, nested_dict) => {
                        let mut path = path.clone();
                        path.push(key.clone());
                        Value::Dict(tag, help_traverse(map, nested_dict, path))
                    }
                    other => other,
                };
                return Some((key, value));
            };
            let Value::Dict(_, dict) = value else {
                return Some((key, value));
            };
            let profile_dict = map.entry(profile).or_default();
            insert_at_path(profile_dict, path.clone(), dict);
            None
        })
        .collect()
}

fn insert_at_path(mut dict: &mut Dict, path: Vec<String>, values: Dict) {
    for key in path {
        let value_at_key = dict.entry(key).or_insert(Dict::new().into());
        let Value::Dict(_, dict_at_key) = value_at_key else {
            return;
        };
        dict = dict_at_key;
    }
    merge::merge(dict, values);
}

#[cfg(test)]
mod tests {
    use super::*;
    use figment::{
        providers::{self, Format},
        Figment,
    };
    use serde::Deserialize;

    fn parse_and_extract<'a, T: Deserialize<'a>>(toml: &str, profile: &str) -> figment::Result<T> {
        Figment::new()
            .merge(ImplicitProfileProvider::from(providers::Toml::string(toml)))
            .select(profile)
            .extract()
    }

    #[test]
    fn works_for_simple_example() -> figment::Result<()> {
        let toml = r#"
            int = 5

            [_dev_]
            int = 6
        "#;
        let cfg: Simple = parse_and_extract(toml, "dev")?;
        assert_eq!(cfg.int, 6);
        Ok(())
    }

    #[test]
    fn works_for_nested_example() -> figment::Result<()> {
        let toml = r#"
            [nested]
            int = 5

            [nested._dev_]
            int = 6
        "#;
        let cfg: Nested = parse_and_extract(toml, "dev")?;
        assert_eq!(cfg.nested.int, 6);

        Ok(())
    }

    #[derive(Debug, Deserialize)]
    struct Simple {
        int: i64,
    }

    #[derive(Debug, Deserialize)]
    struct Nested {
        nested: Simple,
    }
}
