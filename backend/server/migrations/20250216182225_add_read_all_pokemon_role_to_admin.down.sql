UPDATE roles
SET permissions = permissions [1:array_length(permissions, 1) - 1]
WHERE role_name = 'SAFARI_ADMIN';
