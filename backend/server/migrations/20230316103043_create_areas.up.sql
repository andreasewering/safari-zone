CREATE EXTENSION btree_gist;

CREATE TYPE LOCALE AS ENUM ('DE', 'EN');

CREATE TABLE area (
    id BIGSERIAL PRIMARY KEY,
    map_id BIGINT NOT NULL,
    background_music_track INT NULL,
    FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE CASCADE
);

SELECT create_encryption_function_for_table('area');
ALTER TABLE area ALTER COLUMN id SET DEFAULT encrypt_area(nextval('area_id_seq'));

CREATE TABLE area_part (
    id BIGSERIAL PRIMARY KEY,
    map_id BIGINT NOT NULL,
    area_id BIGINT NOT NULL,
    range_x int4range NOT NULL,
    range_y int4range NOT NULL,
    range_z int4range NOT NULL,
    EXCLUDE USING GIST (map_id WITH =, range_x WITH &&, range_y WITH &&, range_z WITH &&),
    FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE CASCADE,
    FOREIGN KEY (area_id) REFERENCES area(id) ON DELETE CASCADE
);

CREATE TABLE area_translations (
    area_id BIGINT NOT NULL,
    map_id BIGINT NOT NULL, -- No foreign key constraint because this would make it impossible to add unique constraint
    locale LOCALE NOT NULL,
    -- Translated fields
    area_name TEXT NOT NULL,
    PRIMARY KEY (area_id, locale),
    UNIQUE (map_id, locale, area_name),
    FOREIGN KEY (area_id) REFERENCES area(id) ON DELETE CASCADE
);
