DROP TABLE area_translations;
DROP TABLE area_part;
DROP TABLE area;
DROP FUNCTION encrypt_area;
DROP TYPE LOCALE;
DROP EXTENSION btree_gist;
