CREATE TABLE chat_message (
    id BIGSERIAL PRIMARY KEY,
    from_user BIGINT NOT NULL,
    text TEXT NOT NULL,
    sent_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE,
    FOREIGN KEY (from_user) REFERENCES users(id) ON DELETE
    SET NULL
);
-- We insert one receiver for each member of a group
-- The group id here is just for reference so the message gets displayed in the right channel
-- and also for newly joined members - these can opt-in to receiving old messages
CREATE TABLE chat_message_receiver (
    message_id BIGINT NOT NULL,
    to_user BIGINT NOT NULL,
    to_group BIGINT,
    PRIMARY KEY (message_id, to_user),
    FOREIGN KEY (message_id) REFERENCES chat_message(id) ON DELETE CASCADE,
    FOREIGN KEY (to_user) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (to_group) REFERENCES groups(id) ON DELETE CASCADE
);
CREATE TABLE tagged_user (
    id BIGSERIAL PRIMARY KEY,
    message_id BIGINT NOT NULL,
    referenced_user_id BIGINT,
    span int4range NOT NULL CHECK (lower(span) >= 0),
    EXCLUDE USING GIST (
        message_id WITH =,
        referenced_user_id WITH =,
        span WITH &&
    ),
    FOREIGN KEY (message_id) REFERENCES chat_message(id) ON DELETE CASCADE,
    FOREIGN KEY (referenced_user_id) REFERENCES users(id) ON DELETE SET NULL
);
CREATE TABLE tagged_group (
    id BIGSERIAL PRIMARY KEY,
    message_id BIGINT NOT NULL,
    referenced_group_id BIGINT,
    span int4range NOT NULL CHECK (lower(span) >= 0),
    EXCLUDE USING GIST (
        message_id WITH =,
        referenced_group_id WITH =,
        span WITH &&
    ),
    FOREIGN KEY (message_id) REFERENCES chat_message(id) ON DELETE CASCADE,
    FOREIGN KEY (referenced_group_id) REFERENCES groups(id) ON DELETE SET NULL
);