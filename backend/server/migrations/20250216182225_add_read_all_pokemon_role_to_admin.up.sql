UPDATE roles
SET permissions = permissions || ARRAY ['READ_ALL_POKEMON']
WHERE role_name = 'SAFARI_ADMIN';
