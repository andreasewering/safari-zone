CREATE TYPE SAFARI_ITEM AS ENUM ('pokeball', 'superball', 'hyperball', 'diveball', 'quickball');

CREATE TABLE item (
    id BIGSERIAL PRIMARY KEY,
    item SAFARI_ITEM NOT NULL,
    x REAL NOT NULL,
    y REAL NOT NULL,
    map_id BIGINT NOT NULL,
    layer_number INT NOT NULL,
    despawn_time TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE CASCADE
);

SELECT create_encryption_function_for_table('item');
ALTER TABLE item ALTER COLUMN id SET DEFAULT encrypt_item(nextval('item_id_seq'));

CREATE TABLE area_items (
  id BIGSERIAL PRIMARY KEY,
  area_id BIGINT NOT NULL,
  item SAFARI_ITEM NOT NULL,
  probability REAL NOT NULL,
  FOREIGN KEY (area_id) REFERENCES area(id) ON DELETE CASCADE,
  UNIQUE (area_id, item)
);
