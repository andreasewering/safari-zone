DROP TABLE tile;
DROP TABLE tilemap;
DROP TABLE map;
DROP FUNCTION encrypt_map;
DROP FUNCTION create_encryption_function_for_table(TEXT, BIGINT);
DROP EXTENSION permuteseq;
