INSERT INTO area_translations (area_id, map_id, locale, area_name)
    SELECT area_id, map_id, 'FR', area_name
    FROM area_translations 
    WHERE locale = 'EN';
