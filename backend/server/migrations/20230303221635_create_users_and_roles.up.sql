-- For gen_random_bytes function
CREATE EXTENSION pgcrypto;
CREATE FUNCTION generate_random_i64() RETURNS bigint AS $$
SELECT (
    (get_byte(r, 0)::bigint << 56) + (get_byte(r, 1)::bigint << 48) + (get_byte(r, 2)::bigint << 40) + (get_byte(r, 3)::bigint << 32) + (get_byte(r, 4)::bigint << 24) + (get_byte(r, 5)::bigint << 16) + (get_byte(r, 6)::bigint << 8) + get_byte(r, 7)::bigint
  ) as random_bigint
FROM (
    SELECT gen_random_bytes(8) as r
  ) as bytes;
$$ LANGUAGE SQL;
CREATE TABLE roles (
  id BIGSERIAL PRIMARY KEY,
  role_name TEXT NOT NULL UNIQUE,
  permissions TEXT [] NOT NULL
);
CREATE TYPE DIRECTION AS ENUM ('L', 'R', 'U', 'D');
CREATE TYPE AUTH_PROVIDER AS ENUM ('safarizone', 'andrena', 'github');
CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  username TEXT NOT NULL,
  auth_provider AUTH_PROVIDER NOT NULL,
  display_name TEXT NOT NULL,
  role_id BIGINT,
  UNIQUE (username, auth_provider),
  FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE
  SET NULL
);
SELECT create_encryption_function_for_table('users');
ALTER TABLE users
ALTER COLUMN id
SET DEFAULT encrypt_users(nextval('users_id_seq'));
CREATE TABLE user_login (
  user_id BIGINT PRIMARY KEY,
  password TEXT NOT NULL,
  email TEXT NOT NULL UNIQUE,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE TABLE user_verification (
  verification_id BIGINT NOT NULL DEFAULT generate_random_i64(),
  user_id BIGINT NOT NULL,
  verification_id_valid_until TIMESTAMP WITH TIME ZONE NOT NULL,
  UNIQUE (user_id, verification_id),
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE TABLE user_password_reset (
  verification_id BIGINT NOT NULL DEFAULT generate_random_i64(),
  user_id BIGINT NOT NULL,
  verification_id_valid_until TIMESTAMP WITH TIME ZONE,
  UNIQUE (user_id, verification_id),
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE TABLE user_position (
  user_id BIGINT PRIMARY KEY,
  map_id BIGINT NOT NULL,
  x INT NOT NULL,
  y INT NOT NULL,
  layer_number INT NOT NULL,
  direction DIRECTION NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE RESTRICT
);
CREATE TABLE user_partner_pokemon (
  user_id BIGINT PRIMARY KEY,
  partner_pokemon_number INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE TABLE user_trainer (
  user_id BIGINT PRIMARY KEY,
  trainer_number INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);