CREATE TABLE user_online (
    connection_id BIGSERIAL PRIMARY KEY,
    user_id BIGINT NOT NULL UNIQUE,
    last_seen TIMESTAMP WITH TIME ZONE NOT NULL,
    online BOOL DEFAULT true,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);