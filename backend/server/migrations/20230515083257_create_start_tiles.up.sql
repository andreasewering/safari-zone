CREATE TABLE start_tile (
  id BIGSERIAL PRIMARY KEY,
  map_id BIGINT NOT NULL,
  x INT NOT NULL,
  y INT NOT NULL,
  layer_number INT NOT NULL,
  UNIQUE(map_id, x, y, layer_number),
  FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE CASCADE
);

SELECT create_encryption_function_for_table('start_tile');
ALTER TABLE start_tile ALTER COLUMN id SET DEFAULT encrypt_start_tile(nextval('start_tile_id_seq'));
