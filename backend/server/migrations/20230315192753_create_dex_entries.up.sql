CREATE TABLE dex_entry (
    id BIGSERIAL PRIMARY KEY,
    amount_normal INTEGER NOT NULL,
    amount_shiny INTEGER NOT NULL,
    user_id BIGINT NOT NULL,
    pokemon_number INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    UNIQUE (user_id, pokemon_number)
);

CREATE INDEX idx_user_id ON dex_entry(user_id);
