CREATE TABLE user_items (
  id BIGSERIAL PRIMARY KEY,
  user_id BIGINT NOT NULL,
  item SAFARI_ITEM NOT NULL,
  amount INT NOT NULL,
  CONSTRAINT amount_non_negative CHECK (amount >= 0),
  UNIQUE (user_id, item),
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
