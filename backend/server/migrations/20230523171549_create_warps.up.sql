CREATE TABLE warp (
  id BIGSERIAL PRIMARY KEY,
  from_x INT NOT NULL,
  from_y INT NOT NULL,
  from_layer_number INT NOT NULL,
  from_map_id BIGINT NOT NULL,
  to_x INT NOT NULL,
  to_y INT NOT NULL,
  to_layer_number INT NOT NULL,
  to_map_id BIGINT NOT NULL,
  FOREIGN KEY (from_map_id) REFERENCES map(id) ON DELETE CASCADE,
  FOREIGN KEY (to_map_id) REFERENCES map(id) ON DELETE CASCADE
);

CREATE INDEX idx_from_map_id ON warp(from_map_id);
