CREATE TYPE SPAWN_KIND AS ENUM ('grass', 'water');

CREATE TABLE area_encounters (
    id BIGSERIAL PRIMARY KEY,
    probability REAL NOT NULL,
    area_id BIGINT NOT NULL,
    pokemon_number INTEGER NOT NULL,
    spawn_kind SPAWN_KIND NOT NULL,
    FOREIGN KEY (area_id) REFERENCES area(id) ON DELETE CASCADE,
    UNIQUE (area_id, pokemon_number)
);

CREATE INDEX idx_area_id ON area_encounters(area_id);

CREATE TABLE encounter (
    id BIGSERIAL PRIMARY KEY,
    pokemon_number INT NOT NULL,
    map_id BIGINT NOT NULL,
    x INT NOT NULL,
    y INT NOT NULL,
    layer_number INT NOT NULL,
    direction DIRECTION NOT NULL,
    is_shiny BOOL NOT NULL,
    despawn_time timestamp with time zone NOT NULL,
    FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE CASCADE
);

SELECT create_encryption_function_for_table('encounter');
ALTER TABLE encounter ALTER COLUMN id SET DEFAULT encrypt_encounter(nextval('encounter_id_seq'));
