CREATE TABLE friend (
    id BIGSERIAL PRIMARY KEY,
    from_user_id BIGINT NOT NULL,
    to_user_id BIGINT NOT NULL,
    favorite BOOL NOT NULL DEFAULT false,
    /* By using soft-delete here instead of removing a previous friend relationship entirely, we can preserve e.g. chat messages */
    deleted_at TIMESTAMP WITH TIME ZONE,
    since TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    UNIQUE (from_user_id, to_user_id),
    FOREIGN KEY (from_user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (to_user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE TABLE groups (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    created_by BIGINT NOT NULL,
    /* By using a soft-delete here instead of removing a previous group entirely, we can preserve e.g. chat messages */
    deleted_at TIMESTAMP WITH TIME ZONE,
    UNIQUE (name),
    FOREIGN KEY (created_by) REFERENCES users(id) ON DELETE CASCADE
);
SELECT create_encryption_function_for_table('groups');
ALTER TABLE groups
ALTER COLUMN id
SET DEFAULT encrypt_groups(nextval('groups_id_seq'));
CREATE TYPE GROUPROLE AS ENUM ('admin', 'member');
CREATE TABLE group_member (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGINT NOT NULL,
    group_id BIGINT NOT NULL,
    favorite BOOL NOT NULL DEFAULT false,
    /* By using a soft-delete here instead of removing a previous group relationship entirely, we can preserve e.g. chat messages */
    deleted_at TIMESTAMP WITH TIME ZONE,
    since TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    role GROUPROLE NOT NULL,
    FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    UNIQUE (group_id, user_id)
);
CREATE TABLE friend_request (
    from_user_id BIGINT NOT NULL,
    to_user_id BIGINT NOT NULL,
    issued_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    PRIMARY KEY (from_user_id, to_user_id),
    FOREIGN KEY (from_user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (to_user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE TABLE group_member_request (
    user_id BIGINT NOT NULL,
    group_id BIGINT NOT NULL,
    issued_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    PRIMARY KEY (user_id, group_id),
    FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);