-- Postgres does not support enum value removal.
-- So instead we execute the following steps:
-- 1. Rename the enum to *_old
ALTER TYPE LOCALE RENAME TO LOCALE_OLD;
-- 2. Recreate the enum with all variants except the one to remove
CREATE TYPE LOCALE AS ENUM ('DE', 'EN');

-- 3. Delete rows using the enum variant to remove from all affected tables
DELETE FROM area_translations WHERE locale = 'FR';
-- 4. Migrate other rows to the new type
ALTER TABLE area_translations
    ALTER COLUMN locale TYPE LOCALE USING (locale::text::LOCALE);

-- 5. Drop the *_old enum
DROP TYPE LOCALE_OLD;
