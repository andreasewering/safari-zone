CREATE TABLE area_item_generator_metadata (
    id BIGSERIAL PRIMARY KEY,
    area_id BIGINT NOT NULL,
    last_running TIMESTAMP WITH TIME ZONE NOT NULL,
    rng_state BIGINT NOT NULL,
    UNIQUE (area_id),
    FOREIGN KEY (area_id) REFERENCES area(id) ON DELETE CASCADE
);

CREATE TABLE area_encounter_generator_metadata (
    id BIGSERIAL PRIMARY KEY,
    area_id BIGINT NOT NULL,
    last_running TIMESTAMP WITH TIME ZONE NOT NULL,
    rng_state BIGINT NOT NULL,
    UNIQUE (area_id),
    FOREIGN KEY (area_id) REFERENCES area(id) ON DELETE CASCADE
);

CREATE TABLE encounter_generator_metadata (
    id BIGSERIAL PRIMARY KEY,
    encounter_id BIGINT NOT NULL,
    last_running TIMESTAMP WITH TIME ZONE NOT NULL,
    rng_state BIGINT NOT NULL,
    UNIQUE (encounter_id),
    FOREIGN KEY (encounter_id) REFERENCES encounter(id) ON DELETE CASCADE
);
