CREATE EXTENSION permuteseq;

-- Calling this function via SELECT create_encryption_function_for_table('your_table_name');
-- causes a function 'encrypt_your_table_name(id)' to be generated.
-- The function can be used to generate seemingly random ids from the underlying sequence.
-- Since the secret setting xtea.key is used to encrypt, there is no way for a user to guess the sequence
-- and since the regclass of the table is used, a user cannot use data from table to guess the sequence from another table.
-- This comes at a performance cost, but from simple benchmarking it seems like this approach falls somewhere between
-- raw sequential ids and randomly generated uuids when it comes to insertion and query speed as well as table/index size.
CREATE OR REPLACE FUNCTION create_encryption_function_for_table(
  table_name TEXT, 
  min_val BIGINT DEFAULT -9223372036853775807 -- by default leave 1000000 entries open for hardcoded items
)
RETURNS void AS $$
BEGIN
    EXECUTE format(
        'CREATE OR REPLACE FUNCTION encrypt_%I(id BIGINT) RETURNS BIGINT AS $body$ SELECT range_encrypt_element(id, %s, 9223372036854775807, current_setting(''xtea.key'')::BIGINT + to_regclass(%L)::BIGINT); $body$ LANGUAGE sql;',
        table_name,
        min_val,
        table_name
    );
END;
$$ LANGUAGE plpgsql;

CREATE TABLE map (
  id BIGSERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  UNIQUE (name)
);

SELECT create_encryption_function_for_table('map');
ALTER TABLE map ALTER COLUMN id SET DEFAULT encrypt_map(nextval('map_id_seq'));

CREATE TABLE tilemap (
  id BIGSERIAL PRIMARY KEY,
  image_hash VARCHAR NOT NULL,
  map_id BIGINT NOT NULL,
  offset_x INT NOT NULL,
  offset_y INT NOT NULL,
  layers INT[] NOT NULL,
  UNIQUE (map_id),
  FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE CASCADE
);

CREATE TABLE tile (
  id BIGSERIAL PRIMARY KEY,
  x INT NOT NULL,
  y INT NOT NULL,
  layer_number INT NOT NULL,
  map_id BIGINT NOT NULL,
  tile_id INT NOT NULL,
  FOREIGN KEY (map_id) REFERENCES map(id) ON DELETE CASCADE
);

CREATE INDEX idx_map_id ON tile(map_id);
