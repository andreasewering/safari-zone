fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=migrations");

    gen_grpc(&[
        "../../proto/kangaskhan.proto",
        "../../proto/smeargle.proto",
        "../../proto/chatot.proto",
        "../../proto/arceus.proto",
        "../../proto/gardevoir.proto",
    ])?;

    gen_proto(&[
        "../../proto/direction.proto",
        "../../proto/error_code.proto",
        "../../proto/item.proto",
        "../../proto/tag.proto",
        "../../proto/translated.proto",
    ])?;

    Ok(())
}

fn gen_proto(paths: &[&str]) -> Result<(), Box<dyn std::error::Error>> {
    prost_build::compile_protos(paths, &["../../proto"])?;
    Ok(())
}

fn gen_grpc(paths: &[&str]) -> Result<(), Box<dyn std::error::Error>> {
    for path in paths {
        tonic_build::compile_protos(path)?;
    }
    Ok(())
}
