pub mod base64;
pub mod cache;
pub mod time;

pub fn encode_to_bytes<M: prost::Message>(m: M) -> prost::bytes::Bytes {
    let mut bytes = prost::bytes::BytesMut::with_capacity(m.encoded_len());
    m.encode(&mut bytes).expect("encoded_len is correct");
    bytes.into()
}
