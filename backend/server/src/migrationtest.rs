#[cfg(test)]
mod tests {
    use anyhow::Context;

    #[sqlx::test(migrations = false)]
    async fn test(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let migrations = database_macros::include_migrations!();
        let mut conn = pool.acquire().await?;
        database::ensure_migrations_table(conn.as_mut()).await?;

        let mut snapshot = database::create_snapshot(&pool).await?;
        for i in 0..migrations.len() {
            let migrations_up_to_i: Vec<_> = migrations.iter().take(i + 1).cloned().collect();
            database::execute_migrations(&mut conn, migrations_up_to_i.clone())
                .await
                .context(format!("Applying migration {i}"))?;

            let migrations_up_to_i_minus_one = migrations.iter().take(i).cloned().collect();
            database::execute_migrations(&mut conn, migrations_up_to_i_minus_one)
                .await
                .context(format!("Reverting migration {i}"))?;

            let snapshot_after_apply_and_revert = database::create_snapshot(&pool).await?;
            let snapshot_diff = database::diff_snapshot(snapshot, snapshot_after_apply_and_revert);
            assert_eq!(
                snapshot_diff, database::DatabaseSnapshotDiff::default(),
                "Snapshots for tables after migration {i} up/down did not match. This means either 'up' did something that 'down' did not undo or 'down' undid more than it should have.",
            );

            database::execute_migrations(&mut conn, migrations_up_to_i)
                .await
                .context(format!("Reapplying migration {i}"))?;
            snapshot = database::create_snapshot(&pool).await?;
        }

        Ok(())
    }
}
