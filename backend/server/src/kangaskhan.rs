pub mod proto {
    pub mod kangaskhan {
        tonic::include_proto!("kangaskhan");
    }
    use crate::data::proto as data;
}

mod api;
mod catch;
mod config;
mod encounter_state;
mod game_state;
mod item_state;
mod partner_pokemon;
mod pathfinder;
mod player_state;
mod throw;
mod user_trace;
mod warp_cache;
pub mod websocket;

pub use api::Kangaskhan;
pub use config::Settings;
pub use encounter_state::EncounterState;
pub use item_state::ItemState;
pub use player_state::PlayerState;
pub use proto::kangaskhan::kangaskhan_service_server::KangaskhanServiceServer;
pub use warp_cache::WarpCache;

use crate::{smeargle::MoveRuleProvider, TileCache};

pub trait TileDataProvider: MoveRuleProvider + Sync + Send {}

impl TileDataProvider for TileCache {}

#[cfg(test)]
use crate::smeargle::test_helper::TestMoveRuleProvider;
#[cfg(test)]
impl TileDataProvider for TestMoveRuleProvider {}

#[cfg(test)]
pub mod test_helper {
    use crate::{
        data::{MapId, Position, UserId},
        gardevoir::test_helper as gardevoir,
    };

    pub async fn set_user_position(
        pool: &sqlx::PgPool,
        user: &gardevoir::LoggedInUser,
        position: Position,
    ) -> anyhow::Result<()> {
        let Some(map_id) = user.claims().map_id() else {
            anyhow::bail!("Cannot set user position without entering a map first");
        };

        let user_id = user.claims().user_id();
        sqlx::query!("
            INSERT INTO user_position (x, y, layer_number, map_id, user_id, direction) VALUES ($1, $2, $3, $4, $5, 'D')
            ON CONFLICT (user_id) DO UPDATE
                SET x = EXCLUDED.x,
                    y = EXCLUDED.y,
                    layer_number = EXCLUDED.layer_number,
                    map_id = EXCLUDED.map_id",
                position.x,
                position.y,
                position.layer_number,
                map_id as MapId,
                user_id as UserId
                ).execute(pool).await?;
        Ok(())
    }
}
