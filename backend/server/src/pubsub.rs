use std::pin::Pin;

use futures::Stream;

mod channel;
mod mpsc_pubsub;
mod pg_pubsub;

pub type Subscription<Message> = Pin<Box<dyn Stream<Item = Message> + Sync + Send>>;

pub use channel::Channel;

pub use channel::JsonEncoding;
pub use pg_pubsub::PgPubSub;
