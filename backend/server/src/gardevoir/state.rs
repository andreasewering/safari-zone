use std::time::Duration;

use super::configuration::{Settings, VerificationMode};
use super::proto;
use protobuf_web_token::ed25519::SigningKey;
use protobuf_web_token::Signer;
use protobuf_web_token::{ed25519::pkcs8::DecodePrivateKey, Verifier};

pub struct AuthState {
    signer: Signer,
    verifier: Verifier,
    pub(super) settings: Settings,
}

pub struct Token {
    pub(super) token: String,
    valid_for: Duration,
}

impl Token {
    pub fn token(&self) -> &str {
        &self.token
    }
    pub fn valid_for(&self) -> Duration {
        self.valid_for
    }
}

impl AuthState {
    pub async fn new(settings: Settings) -> anyhow::Result<Self> {
        let private_key = settings.jwt.private_key.as_ref();
        let pem = "-----BEGIN PRIVATE KEY-----\n".to_owned()
            + private_key
            + "\n-----END PRIVATE KEY-----";
        let signing_key = SigningKey::from_pkcs8_pem(&pem)?;
        let signer = Signer::new(signing_key);
        let verifier = signer.as_verifier();

        Ok(AuthState {
            signer,
            verifier,
            settings,
        })
    }

    pub(super) fn decode_access_token(
        &self,
        token: &str,
    ) -> Result<proto::AccessTokenClaims, protobuf_web_token::Error> {
        self.verifier.verify_and_check_expiry(token)
    }

    pub(super) fn decode_refresh_token(
        &self,
        token: &str,
    ) -> Result<proto::RefreshTokenClaims, protobuf_web_token::Error> {
        self.verifier.verify_and_check_expiry(token)
    }

    pub(super) fn encode_access_token(&self, claims: &proto::AccessTokenClaims) -> Token {
        let valid_for = Duration::from_secs(self.settings.jwt.access_token_expire_time_in_seconds);
        Token {
            token: self.signer.sign(claims, valid_for),
            valid_for,
        }
    }

    pub(super) fn encode_refresh_token(&self, claims: &proto::RefreshTokenClaims) -> Token {
        let valid_for = Duration::from_secs(self.settings.jwt.refresh_token_expire_time_in_seconds);
        Token {
            token: self.signer.sign(claims, valid_for),
            valid_for,
        }
    }

    pub(super) fn confirm_account_expire_duration(&self) -> Duration {
        Duration::from_secs(self.settings.account.confirm_account_expire_time_in_seconds)
    }

    pub(super) fn verification_mode(&self) -> VerificationMode {
        self.settings.account.verification_mode
    }
}

impl std::fmt::Debug for AuthState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("AuthState")
            .field("signer", &"<signer>")
            .field("verifier", &"<verifier>")
            .field("settings", &self.settings)
            .finish()
    }
}
