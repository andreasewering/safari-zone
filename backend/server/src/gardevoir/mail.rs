use lettre::message::header;
use lettre::message::MultiPart;
use lettre::message::SinglePart;

use lazy_static::lazy_static;
use lettre::AsyncTransport as _;
use maud::PreEscaped;
use maud::{html, DOCTYPE};

use crate::configuration::MailSettings;
use crate::data::SupportedLanguage;
use crate::data::Translated;

use crate::error::AppResult;
use crate::gardevoir::Verification;
use crate::util;
use lettre::{
    transport::smtp, transport::smtp::authentication::Credentials, AsyncSmtpTransport, Message,
    Tokio1Executor,
};

#[async_trait::async_trait]
pub trait MailSender {
    async fn send_mail(&self, message: Message) -> AppResult<()>;
}

pub type MailSenderImpl = AsyncSmtpTransport<Tokio1Executor>;

#[async_trait::async_trait]
impl MailSender for MailSenderImpl {
    async fn send_mail(&self, message: Message) -> AppResult<()> {
        self.send(message).await?;
        Ok(())
    }
}

pub fn build_mail_sender(mail_settings: &MailSettings) -> Result<MailSenderImpl, smtp::Error> {
    if mail_settings.smtp_password.is_empty() && mail_settings.smtp_user.is_empty() {
        tracing::warn!("Using mail sender without TLS. This is fine for development but insecure in production.");
        return Ok(AsyncSmtpTransport::<Tokio1Executor>::builder_dangerous(
            mail_settings.smtp_host.as_ref(),
        )
        .port(mail_settings.smtp_port)
        .build());
    }
    tracing::info!(
        "Using {} as relay for sending mails.",
        mail_settings.smtp_host
    );
    Ok(
        AsyncSmtpTransport::<Tokio1Executor>::relay(&mail_settings.smtp_host)?
            .port(mail_settings.smtp_port)
            .credentials(Credentials::new(
                mail_settings.smtp_user.to_string(),
                mail_settings.smtp_password.to_string(),
            ))
            .build(),
    )
}

struct MailTranslations<'a> {
    title: &'a str,
    prelink: &'a str,
    linktext: &'a str,
    postlink: &'a str,
}

lazy_static! {
    static ref TRANSLATIONS: Translated<MailTranslations<'static>> = {
        let en = MailTranslations {
            title: "Email Verification",
            prelink: "Thank you for signing up! To verify your email address and complete your registration, please click the link below:",
            linktext: "Verify Email",
            postlink: "If you did not sign up for an account, you can safely ignore this email.",
        };
        let de = MailTranslations {
            title: "E-Mail-Verifizierung",
            prelink: "Vielen Dank für deine Anmeldung! Um deine E-Mail-Adresse zu bestätigen und die Registrierung abzuschließen, klicke bitte auf den folgenden Link:",
            linktext: "E-Mail bestätigen",
            postlink: "Wenn du dich nicht für ein Konto angemeldet haben, kannst du diese E-Mail sicher ignorieren.",
        };
        let fr = MailTranslations {
            title: "Vérification de l'Email",
            prelink: "Merci de t'être inscrit ! Pour vérifier ton adresse email et compléter ton inscription, clique sur le lien ci-dessous :",
            linktext: "Vérifier l'Email",
            postlink: "Si tu ne t'es pas inscrit pour un compte, tu peux ignorer cet email en toute sécurité."
        };
        Translated { de, en, fr }
    };
}

pub fn build_registration_verification_mail(
    Verification {
        user_id,
        verification_id,
        email,
        locale,
    }: Verification,
    base_url: &str,
) -> AppResult<Message> {
    let mail_translations = TRANSLATIONS.get(locale);
    let user_id = util::base64::encode_64bit_id(user_id);
    let verification_id = util::base64::encode_64bit_id(verification_id);
    let href =
        format!("{base_url}/verification/?user_id={user_id}&verification_id={verification_id}");

    let html_text = registration_verification_mail_html(locale, &href, mail_translations);
    let plain_text = registration_verification_mail_text(&href, mail_translations);

    let message = Message::builder()
        .to(email.parse()?)
        .from("Safari-Zone <noreply@safari-zone.app>".parse().unwrap())
        .subject("Complete your registration to the Safari-Zone")
        .multipart(
            MultiPart::alternative()
                .singlepart(
                    SinglePart::builder()
                        .header(header::ContentType::TEXT_PLAIN)
                        .body(plain_text),
                )
                .singlepart(
                    SinglePart::builder()
                        .header(header::ContentType::TEXT_HTML)
                        .body(html_text.into_string()),
                ),
        )?;
    Ok(message)
}

fn registration_verification_mail_text(
    href: &str,
    MailTranslations {
        title,
        prelink,
        linktext: _,
        postlink,
    }: &MailTranslations,
) -> String {
    format!(
        r#"{title}
{prelink}:
{href}
{postlink}"#
    )
}

fn registration_verification_mail_html(
    locale: SupportedLanguage,
    href: &str,
    MailTranslations {
        title,
        prelink,
        linktext,
        postlink,
    }: &MailTranslations,
) -> maud::Markup {
    // Note: Emails need some WEIRD html to render correctly on most clients (especially Outlook, which pretty much ignores all css).
    // Tables with role="presentation" mostly replace flexbox divs.
    // Cellpadding, br, empty paragraphs and non-breaking whitespace (&nbsp;) are used for alignment.
    fn horizontal_alignment(spaces: usize) -> maud::Markup {
        PreEscaped("&nbsp;".repeat(spaces))
    }
    html! {
     (DOCTYPE)
     html lang=(locale) {
      table width="100%" border="0" role="presentation" {
        tr {
          td align="center" {
              table cellpadding="20" style="background-color: #f4f4f4; max-width: 600px; font-family: 'Arial', sans-serif" role="presentation" {
              tr {
                td align="center" {
                  br;
                  h1 style="color: #333333" { (title) }
                  p {}
                  p style="color: #555555" { (prelink) }
                  p {}
                  table cellpadding="10" style="background-color: #4caf50; color: #fffff1; border-radius: 4px;" role="presentation" {
                    tr {
                      td align="center" {
                        a style="text-decoration: none; color: #ffffff" href=(href) { (horizontal_alignment(4)) (linktext ) (horizontal_alignment(4)) }
                      }
                    }
                  }
                  p {}
                  p style="color: #555555" { (postlink) }
                  br;
                }
              }
            }

          }
        }
      }
     }
    }
}
