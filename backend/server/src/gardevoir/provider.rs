use std::{collections::HashMap, sync::Arc, time::Duration};

use jwtk::{jwk::RemoteJwksVerifier, HeaderAndClaims};
use serde::Deserialize;

use crate::error::{AppError, AppResult};

use super::configuration::{AndrenaProviderSettings, GithubProviderSettings, Providers};

#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ExternalAuthProvider {
    Andrena,
    Github,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, sqlx::Type)]
#[sqlx(type_name = "auth_provider", rename_all = "lowercase")]
pub enum AuthProvider {
    Andrena,
    Github,
    Safarizone,
}

#[derive(Debug, Clone)]
pub struct ExternalUserData {
    pub user_id: String,
    pub user_name: String,
}

trait ExternalUserDataDecoder {
    async fn verify_and_decode(&self, token: &str) -> AppResult<ExternalUserData>;
}

#[derive(Clone)]
pub struct ExternalAuthProviders {
    andrena: AndrenaAuthProvider,
    github: GithubAuthProvider,
}

impl ExternalAuthProviders {
    pub fn new(providers: Providers, client: reqwest::Client) -> Self {
        Self {
            andrena: AndrenaAuthProvider::new(providers.andrena, client.clone()),
            github: GithubAuthProvider {
                client: client.clone(),
                settings: providers.github,
            },
        }
    }

    pub async fn verify_and_decode(
        &self,
        provider: ExternalAuthProvider,
        token: &str,
    ) -> AppResult<ExternalUserData> {
        match provider {
            ExternalAuthProvider::Andrena => self.andrena.verify_and_decode(token).await,
            ExternalAuthProvider::Github => self.github.verify_and_decode(token).await,
        }
    }
}

#[derive(Clone)]
struct AndrenaAuthProvider(Arc<RemoteJwksVerifier>);

impl AndrenaAuthProvider {
    fn new(settings: AndrenaProviderSettings, client: reqwest::Client) -> Self {
        let verifier = RemoteJwksVerifier::new(
            settings.jwk_url.to_string(),
            Some(client),
            Duration::from_secs(settings.refresh_rate_in_seconds),
        );
        AndrenaAuthProvider(Arc::new(verifier))
    }
}

impl ExternalUserDataDecoder for AndrenaAuthProvider {
    async fn verify_and_decode(&self, token: &str) -> AppResult<ExternalUserData> {
        #[derive(Debug, Clone, Deserialize)]
        struct Claims {
            name: String,
            preferred_username: String,
        }
        let header_and_claims: HeaderAndClaims<Claims> = self.0.verify(token).await?;
        let Claims {
            preferred_username,
            name,
        } = &header_and_claims.claims().extra;
        Ok(ExternalUserData {
            user_id: preferred_username.to_owned(),
            user_name: name.to_owned(),
        })
    }
}

#[derive(Clone)]
struct GithubAuthProvider {
    client: reqwest::Client,
    settings: GithubProviderSettings,
}

impl ExternalUserDataDecoder for GithubAuthProvider {
    async fn verify_and_decode(&self, token: &str) -> AppResult<ExternalUserData> {
        #[derive(Debug, Deserialize)]
        struct GithubUserData {
            id: i128,
            name: String,
        }

        let mut params = HashMap::new();
        params.insert("code", token);
        params.insert("client_id", &self.settings.client_id);
        params.insert("client_secret", &self.settings.client_secret);

        let response = self
            .client
            .post("https://github.com/login/oauth/access_token")
            .header("accept", "application/vnd.github+json")
            .form(&params)
            .send()
            .await?;
        let access_token_response: GithubAccessTokenResponse =
            response.error_for_status()?.json().await?;
        let access_token = match access_token_response {
            GithubAccessTokenResponse::Success { access_token } => access_token,
            GithubAccessTokenResponse::Error {
                error,
                error_description,
            } => {
                return Err(AppError::unauthenticated(format!(
                    "Gitlab authentication failed. {error}: {error_description}"
                )))?;
            }
        };

        let GithubUserData { name, id } = self
            .client
            .get("https://api.github.com/user")
            .header("accept", "application/vnd.github+json")
            .header("authorization", format!("Bearer {access_token}"))
            .header("x-github-api-version", "2022-11-28")
            .header("user-agent", "safarizone/backend")
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?;
        Ok(ExternalUserData {
            user_id: id.to_string(),
            user_name: name,
        })
    }
}

impl From<ExternalAuthProvider> for AuthProvider {
    fn from(value: ExternalAuthProvider) -> Self {
        match value {
            ExternalAuthProvider::Andrena => AuthProvider::Andrena,
            ExternalAuthProvider::Github => AuthProvider::Github,
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum GithubAccessTokenResponse {
    Success {
        access_token: String,
    },
    Error {
        error: String,
        error_description: String,
    },
}
