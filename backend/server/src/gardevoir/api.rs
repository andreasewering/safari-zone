use std::sync::Arc;

use tonic::metadata::AsciiMetadataValue;
use tonic::{Request, Response, Status};

use crate::error::{AppError, AppResult};
use crate::gardevoir::hash;
use crate::gardevoir::provider::AuthProvider;

use super::configuration::VerificationMode;
use super::mail::MailSender;
use super::permission::permission_to_proto;
use super::proto::{
    gardevoir_service_server::GardevoirService, login_request, AccessTokenData, GetConfigRequest,
    GetConfigResponse, LoginRequest, RegistrationRequest, VerificationRequest,
};
use super::proto::{RefreshTokenClaims, SafarizoneCredentials};
use super::provider::{ExternalAuthProvider, ExternalAuthProviders, ExternalUserData};
use super::{mail, Verification};
use super::{AccessTokenClaims, AuthCtx};
use crate::data::{MapId, SupportedLanguage, UserId};
use axum_extra::extract::cookie::{Cookie, SameSite};

#[derive(Clone)]
pub struct Gardevoir {
    db_connection_pool: sqlx::PgPool,
    auth_ctx: AuthCtx,
    external_auth_providers: ExternalAuthProviders,
    mail_sender: Arc<dyn MailSender + Send + Sync>,
}

#[tonic::async_trait]
impl GardevoirService for Gardevoir {
    async fn get_config(
        &self,
        request: Request<GetConfigRequest>,
    ) -> Result<Response<GetConfigResponse>, Status> {
        let GetConfigRequest {} = request.into_inner();
        let settings = &self.auth_ctx.0.settings;
        let response = GetConfigResponse {
            github_client_id: settings.providers.github.client_id.to_string(),
            andrena_realm: settings.providers.andrena.realm.to_string(),
            andrena_client_id: settings.providers.andrena.client_id.to_string(),
        };
        Ok(Response::new(response))
    }

    async fn login(
        &self,
        request: Request<LoginRequest>,
    ) -> Result<Response<AccessTokenData>, Status> {
        let LoginRequest { payload } = request.into_inner();
        let claims = match payload {
            Some(login_request::Payload::SafarizoneCredentials(credentials)) => {
                self.build_claims(credentials).await?
            }
            Some(login_request::Payload::AndrenaAccessToken(access_token)) => {
                let external_user_data = self
                    .external_auth_providers
                    .verify_and_decode(ExternalAuthProvider::Andrena, &access_token)
                    .await?;
                self.provider_user_data_to_claims(ExternalAuthProvider::Andrena, external_user_data)
                    .await?
            }
            Some(login_request::Payload::GithubCode(code)) => {
                let external_user_data = self
                    .external_auth_providers
                    .verify_and_decode(ExternalAuthProvider::Github, &code)
                    .await?;
                self.provider_user_data_to_claims(ExternalAuthProvider::Andrena, external_user_data)
                    .await?
            }
            None => {
                return Err(AppError::bad_request("Missing provider payload").into());
            }
        };
        let (access_token_data, refresh_token) = claims_to_tokens(&self.auth_ctx, claims).await?;
        let refresh_cookie = create_refresh_cookie(refresh_token);
        let mut response = Response::new(access_token_data);
        set_cookie(&mut response, refresh_cookie);
        Ok(response)
    }

    async fn register(
        &self,
        request: Request<RegistrationRequest>,
    ) -> Result<Response<()>, Status> {
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let registration = request.into_inner();
        self.register_user(locale, registration).await?;
        Ok(Response::new(()))
    }

    async fn refresh(&self, request: Request<()>) -> Result<Response<AccessTokenData>, Status> {
        let cookie_header_value = request
            .metadata()
            .get("cookie")
            .ok_or(Status::unauthenticated("No cookie header"))?;

        let cookie_string = cookie_header_value
            .to_str()
            .map_err(|_| Status::invalid_argument("Cookie was not valid ASCII"))?;

        let Some(refresh_token) = find_cookie(cookie_string, REFRESH_COOKIE_KEY) else {
            return Err(Status::unauthenticated("No refresh token cookie"));
        };

        let claims = self
            .auth_ctx
            .0
            .decode_refresh_token(&refresh_token)
            .map_err(AppError::from)?;

        let user = sqlx::query!(
                r#"SELECT user_position.map_id as "map_id?", COALESCE(permissions, '{}') as "permissions!"
                FROM users
                LEFT JOIN user_position ON user_position.user_id = users.id
                LEFT JOIN roles ON roles.id = users.role_id
                WHERE users.id = $1"#,
                claims.user_id as i64
            )
            .fetch_one(&self.db_connection_pool)
            .await.map_err(AppError::from)?;
        let permissions = user
            .permissions
            .into_iter()
            .filter_map(|permission| {
                let proto_permission = permission_to_proto(permission);
                if let Err(error) = &proto_permission {
                    tracing::warn!(message = "Could not serialize permission", %error);
                }
                proto_permission.ok()
            })
            .map(i32::from)
            .collect();

        let claims = AccessTokenClaims {
            user_id: claims.user_id,
            permissions,
            map_id: user.map_id,
        };

        let access_token = self.auth_ctx.0.encode_access_token(&claims);
        let valid_for_seconds = access_token
            .valid_for()
            .as_secs()
            .try_into()
            .expect("Valid for seconds exceeds max u32");
        Ok(Response::new(AccessTokenData {
            access_token: access_token.token,
            valid_for_seconds,
        }))
    }

    async fn logout(&self, _: Request<()>) -> Result<Response<()>, Status> {
        let expired_cookie = Cookie::build((REFRESH_COOKIE_KEY, ""))
            .max_age(time::Duration::seconds(0))
            .build();
        let mut response = Response::new(());
        set_cookie(&mut response, expired_cookie);
        Ok(response)
    }

    async fn verify(&self, request: Request<VerificationRequest>) -> Result<Response<()>, Status> {
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let VerificationRequest {
            user_id,
            verification_id,
        } = request.into_inner();

        self.verify_user(locale, user_id.into(), verification_id)
            .await?;
        Ok(Response::new(()))
    }
}

impl Gardevoir {
    pub fn new<MS>(
        db_connection_pool: sqlx::PgPool,
        client: reqwest::Client,
        auth_ctx: AuthCtx,
        mail_sender: MS,
    ) -> Self
    where
        MS: MailSender + Send + Sync + 'static,
    {
        let providers = auth_ctx.0.settings.providers.clone();
        Gardevoir {
            auth_ctx,
            db_connection_pool,
            mail_sender: Arc::new(mail_sender),
            external_auth_providers: ExternalAuthProviders::new(providers, client),
        }
    }

    async fn send_verification<'a, E>(
        &self,
        db_executor: E,
        verification: Verification,
    ) -> AppResult<()>
    where
        E: sqlx::PgExecutor<'a>,
    {
        match self.auth_ctx.0.verification_mode() {
            VerificationMode::None => {
                verify_user_internal(
                    db_executor,
                    verification.user_id,
                    verification.verification_id,
                )
                .await?;
            }
            VerificationMode::Mail => {
                let message = mail::build_registration_verification_mail(
                    verification,
                    "http://localhost:3000",
                )?;
                self.mail_sender.send_mail(message).await?;
            }
        };
        Ok(())
    }

    async fn build_claims(&self, login: SafarizoneCredentials) -> AppResult<AccessTokenClaims> {
        let _x = sqlx::query!(
            "SELECT user_id, verification_id FROM users JOIN user_verification ON user_id = users.id WHERE username = $1",
            login.identifier,
        )
        .fetch_all(&self.db_connection_pool)
        .await?;
        struct QueryResult {
            id: UserId,
            password: String,
            map_id: Option<MapId>,
            permissions: Vec<String>,
        }
        let QueryResult {
            id,
            map_id,
            password,
            permissions,
        } = sqlx::query_as!(
            QueryResult,
            r#"
            SELECT users.id, password, COALESCE(permissions, '{}') as "permissions!", user_position.map_id as "map_id: _"
            FROM users
            LEFT JOIN roles ON roles.id = users.role_id
            LEFT JOIN user_position ON user_position.user_id = users.id
            JOIN user_login ON user_login.user_id = users.id
            WHERE email = $1
            OR username = $1
            AND NOT EXISTS (
                SELECT 1 FROM user_verification WHERE user_id = users.id
            )
            "#,
            login.identifier
        )
        .fetch_one(&self.db_connection_pool)
        .await?;

        hash::verify(&login.password, password.as_str())?;
        let permissions = permissions_to_proto(permissions);

        Ok(AccessTokenClaims {
            user_id: id.into(),
            permissions,
            map_id: map_id.map(i64::from),
        })
    }

    async fn register_user(
        &self,
        locale: SupportedLanguage,
        registration: RegistrationRequest,
    ) -> AppResult<()> {
        let new_user = NewUser::try_from(registration)?;
        let mut transaction = self.db_connection_pool.begin().await?;

        let inserted_row = sqlx::query!(
            r#"INSERT INTO users (username, display_name, auth_provider) 
            VALUES ($1, $1, 'safarizone')
            RETURNING id as "id: UserId""#,
            new_user.username
        )
        .fetch_one(&mut *transaction)
        .await?;

        sqlx::query!(
            r#"
            INSERT INTO user_login (user_id, email, password)
            VALUES ($1, $2, $3)
        "#,
            inserted_row.id as UserId,
            new_user.email,
            new_user.password
        )
        .execute(&mut *transaction)
        .await?;

        let confirm_account_expire_duration = sqlx::postgres::types::PgInterval::try_from(
            self.auth_ctx.0.confirm_account_expire_duration(),
        )
        .map_err(|err| {
            AppError::inconsistency(format!("Invalid confirm_account_expire_duration: {err}"))
        })?;

        let user_id = inserted_row.id;
        let verification = sqlx::query!(
            r#"INSERT INTO user_verification (user_id, verification_id_valid_until)
            VALUES ($1, NOW() + $2)
            RETURNING verification_id"#,
            user_id as UserId,
            confirm_account_expire_duration
        )
        .fetch_one(&mut *transaction)
        .await?;

        let verification = Verification {
            user_id,
            verification_id: verification.verification_id,
            locale,
            email: new_user.email,
        };

        self.send_verification(&mut *transaction, verification)
            .await?;
        transaction.commit().await?;

        Ok(())
    }

    async fn verify_user(
        &self,
        locale: SupportedLanguage,
        user_id: UserId,
        verification_id: i64,
    ) -> AppResult<()> {
        let verification = sqlx::query!(
            r#"SELECT verification_id as "verification_id?", verification_id_valid_until < NOW() as "expired?", email
            FROM user_login
            LEFT JOIN user_verification
            ON user_login.user_id = user_verification.user_id
            WHERE user_login.user_id = $1"#,
            user_id as UserId
        ).fetch_one(&self.db_connection_pool).await?;

        let (Some(id), Some(expired)) = (verification.verification_id, verification.expired) else {
            return AppError::conflict(format!("User {user_id} is already verified")).into();
        };
        if id != verification_id {
            return AppError::not_found(format!(
                    "Given verification id {verification_id} for user {user_id} did not match the one in the database"
                )).into();
        }
        if expired {
            // generate new verification_id and send it to the user
            let confirm_account_expire_duration = sqlx::postgres::types::PgInterval::try_from(
                self.auth_ctx.0.confirm_account_expire_duration(),
            )
            .map_err(|err| {
                AppError::inconsistency(format!("Invalid confirm_account_expire_duration: {err}"))
            })?;

            let new_verification = sqlx::query!(
                r#"UPDATE user_verification
                SET verification_id = generate_random_i64(),
                    verification_id_valid_until = NOW() + $2
                WHERE user_id = $1  
                RETURNING verification_id"#,
                user_id as UserId,
                confirm_account_expire_duration
            )
            .fetch_one(&self.db_connection_pool)
            .await?;

            let verification = Verification {
                user_id,
                verification_id: new_verification.verification_id,
                locale,
                email: verification.email,
            };

            self.send_verification(&self.db_connection_pool, verification)
                .await?;
        } else {
            verify_user_internal(&self.db_connection_pool, user_id, verification_id).await?;
        }

        Ok(())
    }

    async fn provider_user_data_to_claims(
        &self,
        external_auth_provider: ExternalAuthProvider,
        external_user_data: ExternalUserData,
    ) -> AppResult<AccessTokenClaims> {
        let user = sqlx::query!(
            r#"WITH upsert AS (
                INSERT INTO users (username, display_name, auth_provider)
                VALUES ($1, $2, $3)
                ON CONFLICT (username, auth_provider) DO UPDATE
                    SET display_name = EXCLUDED.display_name
                RETURNING id, role_id, display_name
            )
                SELECT upsert.id as "user_id!: UserId", map_id as "map_id?: MapId", COALESCE(permissions, '{}') as "permissions!" FROM upsert
                LEFT JOIN user_position ON user_position.user_id = upsert.id 
                LEFT JOIN roles ON roles.id = upsert.role_id
            "#,
            external_user_data.user_id,
            external_user_data.user_name,
            AuthProvider::from(external_auth_provider) as AuthProvider
        ).fetch_one(&self.db_connection_pool).await?;

        Ok(AccessTokenClaims {
            map_id: user.map_id.map(i64::from),
            user_id: user.user_id.into(),
            permissions: permissions_to_proto(user.permissions),
        })
    }
}

#[derive(Debug, Clone)]
struct NewUser {
    username: String,
    password: String,
    email: String,
}

impl TryFrom<RegistrationRequest> for NewUser {
    type Error = hash::Error;
    fn try_from(user: RegistrationRequest) -> Result<Self, Self::Error> {
        Ok(NewUser {
            username: user.username,
            password: hash::hash(user.password.as_str())?,
            email: user.email,
        })
    }
}

async fn verify_user_internal<'a, E>(
    db_executor: E,
    user_id: UserId,
    verification_id: i64,
) -> AppResult<()>
where
    E: sqlx::PgExecutor<'a>,
{
    sqlx::query!(
        "DELETE FROM user_verification WHERE user_id = $1 AND verification_id = $2",
        user_id as UserId,
        verification_id
    )
    .execute(db_executor)
    .await?;

    Ok(())
}

async fn claims_to_tokens(
    auth_ctx: &AuthCtx,
    claims: AccessTokenClaims,
) -> AppResult<(AccessTokenData, String)> {
    let access_token = auth_ctx.0.encode_access_token(&claims);
    let refresh_token_claims = RefreshTokenClaims {
        user_id: claims.user_id,
    };
    let refresh_token = auth_ctx.0.encode_refresh_token(&refresh_token_claims);

    Ok((
        AccessTokenData {
            access_token: access_token.token,
            valid_for_seconds: 900,
        },
        refresh_token.token,
    ))
}

fn permissions_to_proto(permissions: Vec<String>) -> Vec<i32> {
    permissions
        .into_iter()
        .filter_map(|permission| {
            let proto_permission = permission_to_proto(permission);
            if let Err(error) = &proto_permission {
                tracing::warn!(message = "Could not serialize permission", %error);
            }
            proto_permission.ok()
        })
        .map(i32::from)
        .collect()
}

const REFRESH_COOKIE_KEY: &str = "refresh_token";

fn create_refresh_cookie<'c>(refresh_token: String) -> Cookie<'c> {
    Cookie::build((REFRESH_COOKIE_KEY, refresh_token))
        .max_age(time::Duration::days(365))
        .http_only(true)
        .same_site(SameSite::Strict)
        .secure(true)
        .path("/gardevoir.GardevoirService")
        .build()
}

fn set_cookie<T>(response: &mut Response<T>, cookie: Cookie) {
    match cookie.to_string().parse::<AsciiMetadataValue>() {
        Err(error) => {
            tracing::error!(message = "Converting cookie to gRPC metadata value failed", %error);
        }
        Ok(cookie_string) => {
            response.metadata_mut().insert("set-cookie", cookie_string);
        }
    }
}

fn find_cookie(cookie_string: &str, cookie_name: &str) -> Option<String> {
    cookie_string
        .split(';')
        .filter_map(|cookie| {
            Cookie::parse(cookie.trim())
                .inspect_err(|error| {
                    tracing::debug!(message = "Failed to parse cookie", %error);
                })
                .ok()
        })
        .find(|cookie| cookie.name() == cookie_name)
        .map(|cookie| cookie.value().to_owned())
}
