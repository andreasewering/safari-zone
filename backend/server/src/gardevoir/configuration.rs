use std::sync::Arc;

use serde::Deserialize;

#[derive(Debug, Deserialize, Clone)]
pub struct Settings {
    pub jwt: JwtSettings,
    pub account: AccountSettings,
    pub providers: Providers,
}

#[derive(Debug, Deserialize, Clone)]
pub struct JwtSettings {
    pub access_token_expire_time_in_seconds: u64,
    pub refresh_token_expire_time_in_seconds: u64,
    pub private_key: Arc<str>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct AccountSettings {
    #[allow(dead_code)] // TODO reset password functionality is not yet implemented
    pub reset_password_expire_time_in_seconds: u32,
    pub confirm_account_expire_time_in_seconds: u64,
    pub verification_mode: VerificationMode,
}

#[derive(Debug, Deserialize, Clone, Copy)]
#[serde(rename_all = "snake_case")]
pub enum VerificationMode {
    Mail,
    None,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Providers {
    pub andrena: AndrenaProviderSettings,
    pub github: GithubProviderSettings,
}

#[derive(Debug, Deserialize, Clone)]
pub struct AndrenaProviderSettings {
    pub jwk_url: Arc<str>,
    pub refresh_rate_in_seconds: u64,
    pub client_id: Arc<str>,
    pub realm: Arc<str>
}

#[derive(Debug, Deserialize, Clone)]
pub struct GithubProviderSettings {
    pub client_id: Arc<str>,
    pub client_secret: Arc<str>
}
