use argon2::{
    password_hash::{
        self, rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString,
    },
    Argon2,
};

pub type Error = password_hash::Error;

pub fn hash(password: &str) -> Result<String, password_hash::Error> {
    let salt = SaltString::generate(&mut OsRng);

    let hashed_password = Argon2::default()
        .hash_password(password.as_bytes(), &salt)?
        .to_string();
    Ok(hashed_password)
}

pub fn verify(plain: &str, hashed: &str) -> Result<(), password_hash::Error> {
    Argon2::default().verify_password(plain.as_bytes(), &PasswordHash::new(hashed)?)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn hash_and_verify_works_for_example() -> Result<(), password_hash::Error> {
        let hashed_password = hash("my_password")?;
        let _hashed_password2 = hash("admin123")?;
        verify("my_password", &hashed_password)
    }

    #[test]
    fn hash_and_verify_fails_for_wrong_password() -> Result<(), password_hash::Error> {
        let hashed_password = hash("my_password")?;
        verify("not_my_password", &hashed_password).expect_err("Passwords should not match!");
        Ok(())
    }
}
