use std::fmt::Display;

use super::proto;

pub struct UnknownPermission(String);

impl Display for UnknownPermission {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Unknown permission: '{}'", self.0))
    }
}

pub fn permission_to_proto(value: String) -> Result<proto::Permission, UnknownPermission> {
    match value.as_str() {
        "GIVE_ROLE" => Ok(proto::Permission::GiveRole),
        "VIEW_ROLE" => Ok(proto::Permission::ViewRole),
        "READ_MAP_DETAILS" => Ok(proto::Permission::ReadMapDetails),
        "EDIT_MAPS" => Ok(proto::Permission::EditMaps),
        "EDIT_AREAS" => Ok(proto::Permission::EditAreas),
        "EDIT_TILES" => Ok(proto::Permission::EditTiles),
        "EDIT_WARPS" => Ok(proto::Permission::EditWarps),
        "EDIT_START_TILES" => Ok(proto::Permission::EditStartTiles),
        "TRANSLATE_API" => Ok(proto::Permission::TranslateApi),
        "READ_ALL_POKEMON" => Ok(proto::Permission::ReadAllPokemon),
        _ => Err(UnknownPermission(value)),
    }
}
