use std::ops::Bound;

use sqlx::{postgres::types::PgRange, Postgres};

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct IntRange(i32, i32);

impl IntRange {
    pub fn new(min: i32, max: i32) -> Self {
        if min > max {
            Self(max, min)
        } else {
            Self(min, max)
        }
    }

    pub fn start_inclusive(&self) -> i32 {
        self.0
    }

    pub fn end_inclusive(&self) -> i32 {
        self.1
    }

    pub fn includes(&self, value: i32) -> bool {
        self.0 <= value && self.1 >= value
    }

    pub fn diff(&self) -> u32 {
        (self.1 - self.0)
            .try_into()
            .expect("Invariant of IntRange should guarantee that max >= min")
    }
}

impl From<IntRange> for PgRange<i32> {
    fn from(value: IntRange) -> Self {
        PgRange {
            start: Bound::Included(value.0),
            end: Bound::Included(value.1),
        }
    }
}

impl TryFrom<PgRange<i32>> for IntRange {
    type Error = String;
    fn try_from(value: PgRange<i32>) -> Result<Self, Self::Error> {
        match (value.start, value.end) {
            (Bound::Included(s), Bound::Excluded(e)) => Ok(IntRange(s, e - 1)),
            (Bound::Excluded(s), Bound::Included(e)) => Ok(IntRange(s + 1, e)),
            (Bound::Included(s), Bound::Included(e)) => Ok(IntRange(s, e)),
            (Bound::Excluded(s), Bound::Excluded(e)) => Ok(IntRange(s + 1, e - 1)),
            _ => Err("Ranges are not allowed to be unbounded".to_owned()),
        }
    }
}

impl sqlx::Type<Postgres> for IntRange {
    fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
        <PgRange<i32> as sqlx::Type<Postgres>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, Postgres> for IntRange {
    fn encode_by_ref(
        &self,
        buf: &mut <Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <PgRange<i32> as sqlx::Encode<Postgres>>::encode((*self).into(), buf)
    }
}

impl<'r> sqlx::Decode<'r, Postgres> for IntRange {
    fn decode(
        value: <Postgres as sqlx::Database>::ValueRef<'r>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let pg_range = <PgRange<i32> as sqlx::Decode<Postgres>>::decode(value)?;
        let range = IntRange::try_from(pg_range)?;
        Ok(range)
    }
}

impl sqlx::postgres::PgHasArrayType for IntRange {
    fn array_type_info() -> sqlx::postgres::PgTypeInfo {
        <PgRange<i32> as sqlx::postgres::PgHasArrayType>::array_type_info()
    }
}
