use time::OffsetDateTime;

use crate::{
    chat_message::{Located, Tag},
    data::{MapId, UserId},
};

#[derive(Debug, Clone)]
pub struct MapMessage {
    pub map_id: MapId,
    pub text: String,
    pub tags: Vec<Located<Tag>>,
    pub from_user: UserId,
    pub sent_at: OffsetDateTime,
}
