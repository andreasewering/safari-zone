use crate::data::{EncounterId, ItemId, PokemonNumber};
use time::OffsetDateTime;

use crate::data::{Direction, MapId, Position};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct PokemonMessageWrapper {
    pub map_id: MapId,
    pub encounter_id: EncounterId,
    pub message: PokemonMessage,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum PokemonMessage {
    New(NewEncounter),
    Move {
        position: Position,
        direction: Direction,
    },
    Remove,
    Caught {
        by: ItemId,
    },
    BrokeOut {
        from: ItemId,
    },
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct NewEncounter {
    pub position: Position,
    pub direction: Direction,
    pub pokemon_number: PokemonNumber,
    pub is_shiny: bool,
    pub despawn_time: OffsetDateTime,
}
