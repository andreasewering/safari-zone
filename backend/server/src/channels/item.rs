use time::OffsetDateTime;

use crate::data::{Item, ItemId, MapId};

#[derive(Clone, Debug, Copy, PartialEq)]
pub struct ItemMessageWrapper {
    pub item_id: ItemId,
    pub map_id: MapId,
    pub msg: ItemMessage,
}

#[derive(Clone, Debug, Copy, PartialEq)]
pub enum ItemMessage {
    New {
        item: Item,
        x: f32,
        y: f32,
        layer_number: i32,
        despawn_time: OffsetDateTime,
    },
    Remove,
}
