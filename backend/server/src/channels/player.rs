use crate::data::{Item, PokemonNumber};
use crate::data::{ItemId, Throw, TrainerNumber, UserId};

use crate::data::Direction;
use crate::data::MapId;
use crate::data::Position;

#[derive(Clone, Debug)]
pub struct PlayerMessageWrapper {
    pub user_id: UserId,
    pub map_id: Option<MapId>,
    pub message: PlayerMessage,
}

#[derive(Clone, Debug)]
pub enum PlayerMessage {
    PlayerMoved {
        position: Position,
        direction: Direction,
    },
    WentOffline,
    EnteredMap {
        position: Position,
        direction: Direction,
        user_name: String,
        trainer_number: TrainerNumber,
        partner_pokemon_number: PokemonNumber,
        partner_pokemon_is_shiny: bool,
    },
    PartnerPokemonMoved {
        position: Position,
        direction: Direction,
    },
    ChangedPartnerPokemon {
        pokemon_number: PokemonNumber,
        is_shiny: bool,
    },
    ChangedTrainerSprite {
        trainer_number: TrainerNumber,
    },
    ThrewBall {
        from_x: f32,
        from_y: f32,
        throw: Throw,
        item: Item,
        ball_id: ItemId,
    },
}
