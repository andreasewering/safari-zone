/**
 * Module centered around async cancellation and graceful shutdown.
 *
 * Why do we need this when we have the `Drop` trait? Because some cleanups need to run asynchronous code.
 * Tokio tasks are automatically detached and run in the background to completion unless explicitely awaited/joined.
 * Unfortunately, this explicit awaiting does not work in many scenarios since libraries will just start tokio tasks for you.
 *
 * The approach taken here is derived from the recommended graceful shutdown approach from the [tokio documentation](https://tokio.rs/tokio/topics/shutdown):
 * - A CancellationToken is used to communicate the intent of shutting down
 * - A TaskTracker is used to communicate when all tasks have successfully shut down
 *
 * Due to requirements in the `arceus` package (generators with subtasks may be started and stopped for specific areas),
 * we need to make the approach a bit more complicated.
 * It should not only be possible to shutdown a subtree of the application
 * (already possible with CancellationToken), but to shutdown tasks in order from leaves to the root (not possible with TaskTracker).
 *
 * The solution presented by this module therefore includes a tree of task trackers.
 * Then, we can shutdown starting from the leaves.
 */
use std::{
    collections::HashSet,
    fmt::Display,
    sync::{Arc, Mutex},
    time::Duration,
};

use futures::{Stream, StreamExt};
use fxhash::FxBuildHasher;
use tokio::sync::{watch, Notify};
use tokio_stream::wrappers::WatchStream;

///
/// Gracefully shutdown a future/select over a list of futures.
/// The macro will stop all other futures from proceeding after the cancellation signal was received.
/// It then waits for any child tasks to finish and then optionally runs custom cleanup code.
///
/// # Examples
///
/// With cleanup code:  
/// ```
/// graceful_shutdown! {
///   monitor: monitor,
///   cleanup: {
///     // any expression
///   },
///   _ = my_main_loop => {
///     // ...
///   }
///   _ = my_other_future => {
///     // ...
///   }   
/// }
/// ```
///
/// Without cleanup code:
/// ```
/// graceful_shutdown! {
///   monitor: monitor,
///   _ = my_main_loop => {
///     // ...
///   }
///   _ = my_other_future => {
///     // ...
///   }   
/// }
/// ```
#[macro_export]
macro_rules! graceful_shutdown {
    (ctrl: $ctrl:expr, filter: $filter:expr, name: $name:expr, cleanup: $result:pat => $cleanup:block, $($rest:tt)*) => {{
        let _prevent_shutdown = $ctrl.prevent_shutdown($name);
        tokio::select! {
            $result = $ctrl.cancelled_with($filter) => {
                $cleanup
            },
            $($rest)*
        }
    }};
    (ctrl: $ctrl:expr, name: $name:expr, cleanup: $result:pat => $cleanup:block, $($rest:tt)*) => {{
        let _prevent_shutdown = $ctrl.prevent_shutdown($name);
        tokio::select! {
            $result = $ctrl.cancelled() => {
                $cleanup
            },
            $($rest)*
        }
    }};
    (ctrl: $ctrl:expr, filter: $filter:expr, $($rest:tt)*) => {{
        tokio::select! {
            _ = $ctrl.cancelled_with($filter) => {
            },
            $($rest)*
        }
    }};
    (ctrl: $ctrl:expr, $($rest:tt)*) => {{
        tokio::select! {
            _ = $ctrl.cancelled() => {
            },
            $($rest)*
        }
    }};
}

#[derive(Clone)]
pub struct Ctrl<T = ()> {
    cancellation_token: CancellationTokenWithPayload<T>,
    tracker: NamedTracker,
}

#[derive(Clone)]
pub struct CancellationTokenWithPayload<T> {
    token: watch::Sender<Option<T>>,
    rec: watch::Receiver<Option<T>>,
}

impl<T> CancellationTokenWithPayload<T>
where
    T: Clone + Sync + Send + 'static,
{
    fn new() -> Self {
        let (send, rec) = watch::channel(None);
        CancellationTokenWithPayload { token: send, rec }
    }

    fn cancel(&self, payload: T) {
        self.token
            .send(Some(payload))
            .expect("Last receiver cannot be dropped because self includes one instance.");
    }

    fn cancelled(&self) -> impl Stream<Item = T> + Unpin + use<T> {
        let stream = WatchStream::new(self.rec.clone());
        Box::pin(stream.filter_map(|t| async { t }))
    }
}

pub struct NamedTrackerInner {
    tasks: Mutex<HashSet<String, FxBuildHasher>>,
    on_last_exit: Notify,
}

#[derive(Clone)]
pub struct NamedTracker(Arc<NamedTrackerInner>);

impl NamedTracker {
    fn new() -> Self {
        let inner = NamedTrackerInner {
            tasks: Mutex::new(HashSet::with_hasher(FxBuildHasher::default())),
            on_last_exit: Notify::new(),
        };
        Self(Arc::new(inner))
    }

    fn token(&self, name: String) -> NamedTrackerToken {
        let mut tasks = self
            .0
            .tasks
            .lock()
            .expect("Taskset lock was accessed by the same thread twice.");
        if !tasks.insert(name.clone()) {
            tracing::warn!(%name, "Added task to task tracker with the same name as another task that was already tracked.");
        }

        NamedTrackerToken {
            name,
            tracker: self.clone(),
        }
    }

    async fn wait(&self) {
        match self.0.tasks.lock() {
            Ok(tracker) => {
                if tracker.is_empty() {
                    return;
                }
            }
            Err(error) => tracing::error!(%error, "Named Tracker Lock was poisoned!"),
        }
        self.0.on_last_exit.notified().await
    }
}

pub struct NamedTrackerToken {
    name: String,
    tracker: NamedTracker,
}

impl Drop for NamedTrackerToken {
    fn drop(&mut self) {
        match self.tracker.0.tasks.lock() {
            Ok(mut tracker) => {
                if !tracker.remove(&self.name) {
                    tracing::warn!("Tried to remove tracked task {} but the task was not tracked. Maybe some other task has the same exact name and exited before?", self.name)
                };
                if tracker.is_empty() {
                    self.tracker.0.on_last_exit.notify_waiters();
                }
            }
            Err(error) => tracing::error!(%error, "Named Tracker Lock was poisoned!"),
        }
    }
}

impl<T> Ctrl<T>
where
    T: Clone + Sync + Send + 'static,
{
    pub fn new() -> Self {
        Self {
            cancellation_token: CancellationTokenWithPayload::new(),
            tracker: NamedTracker::new(),
        }
    }

    pub fn prevent_shutdown<D>(&self, name: D) -> NamedTrackerToken
    where
        D: Display,
    {
        self.tracker.token(name.to_string())
    }

    pub async fn shutdown(&self, reason: T, timeout: Duration) {
        self.cancellation_token.cancel(reason);
        if tokio::time::timeout(timeout, self.tracker.wait())
            .await
            .is_err()
        {
            tracing::warn!(open_tasks = %self.tracker, "Unfinished tasks will be forcefully cancelled by the tokio runtime");
        };
    }

    pub async fn cancelled(&self) -> T {
        self.cancelled_with(|_| true).await
    }

    pub async fn cancelled_with<F>(&self, mut cancel_if: F) -> T
    where
        F: FnMut(&T) -> bool,
    {
        let mut cancelled = self.cancellation_token.cancelled();
        loop {
            let next = cancelled.next().await;
            let Some(next) = next else {
                continue;
            };
            if cancel_if(&next) {
                return next;
            }
        }
    }
}

impl Display for NamedTracker {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let tasks = self
            .0
            .tasks
            .lock()
            .expect("Taskset lock was accessed by the same thread twice.");
        let tasks_vec = tasks.iter().map(|str| str.as_ref()).collect::<Vec<_>>();
        f.write_str(&tasks_vec.join(", "))
    }
}

#[cfg(test)]
mod tests {
    use std::sync::{Arc, Mutex};
    use std::time::Duration;

    use super::*;

    #[tokio::test]
    async fn cancel_single_task_gracefully() {
        let ctrl = Ctrl::new();
        let val = Arc::new(Mutex::new(0));
        {
            let val = val.clone();
            let ctrl = ctrl.clone();
            tokio::spawn(async move {
                let _prevent_shutdown = ctrl.prevent_shutdown("test");
                tokio::select! {
                    reason = ctrl.cancelled() => {
                        tokio::time::sleep(Duration::from_millis(100)).await;
                        let mut data = val.lock().unwrap();
                        *data += reason;
                    },
                    _ = tokio::time::sleep(Duration::from_millis(100)) => {
                        panic!("Task should have been cancelled");
                    }
                }
            })
        };
        tokio::time::sleep(Duration::from_millis(10)).await;
        ctrl.shutdown(1, Duration::from_millis(500)).await;
        assert_eq!(*val.lock().unwrap(), 1);
    }

    #[tokio::test]
    async fn cancel_two_tasks_gracefully() {
        let ctrl = Ctrl::new();
        let val = Arc::new(Mutex::new(0));
        {
            let val = val.clone();
            let ctrl = ctrl.clone();
            tokio::spawn(async move {
                let _prevent_shutdown = ctrl.prevent_shutdown("1");
                tokio::select! {
                    () = ctrl.cancelled() => {
                        tokio::time::sleep(Duration::from_millis(50)).await;
                        let mut data = val.lock().unwrap();
                        *data += 1;
                    },
                    _ = tokio::time::sleep(Duration::from_millis(50)) => {
                        panic!("Task should have been cancelled");
                    }
                }
            })
        };
        {
            let val = val.clone();
            let ctrl = ctrl.clone();
            tokio::spawn(async move {
                let _prevent_shutdown = ctrl.prevent_shutdown("2");
                tokio::select! {
                    () = ctrl.cancelled() => {
                        tokio::time::sleep(Duration::from_millis(100)).await;
                        let mut data = val.lock().unwrap();
                        *data += 2;
                    },
                    _ = tokio::time::sleep(Duration::from_millis(100)) => {
                        panic!("Task should have been cancelled");
                    }
                }
            })
        };
        tokio::time::sleep(Duration::from_millis(10)).await;
        ctrl.shutdown((), Duration::from_millis(500)).await;
        assert_eq!(*val.lock().unwrap(), 3);
    }

    #[tokio::test]
    async fn cancel_one_of_two_tasks_specifically() {
        let ctrl = Ctrl::new();
        let val = Arc::new(Mutex::new(0));
        {
            let val = val.clone();
            let ctrl = ctrl.clone();
            tokio::spawn(async move {
                let _prevent_shutdown = ctrl.prevent_shutdown("1");
                tokio::select! {
                    _ = ctrl.cancelled_with(|val| *val != 2) => {
                        panic!("Task should not have been cancelled");
                    },
                    _ = tokio::time::sleep(Duration::from_millis(50)) => {
                        let mut data = val.lock().unwrap();
                        *data += 1;
                    }
                }
            })
        };
        {
            let val = val.clone();
            let ctrl = ctrl.clone();
            tokio::spawn(async move {
                let _prevent_shutdown = ctrl.prevent_shutdown("2");
                tokio::select! {
                    _ = ctrl.cancelled_with(|val| *val == 2) => {
                        tokio::time::sleep(Duration::from_millis(100)).await;
                        let mut data = val.lock().unwrap();
                        *data += 2;
                    },
                    _ = tokio::time::sleep(Duration::from_millis(100)) => {
                        panic!("Task should have been cancelled");
                    }
                }
            })
        };
        tokio::time::sleep(Duration::from_millis(10)).await;
        ctrl.shutdown(2, Duration::from_millis(500)).await;
        assert_eq!(*val.lock().unwrap(), 3);
    }
}
