pub mod proto {
    pub mod smeargle {
        tonic::include_proto!("smeargle");
    }
}

mod api;
mod config;
mod data;
mod hash;
mod move_rule_provider;
mod neighbour;
mod order;
mod queries;
mod spawn_kind_provider;
mod tile_cache;
mod tilemap;
mod tileset;

use crate::error::{AppError, AppResult};
use std::path::Path;

pub use api::Smeargle;
pub use config::Settings;
pub use data::DirectionalMoveRules;

#[cfg(test)]
pub use data::move_rule::MoveRule;

pub use move_rule_provider::{MapMoveRuleProvider, MoveRuleProvider};
pub use spawn_kind_provider::SpawnKindProvider;

use nonempty::NonEmpty;
pub use proto::smeargle::smeargle_service_server::SmeargleServiceServer;
pub use tile_cache::TileCache;
pub use tileset::write_tileset;

use self::{config::TileConfigs, data::TileId, proto::smeargle::EditMapResponse};
use crate::{
    data::{MapId, Position},
    public_path,
};
use sqlx::PgPool;

pub async fn recreate_tilemap_images(
    tile_configs: &TileConfigs,
    public_path: &Path,
    db_connection_pool: &PgPool,
) -> AppResult<()> {
    let mut tile_stream = sqlx::query_as!(
        TileEntity,
        "SELECT map_id, x, y, layer_number, tile_id FROM tile ORDER BY map_id"
    )
    .fetch(db_connection_pool);

    tracing::info!("Starting to re-create tilemap images based on the current data.");

    type Tile = (MapId, (TileId, Position));

    fn handle_next(tile: TileEntity) -> AppResult<Tile> {
        let result = TileId::try_from(tile.tile_id)
            .map_err(|err| AppError::inconsistency(
                format!(
                    "The following tile is in the database but does not have a valid tile_id: {:?}. Error: {err}",
                    tile
                ),
            ))
            .map(|tile_id| {
                (
                    tile.map_id,
                    (
                        tile_id,
                        Position {
                            x: tile.x,
                            y: tile.y,
                            layer_number: tile.layer_number,
                        },
                    ),
                )
            })?;
        Ok(result)
    }

    use futures::StreamExt as _;

    match tile_stream
        .next()
        .await
        .transpose()?
        .map(handle_next)
        .transpose()?
    {
        None => return Ok(()),
        Some((first_map_id, first_tile)) => {
            let mut current_map_id = first_map_id;
            let mut current_tiles = nonempty::nonempty![first_tile];
            while let Some((map_id, tile)) = tile_stream
                .next()
                .await
                .transpose()?
                .map(handle_next)
                .transpose()?
            {
                // If the map_id stays the same, we just push tiles into current_tiles
                // We are guaranteed that no map_id appears out of order since the database query orders by map_id.
                if map_id == current_map_id {
                    current_tiles.push(tile);
                    continue;
                }

                // If the map_id changes, the current map is finished, so we create the image.
                make_and_write_tilemap_image(
                    db_connection_pool,
                    tile_configs,
                    public_path,
                    current_map_id,
                    &current_tiles,
                )
                .await?;
                // We then start with the next map
                current_map_id = map_id;
                current_tiles = nonempty::nonempty![tile];
            }
            // this is needed for the last map
            make_and_write_tilemap_image(
                db_connection_pool,
                tile_configs,
                public_path,
                current_map_id,
                &current_tiles,
            )
            .await?;
        }
    };
    Ok(())
}

#[derive(Debug)]
struct TileEntity {
    map_id: MapId,
    x: i32,
    y: i32,
    layer_number: i32,
    tile_id: i32,
}

async fn make_and_write_tilemap_image(
    db_connection_pool: &PgPool,
    tile_configs: &TileConfigs,
    public_path: &Path,
    map_id: MapId,
    tiles: &NonEmpty<(TileId, Position)>,
) -> AppResult<EditMapResponse> {
    tracing::info!(
        message = "Writing tilemap for map",
        %map_id
    );
    let tilemap = tilemap::make_tilemap_image(tile_configs, map_id.to_string(), tiles);

    let layers = tilemap.layers.iter().map(|layer| layer.as_i32()).collect();
    let proto_tilemap = EditMapResponse {
        path: tilemap::public_path(public_path, &tilemap.identifier, &tilemap.hash),
        layers,
        offset_x: tilemap.offset.0,
        offset_y: tilemap.offset.1,
    };
    let mut transaction = db_connection_pool.begin().await?;
    queries::upsert_tilemap(&mut *transaction, map_id, &tilemap).await?;
    tilemap.write(&public_path::os_path(public_path)).await?;
    transaction.commit().await?;

    Ok(proto_tilemap)
}

async fn delete_tilemap_image(public_path: &Path, map_id: MapId) -> AppResult<()> {
    let tilemap_dir = public_path::os_path(&tilemap::tilemap_directory(
        public_path,
        &map_id.to_string(),
    ));
    if let Err(err) = tokio::fs::remove_dir_all(tilemap_dir).await {
        if err.kind() != std::io::ErrorKind::NotFound {
            return Err(err.into());
        }
    }
    Ok(())
}

pub async fn set_map_tiles(
    db_connection_pool: &PgPool,
    tile_configs: &TileConfigs,
    public_path: &Path,
    map_id: MapId,
    tiles: NonEmpty<(TileId, Position)>,
) -> AppResult<EditMapResponse> {
    let tilemap = make_and_write_tilemap_image(
        db_connection_pool,
        tile_configs,
        public_path,
        map_id,
        &tiles,
    )
    .await?;
    queries::delete_tiles_for_map(db_connection_pool, map_id).await?;
    queries::insert_tiles_into_map(db_connection_pool, map_id, tiles).await?;
    Ok(tilemap)
}

#[cfg(test)]
pub mod test_helper {

    use crate::data::{MapId, Position};

    pub use super::move_rule_provider::test_impl::{TestMapMoveRuleProvider, TestMoveRuleProvider};
    pub use super::spawn_kind_provider::test_impl::TestSpawnKindProvider;

    pub async fn create_map(pool: &sqlx::PgPool, name: &str) -> anyhow::Result<MapId> {
        let map_id = super::queries::create_map(pool, name).await?;
        Ok(map_id)
    }

    pub async fn add_start_tile(
        pool: &sqlx::PgPool,
        map_id: MapId,
        x: i32,
        y: i32,
    ) -> anyhow::Result<i64> {
        let start_tile_id = super::queries::add_start_tile(
            pool,
            map_id,
            Position {
                x,
                y,
                layer_number: 0,
            },
        )
        .await?;
        Ok(start_tile_id)
    }
}
