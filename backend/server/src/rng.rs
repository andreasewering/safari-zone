use std::sync::Arc;

use parking_lot::Mutex;
use rand::TryRngCore as _;
use rand_core::impls::fill_bytes_via_next;
use sqlx::Postgres;

/**
 * A newtype wrapper around the wyrand pseudo-random number generator.
 *
 * Wyrand only needs a single u64 as state for its "randomness",
 * which obviously does not suffice to make it cryptographically secure.
 * However, it is extremely fast and still provides statistically good randomness.
 *
 * Since wyrand is essentially a pure function u64 -> (u64, u64),
 * the outputs are deterministic, provided you know the initial seed.
 * This is key when tracking down bugs related to randomness or testing without using unrealistic mocks.
 * We take advantage of this fact by storing the seed in the database, but not exposing it to the user.
 *
 * This type is not copy although it could be. This is a deliberate decision to prevent mistakes -
 * cloning/copying the rng means that 2 seperate instances will produce the same events.
 * Calling .clone() manually makes this more explicit.
 */
#[derive(Clone, Debug)]
pub struct FastRng(u64);

impl FastRng {
    #[cfg(test)]
    pub fn from_seed(seed: u64) -> Self {
        FastRng(seed)
    }

    /// Create a new `FastRng` instance seeded from OS randomness.
    /// This should be the only way a fresh instance is created in application code,
    /// but also, this should be only created once on application start, because then, the rest of the application
    /// is deterministic (based on that seed), which makes (integration) testing easier
    /// - all you have to do is call `from_seed` at that central point instead.
    ///
    /// To derive new "seperate" instances from the central `FastRng` instance, use `FastRng::split`.
    /// Most likely, you will also want to call `FastRng::rand` afterwards to mutate the central instance,
    /// such that the next `split` will not produce the same outcome.
    pub fn new() -> Self {
        let seed = rand::rngs::OsRng
            .try_next_u64()
            .expect("OSRng can be initialized");
        FastRng(seed)
    }

    /// Split off a new FastRng instance based on the current seed.
    /// The numbers generated from the resulting instance should be "relatively independent" from the sequence
    /// of the current instance. This makes it suitable when .clone() is not -
    /// when the system in total should be deterministic but it should not be possible for an outsider to observe that
    /// results from one part of the system impact another part.
    pub fn split(&self) -> Self {
        // GPT-4 generated. Overlap of the given pseudo-number generator and the one produced by this function
        // is hopefully low but not tested.
        let prime = 0x9E3779B97F4A7C15;
        let xorshifted = self.0 ^ (self.0 >> 12);
        Self(xorshifted.wrapping_mul(prime))
    }

    #[inline]
    pub fn rand(&mut self) -> u64 {
        // https://github.com/wangyi-fudan/wyhash/blob/master/Modern%20Non-Cryptographic%20Hash%20Function%20and%20Pseudorandom%20Number%20Generator.pdf)
        self.0 = self.0.wrapping_add(0xA076_1D64_78BD_642F);
        let r = u128::from(self.0) * u128::from(self.0 ^ 0xE703_7ED1_A0B4_28DB);
        (r as u64) ^ (r >> 64) as u64
    }
}

/// Simple wrapper around `Arc<Mutex<FastRng>>` to generate new FastRng instances from a central one with interior mutability
#[derive(Clone)]
pub struct RngProvider {
    inner: Arc<Mutex<FastRng>>,
}

impl RngProvider {
    #[cfg(test)]
    pub fn from_seed(seed: u64) -> Self {
        let inner = Arc::new(Mutex::new(FastRng::from_seed(seed)));
        RngProvider { inner }
    }
    pub fn new() -> Self {
        let inner = Arc::new(Mutex::new(FastRng::new()));
        RngProvider { inner }
    }
    pub fn next(&self) -> FastRng {
        let mut rng = self.inner.lock();
        let seeded_rng = rng.split();
        // Mutate rng so that the next call does not yield the same rng
        let _ = rng.rand();
        seeded_rng
    }
}

impl<'q> sqlx::Encode<'q, Postgres> for FastRng {
    fn encode_by_ref(
        &self,
        buf: &mut <Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <i64 as sqlx::Encode<Postgres>>::encode(self.0 as i64, buf)
    }
}

impl<'r> sqlx::Decode<'r, Postgres> for FastRng {
    fn decode(
        value: <Postgres as sqlx::Database>::ValueRef<'r>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let i64 = <i64 as sqlx::Decode<Postgres>>::decode(value)?;
        Ok(FastRng(i64 as u64))
    }
}

impl sqlx::Type<Postgres> for FastRng {
    fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
        <i64 as sqlx::Type<Postgres>>::type_info()
    }
}

impl From<i64> for FastRng {
    fn from(value: i64) -> Self {
        Self(value as u64)
    }
}

impl rand::RngCore for FastRng {
    #[inline]
    fn next_u32(&mut self) -> u32 {
        self.rand() as u32
    }

    #[inline]
    fn next_u64(&mut self) -> u64 {
        self.rand()
    }

    #[inline]
    fn fill_bytes(&mut self, dest: &mut [u8]) {
        fill_bytes_via_next(self, dest);
    }
}
