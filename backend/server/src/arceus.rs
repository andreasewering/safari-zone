pub mod proto {
    pub mod arceus {
        tonic::include_proto!("arceus");
    }
    use crate::data::proto as data;
}

mod api;
mod area_encounter;
mod area_id;
mod area_item;
pub mod config;
mod generation_context;
mod generator;
mod queries;
mod translation_client;

pub use api::Arceus;
pub use config::Settings;
pub use generation_context::GenerationContext;
pub use generator::start_generator;
#[cfg(test)]
pub use generator::Message;
pub use generator::SignalSender;
pub use proto::arceus::arceus_service_server::ArceusServiceServer;
pub use translation_client::OpenAITranslationClient;

#[cfg(test)]
pub mod test_helper {
    use std::time::Duration;

    use soa_rs::Soa;
    use time::OffsetDateTime;

    use super::{
        area_encounter::{self, AreaEncounter},
        area_id::AreaId,
        area_item::{self, AreaItem},
        generator::{self, EncounterData},
        queries,
    };
    use crate::{
        channels::{
            ItemChannel, ItemMessage, ItemMessageWrapper, NewEncounter, PokemonChannel,
            PokemonMessage, PokemonMessageWrapper,
        },
        data::{Direction, EncounterId, Item, ItemId, MapId, PokemonNumber, Position, Translated},
        range::IntRange,
        rng,
    };

    pub async fn spawn_encounter(
        pool: &sqlx::PgPool,
        pokemon_channel: &PokemonChannel,
        map_id: MapId,
        x: i32,
        y: i32,
        pokemon_number: PokemonNumber,
    ) -> anyhow::Result<EncounterId> {
        let despawn_time = generator::TimedInstant::now() + Duration::from_secs(5);
        let is_shiny = false;
        let position = Position {
            x,
            y,
            layer_number: 0,
        };
        let direction = Direction::D;
        let encounter_id = generator::insert_encounter(
            EncounterData {
                pokemon_number,
                map_id,
                position,
                direction,
                is_shiny,
                despawn_time,
            },
            rng::FastRng::new(),
            OffsetDateTime::now_utc(),
            pool,
        )
        .await?;
        pokemon_channel.send(PokemonMessageWrapper {
            map_id,
            encounter_id,
            message: PokemonMessage::New(NewEncounter {
                position,
                direction,
                pokemon_number,
                is_shiny,
                despawn_time: despawn_time.into(),
            }),
        });

        Ok(encounter_id)
    }

    pub async fn spawn_item(
        pool: &sqlx::PgPool,
        item_channel: &ItemChannel,
        map_id: MapId,
        x: f32,
        y: f32,
        item: Item,
    ) -> anyhow::Result<ItemId> {
        let layer_number = 0;
        let despawn_time = generator::TimedInstant::now() + Duration::from_secs(5);
        let item_id = generator::insert_item(
            pool,
            &generator::ItemData {
                item,
                map_id,
                x,
                y,
                layer_number,
                despawn_time,
            },
        )
        .await?;
        item_channel.send(ItemMessageWrapper {
            item_id,
            map_id,
            msg: ItemMessage::New {
                item,
                x,
                y,
                layer_number,
                despawn_time: despawn_time.into(),
            },
        });
        Ok(item_id)
    }

    pub struct AreaBuilder {
        name: String,
        map_id: MapId,
        range_x: IntRange,
        range_y: IntRange,
        range_z: IntRange,
        encounters: Soa<AreaEncounter>,
        items: Vec<AreaItem>,
    }

    impl AreaBuilder {
        pub fn new(map_id: MapId, left_x: i32, right_x: i32, top_y: i32, bottom_y: i32) -> Self {
            AreaBuilder {
                name: "Area".to_string(),
                map_id,
                range_x: IntRange::new(left_x, right_x),
                range_y: IntRange::new(top_y, bottom_y),
                range_z: IntRange::new(0, 0),
                encounters: Soa::new(),
                items: Vec::new(),
            }
        }

        pub fn with_items(mut self, items: Vec<AreaItem>) -> Self {
            self.items = items;
            self
        }

        pub fn with_encounters(mut self, encounters: Soa<AreaEncounter>) -> Self {
            self.encounters = encounters;
            self
        }

        pub async fn persist(self, pool: &sqlx::PgPool) -> anyhow::Result<AreaId> {
            let AreaBuilder {
                name,
                map_id,
                range_x,
                range_y,
                range_z,
                encounters,
                items,
            } = self;
            let area_id = queries::create_area(
                pool,
                queries::AreaToCreate {
                    map_id,
                    name: Translated::from_constant(name),
                    parts: vec![(range_x, range_y, range_z)],
                },
            )
            .await?;

            area_encounter::set_encounters_in_db(area_id, encounters, pool).await?;
            area_item::set_items_from_db(area_id, items, pool).await?;

            Ok(area_id)
        }
    }
}
