use futures_util::StreamExt;
use tokio::sync::broadcast;
use tokio_stream::wrappers::{errors::BroadcastStreamRecvError, BroadcastStream};

use futures::Stream;
pub use item::{ItemMessage, ItemMessageWrapper};
pub use map::MapMessage;
pub use player::{PlayerMessage, PlayerMessageWrapper};
pub use pokemon::{NewEncounter, PokemonMessage, PokemonMessageWrapper};

mod item;
mod map;
mod player;
mod pokemon;

pub type PlayerChannel = Channel<PlayerMessageWrapper>;
pub type PokemonChannel = Channel<PokemonMessageWrapper>;
pub type ItemChannel = Channel<ItemMessageWrapper>;
pub type MapChannel = Channel<MapMessage>;

#[derive(Clone)]
pub struct Channel<T> {
    sender: broadcast::Sender<T>,
    name: &'static str,
}

impl<T> Channel<T>
where
    T: Clone + std::fmt::Debug + Send + 'static,
{
    pub fn new(name: &'static str) -> Self {
        let (sender, _receiver) = broadcast::channel(128);
        Self { sender, name }
    }

    pub fn send(&self, msg: T) {
        let number_of_receivers = self.sender.send(msg).unwrap_or_default();
        tracing::trace!(
            message = "Sent message over broadcast channel",
            receivers = number_of_receivers,
            channel_name = self.name
        );
    }

    pub fn receive(&self) -> impl Stream<Item = T> + use<T> {
        let channel_name = self.name;
        BroadcastStream::new(self.sender.subscribe()).filter_map(move |result| {
            Box::pin(async move {
                match result {
                    Err(BroadcastStreamRecvError::Lagged(number_of_skipped_messages)) => {
                        tracing::warn!(
                            message = "Skipped processing of broadcasted messages",
                            skipped_messages = number_of_skipped_messages,
                            %channel_name
                        );
                        None
                    }
                    Ok(msg) => Some(msg),
                }
            })
        })
    }
}
