mod arceus;
mod channels;
mod chat_message;
mod chatot;
mod configuration;
mod data;
mod error;
mod gardevoir;
mod kangaskhan;
mod migrationtest;
mod public_path;
mod pubsub;
mod range;
mod rng;
mod shutdown;
mod smeargle;
mod util;
mod watchog;

extern crate alloc; // Needed because of https://github.com/cloudhead/nonempty/issues/68

use crate::{kangaskhan::WarpCache, smeargle::TileCache};
use axum::{
    Router,
    extract::{Query, State, ws::WebSocketUpgrade},
    response::{IntoResponse, Response},
    routing::get,
};
use data::MapId;
use futures::StreamExt as _;
use http::StatusCode;
use kangaskhan::websocket::WebsocketCtx;
use rng::RngProvider;
use serde::Deserialize;
use sqlx::postgres::PgPoolOptions;
use std::{net::SocketAddr, sync::Arc, time::Duration};
use tokio::task;
use tonic::transport::Server;
use tonic_web::GrpcWebLayer;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let shutdown_ctrl = shutdown::Ctrl::new();

    let settings = configuration::Settings::new()?;
    let public_path = settings.server.public_path;

    watchog::install_default_tracer(&settings.log);
    let prometheus_handle = watchog::setup_metrics_recorder()?;
    tracing::info!("Successfully read settings and initialized tracing");

    let client = reqwest::Client::new();

    tracing::info!("Initializing auth context");
    let auth_ctx = gardevoir::AuthCtx::new(settings.gardevoir.clone()).await?;
    tracing::info!("Initialized auth context");

    tracing::info!(
        message = "Connecting to database",
        url = settings.database.url.as_ref()
    );
    let db_connection_pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&settings.database.url)
        .await?;
    tracing::info!("Connected to database");
    tracing::info!("Executing migrations");
    {
        let mut conn = db_connection_pool.acquire().await?;
        database::execute_migrations(&mut conn, database_macros::include_migrations!()).await?;
    }

    tracing::info!(message = "Connecting to pubsub server");
    let pubsub = pubsub::PgPubSub::new(&db_connection_pool, shutdown_ctrl.clone()).await?;
    tracing::info!("Connected to pubsub server");

    let mail_sender = gardevoir::build_mail_sender(&settings.mail)?;

    let tile_cache = Arc::new(TileCache::new(
        db_connection_pool.clone(),
        settings.smeargle.clone(),
    ));
    let warp_cache = WarpCache::new(db_connection_pool.clone(), settings.kangaskhan.warp_cache);
    let tileset_path = smeargle::write_tileset(&public_path, &settings.smeargle.tiles)?;
    smeargle::recreate_tilemap_images(&settings.smeargle.tiles, &public_path, &db_connection_pool)
        .await?;

    let player_channel = channels::PlayerChannel::new("player");
    let pokemon_channel = channels::PokemonChannel::new("pokemon");
    let item_channel = channels::ItemChannel::new("item");
    let map_channel = channels::MapChannel::new("map");

    let encounter_state = kangaskhan::EncounterState::new(
        &db_connection_pool,
        &pokemon_channel,
        shutdown_ctrl.clone(),
    )
    .await;

    let item_state =
        kangaskhan::ItemState::new(&db_connection_pool, &item_channel, shutdown_ctrl.clone()).await;

    let player_state =
        kangaskhan::PlayerState::new(&db_connection_pool, &player_channel, shutdown_ctrl.clone())
            .await;

    let arceus_signal_sender = {
        let db_connection_pool = db_connection_pool.clone();
        let pokemon_config = settings.pokemon.clone();
        let pokemon_channel = pokemon_channel.clone();
        let item_channel = item_channel.clone();
        let generation_settings = settings.arceus.clone();
        let tile_cache = tile_cache.clone();

        let rng_provider = RngProvider::new();

        let ctx = arceus::GenerationContext {
            db_connection_pool,
            pokemon_channel,
            item_channel,
            pokemon_config,
            tile_cache,
            generation_settings,
            rng_provider,
        };
        arceus::start_generator(ctx, shutdown_ctrl.clone())
    };

    let app_state = WebsocketCtx {
        player_channel: player_channel.clone(),
        pokemon_channel: pokemon_channel.clone(),
        item_channel: item_channel.clone(),
        map_channel: map_channel.clone(),
        settings: settings.kangaskhan,
        db_connection_pool: db_connection_pool.clone(),
        auth_ctx: auth_ctx.clone(),
        tile_cache: tile_cache.clone(),
        warp_cache: warp_cache.clone(),
        shutdown_ctrl: shutdown_ctrl.clone(),
        arceus_signal_sender: arceus_signal_sender.clone(),
        encounter_state,
        item_state,
        player_state,
        pokemon_config: settings.pokemon.clone(),
        rng: rng::RngProvider::new(),
    };

    let chatot_state = chatot::websocket::WebsocketCtx {
        pubsub: pubsub.clone(),
        db_connection_pool: db_connection_pool.clone(),
        auth_ctx: auth_ctx.clone(),
        shutdown_ctrl: shutdown_ctrl.clone(),
        settings: settings.chatot,
    };

    let gardevoir = gardevoir::Gardevoir::new(
        db_connection_pool.clone(),
        client.clone(),
        auth_ctx.clone(),
        mail_sender,
    );

    let app: Router = Router::new()
        .route("/kangaskhan/ws", get(websocket_handler))
        .route("/chatot/ws", get(chatot_websocket_handler))
        .nest_service(
            &public_path.to_string_lossy(),
            tower_http::services::ServeDir::new(public_path::os_path(&public_path)),
        )
        .with_state((app_state, chatot_state));

    let grpc_addr = SocketAddr::from(([0, 0, 0, 0], settings.server.grpc_port));
    tracing::info!("Listening on grpc port {}", grpc_addr);
    let grpc = Server::builder()
        .accept_http1(true)
        .layer(watchog::watchog_layer())
        .layer(watchog::PrometheusLayer::new(prometheus_handle))
        .layer(GrpcWebLayer::new())
        .add_service(kangaskhan::KangaskhanServiceServer::new(
            kangaskhan::Kangaskhan {
                auth_ctx: auth_ctx.clone(),
                pokemon: settings.pokemon.clone(),
                trainers: settings.trainer.clone(),
                db_connection_pool: db_connection_pool.clone(),
                player_channel,
                warp_cache: warp_cache.clone(),
            },
        ))
        .add_service(smeargle::SmeargleServiceServer::new(smeargle::Smeargle {
            auth_ctx: auth_ctx.clone(),
            settings: settings.smeargle.clone(),
            db_connection_pool: db_connection_pool.clone(),
            tileset_path,
            public_path: public_path.to_path_buf(),
            tile_cache: tile_cache.clone(),
        }))
        .add_service(chatot::ChatotServiceServer::new(chatot::Chatot {
            auth_ctx: auth_ctx.clone(),
            db_connection_pool: db_connection_pool.clone(),
            pubsub,
            settings: settings.chatot,
        }))
        .add_service(arceus::ArceusServiceServer::new(arceus::Arceus {
            auth_ctx: auth_ctx.clone(),
            db_connection_pool: db_connection_pool.clone(),
            signal_sender: arceus_signal_sender,
            settings: settings.arceus.clone(),
            translation_client: arceus::OpenAITranslationClient::new(
                &client,
                &settings.arceus.translation,
            ),
        }))
        .add_service(gardevoir::GardevoirServiceServer::new(gardevoir));

    let http_addr = SocketAddr::from(([0, 0, 0, 0], settings.server.http_port));
    tracing::info!("Listening on http port {}", http_addr);

    let http_task = {
        let shutdown_ctrl = shutdown_ctrl.clone();
        task::spawn(async move {
            let listener = tokio::net::TcpListener::bind(&http_addr)
                .await
                .expect("Failed to bind port");
            let _prevent_shutdown = shutdown_ctrl.prevent_shutdown("Http server");
            axum::serve(listener, app.into_make_service())
                .with_graceful_shutdown(async move {
                    shutdown_ctrl.cancelled().await;
                    tracing::debug!("Gracefully shutting down http task");
                })
                .await
                .unwrap();
            tracing::debug!("Gracefully shut down http task");
        })
    };

    let grpc_task = {
        let shutdown_ctrl = shutdown_ctrl.clone();
        task::spawn(async move {
            let _prevent_shutdown = shutdown_ctrl.prevent_shutdown("Grpc server");
            grpc.serve_with_shutdown(grpc_addr, async move {
                shutdown_ctrl.cancelled().await;
                tracing::debug!("Gracefully shutting down grpc task");
            })
            .await
            .unwrap();
            tracing::debug!("Gracefully shut down grpc task");
        })
    };

    tokio::select! {
        ctrl_c = tokio::signal::ctrl_c() => {
            match ctrl_c {
                Ok(()) => {
                    tracing::info!("Received shutdown request");
                }
                Err(error) => {
                    tracing::error!(%error, "Failed to set ctrl_c handler");
                }
            }
        }
        grpc_task_result = grpc_task => {
            match grpc_task_result {
                Ok(()) => {
                    tracing::warn!("Grpc server ended early.");
                }
                Err(error) => {
                    tracing::error!(%error, "Grpc server failed");
                }
            }
        }
        http_task_result = http_task => {
            match http_task_result {
                Ok(()) => {
                    tracing::warn!("Http server ended early.");
                }
                Err(error) => {
                    tracing::error!(%error, "Http server failed");
                }
            }
        }
    }

    shutdown_ctrl.shutdown((), Duration::from_secs(5)).await;
    db_connection_pool.close().await;

    tracing::info!("Server was gracefully shut down.");

    Ok(())
}

#[derive(Deserialize)]
struct WebsocketParams {
    token: String,
    map_id: Option<String>,
}

async fn websocket_handler<T>(
    Query(params): Query<WebsocketParams>,
    ws: WebSocketUpgrade,
    State((state, _)): State<(WebsocketCtx, T)>,
) -> Response {
    let Some(x_map_id) = params.map_id else {
        return (StatusCode::BAD_REQUEST, "Missing x-map-id query parameter").into_response();
    };
    let map_id = match x_map_id.parse::<i64>() {
        Err(error) => {
            tracing::debug!(%error, "Invalid or missing x-map-id query parameter");
            return (
                StatusCode::BAD_REQUEST,
                "Invalid or missing x-map-id query parameter",
            )
                .into_response();
        }
        Ok(map_id) => MapId::from(map_id),
    };
    let claims = match state.auth_ctx.decode_token(Some(map_id), &params.token) {
        Err(error) => {
            tracing::warn!(%error, %map_id, "Failed to decode token for kangaskhan websocket");
            return StatusCode::UNAUTHORIZED.into_response();
        }
        Ok(claims) => claims,
    };
    let Some(_) = claims.map_id() else {
        return StatusCode::UNAUTHORIZED.into_response();
    };

    ws.on_upgrade(move |socket| {
        let (sender, receiver) = socket.split();
        kangaskhan::websocket::websocket(sender, receiver, claims, state)
    })
    .into_response()
}

async fn chatot_websocket_handler<T>(
    Query(params): Query<WebsocketParams>,
    ws: WebSocketUpgrade,
    State((_, state)): State<(T, chatot::websocket::WebsocketCtx)>,
) -> Response {
    let claims = match state.auth_ctx.decode_token_without_map_id(&params.token) {
        Err(error) => {
            tracing::warn!(%error, "Failed to decode token for chatot websocket");
            return StatusCode::UNAUTHORIZED.into_response();
        }
        Ok(claims) => claims,
    };
    ws.on_upgrade(move |socket| async {
        chatot::websocket::websocket(socket, claims, state).await;
    })
    .into_response()
}
