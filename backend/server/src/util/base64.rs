use base64::Engine;

pub fn encode_64bit_id<I>(id: I) -> String
where
    i64: From<I>,
{
    let bytes = i64::from(id).to_be_bytes();
    base64::engine::general_purpose::URL_SAFE_NO_PAD.encode(bytes)
}

pub fn decode_64bit_id(base64: &str) -> Result<i64, base64::DecodeSliceError> {
    let mut output = [0u8; 8];
    base64::engine::general_purpose::URL_SAFE_NO_PAD.decode_slice(base64, &mut output)?;
    Ok(i64::from_be_bytes(output))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn roundtrip_0() {
        let result = decode_64bit_id(&encode_64bit_id(0i64));
        assert_eq!(result, Ok(0i64));
    }

    #[test]
    fn roundtrip_1234() {
        let result = decode_64bit_id(&encode_64bit_id(1234i64));
        assert_eq!(result, Ok(1234i64));
    }

    #[test]
    fn roundtrip_max() {
        let result = decode_64bit_id(&encode_64bit_id(i64::MAX));
        assert_eq!(result, Ok(i64::MAX));
    }

    #[test]
    fn roundtrip_min() {
        let result = decode_64bit_id(&encode_64bit_id(i64::MIN));
        assert_eq!(result, Ok(i64::MIN));
    }
}
