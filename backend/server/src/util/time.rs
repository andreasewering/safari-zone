use std::time::SystemTime;

use time::OffsetDateTime;

pub fn offset_date_time_into_timestamp(offset_date_time: OffsetDateTime) -> prost_types::Timestamp {
    let system_time = SystemTime::from(offset_date_time);
    prost_types::Timestamp::from(system_time)
}

pub fn timestamp_into_offset_date_time(
    timestamp: prost_types::Timestamp,
) -> Option<OffsetDateTime> {
    match SystemTime::try_from(timestamp) {
        Err(error) => {
            tracing::warn!(%error, "Failed to convert protobuf timestamp into SystemTime");
            None
        }
        Ok(time) => Some(OffsetDateTime::from(time)),
    }
}

#[cfg(test)]
pub fn compare_timestamps(
    timestamp_l: &prost_types::Timestamp,
    timestamp_r: &prost_types::Timestamp,
) -> std::cmp::Ordering {
    let second_comparison = timestamp_l.seconds.cmp(&timestamp_r.seconds);
    if second_comparison.is_ne() {
        return second_comparison;
    }
    timestamp_l.nanos.cmp(&timestamp_r.nanos)
}
