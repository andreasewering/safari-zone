use deepsize::DeepSizeOf;

pub fn deep_size_of_weigher<K, V>(k: &K, v: &V) -> u32
where
    K: DeepSizeOf,
    V: DeepSizeOf,
{
    (k.deep_size_of() + v.deep_size_of()) as u32
}