use core::panic;
use std::error::Error;
use std::fmt::{Debug, Display};
use std::sync::Arc;

use argon2::password_hash;

use crate::watchog::err_code::ErrorCode;

#[derive(Debug)]
pub struct AppError {
    source: anyhow::Error,
    code: ErrorCode,
    grpc_status: tonic::Code,
    location: &'static panic::Location<'static>,
}

impl PartialEq for AppError {
    fn eq(&self, other: &Self) -> bool {
        self.source.to_string() == other.source.to_string()
            && self.code == other.code
            && self.grpc_status == other.grpc_status
            && self.location == other.location
    }
}

impl Eq for AppError {}

impl<T> From<AppError> for Result<T, AppError> {
    fn from(value: AppError) -> Self {
        Err(value)
    }
}

impl Error for AppError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(self.source.as_ref())
    }
}

#[derive(Debug)]
pub struct DisplayError<D>(D);

impl<D> Error for DisplayError<D> where D: Display + Debug {}

impl<D> Display for DisplayError<D>
where
    D: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

pub type AppResult<T> = Result<T, AppError>;

pub trait Context<T> {
    fn context<D>(self, context: D) -> AppResult<T>
    where
        D: Display + Send + Sync + 'static;
}

impl<T, E> Context<T> for Result<T, E>
where
    AppError: From<E>,
{
    fn context<D>(self, context: D) -> AppResult<T>
    where
        D: Display + Send + Sync + 'static,
    {
        self.map_err(|err| AppError::from(err).context(context))
    }
}

macro_rules! define_error_constructor {
    ($name:ident, $grpc_status:expr) => {
        #[track_caller]
        pub fn $name<D>(message: D) -> Self
        where
            D: Display + Debug + Sync + Send + 'static,
        {
            Self {
                source: anyhow::anyhow!(message),
                code: ErrorCode::Unspecified,
                grpc_status: $grpc_status,
                location: panic::Location::caller(),
            }
        }
    };
}

impl AppError {
    fn context<D>(self, context: D) -> Self
    where
        D: Display + Send + Sync + 'static,
    {
        let source = self.source.context(context);
        Self { source, ..self }
    }

    pub fn set_error_code(mut self, code: ErrorCode) -> Self {
        self.code = code;
        self
    }

    define_error_constructor!(bad_request, tonic::Code::InvalidArgument);
    define_error_constructor!(internal, tonic::Code::Internal);
    define_error_constructor!(inconsistency, tonic::Code::FailedPrecondition);
    define_error_constructor!(not_found, tonic::Code::NotFound);
    define_error_constructor!(unavailable, tonic::Code::Unavailable);
    define_error_constructor!(unauthenticated, tonic::Code::Unauthenticated);
    define_error_constructor!(permission_denied, tonic::Code::PermissionDenied);
    define_error_constructor!(conflict, tonic::Code::AlreadyExists);
}

fn sqlx_status(sqlx_error: &sqlx::Error) -> (ErrorCode, tonic::Code) {
    match &sqlx_error {
        sqlx::Error::Database(wrapped_err) if wrapped_err.is_unique_violation() => {
            (ErrorCode::EntityAlreadyExists, tonic::Code::AlreadyExists)
        }
        sqlx::Error::Database(wrapped_err) if wrapped_err.is_foreign_key_violation() => {
            (ErrorCode::EntityDoesNotExist, tonic::Code::NotFound)
        }
        sqlx::Error::RowNotFound => (ErrorCode::EntityDoesNotExist, tonic::Code::NotFound),
        _ => (ErrorCode::DatabaseAccessFailed, tonic::Code::Internal),
    }
}

impl From<sqlx::Error> for AppError {
    #[track_caller]
    fn from(source: sqlx::Error) -> Self {
        let (code, grpc_status) = sqlx_status(&source);
        Self {
            source: source.into(),
            code,
            grpc_status,
            location: panic::Location::caller(),
        }
    }
}

impl From<Arc<sqlx::Error>> for AppError {
    #[track_caller]
    fn from(source: Arc<sqlx::Error>) -> Self {
        let (code, grpc_status) = sqlx_status(source.as_ref());
        Self {
            source: source.into(),
            code,
            grpc_status,
            location: panic::Location::caller(),
        }
    }
}

impl From<argon2::password_hash::Error> for AppError {
    #[track_caller]
    fn from(source: argon2::password_hash::Error) -> Self {
        let (code, grpc_status) = match &source {
            password_hash::Error::Password => {
                (ErrorCode::Unspecified, tonic::Code::Unauthenticated)
            }
            _ => (ErrorCode::Unspecified, tonic::Code::Internal),
        };
        Self {
            source: source.into(),
            code,
            grpc_status,
            location: panic::Location::caller(),
        }
    }
}

impl From<image::ImageError> for AppError {
    #[track_caller]
    fn from(source: image::ImageError) -> Self {
        Self {
            source: source.into(),
            code: ErrorCode::Unspecified,
            grpc_status: tonic::Code::Internal,
            location: panic::Location::caller(),
        }
    }
}

impl From<std::io::Error> for AppError {
    #[track_caller]
    fn from(source: std::io::Error) -> Self {
        Self {
            source: source.into(),
            code: ErrorCode::Unspecified,
            grpc_status: tonic::Code::Internal,
            location: panic::Location::caller(),
        }
    }
}

impl From<lettre::address::AddressError> for AppError {
    #[track_caller]
    fn from(source: lettre::address::AddressError) -> Self {
        Self {
            source: source.into(),
            code: ErrorCode::BadRequest,
            grpc_status: tonic::Code::InvalidArgument,
            location: panic::Location::caller(),
        }
    }
}

impl From<lettre::error::Error> for AppError {
    #[track_caller]
    fn from(source: lettre::error::Error) -> Self {
        Self {
            source: source.into(),
            code: ErrorCode::Unspecified,
            grpc_status: tonic::Code::FailedPrecondition,
            location: panic::Location::caller(),
        }
    }
}

impl From<lettre::transport::smtp::Error> for AppError {
    #[track_caller]
    fn from(source: lettre::transport::smtp::Error) -> Self {
        Self {
            source: source.into(),
            code: ErrorCode::UpstreamUnavailable,
            grpc_status: tonic::Code::Unavailable,
            location: panic::Location::caller(),
        }
    }
}

impl From<reqwest::Error> for AppError {
    #[track_caller]
    fn from(source: reqwest::Error) -> Self {
        let (code, grpc_status) = match source.status() {
            Some(status_code) => {
                if status_code.is_client_error() {
                    (ErrorCode::UpstreamFailedExpectation, tonic::Code::Internal)
                } else {
                    (ErrorCode::UpstreamUnavailable, tonic::Code::Internal)
                }
            }
            None => {
                if source.is_connect() || source.is_timeout() {
                    (ErrorCode::UpstreamUnavailable, tonic::Code::Internal)
                } else {
                    (ErrorCode::UpstreamUnknownError, tonic::Code::Internal)
                }
            }
        };
        Self {
            source: source.into(),
            code,
            grpc_status,
            location: panic::Location::caller(),
        }
    }
}

impl Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Error occurred: {}\nError Code: {}\ngRPC Status: {}\nLocation:\n{}",
            self.source.root_cause(),
            self.code.as_str_name(),
            self.grpc_status,
            self.location,
        )
    }
}

impl From<AppError> for tonic::Status {
    #[track_caller]
    fn from(error: AppError) -> Self {
        let error = error.context(panic::Location::caller());
        let grpc_status_code = error.grpc_status;
        tracing::error!(?error, "BLABLABLALABA");
        tracing::error!(message = "Unhandled error while handling grpc request.", %error );
        tonic::Status::new(grpc_status_code, error.code.to_response())
    }
}

impl From<protobuf_web_token::Error> for AppError {
    #[track_caller]
    fn from(source: protobuf_web_token::Error) -> Self {
        use protobuf_web_token::Error as E;
        let (code, grpc_status) = match &source {
            E::TokenExpired => (
                ErrorCode::AuthenticationTokenExpired,
                tonic::Code::Unauthenticated,
            ),
            _ => (
                ErrorCode::AuthenticationTokenCouldNotBeParsed,
                tonic::Code::Unauthenticated,
            ),
        };
        Self {
            source: source.into(),
            code,
            grpc_status,
            location: panic::Location::caller(),
        }
    }
}

impl From<jwtk::Error> for AppError {
    #[track_caller]
    fn from(source: jwtk::Error) -> Self {
        let location = panic::Location::caller();
        match source {
            jwtk::Error::Reqwest(request_error) => Self {
                source: jwtk::Error::Reqwest(request_error).into(),
                code: ErrorCode::UpstreamUnavailable,
                grpc_status: tonic::Code::Unavailable,
                location,
            },
            _ => Self {
                source: source.into(),
                code: ErrorCode::UpstreamFailedExpectation,
                grpc_status: tonic::Code::Internal,
                location,
            },
        }
    }
}
