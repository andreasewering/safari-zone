use crate::{
    data::{self, proto::tag, UserId},
    error::{AppError, AppResult},
    util::base64::decode_64bit_id,
};

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ChatMessage {
    text: String,
    tags: Vec<Located<Tag>>,
}

impl ChatMessage {
    pub fn new<S>(text: S) -> Self
    where
        S: Into<String>,
    {
        let text: String = text.into();

        let tags = parse(&text);

        Self { text, tags }
    }

    pub fn text(&self) -> &str {
        &self.text
    }

    pub fn into_tags(self) -> Vec<Located<Tag>> {
        self.tags
    }

    pub fn referenced_users(&self) -> Vec<Located<UserId>> {
        self.tags
            .iter()
            .filter_map(|tag| {
                if let Tag::User(user_id) = tag.tag {
                    Some(Located {
                        tag: user_id,
                        span: tag.span,
                    })
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn referenced_groups(&self) -> Vec<Located<i64>> {
        self.tags
            .iter()
            .filter_map(|tag| {
                if let Tag::Group(group_id) = tag.tag {
                    Some(Located {
                        tag: group_id,
                        span: tag.span,
                    })
                } else {
                    None
                }
            })
            .collect()
    }
}

#[derive(Clone, Copy)]
enum ParseState {
    EncounteredAt,
    Text,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Located<T> {
    tag: T,
    span: (u16, u16),
}

impl<T> Located<T> {
    pub fn tag(&self) -> &T {
        &self.tag
    }

    pub fn span<U>(&self) -> (U, U)
    where
        U: From<u16>,
    {
        (self.span.0.into(), self.span.1.into())
    }
}

fn parse(str: &str) -> Vec<Located<Tag>> {
    let mut parse_state = ParseState::Text;
    let mut tags: Vec<Located<Tag>> = Vec::new();

    let mut chars = str.char_indices();
    while let Some((index, char)) = chars.next() {
        let index = index as u16;
        match (parse_state, char) {
            (ParseState::Text, '@') => {
                parse_state = ParseState::EncounteredAt;
            }
            (ParseState::Text, _) => {}
            (ParseState::EncounteredAt, 'u') => {
                const BASE_64_USER_ID_SIZE: u16 = 11;
                let potential_id = &chars.as_str()[0..BASE_64_USER_ID_SIZE.into()];
                for _ in 0..BASE_64_USER_ID_SIZE {
                    chars.next();
                }
                if let Ok(user_id) = decode_64bit_id(potential_id) {
                    let tag = Tag::User(UserId::from(user_id));
                    let span = (index - 1, index + BASE_64_USER_ID_SIZE);
                    tags.push(Located { tag, span });
                }
            }
            (ParseState::EncounteredAt, 'g') => {
                const BASE_64_GROUP_ID_SIZE: u16 = 11;
                let potential_id = &chars.as_str()[0..BASE_64_GROUP_ID_SIZE.into()];
                for _ in 0..BASE_64_GROUP_ID_SIZE {
                    chars.next();
                }
                if let Ok(group_id) = decode_64bit_id(potential_id) {
                    let tag = Tag::Group(group_id);
                    let span = (index - 1, index + BASE_64_GROUP_ID_SIZE);
                    tags.push(Located { tag, span });
                }
            }
            (ParseState::EncounteredAt, _) => {
                parse_state = ParseState::Text;
            }
        }
    }

    tags
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Tag {
    User(UserId),
    Group(i64),
}

pub fn proto_from_tag(located_tag: Located<Tag>) -> data::proto::Tag {
    let (span_from, span_to) = located_tag.span();
    let kind = match located_tag.tag() {
        Tag::User(user_id) => tag::Kind::UserId(i64::from(*user_id)),
        Tag::Group(group_id) => tag::Kind::GroupId(*group_id),
    };
    data::proto::Tag {
        span_from,
        span_to,
        kind: Some(kind),
    }
}

pub async fn keep_valid_tags(
    pool: &sqlx::PgPool,
    tags: Vec<Located<Tag>>,
) -> AppResult<Vec<Located<Tag>>> {
    if tags.is_empty() {
        return Ok(tags);
    }

    let mut tag_kinds = Vec::new();
    let mut ids = Vec::new();
    let mut indices = Vec::new();

    for (idx, tag) in tags.iter().enumerate() {
        let Ok(idx) = i32::try_from(idx) else {
            return Err(AppError::bad_request("Too many tags"));
        };
        match tag.tag {
            Tag::Group(group_id) => {
                tag_kinds.push(0);
                ids.push(group_id);
                indices.push(idx);
            }
            Tag::User(user_id) => {
                tag_kinds.push(1);
                ids.push(user_id.into());
                indices.push(idx);
            }
        }
    }

    let invalid_indices = sqlx::query_scalar!(
        r#"
        SELECT i.idx as "idx!" FROM UNNEST($1::int[], $2::bigint[], $3::int[]) as i(tag_kind, id, idx)
        WHERE ( i.tag_kind = 0 AND NOT EXISTS ( SELECT 1 FROM groups g WHERE g.id = i.id ))
           OR ( i.tag_kind = 1 AND NOT EXISTS ( SELECT 1 FROM users u WHERE u.id = i.id ))
        ORDER BY i.idx ASC
           "#,
        &tag_kinds,
        &ids,
        &indices
    )
    .fetch_all(pool)
    .await?;

    let mut valid_tags = Vec::new();
    let mut curr_idx = 0;
    let mut tag_iter = tags.into_iter();

    for invalid_idx in invalid_indices.into_iter() {
        for tag in tag_iter.by_ref() {
            let is_invalid_tag = invalid_idx == curr_idx;
            curr_idx += 1;
            if is_invalid_tag {
                break;
            }
            valid_tags.push(tag);
        }
    }
    // push remaining tags
    valid_tags.extend(tag_iter);

    Ok(valid_tags)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{data::UserId, util::base64::encode_64bit_id};

    #[test]
    fn detects_tagged_user() {
        let user_id = UserId::from(1234i64);
        let message = ChatMessage::new(format!("Hi @u{}", encode_64bit_id(user_id)));
        assert_eq!(
            message.referenced_users(),
            vec![Located {
                tag: user_id,
                span: (3, 15)
            }]
        )
    }

    #[test]
    fn detects_tagged_user_even_with_no_whitespace_in_front() {
        let user_id = UserId::from(1234i64);
        let message = ChatMessage::new(format!("Hi@u{}", encode_64bit_id(user_id)));
        assert_eq!(
            message.referenced_users(),
            vec![Located {
                tag: user_id,
                span: (2, 14)
            }]
        )
    }

    #[test]
    fn detects_tagged_user_even_with_no_whitespace_after_it() {
        let user_id = UserId::from(1234i64);
        let message = ChatMessage::new(format!("Hi@u{}bla", encode_64bit_id(user_id)));
        assert_eq!(
            message.referenced_users(),
            vec![Located {
                tag: user_id,
                span: (2, 14)
            }]
        )
    }

    #[test]
    fn does_not_crash_for_user_id_that_isnt_one() {
        let user_id = UserId::from(1234i64);
        let encoded = encode_64bit_id(user_id);
        let close_enough = encoded[0..(encoded.len() - 1)].to_string();
        let message = ChatMessage::new(format!("Hi@u{close_enough}%"));
        assert_eq!(message.referenced_users(), vec![])
    }

    #[test]
    fn detects_tagged_group() {
        let group_id = 1234i64;
        let message = ChatMessage::new(format!("Hi @g{}", encode_64bit_id(group_id)));
        assert_eq!(
            message.referenced_groups(),
            vec![Located {
                tag: group_id,
                span: (3, 15)
            }]
        )
    }

    #[test]
    fn detects_tagged_group_even_with_no_whitespace_in_front() {
        let group_id = 1234i64;
        let message = ChatMessage::new(format!("Hi@g{}", encode_64bit_id(group_id)));
        assert_eq!(
            message.referenced_groups(),
            vec![Located {
                tag: group_id,
                span: (2, 14)
            }]
        )
    }

    #[test]
    fn detects_tagged_group_even_with_no_whitespace_after_it() {
        let group_id = 1234i64;
        let message = ChatMessage::new(format!("Hi@g{}bla", encode_64bit_id(group_id)));
        assert_eq!(
            message.referenced_groups(),
            vec![Located {
                tag: group_id,
                span: (2, 14)
            }]
        )
    }

    #[test]
    fn does_not_crash_for_group_id_that_isnt_one() {
        let group_id = 1234i64;
        let encoded = encode_64bit_id(group_id);
        let close_enough = encoded[0..(encoded.len() - 1)].to_string();
        let message = ChatMessage::new(format!("Hi@g{close_enough}%"));
        assert_eq!(message.referenced_groups(), vec![])
    }
}
