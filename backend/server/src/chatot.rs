pub mod proto {
    pub mod chatot {
        #![allow(clippy::style)]
        tonic::include_proto!("chatot");
    }
    use crate::data::proto as data;
}

mod api;
mod chat_queries;
mod config;
mod friend_queries;
mod group_queries;
mod pubsub;
pub mod websocket;

pub use api::Chatot;
pub use config::Settings;
pub use proto::chatot::chatot_service_server::ChatotServiceServer;

#[cfg(test)]
mod tests {}

#[cfg(test)]
mod test_setup {
    use super::Chatot;
    use super::proto;
    use super::proto::chatot::chatot_service_server::ChatotService as _;
    use super::proto::chatot::{
        AddFriendRequest, AddFriendResponse, AddToGroupRequest, AddToGroupResponse,
        CreateGroupRequest, CreateGroupResponse, GetChatMessagesRequest, GetChatMessagesResponse,
        GetFriendRequestsRequest, GetFriendRequestsResponse, GetFriendsRequest, GetFriendsResponse,
        GetGroupDetailRequest, GetGroupDetailResponse, GetGroupsRequest, GetGroupsResponse, Group,
        RemoveFriendRequest, RemoveFriendResponse, WebsocketMessageToClient,
        WebsocketMessageToServer, websocket_message_to_client, websocket_message_to_server,
    };
    use super::websocket::{WebsocketCtx, websocket};
    use crate::data::{self, UserId};
    use crate::gardevoir::{
        test_helper as gardevoir,
        test_helper::{LoggedInUser, authorized_request},
    };
    use crate::pubsub::PgPubSub;
    use crate::shutdown;
    use crate::util::encode_to_bytes;
    use crate::util::time::offset_date_time_into_timestamp;
    use axum::extract::{Query, State, WebSocketUpgrade};
    use axum::response::{IntoResponse as _, Response};
    use axum::routing::get;
    use futures::SinkExt as _;
    use futures::StreamExt as _;
    use prost::Message as _;
    use serde::{Deserialize, Serialize};
    use sqlx::PgPool;
    use std::future::IntoFuture as _;
    use std::net::{Ipv4Addr, SocketAddr};
    use std::ops::RangeInclusive;
    use std::time::Duration;
    use time::OffsetDateTime;
    use tokio::net::TcpStream;
    use tokio::time::timeout;
    use tokio_tungstenite::{MaybeTlsStream, WebSocketStream, tungstenite};

    pub struct Setup {
        ctx: WebsocketCtx,
        pub chatot: Chatot,
        addr: tokio::sync::OnceCell<SocketAddr>,
        ctrl: shutdown::Ctrl,
    }

    #[derive(Debug)]
    pub struct Connection {
        socket: WebSocketStream<MaybeTlsStream<TcpStream>>,
    }

    impl Connection {
        pub async fn wait_for_ping(&mut self) {
            while let Some(Ok(message)) = self.next().await {
                if let tungstenite::Message::Ping(_) = message {
                    return;
                };
            }

            panic!("Expected ping but the connection ended");
        }

        pub async fn wait_for_close(&mut self) -> Option<tungstenite::protocol::CloseFrame> {
            while let Some(Ok(message)) = self.next().await {
                if let tungstenite::Message::Close(close_frame) = message {
                    return close_frame;
                };
            }

            panic!("Expected close but the connection ended");
        }

        pub async fn wait_for_messages(
            &mut self,
            count: u8,
        ) -> Vec<websocket_message_to_client::Message> {
            let mut messages = Vec::new();
            for _ in 0..count {
                messages.push(self.wait_for_message().await);
            }
            messages
        }

        pub async fn wait_for_message(&mut self) -> websocket_message_to_client::Message {
            while let Some(Ok(message)) = self.next().await {
                let tungstenite::Message::Binary(mut binary) = message else {
                    continue;
                };
                return WebsocketMessageToClient::decode(&mut binary)
                    .expect("No decode error")
                    .message
                    .expect("Some message");
            }

            panic!("Expected message but the connection ended");
        }

        pub async fn assert_no_message(&mut self) {
            // Need to try multiple times because of pings.
            while let Ok(next) = timeout(Duration::from_secs(1), self.socket.next()).await {
                if let Some(Ok(tungstenite::Message::Binary(mut binary))) = next {
                    let message = WebsocketMessageToClient::decode(&mut binary)
                        .expect("No decode error")
                        .message
                        .expect("Some message");
                    panic!("expected not to get message but got {message:?}");
                }
            }
        }

        pub async fn close(&mut self) -> Result<(), tungstenite::Error> {
            self.socket.close(None).await
        }

        async fn next(&mut self) -> Option<Result<tungstenite::Message, tungstenite::Error>> {
            timeout(Duration::from_secs(10), self.socket.next())
                .await
                .expect("No timeout")
        }

        pub async fn send_private_message<S: Into<String>>(
            &mut self,
            to_user_id: UserId,
            message: S,
        ) -> Result<(), tungstenite::Error> {
            self.send(websocket_message_to_server::Message::SentMessage(
                websocket_message_to_server::SentMessage {
                    channel: Some(proto::chatot::Channel {
                        kind: Some(proto::chatot::channel::Kind::Private(to_user_id.into())),
                    }),
                    text: message.into(),
                },
            ))
            .await
        }

        pub async fn send_group_message<S: Into<String>>(
            &mut self,
            to_group_id: i64,
            message: S,
        ) -> Result<(), tungstenite::Error> {
            self.send(websocket_message_to_server::Message::SentMessage(
                websocket_message_to_server::SentMessage {
                    channel: Some(proto::chatot::Channel {
                        kind: Some(proto::chatot::channel::Kind::Group(to_group_id)),
                    }),
                    text: message.into(),
                },
            ))
            .await
        }

        pub async fn start_typing_in_private(
            &mut self,
            friend_id: UserId,
        ) -> Result<(), tungstenite::Error> {
            self.send(websocket_message_to_server::Message::StartedTyping(
                proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Private(friend_id.into())),
                },
            ))
            .await
        }

        pub async fn stop_typing_in_private(
            &mut self,
            friend_id: UserId,
        ) -> Result<(), tungstenite::Error> {
            self.send(websocket_message_to_server::Message::StoppedTyping(
                proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Private(friend_id.into())),
                },
            ))
            .await
        }

        pub async fn start_typing_in_group(
            &mut self,
            to_group_id: i64,
        ) -> Result<(), tungstenite::Error> {
            self.send(websocket_message_to_server::Message::StartedTyping(
                proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Group(to_group_id)),
                },
            ))
            .await
        }

        pub async fn stop_typing_in_group(
            &mut self,
            to_group_id: i64,
        ) -> Result<(), tungstenite::Error> {
            self.send(websocket_message_to_server::Message::StoppedTyping(
                proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Group(to_group_id)),
                },
            ))
            .await
        }

        async fn send(
            &mut self,
            message: websocket_message_to_server::Message,
        ) -> Result<(), tungstenite::Error> {
            let proto_msg = WebsocketMessageToServer {
                message: Some(message),
            };
            self.socket
                .send(tungstenite::Message::Binary(encode_to_bytes(proto_msg)))
                .await
        }
    }

    impl Setup {
        async fn connect_to_url(&self, url: &str) -> Result<Connection, tungstenite::Error> {
            let ctx = self.ctx.clone();
            // Lazy initialize websocket server to not slowdown tests that do not use it
            let addr = self.addr.get_or_init(move || minimal_ws_server(ctx)).await;
            let url = format!("ws://{addr}{url}");
            let (socket, _response) = tokio_tungstenite::connect_async(url).await?;
            Ok(Connection { socket })
        }

        pub async fn connect(&self, user: &LoggedInUser) -> Result<Connection, tungstenite::Error> {
            let token = user.access_token.clone();
            let url = format!("/ws?token={token}");
            self.connect_to_url(&url).await
        }

        pub async fn create_user(&self, name: &str) -> anyhow::Result<LoggedInUser> {
            gardevoir::UserBuilder::from_name(name)
                .create(&self.ctx.auth_ctx, &self.ctx.db_connection_pool)
                .await
        }

        pub async fn add_each_other_as_friends(
            &self,
            user_1: &LoggedInUser,
            user_2: &LoggedInUser,
        ) -> anyhow::Result<()> {
            self.chatot
                .add_friend(authorized_request(
                    AddFriendRequest {
                        user_id: user_1.claims().user_id().into(),
                    },
                    user_2,
                ))
                .await?;
            self.chatot
                .add_friend(authorized_request(
                    AddFriendRequest {
                        user_id: user_2.claims().user_id().into(),
                    },
                    user_1,
                ))
                .await?;
            Ok(())
        }

        pub async fn create_group(
            &self,
            creator: &LoggedInUser,
            group_name: &str,
        ) -> Result<Group, tonic::Status> {
            let CreateGroupResponse { created } = self
                .chatot
                .create_group(authorized_request(
                    CreateGroupRequest {
                        group_name: group_name.to_string(),
                    },
                    creator,
                ))
                .await?
                .into_inner();
            Ok(created.unwrap())
        }

        pub async fn add_to_group(
            &self,
            user: &LoggedInUser,
            new_member_id: UserId,
            group_id: i64,
        ) -> Result<AddToGroupResponse, tonic::Status> {
            let response = self
                .chatot
                .add_to_group(authorized_request(
                    AddToGroupRequest {
                        group_id,
                        user_id: new_member_id.into(),
                    },
                    user,
                ))
                .await?
                .into_inner();
            Ok(response)
        }

        pub async fn create_group_with_members(
            &self,
            creator: &LoggedInUser,
            group_name: &str,
            members: &[&LoggedInUser],
        ) -> anyhow::Result<i64> {
            let Group { group_id, .. } = self.create_group(creator, group_name).await?;

            for member in members {
                let user_id = member.id();
                self.chatot
                    .add_to_group(authorized_request(
                        AddToGroupRequest { user_id, group_id },
                        creator,
                    ))
                    .await?;
            }

            Ok(group_id)
        }

        pub async fn add_friend(
            &self,
            user: &LoggedInUser,
            friend_id: UserId,
        ) -> Result<AddFriendResponse, tonic::Status> {
            let response = self
                .chatot
                .add_friend(authorized_request(
                    AddFriendRequest {
                        user_id: friend_id.into(),
                    },
                    user,
                ))
                .await?;
            Ok(response.into_inner())
        }

        pub async fn remove_friend(
            &self,
            user: &LoggedInUser,
            friend_id: UserId,
        ) -> Result<RemoveFriendResponse, tonic::Status> {
            let response = self
                .chatot
                .remove_friend(authorized_request(
                    RemoveFriendRequest {
                        user_id: friend_id.into(),
                    },
                    user,
                ))
                .await?;
            Ok(response.into_inner())
        }

        pub async fn get_friend_requests(
            &self,
            user: &LoggedInUser,
        ) -> Result<GetFriendRequestsResponse, tonic::Status> {
            let response = self
                .chatot
                .get_friend_requests(authorized_request(GetFriendRequestsRequest {}, user))
                .await?;
            Ok(response.into_inner())
        }

        pub async fn get_friends(
            &self,
            user: &LoggedInUser,
        ) -> Result<GetFriendsResponse, tonic::Status> {
            let response = self
                .chatot
                .get_friends(authorized_request(GetFriendsRequest {}, user))
                .await?;
            Ok(response.into_inner())
        }

        pub async fn get_groups(
            &self,
            user: &LoggedInUser,
        ) -> Result<GetGroupsResponse, tonic::Status> {
            let response = self
                .chatot
                .get_groups(authorized_request(GetGroupsRequest {}, user))
                .await?;
            Ok(response.into_inner())
        }

        pub async fn get_group_detail(
            &self,
            user: &LoggedInUser,
            group_id: i64,
        ) -> Result<GetGroupDetailResponse, tonic::Status> {
            let response = self
                .chatot
                .get_group_detail(authorized_request(GetGroupDetailRequest { group_id }, user))
                .await?;
            Ok(response.into_inner())
        }

        pub fn get_chat_messages(&self) -> GetChatMessagesBuilder {
            GetChatMessagesBuilder {
                setup: self,
                since: None,
                until: None,
                limit: None,
                friend_id: None,
                group_id: None,
            }
        }

        pub fn modify_settings<F>(&mut self, mut f: F)
        where
            F: FnMut(&mut super::Settings),
        {
            f(&mut self.ctx.settings);
            f(&mut self.chatot.settings);
        }

        pub async fn shutdown(self) -> anyhow::Result<()> {
            self.ctrl.shutdown((), Duration::from_secs(1)).await;
            Ok(())
        }
    }

    pub struct GetChatMessagesBuilder<'a> {
        setup: &'a Setup,
        since: Option<OffsetDateTime>,
        until: Option<OffsetDateTime>,
        limit: Option<u32>,
        friend_id: Option<UserId>,
        group_id: Option<i64>,
    }

    impl GetChatMessagesBuilder<'_> {
        pub fn since(self, since: OffsetDateTime) -> Self {
            Self {
                since: Some(since),
                ..self
            }
        }
        pub fn until(self, until: OffsetDateTime) -> Self {
            Self {
                until: Some(until),
                ..self
            }
        }
        pub fn limit(self, limit: u32) -> Self {
            Self {
                limit: Some(limit),
                ..self
            }
        }
        pub fn friend_id(self, user_id: UserId) -> Self {
            Self {
                friend_id: Some(user_id),
                ..self
            }
        }
        pub fn group_id(self, group_id: i64) -> Self {
            Self {
                group_id: Some(group_id),
                ..self
            }
        }
        pub async fn send(
            self,
            user: &LoggedInUser,
        ) -> Result<GetChatMessagesResponse, tonic::Status> {
            let response = self
                .setup
                .chatot
                .get_chat_messages(authorized_request(
                    GetChatMessagesRequest {
                        since: self.since.map(offset_date_time_into_timestamp),
                        until: self.until.map(offset_date_time_into_timestamp),
                        limit: self.limit,
                        friend_id: self.friend_id.map(i64::from),
                        group_id: self.group_id,
                    },
                    user,
                ))
                .await?;
            Ok(response.into_inner())
        }
    }

    pub async fn setup(pool: &PgPool) -> anyhow::Result<Setup> {
        let auth_ctx = gardevoir::create_auth_ctx().await?;
        let shutdown_ctrl = shutdown::Ctrl::new();
        let pubsub = PgPubSub::new(pool, shutdown_ctrl.clone()).await?;
        let settings = super::config::Settings::read()?;

        let ctx = WebsocketCtx {
            pubsub: pubsub.clone(),
            db_connection_pool: pool.clone(),
            auth_ctx: auth_ctx.clone(),
            shutdown_ctrl: shutdown_ctrl.clone(),
            settings,
        };

        let chatot = Chatot {
            auth_ctx,
            pubsub,
            db_connection_pool: pool.clone(),
            settings,
        };
        let addr = tokio::sync::OnceCell::new();

        Ok(Setup {
            addr,
            ctx,
            chatot,
            ctrl: shutdown_ctrl,
        })
    }

    pub fn user_tag(user_id: UserId, span: RangeInclusive<u32>) -> data::proto::Tag {
        data::proto::Tag {
            kind: Some(data::proto::tag::Kind::UserId(user_id.into())),
            span_from: *span.start(),
            span_to: *span.end(),
        }
    }

    pub fn group_tag(group_id: i64, span: RangeInclusive<u32>) -> data::proto::Tag {
        data::proto::Tag {
            kind: Some(data::proto::tag::Kind::GroupId(group_id)),
            span_from: *span.start(),
            span_to: *span.end(),
        }
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct WsQueryParams {
        token: String,
    }

    async fn minimal_ws_server(ctx: WebsocketCtx) -> SocketAddr {
        let shutdown_ctrl = ctx.shutdown_ctrl.clone();

        async fn ws(
            Query(params): Query<WsQueryParams>,
            upgrade: WebSocketUpgrade,
            State(state): State<WebsocketCtx>,
        ) -> Response {
            let Ok(claims) = state.auth_ctx.decode_token_without_map_id(&params.token) else {
                tracing::debug!("Invalid token");
                return http::StatusCode::BAD_REQUEST.into_response();
            };
            upgrade
                .on_upgrade(move |socket| websocket(socket, claims, state))
                .into_response()
        }
        let app: axum::Router = axum::Router::new().route("/ws", get(ws)).with_state(ctx);
        let listener = tokio::net::TcpListener::bind(SocketAddr::from((Ipv4Addr::UNSPECIFIED, 0)))
            .await
            .unwrap();
        let addr = listener.local_addr().unwrap();

        tokio::spawn(
            axum::serve(listener, app)
                .with_graceful_shutdown(async move {
                    shutdown_ctrl.cancelled().await;
                })
                .into_future(),
        );
        addr
    }
}

#[cfg(test)]
mod api_tests {
    use super::proto::chatot::chatot_service_server::ChatotService as _;
    use super::proto::chatot::{
        Friend, GetFriendRequestsResponse, GetFriendsResponse, GetGroupDetailResponse,
        GetGroupsResponse, Group, GroupMember, MarkFriendAsFavoriteRequest, group_member::Role,
    };
    use super::test_setup::setup;
    use crate::chat_message::ChatMessage;
    use crate::chatot::chat_queries;
    use crate::chatot::proto::chatot as proto;
    use crate::chatot::proto::chatot::{
        FriendChannelMessages, GetChatMessagesResponse, ParseChatMessageRequest,
        ParseChatMessageResponse,
    };
    use crate::chatot::test_setup::{group_tag, user_tag};
    use crate::gardevoir::test_helper::authorized_request;
    use crate::util::base64::encode_64bit_id;
    use crate::util::time::{compare_timestamps, offset_date_time_into_timestamp};
    use itertools::Itertools;
    use sqlx::PgPool;
    use std::cmp::Ordering;
    use std::collections::HashMap;
    use time::OffsetDateTime;
    use tonic::Code;

    #[database_macros::test]
    async fn accept_friend_request(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        // we also create a "other" user here as a sanity check - in reality there would be lots of other unaffected users
        let requester = setup.create_user("requester").await?;
        let accepter = setup.create_user("accepter").await?;
        let _other = setup.create_user("other").await?;
        // requester adds accepter as a friend
        setup.add_friend(&requester, accepter.id()).await?;

        // requester should have one outgoing friend request
        let GetFriendRequestsResponse { incoming, outgoing } =
            setup.get_friend_requests(&requester).await?;
        assert_eq!(incoming, vec![]);
        assert_eq!(outgoing.len(), 1);
        let outgoing_friend_request = &outgoing[0];
        assert_eq!(outgoing_friend_request.user_id, accepter.claims().user_id());
        assert_eq!(outgoing_friend_request.user_name, accepter.display_name);
        let now = offset_date_time_into_timestamp(OffsetDateTime::now_utc());
        assert_eq!(
            compare_timestamps(outgoing_friend_request.issued_at.as_ref().unwrap(), &now),
            Ordering::Less
        );

        // requester does not have any friends
        let GetFriendsResponse { friends } = setup.get_friends(&requester).await?;
        assert_eq!(friends, vec![]);

        // accepter should have one incoming friend request
        let GetFriendRequestsResponse { incoming, outgoing } =
            setup.get_friend_requests(&accepter).await?;
        assert_eq!(outgoing, vec![]);
        assert_eq!(incoming.len(), 1);
        let incoming_friend_request = &incoming[0];
        assert_eq!(
            incoming_friend_request.user_id,
            requester.claims().user_id()
        );
        assert_eq!(incoming_friend_request.user_name, requester.display_name);
        assert_eq!(
            incoming_friend_request.issued_at,
            outgoing_friend_request.issued_at
        );

        // accepter does not have any friends
        let GetFriendsResponse { friends } = setup.get_friends(&accepter).await?;
        assert_eq!(friends, vec![]);

        // accepter adds requester as a friend, which should essentially accept the friend request
        setup.add_friend(&accepter, requester.id()).await?;

        // requester does not have any outgoing friend requests
        let GetFriendRequestsResponse { incoming, outgoing } =
            setup.get_friend_requests(&requester).await?;
        assert_eq!(incoming, vec![]);
        assert_eq!(outgoing, vec![]);

        // requester now has one friend
        let GetFriendsResponse { friends } = setup.get_friends(&requester).await?;
        assert_eq!(friends.len(), 1);
        let requester_friend = &friends[0];
        assert_eq!(requester_friend.user_id, accepter.claims().user_id());
        assert_eq!(requester_friend.user_name, accepter.display_name);
        assert!(!requester_friend.favorite);
        assert_eq!(
            compare_timestamps(requester_friend.since.as_ref().unwrap(), &now),
            Ordering::Greater
        );

        // accepter does not have any incoming friend requests
        let GetFriendRequestsResponse { incoming, outgoing } =
            setup.get_friend_requests(&accepter).await?;
        assert_eq!(incoming, vec![]);
        assert_eq!(outgoing, vec![]);

        // accepter now has one friend
        let GetFriendsResponse { friends } = setup.get_friends(&accepter).await?;
        assert_eq!(friends.len(), 1);
        let accepter_friend = &friends[0];
        assert_eq!(accepter_friend.user_id, requester.claims().user_id());
        assert_eq!(accepter_friend.user_name, requester.display_name);
        assert!(!accepter_friend.favorite);
        assert_eq!(accepter_friend.since, requester_friend.since);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn mark_friend_as_favorite(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        let user_3 = setup.create_user("user_3").await?;

        setup.add_each_other_as_friends(&user_1, &user_2).await?;
        setup.add_each_other_as_friends(&user_2, &user_3).await?;
        setup.add_each_other_as_friends(&user_1, &user_3).await?;

        // user_1 marks user_2 as a favorite friend
        setup
            .chatot
            .mark_friend_as_favorite(authorized_request(
                MarkFriendAsFavoriteRequest {
                    user_id: user_2.claims().user_id().into(),
                    favorite: true,
                },
                &user_1,
            ))
            .await?;

        // user_1 now has two friends: user_2 which is marked as a favorite and user_3 which is not
        let GetFriendsResponse { friends } = setup.get_friends(&user_1).await?;
        assert_eq!(friends.len(), 2);
        let friend_map: HashMap<i64, Friend> = friends
            .into_iter()
            .map(|friend| (friend.user_id, friend))
            .collect();
        let user_2_friend = friend_map.get(&user_2.claims().user_id().into()).unwrap();
        let user_3_friend = friend_map.get(&user_3.claims().user_id().into()).unwrap();

        assert_eq!(user_2_friend.user_name, user_2.display_name);
        assert_eq!(user_3_friend.user_name, user_3.display_name);
        assert!(user_2_friend.favorite);
        assert!(!user_3_friend.favorite);

        // user_2 has neither user_1 and user_3 marked as favorite
        let GetFriendsResponse { friends } = setup.get_friends(&user_2).await?;
        assert_eq!(friends.len(), 2);
        let friend_map: HashMap<i64, Friend> = friends
            .into_iter()
            .map(|friend| (friend.user_id, friend))
            .collect();
        let user_1_friend = friend_map.get(&user_1.claims().user_id().into()).unwrap();
        let user_3_friend = friend_map.get(&user_3.claims().user_id().into()).unwrap();

        assert_eq!(user_1_friend.user_name, user_1.display_name);
        assert_eq!(user_3_friend.user_name, user_3.display_name);
        assert!(!user_1_friend.favorite);
        assert!(!user_3_friend.favorite);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn delete_friend_request(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let requester = setup.create_user("requester").await?;
        let rejecter = setup.create_user("rejecter").await?;

        // requester adds rejecter as a friend
        setup.add_friend(&requester, rejecter.id()).await?;

        // rejecter rejects friend request
        setup.remove_friend(&rejecter, requester.id()).await?;

        // rejecter does not have friend requests anymore
        let GetFriendRequestsResponse { incoming, outgoing } =
            setup.get_friend_requests(&rejecter).await?;
        assert_eq!(incoming, vec![]);
        assert_eq!(outgoing, vec![]);

        // requester does not have friend requests anymore
        let GetFriendRequestsResponse { incoming, outgoing } =
            setup.get_friend_requests(&requester).await?;
        assert_eq!(incoming, vec![]);
        assert_eq!(outgoing, vec![]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn delete_friend(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let requester = setup.create_user("requester").await?;
        let rejecter = setup.create_user("rejecter").await?;

        setup
            .add_each_other_as_friends(&requester, &rejecter)
            .await?;

        // rejecter removes previously accepted friend
        setup.remove_friend(&rejecter, requester.id()).await?;

        // rejecter does not have friends anymore
        let GetFriendsResponse { friends } = setup.get_friends(&rejecter).await?;
        assert!(friends[0].deleted_at.is_some());

        // requester does not friends anymore
        let GetFriendsResponse { friends } = setup.get_friends(&requester).await?;
        assert!(friends[0].deleted_at.is_some());

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn add_friend_twice(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let requester = setup.create_user("requester").await?;
        let accepter = setup.create_user("accepter").await?;

        setup
            .add_each_other_as_friends(&requester, &accepter)
            .await?;
        let GetFriendsResponse { friends } = setup.get_friends(&requester).await?;

        // No error
        setup.add_friend(&requester, accepter.id()).await?;
        // Friendship stays untouched
        let GetFriendsResponse {
            friends: friends_after_double_add,
        } = setup.get_friends(&requester).await?;
        assert_eq!(friends, friends_after_double_add);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn delete_friend_twice(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let requester = setup.create_user("requester").await?;
        let rejecter = setup.create_user("rejecter").await?;

        setup
            .add_each_other_as_friends(&requester, &rejecter)
            .await?;

        // rejecter removes previously accepted friend twice
        setup.remove_friend(&rejecter, requester.id()).await?;
        // No error even though friend was already removed
        setup.remove_friend(&rejecter, requester.id()).await?;

        setup.shutdown().await
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn create_group(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let creator = setup.create_user("creator").await?;

        let created_group = setup.create_group(&creator, "mygroup").await?;
        let GetGroupsResponse { groups } = setup.get_groups(&creator).await?;
        let [group]: [Group; 1] = groups.try_into().unwrap();
        assert_eq!(group.group_id, created_group.group_id);
        assert_eq!(group.group_name, "mygroup");
        assert_eq!(group.no_of_group_members, 1);
        assert_eq!(group.created_by_name, "creator");
        assert_eq!(group.created_by_user_id, creator.claims().user_id());
        assert!(!group.favorite);

        let GetGroupDetailResponse {
            group_name,
            favorite,
            members,
        } = setup
            .get_group_detail(&creator, created_group.group_id)
            .await?;
        assert_eq!(group_name, "mygroup");
        assert!(!favorite);
        let [
            GroupMember {
                user_id,
                user_name,
                role,
                since,
                deleted_at,
                last_seen,
                is_online,
            },
        ]: [GroupMember; 1] = members.try_into().unwrap();
        assert_eq!(user_id, creator.claims().user_id());
        assert_eq!(user_name, "creator");
        assert_eq!(role, Role::Creator as i32);
        let now = offset_date_time_into_timestamp(OffsetDateTime::now_utc());
        assert_eq!(compare_timestamps(&since.unwrap(), &now), Ordering::Less);
        assert_eq!(deleted_at, None);
        assert_eq!(last_seen, None);
        assert!(!is_online);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn create_group_name_too_short(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let creator = setup.create_user("creator").await?;

        let status = setup
            .create_group(&creator, "123")
            .await
            .expect_err("Group name should be too short");
        assert_eq!(status.code(), Code::InvalidArgument);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn add_user_to_group(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let creator = setup.create_user("creator").await?;
        let member = setup.create_user("member").await?;

        let created_group = setup.create_group(&creator, "mygroup").await?;

        // Creator adds member to group
        setup
            .add_to_group(&creator, member.id(), created_group.group_id)
            .await?;

        // The group now has two members and both users can access the group
        let creator_groups = setup.get_groups(&creator).await?;
        let member_groups = setup.get_groups(&member).await?;
        assert_eq!(creator_groups, member_groups);
        assert_eq!(creator_groups.groups[0].no_of_group_members, 2);

        let group_detail = setup
            .get_group_detail(&member, created_group.group_id)
            .await?;
        let members: HashMap<i64, GroupMember> = group_detail
            .members
            .into_iter()
            .map(|member| (member.user_id, member))
            .collect();
        let creator_member = members.get(&creator.claims().user_id().into()).unwrap();
        let member_member = members.get(&member.claims().user_id().into()).unwrap();
        assert_eq!(creator_member.user_name, "creator");
        assert_eq!(creator_member.role, Role::Creator as i32);
        assert_eq!(member_member.user_name, "member");
        assert_eq!(member_member.role, Role::Member as i32);
        assert_eq!(
            compare_timestamps(
                creator_member.since.as_ref().unwrap(),
                member_member.since.as_ref().unwrap()
            ),
            Ordering::Less
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn parse_basic_chat_message(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user = setup.create_user("user").await?;
        let text = "A completely normal message".to_string();
        let ParseChatMessageResponse { tags } = setup
            .chatot
            .parse_chat_message(authorized_request(ParseChatMessageRequest { text }, &user))
            .await?
            .into_inner();
        assert_eq!(&tags, &[]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn parse_chat_message_with_invalid_tag(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user = setup.create_user("user").await?;
        let text = "A message tagging a non-existing user @u123456789ab".to_string();
        let ParseChatMessageResponse { tags } = setup
            .chatot
            .parse_chat_message(authorized_request(ParseChatMessageRequest { text }, &user))
            .await?
            .into_inner();
        assert_eq!(&tags, &[]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn parse_chat_message_with_valid_tag(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user = setup.create_user("user").await?;
        let tagged_user = setup.create_user("tagged").await?;
        let text = format!(
            "A message tagging a non-existing user @u{}",
            encode_64bit_id(tagged_user.id::<i64>())
        );
        let ParseChatMessageResponse { tags } = setup
            .chatot
            .parse_chat_message(authorized_request(ParseChatMessageRequest { text }, &user))
            .await?
            .into_inner();
        assert_eq!(&tags, &[user_tag(tagged_user.id(), 38..=50)]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn parse_chat_message_with_multiple_tags(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user = setup.create_user("user").await?;
        let tagged_user = setup.create_user("tagged").await?;
        let group_id = setup.create_group(&user, "mygroup").await?.group_id;

        let text = format!(
            "user @u{}, group @g{} same group again @g{}",
            encode_64bit_id(tagged_user.id::<i64>()),
            encode_64bit_id(group_id),
            encode_64bit_id(group_id)
        );
        let ParseChatMessageResponse { tags } = setup
            .chatot
            .parse_chat_message(authorized_request(ParseChatMessageRequest { text }, &user))
            .await?
            .into_inner();
        assert_eq!(
            &tags,
            &[
                user_tag(tagged_user.id(), 5..=17),
                group_tag(group_id, 26..=38),
                group_tag(group_id, 57..=69)
            ]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn parse_chat_message_with_swapped_tags(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user = setup.create_user("user").await?;
        let tagged_user = setup.create_user("tagged").await?;
        let group_id = setup.create_group(&user, "mygroup").await?.group_id;

        let text = format!(
            "user @g{}, group @u{} same group again @g{}",
            encode_64bit_id(tagged_user.id::<i64>()),
            encode_64bit_id(group_id),
            encode_64bit_id(group_id)
        );
        let ParseChatMessageResponse { tags } = setup
            .chatot
            .parse_chat_message(authorized_request(ParseChatMessageRequest { text }, &user))
            .await?
            .into_inner();
        assert_eq!(&tags, &[group_tag(group_id, 57..=69)]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_empty(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user = setup.create_user("user").await?;

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&user).await?;
        assert_eq!(&friend_messages, &[]);
        assert_eq!(&group_messages, &[]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_single_message(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let sender = setup.create_user("sender").await?;
        let receiver = setup.create_user("receiver").await?;
        setup.add_each_other_as_friends(&sender, &receiver).await?;
        chat_queries::send_private_message(
            &pool,
            sender.id(),
            receiver.id(),
            ChatMessage::new("Hi dude"),
        )
        .await?;
        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&sender).await?;
        assert_eq!(&group_messages, &[]);
        let [
            FriendChannelMessages {
                messages,
                friend_id,
            },
        ] = friend_messages.try_into().unwrap();
        assert_eq!(friend_id, receiver.id::<i64>());

        let proto::ChatMessage {
            text,
            from_user,
            sent_at,
            updated_at,
            tags,
        } = &messages[0];
        assert_eq!(text, "Hi dude");
        assert_eq!(*from_user, sender.id::<i64>());
        assert!(sent_at.is_some());
        assert_eq!(updated_at, &None);
        assert_eq!(tags, &[]);

        // the receiver should also see the message
        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&receiver).await?;
        assert_eq!(&group_messages, &[]);
        let [
            FriendChannelMessages {
                messages,
                friend_id,
            },
        ] = friend_messages.try_into().unwrap();
        assert_eq!(friend_id, sender.id::<i64>());

        let proto::ChatMessage {
            text,
            from_user,
            sent_at,
            updated_at,
            tags,
        } = &messages[0];
        assert_eq!(text, "Hi dude");
        assert_eq!(*from_user, sender.id::<i64>());
        assert!(sent_at.is_some());
        assert_eq!(updated_at, &None);
        assert_eq!(tags, &[]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_old_message(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let sender = setup.create_user("sender").await?;
        let receiver = setup.create_user("receiver").await?;
        setup.add_each_other_as_friends(&sender, &receiver).await?;
        chat_queries::send_private_message(
            &pool,
            sender.id(),
            receiver.id(),
            ChatMessage::new("Hi dude"),
        )
        .await?;
        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup
            .get_chat_messages()
            .since(OffsetDateTime::now_utc())
            .send(&sender)
            .await?;
        assert_eq!(&friend_messages, &[]);
        assert_eq!(&group_messages, &[]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_new_message(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let sender = setup.create_user("sender").await?;
        let receiver = setup.create_user("receiver").await?;
        setup.add_each_other_as_friends(&sender, &receiver).await?;
        let time_before_message = OffsetDateTime::now_utc();
        chat_queries::send_private_message(
            &pool,
            sender.id(),
            receiver.id(),
            ChatMessage::new("Hi dude"),
        )
        .await?;

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup
            .get_chat_messages()
            .until(OffsetDateTime::now_utc())
            .send(&sender)
            .await?;
        assert_eq!(friend_messages.len(), 1);
        assert_eq!(&group_messages, &[]);

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup
            .get_chat_messages()
            .until(time_before_message)
            .send(&sender)
            .await?;
        assert_eq!(&friend_messages, &[]);
        assert_eq!(&group_messages, &[]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_with_explicit_limit(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let sender = setup.create_user("sender").await?;
        let receiver = setup.create_user("receiver").await?;
        setup.add_each_other_as_friends(&sender, &receiver).await?;
        for i in 0..10 {
            chat_queries::send_private_message(
                &pool,
                sender.id(),
                receiver.id(),
                ChatMessage::new(i.to_string()),
            )
            .await?;
        }

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().limit(4).send(&sender).await?;
        assert_eq!(&group_messages, &[]);
        assert_eq!(
            &friend_messages
                .iter()
                .flat_map(|messages| &messages.messages)
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &["9", "8", "7", "6"]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_with_implicit_limit(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.chatot.settings.max_chat_messages_limit = 2;

        let sender = setup.create_user("sender").await?;
        let receiver = setup.create_user("receiver").await?;
        setup.add_each_other_as_friends(&sender, &receiver).await?;
        for i in 0..10 {
            chat_queries::send_private_message(
                &pool,
                sender.id(),
                receiver.id(),
                ChatMessage::new(i.to_string()),
            )
            .await?;
        }

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&sender).await?;
        assert_eq!(&group_messages, &[]);
        assert_eq!(
            &friend_messages
                .iter()
                .flat_map(|messages| &messages.messages)
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &["9", "8"]
        );

        // Explicit limit does not beat configured limit
        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().limit(4).send(&sender).await?;
        assert_eq!(&group_messages, &[]);
        assert_eq!(
            &friend_messages
                .iter()
                .flat_map(|messages| &messages.messages)
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &["9", "8"]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_from_multiple_channels(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user = setup.create_user("user").await?;
        let friend_1 = setup.create_user("friend_1").await?;
        let friend_2 = setup.create_user("friend_2").await?;
        let group_member_1 = setup.create_user("group_member_1").await?;
        let group_member_2 = setup.create_user("group_member_2").await?;

        setup.add_each_other_as_friends(&user, &friend_1).await?;
        setup.add_each_other_as_friends(&user, &friend_2).await?;
        let group_id_1 = setup
            .create_group_with_members(&user, "groupname_1", &[&group_member_1, &group_member_2])
            .await?;
        let group_id_2 = setup
            .create_group_with_members(&user, "groupname_2", &[&group_member_2])
            .await?;

        // 3 messages in friend_1 <-> user
        chat_queries::send_private_message(&pool, friend_1.id(), user.id(), ChatMessage::new("1"))
            .await?;
        chat_queries::send_private_message(&pool, user.id(), friend_1.id(), ChatMessage::new("2"))
            .await?;
        chat_queries::send_private_message(&pool, user.id(), friend_1.id(), ChatMessage::new("3"))
            .await?;
        // 1 message in friend_2 <-> user
        chat_queries::send_private_message(&pool, friend_2.id(), user.id(), ChatMessage::new("4"))
            .await?;
        // 4 messages in group chat 1
        chat_queries::send_group_message(
            &pool,
            group_member_1.id(),
            group_id_1,
            ChatMessage::new("5"),
        )
        .await?;
        chat_queries::send_group_message(
            &pool,
            group_member_2.id(),
            group_id_1,
            ChatMessage::new("6"),
        )
        .await?;
        chat_queries::send_group_message(&pool, user.id(), group_id_1, ChatMessage::new("7"))
            .await?;
        chat_queries::send_group_message(
            &pool,
            group_member_1.id(),
            group_id_1,
            ChatMessage::new("8"),
        )
        .await?;
        // 1 message in group chat 2
        chat_queries::send_group_message(
            &pool,
            group_member_2.id(),
            group_id_2,
            ChatMessage::new("9"),
        )
        .await?;

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().limit(2).send(&user).await?;

        let (friend_1_messages, friend_2_messages) = friend_messages
            .into_iter()
            .sorted_by_key(|messages| messages.friend_id == friend_2.id::<i64>())
            .collect_tuple()
            .expect("2 private channels");

        assert_eq!(friend_1_messages.friend_id, friend_1.id::<i64>());
        assert_eq!(friend_2_messages.friend_id, friend_2.id::<i64>());
        assert_eq!(
            friend_1_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            ["3", "2"]
        );
        assert_eq!(
            friend_2_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            ["4"]
        );

        let (group_1_messages, group_2_messages) = group_messages
            .into_iter()
            .sorted_by_key(|messages| messages.group_id == group_id_2)
            .collect_tuple()
            .expect("2 group channels");
        assert_eq!(group_1_messages.group_id, group_id_1);
        assert_eq!(group_2_messages.group_id, group_id_2);
        assert_eq!(
            group_1_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            ["8", "7"]
        );
        assert_eq!(
            group_2_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            ["9"]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_just_for_group(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user = setup.create_user("user").await?;
        let friend_1 = setup.create_user("friend_1").await?;
        let group_member_1 = setup.create_user("group_member_1").await?;
        let group_member_2 = setup.create_user("group_member_2").await?;

        setup.add_each_other_as_friends(&user, &friend_1).await?;
        let group_id_1 = setup
            .create_group_with_members(&user, "groupname_1", &[&group_member_1, &group_member_2])
            .await?;
        let group_id_2 = setup
            .create_group_with_members(&user, "groupname_2", &[&group_member_2])
            .await?;

        // 2 messages in friend_1 <-> user
        chat_queries::send_private_message(&pool, friend_1.id(), user.id(), ChatMessage::new("1"))
            .await?;
        chat_queries::send_private_message(&pool, user.id(), friend_1.id(), ChatMessage::new("2"))
            .await?;
        // 3 messages in group chat 1
        chat_queries::send_group_message(
            &pool,
            group_member_1.id(),
            group_id_1,
            ChatMessage::new("3"),
        )
        .await?;
        chat_queries::send_group_message(
            &pool,
            group_member_2.id(),
            group_id_1,
            ChatMessage::new("4"),
        )
        .await?;
        chat_queries::send_group_message(&pool, user.id(), group_id_1, ChatMessage::new("5"))
            .await?;
        // 2 messages in group chat 2
        chat_queries::send_group_message(
            &pool,
            group_member_2.id(),
            group_id_2,
            ChatMessage::new("6"),
        )
        .await?;
        chat_queries::send_group_message(&pool, user.id(), group_id_2, ChatMessage::new("7"))
            .await?;

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup
            .get_chat_messages()
            .group_id(group_id_1)
            .limit(3)
            .send(&user)
            .await?;
        assert_eq!(&friend_messages, &[]);
        let [group_messages] = group_messages
            .try_into()
            .expect("Only channel for group_id_1");
        assert_eq!(group_messages.group_id, group_id_1);
        assert_eq!(
            &group_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &["5", "4", "3"]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_just_for_friend(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user = setup.create_user("user").await?;
        let friend_1 = setup.create_user("friend_1").await?;
        let friend_2 = setup.create_user("friend_2").await?;
        let group_member_1 = setup.create_user("group_member_1").await?;

        setup.add_each_other_as_friends(&user, &friend_1).await?;
        setup.add_each_other_as_friends(&user, &friend_2).await?;
        let group_id = setup
            .create_group_with_members(&user, "groupname", &[&group_member_1])
            .await?;

        // 2 messages in friend_1 <-> user
        chat_queries::send_private_message(&pool, friend_1.id(), user.id(), ChatMessage::new("1"))
            .await?;
        chat_queries::send_private_message(&pool, user.id(), friend_1.id(), ChatMessage::new("2"))
            .await?;
        // 2 messages in friend_2 <-> user
        chat_queries::send_private_message(&pool, friend_2.id(), user.id(), ChatMessage::new("3"))
            .await?;
        chat_queries::send_private_message(&pool, user.id(), friend_2.id(), ChatMessage::new("4"))
            .await?;
        // 2 messages in group_1
        chat_queries::send_group_message(
            &pool,
            group_member_1.id(),
            group_id,
            ChatMessage::new("5"),
        )
        .await?;
        chat_queries::send_group_message(&pool, user.id(), group_id, ChatMessage::new("6")).await?;

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup
            .get_chat_messages()
            .friend_id(friend_1.id())
            .limit(2)
            .send(&user)
            .await?;
        assert_eq!(&group_messages, &[]);
        let [friend_messages] = friend_messages
            .try_into()
            .expect("Only channel for friend_1");
        assert_eq!(friend_messages.friend_id, friend_1.id::<i64>());
        assert_eq!(
            &friend_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &["2", "1"]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn get_chat_messages_for_group_and_friend(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user = setup.create_user("user").await?;
        let group_member_1 = setup.create_user("group_member_1").await?;
        let group_member_2 = setup.create_user("group_member_2").await?;

        setup
            .add_each_other_as_friends(&user, &group_member_1)
            .await?;
        setup
            .add_each_other_as_friends(&user, &group_member_2)
            .await?;
        let group_id_1 = setup
            .create_group_with_members(&user, "groupname_1", &[&group_member_1, &group_member_2])
            .await?;
        let group_id_2 = setup
            .create_group_with_members(&user, "groupname_2", &[&group_member_2])
            .await?;

        // 3 messages in group chat 1
        chat_queries::send_group_message(
            &pool,
            group_member_1.id(),
            group_id_1,
            ChatMessage::new("1"),
        )
        .await?;
        chat_queries::send_group_message(
            &pool,
            group_member_2.id(),
            group_id_1,
            ChatMessage::new("2"),
        )
        .await?;
        chat_queries::send_group_message(&pool, user.id(), group_id_1, ChatMessage::new("3"))
            .await?;
        // 2 messages in group chat 2
        chat_queries::send_group_message(
            &pool,
            group_member_2.id(),
            group_id_2,
            ChatMessage::new("4"),
        )
        .await?;
        // Some random private messages
        chat_queries::send_private_message(
            &pool,
            group_member_1.id(),
            user.id(),
            ChatMessage::new("5"),
        )
        .await?;
        chat_queries::send_private_message(
            &pool,
            user.id(),
            group_member_2.id(),
            ChatMessage::new("6"),
        )
        .await?;

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup
            .get_chat_messages()
            .friend_id(group_member_1.id())
            .group_id(group_id_1)
            .send(&user)
            .await?;
        assert_eq!(&friend_messages, &[]);

        let [group_messages] = group_messages
            .try_into()
            .expect("Only channel for group_id_1");
        assert_eq!(group_messages.group_id, group_id_1);
        // group message from group_member_2 gets filtered out
        assert_eq!(
            &group_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &["3", "1"]
        );

        setup.shutdown().await
    }
}

#[cfg(test)]
mod ws_tests {
    use std::time::Duration;

    use super::test_setup::{group_tag, setup, user_tag};
    use crate::util::time::timestamp_into_offset_date_time;
    use crate::{data, data::proto::tag};
    use time::OffsetDateTime;
    use tokio_tungstenite::tungstenite::protocol::CloseFrame;
    use tokio_tungstenite::tungstenite::protocol::frame::coding::CloseCode;

    use super::proto::chatot::websocket_message_to_client as to_client;
    use super::proto::chatot::{StartedTyping, StoppedTyping};
    use crate::chatot::proto::chatot::chatot_service_server::ChatotService;
    use crate::chatot::proto::chatot::{GetChatMessagesResponse, RemoveFromGroupRequest};
    use crate::chatot::proto::chatot::{GetFriendsResponse, WebsocketError};
    use crate::chatot::proto::chatot::{RemoveFriendRequest, RemoveGroupRequest};
    use crate::gardevoir::test_helper::authorized_request;
    use crate::util::base64::encode_64bit_id;

    use super::*;

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn successfully_connects(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user = setup.create_user("user").await?;
        let mut connection = setup.connect(&user).await?;
        connection.wait_for_ping().await;

        setup.shutdown().await
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn chat_message_reaches_other_user(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;

        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        assert_eq!(
            connection_1.wait_for_message().await,
            to_client::Message::WentOnline(user_2.id())
        );

        let time_before_message = OffsetDateTime::now_utc();
        let text = format!("Hello @u{}", encode_64bit_id(user_2.id::<i64>()));
        connection_1
            .send_private_message(user_2.id(), &text)
            .await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::SentMessage(())
        );
        let [friend_channel] = setup
            .get_chat_messages()
            .since(time_before_message)
            .send(&user_2)
            .await?
            .friend_messages
            .try_into()
            .expect("single channel");
        assert_eq!(friend_channel.friend_id, user_1.id::<i64>());
        let [saved_message] = friend_channel
            .messages
            .try_into()
            .expect("A single message");
        assert_eq!(saved_message.text, text);
        assert_eq!(saved_message.from_user, user_1.id::<i64>());
        assert_eq!(
            &saved_message.tags,
            &[data::proto::Tag {
                span_from: 6,
                span_to: 18,
                kind: Some(tag::Kind::UserId(user_2.id()))
            }]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn chat_message_works_across_server_boundary(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let independent_pool_to_same_db =
            sqlx::PgPool::connect_with((*pool.connect_options()).clone()).await?;
        let setup_1 = setup(&pool).await?;
        let setup_2 = setup(&independent_pool_to_same_db).await?;

        let user_1 = setup_1.create_user("user_1").await?;
        let user_2 = setup_2.create_user("user_2").await?;

        // It shouldn't matter whether we use setup_1 or _2 here
        setup_1.add_each_other_as_friends(&user_1, &user_2).await?;

        let mut connection = setup_1.connect(&user_1).await?;
        connection.wait_for_ping().await;

        let mut connection_2 = setup_2.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        assert_eq!(
            connection.wait_for_message().await,
            to_client::Message::WentOnline(user_2.id())
        );

        setup_1.shutdown().await?;
        setup_2.shutdown().await
    }

    #[database_macros::test]
    async fn cannot_send_chat_message_to_non_friend(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;

        let mut connection = setup.connect(&user_1).await?;
        connection.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        let text = "Hello world".to_string();
        connection.send_private_message(user_2.id(), &text).await?;
        connection_2.assert_no_message().await;
        let chat_messages_1 = setup
            .get_chat_messages()
            .send(&user_1)
            .await?
            .friend_messages;
        assert_eq!(&chat_messages_1, &[]);

        let chat_messages_2 = setup
            .get_chat_messages()
            .send(&user_2)
            .await?
            .friend_messages;
        assert_eq!(&chat_messages_2, &[]);

        let error_message = connection.wait_for_message().await;
        assert_eq!(
            error_message,
            to_client::Message::Error(WebsocketError::CannotSentMessageToNonFriend.into())
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn send_chat_message_to_group(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        let user_3 = setup.create_user("user_3").await?;
        let user_not_in_group = setup.create_user("user_not_in_group").await?;

        let group_id = setup
            .create_group_with_members(&user_1, "my group", &[&user_2, &user_3])
            .await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        let mut connection_3 = setup.connect(&user_3).await?;
        connection_3.wait_for_ping().await;
        let mut connection_4 = setup.connect(&user_not_in_group).await?;
        connection_4.wait_for_ping().await;

        let messages = connection_1.wait_for_messages(2).await;
        assert!(messages.contains(&to_client::Message::WentOnline(user_2.id())));
        assert!(messages.contains(&to_client::Message::WentOnline(user_3.id())));

        let time_before_message = OffsetDateTime::now_utc();
        let text = format!(
            "Welcome @u{} to @g{}",
            encode_64bit_id(user_3.id::<i64>()),
            encode_64bit_id(group_id)
        );
        connection_2.send_group_message(group_id, &text).await?;

        assert_eq!(
            connection_1.wait_for_message().await,
            to_client::Message::SentMessage(())
        );
        assert_eq!(
            connection_3.wait_for_message().await,
            to_client::Message::SentMessage(())
        );
        connection_4.assert_no_message().await;

        // user_1 can access the message on demand
        let [group_channel] = setup
            .get_chat_messages()
            .since(time_before_message)
            .send(&user_1)
            .await?
            .group_messages
            .try_into()
            .expect("A single channel");
        assert_eq!(group_channel.group_id, group_id);
        let [saved_message_to_1] = group_channel.messages.try_into().expect("A single message");
        assert_eq!(saved_message_to_1.text, text);
        assert_eq!(saved_message_to_1.from_user, user_2.id::<i64>());
        assert_eq!(
            &saved_message_to_1.tags,
            &[user_tag(user_3.id(), 8..=20), group_tag(group_id, 25..=37)]
        );
        // user_2 can access his own message as well
        let [group_channel] = setup
            .get_chat_messages()
            .since(time_before_message)
            .send(&user_2)
            .await?
            .group_messages
            .try_into()
            .expect("A single channel");
        assert_eq!(group_channel.group_id, group_id);
        let [saved_message_to_2] = group_channel.messages.try_into().expect("A single message");
        assert_eq!(saved_message_to_2.text, text);
        assert_eq!(saved_message_to_2.from_user, user_2.id::<i64>());
        assert_eq!(
            &saved_message_to_2.tags,
            &[user_tag(user_3.id(), 8..=20), group_tag(group_id, 25..=37)]
        );

        // user_3 can access the message on demand
        let [group_channel] = setup
            .get_chat_messages()
            .since(time_before_message)
            .send(&user_3)
            .await?
            .group_messages
            .try_into()
            .expect("A single channel");
        assert_eq!(group_channel.group_id, group_id);
        let [saved_message_to_3] = group_channel.messages.try_into().expect("A single message");
        assert_eq!(saved_message_to_3.text, text);
        assert_eq!(saved_message_to_3.from_user, user_2.id::<i64>());
        assert_eq!(
            &saved_message_to_3.tags,
            &[user_tag(user_3.id(), 8..=20), group_tag(group_id, 25..=37)]
        );

        // user_not_in_group does not see the message
        let channels = setup
            .get_chat_messages()
            .since(time_before_message)
            .send(&user_not_in_group)
            .await?
            .group_messages;
        assert_eq!(&channels, &[]);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn send_chat_message_to_group_you_did_not_join(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;

        let group_id = setup
            .create_group_with_members(&user_1, "my group", &[])
            .await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        let time_before_message = OffsetDateTime::now_utc();
        let text = "Let me into this group!".to_string();
        connection_2.send_group_message(group_id, &text).await?;

        connection_1.assert_no_message().await;
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::Error(
                WebsocketError::CannotSentMessageToGroupYouHaveNotJoined.into()
            )
        );

        assert_eq!(
            setup
                .get_chat_messages()
                .since(time_before_message)
                .send(&user_1)
                .await?
                .group_messages,
            &[]
        );
        assert_eq!(
            setup
                .get_chat_messages()
                .since(time_before_message)
                .send(&user_1)
                .await?
                .group_messages,
            &[]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn send_chat_message_to_non_existing_user(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user = setup.create_user("user").await?;
        let mut connection = setup.connect(&user).await?;
        connection
            .send_private_message(404i64.into(), "doesn't matter")
            .await?;
        assert_eq!(
            connection.wait_for_message().await,
            to_client::Message::Error(WebsocketError::CannotSentMessageToNonFriend.into())
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn send_chat_message_to_non_existing_group(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user = setup.create_user("user").await?;
        let mut connection = setup.connect(&user).await?;
        connection.send_group_message(404, "doesn't matter").await?;
        assert_eq!(
            connection.wait_for_message().await,
            to_client::Message::Error(
                WebsocketError::CannotSentMessageToGroupYouHaveNotJoined.into()
            )
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn start_and_stop_typing_in_private_chat(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        connection_1.start_typing_in_private(user_2.id()).await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::StartedTyping(StartedTyping {
                from_user_id: user_1.id(),
                channel: Some(proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Private(user_1.id())),
                }),
            })
        );

        connection_1.stop_typing_in_private(user_2.id()).await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::StoppedTyping(StoppedTyping {
                from_user_id: user_1.id(),
                channel: Some(proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Private(user_1.id())),
                }),
            })
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn start_and_stop_typing_in_group_chat(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        let group_id = setup
            .create_group_with_members(&user_1, "my group", &[&user_2])
            .await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        connection_1.start_typing_in_group(group_id).await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::StartedTyping(StartedTyping {
                from_user_id: user_1.id(),
                channel: Some(proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Group(group_id)),
                }),
            })
        );

        connection_1.stop_typing_in_group(group_id).await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::StoppedTyping(StoppedTyping {
                from_user_id: user_1.id(),
                channel: Some(proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Group(group_id)),
                }),
            })
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn player_goes_offline(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;

        setup.add_each_other_as_friends(&user_1, &user_2).await?;
        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        connection_1.close().await?;
        connection_1.wait_for_close().await;

        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::WentOffline(user_1.id())
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn connect_to_websocket_a_second_time_with_the_same_user(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_1.wait_for_ping().await;
        connection_2.wait_for_ping().await;

        let _connection_1_new = setup.connect(&user_1).await?;

        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::WentOnline(user_1.id())
        );
        assert_eq!(
            connection_1.wait_for_close().await,
            Some(CloseFrame {
                code: CloseCode::Away,
                reason: "Connected a second time".into()
            })
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn newly_added_friend_can_send_message(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        connection_1.start_typing_in_private(user_2.id()).await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::StartedTyping(StartedTyping {
                from_user_id: user_1.id(),
                channel: Some(proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Private(user_1.id()))
                })
            })
        );
        // vice versa also works
        connection_2.start_typing_in_private(user_1.id()).await?;
        assert_eq!(
            connection_1.wait_for_message().await,
            to_client::Message::StartedTyping(StartedTyping {
                from_user_id: user_2.id(),
                channel: Some(proto::chatot::Channel {
                    kind: Some(proto::chatot::channel::Kind::Private(user_2.id()))
                })
            })
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn messages_from_removed_friend_do_not_get_received_anymore(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        connection_1.wait_for_message().await; // user_2 went online message

        setup
            .chatot
            .remove_friend(authorized_request(
                RemoveFriendRequest {
                    user_id: user_2.id(),
                },
                &user_1,
            ))
            .await?;
        // Both users get informed that the private channel is closed
        assert_eq!(
            connection_1.wait_for_message().await,
            to_client::Message::ChannelClosed(proto::chatot::Channel {
                kind: Some(proto::chatot::channel::Kind::Private(user_2.id()))
            })
        );

        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::ChannelClosed(proto::chatot::Channel {
                kind: Some(proto::chatot::channel::Kind::Private(user_1.id()))
            })
        );
        // Now no information is transmitted between the two users
        connection_2.start_typing_in_private(user_1.id()).await?;
        connection_1.assert_no_message().await;

        connection_1.start_typing_in_private(user_2.id()).await?;
        connection_2.assert_no_message().await;

        setup.shutdown().await
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn can_still_view_old_chat_messages_of_removed_friend(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        let first_message_text = "hi";
        connection_1
            .send_private_message(user_2.id(), first_message_text)
            .await?;
        connection_2.wait_for_message().await; // Wait for SentMessage to avoid race conditions

        setup
            .chatot
            .remove_friend(authorized_request(
                RemoveFriendRequest {
                    user_id: user_2.id(),
                },
                &user_1,
            ))
            .await?;
        connection_2.wait_for_message().await; // ChannelClosed

        connection_1
            .send_private_message(user_2.id(), "did you remove me?")
            .await?;
        connection_2.assert_no_message().await; // Should not get any ping from removed friend

        let GetChatMessagesResponse {
            friend_messages, ..
        } = setup.get_chat_messages().send(&user_1).await?;
        let [friend_message] = friend_messages.try_into().expect("single channel");
        assert_eq!(friend_message.friend_id, user_2.id::<i64>());
        let [message] = friend_message.messages.try_into().expect("single message");
        assert_eq!(message.text, first_message_text);

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn can_still_view_old_chat_messages_of_group_where_the_user_got_removed(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        let group_id = setup
            .create_group_with_members(&user_2, "groupname", &[&user_1])
            .await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        connection_1.wait_for_message().await; // user_2 went online

        let first_message_text = "hi";
        connection_2
            .send_group_message(group_id, first_message_text)
            .await?;
        connection_1.wait_for_message().await; // Wait for SentMessage to avoid race conditions

        setup
            .chatot
            .remove_from_group(authorized_request(
                RemoveFromGroupRequest {
                    user_id: user_1.id(),
                    group_id,
                },
                &user_2,
            ))
            .await?;
        assert_eq!(
            connection_1.wait_for_message().await,
            to_client::Message::ChannelClosed(proto::chatot::Channel {
                kind: Some(proto::chatot::channel::Kind::Group(group_id))
            })
        );

        connection_1
            .send_group_message(group_id, "did you remove me?")
            .await?;
        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&user_1).await?;
        assert_eq!(&friend_messages, &[]);
        let [group_messages] = group_messages.try_into().expect("single channel");
        assert_eq!(group_messages.group_id, group_id);
        assert_eq!(
            &group_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &[first_message_text]
        );

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&user_2).await?;
        assert_eq!(&friend_messages, &[]);
        let [group_messages] = group_messages.try_into().expect("single channel");
        assert_eq!(group_messages.group_id, group_id);
        assert_eq!(
            &group_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &[first_message_text]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn messages_in_removed_group_do_not_get_received_anymore(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        let group_id = setup
            .create_group_with_members(&user_2, "groupname", &[&user_1])
            .await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        connection_1.wait_for_message().await; // user_2 went online
        setup
            .chatot
            .remove_from_group(authorized_request(
                RemoveFromGroupRequest {
                    user_id: user_1.id(),
                    group_id,
                },
                &user_2,
            ))
            .await?;
        assert_eq!(
            connection_1.wait_for_message().await,
            to_client::Message::ChannelClosed(proto::chatot::Channel {
                kind: Some(proto::chatot::channel::Kind::Group(group_id))
            })
        );

        let message_text = "hi";
        connection_2
            .send_group_message(group_id, message_text)
            .await?;
        connection_1.assert_no_message().await;

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&user_1).await?;
        assert_eq!(&friend_messages, &[]);
        assert_eq!(&group_messages, &[]);

        let GetChatMessagesResponse {
            friend_messages,
            group_messages,
        } = setup.get_chat_messages().send(&user_2).await?;
        assert_eq!(&friend_messages, &[]);
        let [group_messages] = group_messages.try_into().expect("single channel");
        assert_eq!(group_messages.group_id, group_id);
        assert_eq!(
            &group_messages
                .messages
                .iter()
                .map(|message| message.text.as_str())
                .collect::<Vec<&str>>(),
            &[message_text]
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn removed_group_closes_channel_for_every_member(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        let group_id = setup
            .create_group_with_members(&user_1, "groupname", &[&user_2])
            .await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        connection_1.wait_for_message().await; // user_2 went online

        setup
            .chatot
            .remove_group(authorized_request(RemoveGroupRequest { group_id }, &user_1))
            .await?;

        assert_eq!(
            connection_1.wait_for_message().await,
            to_client::Message::ChannelClosed(proto::chatot::Channel {
                kind: Some(proto::chatot::channel::Kind::Group(group_id))
            })
        );
        assert_eq!(
            connection_2.wait_for_message().await,
            to_client::Message::ChannelClosed(proto::chatot::Channel {
                kind: Some(proto::chatot::channel::Kind::Group(group_id))
            })
        );

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn is_online_flag_gets_set(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let time_before_connection = OffsetDateTime::now_utc();
        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;

        let GetFriendsResponse { friends } = setup.get_friends(&user_2).await?;
        let [friend] = friends.try_into().expect("single friend");
        assert!(friend.is_online);
        let last_seen = friend
            .last_seen
            .and_then(timestamp_into_offset_date_time)
            .expect("last seen");
        assert!(last_seen > time_before_connection);
        assert!(last_seen < OffsetDateTime::now_utc());

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn is_online_flag_gets_unset(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let time_before_connection = OffsetDateTime::now_utc();
        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;
        connection_1.close().await?;
        // To avoid race condition, we wait for the leaving of user_1 to be transmitted
        connection_2.wait_for_message().await;

        let GetFriendsResponse { friends } = setup.get_friends(&user_2).await?;
        let [friend] = friends.try_into().expect("single friend");
        assert!(!friend.is_online);
        let last_seen = friend
            .last_seen
            .and_then(timestamp_into_offset_date_time)
            .expect("last seen");
        assert!(last_seen > time_before_connection);
        assert!(last_seen < OffsetDateTime::now_utc());

        setup.shutdown().await
    }

    #[database_macros::test]
    async fn last_seen_timestamp_gets_updated(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.modify_settings(|settings| settings.user_last_seen_update_interval_in_ms = 100);

        let user_1 = setup.create_user("user_1").await?;
        let user_2 = setup.create_user("user_2").await?;
        setup.add_each_other_as_friends(&user_1, &user_2).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;

        let GetFriendsResponse { friends } = setup.get_friends(&user_2).await?;
        let [friend] = friends.try_into().expect("single friend");
        assert!(friend.is_online);
        let last_seen = friend
            .last_seen
            .and_then(timestamp_into_offset_date_time)
            .expect("last seen");

        tokio::time::sleep(Duration::from_millis(200)).await;

        let GetFriendsResponse { friends } = setup.get_friends(&user_2).await?;
        let [friend] = friends.try_into().expect("single friend");
        assert!(friend.is_online);
        let new_last_seen = friend
            .last_seen
            .and_then(timestamp_into_offset_date_time)
            .expect("last seen");
        assert!(new_last_seen > last_seen);

        setup.shutdown().await
    }
}
