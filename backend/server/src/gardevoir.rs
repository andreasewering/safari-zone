mod api;
mod configuration;
mod hash;
mod mail;
mod permission;
mod proto;
mod provider;
mod state;

use std::sync::Arc;

use crate::{
    data::{MapId, SupportedLanguage, UserId},
    error::{AppError, AppResult},
};
pub use configuration::Settings;

pub use api::Gardevoir;
pub use mail::build_mail_sender;
pub use proto::AccessTokenClaims;
pub use proto::Permission;

pub use proto::gardevoir_service_server::GardevoirServiceServer;
pub use state::Token;

#[derive(Clone, Debug)]
pub struct AuthCtx(Arc<state::AuthState>);

impl AuthCtx {
    pub async fn new(settings: Settings) -> anyhow::Result<Self> {
        let state = state::AuthState::new(settings).await?;
        Ok(AuthCtx(Arc::new(state)))
    }

    pub fn encode_token(&self, claims: &Claims) -> Token {
        self.0.encode_access_token(&claims.token)
    }

    pub fn decode_token(&self, x_map_id: Option<MapId>, token: &str) -> AppResult<Claims> {
        let claims = self.0.decode_access_token(token)?;
        let claims = Claims::new(claims, x_map_id)?;
        Ok(claims)
    }

    /// Prefer `decode_token` over this unless the users map does not matter at all
    pub fn decode_token_without_map_id(&self, token: &str) -> AppResult<Claims> {
        let claims = self.0.decode_access_token(token)?;
        let map_id = claims.map_id.map(MapId::from);
        Claims::new(claims, map_id)
    }

    pub fn decode_header(&self, headers: &tonic::metadata::MetadataMap) -> AppResult<Claims> {
        let header = match headers.get("authorization") {
            Some(v) => v,
            None => {
                return AppError::unauthenticated("Missing authorization header").into();
            }
        };
        let header_bytes = header.as_bytes();
        let auth_header = match String::from_utf8(header_bytes.to_vec()) {
            Ok(v) => v,
            Err(err) => {
                tracing::debug!(message = "Failed to parse access token.", error = %err);
                return AppError::unauthenticated("Authorization Header could not be parsed.")
                    .into();
            }
        };
        let x_map_id_header = headers
            .get("x-map-id")
            .map(|header| String::from_utf8(header.as_bytes().to_vec()))
            .transpose()
            .map_err(|err| AppError::bad_request(format!("Invalid x-map-id header: {err}")))?
            .map(|str| str.parse::<i64>())
            .transpose()
            .map_err(|err| AppError::bad_request(format!("Invalid x-map-id header: {err}")))?
            .map(MapId::from);

        self.decode_token(x_map_id_header, &auth_header)
    }
}

#[derive(Debug, Clone)]
pub struct Claims {
    token: AccessTokenClaims,
}

impl Claims {
    fn new(token: AccessTokenClaims, x_map_id_header: Option<MapId>) -> AppResult<Self> {
        let Some(map_id) = token.map_id else {
            return Ok(Self { token });
        };
        let map_id = MapId::from(map_id);
        if x_map_id_header != Some(map_id) {
            return AppError::unauthenticated("x-map-id header and map_id claim are not the same.")
                .into();
        }
        Ok(Self { token })
    }

    pub fn user_id(&self) -> UserId {
        UserId::from(self.token.user_id)
    }

    pub fn map_id(&self) -> Option<MapId> {
        self.token.map_id.map(MapId::from)
    }

    pub fn has_permission(&self, permission: Permission) -> bool {
        self.token.permissions.contains(&permission.into())
    }

    pub fn set_map_id(&self, map_id: MapId) -> Claims {
        Claims {
            token: AccessTokenClaims {
                map_id: Some(map_id.into()),
                ..self.token.clone()
            },
        }
    }
}

pub fn guard(claims: &Claims, permission: Permission) -> AppResult<()> {
    if !claims.has_permission(permission) {
        return AppError::permission_denied(format!(
            "User {} requested access to resource but is missing permission {permission:?}",
            claims.user_id()
        ))
        .into();
    }
    Ok(())
}

pub fn guard_map_read(claims: &Claims, map_id: MapId) -> AppResult<()> {
    if claims.map_id() != Some(map_id) && !claims.has_permission(Permission::ReadMapDetails) {
        return AppError::permission_denied(format!(
            "User {} requested access to map {map_id} but is on another map {:?}",
            claims.user_id(),
            claims.map_id()
        ))
        .into();
    }
    Ok(())
}

#[derive(Debug, Clone)]
pub struct Verification {
    user_id: UserId,
    verification_id: i64,
    email: String,
    locale: SupportedLanguage,
}

#[cfg(test)]
pub mod test_helper {
    use api::Gardevoir;
    use configuration::{AndrenaProviderSettings, GithubProviderSettings, Providers};
    use lettre::AsyncTransport as _;
    use mail::MailSender;
    use proto::{
        gardevoir_service_server::GardevoirService, login_request::Payload, AccessTokenData,
        LoginRequest, RegistrationRequest, SafarizoneCredentials,
    };
    use sqlx::PgPool;
    use std::str::FromStr as _;
    use tonic::{
        metadata::{AsciiMetadataValue, MetadataMap},
        Request,
    };

    /**
     * Public test helpers for gardevoir-related functionality.
     * This should be the way you set up users in an integrative test instead of using a handmade query to the database.
     */
    use crate::gardevoir::configuration::{AccountSettings, JwtSettings};

    use self::configuration::VerificationMode;

    use super::*;

    const ACCOUNT_SETTINGS: AccountSettings = AccountSettings {
        confirm_account_expire_time_in_seconds: 10,
        reset_password_expire_time_in_seconds: 10,
        verification_mode: VerificationMode::None,
    };

    fn create_jwt_settings() -> JwtSettings {
        JwtSettings {
            access_token_expire_time_in_seconds: 100,
            refresh_token_expire_time_in_seconds: 1000,
            private_key: "MC4CAQAwBQYDK2VwBCIEIBikeuYVCmzIlquM+romRp6IRKE+G1/QUlJhHbSnRpRJ".into(),
        }
    }

    pub fn create_settings() -> Settings {
        Settings {
            account: ACCOUNT_SETTINGS,
            jwt: create_jwt_settings(),
            providers: Providers {
                andrena: AndrenaProviderSettings {
                    jwk_url: "jwk/url".into(),
                    refresh_rate_in_seconds: 1000,
                    client_id: "test".into(),
                    realm: "andrena".into(),
                },
                github: GithubProviderSettings {
                    client_id: "github_client_id".into(),
                    client_secret: "github_client_secret".into(),
                },
            },
        }
    }

    pub fn create_mail_sender() -> lettre::transport::stub::AsyncStubTransport {
        lettre::transport::stub::AsyncStubTransport::new(Ok(()))
    }

    #[async_trait::async_trait]
    impl MailSender for lettre::transport::stub::AsyncStubTransport {
        async fn send_mail(&self, message: lettre::Message) -> AppResult<()> {
            self.send(message)
                .await
                .map_err(|err| AppError::internal(format!("Stub mail transport failed: {err}")))
        }
    }

    pub async fn create_auth_ctx() -> anyhow::Result<AuthCtx> {
        let auth_ctx = AuthCtx::new(create_settings()).await?;
        Ok(auth_ctx)
    }

    #[derive(Clone, Debug)]
    pub struct LoggedInUser {
        claims: Claims,
        pub access_token: String,
        auth_ctx: AuthCtx,
        pub display_name: String,
    }

    impl LoggedInUser {
        pub fn id<T>(&self) -> T
        where
            UserId: Into<T>,
        {
            self.claims.user_id().into()
        }

        pub fn request<T>(&self, message: T) -> tonic::Request<T> {
            let mut req = tonic::Request::new(message);
            req.metadata_mut().insert(
                "authorization",
                tonic::metadata::MetadataValue::from_str(&self.access_token)
                    .expect("Invalid access token"),
            );
            if let Some(map_id) = self.claims().map_id() {
                req.metadata_mut().insert(
                    "x-map-id",
                    tonic::metadata::MetadataValue::from_str(&map_id.to_string())
                        .expect("Invalid map id"),
                );
            };
            req
        }

        pub fn patch_permissions(&mut self, permissions: &[Permission]) {
            let permissions: Vec<i32> = permissions
                .iter()
                .copied()
                .map(|permission| permission as i32)
                .collect();
            self.claims.token.permissions.clone_from(&permissions);
            let access_token = self
                .auth_ctx
                .0
                .encode_access_token(&proto::AccessTokenClaims {
                    user_id: self.claims.user_id().into(),
                    map_id: self.claims.map_id().map(i64::from),
                    permissions,
                });
            self.access_token = access_token.token;
        }

        pub fn set_access_token(&mut self, map_id: MapId, access_token: String) {
            self.claims = self
                .auth_ctx
                .decode_token(Some(map_id), &access_token)
                .expect("Valid token");
            self.access_token = access_token;
        }

        pub fn enter_map(&mut self, map_id: MapId) {
            self.claims = self.claims.set_map_id(map_id);
            let access_token = self
                .auth_ctx
                .0
                .encode_access_token(&proto::AccessTokenClaims {
                    user_id: self.claims.user_id().into(),
                    map_id: Some(map_id.into()),
                    permissions: self.claims.token.permissions.clone(),
                });
            self.access_token = access_token.token;
        }

        pub fn claims(&self) -> &Claims {
            &self.claims
        }
    }

    pub fn authorized_request<T>(message: T, user: &LoggedInUser) -> tonic::Request<T> {
        let mut req = tonic::Request::new(message);
        req.metadata_mut().insert(
            "authorization",
            tonic::metadata::MetadataValue::from_str(&user.access_token)
                .expect("Invalid access token"),
        );
        req
    }

    pub struct UserBuilder {
        username: String,
        password: String,
        email: String,
    }

    impl UserBuilder {
        pub fn from_name(name: &str) -> Self {
            Self {
                username: name.to_string(),
                password: "password".to_string(),
                email: format!(
                    "{}@test.de",
                    name.chars()
                        .filter(|c| !c.is_whitespace())
                        .collect::<String>(),
                ),
            }
        }

        pub async fn create(
            self,
            auth_ctx: &AuthCtx,
            pool: &PgPool,
        ) -> anyhow::Result<LoggedInUser> {
            let UserBuilder {
                username,
                password,
                email,
            } = self;
            let gardevoir = Gardevoir::new(
                pool.clone(),
                reqwest::Client::new(),
                auth_ctx.clone(),
                create_mail_sender(),
            );
            let registration = RegistrationRequest {
                username: username.clone(),
                password: password.clone(),
                email: email.clone(),
            };

            let credentials = SafarizoneCredentials {
                identifier: username.to_string(),
                password: password.to_string(),
            };
            let login_request = LoginRequest {
                payload: Some(Payload::SafarizoneCredentials(credentials)),
            };
            let mut metadata = MetadataMap::new();
            metadata.append("accept-language", AsciiMetadataValue::from_static("en"));

            gardevoir
                .register(Request::from_parts(
                    metadata,
                    Default::default(),
                    registration,
                ))
                .await?;
            let response = gardevoir.login(Request::new(login_request)).await?;
            let AccessTokenData { access_token, .. } = response.into_inner();
            let claims = auth_ctx.decode_token(None, &access_token)?;
            Ok(LoggedInUser {
                claims,
                access_token,
                auth_ctx: auth_ctx.clone(),
                display_name: username.to_string(),
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use api::Gardevoir;
    use configuration::VerificationMode;
    use proto::{
        gardevoir_service_server::GardevoirService, login_request::Payload, AccessTokenData,
        LoginRequest, RegistrationRequest, SafarizoneCredentials, VerificationRequest,
    };
    use tonic::Request;

    use crate::util;

    use super::*;

    #[database_macros::test]
    async fn create_user_happy_path(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut settings = test_helper::create_settings();
        settings.account.verification_mode = VerificationMode::Mail;
        let client = reqwest::Client::new();
        let auth_ctx = AuthCtx::new(settings).await?;
        let mail_sender = test_helper::create_mail_sender();
        let gardevoir = Gardevoir::new(pool, client, auth_ctx.clone(), mail_sender.clone());

        gardevoir
            .register(Request::new(RegistrationRequest {
                username: "Tiberius Estor".into(),
                password: "verysecure".into(),
                email: "tiberius@estor.de".into(),
            }))
            .await?;

        let (_envelope, mail_content) = mail_sender
            .messages()
            .await
            .into_iter()
            .next()
            .expect("Should have received mail.");
        let start_idx = mail_content
            .find("Content-Transfer-Encoding: quoted-printable\r\n\r\n")
            .unwrap()
            + "Content-Transfer-Encoding: quoted-printable\r\n\r\n".len();
        let content_to_decode = &mail_content[start_idx..];
        let decoded_content = quoted_printable::decode(
            content_to_decode.as_bytes(),
            quoted_printable::ParseMode::Robust,
        )
        .unwrap();
        let decoded_content_str = String::from_utf8(decoded_content).unwrap();
        let regex = regex::Regex::new(
            r#"http://localhost:\d+/verification/\?user_id=(?P<user_id>[A-Za-z0-9_-]+)&verification_id=(?P<verification_id>[A-Za-z0-9_-]+)"#,
        ).unwrap();
        let caps = regex.captures(&decoded_content_str).unwrap_or_else(|| {
            panic!("Expected mail to contain verification link, but received {mail_content}, decoded content: {decoded_content_str}")
        });
        let user_id = caps
            .name("user_id")
            .expect("Regex should have an user_id capture group.")
            .as_str();
        let verification_id = caps
            .name("verification_id")
            .expect("Regex should have a verification_id capture group.")
            .as_str();
        let user_id = util::base64::decode_64bit_id(user_id)?;
        let verification_id = util::base64::decode_64bit_id(verification_id)?;
        gardevoir
            .verify(Request::new(VerificationRequest {
                user_id,
                verification_id,
            }))
            .await?;

        let login_response = gardevoir
            .login(Request::new(LoginRequest {
                payload: Some(Payload::SafarizoneCredentials(SafarizoneCredentials {
                    identifier: "Tiberius Estor".into(),
                    password: "verysecure".into(),
                })),
            }))
            .await?;
        let AccessTokenData { access_token, .. } = login_response.into_inner();

        let claims = auth_ctx.decode_token(None, &access_token)?;
        assert_eq!(i64::from(claims.user_id()), user_id);
        assert_eq!(claims.map_id(), None);

        Ok(())
    }
}
