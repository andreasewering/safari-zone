use http_body_util::combinators::UnsyncBoxBody;
use http_body_util::{BodyExt as _, Full};
use serde::Deserialize;
use tracing_subscriber::fmt::{format, MakeWriter};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use std::io;
use std::sync::Arc;
use std::task::{Context, Poll};
use std::time::{self, Instant};
use tower::{Layer, Service};

use metrics_exporter_prometheus::{Matcher, PrometheusBuilder, PrometheusHandle};

#[derive(Debug, Deserialize, Clone)]
pub struct LogSettings {
    pub level: Arc<str>,
    pub format: LogFormat,
    pub colored: bool,
}

#[derive(Debug, Deserialize, Copy, Clone)]
#[serde(rename_all = "snake_case")]
pub enum LogFormat {
    Json,
    Full,
}

pub mod err_code {
    include!(concat!(env!("OUT_DIR"), "/error.rs"));

    impl ErrorCode {
        pub fn to_response(self) -> String {
            (self as i32).to_string()
        }
    }
}

pub fn watchog_layer() -> MetricLayer {
    MetricLayer
}

pub fn setup_metrics_recorder() -> anyhow::Result<PrometheusHandle> {
    const EXPONENTIAL_SECONDS: &[f64] = &[
        0.005, 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 1.0, 2.5, 5.0, 10.0,
    ];

    let handle = PrometheusBuilder::new()
        .set_buckets_for_metric(
            Matcher::Full("http_requests_duration_seconds".to_string()),
            EXPONENTIAL_SECONDS,
        )?
        .install_recorder()?;

    let process_collector = metrics_process::Collector::default();
    // Call `describe()` method to register help string.
    process_collector.describe();

    tokio::spawn(async move {
        loop {
            process_collector.collect();
            tokio::time::sleep(time::Duration::from_secs(5)).await;
        }
    });
    Ok(handle)
}

#[derive(Debug, Clone)]
pub struct MetricLayer;

impl<S> Layer<S> for MetricLayer {
    type Service = MetricMiddleware<S>;

    fn layer(&self, service: S) -> Self::Service {
        MetricMiddleware { inner: service }
    }
}

#[derive(Debug, Clone)]
pub struct MetricMiddleware<S> {
    inner: S,
}

impl<S, RequestBody, ResponseBody> Service<hyper::Request<RequestBody>> for MetricMiddleware<S>
where
    S: Service<hyper::Request<RequestBody>, Response = hyper::Response<ResponseBody>>
        + Clone
        + Send
        + 'static,
    S::Future: Send + 'static,
    RequestBody: Send + 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = futures::future::BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, req: hyper::Request<RequestBody>) -> Self::Future {
        // This is necessary because tonic internally uses `tower::buffer::Buffer`.
        // See https://github.com/tower-rs/tower/issues/547#issuecomment-767629149
        // for details on why this is necessary
        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        Box::pin(async move {
            // Do extra async work here...
            let start_time = Instant::now();
            let path = req.uri().path().to_string();
            let method = req.method().clone();
            let in_flight_labels = [("method", method.to_string()), ("path", path.clone())];
            let gauge = metrics::gauge!("http_requests_in_flight", &in_flight_labels);
            gauge.increment(1.0);
            let response = inner.call(req).await?;

            let elapsed_time = Instant::now().duration_since(start_time);
            let latency = elapsed_time.as_secs_f64();
            let labels = [
                ("method", method.to_string()),
                ("path", path),
                ("status", response.status().to_string()),
            ];
            metrics::counter!("http_requests_total", &labels).increment(1);
            gauge.decrement(1.0);
            metrics::histogram!("http_requests_duration_seconds", &labels).record(latency);

            Ok(response)
        })
    }
}

#[derive(Clone)]
pub struct PrometheusLayer(PrometheusHandle);

impl PrometheusLayer {
    pub fn new(handle: PrometheusHandle) -> Self {
        PrometheusLayer(handle)
    }
}

impl<S> Layer<S> for PrometheusLayer {
    type Service = PrometheusMiddleware<S>;

    fn layer(&self, service: S) -> Self::Service {
        PrometheusMiddleware {
            inner: service,
            handle: self.0.clone(),
        }
    }
}

#[derive(Clone)]
pub struct PrometheusMiddleware<S> {
    inner: S,
    handle: PrometheusHandle,
}

impl<S, RequestBody, B, E> Service<hyper::Request<RequestBody>> for PrometheusMiddleware<S>
where
    S: Service<hyper::Request<RequestBody>, Response = hyper::Response<UnsyncBoxBody<B, E>>>
        + Clone
        + Send
        + 'static,
    S::Future: Send + 'static,
    B: From<String> + hyper::body::Buf + Send + 'static,
    RequestBody: Send + 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = futures::future::BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, req: hyper::Request<RequestBody>) -> Self::Future {
        if req.uri().path() == "/metrics" {
            let handle = self.handle.clone();
            return Box::pin(async move {
                let bytes: B = handle.render().into();
                let body = UnsyncBoxBody::new(Full::new(bytes).map_err(|_| unreachable!()));

                Ok(hyper::Response::new(body))
            });
        }
        // This is necessary because tonic internally uses `tower::buffer::Buffer`.
        // See https://github.com/tower-rs/tower/issues/547#issuecomment-767629149
        // for details on why this is necessary
        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);
        Box::pin(inner.call(req))
    }
}

pub fn install_default_tracer(settings: &LogSettings) {
    build_tracer(settings, io::stdout).init()
}

pub fn build_tracer<W>(settings: &LogSettings, writer: W) -> impl tracing::Subscriber + use<W>
where
    W: for<'a> MakeWriter<'a> + Sync + Send + 'static,
{
    tracing_subscriber::Registry::default()
        .with(env_filter(&*settings.level))
        .with(format_layer(settings, writer))
}

/// This configures JSON formatting for fields and events as well as some metadata fields that will automatically be included:
/// - Line numbers
/// - File name
/// - Current thread ID
/// - Timestamp
/// - Cargo Rust Target (here this should be "watchog" for example)
///
/// Logging with `tracing::info!(message = "a", abc = 5)` for example, results in a log
///
/// {"timestamp": "...", "fields": {"message": "a", "abc": 5}, "target": "...", "filename": "...", "line_number": ..., "threadId": ...}
fn format_layer<
    S: tracing::Subscriber + for<'a> tracing_subscriber::registry::LookupSpan<'a>,
    W: for<'a> MakeWriter<'a> + Sync + Send + 'static,
>(
    settings: &LogSettings,
    writer: W,
) -> Box<dyn tracing_subscriber::Layer<S> + Sync + Send> {
    match settings.format {
        // Make sure this stays in sync with the other case
        // The types make it hard to extract common code here.
        LogFormat::Json => Box::new(
            tracing_subscriber::fmt::layer()
                .fmt_fields(format::JsonFields::default())
                .event_format(format::json())
                .with_file(true)
                .with_line_number(true)
                .with_thread_ids(true)
                .with_ansi(settings.colored)
                .with_writer(writer),
        ),
        LogFormat::Full => Box::new(
            tracing_subscriber::fmt::layer()
                .with_file(true)
                .with_line_number(true)
                .with_thread_ids(true)
                .with_ansi(settings.colored)
                .with_writer(writer),
        ),
    }
}

pub fn env_filter(filter_string: impl Into<String>) -> tracing_subscriber::EnvFilter {
    tracing_subscriber::EnvFilter::new(filter_string.into())
}

#[cfg(test)]
#[allow(clippy::mutable_key_type)]
mod tests {
    use std::collections::HashMap;
    use std::sync::{Arc, Mutex};

    use metrics::{Key, Label};
    use metrics_util::debugging::{DebugValue, DebuggingRecorder};
    use metrics_util::{CompositeKey, MetricKind};
    use tower::Service;
    use tracing_subscriber::fmt::MakeWriter;

    use super::err_code::ErrorCode;
    use super::*;
    use regex::Regex;

    #[tokio::test]
    async fn basic_log_works() {
        let buffer = SharedBuffer::default();
        let log_settings = LogSettings {
            format: LogFormat::Json,
            level: "info".into(),
            colored: false,
        };

        let sub = build_tracer(&log_settings, buffer.clone());
        let _guard = tracing::subscriber::set_default(sub);

        tracing::info!("test");

        let re = Regex::new(r#"\{"timestamp":"[\d-]+T[\d:.]+Z","level":"INFO","fields":\{"message":"test"},"target":"server::watchog::tests","filename":"server[/\\]+src[/\\]+watchog.rs","line_number":\d+,"threadId":"ThreadId\(\d+\)"\}\n"#).unwrap();
        assert!(
            re.is_match(&buffer.read()),
            "Log did not match expected regex: {}",
            buffer.read()
        );
    }

    #[tokio::test]
    async fn logs_with_arguments() {
        let buffer = SharedBuffer::default();
        let log_settings = LogSettings {
            format: LogFormat::Json,
            level: "info".into(),
            colored: false,
        };
        let sub = build_tracer(&log_settings, buffer.clone());
        let _guard = tracing::subscriber::set_default(sub);

        tracing::info!(message = "a", abc = 42);

        let re = Regex::new(r#"\{"timestamp":"[\d-]+T[\d:.]+Z","level":"INFO","fields":\{"message":"a","abc":42},"target":"server::watchog::tests","filename":"server[/\\]+src[/\\]+watchog.rs","line_number":\d+,"threadId":"ThreadId\(\d+\)"\}\n"#).unwrap();
        assert!(
            re.is_match(&buffer.read()),
            "Log did not match expected regex: {}",
            buffer.read()
        );
    }

    #[tokio::test]
    async fn different_severities() {
        let buffer = SharedBuffer::default();
        let log_settings = LogSettings {
            format: LogFormat::Json,
            level: "info".into(),
            colored: false,
        };
        let sub = build_tracer(&log_settings, buffer.clone());

        let _guard = tracing::subscriber::set_default(sub);

        tracing::info!(message = "a");
        tracing::debug!(message = "b");
        tracing::error!(message = "c");

        let re = Regex::new(r#"\{"timestamp":"[\d-]+T[\d:.]+Z","level":"INFO","fields":\{"message":"a"},"target":"server::watchog::tests","filename":"server[/\\]+src[/\\]+watchog.rs","line_number":\d+,"threadId":"ThreadId\(\d+\)"\}\n\{"timestamp":"[\d-]+T[\d:.]+Z","level":"ERROR","fields":\{"message":"c"},"target":"server::watchog::tests","filename":"server[/\\]+src[/\\]+watchog.rs","line_number":\d+,"threadId":"ThreadId\(\d+\)"\}\n"#).unwrap();
        assert!(
            re.is_match(&buffer.read()),
            "Log did not match expected regex: {}",
            buffer.read()
        );
    }

    #[test]
    fn error_code_to_response_works_for_some_values() {
        assert_eq!(ErrorCode::BadRequest.to_response(), "8");
        assert_eq!(ErrorCode::Unspecified.to_response(), "0");
    }

    #[tokio::test]
    async fn watchog_layer_collects_http_metrics() {
        let recorder = DebuggingRecorder::new();

        metrics::with_local_recorder(&recorder, || {
            futures::executor::block_on(async {
                let mock_inner_service = tower::service_fn(|_req: hyper::Request<()>| async move {
                    let resp = hyper::Response::new(());
                    Ok::<hyper::Response<()>, &'static str>(resp)
                });
                let mut service = watchog_layer().layer(mock_inner_service);

                let request = hyper::Request::new(());
                service.call(request).await.unwrap();

                let metrics = recorder.snapshotter().snapshot().into_hashmap();

                // http request total starts at zero and should now be one.
                let http_requests_total = get_counter(
                    &metrics,
                    Key::from_parts(
                        "http_requests_total",
                        vec![
                            Label::from_static_parts("method", "GET"),
                            Label::from_static_parts("path", "/"),
                            Label::from_static_parts("status", "200 OK"),
                        ],
                    ),
                );
                assert_eq!(http_requests_total, Some(1));

                // http requests in flight should be at zero again after the request
                let http_requests_in_flight = get_gauge(
                    &metrics,
                    Key::from_parts(
                        "http_requests_in_flight",
                        vec![
                            Label::from_static_parts("method", "GET"),
                            Label::from_static_parts("path", "/"),
                        ],
                    ),
                );
                assert_eq!(http_requests_in_flight, Some(0.0));

                // http duration histogram should have recorded a single point with less than a second
                let durations_in_seconds = get_histogram(
                    &metrics,
                    Key::from_parts(
                        "http_requests_duration_seconds",
                        vec![
                            Label::from_static_parts("method", "GET"),
                            Label::from_static_parts("path", "/"),
                            Label::from_static_parts("status", "200 OK"),
                        ],
                    ),
                )
                .unwrap();
                assert_eq!(durations_in_seconds.len(), 1);
                assert!(durations_in_seconds.first().unwrap() < &1.0);
            })
        });
    }

    fn get_counter<A, B>(map: &HashMap<CompositeKey, (A, B, DebugValue)>, key: Key) -> Option<u64> {
        map.get(&CompositeKey::new(MetricKind::Counter, key))
            .and_then(|(_, _, value)| match value {
                DebugValue::Counter(counter) => Some(counter),
                _ => None,
            })
            .copied()
    }

    fn get_gauge<A, B>(map: &HashMap<CompositeKey, (A, B, DebugValue)>, key: Key) -> Option<f64> {
        map.get(&CompositeKey::new(MetricKind::Gauge, key))
            .and_then(|(_, _, value)| match value {
                DebugValue::Gauge(gauge) => Some(gauge.0),
                _ => None,
            })
    }

    fn get_histogram<A, B>(
        map: &HashMap<CompositeKey, (A, B, DebugValue)>,
        key: Key,
    ) -> Option<Vec<f64>> {
        map.get(&CompositeKey::new(MetricKind::Histogram, key))
            .and_then(|(_, _, value)| match value {
                DebugValue::Histogram(histogram) => {
                    Some(histogram.iter().map(|val| val.0).collect())
                }
                _ => None,
            })
    }

    #[derive(Clone, Default)]
    struct SharedBuffer(Arc<Mutex<Vec<u8>>>);

    impl<'a> MakeWriter<'a> for SharedBuffer {
        type Writer = tracing_subscriber::fmt::writer::MutexGuardWriter<'a, Vec<u8>>;

        fn make_writer(&'a self) -> Self::Writer {
            self.0.make_writer()
        }
    }

    impl SharedBuffer {
        fn read(&self) -> String {
            let clone = self.clone();
            let buffer = clone.0.lock().unwrap();
            String::from_utf8_lossy(&buffer).to_string()
        }
    }
}
