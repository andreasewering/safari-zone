use std::collections::BTreeMap;
use std::time::Duration;

use super::chat_queries;
use super::config::Settings;
use super::friend_queries;
use super::group_queries;
use super::proto::chatot::GetChatMessagesRequest;
use super::proto::chatot::GetChatMessagesResponse;
use super::proto::chatot::ParseChatMessageRequest;
use super::proto::chatot::ParseChatMessageResponse;
use super::proto::chatot::{
    chatot_service_server::ChatotService, group_member::Role, AddFriendRequest, AddFriendResponse,
    AddToGroupRequest, AddToGroupResponse, CreateGroupRequest, CreateGroupResponse, Friend,
    FriendRequest, GetFriendRequestsRequest, GetFriendRequestsResponse, GetFriendsRequest,
    GetFriendsResponse, GetGroupDetailRequest, GetGroupDetailResponse, GetGroupsRequest,
    GetGroupsResponse, Group, GroupMember, MarkFriendAsFavoriteRequest,
    MarkFriendAsFavoriteResponse, MarkGroupAsFavoriteRequest, MarkGroupAsFavoriteResponse,
    RemoveFriendRequest, RemoveFriendResponse, RemoveFromGroupRequest, RemoveFromGroupResponse,
    RemoveGroupRequest, RemoveGroupResponse, RequestGroupMembershipRequest,
    RequestGroupMembershipResponse,
};
use super::pubsub::UserChannel;
use super::pubsub::UserChannelMessage;
use crate::chat_message;
use crate::chat_message::proto_from_tag;
use crate::chat_message::ChatMessage;
use crate::chatot::proto::chatot::FriendChannelMessages;
use crate::chatot::proto::chatot::GroupChannelMessages;
use crate::chatot::pubsub::PrivateChannel;
use crate::data;
use crate::data::UserId;
use crate::pubsub::PgPubSub;
use crate::util::time::offset_date_time_into_timestamp;
use crate::util::time::timestamp_into_offset_date_time;
use sqlx::PgPool;
use time::OffsetDateTime;
use tonic::{Request, Response, Status};

use crate::watchog::err_code::ErrorCode;

use crate::error::AppError;
use crate::gardevoir::AuthCtx;

use super::proto::chatot as proto;

pub struct Chatot {
    pub auth_ctx: AuthCtx,
    pub db_connection_pool: PgPool,
    pub pubsub: PgPubSub,
    pub settings: Settings,
}

#[tonic::async_trait]
impl ChatotService for Chatot {
    async fn get_friends(
        &self,
        request: Request<GetFriendsRequest>,
    ) -> Result<Response<GetFriendsResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let friends = friend_queries::get_friends(&self.db_connection_pool, claims.user_id())
            .await?
            .into_iter()
            .map(|r| Friend {
                user_id: r.friend_id.into(),
                user_name: r.username,
                since: Some(offset_date_time_into_timestamp(r.since)),
                deleted_at: r.deleted_at.map(offset_date_time_into_timestamp),
                favorite: r.favorite,
                is_online: r.online.unwrap_or(false),
                last_seen: r.last_seen.map(offset_date_time_into_timestamp),
            })
            .collect();

        Ok(Response::new(GetFriendsResponse { friends }))
    }

    async fn get_friend_requests(
        &self,
        request: Request<GetFriendRequestsRequest>,
    ) -> Result<Response<GetFriendRequestsResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let (incoming_friend_requests, outgoing_friend_requests) =
            friend_queries::get_friend_requests(&self.db_connection_pool, claims.user_id()).await?;

        let incoming = incoming_friend_requests
            .into_iter()
            .map(|request| FriendRequest {
                user_id: request.from_user_id.into(),
                user_name: request.user_name,
                issued_at: Some(crate::util::time::offset_date_time_into_timestamp(
                    request.issued_at,
                )),
            })
            .collect();

        let outgoing = outgoing_friend_requests
            .into_iter()
            .map(|request| FriendRequest {
                user_id: request.to_user_id.into(),
                user_name: request.user_name,
                issued_at: Some(crate::util::time::offset_date_time_into_timestamp(
                    request.issued_at,
                )),
            })
            .collect();

        Ok(Response::new(GetFriendRequestsResponse {
            incoming,
            outgoing,
        }))
    }

    async fn add_friend(
        &self,
        request: Request<AddFriendRequest>,
    ) -> Result<Response<AddFriendResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let AddFriendRequest {
            user_id: friend_to_add,
        } = request.into_inner();
        let friend_to_add = UserId::from(friend_to_add);
        match friend_queries::add_friend(&self.db_connection_pool, claims.user_id(), friend_to_add)
            .await?
        {
            friend_queries::AddFriendResult::NoChange => {}
            friend_queries::AddFriendResult::NewRequest => {}
            friend_queries::AddFriendResult::AcceptedRequest => {
                if let Err(error) = self
                    .pubsub
                    .publish(
                        &UserChannel(claims.user_id()),
                        UserChannelMessage::UsersBecameFriends {
                            friend_id: friend_to_add,
                        },
                    )
                    .await
                {
                    tracing::error!(%error, "Failed to publish message");
                };

                if let Err(error) = self
                    .pubsub
                    .publish(
                        &UserChannel(friend_to_add),
                        UserChannelMessage::UsersBecameFriends {
                            friend_id: claims.user_id(),
                        },
                    )
                    .await
                {
                    tracing::error!(%error, "Failed to publish message");
                };
            }
        }
        Ok(Response::new(AddFriendResponse {}))
    }

    async fn remove_friend(
        &self,
        request: Request<RemoveFriendRequest>,
    ) -> Result<Response<RemoveFriendResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let RemoveFriendRequest {
            user_id: friend_to_remove,
        } = request.into_inner();
        let friend_id = UserId::from(friend_to_remove);
        let was_removed =
            friend_queries::remove_friend(&self.db_connection_pool, claims.user_id(), friend_id)
                .await?;
        if was_removed {
            if let Err(error) = self
                .pubsub
                .publish(
                    &UserChannel(claims.user_id()),
                    UserChannelMessage::UsersAreNoLongerFriends { friend_id },
                )
                .await
            {
                tracing::error!(%error, "Failed to publish message");
            }
            if let Err(error) = self
                .pubsub
                .publish(
                    &UserChannel(friend_id),
                    UserChannelMessage::UsersAreNoLongerFriends {
                        friend_id: claims.user_id(),
                    },
                )
                .await
            {
                tracing::error!(%error, "Failed to publish message");
            }
        }
        Ok(Response::new(RemoveFriendResponse {}))
    }

    async fn mark_friend_as_favorite(
        &self,
        request: Request<MarkFriendAsFavoriteRequest>,
    ) -> Result<Response<MarkFriendAsFavoriteResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let MarkFriendAsFavoriteRequest {
            user_id: friend_id,
            favorite,
        } = request.into_inner();
        friend_queries::mark_friend_as_favorite(
            &self.db_connection_pool,
            claims.user_id(),
            UserId::from(friend_id),
            favorite,
        )
        .await?;

        Ok(Response::new(MarkFriendAsFavoriteResponse {}))
    }

    async fn create_group(
        &self,
        request: Request<CreateGroupRequest>,
    ) -> Result<Response<CreateGroupResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let CreateGroupRequest { group_name } = request.into_inner();

        if group_name.len() < 4 {
            return Err(Status::invalid_argument(
                "Group name needs to be at least 4 characters.",
            ));
        }

        let user_data = sqlx::query!(
            "SELECT display_name FROM users WHERE id = $1",
            claims.user_id() as UserId
        )
        .fetch_one(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        let group_id =
            group_queries::create_group(&self.db_connection_pool, claims.user_id(), &group_name)
                .await?;

        Ok(Response::new(CreateGroupResponse {
            created: Some(Group {
                group_id,
                group_name,
                created_by_name: user_data.display_name,
                created_by_user_id: claims.user_id().into(),
                favorite: false,
                no_of_group_members: 1,
                deleted_at: None,
            }),
        }))
    }

    async fn request_group_membership(
        &self,
        request: Request<RequestGroupMembershipRequest>,
    ) -> Result<Response<RequestGroupMembershipResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let RequestGroupMembershipRequest {
            group_id: group_to_join,
        } = request.into_inner();

        friend_queries::add_group_member_request(
            &self.db_connection_pool,
            claims.user_id(),
            group_to_join,
        )
        .await?;

        Ok(Response::new(RequestGroupMembershipResponse {}))
    }

    async fn remove_group(
        &self,
        request: Request<RemoveGroupRequest>,
    ) -> Result<Response<RemoveGroupResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let RemoveGroupRequest { group_id } = request.into_inner();

        let existing_group = sqlx::query!(
            r#"SELECT created_by as "created_by: UserId" FROM groups WHERE id = $1 AND deleted_at IS NULL"#,
            group_id
        )
        .fetch_one(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        if existing_group.created_by != claims.user_id() {
            return Err(Status::permission_denied(
                ErrorCode::NotEnoughPermissions.to_response(),
            ));
        }
        let previous_group_members = sqlx::query_scalar!(
            r#"WITH deleted as (UPDATE groups SET deleted_at = NOW() WHERE id = $1 AND created_by = $2 RETURNING id)
               SELECT user_id as "user_id: UserId" FROM group_member gm JOIN deleted d ON d.id = gm.group_id"#,
            group_id,
            claims.user_id() as UserId
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        for user_id in previous_group_members {
            if let Err(error) = self
                .pubsub
                .publish(
                    &UserChannel(user_id),
                    UserChannelMessage::UserLeftGroup { group_id },
                )
                .await
            {
                tracing::error!(%error, "Failed to publish message");
            }
        }

        Ok(Response::new(RemoveGroupResponse {}))
    }

    async fn get_groups(
        &self,
        request: Request<GetGroupsRequest>,
    ) -> Result<Response<GetGroupsResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;

        let groups = sqlx::query!(
            r#"SELECT groups.id, groups.name, group_member.favorite,
                      groups.deleted_at as group_deleted_at, group_member.deleted_at as member_deleted_at,
                      groups.created_by as "created_by: UserId", users.username as created_by_name,
                      (SELECT COUNT(*) FROM group_member WHERE group_id = groups.id) AS "no_of_members!"
            FROM group_member
            JOIN groups ON group_member.group_id = groups.id
            JOIN users ON groups.created_by = users.id
            WHERE group_member.user_id = $1"#,
            claims.user_id() as UserId
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?
        .into_iter()
        .map(|group| Group {
            group_id: group.id,
            group_name: group.name,
            created_by_name: group.created_by_name,
            created_by_user_id: group.created_by.into(),
            favorite: group.favorite,
            deleted_at: group.member_deleted_at.or(group.group_deleted_at).map(offset_date_time_into_timestamp),
            no_of_group_members: group
                .no_of_members
                .try_into()
                .expect("Count should always return a positive number"),
        })
        .collect();

        Ok(Response::new(GetGroupsResponse { groups }))
    }

    async fn get_group_detail(
        &self,
        request: Request<GetGroupDetailRequest>,
    ) -> Result<Response<GetGroupDetailResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let user_id = claims.user_id();
        let GetGroupDetailRequest { group_id } = request.into_inner();

        #[derive(Debug, Clone)]
        struct GroupMemberEntity {
            user_id: UserId,
            username: String,
            role: GroupRole,
            favorite: bool,
            since: OffsetDateTime,
            deleted_at: Option<OffsetDateTime>,
            last_seen: Option<OffsetDateTime>,
            online: Option<bool>,
        }

        let get_group_members = sqlx::query_as!(
            GroupMemberEntity,
            r#"SELECT group_member.user_id, username, role as "role: _", favorite, since, deleted_at, online as "online?", last_seen as "last_seen?" FROM group_member
            JOIN users ON group_member.user_id = users.id
            LEFT JOIN user_online ON user_online.user_id = users.id
            WHERE group_id = $1
            ORDER BY user_id ASC"#,
            group_id
        )
        .fetch_all(&self.db_connection_pool);

        let get_group = sqlx::query!(
            r#"SELECT created_by as "created_by: UserId", name FROM groups
            WHERE id = $1"#,
            group_id
        )
        .fetch_one(&self.db_connection_pool);

        let (group_res, member_data_res) = tokio::join!(get_group, get_group_members);

        let group = group_res.map_err(AppError::from)?;
        let group_creator_id = group.created_by;

        let member_data: Vec<GroupMemberEntity> = member_data_res.map_err(AppError::from)?;

        let requesting_user_member_data = match member_data
            .iter()
            .find(|entity| entity.user_id == user_id)
        {
            Some(data) => data,
            None => {
                return Err(AppError::permission_denied(
                    format!("User {user_id} is not permitted to see the group details because they are not a member.")).into());
            }
        };
        let favorite = requesting_user_member_data.favorite;

        let members = member_data
            .into_iter()
            .map(
                |GroupMemberEntity {
                     user_id,
                     username,
                     role,
                     favorite: _,
                     since,
                     deleted_at,
                     last_seen,
                     online,
                 }| GroupMember {
                    user_id: user_id.into(),
                    user_name: username,
                    role: if group_creator_id == user_id {
                        Role::Creator
                    } else {
                        Role::from(role)
                    } as i32,
                    since: Some(offset_date_time_into_timestamp(since)),
                    deleted_at: deleted_at.map(offset_date_time_into_timestamp),
                    last_seen: last_seen.map(offset_date_time_into_timestamp),
                    is_online: online.unwrap_or(false),
                },
            )
            .collect();

        Ok(Response::new(GetGroupDetailResponse {
            members,
            group_name: group.name,
            favorite,
        }))
    }

    async fn add_to_group(
        &self,
        request: Request<AddToGroupRequest>,
    ) -> Result<Response<AddToGroupResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let AddToGroupRequest { group_id, user_id } = request.into_inner();

        let requesting_user = sqlx::query_as!(
            RoleWrapper,
            r#"SELECT role as "role: _" FROM group_member
            WHERE user_id = $1 AND group_id = $2"#,
            claims.user_id() as UserId,
            group_id
        )
        .fetch_one(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        if requesting_user.role == GroupRole::Member {
            return Err(Status::permission_denied(
                ErrorCode::NotEnoughPermissions.to_response(),
            ));
        }

        sqlx::query!(
            r#"INSERT INTO group_member (user_id, group_id, role, favorite) VALUES ($1, $2, 'member', false)"#,
            user_id as i64,
            group_id
        )
        .execute(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        if let Err(error) = self
            .pubsub
            .publish(
                &UserChannel(user_id.into()),
                UserChannelMessage::UserJoinedGroup { group_id },
            )
            .await
        {
            tracing::error!(%error, "Failed to publish message");
        }

        Ok(Response::new(AddToGroupResponse {}))
    }

    async fn remove_from_group(
        &self,
        request: Request<RemoveFromGroupRequest>,
    ) -> Result<Response<RemoveFromGroupResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let RemoveFromGroupRequest { user_id, group_id } = request.into_inner();

        if user_id != claims.user_id() {
            let requesting_user = sqlx::query_as!(
                RoleWrapper,
                r#"SELECT role as "role: _" FROM group_member
                WHERE user_id = $1 AND group_id = $2"#,
                claims.user_id() as UserId,
                group_id
            )
            .fetch_one(&self.db_connection_pool)
            .await
            .map_err(AppError::from)?;

            if requesting_user.role == GroupRole::Member {
                return Err(Status::permission_denied(
                    ErrorCode::NotEnoughPermissions.to_response(),
                ));
            }
        }

        sqlx::query!(
            r#"DELETE FROM group_member WHERE user_id = $1 AND group_id = $2 RETURNING id"#,
            user_id as i64,
            group_id
        )
        .fetch_one(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        if let Err(error) = self
            .pubsub
            .publish(
                &UserChannel(user_id.into()),
                UserChannelMessage::UserLeftGroup { group_id },
            )
            .await
        {
            tracing::error!(%error, "Failed to publish message");
        }

        Ok(Response::new(RemoveFromGroupResponse {}))
    }

    async fn mark_group_as_favorite(
        &self,
        request: Request<MarkGroupAsFavoriteRequest>,
    ) -> Result<Response<MarkGroupAsFavoriteResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let MarkGroupAsFavoriteRequest { group_id, favorite } = request.into_inner();
        sqlx::query!(
            r#"UPDATE group_member SET favorite = $1 WHERE user_id = $2 AND group_id = $3"#,
            favorite,
            claims.user_id() as UserId,
            group_id
        )
        .execute(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        Ok(Response::new(MarkGroupAsFavoriteResponse {}))
    }

    async fn get_chat_messages(
        &self,
        request: Request<GetChatMessagesRequest>,
    ) -> Result<Response<GetChatMessagesResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let GetChatMessagesRequest {
            since,
            until,
            limit,
            group_id,
            friend_id,
        } = request.into_inner();
        let since = since
            .and_then(timestamp_into_offset_date_time)
            .unwrap_or(OffsetDateTime::UNIX_EPOCH);
        let until = until
            .and_then(timestamp_into_offset_date_time)
            // Arbitrary extra time to avoid race conditions with messages coming inbetween this line and the end of this function
            // Does not need to be configurable
            .unwrap_or(OffsetDateTime::now_utc() + Duration::from_secs(60));
        let limit = limit
            .unwrap_or(u32::MAX)
            .min(self.settings.max_chat_messages_limit);

        let mut friend_messages: BTreeMap<UserId, Vec<proto::ChatMessage>> = BTreeMap::new();
        let mut group_messages: BTreeMap<i64, Vec<proto::ChatMessage>> = BTreeMap::new();

        let new_chat_messages = chat_queries::get_chat_messages(
            &self.db_connection_pool,
            claims.user_id(),
            since,
            until,
            limit,
            group_id,
            friend_id.map(UserId::from),
        )
        .await?;

        fn chat_message_to_proto(
            entity: chat_queries::PrivateChatMessageEntity,
        ) -> proto::ChatMessage {
            proto::ChatMessage {
                text: entity.text.clone(),
                from_user: entity.from_user.into(),
                sent_at: Some(offset_date_time_into_timestamp(entity.sent_at)),
                updated_at: entity.updated_at.map(offset_date_time_into_timestamp),
                tags: entity
                    .references
                    .clone()
                    .into_iter()
                    .filter_map(|(user_id, group_id, span)| {
                        let kind = match (user_id, group_id) {
                            (Some(user_id), _) => data::proto::tag::Kind::UserId(user_id.into()),
                            (_, Some(group_id)) => data::proto::tag::Kind::GroupId(group_id),
                            _ => {
                                return None;
                            }
                        };
                        let span_from = span.start_inclusive().try_into().ok()?;
                        let span_to = span.end_inclusive().try_into().ok()?;
                        Some(data::proto::Tag {
                            kind: Some(kind),
                            span_from,
                            span_to,
                        })
                    })
                    .collect(),
            }
        }

        for chat_message in new_chat_messages {
            match chat_message.to_group {
                Some(group_id) => {
                    let messages = group_messages.entry(group_id).or_default();
                    messages.push(chat_message_to_proto(chat_message))
                }
                None => {
                    // We need to figure out if to_user or from_user is the current user
                    // to set the channel correctly
                    for friend_id in chat_message
                        .to_user
                        .iter()
                        .map(|to_user| PrivateChannel::new(*to_user, chat_message.from_user))
                        .filter_map(|private_channel| private_channel.other_than(claims.user_id()))
                    {
                        let messages = friend_messages.entry(friend_id).or_default();
                        messages.push(chat_message_to_proto(chat_message.clone()))
                    }
                }
            }
        }

        let friend_messages = friend_messages
            .into_iter()
            .map(|(friend_id, messages)| FriendChannelMessages {
                friend_id: friend_id.into(),
                messages,
            })
            .collect();
        let group_messages = group_messages
            .into_iter()
            .map(|(group_id, messages)| GroupChannelMessages { group_id, messages })
            .collect();

        Ok(Response::new(GetChatMessagesResponse {
            friend_messages,
            group_messages,
        }))
    }

    async fn parse_chat_message(
        &self,
        request: Request<ParseChatMessageRequest>,
    ) -> Result<Response<ParseChatMessageResponse>, Status> {
        let ParseChatMessageRequest { text } = request.into_inner();

        let chat_message = ChatMessage::new(text);
        let valid_tags =
            chat_message::keep_valid_tags(&self.db_connection_pool, chat_message.into_tags())
                .await?;

        let tags = valid_tags.into_iter().map(proto_from_tag).collect();

        Ok(Response::new(ParseChatMessageResponse { tags }))
    }
}

#[derive(sqlx::Type, Debug, Clone, PartialEq, Eq)]
#[sqlx(type_name = "grouprole", rename_all = "lowercase")]
enum GroupRole {
    Admin,
    Member,
}

impl From<GroupRole> for Role {
    fn from(group_role: GroupRole) -> Self {
        match group_role {
            GroupRole::Admin => Role::Admin,
            GroupRole::Member => Role::Member,
        }
    }
}

struct RoleWrapper {
    role: GroupRole,
}
