use std::time::Duration;

use serde::Deserialize;

#[derive(Debug, Deserialize, Clone, Copy)]
pub struct Settings {
    /// How many chat messages can be retrieved with a single request, per channel
    pub max_chat_messages_limit: u32,
    /// We store a last_seen timestamp in our database - this configures how often we update
    /// the timestamp for a given open websocket connection.
    pub user_last_seen_update_interval_in_ms: u32,
}

impl Settings {
    #[cfg(test)]
    pub fn read() -> Result<Self, klink::Error> {
        use std::env;

        #[derive(Deserialize)]
        struct WrappedConfig {
            chatot: Settings,
        }
        let wrapped_config: WrappedConfig = {
            let root_path = env!("CARGO_MANIFEST_DIR");
            klink::read_config_from_dir(format!("{root_path}/config"))?
        };
        Ok(wrapped_config.chatot)
    }

    pub fn user_last_seen_interval(&self) -> Duration {
        Duration::from_millis(self.user_last_seen_update_interval_in_ms.into())
    }
}
