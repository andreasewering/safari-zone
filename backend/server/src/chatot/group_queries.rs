use sqlx::PgPool;

use crate::{data::UserId, error::AppResult};

pub async fn create_group(pool: &PgPool, user_id: UserId, group_name: &str) -> AppResult<i64> {
    let mut transaction = pool.begin().await?;

    let created = sqlx::query!(
        r#"INSERT INTO groups (name, created_by) VALUES ($1, $2) RETURNING id"#,
        group_name,
        user_id as UserId,
    )
    .fetch_one(&mut *transaction)
    .await?;

    sqlx::query!(
      r#"INSERT INTO group_member (user_id, group_id, role, favorite) VALUES ($1, $2, 'admin', false)"#,
      user_id as UserId,
      created.id
  )
  .execute(&mut *transaction)
  .await?;

    transaction.commit().await?;
    Ok(created.id)
}
