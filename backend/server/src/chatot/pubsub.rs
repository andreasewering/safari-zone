use std::fmt::Display;

use serde::{Deserialize, Serialize};

use crate::{
    data::UserId,
    pubsub::{Channel, JsonEncoding},
};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ChatChannelMessage {
    SentChatMessage,
    StartedTyping { from_user_id: UserId },
    StoppedTyping { from_user_id: UserId },
    WentOnline { user_id: UserId },
    WentOffline { user_id: UserId },
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum UserChannelMessage {
    WentOnline { connection_id: i64 },
    UsersBecameFriends { friend_id: UserId },
    UserJoinedGroup { group_id: i64 },
    UserLeftGroup { group_id: i64 },
    UsersAreNoLongerFriends { friend_id: UserId },
}

/// Global channel for communication between servers on behalf of a specific user.
#[derive(Clone, Copy)]
pub struct UserChannel(pub UserId);

/// Channel for user to user communication
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum ChatChannel {
    Group { group_id: i64 },
    Private(PrivateChannel),
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct PrivateChannel(UserId, UserId);

impl PrivateChannel {
    pub fn new(user_1: UserId, user_2: UserId) -> Self {
        if user_1 < user_2 {
            Self(user_1, user_2)
        } else {
            Self(user_2, user_1)
        }
    }

    pub fn other_than(&self, user_id: UserId) -> Option<UserId> {
        if self.0 == user_id {
            return Some(self.1);
        }
        if self.1 == user_id {
            return Some(self.0);
        }
        None
    }

    fn first(&self) -> UserId {
        self.0
    }

    fn second(&self) -> UserId {
        self.1
    }
}

impl Display for ChatChannel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.name().into().as_ref())
    }
}

impl Channel for ChatChannel {
    type Message = ChatChannelMessage;
    type Encoding = JsonEncoding<ChatChannelMessage>;

    fn name(&self) -> impl Into<String> {
        match self {
            ChatChannel::Group { group_id } => format!("chat_grp_{group_id}"),
            ChatChannel::Private(private) => {
                format!("chat_prv_{}_{}", private.first(), private.second())
            }
        }
    }
}

impl Display for UserChannel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.name().into().as_ref())
    }
}

impl Channel for UserChannel {
    type Message = UserChannelMessage;
    type Encoding = JsonEncoding<UserChannelMessage>;

    fn name(&self) -> impl Into<String> {
        let user_id = self.0;
        format!("chatot_{user_id}")
    }
}
