use sqlx::PgPool;
use time::OffsetDateTime;

use crate::{data::UserId, error::AppResult};

/**
 * General ideas here:
 * - Friends are a symmetric relationship. We still store the data twice because there is additional metadata to the friendship which may differ
 *   (e.g. is the friend a "favorite" friend)
 * - Non-symmetric relationships are modelled via "Friend Requests"
 * - We must make sure that two users are either
 *   - not in the tables friend & friend_request at all
 *   - in a singular entry in friend_request
 *   - in two entries in friend (both directions)   
 */

#[derive(Debug, PartialEq, Eq)]
pub struct FriendEntity {
    pub friend_id: UserId,
    pub username: String,
    pub since: OffsetDateTime,
    pub favorite: bool,
    pub deleted_at: Option<OffsetDateTime>,
    pub last_seen: Option<OffsetDateTime>,
    pub online: Option<bool>,
}

pub async fn get_friends(pool: &PgPool, user_id: UserId) -> AppResult<Vec<FriendEntity>> {
    let friends = sqlx::query_as!(
        FriendEntity,
        r#"SELECT to_user_id as friend_id, username, since,
                  deleted_at, favorite, last_seen as "last_seen?", online as "online?" 
           FROM friend
           JOIN users ON users.id = friend.to_user_id
           LEFT JOIN user_online ON user_online.user_id = friend.to_user_id 
           WHERE from_user_id = $1"#,
        user_id as UserId
    )
    .fetch_all(pool)
    .await?;
    Ok(friends)
}

#[derive(Debug, PartialEq, Eq)]
pub struct IncomingFriendRequest {
    pub from_user_id: UserId,
    pub user_name: String,
    pub issued_at: OffsetDateTime,
}

#[derive(Debug, PartialEq, Eq)]
pub struct OutgoingFriendRequest {
    pub to_user_id: UserId,
    pub user_name: String,
    pub issued_at: OffsetDateTime,
}

pub async fn get_friend_requests(
    pool: &PgPool,
    user_id: UserId,
) -> AppResult<(Vec<IncomingFriendRequest>, Vec<OutgoingFriendRequest>)> {
    let incoming = sqlx::query!(
        r#"SELECT from_user_id as "from_user_id: UserId", username, issued_at FROM friend_request
           JOIN users ON users.id = friend_request.from_user_id
           WHERE to_user_id = $1"#,
        user_id as UserId
    )
    .fetch_all(pool)
    .await?
    .into_iter()
    .map(|r| IncomingFriendRequest {
        from_user_id: r.from_user_id,
        user_name: r.username,
        issued_at: r.issued_at,
    })
    .collect();
    let outgoing = sqlx::query!(
        r#"SELECT to_user_id as "to_user_id: UserId", username, issued_at FROM friend_request
           JOIN users ON users.id = friend_request.to_user_id
           WHERE from_user_id = $1"#,
        user_id as UserId
    )
    .fetch_all(pool)
    .await?
    .into_iter()
    .map(|r| OutgoingFriendRequest {
        to_user_id: r.to_user_id,
        user_name: r.username,
        issued_at: r.issued_at,
    })
    .collect();

    Ok((incoming, outgoing))
}

pub async fn add_group_member_request(
    pool: &PgPool,
    user_id: UserId,
    group_to_join: i64,
) -> AppResult<()> {
    // Do not insert into group_member_request if the user is already a member of the group
    sqlx::query!(
        r#"INSERT INTO group_member_request (user_id, group_id) SELECT $1, $2 WHERE NOT EXISTS (
        SELECT 1
        FROM group_member
        WHERE user_id = $1 AND group_id = $2 AND deleted_at IS NULL
    )"#,
        user_id as UserId,
        group_to_join,
    )
    .execute(pool)
    .await?;
    Ok(())
}

#[derive(Debug, Copy, Clone)]
pub enum AddFriendResult {
    NoChange,
    NewRequest,
    AcceptedRequest,
}

/// Establish a friend relationship between two users.
/// This results in an entry in friend_request if there was no relationship there yet,
/// no change at all if the users were already friends to begin with,
/// or the existing friend_request originated from the same user as the current attempt,
/// and the migration of the friend_request to the friend table if a friend_request was open.
///
/// The return type communicates which case was triggered.
pub async fn add_friend(
    pool: &PgPool,
    user_id: UserId,
    friend_id: UserId,
) -> AppResult<AddFriendResult> {
    let existing = sqlx::query!(
        r#"SELECT true as "is_friend!", to_user_id as "to_user_id!", from_user_id as "from_user_id!" FROM friend 
            WHERE from_user_id = $1 AND to_user_id = $2 
          UNION SELECT false as "is_friend!", to_user_id as "to_user_id!", from_user_id as "from_user_id!" FROM friend_request 
            WHERE (from_user_id = $1 AND to_user_id = $2) OR (from_user_id = $2 AND to_user_id = $1)"#,
        user_id as UserId,
        friend_id as UserId
    )
    .fetch_optional(pool)
    .await?;
    if let Some(friend_or_friend_request) = existing {
        let from_user_id = friend_or_friend_request.from_user_id as u64;
        let to_user_id = friend_or_friend_request.to_user_id as u64;
        if friend_or_friend_request.is_friend {
            tracing::info!(message = "Tried to add existing friend", %from_user_id, %to_user_id);
            return Ok(AddFriendResult::NoChange);
        }
        if from_user_id == user_id {
            tracing::debug!(message = "Received a duplicate friend request, leaving the original one untouched", %from_user_id, %to_user_id);
            return Ok(AddFriendResult::NoChange);
        }
        tracing::debug!(message = "Accepting friend request", %from_user_id, %to_user_id);
        let mut transaction = pool.begin().await?;
        sqlx::query!(
            "DELETE FROM friend_request WHERE from_user_id = $1",
            friend_id as UserId
        )
        .execute(&mut *transaction)
        .await?;

        sqlx::query!(
            "INSERT INTO friend (from_user_id, to_user_id) VALUES ($1, $2), ($2, $1)",
            user_id as UserId,
            friend_id as UserId
        )
        .execute(&mut *transaction)
        .await?;

        transaction.commit().await?;
        return Ok(AddFriendResult::AcceptedRequest);
    }

    // no existing friend or friend request -> add new friend request
    sqlx::query!(
        "INSERT INTO friend_request (from_user_id, to_user_id) VALUES ($1, $2)",
        user_id as UserId,
        friend_id as UserId
    )
    .execute(pool)
    .await?;

    Ok(AddFriendResult::NewRequest)
}

/// Remove a friend or friend_request.
/// Returns true if a friend was removed and false otherwise.
pub async fn remove_friend(pool: &PgPool, user_id: UserId, friend_id: UserId) -> AppResult<bool> {
    let mut transaction = pool.begin().await?;
    let delete_friend_result = sqlx::query!(
        "UPDATE friend
         SET deleted_at = NOW()
         WHERE ((from_user_id = $1 AND to_user_id = $2) OR (from_user_id = $2 AND to_user_id = $1))
         AND deleted_at IS NULL",
        user_id as UserId,
        friend_id as UserId
    )
    .execute(&mut *transaction)
    .await?;
    sqlx::query!(
        "DELETE FROM friend_request WHERE (from_user_id = $1 AND to_user_id = $2) OR (from_user_id = $2 AND to_user_id = $1)",
        user_id as UserId,
        friend_id as UserId
    )
    .execute(&mut *transaction)
    .await?;

    transaction.commit().await?;
    Ok(delete_friend_result.rows_affected() > 0)
}

pub async fn mark_friend_as_favorite(
    pool: &PgPool,
    user_id: UserId,
    friend_id: UserId,
    favorite: bool,
) -> AppResult<()> {
    sqlx::query!(
        r#"UPDATE friend SET favorite = $1 WHERE from_user_id = $2 AND to_user_id = $3 AND deleted_at IS NULL"#,
        favorite,
        user_id as UserId,
        friend_id as UserId
    )
    .execute(pool)
    .await?;
    Ok(())
}
