use sqlx::postgres::types::PgRange;
use time::OffsetDateTime;

use crate::chat_message::ChatMessage;
use crate::{data::UserId, error::AppResult, range::IntRange};

#[derive(Debug, Copy, Clone)]
pub struct ConnectionIds {
    pub previous_connection_id: Option<i64>,
    pub current_connection_id: i64,
}

/// Marks a user online in the database.
/// Returns the current and the last "connection_id", which is used to
/// guarantee only one active connection at a time.
/// The advantage of this approach (over just closing any new connection when the user is already online) is,
/// that the new connection "wins" and we can close the old connection which we can identify via its connection_id.
/// The unique id mechanism prevents us from accidentally closing our own connection.
pub async fn set_online(pool: &sqlx::PgPool, user_id: UserId) -> AppResult<ConnectionIds> {
    let connection_ids = sqlx::query_as!(
        ConnectionIds,
        r#"WITH previous AS (SELECT connection_id FROM user_online WHERE user_id = $1 AND online = true)
        INSERT INTO user_online (user_id, last_seen, online) VALUES ($1, NOW(), true)
        ON CONFLICT (user_id) DO UPDATE SET
            connection_id = EXCLUDED.connection_id,
            last_seen = EXCLUDED.last_seen,
            online = EXCLUDED.online
        RETURNING
            (SELECT connection_id FROM previous) as previous_connection_id,
            connection_id as current_connection_id"#,
        user_id as UserId
    )
    .fetch_one(pool)
    .await?;
    Ok(connection_ids)
}

pub async fn update_online_timestamp(pool: &sqlx::PgPool, user_id: UserId) -> AppResult<()> {
    sqlx::query!(
        "UPDATE user_online SET last_seen = NOW(), online = true WHERE user_id = $1",
        user_id as UserId
    )
    .execute(pool)
    .await?;
    Ok(())
}

pub async fn set_offline(pool: &sqlx::PgPool, user_id: UserId) -> AppResult<()> {
    sqlx::query!(
        "UPDATE user_online SET online = false WHERE user_id = $1",
        user_id as UserId
    )
    .execute(pool)
    .await?;
    Ok(())
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct PrivateChatMessageEntity {
    pub text: String,
    pub from_user: UserId,
    pub to_user: Vec<UserId>,
    pub to_group: Option<i64>,
    pub sent_at: OffsetDateTime,
    pub updated_at: Option<OffsetDateTime>,
    pub references: Vec<(Option<UserId>, Option<i64>, IntRange)>,
}

pub async fn get_chat_messages(
    pool: &sqlx::PgPool,
    user_id: UserId,
    since: OffsetDateTime,
    until: OffsetDateTime,
    limit: u32,
    group_id: Option<i64>,
    friend_id: Option<UserId>,
) -> AppResult<Vec<PrivateChatMessageEntity>> {
    // I think this monstrosity of a SQL query deserves an explanation.
    // The general idea here is that we query for chat messages often, and it should be more efficient
    // to batch new messages together instead of receiving them one by one.
    // The frontend can get pinged with the cheap info that there *exist* new messages
    // and we can decide when we want to actually load them (e.g. when the user clicks on a chat etc.).
    //
    // We also resolve tags with the same query to avoid n + 1 queries
    // and look for group messages and private messages with the same query
    //
    // Lastly we use a postgres window function to allow limiting the amount of message received PER CHANNEL.
    // This is a cool feature since it allows us to do stuff like loading just the latest message of every channel,
    // regardless of when it was sent.
    let messages = sqlx::query_as!(
        PrivateChatMessageEntity,
        r#"WITH ranked_messages AS (
            SELECT c.text, c.from_user, COALESCE(array_agg(DISTINCT r.to_user), '{}') as to_user, r.to_group, c.sent_at, c.updated_at, 
                COALESCE(
                    array_agg(DISTINCT ROW(u.referenced_user_id, NULL::bigint, u.span)) 
                        FILTER (WHERE u.referenced_user_id IS NOT NULL)
                    ||
                    array_agg(DISTINCT ROW(NULL::bigint, g.referenced_group_id, g.span))
                        FILTER (WHERE g.referenced_group_id IS NOT NULL)  
                , '{}') as refs,
                ROW_NUMBER() OVER (
                    PARTITION BY CASE
                    WHEN r.to_group IS NOT NULL THEN row(r.to_group, NULL::bigint, NULL::bigint)
                    ELSE row(NULL::bigint, LEAST(c.from_user, MAX(r.to_user)), GREATEST(c.from_user, MAX(r.to_user)))
                    END
                    ORDER BY c.sent_at DESC
                ) AS channel_number
            FROM chat_message c
            JOIN chat_message_receiver r ON r.message_id = c.id
            LEFT JOIN tagged_user u ON u.message_id = c.id
            LEFT JOIN tagged_group g ON g.message_id = c.id
            WHERE (r.to_user = $1 OR c.from_user = $1)
            AND ($5::bigint IS NULL OR to_group = $5)
            AND ($6::bigint IS NULL OR (r.to_user = $6 OR c.from_user = $6))
            AND ((c.sent_at > $2 AND c.sent_at < $3) OR (c.updated_at > $2 AND c.updated_at < $3))
            GROUP BY c.id, r.to_group
        )
        SELECT text, from_user,
               to_user as "to_user!: _",
               to_group, sent_at, updated_at,
               refs as "references!: _"
        FROM ranked_messages
        WHERE channel_number <= $4
        "#,
        user_id as UserId,
        since, until, limit as i32, group_id, friend_id as Option<UserId>
    )
    .fetch_all(pool)
    .await?;
    Ok(messages)
}

/// Send a private message to a friend.
/// Returns Ok(false) if the two users are not in a `friend` relationship.
/// Tags are automatically validated on insertion.
pub async fn send_private_message(
    pool: &sqlx::PgPool,
    from_user: UserId,
    to_user: UserId,
    chat_message: ChatMessage,
) -> AppResult<bool> {
    let referenced_users = chat_message.referenced_users();
    let referenced_user_ids: Vec<UserId> = referenced_users
        .iter()
        .map(|located| *located.tag())
        .collect();
    let referenced_user_spans: Vec<PgRange<i32>> = referenced_users
        .iter()
        .map(|located| located.span())
        .map(|(start, end)| PgRange::from(start..=end))
        .collect();

    let referenced_groups = chat_message.referenced_groups();
    let referenced_group_ids: Vec<i64> = referenced_groups
        .iter()
        .map(|located| *located.tag())
        .collect();
    let referenced_group_spans: Vec<PgRange<i32>> = referenced_groups
        .iter()
        .map(|located| located.span())
        .map(|(start, end)| PgRange::from(start..=end))
        .collect();

    let may_message_id = sqlx::query!(
        r#"WITH
        friend_exists AS (SELECT 1 FROM friend WHERE from_user_id = $2 AND to_user_id = $3 AND deleted_at IS NULL),
        inserted AS (INSERT INTO chat_message (text, from_user) SELECT $1, $2 FROM friend_exists RETURNING id),
        group_tags AS (
            INSERT INTO tagged_group (message_id, referenced_group_id, span)
            SELECT i.id, g.referenced_group_id, g.span
            FROM inserted i
            CROSS JOIN UNNEST($4::bigint[], $5::int4range[]) AS g(referenced_group_id, span)
        ),
        user_tags AS (
            INSERT INTO tagged_user (message_id, referenced_user_id, span)
            SELECT i.id, g.referenced_user_id, g.span
            FROM inserted i
            CROSS JOIN UNNEST($6::bigint[], $7::int4range[]) AS g(referenced_user_id, span)
        )
        INSERT INTO chat_message_receiver (message_id, to_user)
        SELECT id, $3 FROM inserted
        RETURNING message_id"#,
        chat_message.text(),
        from_user as UserId,
        to_user as UserId,
        &referenced_group_ids,
        &referenced_group_spans,
        referenced_user_ids as Vec<UserId>,
        &referenced_user_spans,
    )
    .fetch_optional(pool)
    .await?;
    Ok(may_message_id.is_some())
}

/// Send a message to a group.
/// Returns Ok(false) if no message was sent because the user was not in the given group.
/// Tags are automatically validated on insertion.
pub async fn send_group_message(
    pool: &sqlx::PgPool,
    from_user: UserId,
    to_group: i64,
    chat_message: ChatMessage,
) -> AppResult<bool> {
    let referenced_users = chat_message.referenced_users();
    let referenced_user_ids: Vec<UserId> = referenced_users
        .iter()
        .map(|located| *located.tag())
        .collect();
    let referenced_user_spans: Vec<PgRange<i32>> = referenced_users
        .iter()
        .map(|located| located.span())
        .map(|(start, end)| PgRange::from(start..=end))
        .collect();

    let referenced_groups = chat_message.referenced_groups();
    let referenced_group_ids: Vec<i64> = referenced_groups
        .iter()
        .map(|located| *located.tag())
        .collect();
    let referenced_group_spans: Vec<PgRange<i32>> = referenced_groups
        .iter()
        .map(|located| located.span())
        .map(|(start, end)| PgRange::from(start..=end))
        .collect();

    let result = sqlx::query!(
        r#"WITH
        is_group_member AS (SELECT 1 FROM group_member WHERE group_id = $3 AND user_id = $2 AND deleted_at IS NULL),
        inserted AS (INSERT INTO chat_message (text, from_user) SELECT $1, $2 FROM is_group_member RETURNING id),
        group_tags AS (
            INSERT INTO tagged_group (message_id, referenced_group_id, span)
            SELECT i.id, g.referenced_group_id, g.span
            FROM inserted i
            CROSS JOIN UNNEST($4::bigint[], $5::int4range[]) AS g(referenced_group_id, span)
        ),
        user_tags AS (
            INSERT INTO tagged_user (message_id, referenced_user_id, span)
            SELECT i.id, g.referenced_user_id, g.span
            FROM inserted i 
            CROSS JOIN UNNEST($6::bigint[], $7::int4range[]) AS g(referenced_user_id, span)
        )
        INSERT INTO chat_message_receiver (message_id, to_user, to_group)
        SELECT inserted.id, group_member.user_id, group_member.group_id FROM inserted
        CROSS JOIN group_member WHERE group_id = $3"#,
        chat_message.text(),
        from_user as UserId,
        to_group,
        &referenced_group_ids,
        &referenced_group_spans,
        referenced_user_ids as Vec<UserId>,
        &referenced_user_spans,
    )
    .execute(pool)
    .await?;
    let recipients = result.rows_affected();
    if recipients > 0 {
        tracing::debug!(%recipients, "Sent group message");
        Ok(true)
    } else {
        Ok(false)
    }
}
