use std::fmt::Display;
use std::time::Duration;

use axum::extract::ws::{self, Utf8Bytes, WebSocket};
use futures::{Sink, SinkExt, StreamExt};
use tokio_stream::StreamMap;

use super::proto::chatot::{
    StartedTyping, StoppedTyping, WebsocketError, WebsocketMessageToClient,
    WebsocketMessageToServer, channel, websocket_message_to_client, websocket_message_to_server,
};
use super::{Settings, proto};
use prost::Message;
use sqlx::PgPool;

use crate::chat_message::ChatMessage;
use crate::chatot::chat_queries;
use crate::chatot::pubsub::{
    ChatChannel, ChatChannelMessage, PrivateChannel, UserChannel, UserChannelMessage,
};
use crate::data::UserId;
use crate::gardevoir::{AuthCtx, Claims};
use crate::pubsub::PgPubSub;
use crate::shutdown;
use crate::util::encode_to_bytes;

fn channel_from_ws(
    channel: proto::chatot::Channel,
    current_user_id: UserId,
) -> Option<ChatChannel> {
    let kind = channel.kind?;
    let chat_channel = match kind {
        channel::Kind::Group(group_id) => ChatChannel::Group { group_id },
        channel::Kind::Private(user_id) => {
            ChatChannel::Private(PrivateChannel::new(UserId::from(user_id), current_user_id))
        }
    };
    Some(chat_channel)
}

fn channel_to_ws(
    chat_channel: &ChatChannel,
    current_user_id: UserId,
) -> Option<proto::chatot::Channel> {
    let kind = match chat_channel {
        ChatChannel::Group { group_id } => channel::Kind::Group(*group_id),
        ChatChannel::Private(private_channel) => {
            let other_user_id = private_channel.other_than(current_user_id)?;
            channel::Kind::Private(other_user_id.into())
        }
    };
    Some(proto::chatot::Channel { kind: Some(kind) })
}

fn pubsub_to_ws(
    chat_channel_msg: ChatChannelMessage,
    chat_channel: ChatChannel,
    current_user_id: UserId,
) -> Option<WebsocketMessageToClient> {
    let channel = Some(channel_to_ws(&chat_channel, current_user_id)?);
    let message = match chat_channel_msg {
        ChatChannelMessage::SentChatMessage => {
            websocket_message_to_client::Message::SentMessage(())
        }
        ChatChannelMessage::StartedTyping { from_user_id } => {
            if from_user_id == current_user_id {
                return None;
            }
            websocket_message_to_client::Message::StartedTyping(StartedTyping {
                from_user_id: from_user_id.into(),
                channel,
            })
        }
        ChatChannelMessage::StoppedTyping { from_user_id } => {
            if from_user_id == current_user_id {
                return None;
            }
            websocket_message_to_client::Message::StoppedTyping(StoppedTyping {
                from_user_id: from_user_id.into(),
                channel,
            })
        }
        ChatChannelMessage::WentOnline { user_id } => {
            if current_user_id == user_id {
                return None;
            }
            websocket_message_to_client::Message::WentOnline(user_id.into())
        }
        ChatChannelMessage::WentOffline { user_id } => {
            if current_user_id == user_id {
                return None;
            }
            websocket_message_to_client::Message::WentOffline(user_id.into())
        }
    };
    Some(WebsocketMessageToClient {
        message: Some(message),
    })
}

#[derive(Clone)]
pub struct WebsocketCtx {
    pub pubsub: PgPubSub,
    pub db_connection_pool: PgPool,
    pub auth_ctx: AuthCtx,
    pub shutdown_ctrl: shutdown::Ctrl,
    pub settings: Settings,
}

pub async fn on_close<S>(sink: &mut S, close_frame: ws::CloseFrame)
where
    S: Sink<ws::Message, Error = axum::Error> + Unpin,
{
    let send_result = sink.send(ws::Message::Close(Some(close_frame))).await;
    if let Err(error) = send_result {
        tracing::debug!(message = "Failed to send close code", %error);
    }
}

const CLOSE_FRAME_ERROR: ws::CloseFrame = ws::CloseFrame {
    code: ws::close_code::ERROR,
    reason: Utf8Bytes::from_static("Internal error"),
};

const CLOSE_FRAME_AWAY: ws::CloseFrame = ws::CloseFrame {
    code: ws::close_code::AWAY,
    reason: Utf8Bytes::from_static("Connected a second time"),
};

// This function deals with a single websocket connection, i.e., a single
// connected client / user, for which we will spawn two independent tasks (for
// receiving / sending chat messages).
pub async fn websocket(stream: WebSocket, claims: Claims, state: WebsocketCtx) {
    // By splitting, we can send and receive at the same time.
    let (mut sender, mut receiver) = stream.split();
    let own_user_id = claims.user_id();

    let (connection_ids, friends, groups) = tokio::join!(
        chat_queries::set_online(&state.db_connection_pool, own_user_id),
        sqlx::query_scalar!(
            r#"SELECT to_user_id as "to_user_id: UserId" FROM friend WHERE from_user_id = $1"#,
            own_user_id as UserId
        )
        .fetch_all(&state.db_connection_pool),
        sqlx::query_scalar!(
            "SELECT group_id FROM group_member WHERE user_id = $1",
            own_user_id as UserId
        )
        .fetch_all(&state.db_connection_pool)
    );
    let connection_ids = match connection_ids {
        Err(error) => {
            tracing::error!(%error, %own_user_id, "Failed to set user online");
            return on_close(&mut sender, CLOSE_FRAME_ERROR).await;
        }
        Ok(ids) => ids,
    };
    let friend_ids = match friends {
        Err(error) => {
            tracing::error!(%error, %own_user_id, "Failed to get friends");
            return on_close(&mut sender, CLOSE_FRAME_ERROR).await;
        }
        Ok(friend_ids) => friend_ids,
    };

    let group_ids = match groups {
        Err(error) => {
            tracing::error!(%error, %own_user_id, "Failed to get groups");
            return on_close(&mut sender, CLOSE_FRAME_ERROR).await;
        }
        Ok(group_ids) => group_ids,
    };
    let private_channels = friend_ids
        .iter()
        .copied()
        .map(|friend_id| ChatChannel::Private(PrivateChannel::new(friend_id, own_user_id)));
    let group_channels = group_ids
        .iter()
        .copied()
        .map(|group_id| ChatChannel::Group { group_id });
    let chat_channels: Vec<ChatChannel> = private_channels.chain(group_channels).collect();

    let mut chatot_stream = match state.pubsub.subscribe(&UserChannel(own_user_id)) {
        Err(error) => {
            tracing::error!(message = "Failed to subscribe to chatot channel", %error);
            return on_close(&mut sender, CLOSE_FRAME_ERROR).await;
        }
        Ok(sub) => sub,
    };

    let chat_channel_sub = |chat_channel| {
        state
            .pubsub
            .subscribe(&chat_channel)
            .unwrap_or(Box::pin(futures::stream::empty()))
            .map(move |chat_channel_msg| pubsub_to_ws(chat_channel_msg, chat_channel, own_user_id))
    };

    let mut stream_map = StreamMap::new();

    for chat_channel in chat_channels {
        stream_map.insert(chat_channel, chat_channel_sub(chat_channel));
    }

    if connection_ids.previous_connection_id.is_some() {
        let publish_result = state
            .pubsub
            .publish(
                &UserChannel(own_user_id),
                UserChannelMessage::WentOnline {
                    connection_id: connection_ids.current_connection_id,
                },
            )
            .await;
        if let Err(error) = publish_result {
            tracing::error!(message = "Failed to publish message", %error);
            on_close(&mut sender, CLOSE_FRAME_ERROR).await;
        }
    }

    for channel in stream_map.keys() {
        let publish_result = state
            .pubsub
            .publish(
                channel,
                ChatChannelMessage::WentOnline {
                    user_id: own_user_id,
                },
            )
            .await;
        if let Err(error) = publish_result {
            tracing::error!(%error, %channel, "Failed to publish message");
        }
    }

    let mut ping_interval = tokio::time::interval(Duration::from_secs(30));
    let mut last_seen_interval = tokio::time::interval(state.settings.user_last_seen_interval());

    let _prevent_shutdown = state
        .shutdown_ctrl
        .prevent_shutdown(format!("chatot::websocket_{own_user_id}",));
    let mut chat_channel_running = true;
    loop {
        tokio::select! {
            _ = state.shutdown_ctrl.cancelled() => {
                break;
            }
            _ = ping_interval.tick() => {
                if let Err(error) = sender.send(ws::Message::Ping(Default::default())).await {
                    tracing::debug!(message = "Failed to send ping", %error);
                    break;
                }
            },
            _ = last_seen_interval.tick() => {
                let pool = state.db_connection_pool.clone();
                tokio::spawn(async move {
                    if let Err(error) = chat_queries::update_online_timestamp(&pool, own_user_id).await {
                        tracing::error!(%error, "Failed to update last_seen timestamp");
                    }
                });
            },
            message = receiver.next() => {
                let Some(message) = message else {
                    break;
                };
                on_receive_message(message, own_user_id, &state.db_connection_pool, &state.pubsub, &mut sender).await;
            },
            message = chatot_stream.next() => {
                let Some(message) = message else {
                    break;
                };
                match message {
                    UserChannelMessage::WentOnline { connection_id } => {
                        if connection_id != connection_ids.current_connection_id {
                            // User opened another websocket connection, so we close this one
                            // We `return` here to not run any cleanup (like marking the user as "offline")
                            return on_close(&mut sender, CLOSE_FRAME_AWAY).await;
                        }
                    }
                    UserChannelMessage::UsersBecameFriends { friend_id } => {
                        let channel = ChatChannel::Private(PrivateChannel::new(own_user_id, friend_id));
                        stream_map.insert(channel, chat_channel_sub(channel));
                        chat_channel_running = true;
                    }
                    UserChannelMessage::UserJoinedGroup { group_id } => {
                        let channel = ChatChannel::Group { group_id };
                        stream_map.insert(channel, chat_channel_sub(channel));
                        chat_channel_running = true;
                    }
                    UserChannelMessage::UsersAreNoLongerFriends { friend_id } => {
                        let channel = ChatChannel::Private(PrivateChannel::new(own_user_id, friend_id));
                        stream_map.remove(&channel);
                        if let Some(channel) = channel_to_ws(&channel, own_user_id) {
                            if sender.send(ws::Message::Binary(encode_to_bytes(WebsocketMessageToClient {
                                message: Some(websocket_message_to_client::Message::ChannelClosed(channel))
                            }))).await.is_err() {
                                break;
                            }
                        }
                    }
                    UserChannelMessage::UserLeftGroup { group_id } => {
                        let channel = ChatChannel::Group { group_id };
                        stream_map.remove(&channel);
                        if let Some(channel) = channel_to_ws(&channel, own_user_id) {
                            if sender.send(ws::Message::Binary(encode_to_bytes(WebsocketMessageToClient {
                                message: Some(websocket_message_to_client::Message::ChannelClosed(channel))
                            }))).await.is_err() {
                                break;
                            }
                        }
                    }
                }
            },
            message = stream_map.next(), if chat_channel_running => {
                if message.is_none() {
                    chat_channel_running = false;
                }
                if let Some((_, Some(message))) = message {
                    let bytes = encode_to_bytes(message);
                    // In any websocket error, break loop.
                    if sender.send(ws::Message::Binary(bytes)).await.is_err() {
                        break;
                    }
                }
            }
        };
    }

    tracing::debug!(%own_user_id, "Closing connection");
    // Mark user as offline
    if let Err(error) = chat_queries::set_offline(&state.db_connection_pool, own_user_id).await {
        tracing::error!(%error, "Failed to set user offline");
    }
    for channel in stream_map.keys() {
        let publish_result = state
            .pubsub
            .publish(
                channel,
                ChatChannelMessage::WentOffline {
                    user_id: own_user_id,
                },
            )
            .await;
        if let Err(error) = publish_result {
            tracing::error!(%error, %channel, "Failed to publish message");
        }
    }
}

async fn on_receive_message<S>(
    message: Result<ws::Message, axum::Error>,
    user_id: UserId,
    pool: &sqlx::PgPool,
    pubsub: &PgPubSub,
    sender: &mut S,
) where
    S: Sink<ws::Message> + Unpin,
    S::Error: Display,
{
    let Ok(ws::Message::Binary(bytes)) = message else {
        return;
    };
    let to_server = match WebsocketMessageToServer::decode(bytes) {
        Ok(ok) => ok,
        Err(error) => {
            tracing::warn!(%error, "Failed to decode websocket protobuf message");
            return;
        }
    };
    let Some(message) = to_server.message else {
        return;
    };
    match message {
        websocket_message_to_server::Message::SentMessage(
            websocket_message_to_server::SentMessage { channel, text },
        ) => {
            let Some(channel) = channel.and_then(|c| channel_from_ws(c, user_id)) else {
                return;
            };
            let chat_message = ChatMessage::new(text);
            let result = match channel {
                ChatChannel::Group { group_id } => {
                    chat_queries::send_group_message(pool, user_id, group_id, chat_message)
                        .await
                        .map(|was_sent| {
                            if was_sent {
                                None
                            } else {
                                Some(WebsocketError::CannotSentMessageToGroupYouHaveNotJoined)
                            }
                        })
                }
                ChatChannel::Private(private_channel) => {
                    let Some(to_user) = private_channel.other_than(user_id) else {
                        tracing::warn!(
                            ?private_channel,
                            "Got message from unrelated private channel"
                        );
                        return;
                    };
                    chat_queries::send_private_message(pool, user_id, to_user, chat_message)
                        .await
                        .map(|was_sent| {
                            if was_sent {
                                None
                            } else {
                                Some(WebsocketError::CannotSentMessageToNonFriend)
                            }
                        })
                }
            };
            match result {
                Err(error) => {
                    tracing::error!(%error, %channel, "Failed to save message")
                }
                Ok(Some(error)) => {
                    if let Err(error) = sender
                        .send(ws::Message::Binary(encode_to_bytes(
                            WebsocketMessageToClient {
                                message: Some(websocket_message_to_client::Message::Error(
                                    error.into(),
                                )),
                            },
                        )))
                        .await
                    {
                        tracing::error!(%error, "Failed to send message to client");
                    }
                }
                Ok(None) => {
                    if let Err(error) = pubsub
                        .publish(&channel, ChatChannelMessage::SentChatMessage)
                        .await
                    {
                        tracing::error!(%error, "Failed to publish chat message send");
                    }
                }
            }
        }
        websocket_message_to_server::Message::StartedTyping(channel) => {
            let Some(channel) = channel_from_ws(channel, user_id) else {
                return;
            };
            if let Err(error) = pubsub
                .publish(
                    &channel,
                    ChatChannelMessage::StartedTyping {
                        from_user_id: user_id,
                    },
                )
                .await
            {
                tracing::error!(%error, "Failed to send StartedTyping event")
            }
        }
        websocket_message_to_server::Message::StoppedTyping(channel) => {
            let Some(channel) = channel_from_ws(channel, user_id) else {
                return;
            };
            if let Err(error) = pubsub
                .publish(
                    &channel,
                    ChatChannelMessage::StoppedTyping {
                        from_user_id: user_id,
                    },
                )
                .await
            {
                tracing::error!(%error, "Failed to send StoppedTyping event");
            }
        }
    };
}
