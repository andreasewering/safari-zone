use std::time::Duration;

use sqlx::PgPool;

use crate::data::{Item, ItemId, Throw, UserId};

use super::Settings;

pub enum ThrowItemError {
    NoItemToThrow,
    Other(sqlx::Error),
}

pub async fn thrown_item_id(
    pool: &PgPool,
    user_id: UserId,
    item: Item,
) -> Result<ItemId, ThrowItemError> {
    // Make sure that the user actually has a item of the given kind before generating an ItemId
    sqlx::query_scalar!(
        r#"
        WITH updated AS (
            UPDATE user_items
            SET amount = amount - 1
            WHERE user_id = $1 AND item = $2
            RETURNING id, amount
        )
        SELECT encrypt_item(nextval('item_id_seq')) as "id!: ItemId"
        WHERE EXISTS (SELECT 1 FROM updated)"#,
        user_id as UserId,
        item as Item
    )
    .fetch_one(pool)
    .await
    .map_err(|err| {
        if let sqlx::Error::RowNotFound = err {
            return ThrowItemError::NoItemToThrow;
        }
        let Some(db_error) = err.as_database_error() else {
            return ThrowItemError::Other(err);
        };
        if db_error.is_check_violation() {
            return ThrowItemError::NoItemToThrow;
        }
        ThrowItemError::Other(err)
    })
}

#[derive(Copy, Clone, Debug)]
pub struct ThrowTarget {
    pub x: f32,
    pub y: f32,
    pub duration_until_impact: Duration,
}

/**
 * Frontend formula for ball height for reference:
 *      Config.startVelocity * sin angle * timeInSeconds - 0.5 * Config.gravity * timeInSeconds ^ 2
 *
 * We want to solve for timeInSeconds with height = 0 and timeInSeconds != 0
 *
 * Rearranging gives:
 *      Config.startVelocity * sin angle = 0.5 * Config.gravity * timeInSeconds
 *
 * and next
 *      timeInSeconds = 2 * (Config.startVelocity * sin angle) / Config.gravity
 */
pub fn calc_throw_target_and_time(settings: &Settings, throw: Throw) -> ThrowTarget {
    let time_of_ground_impact_in_seconds =
        (2.0 * throw.angle_radians().sin() * settings.ball_throw_velocity / GRAVITY).abs();
    let distance = settings.ball_throw_velocity
        * throw.angle_radians().cos()
        * time_of_ground_impact_in_seconds;
    ThrowTarget {
        x: throw.vector_x() * distance,
        y: throw.vector_y() * distance,
        duration_until_impact: Duration::from_secs_f32(time_of_ground_impact_in_seconds),
    }
}

const GRAVITY: f32 = 9.81;

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]
    use std::sync::LazyLock;

    use crate::kangaskhan::Settings;

    use super::{calc_throw_target_and_time, Throw};
    use elm_rust_binding::{ElmFunctionHandle, ElmRoot};
    use serde::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize)]
    struct ElmThrow {
        angle: f32,
        timeInSeconds: f32,
    }

    fn elm_root() -> ElmRoot {
        ElmRoot::new(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/../../frontend/src/main"
        ))
        .expect("Elm root path is correct")
    }

    thread_local! {
            static ELM_CALL_BALL_HEIGHT: ElmFunctionHandle<ElmThrow, f32> = {
                let elm_root = elm_root();
                elm_root
                .prepare("Main.Safari.Overworld.Types.ActivePokeballs.calcBallHeight")
                .expect("Preparing elm function failed")
        };
    }

    thread_local! {
            static ELM_CALL_BALL_DISTANCE: ElmFunctionHandle<ElmThrow, f32> = {
                let elm_root = elm_root();
                elm_root
                .prepare("Main.Safari.Overworld.Types.ActivePokeballs.calcBallDistance")
                .expect("Preparing elm function failed")
        };
    }

    static SETTINGS: LazyLock<Settings> =
        LazyLock::new(|| Settings::read().expect("Read settings"));

    quickcheck::quickcheck! {
         fn height_throw_target_matches_frontend_calculation_property(throw: Throw) -> elm_rust_binding::Result<()> {
            height_throw_target_matches_frontend_calculation(throw)
        }
    }

    quickcheck::quickcheck! {
        fn distance_throw_target_matches_frontend_calculation_property(throw: Throw) -> elm_rust_binding::Result<()> {
            distance_throw_target_matches_frontend_calculation(throw)
        }
    }

    fn height_throw_target_matches_frontend_calculation(
        throw: Throw,
    ) -> elm_rust_binding::Result<()> {
        let throw_target = calc_throw_target_and_time(&SETTINGS, throw);
        let elm_ball_height: f32 = ELM_CALL_BALL_HEIGHT.with(|f| {
            f.call(ElmThrow {
                angle: throw.angle_radians(),
                timeInSeconds: throw_target.duration_until_impact.as_secs_f32(),
            })
        })?;
        assert!(
            elm_ball_height.abs() < 1e-5,
            "Elm ball height {elm_ball_height} was further away from 0 than expected."
        );
        Ok(())
    }

    fn distance_throw_target_matches_frontend_calculation(
        throw: Throw,
    ) -> elm_rust_binding::Result<()> {
        let throw_target = calc_throw_target_and_time(&SETTINGS, throw);
        let elm_ball_distance: f32 = ELM_CALL_BALL_DISTANCE.with(|f| {
            f.call(ElmThrow {
                angle: throw.angle_radians(),
                timeInSeconds: throw_target.duration_until_impact.as_secs_f32(),
            })
        })?;
        let elm_x_target = elm_ball_distance * throw.vector_x();
        let elm_y_target = elm_ball_distance * throw.vector_y();

        assert!((throw_target.x - elm_x_target).abs() < 1e5);
        assert!((throw_target.y - elm_y_target).abs() < 1e5);
        Ok(())
    }
}
