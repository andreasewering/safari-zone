use std::collections::HashSet;

use fxhash::FxBuildHasher;

use crate::{
    data::{Direction, MoveAbilities, Position},
    smeargle::MapMoveRuleProvider,
};

pub fn find_paths_with_max_steps<M>(
    provider: &M,
    move_ability: MoveAbilities,
    from: Position,
    to: Position,
    max_steps: u8,
) -> Vec<Vec<(Direction, Position)>>
where
    M: MapMoveRuleProvider,
{
    if max_steps == 0 {
        return Vec::new();
    }
    let mut visited = HashSet::with_capacity_and_hasher(
        3_u32
            .pow(max_steps.into())
            .try_into()
            .expect("u32 <= usize"),
        FxBuildHasher::default(),
    );
    visited.insert(from);

    let mut next_visited = HashSet::with_capacity_and_hasher(
        2_u32
            .pow(max_steps.into())
            .try_into()
            .expect("u32 <= usize"),
        FxBuildHasher::default(),
    );

    let mut current_generation: Vec<Vec<(Direction, Position)>> = Direction::iter()
        .filter_map(|dir| {
            let next_pos = provider.move_in_dir(from, move_ability, dir)?;
            visited.insert(next_pos);
            Some(vec![(dir, next_pos)])
        })
        .collect();
    let mut next_generation = Vec::with_capacity(4 * current_generation.len());

    for _gen in 1..max_steps {
        let solutions: Vec<_> = current_generation
            .iter()
            .filter(|path| {
                let (_, last_pos) = path[path.len() - 1];
                last_pos == to
            })
            .cloned()
            .collect();
        if !solutions.is_empty() {
            return solutions;
        }
        for path in &current_generation {
            let (_, last_pos) = path[path.len() - 1];
            for dir in Direction::iter() {
                if let Some(next_pos) = provider.move_in_dir(last_pos, move_ability, dir) {
                    if !visited.contains(&next_pos) {
                        next_visited.insert(next_pos);
                        let mut path = path.clone();
                        path.push((dir, next_pos));
                        next_generation.push(path);
                    }
                }
            }
        }

        next_visited.drain().for_each(|pos| {
            visited.insert(pos);
        });
        current_generation = next_generation;
        next_generation = Vec::with_capacity(4 * current_generation.len());
    }

    current_generation
        .into_iter()
        .filter(|path| {
            let (_, last_pos) = path[path.len() - 1];
            last_pos == to
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use std::iter::repeat_n;

    use super::*;
    use crate::{
        data::{Directional, MoveAbilities, Position},
        smeargle::{test_helper::TestMapMoveRuleProvider, MoveRule},
    };

    #[test]
    fn path_of_length_2() {
        let from = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        let to = Position {
            x: 2,
            y: 0,
            layer_number: 0,
        };
        let paths = find_paths_with_max_steps(
            &AnythingGoesMoveRuleProvider,
            MoveAbilities::all(),
            from,
            to,
            2,
        );
        assert_eq!(
            paths,
            vec![vec![
                (
                    Direction::R,
                    Position {
                        x: 1,
                        y: 0,
                        layer_number: 0
                    }
                ),
                (Direction::R, to),
            ]]
        )
    }

    #[test]
    fn to_and_from_are_the_same() {
        let position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        let paths = find_paths_with_max_steps(
            &AnythingGoesMoveRuleProvider,
            MoveAbilities::all(),
            position,
            position,
            1,
        );
        assert!(paths.is_empty());
    }

    #[test]
    fn finds_the_direct_path_if_possible() {
        let position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };

        for dir in Direction::iter() {
            let paths = find_paths_with_max_steps(
                &AnythingGoesMoveRuleProvider,
                MoveAbilities::all(),
                position,
                position.move_in_dir(dir),
                1,
            );
            assert_eq!(paths, vec![vec![(dir, position.move_in_dir(dir))]]);
        }
    }

    #[test]
    fn does_not_find_direct_path_if_blocked() {
        let position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        for dir in Direction::iter() {
            let paths = find_paths_with_max_steps(
                &NothingGoesMoveRuleProvider,
                MoveAbilities::all(),
                position,
                position.move_in_dir(dir),
                10,
            );
            assert!(paths.is_empty());
        }
    }

    #[test]
    fn finds_path_if_target_is_further_away() {
        let position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        for dir in Direction::iter() {
            let target = repeat_n(dir, 10).fold(position, |pos, d| pos.move_in_dir(d));
            let paths = find_paths_with_max_steps(
                &AnythingGoesMoveRuleProvider,
                MoveAbilities::all(),
                position,
                target,
                10,
            );
            assert_eq!(
                paths,
                vec![vec![
                    (dir, position.move_in_dir(dir)),
                    (dir, repeat_n(dir, 2).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 3).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 4).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 5).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 6).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 7).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 8).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 9).fold(position, |p, d|p.move_in_dir(d))),
                    (dir, repeat_n(dir, 10).fold(position, |p, d|p.move_in_dir(d))),
                ]]
            );
        }
    }

    #[test]
    fn finds_path_if_target_is_reachable_by_doing_a_loop() {
        let from = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        let provider: TestMapMoveRuleProvider = vec![
            (0, 0),
            (1, 0),
            (1, 1),
            (1, 2),
            (0, 2),
            (-1, 2),
            (-2, 2),
            (-2, 1),
            (-2, 0),
        ]
        .into_iter()
        .map(|(x, y)| {
            (
                Position {
                    x,
                    y,
                    layer_number: 0,
                },
                MoveAbilities::all(),
            )
        })
        .collect();

        let target = Position {
            x: -2,
            y: 0,
            layer_number: 0,
        };

        let paths = find_paths_with_max_steps(&provider, MoveAbilities::all(), from, target, 8);
        assert_eq!(
            paths,
            vec![vec![
                (
                    Direction::R,
                    Position {
                        x: 1,
                        y: 0,
                        layer_number: 0
                    }
                ),
                (
                    Direction::D,
                    Position {
                        x: 1,
                        y: 1,
                        layer_number: 0
                    }
                ),
                (
                    Direction::D,
                    Position {
                        x: 1,
                        y: 2,
                        layer_number: 0
                    }
                ),
                (
                    Direction::L,
                    Position {
                        x: 0,
                        y: 2,
                        layer_number: 0
                    }
                ),
                (
                    Direction::L,
                    Position {
                        x: -1,
                        y: 2,
                        layer_number: 0
                    }
                ),
                (
                    Direction::L,
                    Position {
                        x: -2,
                        y: 2,
                        layer_number: 0
                    }
                ),
                (
                    Direction::U,
                    Position {
                        x: -2,
                        y: 1,
                        layer_number: 0
                    }
                ),
                (
                    Direction::U,
                    Position {
                        x: -2,
                        y: 0,
                        layer_number: 0
                    }
                ),
            ]]
        );
    }

    struct AnythingGoesMoveRuleProvider;

    impl MapMoveRuleProvider for AnythingGoesMoveRuleProvider {
        fn move_rule_for_position(
            &self,
            _position: Position,
        ) -> Option<crate::smeargle::DirectionalMoveRules> {
            Some(Directional::from_single(
                MoveRule::default() | MoveAbilities::all(),
            ))
        }

        fn move_rules(&self) -> Vec<(Position, crate::smeargle::DirectionalMoveRules)> {
            unimplemented!("Expected to only need move_rule_for_position")
        }
    }

    struct NothingGoesMoveRuleProvider;

    impl MapMoveRuleProvider for NothingGoesMoveRuleProvider {
        fn move_rule_for_position(
            &self,
            _position: Position,
        ) -> Option<crate::smeargle::DirectionalMoveRules> {
            Some(Directional::from_single(MoveRule::default()))
        }

        fn move_rules(&self) -> Vec<(Position, crate::smeargle::DirectionalMoveRules)> {
            unimplemented!("Expected to only need move_rule_for_position")
        }
    }
}
