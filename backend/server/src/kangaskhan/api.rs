use crate::{
    configuration::TrainerConfig,
    data::{proto, PokemonNumber, Position, SupportedLanguage, TrainerNumber},
    error::{AppError, AppResult},
    gardevoir::{guard, Claims, Permission},
};
use sqlx::PgPool;
use tonic::{Request, Response, Status};

use crate::{
    channels::{PlayerChannel, PlayerMessage, PlayerMessageWrapper},
    configuration::PokemonConfig,
    data::{Direction, Item, MapId, UserId},
    gardevoir::AuthCtx,
};

use super::{
    proto::kangaskhan::{
        AddWarpRequest, AddWarpResponse, ChooseStartTileRequest, ChooseStartTileResponse,
        CollectedItem, DeleteWarpRequest, DeleteWarpResponse, DexEntry, GetAllPokemonRequest,
        GetAllPokemonResponse, GetDexEntriesRequest, GetDexEntriesResponse,
        GetDexEntryDetailRequest, GetDexEntryDetailResponse, GetOtherUserDataRequest,
        GetOtherUserDataResponse, GetPokemonRequest, GetPokemonResponse, GetStarterPokemonRequest,
        GetStarterPokemonResponse, GetTrainersRequest, GetTrainersResponse, GetUserDataRequest,
        GetUserDataResponse, GetUserItemsRequest, GetUserItemsResponse, GetWarpsRequest,
        GetWarpsResponse, PartnerPokemon, PokemonData, SelectedTrainer, SetPartnerPokemonRequest,
        SetPartnerPokemonResponse, SetTrainerSpriteRequest, SetTrainerSpriteResponse,
        StarterPokemon, Tag, Trainer, Type, UserPosition, Warp,
    },
    WarpCache,
};

use super::proto::kangaskhan::kangaskhan_service_server::KangaskhanService;

use crate::watchog::err_code::ErrorCode;

#[derive(Clone)]
pub struct Kangaskhan {
    pub auth_ctx: AuthCtx,
    pub player_channel: PlayerChannel,
    pub pokemon: PokemonConfig,
    pub trainers: TrainerConfig,
    pub db_connection_pool: PgPool,
    pub warp_cache: WarpCache,
}

#[tonic::async_trait]
impl KangaskhanService for Kangaskhan {
    async fn get_all_pokemon(
        &self,
        request: Request<GetAllPokemonRequest>,
    ) -> Result<Response<GetAllPokemonResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::ReadAllPokemon)?;

        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let GetAllPokemonRequest {} = request.into_inner();
        let pokemon = self
            .pokemon
            .iter()
            .map(|(number, poke)| PokemonData {
                number: u32::from(*number),
                name: poke.name.get(locale).to_string(),
                move_speed: u32::from(poke.move_speed),
                move_ability: u32::from(poke.move_ability.as_repr()),
                primary_type: Type::from(poke.primary_type).into(),
                secondary_type: poke.secondary_type.as_ref().map(|t| Type::from(*t).into()),
            })
            .collect();

        Ok(Response::new(GetAllPokemonResponse { pokemon }))
    }

    async fn get_pokemon(
        &self,
        request: Request<GetPokemonRequest>,
    ) -> Result<Response<GetPokemonResponse>, Status> {
        // Access control not possible, since this endpoint is needed for each spawned encounter
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let GetPokemonRequest { pokemon_number } = request.into_inner();

        let number = PokemonNumber::try_from(pokemon_number)?;

        let poke = self.pokemon.get(number).ok_or(Status::not_found(
            ErrorCode::EntityDoesNotExist.to_response(),
        ))?;

        let response = GetPokemonResponse {
            number: pokemon_number,
            name: poke.name.get(locale).to_string(),
            move_duration_in_ms: (self.pokemon.move_duration(poke.move_speed).as_secs_f64()
                * 1000.0) as f32,
            move_ability: u32::from(poke.move_ability.as_repr()),
            primary_type: Type::from(poke.primary_type).into(),
            secondary_type: poke.secondary_type.as_ref().map(|t| Type::from(*t).into()),
        };
        Ok(Response::new(response))
    }

    async fn get_dex_entries(
        &self,
        request: Request<GetDexEntriesRequest>,
    ) -> Result<Response<GetDexEntriesResponse>, Status> {
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let GetDexEntriesRequest {} = request.into_inner();

        let entries = sqlx::query!(
            r#"SELECT pokemon_number as "pokemon_number: PokemonNumber", amount_normal, amount_shiny FROM dex_entry
        WHERE dex_entry.user_id = $1
        ORDER BY pokemon_number"#,
            claims.user_id() as UserId
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?
        .into_iter()
        .filter_map(|entry| {
            let pokemon = self.pokemon.get(entry.pokemon_number)?;
            let entry = DexEntry {
                number: u32::from(entry.pokemon_number),
                amount_normal: u32::try_from(entry.amount_normal).ok()?,
                amount_shiny: u32::try_from(entry.amount_shiny).ok()?,
                pokemon_name: pokemon.name.get(locale).to_string(),
            };
            Some(entry)
        })
        .collect();

        Ok(Response::new(GetDexEntriesResponse { entries }))
    }

    async fn get_dex_entry_detail(
        &self,
        request: Request<GetDexEntryDetailRequest>,
    ) -> Result<Response<GetDexEntryDetailResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let GetDexEntryDetailRequest { number } = request.into_inner();
        let pokemon_number = PokemonNumber::try_from(number)?;

        let entry = sqlx::query!(
            "SELECT pokemon_number, amount_normal, amount_shiny FROM dex_entry
        WHERE dex_entry.user_id = $1
        AND dex_entry.pokemon_number = $2",
            claims.user_id() as UserId,
            pokemon_number as PokemonNumber
        )
        .fetch_one(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        let pokemon = self.pokemon.get(pokemon_number).ok_or(Status::not_found(
            ErrorCode::EntityDoesNotExist.to_response(),
        ))?;
        let amount_normal = u32::try_from(entry.amount_normal).map_err(|_err| {
            Status::failed_precondition("Amount of Pokémon should never be negative")
        })?;
        let amount_shiny = u32::try_from(entry.amount_shiny).map_err(|_err| {
            Status::failed_precondition("Amount of Pokémon should never be negative")
        })?;

        Ok(Response::new(GetDexEntryDetailResponse {
            amount_normal,
            amount_shiny,
            name: pokemon.name.get(locale).to_string(),
            description: pokemon.description.get(locale).to_string(),
            height: pokemon.height,
            weight: pokemon.weight,
            primary_type: Type::from(pokemon.primary_type).into(),
            secondary_type: pokemon
                .secondary_type
                .as_ref()
                .map(|t| Type::from(*t).into()),
        }))
    }

    async fn get_starter_pokemon(
        &self,
        request: Request<GetStarterPokemonRequest>,
    ) -> Result<Response<GetStarterPokemonResponse>, Status> {
        let _claims = self.auth_ctx.decode_header(request.metadata())?;
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());

        let pokemon = self
            .pokemon
            .starters()
            .map(|(pokemon_number, poke)| StarterPokemon {
                number: pokemon_number.into(),
                name: poke.name.get(locale).to_string(),
                description: poke.description.get(locale).to_string(),
                height: poke.height,
                weight: poke.weight,
                primary_type: Type::from(poke.primary_type).into(),
                secondary_type: poke.secondary_type.as_ref().map(|t| Type::from(*t).into()),
            })
            .collect();

        Ok(Response::new(GetStarterPokemonResponse { pokemon }))
    }

    async fn set_partner_pokemon(
        &self,
        request: Request<SetPartnerPokemonRequest>,
    ) -> Result<Response<SetPartnerPokemonResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let SetPartnerPokemonRequest { pokemon_number } = request.into_inner();

        let pokemon_number = PokemonNumber::try_from(pokemon_number)?;

        set_partner_pokemon(
            &self.db_connection_pool,
            &self.player_channel,
            &self.pokemon,
            &claims,
            pokemon_number,
        )
        .await?;

        Ok(Response::new(SetPartnerPokemonResponse {}))
    }

    async fn set_trainer_sprite(
        &self,
        request: Request<SetTrainerSpriteRequest>,
    ) -> Result<Response<SetTrainerSpriteResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let SetTrainerSpriteRequest { trainer_number } = request.into_inner();
        let trainer_number = TrainerNumber::try_from(trainer_number)?;
        self.trainers
            .get(trainer_number)
            .ok_or(Status::not_found("Trainer not found"))?;

        set_trainer_sprite(
            &self.db_connection_pool,
            &self.player_channel,
            &claims,
            trainer_number,
        )
        .await?;

        Ok(Response::new(SetTrainerSpriteResponse {}))
    }

    async fn get_user_items(
        &self,
        request: Request<GetUserItemsRequest>,
    ) -> Result<Response<GetUserItemsResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;

        let items = sqlx::query_as!(
            UserItem,
            r#"SELECT item as "item: _", amount FROM user_items WHERE user_id = $1 ORDER BY item"#,
            claims.user_id() as UserId
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?
        .into_iter()
        .map(|user_item| {
            let amount = u32::try_from(user_item.amount).map_err(|_err| {
                Status::failed_precondition(ErrorCode::InconsistentDatabaseEntry.to_response())
            })?;
            let item = proto::Item::from(user_item.item).into();
            Ok(CollectedItem { amount, item })
        })
        .collect::<Result<Vec<_>, Status>>()?;

        Ok(Response::new(GetUserItemsResponse { items }))
    }

    async fn get_user_data(
        &self,
        request: Request<GetUserDataRequest>,
    ) -> Result<Response<GetUserDataResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;

        let user = get_user_data(&self.db_connection_pool, claims.user_id()).await?;

        let position = (|| {
            let x = user.x?;
            let y = user.y?;
            let layer_number = user.layer_number?;
            let map_id = user.map_id?.into();
            let direction = proto::Direction::from(user.direction?).into();
            Some(UserPosition {
                x,
                y,
                layer_number,
                direction,
                map_id,
            })
        })();

        let partner_pokemon = (|| {
            let pokemon_number = user.partner_pokemon_number?.into();
            let is_shiny = user.partner_pokemon_is_shiny?;
            Some(PartnerPokemon {
                pokemon_number,
                is_shiny,
            })
        })();

        let trainer = (|| {
            let trainer_number = user.trainer_number?;
            Some(SelectedTrainer {
                trainer_number: trainer_number.into(),
            })
        })();

        Ok(Response::new(GetUserDataResponse {
            position,
            partner_pokemon,
            trainer,
            name: user.display_name,
        }))
    }

    async fn get_other_user_data(
        &self,
        request: Request<GetOtherUserDataRequest>,
    ) -> Result<Response<GetOtherUserDataResponse>, Status> {
        let _claims = self.auth_ctx.decode_header(request.metadata())?;
        let GetOtherUserDataRequest { user_id } = request.into_inner();

        let user = get_user_data(&self.db_connection_pool, user_id.into()).await?;

        let partner_pokemon = (|| {
            let pokemon_number = user.partner_pokemon_number?.into();
            let is_shiny = user.partner_pokemon_is_shiny?;
            Some(PartnerPokemon {
                pokemon_number,
                is_shiny,
            })
        })();

        let trainer = (|| {
            let trainer_number = user.trainer_number?;
            Some(SelectedTrainer {
                trainer_number: trainer_number.into(),
            })
        })();

        Ok(Response::new(GetOtherUserDataResponse {
            partner_pokemon,
            trainer,
            name: user.display_name,
        }))
    }

    async fn get_warps(
        &self,
        request: Request<GetWarpsRequest>,
    ) -> Result<Response<GetWarpsResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::ReadMapDetails)?;
        let GetWarpsRequest { map_id } = request.into_inner();

        let warps = sqlx::query!(
            r#"SELECT id, from_x, from_y, from_layer_number, from_map_id,
                      to_x, to_y, to_layer_number, to_map_id
               FROM warp WHERE from_map_id = $1 OR to_map_id = $1"#,
            map_id
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?
        .into_iter()
        .map(|warp| Warp {
            warp_id: warp.id,
            from_x: warp.from_x,
            from_y: warp.from_y,
            from_layer_number: warp.from_layer_number,
            from_map_id: warp.from_map_id,
            to_x: warp.to_x,
            to_y: warp.to_y,
            to_layer_number: warp.to_layer_number,
            to_map_id: warp.to_map_id,
        })
        .collect();

        Ok(Response::new(GetWarpsResponse { warps }))
    }

    async fn delete_warp(
        &self,
        request: Request<DeleteWarpRequest>,
    ) -> Result<Response<DeleteWarpResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::EditWarps)?;

        let DeleteWarpRequest { warp_id } = request.into_inner();

        let deleted = sqlx::query!(
            r#"DELETE FROM warp WHERE id = $1 RETURNING from_map_id as "from_map_id: MapId""#,
            warp_id
        )
        .fetch_one(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;
        self.warp_cache.invalidate(deleted.from_map_id).await;

        Ok(Response::new(DeleteWarpResponse {}))
    }

    async fn add_warp(
        &self,
        request: Request<AddWarpRequest>,
    ) -> Result<Response<AddWarpResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::EditWarps)?;

        let AddWarpRequest {
            from_x,
            from_y,
            from_layer_number,
            from_map_id,
            to_x,
            to_y,
            to_layer_number,
            to_map_id,
        } = request.into_inner();

        let warp_id = add_warp(
            &self.db_connection_pool,
            &self.warp_cache,
            Position {
                x: from_x,
                y: from_y,
                layer_number: from_layer_number,
            },
            MapId::from(from_map_id),
            Position {
                x: to_x,
                y: to_y,
                layer_number: to_layer_number,
            },
            MapId::from(to_map_id),
        )
        .await?;

        Ok(Response::new(AddWarpResponse { warp_id }))
    }

    async fn choose_start_tile(
        &self,
        request: Request<ChooseStartTileRequest>,
    ) -> Result<Response<ChooseStartTileResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let ChooseStartTileRequest { start_tile_id } = request.into_inner();

        choose_start_tile(&self.db_connection_pool, claims.user_id(), start_tile_id).await?;

        Ok(Response::new(ChooseStartTileResponse {}))
    }

    async fn get_trainers(
        &self,
        request: Request<GetTrainersRequest>,
    ) -> Result<Response<GetTrainersResponse>, Status> {
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let GetTrainersRequest {} = request.into_inner();
        let trainers = self
            .trainers
            .iter()
            .map(|(number, trainer)| Trainer {
                trainer_number: u32::from(*number),
                display_name: trainer.display_name.get(locale).to_string(),
                tag_ids: trainer
                    .tags
                    .iter()
                    .map(|tag_id| u32::from(*tag_id))
                    .collect(),
            })
            .collect();

        let tags = self
            .trainers
            .iter_tags()
            .map(|(tag_id, tag)| Tag {
                tag_id: u32::from(*tag_id),
                display_name: tag.get(locale).to_string(),
            })
            .collect();

        Ok(Response::new(GetTrainersResponse { trainers, tags }))
    }
}

#[derive(Debug, Clone)]
struct UserItem {
    item: Item,
    amount: i32,
}

pub(super) async fn set_partner_pokemon(
    pool: &PgPool,
    player_channel: &PlayerChannel,
    pokemon_config: &PokemonConfig,
    claims: &Claims,
    pokemon_number: PokemonNumber,
) -> AppResult<()> {
    let user_id = claims.user_id();
    let opt_caught_pokemon = sqlx::query!(
        "SELECT amount_normal, amount_shiny FROM dex_entry
        WHERE user_id = $1
        AND pokemon_number = $2
        AND amount_normal + amount_shiny > 0",
        user_id as UserId,
        pokemon_number as PokemonNumber
    )
    .fetch_optional(pool)
    .await?;

    if let Some(caught_pokemon) = opt_caught_pokemon {
        sqlx::query!(
            "UPDATE user_partner_pokemon
                    SET partner_pokemon_number = $1
                    WHERE user_id = $2",
            pokemon_number as PokemonNumber,
            user_id as UserId,
        )
        .execute(pool)
        .await?;

        player_channel.send(PlayerMessageWrapper {
            user_id,
            map_id: claims.map_id(),
            message: PlayerMessage::ChangedPartnerPokemon {
                pokemon_number,
                is_shiny: caught_pokemon.amount_shiny > 0,
            },
        });
        return Ok(());
    }
    let existing_partner = sqlx::query!(
        "SELECT partner_pokemon_number FROM user_partner_pokemon WHERE user_id = $1",
        user_id as UserId
    )
    .fetch_optional(pool)
    .await?;
    let has_completed_setup = sqlx::query!(
        "SELECT 1 as ignore FROM user_position WHERE user_id = $1",
        user_id as UserId
    )
    .fetch_optional(pool)
    .await?
    .is_some();
    if has_completed_setup && existing_partner.is_some() {
        return Err(AppError::bad_request(
            "Cannot set partner which you did not catch",
        ));
    }
    // check if pokemon is a valid starter pokemon
    if !pokemon_config.is_starter(pokemon_number) {
        return Err(AppError::bad_request(
            "This pokemon is not configured as a starter pokemon",
        ));
    }
    tracing::info!(message = "Player chose starter pokemon", %pokemon_number);
    let mut transaction = pool.begin().await.map_err(AppError::from)?;

    sqlx::query!(
                    "INSERT INTO user_partner_pokemon (user_id, partner_pokemon_number)
                    VALUES ($1, $2) ON CONFLICT (user_id) DO UPDATE SET partner_pokemon_number = $2",
                    user_id as UserId,
                    pokemon_number as PokemonNumber,
                )
                .execute(&mut *transaction)
                .await?;
    if existing_partner.is_some() {
        // Just delete all entries for the user_id. This is fine
        // since the user has to have exactly one pokemon,
        // his previously selected starter.
        sqlx::query!(
            "DELETE FROM dex_entry WHERE user_id = $1",
            user_id as UserId
        )
        .execute(&mut *transaction)
        .await?;
    }
    sqlx::query!(
        "INSERT INTO dex_entry (user_id, pokemon_number, amount_normal, amount_shiny)
                VALUES ($1, $2, 1, 0)",
        user_id as UserId,
        pokemon_number as PokemonNumber
    )
    .execute(&mut *transaction)
    .await?;

    transaction.commit().await?;

    Ok(())
}

pub(super) async fn set_trainer_sprite(
    pool: &PgPool,
    player_channel: &PlayerChannel,
    claims: &Claims,
    trainer_number: TrainerNumber,
) -> AppResult<()> {
    let user_id = claims.user_id();
    sqlx::query!(
        "INSERT INTO user_trainer (trainer_number, user_id) 
             VALUES ($1, $2)
             ON CONFLICT (user_id)
             DO UPDATE SET trainer_number = EXCLUDED.trainer_number",
        trainer_number as TrainerNumber,
        user_id as UserId,
    )
    .execute(pool)
    .await?;

    player_channel.send(PlayerMessageWrapper {
        user_id,
        map_id: claims.map_id(),
        message: PlayerMessage::ChangedTrainerSprite { trainer_number },
    });

    Ok(())
}

pub(super) async fn choose_start_tile(
    pool: &PgPool,
    user_id: UserId,
    start_tile_id: i64,
) -> AppResult<()> {
    let result = sqlx::query!(r#"INSERT INTO user_position (user_id, map_id, x, y, layer_number, direction)
                SELECT $1, map_id, x, y, layer_number, 'D' FROM start_tile WHERE start_tile.id = $2"#, 
                user_id as UserId,
                start_tile_id)
            .execute(pool)
            .await?;
    match result.rows_affected() {
        0 => Err(AppError::not_found("Start tile not found")),
        1 => Ok(()),
        n => Err(AppError::inconsistency(format!(
            "Should never have multiple user_position entries but inserted {n}"
        ))),
    }
}

pub(super) async fn add_warp(
    pool: &sqlx::PgPool,
    warp_cache: &WarpCache,
    from_position: Position,
    from_map: MapId,
    to_position: Position,
    to_map: MapId,
) -> AppResult<i64> {
    let result = sqlx::query!(r#"
        INSERT INTO warp (from_x, from_y, from_layer_number, from_map_id, to_x, to_y, to_layer_number, to_map_id) 
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id"#, 
        from_position.x, from_position.y, from_position.layer_number, from_map as MapId,
        to_position.x, to_position.y, to_position.layer_number, to_map as MapId)
            .fetch_one(pool)
            .await
            .map_err(AppError::from)?;
    warp_cache.invalidate(from_map).await;
    Ok(result.id)
}

#[derive(Debug)]
struct UserEntity {
    display_name: String,
    map_id: Option<MapId>,
    x: Option<i32>,
    y: Option<i32>,
    layer_number: Option<i32>,
    trainer_number: Option<TrainerNumber>,
    direction: Option<Direction>,
    partner_pokemon_number: Option<PokemonNumber>,
    partner_pokemon_is_shiny: Option<bool>,
}

async fn get_user_data(pool: &sqlx::PgPool, user_id: UserId) -> AppResult<UserEntity> {
    let user = sqlx::query_as!(UserEntity,
            r#"SELECT users.display_name, user_position.map_id as "map_id?: _", x as "x?", y as "y?", layer_number as "layer_number?",
                      trainer_number as "trainer_number?: _", 
                      direction as "direction?: _", 
                      partner_pokemon_number as "partner_pokemon_number?: PokemonNumber",
                      dex_entry.amount_shiny > 0 as "partner_pokemon_is_shiny?"
               FROM users
               LEFT JOIN user_position ON user_position.user_id = users.id
               LEFT JOIN user_partner_pokemon ON user_partner_pokemon.user_id = users.id
               LEFT JOIN user_trainer ON user_trainer.user_id = users.id
               LEFT JOIN dex_entry ON users.id = dex_entry.user_id AND user_partner_pokemon.partner_pokemon_number = dex_entry.pokemon_number
               WHERE users.id = $1"#,
            user_id as UserId
        )
        .fetch_one(pool)
        .await?;
    Ok(user)
}

#[cfg(test)]
mod tests {
    use std::future::Future;

    use crate::{
        arceus::test_helper as arceus,
        channels::ItemChannel,
        data::{ItemId, RequestExt},
        gardevoir::test_helper as gardevoir,
        kangaskhan::{
            websocket::{catch_pokemon, sync_collected_items},
            Settings,
        },
        smeargle::test_helper as smeargle,
    };

    use super::*;

    #[database_macros::test]
    async fn get_all_pokemon_permission_denied(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let status = setup
            .req(Kangaskhan::get_all_pokemon, GetAllPokemonRequest {})
            .await
            .expect_err("Should fail because ReadAllPokemon permission is missing");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);

        Ok(())
    }

    #[database_macros::test]
    async fn get_all_pokemon_works(pool: PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::ReadAllPokemon]);
        let GetAllPokemonResponse { pokemon } = setup
            .req(Kangaskhan::get_all_pokemon, GetAllPokemonRequest {})
            .await?
            .into_inner();
        assert_eq!(&pokemon.first().unwrap().name, "Bulbasaur");

        Ok(())
    }

    #[database_macros::test]
    async fn get_all_pokemon_supports_i18n(pool: PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::ReadAllPokemon]);

        let request = setup
            .user
            .request(GetAllPokemonRequest {})
            .with_locale(SupportedLanguage::DE);

        let GetAllPokemonResponse { pokemon } =
            setup.api.get_all_pokemon(request).await?.into_inner();
        assert_eq!(&pokemon.first().unwrap().name, "Bisasam");

        Ok(())
    }

    #[database_macros::test]
    async fn get_pokemon_works(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let response = setup
            .req(
                Kangaskhan::get_pokemon,
                GetPokemonRequest { pokemon_number: 1 },
            )
            .await?
            .into_inner();
        assert_eq!(&response.name, "Bulbasaur");

        Ok(())
    }

    #[database_macros::test]
    async fn get_pokemon_supports_i18n(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let request = setup
            .user
            .request(GetPokemonRequest { pokemon_number: 1 })
            .with_locale(SupportedLanguage::DE);

        let response = setup.api.get_pokemon(request).await?.into_inner();
        assert_eq!(&response.name, "Bisasam");

        Ok(())
    }

    #[database_macros::test]
    async fn get_trainers_works(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let GetTrainersResponse { trainers, tags } = setup
            .req(Kangaskhan::get_trainers, GetTrainersRequest {})
            .await?
            .into_inner();
        assert_eq!(trainers[0].display_name, "Default Trainer Girl");
        assert_eq!(tags[0].display_name, "Male");

        Ok(())
    }

    #[database_macros::test]
    async fn get_trainers_supports_i18n(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let request = setup
            .user
            .request(GetTrainersRequest {})
            .with_locale(SupportedLanguage::DE);

        let GetTrainersResponse { trainers, tags } =
            setup.api.get_trainers(request).await?.into_inner();

        assert_eq!(trainers[0].display_name, "Standard Trainerin");
        assert_eq!(tags[0].display_name, "Männlich");

        Ok(())
    }

    #[database_macros::test]
    async fn get_dex_entries_empty(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let GetDexEntriesResponse { entries } = setup
            .req(Kangaskhan::get_dex_entries, GetDexEntriesRequest {})
            .await?
            .into_inner();

        assert!(entries.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn get_dex_entries_caught_pokemon(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        catch_pokemon(
            &pool,
            setup.user.claims().user_id(),
            PokemonNumber(1),
            false,
        )
        .await?;

        let GetDexEntriesResponse { entries } = setup
            .req(Kangaskhan::get_dex_entries, GetDexEntriesRequest {})
            .await?
            .into_inner();

        assert_eq!(
            entries,
            vec![DexEntry {
                number: 1,
                amount_normal: 1,
                amount_shiny: 0,
                pokemon_name: "Bulbasaur".to_string()
            }]
        );

        let detail_response = setup
            .req(
                Kangaskhan::get_dex_entry_detail,
                GetDexEntryDetailRequest { number: 1 },
            )
            .await?
            .into_inner();
        assert_eq!(&detail_response.name, "Bulbasaur");
        assert_eq!(detail_response.amount_normal, 1);
        assert_eq!(detail_response.amount_shiny, 0);

        Ok(())
    }

    #[database_macros::test]
    async fn get_dex_entries_supports_i18n(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        catch_pokemon(&pool, setup.user.claims().user_id(), PokemonNumber(1), true).await?;

        let request = setup
            .user
            .request(GetDexEntriesRequest {})
            .with_locale(SupportedLanguage::DE);

        let GetDexEntriesResponse { entries } =
            setup.api.get_dex_entries(request).await?.into_inner();

        assert_eq!(
            entries,
            vec![DexEntry {
                number: 1,
                amount_normal: 0,
                amount_shiny: 1,
                pokemon_name: "Bisasam".to_string()
            }]
        );

        let detail_request = setup
            .user
            .request(GetDexEntryDetailRequest { number: 1 })
            .with_locale(SupportedLanguage::DE);

        let detail_response = setup
            .api
            .get_dex_entry_detail(detail_request)
            .await?
            .into_inner();
        assert_eq!(&detail_response.name, "Bisasam");
        assert_eq!(detail_response.amount_normal, 0);
        assert_eq!(detail_response.amount_shiny, 1);
        Ok(())
    }

    #[database_macros::test]
    async fn get_dex_entry_detail_not_found(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let status = setup
            .req(
                Kangaskhan::get_dex_entry_detail,
                GetDexEntryDetailRequest { number: 1 },
            )
            .await
            .expect_err("Should fail because no pokemon was caught yet.");

        assert_eq!(status.code(), tonic::Code::NotFound);
        Ok(())
    }

    #[database_macros::test]
    async fn get_starter_pokemon_works(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let GetStarterPokemonResponse { pokemon } = setup
            .req(Kangaskhan::get_starter_pokemon, GetStarterPokemonRequest {})
            .await?
            .into_inner();

        assert_eq!(pokemon.len(), setup.api.pokemon.starters().count());
        Ok(())
    }

    #[database_macros::test]
    async fn get_user_data_initial(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let GetUserDataResponse {
            position,
            trainer,
            partner_pokemon,
            name,
        } = setup
            .req(Kangaskhan::get_user_data, GetUserDataRequest {})
            .await?
            .into_inner();

        assert!(position.is_none());
        assert!(trainer.is_none());
        assert!(partner_pokemon.is_none());
        assert_eq!(&name, "user");
        Ok(())
    }

    #[database_macros::test]
    async fn set_partner_pokemon_works_initially_for_starter(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let starter_pokemon_number = setup
            .api
            .pokemon
            .starters()
            .next()
            .expect("Some starter pokemon to be configured")
            .0
            .into();

        let SetPartnerPokemonResponse {} = setup
            .req(
                Kangaskhan::set_partner_pokemon,
                SetPartnerPokemonRequest {
                    pokemon_number: starter_pokemon_number,
                },
            )
            .await?
            .into_inner();

        let GetUserDataResponse {
            position,
            trainer,
            partner_pokemon,
            name: _,
        } = setup
            .req(Kangaskhan::get_user_data, GetUserDataRequest {})
            .await?
            .into_inner();

        assert!(position.is_none());
        assert!(trainer.is_none());
        assert_eq!(
            partner_pokemon,
            Some(PartnerPokemon {
                pokemon_number: starter_pokemon_number,
                is_shiny: false
            })
        );
        Ok(())
    }

    #[database_macros::test]
    async fn set_partner_pokemon_fails_initially_for_non_starter(
        pool: PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let non_starter_pokemon_number = *setup
            .api
            .pokemon
            .iter()
            .find(|(num, _)| !setup.api.pokemon.is_starter(**num))
            .expect("Some pokemon that is not a starter")
            .0;

        let status = setup
            .req(
                Kangaskhan::set_partner_pokemon,
                SetPartnerPokemonRequest {
                    pokemon_number: non_starter_pokemon_number.into(),
                },
            )
            .await
            .expect_err("Should only be allowed to choose starter pokemon");

        assert_eq!(status.code(), tonic::Code::InvalidArgument);
        Ok(())
    }

    #[database_macros::test]
    async fn set_partner_pokemon_starter_multiple_times(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let mut starters = setup.api.pokemon.starters().map(|(num, _)| num);
        let first_starter = starters.next().expect("first starter");
        let second_starter = starters.next().expect("second starter");

        setup
            .req(
                Kangaskhan::set_partner_pokemon,
                SetPartnerPokemonRequest {
                    pokemon_number: first_starter.into(),
                },
            )
            .await?;

        setup
            .req(
                Kangaskhan::set_partner_pokemon,
                SetPartnerPokemonRequest {
                    pokemon_number: second_starter.into(),
                },
            )
            .await?;

        let GetUserDataResponse {
            partner_pokemon, ..
        } = setup
            .req(Kangaskhan::get_user_data, GetUserDataRequest {})
            .await?
            .into_inner();
        assert_eq!(
            partner_pokemon,
            Some(PartnerPokemon {
                pokemon_number: second_starter.into(),
                is_shiny: false
            })
        );
        Ok(())
    }

    #[database_macros::test]
    async fn set_starter_pokemon_after_initial_setup_is_complete(
        pool: PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let mut starters = setup.api.pokemon.starters().map(|(num, _)| num);
        let first_starter = starters.next().expect("first starter");
        let second_starter = starters.next().expect("second starter");

        let trainer = *setup.api.trainers.iter().next().expect("first trainer").0;

        setup.do_initial_user_setup(first_starter, trainer).await?;

        let status = setup
            .req(
                Kangaskhan::set_partner_pokemon,
                SetPartnerPokemonRequest {
                    pokemon_number: second_starter.into(),
                },
            )
            .await
            .expect_err("You cannot switch partner after you completed the introduction");
        assert_eq!(status.code(), tonic::Code::InvalidArgument);

        Ok(())
    }

    #[database_macros::test]
    async fn set_partner_pokemon_to_caught_pokemon_after_setup_is_complete(
        pool: PgPool,
    ) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let mut starters = setup.api.pokemon.starters().map(|(num, _)| num);
        let first_starter = starters.next().expect("first starter");

        let trainer = *setup.api.trainers.iter().next().expect("first trainer").0;

        setup.do_initial_user_setup(first_starter, trainer).await?;
        catch_pokemon(
            &pool,
            setup.user.claims().user_id(),
            PokemonNumber(123),
            true,
        )
        .await?;

        let SetPartnerPokemonResponse {} = setup
            .req(
                Kangaskhan::set_partner_pokemon,
                SetPartnerPokemonRequest {
                    pokemon_number: 123,
                },
            )
            .await?
            .into_inner();

        let GetUserDataResponse {
            partner_pokemon, ..
        } = setup
            .req(Kangaskhan::get_user_data, GetUserDataRequest {})
            .await?
            .into_inner();
        assert_eq!(
            partner_pokemon,
            Some(PartnerPokemon {
                pokemon_number: 123,
                is_shiny: true
            })
        );

        Ok(())
    }

    #[database_macros::test]
    async fn set_trainer_sprite_works(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let trainer = *setup.api.trainers.iter().next().expect("some trainer").0;
        let trainer_number = u32::from(trainer);

        let SetTrainerSpriteResponse {} = setup
            .req(
                Kangaskhan::set_trainer_sprite,
                SetTrainerSpriteRequest { trainer_number },
            )
            .await?
            .into_inner();

        let GetUserDataResponse { trainer, .. } = setup
            .req(Kangaskhan::get_user_data, GetUserDataRequest {})
            .await?
            .into_inner();
        assert_eq!(trainer, Some(SelectedTrainer { trainer_number }));

        Ok(())
    }

    #[database_macros::test]
    async fn set_trainer_sprite_not_found(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let trainer_number = 1234;

        let status = setup
            .req(
                Kangaskhan::set_trainer_sprite,
                SetTrainerSpriteRequest { trainer_number },
            )
            .await
            .expect_err("No trainer with number 1234");
        assert_eq!(status.code(), tonic::Code::NotFound);

        Ok(())
    }

    #[database_macros::test]
    async fn set_trainer_sprite_invalid_argument(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let trainer_number = u32::MAX;

        let status = setup
            .req(
                Kangaskhan::set_trainer_sprite,
                SetTrainerSpriteRequest { trainer_number },
            )
            .await
            .expect_err("trainer number is limited");
        assert_eq!(status.code(), tonic::Code::InvalidArgument);

        Ok(())
    }

    #[database_macros::test]
    async fn choose_start_tile_not_found(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let start_tile_id = 404;

        let status = setup
            .req(
                Kangaskhan::choose_start_tile,
                ChooseStartTileRequest { start_tile_id },
            )
            .await
            .expect_err("start tile does not exist");
        assert_eq!(status.code(), tonic::Code::NotFound);

        Ok(())
    }

    #[database_macros::test]
    async fn cannot_choose_start_tile_twice(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 1, 2).await?;
        let start_tile_id_2 = smeargle::add_start_tile(&pool, map_id, 2, 2).await?;

        setup
            .req(
                Kangaskhan::choose_start_tile,
                ChooseStartTileRequest { start_tile_id },
            )
            .await?;
        let status = setup
            .req(
                Kangaskhan::choose_start_tile,
                ChooseStartTileRequest {
                    start_tile_id: start_tile_id_2,
                },
            )
            .await
            .expect_err("start tile was already set");
        assert_eq!(status.code(), tonic::Code::AlreadyExists);

        Ok(())
    }

    #[database_macros::test]
    async fn get_user_items_empty(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let GetUserItemsResponse { items } = setup
            .req(Kangaskhan::get_user_items, GetUserItemsRequest {})
            .await?
            .into_inner();

        assert!(items.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn get_user_items_after_ball_was_picked_up(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let item_id = setup.spawn_item(map_id, 0.0, 0.0, Item::Pokeball).await?;

        sync_collected_items(setup.user.claims().user_id(), &[item_id], &pool).await?;

        let GetUserItemsResponse { items } = setup
            .req(Kangaskhan::get_user_items, GetUserItemsRequest {})
            .await?
            .into_inner();

        assert_eq!(
            items,
            vec![CollectedItem {
                item: proto::Item::Pokeball.into(),
                amount: 1
            }]
        );
        Ok(())
    }

    #[database_macros::test]
    async fn user_items_get_summarized_via_amount_field(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let item_id_1 = setup.spawn_item(map_id, 0.0, 0.0, Item::Pokeball).await?;
        let item_id_2 = setup.spawn_item(map_id, 0.0, 0.0, Item::Pokeball).await?;
        let item_id_3 = setup.spawn_item(map_id, 0.0, 0.0, Item::Superball).await?;
        // sanity check - not all items should be collected
        let _item_id_4 = setup.spawn_item(map_id, 0.0, 0.0, Item::Superball).await?;

        sync_collected_items(
            setup.user.claims().user_id(),
            &[item_id_1, item_id_2, item_id_3],
            &pool,
        )
        .await?;

        let GetUserItemsResponse { items } = setup
            .req(Kangaskhan::get_user_items, GetUserItemsRequest {})
            .await?
            .into_inner();

        assert_eq!(
            items,
            vec![
                CollectedItem {
                    item: proto::Item::Pokeball.into(),
                    amount: 2
                },
                CollectedItem {
                    item: proto::Item::Superball.into(),
                    amount: 1
                }
            ]
        );
        Ok(())
    }

    #[database_macros::test]
    async fn get_warps_permission_denied(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let map_id = 1234;

        let status = setup
            .req(Kangaskhan::get_warps, GetWarpsRequest { map_id })
            .await
            .expect_err("Should fail because ReadMapDetails permission is missing");

        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn get_warps_not_found(pool: PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::ReadMapDetails]);
        let map_id = 1234;

        let GetWarpsResponse { warps } = setup
            .req(Kangaskhan::get_warps, GetWarpsRequest { map_id })
            .await?
            .into_inner();

        assert!(warps.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn add_warp_permission_denied(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let warp_request = AddWarpRequest {
            from_x: 0,
            from_y: 1,
            from_layer_number: 2,
            from_map_id: 3,
            to_x: 4,
            to_y: 5,
            to_layer_number: 6,
            to_map_id: 7,
        };

        let status = setup
            .req(Kangaskhan::add_warp, warp_request)
            .await
            .expect_err("Should fail because EditWarps permission is missing");

        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn add_read_delete_warp(pool: PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup
            .user
            .patch_permissions(&[Permission::ReadMapDetails, Permission::EditWarps]);

        let map_id_1 = smeargle::create_map(&pool, "map 1").await?;
        let map_id_2 = smeargle::create_map(&pool, "map 2").await?;

        let warp_request = AddWarpRequest {
            from_x: 0,
            from_y: 1,
            from_layer_number: 2,
            from_map_id: map_id_1.into(),
            to_x: 4,
            to_y: 5,
            to_layer_number: 6,
            to_map_id: map_id_2.into(),
        };

        let AddWarpResponse { warp_id } = setup
            .req(Kangaskhan::add_warp, warp_request)
            .await?
            .into_inner();

        let map_1_response = setup
            .req(
                Kangaskhan::get_warps,
                GetWarpsRequest {
                    map_id: map_id_1.into(),
                },
            )
            .await?
            .into_inner();
        let map_2_response = setup
            .req(
                Kangaskhan::get_warps,
                GetWarpsRequest {
                    map_id: map_id_2.into(),
                },
            )
            .await?
            .into_inner();

        assert_eq!(
            map_1_response.warps,
            vec![Warp {
                from_x: warp_request.from_x,
                from_y: warp_request.from_y,
                warp_id,
                from_layer_number: warp_request.from_layer_number,
                from_map_id: warp_request.from_map_id,
                to_x: warp_request.to_x,
                to_y: warp_request.to_y,
                to_layer_number: warp_request.to_layer_number,
                to_map_id: warp_request.to_map_id
            }]
        );

        assert_eq!(
            map_2_response.warps,
            vec![Warp {
                from_x: warp_request.from_x,
                from_y: warp_request.from_y,
                warp_id,
                from_layer_number: warp_request.from_layer_number,
                from_map_id: warp_request.from_map_id,
                to_x: warp_request.to_x,
                to_y: warp_request.to_y,
                to_layer_number: warp_request.to_layer_number,
                to_map_id: warp_request.to_map_id
            }]
        );

        let DeleteWarpResponse {} = setup
            .req(Kangaskhan::delete_warp, DeleteWarpRequest { warp_id })
            .await?
            .into_inner();

        let GetWarpsResponse { warps } = setup
            .req(
                Kangaskhan::get_warps,
                GetWarpsRequest {
                    map_id: map_id_1.into(),
                },
            )
            .await?
            .into_inner();

        assert!(warps.is_empty());

        Ok(())
    }

    #[database_macros::test]
    async fn add_warp_unknown_map(pool: PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::EditWarps]);

        let map_id_1 = smeargle::create_map(&pool, "map 1").await?;
        let map_id_2 = i64::from(map_id_1) + 1;

        let warp_request = AddWarpRequest {
            from_x: 0,
            from_y: 1,
            from_layer_number: 2,
            from_map_id: map_id_1.into(),
            to_x: 4,
            to_y: 5,
            to_layer_number: 6,
            to_map_id: map_id_2,
        };
        let status = setup
            .req(Kangaskhan::add_warp, warp_request)
            .await
            .expect_err("Should fail because map_id_2 is not known");

        assert_eq!(status.code(), tonic::Code::NotFound);

        Ok(())
    }

    #[database_macros::test]
    async fn delete_warp_permission_denied(pool: PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::EditWarps]);

        let map_id_1 = smeargle::create_map(&pool, "map 1").await?;
        let map_id_2 = smeargle::create_map(&pool, "map 2").await?;

        let warp_request = AddWarpRequest {
            from_x: 0,
            from_y: 1,
            from_layer_number: 2,
            from_map_id: map_id_1.into(),
            to_x: 4,
            to_y: 5,
            to_layer_number: 6,
            to_map_id: map_id_2.into(),
        };
        let AddWarpResponse { warp_id } = setup
            .req(Kangaskhan::add_warp, warp_request)
            .await?
            .into_inner();

        setup.user.patch_permissions(&[]);
        let status = setup
            .req(Kangaskhan::delete_warp, DeleteWarpRequest { warp_id })
            .await
            .expect_err("Should fail because EditWarps permission is missing");

        assert_eq!(status.code(), tonic::Code::PermissionDenied);

        Ok(())
    }

    struct Setup {
        api: Kangaskhan,
        user: gardevoir::LoggedInUser,
        item_channel: ItemChannel,
    }

    impl Setup {
        async fn req<'a, Req, Res, F, Fut>(
            &'a self,
            f: F,
            request: Req,
        ) -> Result<Response<Res>, Status>
        where
            F: Fn(&'a Kangaskhan, Request<Req>) -> Fut,
            Fut: Future<Output = Result<Response<Res>, Status>>,
        {
            f(&self.api, self.user.request(request)).await
        }

        async fn do_initial_user_setup(
            &self,
            starter: PokemonNumber,
            trainer: TrainerNumber,
        ) -> anyhow::Result<MapId> {
            let map_id = smeargle::create_map(&self.api.db_connection_pool, "my map").await?;
            let start_tile_id =
                smeargle::add_start_tile(&self.api.db_connection_pool, map_id, 1, 2).await?;

            self.req(
                Kangaskhan::choose_start_tile,
                ChooseStartTileRequest { start_tile_id },
            )
            .await?;

            self.req(
                Kangaskhan::set_trainer_sprite,
                SetTrainerSpriteRequest {
                    trainer_number: trainer.into(),
                },
            )
            .await?;

            self.req(
                Kangaskhan::set_partner_pokemon,
                SetPartnerPokemonRequest {
                    pokemon_number: starter.into(),
                },
            )
            .await?;

            Ok(map_id)
        }

        async fn spawn_item(
            &self,
            map_id: MapId,
            x: f32,
            y: f32,
            item: Item,
        ) -> anyhow::Result<ItemId> {
            arceus::spawn_item(
                &self.api.db_connection_pool,
                &self.item_channel,
                map_id,
                x,
                y,
                item,
            )
            .await
        }
    }

    async fn setup(pool: &sqlx::PgPool) -> anyhow::Result<Setup> {
        let settings = Settings::read()?;
        let pokemon = PokemonConfig::read()?;
        let trainers = TrainerConfig::read()?;
        let auth_ctx = gardevoir::create_auth_ctx().await?;
        let user = gardevoir::UserBuilder::from_name("user")
            .create(&auth_ctx, pool)
            .await?;

        let warp_cache: WarpCache = WarpCache::new(pool.clone(), settings.warp_cache);
        let player_channel = PlayerChannel::new("player");
        let item_channel = ItemChannel::new("item");

        let api = Kangaskhan {
            auth_ctx,
            db_connection_pool: pool.clone(),
            pokemon,
            trainers,
            warp_cache,
            player_channel,
        };
        Ok(Setup {
            api,
            user,
            item_channel,
        })
    }
}
