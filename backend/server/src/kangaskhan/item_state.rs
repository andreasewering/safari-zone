use std::{collections::HashSet, sync::Arc};

use dashmap::DashMap;
use futures_util::StreamExt;
use sqlx::PgPool;
use time::OffsetDateTime;

use crate::{
    channels::{ItemChannel, ItemMessage, ItemMessageWrapper},
    data::{Item, ItemId, MapId, Position},
    graceful_shutdown, shutdown,
};

#[derive(Clone)]
pub struct ItemState {
    pool: PgPool,
    state: Arc<DashMap<ItemId, SpawnedItemWithMetadata>>,
    map_state: Arc<DashMap<MapId, HashSet<ItemId>>>,
    channel: ItemChannel,
    shutdown_ctrl: shutdown::Ctrl,
}

struct SpawnedItemWithMetadata {
    item: SpawnedItem,
    changes_persisted: bool,
}

#[derive(Clone)]
pub struct SpawnedItem {
    pub item: Item,
    pub map_id: MapId,
    pub x: f32,
    pub y: f32,
    pub layer_number: i32,
    pub despawn_time: OffsetDateTime,
}

impl ItemState {
    pub async fn new(pool: &PgPool, channel: &ItemChannel, shutdown_ctrl: shutdown::Ctrl) -> Self {
        let state = Arc::new(DashMap::<ItemId, SpawnedItemWithMetadata>::new());
        let map_state = Arc::new(DashMap::<MapId, HashSet<ItemId>>::new());

        let query_result = sqlx::query_as!(
            SpawnedItemEntity,
            r#"SELECT id, x, y, layer_number, 
              map_id as "map_id: _",
              item as "item: _", despawn_time
          FROM item WHERE despawn_time > NOW()"#
        )
        .fetch_all(pool)
        .await;

        let entities = match query_result {
            Err(error) => {
                tracing::error!(message = "Failed to restore items from database.", %error);
                Vec::new()
            }
            Ok(entities) => entities,
        };

        let mut item_state = Self {
            state,
            map_state,
            pool: pool.clone(),
            channel: channel.clone(),
            shutdown_ctrl,
        };

        item_state.connect_to_channel();
        item_state.spawn_bulk_persistence_task();
        for entity in entities {
            let item_id = entity.id;
            let item = SpawnedItem::from(entity);
            item_state.persist_item(item_id, item);
        }

        item_state
    }

    pub fn get_items_on_map(&self, map_id: MapId) -> Vec<ItemMessageWrapper> {
        let Some(item_ids) = self.map_state.get(&map_id) else {
            return Vec::new();
        };
        let now = OffsetDateTime::now_utc();
        item_ids
            .iter()
            .filter_map(|item_id| {
                let item = self.state.get(item_id)?;
                let despawn_time = item.item.despawn_time;
                if despawn_time <= now {
                    return None;
                }
                let msg = ItemMessageWrapper {
                    map_id: item.item.map_id,
                    item_id: *item_id,
                    msg: ItemMessage::New {
                        item: item.item.item,
                        x: item.item.x,
                        y: item.item.y,
                        layer_number: item.item.layer_number,
                        despawn_time,
                    },
                };
                Some(msg)
            })
            .collect()
    }

    fn connect_to_channel(&mut self) {
        let mut messages = self.channel.receive();
        let clone = self.clone();
        let main_loop = async move {
            while let Some(message) = messages.next().await {
                if let Err(error) = clone.handle_message(message).await {
                    tracing::error!(
                        message = "Failed to handle item message in item state",
                        %error
                    );
                }
            }
        };
        let shutdown_ctrl = self.shutdown_ctrl.clone();
        tokio::spawn(async move {
            graceful_shutdown! {
                ctrl: shutdown_ctrl,
                name: "item_state::connect_to_channel",
                cleanup: () => {
                    tracing::info!("Gracefully shutdown item state subscription to item channel.");
                },
                _ = main_loop => {
                            tracing::error!("Subscription to item channel ended early.");
                }
            }
        });
    }

    fn spawn_bulk_persistence_task(&self) {
        let clone = self.clone();
        let main_loop = async move {
            let mut interval = tokio::time::interval(std::time::Duration::from_secs(300));
            loop {
                interval.tick().await;
                clone.bulk_persist().await;
            }
        };
        let clone = self.clone();
        tokio::spawn(async move {
            let shutdown_ctrl = clone.shutdown_ctrl.clone();
            graceful_shutdown! {
              ctrl: shutdown_ctrl,
              name: "item_state::spawn_bulk_persistence_task",
              cleanup: () => {
                  tracing::info!("Shutting down item state, persisting unsaved changes");
                  clone.bulk_persist().await;
                  tracing::info!("Gracefully shutdown item state");
              },
              _ = main_loop => {
                  tracing::error!("Bulk persistence task finished early.");
              }
            }
        });
    }

    async fn bulk_persist(&self) {
        let non_persisted = self.state.iter_mut().filter(|e| !e.changes_persisted);

        let mut item_ids = Vec::new();
        let mut map_ids = Vec::new();
        let mut xs = Vec::new();
        let mut ys = Vec::new();
        let mut layer_numbers = Vec::new();
        let mut despawn_times = Vec::new();
        for mut item_with_metadata in non_persisted {
            item_with_metadata.value_mut().changes_persisted = true;
            let item = &item_with_metadata.item;
            item_ids.push(*item_with_metadata.key());

            map_ids.push(item.map_id);
            xs.push(item.x);
            ys.push(item.y);
            layer_numbers.push(item.layer_number);
            despawn_times.push(item.despawn_time);
        }
        let bulk_update_result = sqlx::query!(
            "UPDATE item
            SET map_id = u.map_id,
                x = u.x,
                y = u.y,
                layer_number = u.layer_number,
                despawn_time = u.despawn_time
            FROM UNNEST(
                $1::BIGINT[], 
                $2::BIGINT[], 
                $3::REAL[], 
                $4::REAL[], 
                $5::INT[],
                $6::TIMESTAMP WITH TIME ZONE[])
                AS u(id, map_id, x, y, layer_number, despawn_time)
            WHERE item.id = u.id
                ",
            item_ids as Vec<ItemId>,
            map_ids as Vec<MapId>,
            &xs,
            &ys,
            &layer_numbers,
            &despawn_times,
        )
        .execute(&self.pool)
        .await;
        if let Err(error) = bulk_update_result {
            tracing::warn!(message = "Failed to persist item updates to the database", %error);
        }
    }

    async fn handle_message(
        &self,
        ItemMessageWrapper {
            map_id,
            item_id,
            msg,
        }: ItemMessageWrapper,
    ) -> Result<(), sqlx::Error> {
        match msg {
            ItemMessage::New {
                item,
                x,
                y,
                layer_number,
                despawn_time,
            } => self.persist_item(
                item_id,
                SpawnedItem {
                    map_id,
                    x,
                    y,
                    layer_number,
                    item,
                    despawn_time,
                },
            ),
            ItemMessage::Remove => {
                self.remove_item(item_id);
            }
        }
        Ok(())
    }

    fn persist_item(&self, item_id: ItemId, item: SpawnedItem) {
        let map_id = item.map_id;
        let item_with_metadata = SpawnedItemWithMetadata {
            item,
            changes_persisted: true,
        };
        self.state.insert(item_id, item_with_metadata);
        let mut map_entry = self.map_state.entry(map_id).or_default();
        map_entry.insert(item_id);
    }

    fn remove_item(&self, item_id: ItemId) {
        let removal_result = self.state.remove(&item_id);
        let Some((_, item)) = removal_result else {
            return;
        };
        let map_id = item.item.map_id;
        let dashmap::Entry::Occupied(mut entry) = self.map_state.entry(map_id) else {
            return;
        };
        let item_ids = entry.get_mut();
        item_ids.remove(&item_id);
        if item_ids.is_empty() {
            entry.remove();
        }
    }

    pub fn items_on_position(&self, map_id: MapId, position: Position) -> Vec<(ItemId, Item)> {
        let Some(item_ids) = self.map_state.get(&map_id) else {
            return Vec::new();
        };
        item_ids
            .iter()
            .filter_map(|item_id| {
                let item = self.state.get(item_id)?;
                if (item.item.x.round() as i32) != position.x
                    || (item.item.y.round() as i32) != position.y
                    || item.item.layer_number != position.layer_number
                {
                    return None;
                }
                Some((*item_id, item.item.item))
            })
            .collect()
    }
}

struct SpawnedItemEntity {
    id: ItemId,
    x: f32,
    y: f32,
    layer_number: i32,
    map_id: MapId,
    item: Item,
    despawn_time: OffsetDateTime,
}

impl From<SpawnedItemEntity> for SpawnedItem {
    fn from(
        SpawnedItemEntity {
            id: _,
            x,
            y,
            layer_number,
            map_id,
            item,
            despawn_time,
        }: SpawnedItemEntity,
    ) -> Self {
        SpawnedItem {
            map_id,
            x,
            y,
            layer_number,
            item,
            despawn_time,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use crate::channels::Channel;

    use super::*;
    use crate::smeargle::test_helper as smeargle;

    #[database_macros::test]
    async fn item_state_spawn_and_delete_item(pool: PgPool) -> anyhow::Result<()> {
        let item_channel = Channel::new("item");
        let shutdown_controller = shutdown::Ctrl::new();
        let state = ItemState::new(&pool, &item_channel, shutdown_controller.clone()).await;
        let map_id = smeargle::create_map(&pool, "map_name").await?;

        // Initial: no items on map
        assert_eq!(state.get_items_on_map(map_id), vec![]);

        // Create a new item on the map
        let item_message = ItemMessage::New {
            item: Item::Pokeball,
            x: 1.0,
            y: 2.0,
            layer_number: 3,
            despawn_time: OffsetDateTime::now_utc() + Duration::from_secs(100),
        };
        let item_id = ItemId::from(1);
        let item_message = ItemMessageWrapper {
            map_id,
            item_id,
            msg: item_message,
        };

        // Communicate the creation via a channel
        item_channel.send(item_message);
        tokio::time::sleep(Duration::from_millis(10)).await;

        // The inner subscription to the channel should have picked up the message
        // and created the item in the item state
        assert_eq!(state.get_items_on_map(map_id), vec![item_message]);

        // Delete the item
        // Communicate the deletion via the channel
        item_channel.send(ItemMessageWrapper {
            map_id,
            item_id,
            msg: ItemMessage::Remove,
        });
        tokio::time::sleep(Duration::from_millis(10)).await;

        // The inner subscription to the channel should have picked up the message and deleted the item from the state
        assert_eq!(state.get_items_on_map(map_id), vec![]);

        Ok(())
    }
}
