use crate::data::{Direction, MapId, Position};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct GameStateSnapshot {
    pub position: Position,
    pub map_id: MapId,
    pub direction: Direction,
}
