use std::{collections::VecDeque, iter::zip};

use enumset::EnumSet;

use crate::data::{Direction, Position};

/// Invariants:
/// - does not contain the same position more than once
/// - has at least one trace entry
/// - trace length is within 0..=limit
#[derive(Debug)]
pub struct UserTrace {
    trace: VecDeque<(Direction, Position)>,
    limit: u8,
}

#[derive(Debug, PartialEq, Eq)]
pub enum PositionInTrace {
    NotFound,
    WithOptimalPathToTarget {
        direction: Direction,
        position: Position,
    },
    IsEndOfTrace {
        previous_move: Option<(Direction, Position)>,
    },
    WithSuboptimalPathToTarget {
        number_of_steps_to_be_better: u8,
    },
}

impl UserTrace {
    pub fn new(limit: u8, initial_position: Position, initial_direction: Direction) -> Self {
        let mut trace = VecDeque::with_capacity(limit.into());
        trace.push_back((initial_direction, initial_position));
        UserTrace { trace, limit }
    }

    pub fn limit(&self) -> u8 {
        self.limit
    }

    /// should only be called at initalisation time since it does not check
    /// the internal invariants!
    pub(super) fn add_to_front(&mut self, value: (Direction, Position)) {
        self.trace.push_front(value);
    }

    pub fn last(&self) -> (Direction, Position) {
        *self
            .trace
            .back()
            .expect("UserTrace Invariant: has at least one entry at all time")
    }

    pub fn add(&mut self, user_move: (Direction, Position)) {
        if let Some(pos) = self.trace.iter().position(|(_, p)| *p == user_move.1) {
            // Remove all elements in front of the found element
            for _ in 0..=pos {
                self.trace.pop_front();
            }
        } else if self.trace.len() >= self.limit.into() {
            self.trace.pop_front();
        }
        self.trace.push_back(user_move);
    }

    pub fn find_pos(&self, pos: &Position) -> PositionInTrace {
        let trace_len = self.trace.len();
        let mut iter = self.trace.iter().enumerate();
        let Some(index) = find_index_and_advance_iter(&mut iter, pos) else {
            return PositionInTrace::NotFound;
        };
        let Some((_, next)) = iter.next() else {
            let previous_move = if index == 0 {
                None
            } else {
                self.trace.get(index - 1).copied()
            };
            return PositionInTrace::IsEndOfTrace { previous_move };
        };

        let mut dir_set: EnumSet<Direction> = iter.map(|(_, (dir, _))| *dir).collect();
        dir_set.insert(next.0);

        let path_is_suboptimal =
            Direction::iter().any(|dir| dir_set.contains(dir) && dir_set.contains(dir.turn_180()));

        if path_is_suboptimal {
            let number_of_steps_to_be_better = u8::try_from(trace_len - index - 2)
                .expect("trace is at most u8 long because of UserTrace invariant");
            return PositionInTrace::WithSuboptimalPathToTarget {
                number_of_steps_to_be_better,
            };
        }

        let (direction, position) = *next;
        PositionInTrace::WithOptimalPathToTarget {
            direction,
            position,
        }
    }

    pub fn path_matches_trace(&self, path: &[(Direction, Position)]) -> usize {
        let mut count = 0;
        // reverse since the target must be the same
        for ((_, trace_pos), (_, path_pos)) in zip(self.trace.iter().rev(), path.iter().rev()) {
            if trace_pos == path_pos {
                count += 1;
            } else {
                break;
            }
        }
        count
    }
}

fn find_index_and_advance_iter<'a, 'b, 'c, I>(
    iter: &'a mut I,
    target: &'b Position,
) -> Option<usize>
where
    I: Iterator<Item = (usize, &'c (Direction, Position))>,
{
    for (index, (_, p)) in iter {
        if p == target {
            return Some(index);
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_different_position() {
        let initial_position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        let mut trace = UserTrace::new(8, initial_position, Direction::D);
        let next_position = Position {
            x: 0,
            y: 1,
            layer_number: 0,
        };
        trace.add((Direction::D, next_position));
        assert_eq!(
            trace.find_pos(&initial_position),
            PositionInTrace::WithOptimalPathToTarget {
                direction: Direction::D,
                position: next_position,
            }
        );
    }

    #[test]
    fn add_duplicate_position() {
        let initial_position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        let mut trace = UserTrace::new(8, initial_position, Direction::D);
        let next_position = Position {
            x: 0,
            y: 1,
            layer_number: 0,
        };

        trace.add((Direction::D, next_position));
        trace.add((Direction::U, initial_position));

        assert_eq!(
            trace.find_pos(&initial_position),
            PositionInTrace::IsEndOfTrace {
                previous_move: Some((Direction::D, next_position))
            },
        );
        assert_eq!(
            trace.find_pos(&next_position),
            PositionInTrace::WithOptimalPathToTarget {
                direction: Direction::U,
                position: initial_position
            }
        );
    }

    #[test]
    fn limit_exceeded() {
        let initial_position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        let mut trace = UserTrace::new(2, initial_position, Direction::D);
        trace.add((Direction::D, initial_position.move_in_dir(Direction::D)));
        trace.add((
            Direction::D,
            initial_position
                .move_in_dir(Direction::D)
                .move_in_dir(Direction::D),
        ));
        assert_eq!(trace.find_pos(&initial_position), PositionInTrace::NotFound);
    }

    #[test]
    fn better_path() {
        let initial_position = Position {
            x: 0,
            y: 0,
            layer_number: 0,
        };
        let mut trace = UserTrace::new(4, initial_position, Direction::D);
        trace.add((Direction::D, initial_position.move_in_dir(Direction::D)));
        trace.add((
            Direction::L,
            initial_position
                .move_in_dir(Direction::D)
                .move_in_dir(Direction::L),
        ));
        trace.add((
            Direction::U,
            initial_position
                .move_in_dir(Direction::D)
                .move_in_dir(Direction::L)
                .move_in_dir(Direction::U),
        ));

        assert_eq!(
            trace.find_pos(&initial_position),
            PositionInTrace::WithSuboptimalPathToTarget {
                number_of_steps_to_be_better: 2
            }
        )
    }
}
