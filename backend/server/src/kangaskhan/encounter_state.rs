use std::{collections::HashSet, sync::Arc};

use dashmap::DashMap;
use futures_util::StreamExt;
use sqlx::PgPool;
use time::OffsetDateTime;

use crate::{
    channels::{NewEncounter, PokemonChannel, PokemonMessage, PokemonMessageWrapper},
    data::{Direction, EncounterId, MapId, PokemonNumber, Position},
    graceful_shutdown, shutdown,
};

#[derive(Clone)]
pub struct EncounterState {
    pool: PgPool,
    state: Arc<DashMap<EncounterId, EncounterWithMetadata>>,
    map_state: Arc<DashMap<MapId, HashSet<EncounterId>>>,
    channel: PokemonChannel,
    shutdown_ctrl: shutdown::Ctrl,
}

struct EncounterWithMetadata {
    encounter: Encounter,
    changes_persisted: bool,
}

#[derive(Clone, Copy)]
pub struct Encounter {
    pub map_id: MapId,
    pub position: Position,
    pub is_shiny: bool,
    pub direction: Direction,
    pub pokemon_number: PokemonNumber,
    pub despawn_time: OffsetDateTime,
}

impl EncounterState {
    pub async fn new(
        pool: &PgPool,
        channel: &PokemonChannel,
        shutdown_controller: shutdown::Ctrl,
    ) -> Self {
        let state = Arc::new(DashMap::<EncounterId, EncounterWithMetadata>::new());
        let map_state = Arc::new(DashMap::<MapId, HashSet<EncounterId>>::new());

        let query_result = sqlx::query_as!(
            EncounterEntity,
            r#"SELECT id, x, y, layer_number, 
              map_id as "map_id: _", direction as "direction: _", 
              pokemon_number as "pokemon_number: _", is_shiny, despawn_time 
          FROM encounter WHERE despawn_time > NOW()"#
        )
        .fetch_all(pool)
        .await;

        let entities = match query_result {
            Err(error) => {
                tracing::error!(message = "Failed to restore encounters from database.", %error);
                Vec::new()
            }
            Ok(entities) => entities,
        };

        let mut encounter_state = Self {
            state,
            map_state,
            pool: pool.clone(),
            channel: channel.clone(),
            shutdown_ctrl: shutdown_controller,
        };

        encounter_state.connect_to_channel();
        encounter_state.spawn_bulk_persistence_task();
        for entity in entities {
            let encounter_id = entity.id;
            let encounter = Encounter::from(entity);
            encounter_state.persist_encounter(encounter_id, encounter);
        }

        encounter_state
    }

    pub fn get_encounter_on_position(
        &self,
        map_id: MapId,
        position: Position,
    ) -> Option<(EncounterId, Encounter)> {
        let encounters_on_map = self.map_state.get(&map_id)?;
        let target = encounters_on_map
            .iter()
            .filter_map(|encounter_id| self.state.get(encounter_id))
            .find(|encounter| encounter.encounter.position == position)?;
        let target_id = *target.key();
        Some((target_id, target.encounter))
    }

    pub fn get_encounters_on_map(&self, map_id: MapId) -> Vec<PokemonMessageWrapper> {
        let Some(encounter_ids) = self.map_state.get(&map_id) else {
            return Vec::new();
        };
        encounter_ids
            .iter()
            .filter_map(|encounter_id| {
                let encounter = self.state.get(encounter_id)?;
                let msg = PokemonMessageWrapper {
                    map_id: encounter.encounter.map_id,
                    encounter_id: *encounter_id,
                    message: PokemonMessage::New(NewEncounter {
                        position: encounter.encounter.position,
                        direction: encounter.encounter.direction,
                        pokemon_number: encounter.encounter.pokemon_number,
                        is_shiny: encounter.encounter.is_shiny,
                        despawn_time: encounter.encounter.despawn_time,
                    }),
                };
                Some(msg)
            })
            .collect()
    }

    fn connect_to_channel(&mut self) {
        let mut messages = self.channel.receive();
        let clone = self.clone();
        let main_loop = async move {
            while let Some(message) = messages.next().await {
                if let Err(error) = clone.handle_message(message).await {
                    tracing::error!(
                        message = "Failed to handle pokemon message in encounter state",
                        %error
                    );
                }
            }
        };
        let shutdown_ctrl = self.shutdown_ctrl.clone();
        tokio::spawn(async move {
            graceful_shutdown! {
                ctrl: shutdown_ctrl,
                name: "encounter_state::connect_to_channel",
                cleanup: () => {
                    tracing::info!("Gracefully shutdown encounter state subscription to pokemon channel.");
                },
                _ = main_loop => {
                            tracing::error!("Subscription to channel ended early.");
                }
            }
        });
    }

    fn spawn_bulk_persistence_task(&self) {
        let clone = self.clone();
        let main_loop = async move {
            let mut interval = tokio::time::interval(std::time::Duration::from_secs(300));
            loop {
                interval.tick().await;
                clone.bulk_persist().await;
            }
        };
        let clone = self.clone();
        tokio::spawn(async move {
            let shutdown_ctrl = clone.shutdown_ctrl.clone();
            graceful_shutdown! {
              ctrl: shutdown_ctrl,
              name: "encounter_state::spawn_bulk_persistence_task",
              cleanup: () => {
                  tracing::info!("Shutting down encounter state, persisting unsaved changes");
                  clone.bulk_persist().await;
                  tracing::info!("Gracefully shutdown encounter state");
              },
              _ = main_loop => {
                  tracing::error!("Bulk persistence task finished early.");
              }
            }
        });
    }

    async fn bulk_persist(&self) {
        let non_persisted = self.state.iter_mut().filter(|e| !e.changes_persisted);

        let mut encounter_ids = Vec::new();
        let mut xs = Vec::new();
        let mut ys = Vec::new();
        let mut layer_numbers = Vec::new();
        let mut directions = Vec::new();
        for mut encounter_with_metadata in non_persisted {
            encounter_with_metadata.value_mut().changes_persisted = true;
            let encounter = &encounter_with_metadata.encounter;
            encounter_ids.push(*encounter_with_metadata.key());
            xs.push(encounter.position.x);
            ys.push(encounter.position.y);
            layer_numbers.push(encounter.position.layer_number);
            directions.push(encounter.direction);
        }
        let bulk_update_result = sqlx::query!(
            "UPDATE encounter SET
                    x = bulk_query.x,
                    y = bulk_query.y,
                    layer_number = bulk_query.layer_number,
                    direction = bulk_query.direction
                FROM (SELECT * FROM UNNEST(
                    $1::BIGINT[], 
                    $2::INT[], 
                    $3::INT[], 
                    $4::INT[], 
                    $5::DIRECTION[]) 
                  AS t(id, x, y, layer_number, direction)) AS bulk_query
                WHERE encounter.id = bulk_query.id",
            encounter_ids as Vec<EncounterId>,
            &xs,
            &ys,
            &layer_numbers,
            directions as Vec<Direction>
        )
        .execute(&self.pool)
        .await;
        if let Err(error) = bulk_update_result {
            tracing::warn!(message = "Failed to persist encounter updates to the database", %error);
        }
    }

    async fn handle_message(
        &self,
        PokemonMessageWrapper {
            map_id,
            encounter_id,
            message,
        }: PokemonMessageWrapper,
    ) -> Result<(), sqlx::Error> {
        match message {
            PokemonMessage::New(NewEncounter {
                position,
                direction,
                pokemon_number,
                is_shiny,
                despawn_time,
            }) => self.persist_encounter(
                encounter_id,
                Encounter {
                    map_id,
                    position,
                    direction,
                    is_shiny,
                    pokemon_number,
                    despawn_time,
                },
            ),
            PokemonMessage::Move {
                position,
                direction,
            } => {
                self.modify_encounter(encounter_id, |encounter| {
                    encounter.position = position;
                    encounter.direction = direction;
                });
            }
            PokemonMessage::Remove => {
                self.remove_encounter(encounter_id).await?;
            }
            PokemonMessage::Caught { by: _ } => {
                self.remove_encounter(encounter_id).await?;
            }
            PokemonMessage::BrokeOut { from: _ } => {
                // From the perspective of this module, nothing changes
            }
        }
        Ok(())
    }

    fn persist_encounter(&self, encounter_id: EncounterId, encounter: Encounter) {
        let map_id = encounter.map_id;
        let encounter_with_metadata = EncounterWithMetadata {
            encounter,
            changes_persisted: true,
        };
        self.state.insert(encounter_id, encounter_with_metadata);
        let mut map_entry = self.map_state.entry(map_id).or_default();
        map_entry.insert(encounter_id);
    }

    fn modify_encounter<F>(&self, encounter_id: EncounterId, mut modify: F)
    where
        F: FnMut(&mut Encounter),
    {
        self.state
            .entry(encounter_id)
            .and_modify(|encounter_with_metadata| {
                modify(&mut encounter_with_metadata.encounter);
                encounter_with_metadata.changes_persisted = false;
            });
    }

    async fn remove_encounter(&self, encounter_id: EncounterId) -> Result<(), sqlx::Error> {
        sqlx::query!(
            "DELETE FROM encounter WHERE id = $1",
            encounter_id as EncounterId
        )
        .execute(&self.pool)
        .await?;
        let removal_result = self.state.remove(&encounter_id);
        let Some((_, encounter)) = removal_result else {
            return Ok(());
        };
        let map_id = encounter.encounter.map_id;
        let dashmap::Entry::Occupied(mut entry) = self.map_state.entry(map_id) else {
            return Ok(());
        };
        let encounter_ids = entry.get_mut();
        encounter_ids.remove(&encounter_id);
        if encounter_ids.is_empty() {
            entry.remove();
        }

        Ok(())
    }
}

struct EncounterEntity {
    id: EncounterId,
    x: i32,
    y: i32,
    layer_number: i32,
    map_id: MapId,
    direction: Direction,
    pokemon_number: PokemonNumber,
    despawn_time: OffsetDateTime,
    is_shiny: bool,
}

impl From<EncounterEntity> for Encounter {
    fn from(
        EncounterEntity {
            id: _,
            x,
            y,
            layer_number,
            map_id,
            is_shiny,
            direction,
            pokemon_number,
            despawn_time,
        }: EncounterEntity,
    ) -> Self {
        let position = Position { x, y, layer_number };
        Encounter {
            map_id,
            position,
            is_shiny,
            direction,
            pokemon_number,
            despawn_time,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use crate::channels::Channel;

    use super::*;
    use crate::smeargle::test_helper as smeargle;

    #[database_macros::test]
    async fn encounter_state_spawn_and_move_encounter(pool: PgPool) -> anyhow::Result<()> {
        let pokemon_channel = Channel::new("pokemon");
        let shutdown_ctrl = shutdown::Ctrl::new();
        let state = EncounterState::new(&pool, &pokemon_channel, shutdown_ctrl.clone()).await;
        let map_id = smeargle::create_map(&pool, "map_name").await?;

        // Initial: no encounters on map
        assert_eq!(state.get_encounters_on_map(map_id), vec![]);

        // Create a new encounter on the map
        let encounter = NewEncounter {
            position: Position {
                x: 1,
                y: 2,
                layer_number: 3,
            },
            direction: Direction::D,
            pokemon_number: PokemonNumber(5),
            is_shiny: false,
            despawn_time: OffsetDateTime::now_utc() + Duration::from_secs(60),
        };
        let encounter_id = EncounterId::from(1);
        let pokemon_message = PokemonMessageWrapper {
            map_id,
            encounter_id,
            message: PokemonMessage::New(encounter.clone()),
        };

        // Communicate the creation via a channel
        pokemon_channel.send(pokemon_message.clone());
        tokio::time::sleep(Duration::from_millis(10)).await;

        // The inner subscription to the channel should have picked up the message
        // and created the encounter in the encounter state
        assert_eq!(
            state.get_encounters_on_map(map_id),
            vec![pokemon_message.clone()]
        );

        // Move the encounter
        let new_position = Position {
            x: 2,
            y: 2,
            layer_number: 3,
        };
        let new_direction = Direction::R;

        // Communicate the move via the channel
        pokemon_channel.send(PokemonMessageWrapper {
            map_id,
            encounter_id,
            message: PokemonMessage::Move {
                position: new_position,
                direction: new_direction,
            },
        });
        tokio::time::sleep(Duration::from_millis(10)).await;

        // The inner subscription to the channel should have picked up the message
        // and moved the encounter in the encounter state
        assert_eq!(
            state.get_encounters_on_map(map_id),
            vec![PokemonMessageWrapper {
                message: PokemonMessage::New(NewEncounter {
                    position: new_position,
                    direction: new_direction,
                    ..encounter
                }),
                ..pokemon_message
            }]
        );

        // Delete the encounter
        // Communicate the deletion via the channel
        pokemon_channel.send(PokemonMessageWrapper {
            map_id,
            encounter_id,
            message: PokemonMessage::Remove,
        });
        tokio::time::sleep(Duration::from_millis(10)).await;

        // The inner subscription to the channel should have picked up the message and deleted the encounter from the state
        assert_eq!(state.get_encounters_on_map(map_id), vec![]);

        Ok(())
    }
}
