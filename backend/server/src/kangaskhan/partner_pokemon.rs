use super::user_trace::{PositionInTrace, UserTrace};
use tokio::sync::broadcast;

use crate::{
    configuration::{self, PokemonConfig},
    data::{Direction, MoveAbilities, Position, UserId},
    kangaskhan::pathfinder,
    shutdown,
    smeargle::MapMoveRuleProvider,
};

/// A partner pokemon has knowledge of map move rules and its own move abilities.
/// It also receives messages from it's owner whenever his position changes.
/// It communicates updates via the on_move function.
/// The worker may be stopped when
/// - the player changes map (the pokemon then just teleports next to the player on the new map)
/// - the player changes their partner pokemon (this should usually not happen while the worker is running, since it's on another page, but the player could use two tabs for example)
/// - the maps move rules change (the pokemon just gets teleported to avoid race conditions causing it to get blocked)
///
/// It follows the user around with the following rules:
/// - if the player is more than 10 steps away from the pokemon, it will teleport directly to the player
/// - it will always takes a shortest path to the player and will adjust mid path
/// - if possible with the previous condition, it will use the same path as the player did
pub struct PartnerPokemonWorker<M> {
    move_rule_provider: M,
    position: Position,
    direction: Direction,
    user_id: UserId,
    receiver: broadcast::Receiver<(Direction, Position)>,
    partner_pokemon_config: configuration::Pokemon,
    pokemon_config: PokemonConfig,
    user_trace: UserTrace,
}

pub struct PartnerPokemonWorkerArgs<M> {
    pub move_rule_provider: M,
    pub receiver: broadcast::Receiver<(Direction, Position)>,
    pub user_id: UserId,
    pub initial_user_position: Position,
    pub partner_pokemon_config: configuration::Pokemon,
    pub pokemon_config: PokemonConfig,
    pub trace_length: u8,
}

impl<M> PartnerPokemonWorker<M>
where
    M: MapMoveRuleProvider,
{
    pub fn new(
        PartnerPokemonWorkerArgs {
            move_rule_provider,
            receiver,
            initial_user_position,
            user_id,
            partner_pokemon_config,
            pokemon_config,
            trace_length,
        }: PartnerPokemonWorkerArgs<M>,
    ) -> Self {
        let (direction, position) = Self::find_tile_next_to(
            &move_rule_provider,
            partner_pokemon_config.move_ability,
            initial_user_position,
        )
        .unwrap_or((Direction::D, initial_user_position));

        let mut user_trace = UserTrace::new(trace_length, initial_user_position, direction);
        user_trace.add_to_front((direction, position));

        PartnerPokemonWorker {
            move_rule_provider,
            position,
            direction,
            user_id,
            receiver,
            partner_pokemon_config,
            pokemon_config,
            user_trace,
        }
    }

    fn find_tiles_next_to(
        move_rule_provider: &M,
        move_ability: MoveAbilities,
        user_position: Position,
    ) -> Vec<(Direction, Position)> {
        Direction::iter()
            .filter_map(|dir| {
                let next_pos = move_rule_provider.move_in_dir(user_position, move_ability, dir)?;
                Some((dir, next_pos))
            })
            .collect()
    }

    fn find_tile_next_to(
        move_rule_provider: &M,
        move_ability: MoveAbilities,
        user_position: Position,
    ) -> Option<(Direction, Position)> {
        let (dir, pos) = PartnerPokemonWorker::find_tiles_next_to(
            move_rule_provider,
            move_ability,
            user_position,
        )
        .into_iter()
        .next()?;
        Some((dir.turn_180(), pos))
    }

    pub async fn work<F>(mut self, shutdown_ctrl: shutdown::Ctrl, mut on_move: F)
    where
        F: FnMut(Position, Direction),
    {
        on_move(self.position, self.direction);
        let user_id = self.user_id;
        let _prevent_shutdown =
            shutdown_ctrl.prevent_shutdown(format!("partner_pokemon_{user_id}"));
        let move_duration = self
            .pokemon_config
            .move_duration(self.partner_pokemon_config.move_speed);
        let mut movement_interval = tokio::time::interval(move_duration);
        let mut is_moving = false;
        loop {
            tokio::select! {
                _ = shutdown_ctrl.cancelled() => {
                    tracing::debug!(%user_id, "Gracefully shutdown partner pokemon worker");
                    break;
                }
                _ = movement_interval.tick(), if is_moving => {
                    is_moving = self.move_towards_user(&mut on_move);
                }
                user_move = self.receiver.recv() => {
                    match user_move {
                        Err(error) => {
                            tracing::error!(%error, "Failed to add user move to partner trace")
                        },
                        Ok(user_move) => {
                            tracing::debug!(position=?user_move.1, direction=%user_move.0, "Received position for partner trace");
                            self.user_trace.add(user_move);
                        }
                    }
                    if !is_moving {
                       is_moving = self.move_towards_user(&mut on_move);
                       movement_interval.reset();
                    }
                }
            }
        }
    }

    /// Returns whether a move was triggered or not
    fn move_towards_user<F>(&mut self, on_move: &mut F) -> bool
    where
        F: FnMut(Position, Direction),
    {
        let (user_direction, user_position) = self.user_trace.last();
        let opt_move = match self.user_trace.find_pos(&self.position) {
            PositionInTrace::NotFound => {
                self.find_best_path(user_position, self.user_trace.limit())
            }
            PositionInTrace::WithOptimalPathToTarget {
                direction,
                position,
            } => Some((direction, position)),
            PositionInTrace::IsEndOfTrace { previous_move } => previous_move,
            PositionInTrace::WithSuboptimalPathToTarget {
                number_of_steps_to_be_better,
            } => self.find_best_path(user_position, number_of_steps_to_be_better),
        }
        .or_else(|| {
            let tiles = Self::find_tiles_next_to(
                &self.move_rule_provider,
                self.partner_pokemon_config.move_ability,
                user_position,
            );
            tiles
                .iter()
                .find(|(dir, _)| dir.turn_180() == user_direction)
                .or(tiles.first())
                .copied()
        });
        let Some((next_dir, next_pos)) = opt_move else {
            return false;
        };
        if next_pos == user_position {
            if next_dir == self.direction {
                return false;
            }
            self.direction = next_dir;
            on_move(self.position, self.direction);
            return false;
        }

        self.direction = next_dir;
        self.position = next_pos;
        on_move(self.position, self.direction);
        true
    }

    /// Finds the direction for the beginning of the best path to a target given a set of move abilities.
    /// Uses the given `MapMoveRuleProvider` to determine which moves are allowed.
    /// In principle, we could construct the whole path, but we refrain from doing so because right now,
    /// it is not necessary - it would only be more efficient if the target `to` is not moving.
    ///
    /// We get a `trace` of steps that we can follow, or we could take a different path, provided that it is shorter.
    /// The expectation is that we always follow the trace unless we find a shorter path.
    ///
    /// Due to the necessicity of a best path to be strictly better than the `trace` we can explore `n - 1` steps
    /// where `n` is the length of the `trace`. If we find `from` within that number of steps, we use that path,
    /// otherwise, we use the trace.
    pub fn find_best_path(
        &mut self,
        target: Position,
        max_steps: u8,
    ) -> Option<(Direction, Position)>
    where
        M: MapMoveRuleProvider,
    {
        let mut paths = pathfinder::find_paths_with_max_steps(
            &self.move_rule_provider,
            self.partner_pokemon_config.move_ability,
            self.position,
            target,
            max_steps,
        );
        paths.sort_by_key(|p| self.user_trace.path_matches_trace(p));

        let best_path = paths.last()?;
        best_path.first().copied()
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use configuration::PokemonConfig;
    use tokio::{sync::mpsc, time::timeout};

    use crate::{data::PokemonNumber, smeargle::test_helper::TestMapMoveRuleProvider};

    use super::*;

    #[tokio::test]
    async fn spawns_in_a_valid_spot() {
        let mut setup = setup((3, 5), &[(3, 5), (3, 6)], 10);

        setup.expect_partner_move(3, 6, Direction::U).await;
    }

    #[tokio::test]
    #[tracing_test::traced_test]
    async fn go_in_circle() {
        let mut setup = setup((1, 0), &[(0, 0), (1, 0), (0, 1), (1, 1)], 10);
        setup.expect_partner_move(0, 0, Direction::R).await;
        setup.send_user_move(Direction::D);
        setup.send_user_move(Direction::L);
        setup.send_user_move(Direction::U);
        setup.expect_partner_move(1, 0, Direction::R).await;
        setup.expect_partner_move(1, 0, Direction::L).await;
        setup.expect_no_partner_move().await;
    }

    #[tokio::test]
    async fn entering_the_same_position_at_the_same_time_as_user() {
        let mut setup = setup(
            (1, 0),
            &[(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0)],
            10,
        );
        setup.expect_partner_move(0, 0, Direction::R).await;
        setup.send_user_move(Direction::R);
        setup.send_user_move(Direction::R);
        setup.send_user_move(Direction::L);
        setup.expect_partner_move(1, 0, Direction::R).await;
        setup.expect_no_partner_move().await;
    }

    #[tokio::test]
    #[tracing_test::traced_test]
    async fn moving_in_different_timings() {
        let mut setup = setup(
            (1, 0),
            &[(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0)],
            10,
        );
        setup.expect_partner_move(0, 0, Direction::R).await;
        setup.send_user_move(Direction::R);
        setup.send_user_move(Direction::R);
        setup.expect_partner_move(1, 0, Direction::R).await;
        setup.send_user_move(Direction::R);
        setup.expect_partner_move(2, 0, Direction::R).await;
        setup.expect_partner_move(3, 0, Direction::R).await;

        setup.expect_no_partner_move().await;
        setup.send_user_move(Direction::R);
        setup.expect_partner_move(4, 0, Direction::R).await;
    }

    #[tokio::test]
    #[tracing_test::traced_test]
    async fn automatically_moves_towards_player() {
        let mut setup = setup((3, 6), &[(3, 5), (3, 6), (3, 7), (3, 8), (3, 9)], 10);

        setup.expect_partner_move(3, 5, Direction::D).await;

        setup.send_user_move(Direction::D);
        setup.send_user_move(Direction::D);
        setup.send_user_move(Direction::D);

        setup.expect_partner_move(3, 6, Direction::D).await;
        setup.expect_partner_move(3, 7, Direction::D).await;
        setup.expect_partner_move(3, 8, Direction::D).await;
        setup.expect_no_partner_move().await;
    }

    #[tokio::test]
    #[tracing_test::traced_test]
    async fn considers_player_movement_midway() {
        let mut setup = setup(
            (-1, -1),
            &[
                (-1, -1), // These coordinates make this shape:
                (-1, 0),
                (-1, 1), // xxx
                (0, -1), // x x
                (0, 1),  // xxx
                (1, -1),
                (1, 0),  // The idea is that the pokemon goes around one way, the player moves,
                (1, -1), // and the pokemon changes direction as well.
            ],
            10,
        );

        setup.expect_partner_move(0, -1, Direction::L).await;

        setup.send_user_move(Direction::D);
        setup.send_user_move(Direction::D);
        setup.send_user_move(Direction::R);
        setup.send_user_move(Direction::R);

        setup.expect_partner_move(-1, -1, Direction::L).await;

        setup.send_user_move(Direction::U);

        setup.expect_partner_move(0, -1, Direction::R).await;
    }

    #[tokio::test]
    #[tracing_test::traced_test]
    async fn walk_into_partner() {
        let coords: Vec<_> = (0..=4).flat_map(|x| (0..=4).map(move |y| (x, y))).collect();
        let mut setup = setup((2, 2), &coords, 10);

        setup.expect_partner_move(1, 2, Direction::R).await;
        setup.send_user_move(Direction::L);
        setup.expect_partner_move(2, 2, Direction::R).await;
        setup.expect_partner_move(2, 2, Direction::L).await;
    }

    #[tokio::test]
    async fn turns_towards_partner() {
        let mut setup = setup((3, 6), &[(3, 5), (3, 6), (4, 6)], 10);

        setup.expect_partner_move(4, 6, Direction::L).await;
        setup.send_user_move(Direction::U);
        setup.expect_partner_move(3, 6, Direction::L).await;
        setup.expect_partner_move(3, 6, Direction::U).await;
    }

    #[tokio::test]
    async fn uses_user_positions_if_other_paths_of_same_length_are_available() {
        let coords: Vec<_> = (0..=4).flat_map(|x| (0..=4).map(move |y| (x, y))).collect();
        let mut setup = setup((0, 0), &coords, 10);

        setup.expect_partner_move(1, 0, Direction::L).await;
        for _ in 0..4 {
            setup.send_user_move(Direction::D);
            setup.send_user_move(Direction::R);
        }

        setup.expect_partner_move(0, 0, Direction::L).await;
        setup.expect_partner_move(0, 1, Direction::D).await;
        setup.expect_partner_move(1, 1, Direction::R).await;
        setup.expect_partner_move(1, 2, Direction::D).await;
        setup.expect_partner_move(2, 2, Direction::R).await;
        setup.expect_partner_move(2, 3, Direction::D).await;
        setup.expect_partner_move(3, 3, Direction::R).await;
        setup.expect_partner_move(3, 4, Direction::D).await;
    }

    #[tokio::test]
    #[tracing_test::traced_test]
    async fn uses_the_trace_again_after_trace_departure_when_intersecting_with_it() {
        let coords: Vec<_> = (0..=4).flat_map(|x| (0..=4).map(move |y| (x, y))).collect();
        let mut setup = setup((0, 0), &coords, 10);

        setup.expect_partner_move(1, 0, Direction::L).await;
        for _ in 0..4 {
            setup.send_user_move(Direction::D);
        }
        setup.send_user_move(Direction::R);
        setup.expect_partner_move(0, 0, Direction::L).await;

        for _ in 0..4 {
            setup.send_user_move(Direction::U);
        }
        setup.send_user_move(Direction::R);
        // the partner leaves the trace
        setup.expect_partner_move(1, 0, Direction::R).await;

        setup.send_user_move(Direction::D);
        setup.send_user_move(Direction::R);
        setup.send_user_move(Direction::D);
        setup.send_user_move(Direction::R);
        setup.expect_partner_move(2, 0, Direction::R).await;
        setup.expect_partner_move(2, 1, Direction::D).await;
        setup.expect_partner_move(3, 1, Direction::R).await;
        setup.expect_partner_move(3, 2, Direction::D).await;
        setup.expect_partner_move(3, 2, Direction::R).await;
        setup.expect_no_partner_move().await;
    }

    #[tokio::test]
    async fn trace_does_not_grow_infinitely() {
        let mut setup = setup((0, 0), &[(0, 0), (1, 0)], 10);
        setup.expect_partner_move(1, 0, Direction::L).await;
        for _ in 0..100 {
            setup.send_user_move(Direction::R);
            setup.expect_partner_move(0, 0, Direction::L).await;
            setup.expect_partner_move(0, 0, Direction::R).await;
            setup.send_user_move(Direction::L);
            setup.expect_partner_move(1, 0, Direction::R).await;
            setup.expect_partner_move(1, 0, Direction::L).await;
        }
    }

    #[tokio::test]
    async fn stops_when_signaled_to_stop() {
        let mut setup = setup((0, 0), &[(0, 0), (1, 0)], 10);
        setup.expect_partner_move(1, 0, Direction::L).await;
        setup
            .shutdown_ctrl
            .shutdown((), Duration::from_millis(100))
            .await;
        setup.send_user_move(Direction::R);
        setup.expect_no_partner_move().await;
    }

    #[tokio::test]
    async fn teleports_near_user_when_trace_length_is_exceeded() {
        let coords: Vec<_> = (0..=20).map(|x| (x, 0)).collect();
        let mut setup = setup((1, 0), &coords, 10);
        for _ in 0..11 {
            setup.send_user_move(Direction::R);
        }
        setup.expect_partner_move(0, 0, Direction::R).await;
        setup.expect_partner_move(1, 0, Direction::R).await;
        setup.expect_partner_move(11, 0, Direction::L).await;
    }

    struct Setup {
        sender: broadcast::Sender<(Direction, Position)>,
        _receiver: broadcast::Receiver<(Direction, Position)>,
        partner_receiver: mpsc::Receiver<(Position, Direction)>,
        shutdown_ctrl: shutdown::Ctrl,
        user_position: Position,
    }

    impl Setup {
        fn send_user_move(&mut self, direction: Direction) {
            self.user_position = self.user_position.move_in_dir(direction);
            self.sender.send((direction, self.user_position)).unwrap();
        }

        async fn expect_partner_move(&mut self, x: i32, y: i32, direction: Direction) {
            let (pos, dir) = timeout(Duration::from_secs(1), self.partner_receiver.recv())
                .await
                .expect("no timeout")
                .expect("a move");
            assert_eq!(
                (pos, dir),
                (
                    Position {
                        x,
                        y,
                        layer_number: 0
                    },
                    direction
                ),
            );
        }

        async fn expect_no_partner_move(&mut self) {
            let move_or_timeout =
                timeout(Duration::from_secs(1), self.partner_receiver.recv()).await;
            assert!(
                move_or_timeout.is_err() || move_or_timeout.as_ref().is_ok_and(|opt| opt.is_none()),
                "Expected no move but received {:?}",
                move_or_timeout.unwrap()
            );
        }
    }

    fn setup(
        initial_user_position: (i32, i32),
        allowed_coords: &[(i32, i32)],
        trace_length: u8,
    ) -> Setup {
        let move_rule_provider = move_rule_provider_for_coords(allowed_coords);

        let initial_user_position = Position {
            x: initial_user_position.0,
            y: initial_user_position.1,
            layer_number: 0,
        };
        let user_id = UserId::from(1i64);
        let mut pokemon_config = PokemonConfig::read().unwrap();
        pokemon_config.move_duration_base_in_ms = 100;
        let partner_pokemon_config = pokemon_config.get(PokemonNumber(1)).unwrap().clone();
        let (sender, receiver) = broadcast::channel(16);
        let (partner_sender, partner_receiver) = mpsc::channel(4);
        let on_move =
            move |position, direction| partner_sender.try_send((position, direction)).unwrap();

        let worker = PartnerPokemonWorker::new(PartnerPokemonWorkerArgs {
            move_rule_provider,
            receiver: sender.subscribe(),
            user_id,
            partner_pokemon_config,
            pokemon_config,
            initial_user_position,
            trace_length,
        });

        let shutdown_ctrl = shutdown::Ctrl::new();
        tokio::spawn(worker.work(shutdown_ctrl.clone(), on_move));

        Setup {
            sender,
            _receiver: receiver,
            partner_receiver,
            shutdown_ctrl,
            user_position: initial_user_position,
        }
    }

    fn move_rule_provider_for_coords(coords: &[(i32, i32)]) -> impl MapMoveRuleProvider + use<> {
        coords
            .iter()
            .map(|(x, y)| {
                (
                    Position {
                        x: *x,
                        y: *y,
                        layer_number: 0,
                    },
                    MoveAbilities::all(),
                )
            })
            .collect::<TestMapMoveRuleProvider>()
    }
}
