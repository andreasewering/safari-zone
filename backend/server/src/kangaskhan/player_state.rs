use std::{collections::HashSet, sync::Arc};

use dashmap::DashMap;
use futures::StreamExt;
use sqlx::PgPool;

use crate::{
    channels::{PlayerChannel, PlayerMessage, PlayerMessageWrapper},
    data::{Direction, MapId, PokemonNumber, Position, TrainerNumber, UserId},
    graceful_shutdown, shutdown,
};

#[derive(Clone)]
pub struct PlayerState {
    pool: PgPool,
    state: Arc<DashMap<UserId, PlayerWithMetadata>>,
    partner_state: Arc<DashMap<UserId, PartnerPokemonPosition>>,
    map_state: Arc<DashMap<MapId, HashSet<UserId>>>,
    channel: PlayerChannel,
    shutdown_ctrl: shutdown::Ctrl,
}

struct PlayerWithMetadata {
    player: Player,
    partner: PartnerPokemon,
    changes_persisted: bool,
}

#[derive(Clone)]
pub struct Player {
    pub user_name: String,
    pub map_id: MapId,
    pub position: Position,
    pub direction: Direction,
    pub trainer_number: TrainerNumber,
}

#[derive(Clone, Copy)]
pub struct PartnerPokemonPosition {
    pub position: Position,
    pub direction: Direction,
}

#[derive(Clone, Copy)]
pub struct PartnerPokemon {
    pub pokemon_number: PokemonNumber,
    pub is_shiny: bool,
}

impl PlayerState {
    pub async fn new(
        pool: &PgPool,
        channel: &PlayerChannel,
        shutdown_controller: shutdown::Ctrl,
    ) -> Self {
        // When this function executes, the server is starting up. That means every player is offline
        // and there is no reason to load anything eagerly from the database.
        let state = Arc::new(DashMap::<UserId, PlayerWithMetadata>::new());
        let partner_state = Arc::new(DashMap::<UserId, PartnerPokemonPosition>::new());
        let map_state = Arc::new(DashMap::<MapId, HashSet<UserId>>::new());

        let mut player_state = Self {
            state,
            partner_state,
            map_state,
            pool: pool.clone(),
            channel: channel.clone(),
            shutdown_ctrl: shutdown_controller,
        };

        player_state.connect_to_channel();
        player_state.spawn_bulk_persistence_task();

        player_state
    }

    pub fn get_players_on_map(&self, map_id: MapId) -> Vec<PlayerMessageWrapper> {
        let Some(user_ids) = self.map_state.get(&map_id) else {
            return Vec::new();
        };
        let mut messages = Vec::new();

        for user_id in user_ids.iter() {
            if let Some(player) = self.state.get(user_id) {
                messages.push(PlayerMessageWrapper {
                    map_id: Some(map_id),
                    user_id: *user_id,
                    message: PlayerMessage::EnteredMap {
                        user_name: player.player.user_name.clone(),
                        position: player.player.position,
                        direction: player.player.direction,
                        trainer_number: player.player.trainer_number,
                        partner_pokemon_number: player.partner.pokemon_number,
                        partner_pokemon_is_shiny: player.partner.is_shiny,
                    },
                });
            }

            if let Some(pokemon) = self.partner_state.get(user_id) {
                messages.push(PlayerMessageWrapper {
                    map_id: Some(map_id),
                    user_id: *user_id,
                    message: PlayerMessage::PartnerPokemonMoved {
                        position: pokemon.position,
                        direction: pokemon.direction,
                    },
                })
            }
        }

        messages
    }

    fn connect_to_channel(&mut self) {
        let mut messages = self.channel.receive();
        let clone = self.clone();
        let main_loop = async move {
            while let Some(message) = messages.next().await {
                if let Err(error) = clone.handle_message(message).await {
                    tracing::error!(
                        message = "Failed to handle player message in player state",
                        %error
                    );
                }
            }
        };
        let shutdown_ctrl = self.shutdown_ctrl.clone();
        tokio::spawn(async move {
            graceful_shutdown! {
                ctrl: shutdown_ctrl,
                name: "player_state::connect_to_channel",
                cleanup: () => {
                    tracing::info!("Gracefully shutdown player state subscription to player channel.");
                },
                _ = main_loop => {
                    tracing::error!("Subscription to channel ended early.");
                }
            }
        });
    }

    fn spawn_bulk_persistence_task(&self) {
        let clone = self.clone();
        let main_loop = async move {
            let mut interval = tokio::time::interval(std::time::Duration::from_secs(300));
            loop {
                interval.tick().await;
                clone.bulk_persist().await;
            }
        };
        let clone = self.clone();
        tokio::spawn(async move {
            let shutdown_ctrl = clone.shutdown_ctrl.clone();
            graceful_shutdown! {
              ctrl: shutdown_ctrl,
              name: "player_state::spawn_bulk_persistence_task",
              cleanup: () => {
                  tracing::info!("Shutting down player state, persisting unsaved changes");
                  clone.bulk_persist().await;
                  tracing::info!("Gracefully shutdown player state");
              },
              _ = main_loop => {
                  tracing::error!("Bulk persistence task finished early.");
              }
            }
        });
    }

    async fn bulk_persist(&self) {
        let non_persisted = self.state.iter_mut().filter(|e| !e.changes_persisted);

        let mut user_ids = Vec::new();
        let mut xs = Vec::new();
        let mut ys = Vec::new();
        let mut layer_numbers = Vec::new();
        let mut directions = Vec::new();
        let mut map_ids = Vec::new();
        for mut player_with_metadata in non_persisted {
            player_with_metadata.value_mut().changes_persisted = true;
            let player = &player_with_metadata.player;
            user_ids.push(*player_with_metadata.key());
            xs.push(player.position.x);
            ys.push(player.position.y);
            layer_numbers.push(player.position.layer_number);
            directions.push(player.direction);
            map_ids.push(player.map_id);
        }
        let bulk_update_result = sqlx::query!(
            "UPDATE user_position SET
                    x = bulk_query.x,
                    y = bulk_query.y,
                    layer_number = bulk_query.layer_number,
                    direction = bulk_query.direction,
                    map_id = bulk_query.map_id
                FROM (SELECT * FROM UNNEST(
                    $1::BIGINT[], 
                    $2::INT[], 
                    $3::INT[], 
                    $4::INT[], 
                    $5::DIRECTION[],
                    $6::BIGINT[]) 
                  AS t(id, x, y, layer_number, direction, map_id)) AS bulk_query
                WHERE user_position.user_id = bulk_query.id",
            user_ids as Vec<UserId>,
            &xs,
            &ys,
            &layer_numbers,
            directions as Vec<Direction>,
            map_ids as Vec<MapId>
        )
        .execute(&self.pool)
        .await;
        if let Err(error) = bulk_update_result {
            tracing::warn!(message = "Failed to persist player updates to the database", %error);
        }
    }

    async fn handle_message(
        &self,
        PlayerMessageWrapper {
            map_id,
            user_id,
            message,
        }: PlayerMessageWrapper,
    ) -> Result<(), sqlx::Error> {
        let Some(map_id) = map_id else {
            return Ok(());
        };
        match message {
            PlayerMessage::EnteredMap {
                position,
                direction,
                user_name,
                trainer_number,
                partner_pokemon_is_shiny,
                partner_pokemon_number,
            } => self.persist_player(
                user_id,
                Player {
                    user_name,
                    position,
                    direction,
                    trainer_number,
                    map_id,
                },
                PartnerPokemon {
                    pokemon_number: partner_pokemon_number,
                    is_shiny: partner_pokemon_is_shiny,
                },
            ),
            PlayerMessage::PlayerMoved {
                position,
                direction,
            } => {
                self.modify_player(user_id, |player| {
                    player.position = position;
                    player.direction = direction;
                });
            }
            PlayerMessage::PartnerPokemonMoved {
                position,
                direction,
            } => {
                self.persist_partner(
                    user_id,
                    PartnerPokemonPosition {
                        position,
                        direction,
                    },
                );
            }
            PlayerMessage::WentOffline => {
                self.remove_player(user_id, map_id);
            }
            PlayerMessage::ChangedPartnerPokemon {
                pokemon_number,
                is_shiny,
            } => {
                self.modify_partner(user_id, |partner| {
                    partner.pokemon_number = pokemon_number;
                    partner.is_shiny = is_shiny;
                });
            }
            PlayerMessage::ChangedTrainerSprite { trainer_number } => {
                self.modify_player(user_id, |player| {
                    player.trainer_number = trainer_number;
                });
            }
            PlayerMessage::ThrewBall { .. } => {}
        }
        Ok(())
    }

    fn persist_player(&self, user_id: UserId, player: Player, partner: PartnerPokemon) {
        let map_id = player.map_id;
        let player_with_metadata = PlayerWithMetadata {
            player,
            partner,
            changes_persisted: true,
        };
        self.state.insert(user_id, player_with_metadata);
        let mut map_entry = self.map_state.entry(map_id).or_default();
        map_entry.insert(user_id);
    }

    fn persist_partner(&self, user_id: UserId, partner: PartnerPokemonPosition) {
        self.partner_state.insert(user_id, partner);
    }

    fn modify_player<F>(&self, user_id: UserId, mut modify: F)
    where
        F: FnMut(&mut Player),
    {
        self.state
            .entry(user_id)
            .and_modify(|player_with_metadata| {
                modify(&mut player_with_metadata.player);
                player_with_metadata.changes_persisted = false;
            });
    }

    fn modify_partner<F>(&self, user_id: UserId, mut modify: F)
    where
        F: FnMut(&mut PartnerPokemon),
    {
        self.state
            .entry(user_id)
            .and_modify(|player_with_metadata| {
                modify(&mut player_with_metadata.partner);
                player_with_metadata.changes_persisted = false;
            });
    }

    fn remove_player(&self, user_id: UserId, map_id: MapId) {
        let dashmap::Entry::Occupied(user_entry) = self.state.entry(user_id) else {
            return;
        };
        let dashmap::Entry::Occupied(mut map_entry) = self.map_state.entry(map_id) else {
            return;
        };
        // Make sure this does not break players connecting to the new map before disconnecting from the old map
        if user_entry.get().player.map_id == map_id {
            user_entry.remove();
        }

        let player_ids = map_entry.get_mut();
        player_ids.remove(&user_id);
        if player_ids.is_empty() {
            map_entry.remove();
        }
    }
}
