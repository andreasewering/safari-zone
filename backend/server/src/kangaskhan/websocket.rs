use std::{fmt::Display, sync::Arc};

use crate::{
    arceus::{self, SignalSender},
    channels::{
        ItemChannel, ItemMessage, ItemMessageWrapper, MapChannel, MapMessage, NewEncounter,
    },
    chat_message::{self, ChatMessage, proto_from_tag},
    configuration::PokemonConfig,
    data::{
        Item, ItemId, MoveAbilities, MoveAbility, PokemonNumber, Throw, TrainerNumber, UserId,
        proto,
    },
    error::AppResult,
    gardevoir::Claims,
    kangaskhan::{
        proto::kangaskhan::websocket_message_to_client::EnteredWarp,
        throw::{self, ThrowItemError, calc_throw_target_and_time},
    },
    rng, shutdown,
    util::{encode_to_bytes, time::offset_date_time_into_timestamp},
};
use axum::extract::ws::{self, CloseFrame, Utf8Bytes, close_code};
use futures::{Sink, SinkExt, Stream, StreamExt, stream};
use prost::Message;
use sqlx::PgPool;
use std::time::Duration;
use time::OffsetDateTime;
use tokio::sync::{broadcast, mpsc};
use tokio::time::Instant;
use tracing::Instrument as _;

use crate::{
    channels::{
        PlayerChannel, PlayerMessage, PlayerMessageWrapper, PokemonChannel, PokemonMessage,
        PokemonMessageWrapper,
    },
    data::{Direction, MapId, Position},
    gardevoir::AuthCtx,
    kangaskhan::proto::kangaskhan::{WebsocketMessageToServer, websocket_message_to_client},
};

use super::{
    Settings, TileDataProvider, catch,
    encounter_state::EncounterState,
    game_state::GameStateSnapshot,
    item_state::ItemState,
    partner_pokemon::{self, PartnerPokemonWorkerArgs},
    player_state::PlayerState,
    proto::kangaskhan::{
        CollectedItem, WebsocketMessageToClient,
        websocket_message_to_client::{GotChatMessage, ItemSpawned, TargetPokemon},
        websocket_message_to_server,
    },
    warp_cache::WarpCache,
};

fn player_message_to_ws(
    msg: PlayerMessageWrapper,
    current_map_id: MapId,
    own_user_id: UserId,
) -> Option<WebsocketMessageToClient> {
    let user_id = i64::from(msg.user_id);
    let map_id = msg.map_id?;
    if current_map_id != map_id {
        return None;
    }
    let ws_msg = match msg.message {
        PlayerMessage::PlayerMoved {
            position,
            direction,
        } => {
            if msg.user_id == own_user_id {
                return None;
            }
            websocket_message_to_client::Message::PlayerMoved(
                websocket_message_to_client::PlayerMoved {
                    coord_x: position.x,
                    coord_y: position.y,
                    layer_number: position.layer_number,
                    direction: direction.into(),
                    user_id,
                },
            )
        }

        PlayerMessage::WentOffline => {
            if msg.user_id == own_user_id {
                return None;
            }
            websocket_message_to_client::Message::PlayerWentOffline(user_id)
        }

        PlayerMessage::EnteredMap {
            position,
            user_name,
            trainer_number,
            partner_pokemon_number,
            partner_pokemon_is_shiny,
            direction,
        } => {
            if msg.user_id == own_user_id {
                return None;
            }
            websocket_message_to_client::Message::PlayerEnteredMap(
                websocket_message_to_client::PlayerEnteredMap {
                    coord_x: position.x,
                    coord_y: position.y,
                    layer_number: position.layer_number,
                    user_name,
                    trainer_number: trainer_number.into(),
                    partner_pokemon_number: u32::from(partner_pokemon_number),
                    partner_is_shiny: partner_pokemon_is_shiny,
                    direction: direction.into(),
                    user_id,
                },
            )
        }

        PlayerMessage::ChangedPartnerPokemon {
            pokemon_number,
            is_shiny,
        } => {
            if msg.user_id == own_user_id {
                return None;
            }
            websocket_message_to_client::Message::PlayerChangedPartnerPokemon(
                websocket_message_to_client::PlayerChangedPartnerPokemon {
                    pokemon_number: pokemon_number.into(),
                    is_shiny,
                    user_id,
                },
            )
        }

        PlayerMessage::ChangedTrainerSprite { trainer_number } => {
            if msg.user_id == own_user_id {
                return None;
            }
            websocket_message_to_client::Message::PlayerChangedTrainerSprite(
                websocket_message_to_client::PlayerChangedTrainerSprite {
                    trainer_number: trainer_number.into(),
                    user_id,
                },
            )
        }

        PlayerMessage::ThrewBall {
            throw,
            from_x,
            from_y,
            item,
            ball_id,
        } => {
            if msg.user_id == own_user_id {
                return None;
            }
            websocket_message_to_client::Message::PlayerThrewBall(
                websocket_message_to_client::PlayerThrewBall {
                    vector_x: throw.vector_x(),
                    vector_y: throw.vector_y(),
                    from_x,
                    from_y,
                    angle_radians: throw.angle_radians(),
                    item: item.into(),
                    ball_id: ball_id.into(),
                    user_id,
                },
            )
        }
        PlayerMessage::PartnerPokemonMoved {
            position,
            direction,
        } => websocket_message_to_client::Message::MovePartnerPokemon(
            websocket_message_to_client::MovePartnerPokemon {
                coord_x: position.x,
                coord_y: position.y,
                layer_number: position.layer_number,
                direction: direction.into(),
                user_id,
            },
        ),
    };
    Some(WebsocketMessageToClient {
        message: Some(ws_msg),
    })
}

fn pokemon_message_to_ws(
    msg: PokemonMessageWrapper,
    current_map_id: MapId,
) -> Option<WebsocketMessageToClient> {
    let map_id = msg.map_id;
    let encounter_id = i64::from(msg.encounter_id);
    if current_map_id != map_id {
        return None;
    }
    let ws_msg = match msg.message {
        PokemonMessage::New(NewEncounter {
            position,
            pokemon_number,
            is_shiny,
            direction,
            ..
        }) => websocket_message_to_client::Message::NewEncounter(
            websocket_message_to_client::NewEncounter {
                coord_x: position.x,
                coord_y: position.y,
                layer_number: position.layer_number,
                pokemon_number: pokemon_number.into(),
                is_shiny,
                direction: direction.into(),
                encounter_id,
            },
        ),

        PokemonMessage::Move {
            position,
            direction,
        } => websocket_message_to_client::Message::MoveEncounter(
            websocket_message_to_client::MoveEncounter {
                coord_x: position.x,
                coord_y: position.y,
                layer_number: position.layer_number,
                direction: direction.into(),
                encounter_id,
            },
        ),

        PokemonMessage::Remove => {
            websocket_message_to_client::Message::RemoveEncounter(encounter_id)
        }
        PokemonMessage::Caught { by } => {
            websocket_message_to_client::Message::PokemonCaught(TargetPokemon {
                encounter_id,
                item_id: by.into(),
            })
        }
        PokemonMessage::BrokeOut { from } => {
            websocket_message_to_client::Message::PokemonBrokeOut(TargetPokemon {
                encounter_id,
                item_id: from.into(),
            })
        }
    };
    Some(WebsocketMessageToClient {
        message: Some(ws_msg),
    })
}

fn item_message_to_ws(
    msg: ItemMessageWrapper,
    current_map_id: MapId,
) -> Option<WebsocketMessageToClient> {
    let map_id = msg.map_id;
    let item_id = i64::from(msg.item_id);
    if current_map_id != map_id {
        return None;
    }
    let ws_msg = match msg.msg {
        crate::channels::ItemMessage::New {
            item,
            x,
            y,
            layer_number,
            despawn_time: _,
        } => websocket_message_to_client::Message::ItemSpawned(ItemSpawned {
            item: item.into(),
            x,
            y,
            layer_number,
            amount: 1,
            item_id,
        }),
        crate::channels::ItemMessage::Remove => {
            websocket_message_to_client::Message::ItemDespawned(item_id)
        }
    };
    Some(WebsocketMessageToClient {
        message: Some(ws_msg),
    })
}

fn map_message_to_ws(msg: MapMessage, current_map_id: MapId) -> Option<WebsocketMessageToClient> {
    let map_id = msg.map_id;
    if current_map_id != map_id {
        return None;
    }

    let text = msg.text;
    let from_user = msg.from_user.into();
    let sent_at = Some(offset_date_time_into_timestamp(msg.sent_at));
    let tags = msg.tags.into_iter().map(proto_from_tag).collect();
    let ws_msg = websocket_message_to_client::Message::GotChat(GotChatMessage {
        text,
        tags,
        sent_at,
        from_user,
    });

    Some(WebsocketMessageToClient {
        message: Some(ws_msg),
    })
}

#[derive(Clone)]
pub struct WebsocketCtx {
    pub player_channel: PlayerChannel,
    pub pokemon_channel: PokemonChannel,
    pub map_channel: MapChannel,
    pub item_channel: ItemChannel,
    pub db_connection_pool: PgPool,
    pub settings: Settings,
    pub auth_ctx: AuthCtx,
    pub tile_cache: Arc<dyn TileDataProvider>,
    pub warp_cache: WarpCache,
    pub arceus_signal_sender: arceus::SignalSender<UserId>,
    pub encounter_state: EncounterState,
    pub item_state: ItemState,
    pub player_state: PlayerState,
    pub shutdown_ctrl: shutdown::Ctrl,
    pub pokemon_config: PokemonConfig,
    pub rng: rng::RngProvider,
}

pub async fn websocket<S, R>(sender: S, receiver: R, claims: Claims, state: WebsocketCtx)
where
    S: Sink<ws::Message> + Unpin,
    S::Error: Display,
    R: Stream<Item = Result<ws::Message, axum::Error>> + Unpin,
{
    let mut sender = KangaskhanSink { inner: sender };
    let receiver = map_stream(receiver);

    let own_user_id = claims.user_id();
    let user = get_user(own_user_id, &state.db_connection_pool).await;

    let user = match user {
        Err(error) => {
            tracing::error!(message = "Failed to get user", %error, %own_user_id);
            return sender.send_internal_error().await;
        }
        Ok(None) => {
            tracing::error!(message = "User tried to connect but was not fully created yet", %own_user_id);
            return sender.send_not_fully_created_error().await;
        }
        Ok(Some(user)) => user,
    };

    // User is validated! Now some initial setup
    tracing::info!(
        message = "User connected to websocket",
        user_id = %own_user_id,
    );
    let ws_span = tracing::info_span!("ws", user_id = %own_user_id, map_id = %user.map_id);

    websocket_internal(sender, receiver, claims, user, state)
        .instrument(ws_span)
        .await;
}

struct KangaskhanSink<S> {
    inner: S,
}

impl<S: Sink<ws::Message> + Unpin> KangaskhanSink<S>
where
    S::Error: Display,
{
    async fn send_message(&mut self, msg: WebsocketMessageToClient) {
        if let Err(error) = self
            .inner
            .send(ws::Message::Binary(encode_to_bytes(msg)))
            .await
        {
            tracing::warn!(%error, "Failed to send message");
        }
    }

    async fn send_messages<I>(&mut self, msgs: I)
    where
        I: Iterator<Item = WebsocketMessageToClient>,
    {
        if let Err(error) = self
            .inner
            .send_all(&mut stream::iter(
                msgs.map(|msg| Ok(ws::Message::Binary(encode_to_bytes(msg)))),
            ))
            .await
        {
            tracing::warn!(%error, "Failed to send messages");
        }
    }

    async fn send_close(&mut self, close_frame: CloseFrame) {
        on_close(&mut self.inner, close_frame).await;
    }

    async fn send_not_fully_created_error(&mut self) {
        on_close(
            &mut self.inner,
            ws::CloseFrame {
                code: ws::close_code::UNSUPPORTED,
                reason: Utf8Bytes::from_static("User not fully created"),
            },
        )
        .await;
    }

    async fn send_internal_error(&mut self) {
        on_close(
            &mut self.inner,
            ws::CloseFrame {
                code: ws::close_code::ERROR,
                reason: Utf8Bytes::from_static("Internal"),
            },
        )
        .await;
    }

    async fn ping(&mut self) -> Result<(), S::Error> {
        self.inner.send(ws::Message::Ping(Default::default())).await
    }
}

fn map_stream<S: Stream<Item = Result<ws::Message, axum::Error>> + Unpin>(
    s: S,
) -> impl Stream<Item = websocket_message_to_server::Message> + Unpin {
    use futures::StreamExt as _;
    s.filter_map(|msg| {
        Box::pin(async move {
            let binary = match msg {
                Err(error) => {
                    tracing::warn!(%error, "Websocket error");
                    return None;
                }
                Ok(ws::Message::Binary(binary)) => binary,
                Ok(other) => {
                    tracing::debug!(message=?other, "Received non-binary message");
                    return None;
                }
            };
            match WebsocketMessageToServer::decode(binary) {
                Err(error) => {
                    tracing::warn!(%error, "Failed to decode binary message");
                    None
                }
                Ok(msg) => msg.message,
            }
        })
    })
}

async fn websocket_internal<S, R>(
    mut sender: KangaskhanSink<S>,
    mut receiver: R,
    claims: Claims,
    user: User,
    state: WebsocketCtx,
) where
    S: Sink<ws::Message> + Unpin,
    S::Error: Display,
    R: Stream<Item = websocket_message_to_server::Message> + Unpin,
{
    let own_user_id = claims.user_id();
    let signal_sender = state.arceus_signal_sender.clone();
    tokio::spawn(async move { signal_sender.start_map(user.map_id, own_user_id).await });

    let initial_user_messages = state
        .player_state
        .get_players_on_map(user.map_id)
        .into_iter()
        .filter_map(|msg| player_message_to_ws(msg, user.map_id, own_user_id));

    let initial_pokemon_messages = state
        .encounter_state
        .get_encounters_on_map(user.map_id)
        .into_iter()
        .filter_map(|msg| pokemon_message_to_ws(msg, user.map_id));

    let initial_item_messages = state
        .item_state
        .get_items_on_map(user.map_id)
        .into_iter()
        .filter_map(|msg| item_message_to_ws(msg, user.map_id));

    let initial_messages = initial_user_messages
        .chain(initial_pokemon_messages)
        .chain(initial_item_messages);
    sender.send_messages(initial_messages).await;

    // Main event loop
    let mut ping_interval = tokio::time::interval(Duration::from_secs(30));
    let mut sync_game_state_interval = tokio::time::interval(Duration::from_secs(5));
    let mut player_message_stream = state.player_channel.receive();
    let mut pokemon_message_stream = state.pokemon_channel.receive();
    let mut item_message_stream = state.item_channel.receive();
    let mut map_message_stream = state.map_channel.receive();
    let (game_loop_sender, mut game_loop_receiver) = mpsc::channel::<GameLoopChannelMessage>(10);

    let position = Position {
        x: user.x,
        y: user.y,
        layer_number: user.layer_number,
    };
    let game_state = GameStateSnapshot {
        position,
        map_id: user.map_id,
        direction: user.direction,
    };

    state.player_channel.send(PlayerMessageWrapper {
        user_id: own_user_id,
        map_id: Some(game_state.map_id),
        message: PlayerMessage::EnteredMap {
            position,
            direction: game_state.direction,
            user_name: user.display_name.clone(),
            trainer_number: user.trainer_number,
            partner_pokemon_number: user.partner_pokemon_number,
            partner_pokemon_is_shiny: user.partner_pokemon_is_shiny,
        },
    });

    let (own_position_sender, own_position_receiver) = broadcast::channel(16);
    let partner_pokemon_shutdown_ctrl = shutdown::Ctrl::new();

    if let Some(partner_pokemon) = state
        .pokemon_config
        .get(user.partner_pokemon_number)
        .cloned()
    {
        if let Ok(move_rule_provider) = state.tile_cache.move_rules_for_map_id(user.map_id).await.inspect_err(|error| tracing::error!(%error, "Failed to start partner pokemon because move rules could not be loaded")) {
            let player_channel = state.player_channel.clone();
            let on_move = move |partner_pos, partner_dir| {
                player_channel.send(PlayerMessageWrapper {
                    user_id: own_user_id,
                    map_id: Some(game_state.map_id),
                    message: PlayerMessage::PartnerPokemonMoved {
                        position: partner_pos,
                        direction: partner_dir,
                    },
                });
            };
            let partner_pokemon =
            partner_pokemon::PartnerPokemonWorker::new(PartnerPokemonWorkerArgs {
                user_id: own_user_id,
                receiver: own_position_receiver,
                move_rule_provider,
                partner_pokemon_config: partner_pokemon,
                pokemon_config: state.pokemon_config.clone(),
                initial_user_position: position,
                trace_length: state.settings.partner_trace_length
            });
            tokio::spawn(partner_pokemon.work(partner_pokemon_shutdown_ctrl.clone(), on_move));
        }
    }

    let mut game_loop_ctx = GameLoopCtx {
        player_channel: state.player_channel,
        pokemon_channel: state.pokemon_channel,
        map_channel: state.map_channel,
        pokemon_config: state.pokemon_config,
        settings: state.settings,
        db_connection_pool: state.db_connection_pool,
        game_loop_channel: game_loop_sender,
        game_state,
        user,
        claims,
        tile_cache: state.tile_cache,
        warp_cache: state.warp_cache,
        auth_ctx: state.auth_ctx,
        arceus_signal_sender: state.arceus_signal_sender,
        warped_to: None,
        item_state: state.item_state,
        item_channel: state.item_channel,
        encounter_state: state.encounter_state,
        own_position_sender,
        rng: state.rng.next(),
    };

    let _prevent_shutdown = state.shutdown_ctrl.prevent_shutdown(format!(
        "kangaskhan::websocket_{own_user_id}_{}",
        game_loop_ctx.user.map_id
    ));
    loop {
        tokio::select! {
            _ = state.shutdown_ctrl.cancelled() => {
                break;
            },
            _ = ping_interval.tick() => {
                if let Err(error) = sender.ping().await {
                    tracing::debug!(message = "Failed to send ping", %error);
                    break;
                }
            },
            _ = sync_game_state_interval.tick() => {

                    sync_game_state(game_loop_ctx.game_state, game_loop_ctx.claims.user_id(), &game_loop_ctx.db_connection_pool).await;
            },
            msg = player_message_stream.next() => {
                let Some(msg) = msg else {
                    tracing::error!("Player channel subscription stopped!");
                    break;
                };
                let Some(ws_msg) = player_message_to_ws(msg, game_loop_ctx.game_state.map_id, own_user_id) else {
                    continue;
                };
                sender
                    .send_message(ws_msg)
                    .await;
            },
            msg = pokemon_message_stream.next() => {
                let Some(msg) = msg else {
                    tracing::error!("Pokemon channel subscription stopped!");
                    break;
                };
                let Some(ws_msg) = pokemon_message_to_ws(msg, game_loop_ctx.game_state.map_id) else {
                    continue;
                };
                sender.send_message(ws_msg).await;
            },
            msg = item_message_stream.next() => {
                let Some(msg) = msg else {
                    tracing::error!("Item channel subscription stopped!");
                    break;
                };
                let Some(ws_msg) = item_message_to_ws(msg, game_loop_ctx.game_state.map_id) else {
                    continue;
                };
                sender
                    .send_message(ws_msg)
                    .await
            }
            msg = map_message_stream.next() => {
                let Some(msg) = msg else {
                    tracing::error!("Map channel subscription stopped!");
                    break;
                };
                let Some(ws_msg) = map_message_to_ws(msg, game_loop_ctx.game_state.map_id) else {
                    continue;
                };
                sender
                    .send_message(ws_msg)
                    .await
            }
            msg = receiver.next() => {
                let Some(msg) = msg else {
                    break;
                };
                if game_loop_ctx.locked() {
                    tracing::debug!("Ignored message since this websocket was locked");
                    continue;
                }
                game_loop(&mut game_loop_ctx, msg);
            }
            msg = game_loop_receiver.recv() => {
                let Some(msg) = msg else {
                    tracing::error!("Game loop channel subscription stopped!");
                    break;
                };
                match msg {
                    GameLoopChannelMessage::Close(close_frame) => {
                        tracing::debug!("Initiating connection close");
                        sender.send_close(close_frame).await;
                        break;
                    }
                    GameLoopChannelMessage::ReservedItemId(reserved_item_id) => {
                        let ws_msg = on_reserved_thrown_ball_id(&mut game_loop_ctx, reserved_item_id);
                        sender.send_message(ws_msg).await;
                    },
                    GameLoopChannelMessage::AttemptedMove(attempted_move) => {
                        on_attempted_move(&mut game_loop_ctx, attempted_move);
                    },
                    GameLoopChannelMessage::SwitchMap {future_map_id, future_position } => {
                        let ws_msg = on_switch_map(&mut game_loop_ctx, future_map_id, future_position);
                        sender.send_message(ws_msg).await;
                        let game_loop_channel = game_loop_ctx.game_loop_channel.clone();
                        tokio::spawn(async move {
                            tokio::time::sleep(Duration::from_secs(2)).await;
                            if let Err(error) = game_loop_channel.send(GameLoopChannelMessage::Close(CloseFrame {
                                code: ws::close_code::NORMAL,
                                reason: Utf8Bytes::from_static("Changed map"),
                            })).await {
                                // If the client closes the websocket connection himself,
                                // the channel might already be dropped and this errors
                                // but this is completely fine.
                                tracing::debug!(%error, "Failed to send close, client probably already closed the connection");
                            }
                        });
                    }
                    GameLoopChannelMessage::CollectedItems(items) => {
                        sender.send_messages(collected_items_to_ws(items)).await;
                    }
                }
            }
        }
    }

    partner_pokemon_shutdown_ctrl
        .shutdown((), Duration::from_millis(100))
        .await;
    if let Err(error) = on_disconnect(&game_loop_ctx).await {
        tracing::error!(message = "Failed to save game state to database", %error);
    }
}

struct GameLoopCtx {
    player_channel: PlayerChannel,
    pokemon_channel: PokemonChannel,
    map_channel: MapChannel,
    pokemon_config: PokemonConfig,
    game_state: GameStateSnapshot,
    user: User,
    claims: Claims,
    settings: Settings,
    db_connection_pool: PgPool,
    game_loop_channel: mpsc::Sender<GameLoopChannelMessage>,
    tile_cache: Arc<dyn TileDataProvider>,
    warp_cache: WarpCache,
    auth_ctx: AuthCtx,
    arceus_signal_sender: SignalSender<UserId>,
    item_state: ItemState,
    item_channel: ItemChannel,
    encounter_state: EncounterState,
    warped_to: Option<(MapId, Position)>,
    own_position_sender: broadcast::Sender<(Direction, Position)>,
    rng: rng::FastRng,
}

impl GameLoopCtx {
    fn locked(&self) -> bool {
        self.warped_to.is_some()
    }
}

#[derive(Debug)]
enum GameLoopChannelMessage {
    ReservedItemId(ReservedItemId),
    AttemptedMove(AttemptedMove),
    SwitchMap {
        future_map_id: MapId,
        future_position: Position,
    },
    CollectedItems(Vec<(Item, u32)>),
    Close(CloseFrame),
}

#[derive(Debug)]
struct ReservedItemId {
    item_id: ItemId,
    map_id: MapId,
    item: proto::Item,
    pos_x: i32,
    pos_y: i32,
    layer_number: i32,
    throw: Throw,
    thrown_at: Instant,
}

#[derive(Debug)]
struct AttemptedMove {
    move_result: MoveResult,
    direction: Direction,
    position: Position,
}

fn game_loop(ctx: &mut GameLoopCtx, msg: websocket_message_to_server::Message) {
    match msg {
        websocket_message_to_server::Message::PlayerMove(direction) => {
            if let Ok(direction) = proto::Direction::try_from(direction) {
                on_player_move(ctx, direction);
            }
        }
        websocket_message_to_server::Message::PlayerTurn(direction) => {
            if let Ok(direction) = proto::Direction::try_from(direction) {
                on_player_turn(ctx, direction);
            }
        }
        websocket_message_to_server::Message::ThrowBall(throw_ball) => {
            on_throw_ball(ctx, throw_ball);
        }
        websocket_message_to_server::Message::SendChat(text) => {
            on_send_chat_message(ctx, text);
        }
        websocket_message_to_server::Message::StartTyping(()) => {}
        websocket_message_to_server::Message::StopTyping(()) => {}
    }
}

fn on_player_move(ctx: &mut GameLoopCtx, direction: proto::Direction) {
    let direction = Direction::from(direction);
    ctx.game_state.direction = direction;

    let tile_cache = ctx.tile_cache.clone();
    let warp_cache = ctx.warp_cache.clone();
    let game_state = ctx.game_state;
    let game_loop_channel = ctx.game_loop_channel.clone();

    tokio::spawn(async move {
        let move_result = move_in_dir(
            tile_cache.as_ref(),
            &warp_cache,
            game_state.map_id,
            game_state.position,
            game_state.direction,
        )
        .await;
        if let Err(error) = game_loop_channel
            .send(GameLoopChannelMessage::AttemptedMove(AttemptedMove {
                move_result,
                direction,
                position: game_state.position,
            }))
            .await
        {
            tracing::debug!(message = "Dropped game loop message due to channel being closed. This is probably fine.", dropped = ?error.0);
        };
    });
}

fn on_attempted_move(
    ctx: &mut GameLoopCtx,
    AttemptedMove {
        move_result,
        direction,
        position,
    }: AttemptedMove,
) {
    let map_id = ctx.game_state.map_id;
    match move_result {
        MoveResult::Allowed { position } => {
            ctx.game_state.direction = direction;
            ctx.game_state.position = position;
            tracing::debug!(message = "Updating player position", ?position, ?direction);
            if let Err(error) = ctx.own_position_sender.send((direction, position)) {
                tracing::error!(%error, "Failed to send own position to partner pokemon channel");
            };
            ctx.player_channel.send(PlayerMessageWrapper {
                user_id: ctx.claims.user_id(),
                map_id: Some(map_id),
                message: PlayerMessage::PlayerMoved {
                    position,
                    direction,
                },
            });
            let items = ctx
                .item_state
                .items_on_position(ctx.game_state.map_id, position);
            if !items.is_empty() {
                let game_loop_channel = ctx.game_loop_channel.clone();
                let items: Vec<ItemId> = items.iter().map(|(item_id, _)| *item_id).collect();
                let user_id = ctx.claims.user_id();
                let pool = ctx.db_connection_pool.clone();
                tokio::spawn(async move {
                    let collected_items = match sync_collected_items(user_id, &items, &pool).await {
                        Err(error) => {
                            tracing::error!(%error, "Failed to sync collected items");
                            return;
                        }
                        Ok(collected) => collected,
                    };
                    if let Err(error) = game_loop_channel
                        .send(GameLoopChannelMessage::CollectedItems(collected_items))
                        .await
                    {
                        tracing::error!(message = "Failed to send CollectedItems message to game loop", %error);
                    }
                });
            }
            for (item_id, _) in items {
                ctx.item_channel.send(ItemMessageWrapper {
                    item_id,
                    map_id,
                    msg: ItemMessage::Remove,
                });
            }
        }
        MoveResult::EnteredWarp { map_id, position } => {
            tracing::debug!(message = "User entered warp", ?position, %map_id);
            // Ignore all future moves, client is expected to reconnect to the new map
            ctx.warped_to = Some((map_id, position));

            let game_state = ctx.game_state;
            let user_id = ctx.claims.user_id();
            let game_loop_channel = ctx.game_loop_channel.clone();
            let pool = ctx.db_connection_pool.clone();
            tokio::spawn(async move {
                sync_game_state(
                    GameStateSnapshot {
                        position,
                        map_id,
                        ..game_state
                    },
                    user_id,
                    &pool,
                )
                .await;
                if let Err(error) = game_loop_channel
                    .send(GameLoopChannelMessage::SwitchMap {
                        future_map_id: map_id,
                        future_position: position,
                    })
                    .await
                {
                    tracing::error!(message = "Failed to send SwitchMap message to game loop", %error);
                }
            });
        }
        MoveResult::Disallowed => {
            tracing::warn!(message = "Tried to do illegal move", ?position);
            let channel = ctx.game_loop_channel.clone();
            tokio::spawn(async move {
                channel
                    .send(GameLoopChannelMessage::Close(CloseFrame {
                        code: close_code::POLICY,
                        reason: "Tried to move on blocked tile".into(),
                    }))
                    .await
                    .ok();
            });
        }
    }
}

fn on_send_chat_message(ctx: &mut GameLoopCtx, text: String) {
    let pool = ctx.db_connection_pool.clone();
    let map_channel = ctx.map_channel.clone();
    let map_id = ctx.game_state.map_id;
    let user_id = ctx.claims.user_id();
    let sent_at = OffsetDateTime::now_utc();

    tokio::spawn(async move {
        let chat_message = ChatMessage::new(text);
        let text = chat_message.text().to_string();
        let tags = match chat_message::keep_valid_tags(&pool, chat_message.into_tags()).await {
            Ok(ok) => ok,
            Err(error) => {
                tracing::error!(%error, "Failed to validate tags");
                return;
            }
        };
        map_channel.send(MapMessage {
            text,
            tags,
            map_id,
            from_user: user_id,
            sent_at,
        });
    });
}

fn on_switch_map(
    ctx: &mut GameLoopCtx,
    future_map_id: MapId,
    future_position: Position,
) -> WebsocketMessageToClient {
    let new_map_claims = ctx.claims.set_map_id(future_map_id);
    let new_map_token = ctx.auth_ctx.encode_token(&new_map_claims);
    WebsocketMessageToClient {
        message: Some(websocket_message_to_client::Message::EnteredWarp(
            EnteredWarp {
                to_x: future_position.x,
                to_y: future_position.y,
                layer_number: future_position.layer_number,
                new_map_token: new_map_token.token().to_owned(),
                token_valid_for_seconds: new_map_token
                    .valid_for()
                    .as_secs()
                    .try_into()
                    .expect("Valid for exceeded u32 max"),
            },
        )),
    }
}

fn collected_items_to_ws(
    items: Vec<(Item, u32)>,
) -> impl Iterator<Item = WebsocketMessageToClient> {
    items
        .into_iter()
        .map(|(item, amount)| WebsocketMessageToClient {
            message: Some(websocket_message_to_client::Message::CollectedItem(
                CollectedItem {
                    item: item.into(),
                    amount,
                },
            )),
        })
}

fn on_player_turn(ctx: &mut GameLoopCtx, direction: proto::Direction) {
    let direction = Direction::from(direction);
    ctx.game_state.direction = direction;

    ctx.player_channel.send(PlayerMessageWrapper {
        user_id: ctx.claims.user_id(),
        map_id: Some(ctx.game_state.map_id),
        message: PlayerMessage::PlayerMoved {
            position: ctx.game_state.position,
            direction,
        },
    });
}

fn on_throw_ball(ctx: &mut GameLoopCtx, throw_ball: websocket_message_to_server::ThrowBall) {
    let Ok(item) = proto::Item::try_from(throw_ball.item) else {
        return;
    };
    let thrown_at = Instant::now();
    let game_state = ctx.game_state;
    let websocket_message_to_server::ThrowBall {
        vector_x,
        vector_y,
        angle_radians,
        item: _,
    } = throw_ball;
    let throw = match Throw::new((vector_x, vector_y), angle_radians) {
        Ok(throw) => throw,
        Err(error) => {
            tracing::warn!(%error, "Invalid thrown ball");
            return;
        }
    };

    let game_loop_channel = ctx.game_loop_channel.clone();
    let pool = ctx.db_connection_pool.clone();
    let user_id = ctx.claims.user_id();

    tokio::spawn(async move {
        match throw::thrown_item_id(&pool, user_id, Item::from(item)).await {
            Ok(item_id) => {
                if let Err(error) = game_loop_channel
                    .send(GameLoopChannelMessage::ReservedItemId(ReservedItemId {
                        item_id,
                        item,
                        pos_x: game_state.position.x,
                        pos_y: game_state.position.y,
                        layer_number: game_state.position.layer_number,
                        map_id: game_state.map_id,
                        throw,
                        thrown_at,
                    }))
                    .await
                {
                    tracing::debug!(message = "Dropped game loop message due to channel being closed. This is probably fine.", dropped = ?error.0);
                };
            }
            Err(error) => match error {
                ThrowItemError::NoItemToThrow => {
                    tracing::warn!(?item, %user_id, "Tried to throw item without owning it");
                    game_loop_channel
                        .send(GameLoopChannelMessage::Close(CloseFrame {
                            code: close_code::POLICY,
                            reason: "Tried to use non-existent item".into(),
                        }))
                        .await
                        .ok();
                }
                ThrowItemError::Other(error) => {
                    tracing::error!(%error, "Failed to reserve item id for thrown ball");
                }
            },
        };
    });
}

fn on_reserved_thrown_ball_id(
    ctx: &mut GameLoopCtx,
    ReservedItemId {
        map_id,
        item_id,
        item,
        pos_x,
        pos_y,
        layer_number,
        throw,
        thrown_at,
    }: ReservedItemId,
) -> WebsocketMessageToClient {
    let item = Item::from(item);
    ctx.player_channel.send(PlayerMessageWrapper {
        user_id: ctx.claims.user_id(),
        map_id: Some(map_id),
        message: PlayerMessage::ThrewBall {
            from_x: pos_x as f32,
            from_y: pos_y as f32,
            throw,
            item,
            ball_id: item_id,
        },
    });

    let throw_target = calc_throw_target_and_time(&ctx.settings, throw);
    let item_channel = ctx.item_channel.clone();
    let settings = ctx.settings;
    let encounter_state = ctx.encounter_state.clone();
    let pool = ctx.db_connection_pool.clone();
    let user_id = ctx.claims.user_id();
    let pokemon_channel: crate::channels::Channel<PokemonMessageWrapper> =
        ctx.pokemon_channel.clone();
    let arceus_signal_sender = ctx.arceus_signal_sender.clone();
    let pokemon_config = ctx.pokemon_config.clone();
    let tile_cache = ctx.tile_cache.clone();
    let mut rng = ctx.rng.split();
    // Make sure the next rng is different
    _ = ctx.rng.rand();

    tokio::spawn(async move {
        tokio::time::sleep_until(thrown_at + throw_target.duration_until_impact).await;

        let x = f64::from(throw_target.x) + f64::from(pos_x);
        let y = f64::from(throw_target.y) + f64::from(pos_y);
        let target_position = Position {
            x: x.round() as i32,
            y: y.round() as i32,
            layer_number,
        };
        let target_encounter = encounter_state.get_encounter_on_position(map_id, target_position);
        if let Some((target_id, target)) = target_encounter {
            let Some(pokemon) = pokemon_config.get(target.pokemon_number) else {
                tracing::error!(pokemon_number = %target.pokemon_number, "Encounter was not found in pokemon config");
                return;
            };

            let map_provider = tile_cache
                .move_rules_for_map_id(map_id)
                .await
                .inspect_err(
                    |error| tracing::error!(%error, map_id = %map_id, "Failed to load move rules"),
                )
                .ok();
            let target_move_rule =
                map_provider.and_then(|provider| provider.move_rule_for_position(target_position));
            let is_caught = catch::determine_catch(&mut rng, pokemon, item, target_move_rule);

            if !is_caught {
                tracing::debug!(%user_id, pokemon = %target.pokemon_number, encounter_id = %target_id, "Pokemon broke out!");
                pokemon_channel.send(PokemonMessageWrapper {
                    map_id,
                    encounter_id: target_id,
                    message: PokemonMessage::BrokeOut { from: item_id },
                });
                return;
            }

            match catch_pokemon(&pool, user_id, target.pokemon_number, target.is_shiny).await {
                Ok(()) => {
                    tracing::debug!(%user_id, pokemon = %target.pokemon_number, encounter_id = %target_id, "Player caught pokemon");
                    pokemon_channel.send(PokemonMessageWrapper {
                        map_id,
                        encounter_id: target_id,
                        message: PokemonMessage::Caught { by: item_id },
                    });
                }
                Err(error) => {
                    tracing::error!(%error, "Failed to save pokemon catch.");
                }
            };
            if let Err(error) = arceus_signal_sender.stop_encounter(target_id, map_id).await {
                tracing::error!(%error, encounter_id=%target_id, "Failed to stop caught encounter");
            }
        } else {
            let x = x as f32;
            let y = y as f32;
            let despawn_time = OffsetDateTime::now_utc() + settings.thrown_item_despawn_time();

            if let Err(error) = sqlx::query!(
                r#"INSERT INTO item (id, item, x, y, map_id, layer_number, despawn_time)
                VALUES ($1, $2, $3, $4, $5, $6, $7)
            "#,
                item_id as ItemId,
                item as Item,
                x,
                y,
                map_id as MapId,
                layer_number,
                despawn_time
            )
            .execute(&pool)
            .await
            {
                tracing::error!(%error, "Failed to save missed item");
            };
            item_channel.send(ItemMessageWrapper {
                item_id,
                map_id,
                msg: ItemMessage::New {
                    item,
                    x,
                    y,
                    layer_number,
                    despawn_time,
                },
            });
            tracing::info!(%item_id, "Thrown item missed and may be picked up now");
        }
    });

    let msg = websocket_message_to_client::Message::GeneratedBallId(item_id.into());
    WebsocketMessageToClient { message: Some(msg) }
}

pub async fn catch_pokemon(
    pool: &sqlx::Pool<sqlx::Postgres>,
    user_id: UserId,
    pokemon_number: PokemonNumber,
    is_shiny: bool,
) -> AppResult<()> {
    let amount_normal;
    let amount_shiny;
    if is_shiny {
        amount_normal = 0;
        amount_shiny = 1;
    } else {
        amount_normal = 1;
        amount_shiny = 0;
    }
    sqlx::query!(
        "INSERT INTO dex_entry (amount_normal, amount_shiny, user_id, pokemon_number)
        VALUES ($1, $2, $3, $4) ON CONFLICT (user_id, pokemon_number) DO UPDATE
            SET amount_normal = EXCLUDED.amount_normal + dex_entry.amount_normal,
                amount_shiny = EXCLUDED.amount_shiny + dex_entry.amount_shiny",
        amount_normal,
        amount_shiny,
        user_id as UserId,
        pokemon_number as PokemonNumber
    )
    .execute(pool)
    .await?;
    Ok(())
}

async fn on_disconnect(ctx: &GameLoopCtx) -> sqlx::Result<()> {
    let user_id = ctx.claims.user_id();
    let last_game_state = ctx.game_state;
    ctx.player_channel.send(PlayerMessageWrapper {
        user_id,
        map_id: Some(last_game_state.map_id),
        message: PlayerMessage::WentOffline,
    });
    ctx.arceus_signal_sender
        .stop_map(last_game_state.map_id, &user_id)
        .await;
    // make sure to not overwrite anything in case the client connects to the new map early
    if ctx.warped_to.is_none() {
        sqlx::query!(
            r#"UPDATE user_position
        SET map_id = $1, 
        x = $2, 
        y = $3, 
        layer_number = $4, 
        direction = $5
        WHERE user_id = $6"#,
            last_game_state.map_id as MapId,
            last_game_state.position.x,
            last_game_state.position.y,
            last_game_state.position.layer_number,
            last_game_state.direction as Direction,
            user_id as UserId
        )
        .execute(&ctx.db_connection_pool)
        .await?;
    }

    tracing::info!(message = "Synced user state to database", %user_id);
    Ok(())
}

#[derive(Debug, Clone)]
struct User {
    display_name: String,
    map_id: MapId,
    x: i32,
    y: i32,
    layer_number: i32,
    trainer_number: TrainerNumber,
    direction: Direction,
    partner_pokemon_number: PokemonNumber,
    partner_pokemon_is_shiny: bool,
}
async fn get_user(
    user_id: UserId,
    db_connection_pool: &PgPool,
) -> Result<Option<User>, sqlx::Error> {
    sqlx::query_as!(User,
        r#"SELECT display_name, map_id AS "map_id!", x, y, layer_number,
           trainer_number as "trainer_number: _",
           direction as "direction: _", 
           partner_pokemon_number as "partner_pokemon_number: _", 
           dex_entry.amount_shiny > 0 as "partner_pokemon_is_shiny!"
        FROM user_position
        JOIN users ON users.id = user_position.user_id
        JOIN user_partner_pokemon ON user_partner_pokemon.user_id = user_position.user_id
        JOIN user_trainer ON user_trainer.user_id = user_position.user_id
        JOIN dex_entry ON user_position.user_id = dex_entry.user_id AND user_partner_pokemon.partner_pokemon_number = dex_entry.pokemon_number
      	WHERE user_position.user_id = $1"#,
        user_id as UserId
    )
    .fetch_optional(db_connection_pool)
    .await
}

async fn sync_game_state(
    snapshot: GameStateSnapshot,
    user_id: UserId,
    db_connection_pool: &PgPool,
) {
    if let Err(error) = sqlx::query!(
        r#"
        UPDATE user_position
        SET map_id = $1, 
            x = $2, 
            y = $3, 
            layer_number = $4, 
            direction = $5
        WHERE user_id = $6
    "#,
        snapshot.map_id as MapId,
        snapshot.position.x,
        snapshot.position.y,
        snapshot.position.layer_number,
        snapshot.direction as Direction,
        user_id as UserId
    )
    .execute(db_connection_pool)
    .await
    {
        tracing::warn!(message = "Failed to sync game state to database.", %error);
    }
}

#[tracing::instrument(skip(db_connection_pool))]
pub async fn sync_collected_items(
    user_id: UserId,
    items: &[ItemId],
    db_connection_pool: &PgPool,
) -> AppResult<Vec<(Item, u32)>> {
    let collected_items = sqlx::query!(
        r#"
        WITH deletions AS (
            DELETE FROM item WHERE id = ANY($1) RETURNING item
        )
        INSERT INTO user_items (user_id, item, amount)
        SELECT $2, item, count(*) FROM deletions GROUP BY item
        ON CONFLICT (user_id, item) DO UPDATE
        SET amount = user_items.amount + EXCLUDED.amount
        RETURNING item as "item: Item", amount
        "#,
        items as &[ItemId],
        user_id as UserId
    )
    .fetch_all(db_connection_pool)
    .await?;

    Ok(collected_items
        .into_iter()
        .map(|ci| (ci.item, ci.amount as u32))
        .collect())
}

#[derive(Debug)]
pub enum MoveResult {
    Disallowed,
    Allowed { position: Position },
    EnteredWarp { map_id: MapId, position: Position },
}

async fn move_in_dir(
    tile_cache: &dyn TileDataProvider,
    warp_cache: &WarpCache,
    map_id: MapId,
    pos: Position,
    direction: Direction,
) -> MoveResult {
    let player_move_abilities = MoveAbilities::default() | MoveAbility::Walk;
    let next_pos = match tile_cache
        .move_in_dir(map_id, pos, player_move_abilities, direction)
        .await
    {
        Ok(Some(next_pos)) => next_pos,
        Ok(None) => {
            return MoveResult::Disallowed;
        }
        Err(_error) => {
            tracing::error!(
                message = "Cannot move in direction because map move rules were not loaded"
            );
            return MoveResult::Disallowed;
        }
    };

    match warp_cache.is_warp(map_id, next_pos).await {
        None => MoveResult::Allowed { position: next_pos },
        Some((warp_pos, warp_map_id)) => MoveResult::EnteredWarp {
            map_id: warp_map_id,
            position: warp_pos,
        },
    }
}

pub async fn on_close<S>(sink: &mut S, close_frame: ws::CloseFrame)
where
    S: Sink<ws::Message> + Unpin,
    S::Error: Display,
{
    let send_result = sink.send(ws::Message::Close(Some(close_frame))).await;
    if let Err(error) = send_result {
        tracing::debug!(message = "Failed to send close code", %error);
    }
}

#[cfg(test)]
mod tests {

    use core::f32;
    use std::future::IntoFuture as _;
    use std::net::{Ipv4Addr, SocketAddr};

    use axum::extract::{Query, State, WebSocketUpgrade};
    use axum::response::{IntoResponse, Response};
    use axum::routing::get;
    use serde::{Deserialize, Serialize};
    use tokio::net::TcpStream;
    use tokio::time::timeout;
    use tokio_tungstenite::tungstenite::protocol::frame::coding::CloseCode;
    use tokio_tungstenite::{MaybeTlsStream, WebSocketStream, tungstenite};

    use crate::arceus::test_helper as arceus;
    use crate::data::EncounterId;
    use crate::gardevoir::test_helper::{self as gardevoir, LoggedInUser};
    use crate::kangaskhan::api::{self, add_warp};
    use crate::kangaskhan::proto::kangaskhan::websocket_message_to_client::{
        MovePartnerPokemon, PlayerEnteredMap, PlayerMoved, PlayerThrewBall,
    };
    use crate::kangaskhan::proto::kangaskhan::websocket_message_to_server::ThrowBall;
    use crate::smeargle::test_helper as smeargle;

    use super::*;

    #[database_macros::test]
    async fn user_not_on_map_unsupported(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let user = gardevoir::UserBuilder::from_name("testuser")
            .create(&setup.ctx.auth_ctx, &pool)
            .await?;
        let error = setup
            .connect(&user)
            .await
            .expect_err("User is not on a map");
        let tungstenite::Error::Http(response) = &error else {
            panic!("Expected http error but got {error}");
        };
        assert_eq!(response.status(), http::StatusCode::BAD_REQUEST);

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    async fn incomplete_user_unsupported(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let mut user = gardevoir::UserBuilder::from_name("testuser")
            .create(&setup.ctx.auth_ctx, &pool)
            .await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 0).await?;
        setup.choose_start_tile(&mut user, start_tile_id).await?;

        // User is now on a map but he has not chosen a partner/trainer yet
        let mut connection = setup.connect(&user).await?;
        assert_eq!(
            connection.wait_for_close().await,
            Some(tungstenite::protocol::CloseFrame {
                code: CloseCode::Unsupported,
                reason: "User not fully created".into()
            })
        );

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn complete_user_notifies_generator(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 0).await?;
        let user = setup
            .create_completed_user("testuser", start_tile_id)
            .await?;

        let mut connection = setup.connect(&user).await?;
        connection.wait_for_ping().await;

        assert_eq!(
            setup.arceus_signal_receiver.next().await,
            Some(crate::arceus::Message::StartMap(map_id))
        );

        connection.socket.close(None).await?;
        assert_eq!(
            timeout(Duration::from_secs(1), setup.arceus_signal_receiver.next()).await,
            Ok(Some(crate::arceus::Message::StopMap(map_id)))
        );

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    async fn two_users_see_each_other(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let mut user = gardevoir::UserBuilder::from_name("testuser")
            .create(&setup.ctx.auth_ctx, &pool)
            .await?;
        let mut other_user = gardevoir::UserBuilder::from_name("otheruser")
            .create(&setup.ctx.auth_ctx, &pool)
            .await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1), (2, 2), (1, 2)]);
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 0).await?;
        let start_tile_id_2 = smeargle::add_start_tile(&pool, map_id, 2, 2).await?;

        // First user completes setup
        setup.set_partner(&user, PokemonNumber(1)).await?;
        setup.set_trainer(&user, TrainerNumber::from(1)).await?;
        setup.choose_start_tile(&mut user, start_tile_id).await?;

        // Second user completes setup
        setup.set_partner(&other_user, PokemonNumber(4)).await?;
        setup
            .set_trainer(&other_user, TrainerNumber::from(2))
            .await?;
        setup
            .choose_start_tile(&mut other_user, start_tile_id_2)
            .await?;

        // First user connects, partner pokemon spawns
        let mut user_connection = setup.connect(&user).await?;
        assert_eq!(
            user_connection.wait_for_message().await,
            websocket_message_to_client::Message::MovePartnerPokemon(MovePartnerPokemon {
                user_id: user.id(),
                coord_x: 0,
                coord_y: 1,
                layer_number: 0,
                direction: Direction::U.into()
            })
        );

        // Second user connects
        let mut other_connection = setup.connect(&other_user).await?;
        // First user gets the second users and their partners position
        assert_eq!(
            user_connection.wait_for_message().await,
            websocket_message_to_client::Message::PlayerEnteredMap(PlayerEnteredMap {
                coord_x: 2,
                coord_y: 2,
                layer_number: 0,
                user_name: "otheruser".to_string(),
                trainer_number: 2,
                partner_pokemon_number: 4,
                partner_is_shiny: false,
                direction: proto::Direction::D.into(),
                user_id: other_user.id()
            })
        );
        assert_eq!(
            user_connection.wait_for_message().await,
            websocket_message_to_client::Message::MovePartnerPokemon(MovePartnerPokemon {
                user_id: other_user.id(),
                coord_x: 1,
                coord_y: 2,
                layer_number: 0,
                direction: Direction::R.into()
            })
        );

        // Second user gets in any order
        // - the first users position
        // - the first users partner position
        // - their own partner position
        let messages = other_connection.wait_for_messages(3).await;
        assert!(
            messages.contains(&websocket_message_to_client::Message::PlayerEnteredMap(
                PlayerEnteredMap {
                    coord_x: 0,
                    coord_y: 0,
                    layer_number: 0,
                    user_name: "testuser".to_string(),
                    trainer_number: 1,
                    partner_pokemon_number: 1,
                    partner_is_shiny: false,
                    direction: proto::Direction::D.into(),
                    user_id: user.id()
                }
            )),
            "Actual messages: {messages:?}"
        );
        assert!(
            messages.contains(&websocket_message_to_client::Message::MovePartnerPokemon(
                MovePartnerPokemon {
                    user_id: other_user.id(),
                    coord_x: 1,
                    coord_y: 2,
                    layer_number: 0,
                    direction: Direction::R.into()
                }
            )),
            "Actual messages: {messages:?}"
        );
        assert!(
            messages.contains(&websocket_message_to_client::Message::MovePartnerPokemon(
                MovePartnerPokemon {
                    user_id: user.id(),
                    coord_x: 0,
                    coord_y: 1,
                    layer_number: 0,
                    direction: Direction::U.into()
                }
            )),
            "Actual messages: {messages:?}"
        );

        // Now the first player moves and a couple things should happen:
        // - The other user should receive the movement
        // - Both users should receive movements for the users partner pokemon
        //   Since the player moves into his partner position and all other tiles are blocked
        //   The partner should swap with the player and then turn back towards him
        user_connection
            .send(websocket_message_to_server::Message::PlayerMove(
                proto::Direction::D.into(),
            ))
            .await?;
        assert_eq!(
            other_connection.wait_for_message().await,
            websocket_message_to_client::Message::PlayerMoved(PlayerMoved {
                coord_x: 0,
                coord_y: 1,
                layer_number: 0,
                direction: proto::Direction::D.into(),
                user_id: user.id()
            })
        );

        let partner_swap_message =
            websocket_message_to_client::Message::MovePartnerPokemon(MovePartnerPokemon {
                coord_x: 0,
                coord_y: 0,
                layer_number: 0,
                direction: proto::Direction::U.into(),
                user_id: user.id(),
            });
        assert_eq!(
            other_connection.wait_for_message().await,
            partner_swap_message
        );
        assert_eq!(
            user_connection.wait_for_message().await,
            partner_swap_message
        );

        let partner_turn_message =
            websocket_message_to_client::Message::MovePartnerPokemon(MovePartnerPokemon {
                coord_x: 0,
                coord_y: 0,
                layer_number: 0,
                direction: proto::Direction::D.into(),
                user_id: user.id(),
            });
        assert_eq!(
            other_connection.wait_for_message().await,
            partner_turn_message
        );
        assert_eq!(
            user_connection.wait_for_message().await,
            partner_turn_message
        );

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn user_switches_map(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id_1 = smeargle::create_map(&pool, "my map").await?;
        let map_id_2 = smeargle::create_map(&pool, "other map").await?;
        let start_tile_id_1 = smeargle::add_start_tile(&pool, map_id_1, 0, 0).await?;
        let start_tile_id_2 = smeargle::add_start_tile(&pool, map_id_2, 1, 1).await?;

        let mut user_1 = setup
            .create_completed_user("user_1", start_tile_id_1)
            .await?;
        let user_2 = setup
            .create_completed_user("user_2", start_tile_id_1)
            .await?;
        let user_3 = setup
            .create_completed_user("user_3", start_tile_id_2)
            .await?;

        setup.set_moveable_coords(map_id_1, &[(0, 0), (0, 1)]);
        setup.set_moveable_coords(map_id_2, &[(1, 1), (2, 1)]);
        setup.add_warp((map_id_1, 0, 1), (map_id_2, 1, 1)).await?;

        // user_1 connects to first map
        let mut connection_1 = setup.connect(&user_1).await?;
        // user_2 connects to first map
        let mut connection_2 = setup.connect(&user_2).await?;
        // user_3 connects to second map
        let mut connection_3 = setup.connect(&user_3).await?;

        connection_1.wait_for_messages(3).await; // user_1 pos, user_1 partner, user_2 partner
        connection_2.wait_for_messages(3).await; // user_2 pos, user_1 partner, user_2 partner

        // user_1 walks into a warp
        connection_1
            .send(websocket_message_to_server::Message::PlayerMove(
                proto::Direction::D.into(),
            ))
            .await?;
        let entered_warp = match connection_1.wait_for_message().await {
            websocket_message_to_client::Message::EnteredWarp(entered_warp) => entered_warp,
            other => {
                panic!("Expected warp message but received {other:?}");
            }
        };

        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PlayerWentOffline(user_1.id())
        );

        // Connect to the new map
        user_1.set_access_token(map_id_2, entered_warp.new_map_token);
        let mut new_map_connection = setup.connect(&user_1).await?;

        // user_1 receives partner position and user_3 data
        let messages = new_map_connection.wait_for_messages(3).await;
        assert_contains_player_entered_map(&messages, user_3.id());
        assert_contains_move_partner_pokemon(&messages, user_3.id(), 1);
        assert_contains_move_partner_pokemon(&messages, user_1.id(), 1);

        // user_3 receives user_1 data as well
        let messages = connection_3.wait_for_messages(3).await;
        assert_contains_player_entered_map(&messages, user_1.id());
        assert_contains_move_partner_pokemon(&messages, user_1.id(), 1);
        assert_contains_move_partner_pokemon(&messages, user_3.id(), 1);

        // Now user_4 connects to map_1
        // This tests e.g. that user_1 is not in some cache anymore since he moved to map_2.
        let user_4 = setup
            .create_completed_user("user_4", start_tile_id_1)
            .await?;
        let mut connection_4 = setup.connect(&user_4).await?;
        let messages = connection_4.wait_for_messages(3).await;
        assert_contains_player_entered_map(&messages, user_2.id());
        assert_contains_move_partner_pokemon(&messages, user_2.id(), 1);
        assert_contains_move_partner_pokemon(&messages, user_4.id(), 1);

        // Now user_5 connects to map_2
        // This tests e.g. that the removal of user_1 from map_1 did not affect his presence on map_2.
        let user_5 = setup
            .create_completed_user("user_5", start_tile_id_2)
            .await?;
        let mut connection_4 = setup.connect(&user_5).await?;
        let messages = connection_4.wait_for_messages(5).await;
        assert_contains_player_entered_map(&messages, user_1.id());
        assert_contains_move_partner_pokemon(&messages, user_1.id(), 1);
        assert_contains_player_entered_map(&messages, user_3.id());
        assert_contains_move_partner_pokemon(&messages, user_3.id(), 1);
        assert_contains_move_partner_pokemon(&messages, user_5.id(), 1);

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn user_throws_ball_but_does_not_have_any(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 1).await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1)]);

        let user_1 = setup.create_completed_user("user_1", start_tile_id).await?;
        let user_2 = setup.create_completed_user("user_2", start_tile_id).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        let mut connection_2 = setup.connect(&user_2).await?;

        connection_1.wait_for_messages(3).await; // user_1 pos, user_1 partner, user_2 partner
        connection_2.wait_for_messages(3).await; // user_2 pos, user_1 partner, user_2 partner

        // user_1 does not have any items but still tries to throw a ball
        connection_1
            .send(websocket_message_to_server::Message::ThrowBall(ThrowBall {
                vector_x: 0.4,
                vector_y: 0.6,
                angle_radians: f32::consts::PI / 4.0,
                item: proto::Item::Quickball.into(),
            }))
            .await?;
        assert_eq!(
            connection_1.wait_for_close().await,
            Some(tungstenite::protocol::CloseFrame {
                code: CloseCode::Policy,
                reason: "Tried to use non-existent item".into()
            })
        );

        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PlayerWentOffline(user_1.id())
        );
        // Illegal throw attempt does not get send to other players
        connection_2.assert_no_message().await;

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    async fn user_throws_ball(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 1).await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1)]);
        setup.spawn_item(map_id, 0.0, 0.0, Item::Quickball).await?;

        let user_1 = setup.create_completed_user("user_1", start_tile_id).await?;
        let user_2 = setup.create_completed_user("user_2", start_tile_id).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        let mut connection_2 = setup.connect(&user_2).await?;

        connection_1.wait_for_messages(3).await; // user_1 pos, user_1 partner, user_2 partner
        connection_2.wait_for_messages(3).await; // user_2 pos, user_1 partner, user_2 partner

        connection_1
            .send(websocket_message_to_server::Message::PlayerMove(
                proto::Direction::U.into(),
            ))
            .await?;
        connection_1.wait_for_messages(4).await; // item spawn, despawn, collected item, partner move
        connection_2.wait_for_messages(4).await; // item spawn, despawn, user_1 move, partner_1 move

        connection_1
            .send(websocket_message_to_server::Message::ThrowBall(ThrowBall {
                vector_x: 0.4,
                vector_y: 0.6,
                angle_radians: f32::consts::PI / 4.0,
                item: proto::Item::Quickball.into(),
            }))
            .await?;
        let ball_id = match connection_1.wait_for_message().await {
            websocket_message_to_client::Message::GeneratedBallId(id) => id,
            other => panic!("Expected GeneratedBallId but got {other:?}"),
        };

        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PlayerThrewBall(PlayerThrewBall {
                user_id: user_1.id(),
                ball_id,
                vector_x: 0.4,
                vector_y: 0.6,
                from_x: 0.0,
                from_y: 0.0,
                angle_radians: f32::consts::PI / 4.0,
                item: proto::Item::Quickball.into()
            })
        );

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn user_turns(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 1).await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1)]);

        let user_1 = setup.create_completed_user("user_1", start_tile_id).await?;
        let user_2 = setup.create_completed_user("user_2", start_tile_id).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_1.wait_for_messages(3).await; // user_1 pos, user_1 partner, user_2 partner
        connection_2.wait_for_messages(3).await; // user_2 pos, user_1 partner, user_2 partner

        connection_1
            .send(websocket_message_to_server::Message::PlayerTurn(
                proto::Direction::L.into(),
            ))
            .await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PlayerMoved(PlayerMoved {
                user_id: user_1.id(),
                coord_x: 0,
                coord_y: 1,
                layer_number: 0,
                direction: proto::Direction::L.into()
            })
        );

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    async fn user_collects_item(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 1).await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1)]);

        let user_1 = setup.create_completed_user("user_1", start_tile_id).await?;
        let user_2 = setup.create_completed_user("user_2", start_tile_id).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_1.wait_for_messages(3).await; // user_1 pos, user_1 partner, user_2 partner
        connection_2.wait_for_messages(3).await; // user_2 pos, user_1 partner, user_2 partner

        let item_id = setup.spawn_item(map_id, 0.0, 0.0, Item::Superball).await?;

        connection_1
            .send(websocket_message_to_server::Message::PlayerMove(
                proto::Direction::U.into(),
            ))
            .await?;
        let messages = connection_1.wait_for_messages(4).await;
        assert_contains_move_partner_pokemon(&messages, user_1.id(), 1);
        assert!(
            messages.contains(&websocket_message_to_client::Message::ItemSpawned(
                ItemSpawned {
                    item: proto::Item::Superball.into(),
                    x: 0.0,
                    y: 0.0,
                    layer_number: 0,
                    amount: 1,
                    item_id: item_id.into()
                }
            )),
            "Actual: {messages:?}"
        );
        assert!(
            messages.contains(&websocket_message_to_client::Message::ItemDespawned(
                item_id.into()
            )),
            "Actual: {messages:?}"
        );
        assert!(
            messages.contains(&websocket_message_to_client::Message::CollectedItem(
                CollectedItem {
                    item: proto::Item::Superball.into(),
                    amount: 1
                }
            )),
            "Actual: {messages:?}"
        );

        let messages = connection_2.wait_for_messages(4).await;
        assert_contains_move_partner_pokemon(&messages, user_1.id(), 1);
        assert!(
            messages.contains(&websocket_message_to_client::Message::PlayerMoved(
                PlayerMoved {
                    user_id: user_1.id(),
                    coord_x: 0,
                    coord_y: 0,
                    layer_number: 0,
                    direction: proto::Direction::U.into()
                }
            )),
            "Actual: {messages:?}"
        );
        assert!(
            messages.contains(&websocket_message_to_client::Message::ItemSpawned(
                ItemSpawned {
                    item: proto::Item::Superball.into(),
                    x: 0.0,
                    y: 0.0,
                    layer_number: 0,
                    amount: 1,
                    item_id: item_id.into()
                }
            )),
            "Actual: {messages:?}"
        );
        assert!(
            messages.contains(&websocket_message_to_client::Message::ItemDespawned(
                item_id.into()
            )),
            "Actual: {messages:?}"
        );

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn user_catches_pokemon(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup_with_seed(&pool, 123).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 1).await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1)]);
        setup.spawn_item(map_id, 0.0, 0.0, Item::Pokeball).await?;

        let user_1 = setup.create_completed_user("user_1", start_tile_id).await?;
        let user_2 = setup.create_completed_user("user_2", start_tile_id).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_1.wait_for_messages(3).await; // user_1 pos, user_1 partner, user_2 partner
        connection_2.wait_for_messages(3).await; // user_2 pos, user_1 partner, user_2 partner

        connection_1
            .send(websocket_message_to_server::Message::PlayerMove(
                proto::Direction::U.into(),
            ))
            .await?;
        connection_1.wait_for_messages(4).await; // item spawn, despawn, collected item, partner move
        connection_2.wait_for_messages(4).await; // item spawn, despawn, user_1 move, partner_1 move

        setup
            .spawn_encounter(map_id, 0, 1, PokemonNumber(120))
            .await?;
        connection_1
            .send(websocket_message_to_server::Message::ThrowBall(ThrowBall {
                vector_x: 0.0,
                vector_y: 1.0,
                angle_radians: f32::consts::PI / 32.0,
                item: proto::Item::Pokeball.into(),
            }))
            .await?;
        let encounter_id = match connection_1.wait_for_message().await {
            websocket_message_to_client::Message::NewEncounter(encounter) => encounter.encounter_id,
            other => panic!("Expected NewEncounter but got {other:?}"),
        };
        let item_id = match connection_1.wait_for_message().await {
            websocket_message_to_client::Message::GeneratedBallId(ball_id) => ball_id,
            other => panic!("Expected GeneratedBallId but got {other:?}"),
        };
        assert_eq!(
            connection_1.wait_for_message().await,
            websocket_message_to_client::Message::PokemonCaught(TargetPokemon {
                encounter_id,
                item_id
            })
        );

        match connection_2.wait_for_message().await {
            websocket_message_to_client::Message::NewEncounter(encounter)
                if encounter.encounter_id == encounter_id => {}
            other => panic!("Expected NewEncounter but got {other:?}"),
        };
        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PlayerThrewBall(PlayerThrewBall {
                ball_id: item_id,
                user_id: user_1.id(),
                from_x: 0.0,
                from_y: 0.0,
                vector_x: 0.0,
                vector_y: 1.0,
                angle_radians: f32::consts::PI / 32.0,
                item: proto::Item::Pokeball.into(),
            })
        );
        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PokemonCaught(TargetPokemon {
                encounter_id,
                item_id
            })
        );

        setup.shutdown().await;
        Ok(())
    }
    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn user_moves_illegally(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 1).await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1)]);

        let user_1 = setup.create_completed_user("user_1", start_tile_id).await?;
        let user_2 = setup.create_completed_user("user_2", start_tile_id).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_1.wait_for_messages(3).await; // user_1 pos, user_1 partner, user_2 partner
        connection_2.wait_for_messages(3).await; // user_2 pos, user_1 partner, user_2 partner

        connection_1
            .send(websocket_message_to_server::Message::PlayerMove(
                // Left would move the player to (-1, 1) which is not part of the moveable coordinates
                proto::Direction::L.into(),
            ))
            .await?;
        assert_eq!(
            connection_1.wait_for_close().await,
            Some(tungstenite::protocol::CloseFrame {
                code: CloseCode::Policy,
                reason: "Tried to move on blocked tile".into()
            })
        );

        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PlayerWentOffline(user_1.id())
        );
        // Illegal throw attempt does not get send to other players
        connection_2.assert_no_message().await;

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    async fn user_disconnects(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let map_id = smeargle::create_map(&pool, "my map").await?;
        let start_tile_id = smeargle::add_start_tile(&pool, map_id, 0, 1).await?;
        setup.set_moveable_coords(map_id, &[(0, 0), (0, 1)]);

        let user_1 = setup.create_completed_user("user_1", start_tile_id).await?;
        let user_2 = setup.create_completed_user("user_2", start_tile_id).await?;

        let mut connection_1 = setup.connect(&user_1).await?;
        connection_1.wait_for_ping().await;
        let mut connection_2 = setup.connect(&user_2).await?;
        connection_2.wait_for_ping().await;

        connection_1.socket.close(None).await?;
        assert_eq!(
            connection_2.wait_for_message().await,
            websocket_message_to_client::Message::PlayerWentOffline(user_1.id())
        );

        setup.shutdown().await;
        Ok(())
    }

    struct Setup<S> {
        ctx: WebsocketCtx,
        addr: SocketAddr,
        ctrl: shutdown::Ctrl,
        arceus_signal_receiver: S,
        tile_cache: Arc<smeargle::TestMoveRuleProvider>,
    }

    #[derive(Debug)]
    struct Connection {
        socket: WebSocketStream<MaybeTlsStream<TcpStream>>,
    }

    impl Connection {
        async fn wait_for_ping(&mut self) {
            while let Some(Ok(message)) = self.next().await {
                if let tungstenite::Message::Ping(_) = message {
                    return;
                };
            }

            panic!("Expected close but the connection ended");
        }

        async fn wait_for_close(&mut self) -> Option<tungstenite::protocol::CloseFrame> {
            while let Some(Ok(message)) = self.next().await {
                if let tungstenite::Message::Close(close_frame) = message {
                    return close_frame;
                };
            }

            panic!("Expected close but the connection ended");
        }

        async fn wait_for_messages(
            &mut self,
            count: u8,
        ) -> Vec<websocket_message_to_client::Message> {
            let mut messages = Vec::new();
            for _ in 0..count {
                messages.push(self.wait_for_message().await);
            }
            messages
        }

        async fn wait_for_message(&mut self) -> websocket_message_to_client::Message {
            while let Some(Ok(message)) = self.next().await {
                let tungstenite::Message::Binary(mut binary) = message else {
                    continue;
                };
                return WebsocketMessageToClient::decode(&mut binary)
                    .expect("No decode error")
                    .message
                    .expect("Some message");
            }

            panic!("Expected message but the connection ended");
        }

        async fn assert_no_message(&mut self) {
            // Need to try multiple times because of pings.
            while let Ok(next) = timeout(Duration::from_secs(1), self.socket.next()).await {
                if let Some(Ok(tungstenite::Message::Binary(mut binary))) = next {
                    let message = WebsocketMessageToClient::decode(&mut binary)
                        .expect("No decode error")
                        .message
                        .expect("Some message");
                    panic!("expected not to get message but got {message:?}");
                }
            }
        }

        async fn next(&mut self) -> Option<Result<tungstenite::Message, tungstenite::Error>> {
            timeout(Duration::from_secs(10), self.socket.next())
                .await
                .expect("No timeout")
        }

        async fn send(
            &mut self,
            message: websocket_message_to_server::Message,
        ) -> anyhow::Result<()> {
            self.socket
                .send(tungstenite::Message::Binary(encode_to_bytes(
                    WebsocketMessageToServer {
                        message: Some(message),
                    },
                )))
                .await?;
            Ok(())
        }
    }

    fn assert_contains_player_entered_map(
        messages: &[websocket_message_to_client::Message],
        user_id: UserId,
    ) {
        let count = messages
            .iter()
            .filter(|message| {
                let websocket_message_to_client::Message::PlayerEnteredMap(player_entered_map) =
                    message
                else {
                    return false;
                };
                player_entered_map.user_id == user_id
            })
            .count();
        if count == 0 {
            panic!("Expected PlayerEnteredMap with user_id {user_id} but got {messages:?}");
        }
        if count > 1 {
            panic!("Expected single PlayerEnteredMap with user_id {user_id} but got {messages:?}");
        }
    }

    fn assert_contains_move_partner_pokemon(
        messages: &[websocket_message_to_client::Message],
        user_id: UserId,
        expected_count: usize,
    ) {
        let count = messages
            .iter()
            .filter(|message| {
                let websocket_message_to_client::Message::MovePartnerPokemon(move_partner_pokemon) =
                    message
                else {
                    return false;
                };
                move_partner_pokemon.user_id == user_id
            })
            .count();
        if count == 0 {
            panic!("Expected MovePartnerPokemon with user_id {user_id} but got {messages:?}");
        }
        if count != expected_count {
            panic!(
                "Expected {expected_count} MovePartnerPokemon with user_id {user_id} but got {messages:?}"
            );
        }
    }

    impl<S> Setup<S> {
        async fn connect(&self, user: &LoggedInUser) -> Result<Connection, tungstenite::Error> {
            let addr = self.addr;
            let token = user.access_token.clone();
            let url = match user.claims().map_id() {
                Some(map_id) => format!("ws://{addr}/ws?map_id={map_id}&token={token}"),
                None => format!("ws://{addr}/ws?token={token}"),
            };
            let (socket, _response) = tokio_tungstenite::connect_async(url).await?;
            Ok(Connection { socket })
        }

        async fn create_completed_user(
            &self,
            name: &str,
            start_tile_id: i64,
        ) -> anyhow::Result<LoggedInUser> {
            let mut user = gardevoir::UserBuilder::from_name(name)
                .create(&self.ctx.auth_ctx, &self.ctx.db_connection_pool)
                .await?;

            self.set_partner(&user, PokemonNumber(1)).await?;
            self.set_trainer(&user, TrainerNumber::from(1)).await?;
            self.choose_start_tile(&mut user, start_tile_id).await?;

            Ok(user)
        }

        async fn set_partner(
            &self,
            user: &LoggedInUser,
            pokemon_number: PokemonNumber,
        ) -> anyhow::Result<()> {
            api::set_partner_pokemon(
                &self.ctx.db_connection_pool,
                &self.ctx.player_channel,
                &self.ctx.pokemon_config,
                user.claims(),
                pokemon_number,
            )
            .await?;
            Ok(())
        }

        async fn set_trainer(
            &self,
            user: &LoggedInUser,
            trainer_number: TrainerNumber,
        ) -> anyhow::Result<()> {
            api::set_trainer_sprite(
                &self.ctx.db_connection_pool,
                &self.ctx.player_channel,
                user.claims(),
                trainer_number,
            )
            .await?;
            Ok(())
        }

        async fn choose_start_tile(
            &self,
            user: &mut LoggedInUser,
            start_tile_id: i64,
        ) -> anyhow::Result<()> {
            api::choose_start_tile(
                &self.ctx.db_connection_pool,
                user.claims().user_id(),
                start_tile_id,
            )
            .await?;
            let map_id =
                sqlx::query_scalar!("SELECT map_id FROM start_tile WHERE id = $1", start_tile_id)
                    .fetch_one(&self.ctx.db_connection_pool)
                    .await?;

            user.enter_map(map_id.into());

            Ok(())
        }

        fn set_moveable_coords(&self, map_id: MapId, coords: &[(i32, i32)]) {
            self.tile_cache.upsert_map(
                map_id,
                coords.iter().copied().map(|(x, y)| {
                    (
                        Position {
                            layer_number: 0,
                            x,
                            y,
                        },
                        MoveAbilities::all(),
                    )
                }),
            );
        }

        async fn add_warp(
            &self,
            (from_map_id, from_x, from_y): (MapId, i32, i32),
            (to_map_id, to_x, to_y): (MapId, i32, i32),
        ) -> anyhow::Result<()> {
            add_warp(
                &self.ctx.db_connection_pool,
                &self.ctx.warp_cache,
                Position {
                    x: from_x,
                    y: from_y,
                    layer_number: 0,
                },
                from_map_id,
                Position {
                    x: to_x,
                    y: to_y,
                    layer_number: 0,
                },
                to_map_id,
            )
            .await?;
            Ok(())
        }

        async fn spawn_item(
            &self,
            map_id: MapId,
            x: f32,
            y: f32,
            item: Item,
        ) -> anyhow::Result<ItemId> {
            arceus::spawn_item(
                &self.ctx.db_connection_pool,
                &self.ctx.item_channel,
                map_id,
                x,
                y,
                item,
            )
            .await
        }

        async fn spawn_encounter(
            &self,
            map_id: MapId,
            x: i32,
            y: i32,
            pokemon_number: PokemonNumber,
        ) -> anyhow::Result<EncounterId> {
            arceus::spawn_encounter(
                &self.ctx.db_connection_pool,
                &self.ctx.pokemon_channel,
                map_id,
                x,
                y,
                pokemon_number,
            )
            .await
        }

        async fn shutdown(self) {
            self.ctrl.shutdown((), Duration::from_secs(1)).await;
        }
    }

    async fn setup(
        pool: &PgPool,
    ) -> anyhow::Result<Setup<impl Stream<Item = crate::arceus::Message> + use<>>> {
        setup_with_seed(pool, 123456789).await
    }

    async fn setup_with_seed(
        pool: &PgPool,
        seed: u64,
    ) -> anyhow::Result<Setup<impl Stream<Item = crate::arceus::Message> + use<>>> {
        let pokemon_config = PokemonConfig::read()?;
        let settings = Settings::read()?;
        let auth_ctx = gardevoir::create_auth_ctx().await?;
        let shutdown_ctrl = shutdown::Ctrl::new();
        let item_channel = ItemChannel::new("item");
        let pokemon_channel = PokemonChannel::new("pokemon");
        let player_channel = PlayerChannel::new("player");
        let map_channel = MapChannel::new("map");
        let item_state = ItemState::new(pool, &item_channel, shutdown_ctrl.clone()).await;
        let encounter_state =
            EncounterState::new(pool, &pokemon_channel, shutdown_ctrl.clone()).await;
        let player_state = PlayerState::new(pool, &player_channel, shutdown_ctrl.clone()).await;
        let tile_cache = Arc::new(smeargle::TestMoveRuleProvider::default());
        let warp_cache = WarpCache::new(pool.clone(), settings.warp_cache);
        let (arceus_signal_sender, arceus_signal_receiver) = crate::arceus::SignalSender::new();

        let ctx = WebsocketCtx {
            player_channel: player_channel.clone(),
            pokemon_channel: pokemon_channel.clone(),
            map_channel: map_channel.clone(),
            item_channel: item_channel.clone(),
            db_connection_pool: pool.clone(),
            settings,
            auth_ctx,
            tile_cache: tile_cache.clone(),
            warp_cache,
            arceus_signal_sender,
            encounter_state,
            item_state,
            player_state,
            shutdown_ctrl: shutdown_ctrl.clone(),
            pokemon_config,
            rng: rng::RngProvider::from_seed(seed),
        };

        let addr = minimal_ws_server(ctx.clone()).await;

        Ok(Setup {
            addr,
            ctx,
            ctrl: shutdown_ctrl,
            arceus_signal_receiver,
            tile_cache,
        })
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct WsQueryParams {
        token: String,
        map_id: String,
    }

    async fn minimal_ws_server(ctx: WebsocketCtx) -> SocketAddr {
        let shutdown_ctrl = ctx.shutdown_ctrl.clone();

        async fn ws(
            Query(params): Query<WsQueryParams>,
            upgrade: WebSocketUpgrade,
            State(state): State<WebsocketCtx>,
        ) -> Response {
            let Ok(map_id) = params.map_id.parse::<i64>() else {
                tracing::debug!("Invalid map_id");
                return http::StatusCode::BAD_REQUEST.into_response();
            };
            let Ok(claims) = state
                .auth_ctx
                .decode_token(Some(map_id.into()), &params.token)
            else {
                tracing::debug!("Invalid token");
                return http::StatusCode::BAD_REQUEST.into_response();
            };
            upgrade
                .on_upgrade(move |socket| {
                    let (sender, receiver) = socket.split();
                    websocket(sender, receiver, claims, state)
                })
                .into_response()
        }
        let app: axum::Router = axum::Router::new().route("/ws", get(ws)).with_state(ctx);
        let listener = tokio::net::TcpListener::bind(SocketAddr::from((Ipv4Addr::UNSPECIFIED, 0)))
            .await
            .unwrap();
        let addr = listener.local_addr().unwrap();

        tokio::spawn(
            axum::serve(listener, app)
                .with_graceful_shutdown(async move {
                    shutdown_ctrl.cancelled().await;
                })
                .into_future(),
        );
        addr
    }
}
