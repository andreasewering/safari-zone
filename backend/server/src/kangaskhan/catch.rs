use std::ops::BitOr;

use rand::Rng;

use crate::{
    configuration::Pokemon,
    data::{Item, MoveAbility},
    smeargle::DirectionalMoveRules,
};

pub fn determine_catch<R>(
    rng: &mut R,
    pokemon: &Pokemon,
    item: Item,
    move_rule: Option<DirectionalMoveRules>,
) -> bool
where
    R: Rng,
{
    let ball_modifier = match item {
        Item::Pokeball => 1.0,
        Item::Superball => 1.3,
        Item::Hyperball => 1.6,
        Item::Diveball => {
            let water_tile = move_rule
                .map(|m| m.fold(BitOr::bitor).contains(MoveAbility::Swim))
                .unwrap_or(false);
            if water_tile {
                2.0
            } else {
                1.0
            }
        }
        Item::Quickball => f32::from(pokemon.move_speed) / 70.0,
    };
    let base_catch_rate = f32::from(pokemon.catch_rate);

    let roll: f32 = rng.random_range(0.0..300.0);
    tracing::debug!(%roll, %ball_modifier, %base_catch_rate, "Rolled catch limit");
    ball_modifier * base_catch_rate > roll
}
