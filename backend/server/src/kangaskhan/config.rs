use std::time::Duration;

use serde::Deserialize;

use super::warp_cache::WarpCacheSettings;

#[derive(Debug, Deserialize, Clone, Copy)]
pub struct Settings {
    /// Settings for the in-memory cache storing connections between maps
    pub warp_cache: WarpCacheSettings,

    /// How quickly does a ball throw travel from tile to tile
    pub ball_throw_velocity: f32,

    /// Time a thrown item that did not hit a Pokémon stays on the map
    pub thrown_item_despawn_time_in_ms: u64,

    /// How much of a users trace is preserved for partner pokemon purposes.
    /// Also has the effect of a partner pokemon teleporting to the player
    /// if the player is more than `partner_trace_length` steps away from their partner.
    pub partner_trace_length: u8,
}

impl Settings {
    pub fn thrown_item_despawn_time(&self) -> Duration {
        Duration::from_millis(self.thrown_item_despawn_time_in_ms)
    }

    #[cfg(test)]
    pub fn read() -> Result<Self, klink::Error> {
        use std::env;

        #[derive(Deserialize)]
        struct WrappedConfig {
            kangaskhan: Settings,
        }
        let root_path = env!("CARGO_MANIFEST_DIR");
        let wrapped_config: WrappedConfig =
            klink::read_config_from_dir(format!("{root_path}/config"))?;
        Ok(wrapped_config.kangaskhan)
    }
}
