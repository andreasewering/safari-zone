use std::{collections::HashMap, sync::Arc, time::Duration};

use fxhash::FxBuildHasher;
use moka::{future::Cache, notification::RemovalCause};
use serde::{Deserialize, Serialize};
use sqlx::PgPool;

use crate::{
    data::{MapId, Position},
    util::cache::deep_size_of_weigher,
};

#[derive(Clone)]
pub struct WarpCache {
    inner: Cache<MapId, WarpsForMap, FxBuildHasher>,
    pool: PgPool,
}

type WarpsForMap = Arc<HashMap<Position, (Position, MapId), FxBuildHasher>>;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct WarpCacheSettings {
    /// How many bytes the cache may occupy.
    /// This is neither a hard upper nor a hard lower bound.
    /// The cache should usually occupy less memory but may temporarily exceed the limit specified here.
    /// Instead, this signals that the least frequently used item should (soon) be removed from the cache.
    max_size_in_bytes: u64,

    /// How many seconds it takes for an unused cache entry to be removed
    expiry_time_in_seconds: u64,
}

impl WarpCacheSettings {
    fn expiry_time(&self) -> Duration {
        Duration::from_secs(self.expiry_time_in_seconds)
    }
}

impl WarpCache {
    pub fn new(pool: PgPool, settings: WarpCacheSettings) -> Self {
        let inner = Cache::builder()
            .max_capacity(settings.max_size_in_bytes)
            .weigher(deep_size_of_weigher)
            .eviction_listener(tracing_eviction_listener)
            .time_to_idle(settings.expiry_time())
            .build_with_hasher(FxBuildHasher::default());
        Self { inner, pool }
    }

    pub async fn is_warp(&self, map_id: MapId, position: Position) -> Option<(Position, MapId)> {
        match self.get(map_id).await {
            Err(error) => {
                tracing::error!(message = "Failed to load warps for map", %map_id, %error);
                None
            }
            Ok(warps) => warps.get(&position).copied(),
        }
    }

    pub async fn invalidate(&self, map_id: MapId) {
        self.inner.invalidate(&map_id).await;
        tracing::info!(message = "Invalidated warp cache", %map_id);
    }

    async fn get(&self, map_id: MapId) -> Result<WarpsForMap, Arc<sqlx::Error>> {
        let warps_for_map = self
            .inner
            .try_get_with(map_id, self.force_load_map(map_id))
            .await?;
        Ok(warps_for_map)
    }

    async fn force_load_map(&self, map_id: MapId) -> sqlx::Result<WarpsForMap> {
        tracing::debug!(message = "Loading warps for map", %map_id);
        struct WarpEntity {
            from_x: i32,
            from_y: i32,
            from_layer_number: i32,
            to_x: i32,
            to_y: i32,
            to_layer_number: i32,
            to_map_id: MapId,
        }
        let tiles = sqlx::query_as!(
            WarpEntity,
            r#"SELECT from_x, from_y, from_layer_number,
                      to_x, to_y, to_layer_number, to_map_id AS "to_map_id: _" 
               FROM warp WHERE from_map_id = $1"#,
            map_id as MapId
        )
        .fetch_all(&self.pool)
        .await?;
        let tile_cache = tiles
            .into_iter()
            .map(
                |WarpEntity {
                     from_x,
                     from_y,
                     from_layer_number,
                     to_x,
                     to_y,
                     to_layer_number,
                     to_map_id,
                 }| {
                    (
                        Position {
                            x: from_x,
                            y: from_y,
                            layer_number: from_layer_number,
                        },
                        (
                            Position {
                                x: to_x,
                                y: to_y,
                                layer_number: to_layer_number,
                            },
                            to_map_id,
                        ),
                    )
                },
            )
            .collect();

        tracing::info!(message = "Loaded warps for map", %map_id);

        Ok(Arc::new(tile_cache))
    }
}

fn tracing_eviction_listener<V>(map_id: Arc<MapId>, _: V, removal_cause: RemovalCause) {
    match removal_cause {
        RemovalCause::Expired => tracing::debug!(message = "Cache entry expired", %map_id),
        RemovalCause::Explicit => tracing::info!(message = "Cache entry was invalidated and removed", %map_id),
        RemovalCause::Replaced => tracing::warn!(message = "Cache entry was manually replaced. We should not do this and use the database as a single source of truth.", %map_id),
        RemovalCause::Size => tracing::warn!(message = "Cache entry was removed due to size constraints. This might mean the size constraint is set too low, or we need to spin up more servers to handle the load, or some bug prevents the cache expiry from working."),
    }
}
