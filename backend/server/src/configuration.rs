mod pokemon;
mod trainer;

use std::{path::Path, sync::Arc};

use serde::Deserialize;

use crate::{arceus, chatot, gardevoir, kangaskhan, smeargle, watchog::LogSettings};

pub use pokemon::{Pokemon, PokemonConfig};
pub use trainer::TrainerConfig;

#[derive(Debug, Deserialize, Clone)]
pub struct Settings {
    pub log: LogSettings,
    pub server: ServerSettings,
    pub database: DatabaseSettings,
    pub mail: MailSettings,
    pub gardevoir: gardevoir::Settings,
    pub pokemon: PokemonConfig,
    pub kangaskhan: kangaskhan::Settings,
    pub smeargle: smeargle::Settings,
    pub trainer: TrainerConfig,
    pub arceus: arceus::Settings,
    pub chatot: chatot::Settings,
}

impl Settings {
    pub fn new() -> Result<Self, klink::Error> {
        klink::read_config()
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct ServerSettings {
    pub grpc_port: u16,
    pub http_port: u16,
    pub public_path: Arc<Path>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct DatabaseSettings {
    pub url: Arc<str>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct MailSettings {
    pub smtp_user: Arc<str>,
    pub smtp_password: Arc<str>,
    pub smtp_host: Arc<str>,
    pub smtp_port: u16,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn load_dev_settings_works() {
        temp_env::with_vars(
            [
                (
                    "SZ_CONFIG",
                    Some(concat!(env!("CARGO_MANIFEST_DIR"), "/config")),
                ),
                ("SZ_ENV", Some("development")),
                (
                    "SZ_GARDEVOIR__PROVIDERS__GITHUB__CLIENT_SECRET",
                    Some("some client secret"),
                ),
                (
                    "SZ_ARCEUS__TRANSLATION__OPENAI_API_KEY",
                    Some("some api key"),
                ),
            ],
            || {
                Settings::new().unwrap();
            },
        );
    }

    #[test]
    fn load_prod_settings_works() {
        temp_env::with_vars(
            [
                (
                    "SZ_CONFIG",
                    Some(concat!(env!("CARGO_MANIFEST_DIR"), "/config")),
                ),
                ("SZ_GARDEVOIR__JWT__PRIVATE_KEY", Some("some private key")),
                (
                    "SZ_GARDEVOIR__PROVIDERS__GITHUB__CLIENT_SECRET",
                    Some("some client secret"),
                ),
                (
                    "SZ_ARCEUS__TRANSLATION__OPENAI_API_KEY",
                    Some("some api key"),
                ),
                ("SZ_MAIL__SMTP_PASSWORD", Some("some smtp password")),
                ("SZ_DATABASE__URL", Some("some postgres connection string")),
            ],
            || {
                Settings::new().unwrap();
            },
        );
    }
}
