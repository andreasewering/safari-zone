use std::{any::Any, collections::HashMap, error::Error, fmt::Display, pin::pin, sync::Arc};

use futures::Stream;
use sqlx::{
    PgPool,
    postgres::{PgListener, PgNotification},
};
use tokio::sync::mpsc::{self, WeakSender};
#[cfg(test)]
use tokio::sync::oneshot;

use super::mpsc_pubsub::{self, MpscPubSub};
use crate::pubsub;
use crate::shutdown;

use super::channel::Channel;

type Decoder = Arc<dyn Fn(&str) -> DecoderResult + Sync + Send>;

type DecoderResult = Result<ArcAny, Box<dyn Error>>;

type DecoderResultWithChannel<'a> = Result<(&'a str, ArcAny), Box<dyn Error>>;

type ArcAny = Arc<dyn Any + Sync + Send>;

type Decoders = HashMap<String, Decoder>;

#[derive(Clone)]
pub struct PgPubSub {
    pool: PgPool,
    publisher: MpscPubSub,
    event_sender: mpsc::Sender<PgPubSubEvent>,
}

pub enum PgPubSubEvent {
    AddSubscription {
        channel_name: String,
        decoder: Decoder,
    },
    RemoveSubscription {
        channel_name: String,
    },
    #[cfg(test)]
    QueryDecoders {
        callback: oneshot::Sender<usize>,
    },
}

impl PgPubSub {
    pub async fn publish<C: Channel>(&self, channel: &C, message: C::Message) -> sqlx::Result<()> {
        let payload = construct_payload(channel.name(), &C::encode(message));
        sqlx::query!("SELECT pg_notify($1, $2)", CHANNEL, payload)
            .execute(&self.pool)
            .await?;
        Ok(())
    }

    pub fn subscribe<C: Channel>(
        &self,
        channel: &C,
    ) -> anyhow::Result<pubsub::Subscription<C::Message>> {
        let event = PgPubSubEvent::AddSubscription {
            channel_name: channel.name().into(),
            decoder: Arc::new(|bytes| {
                let decoded = C::decode(bytes)?;
                Ok(Arc::new(decoded))
            }),
        };
        {
            let event_sender = self.event_sender.clone();
            tokio::spawn(async move { event_sender.send(event).await });
        }
        let subscription = self.publisher.subscribe_untyped(channel.name().into())?;
        let subscription = Subscription {
            inner: subscription,
            _sender: self.event_sender.downgrade(),
        };
        Ok(Box::pin(subscription))
    }

    pub async fn new(pool: &PgPool, shutdown_ctrl: shutdown::Ctrl) -> anyhow::Result<Self> {
        let pool = pool.clone();
        let mut listener = PgListener::connect_with(&pool).await?;
        let (event_sender, mut event_receiver) = mpsc::channel(10);
        let publisher = MpscPubSub::new();
        let publisher_1 = publisher.clone();
        let mut decoders: Decoders = Default::default();

        listener.listen(CHANNEL).await?;

        tokio::spawn(async move {
            fn on_notification(
                decoders: &Decoders,
                publisher: &MpscPubSub,
                notification: Result<PgNotification, sqlx::Error>,
            ) {
                match notification {
                    Ok(message) => {
                        tracing::trace!(?message, "Received postgres message");
                        if let Some(decoder_result) = decode_payload(&message, decoders) {
                            match decoder_result {
                                Err(error) => {
                                    tracing::warn!(?message, %error, "Failed to decode message on channel");
                                }
                                Ok((channel_name, decoded)) => {
                                    let result = publisher.publish_untyped(channel_name, decoded);
                                    if let Err(error) = result {
                                        tracing::warn!(%error, "Failed to publish message to MpscPubSub");
                                    } else {
                                        tracing::trace!(
                                            ?message,
                                            "Successfully distributed message"
                                        );
                                    }
                                }
                            }
                        }
                    }
                    Err(error) => {
                        tracing::error!(message = "PgPubSub lost connection to the database", %error);
                    }
                }
            }

            async fn on_event(
                _listener: &mut PgListener,
                decoders: &mut Decoders,
                event: PgPubSubEvent,
            ) {
                match event {
                    PgPubSubEvent::AddSubscription {
                        channel_name,
                        decoder,
                    } => {
                        decoders.insert(channel_name, decoder);
                    }
                    PgPubSubEvent::RemoveSubscription { channel_name } => {
                        decoders.remove(&channel_name);
                    }
                    #[cfg(test)]
                    PgPubSubEvent::QueryDecoders { callback } => {
                        callback
                            .send(decoders.len())
                            .expect("Callback channel was closed");
                    }
                }
            }

            let _prevent_shutdown = shutdown_ctrl.prevent_shutdown("pg_pubsub");
            loop {
                tokio::select! {
                    _ = shutdown_ctrl.cancelled() => {
                        tracing::info!("Gracefully shutting down pg pubsub");
                        break;
                    }
                    notification = listener.recv() => {
                        on_notification(&decoders, &publisher_1, notification)
                    }
                    event = event_receiver.recv() => {
                        match event {
                            None => {
                                tracing::warn!("PgPubSub Event channel exited early.");
                                break;
                            }
                            Some(event) => {
                                on_event(&mut listener, &mut decoders, event).await;
                            }
                        }
                    }
                }
            }
        });

        Ok(Self {
            publisher,
            event_sender,
            pool: pool.clone(),
        })
    }

    #[cfg(test)]
    async fn query_decoders_size(&self) -> anyhow::Result<usize> {
        let (callback, on_callback) = oneshot::channel();
        self.event_sender
            .send(PgPubSubEvent::QueryDecoders { callback })
            .await?;
        let decoders_size = on_callback.await?;
        Ok(decoders_size)
    }
}

struct Subscription<T> {
    _sender: WeakSender<PgPubSubEvent>,
    inner: mpsc_pubsub::Subscription<T>,
}

impl<T> Drop for Subscription<T> {
    fn drop(&mut self) {
        if !self.inner.is_exclusive_subscription() {
            return;
        }
        let Some(sender) = self._sender.upgrade() else {
            return;
        };
        if let Err(error) = sender.try_send(PgPubSubEvent::RemoveSubscription {
            channel_name: self.inner.channel_name.clone(),
        }) {
            tracing::error!(message = "Failed to cleanup subscription", %error);
        }
    }
}

impl<T: Any + Clone + Unpin> Stream for Subscription<T> {
    type Item = T;

    fn poll_next(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        let pinned = pin!(&mut self.get_mut().inner);
        pinned.poll_next(cx)
    }
}

const CHANNEL: &str = "dugtrio";

fn decode_payload<'a>(
    notification: &'a PgNotification,
    decoders: &Decoders,
) -> Option<DecoderResultWithChannel<'a>> {
    let Some((channel, payload)) = notification.payload().split_once(' ') else {
        let error = PayloadError::MissingSeperator(notification.payload().to_string());
        return Some(Err(Box::new(error)));
    };
    let decoder = decoders.get(channel)?;
    Some(decoder(payload).map(|decoded| (channel, decoded)))
}

fn construct_payload<S: Into<String>>(channel_name: S, message: &str) -> String {
    let mut payload = channel_name.into();
    if payload.contains(' ') {
        panic!("Invariant broken! Channel name should never contain the seperator char ' '!")
    }
    payload.push(' ');
    payload.push_str(message);
    payload
}

#[derive(Debug)]
enum PayloadError {
    MissingSeperator(String),
}

impl Display for PayloadError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PayloadError::MissingSeperator(str) => {
                f.write_fmt(format_args!(r#"Missing seperator " " in payload {}"#, str))
            }
        }
    }
}

impl std::error::Error for PayloadError {}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use futures::StreamExt;

    use crate::pubsub::JsonEncoding;

    use super::*;

    #[database_macros::test]
    async fn publish_subscribe_single_message(pool: PgPool) -> anyhow::Result<()> {
        let ctrl = shutdown::Ctrl::new();
        let pubsub = PgPubSub::new(&pool, ctrl.clone()).await?;
        let mut subscription = pubsub.subscribe(&TestChannel)?;
        let message = "A message".to_string();
        pubsub.publish(&TestChannel, message.clone()).await?;
        let received_message = subscription.next().await;
        assert_eq!(received_message, Some(message));

        let next_message =
            tokio::time::timeout(Duration::from_millis(100), subscription.next()).await;
        assert!(next_message.is_err());
        ctrl.shutdown((), Duration::from_millis(100)).await;
        Ok(())
    }

    #[database_macros::test]
    async fn publish_subscribe_two_messages(pool: PgPool) -> anyhow::Result<()> {
        let ctrl = shutdown::Ctrl::new();
        let pubsub = PgPubSub::new(&pool, ctrl.clone()).await?;
        let mut subscription = pubsub.subscribe(&TestChannel)?;
        let message_1 = "A message".to_string();
        let message_2 = "A message".to_string();
        pubsub.publish(&TestChannel, message_1.clone()).await?;
        pubsub.publish(&TestChannel, message_2.clone()).await?;
        let received_message = subscription.next().await;
        assert_eq!(received_message, Some(message_1));
        let received_message = subscription.next().await;
        assert_eq!(received_message, Some(message_2));
        Ok(())
    }

    #[database_macros::test]
    async fn gracefully_shuts_down(pool: PgPool) -> anyhow::Result<()> {
        let ctrl = shutdown::Ctrl::new();
        let pubsub = tokio::time::timeout(
            Duration::from_millis(100),
            PgPubSub::new(&pool, ctrl.clone()),
        )
        .await??;
        let _subscription = pubsub.subscribe(&TestChannel)?;
        let message = "A message".to_string();
        tokio::time::timeout(
            Duration::from_millis(100),
            pubsub.publish(&TestChannel, message.clone()),
        )
        .await??;
        ctrl.shutdown((), Duration::from_millis(100)).await;
        tokio::time::timeout(Duration::from_millis(100), pool.close()).await?;
        Ok(())
    }

    #[database_macros::test]
    async fn two_channels(pool: PgPool) -> anyhow::Result<()> {
        let ctrl = shutdown::Ctrl::new();
        let pubsub = PgPubSub::new(&pool, ctrl.clone()).await?;

        let chan_1 = DynamicChannel(1);
        let chan_2 = DynamicChannel(2);

        let mut subscription_1 = pubsub.subscribe(&chan_1)?;
        let mut subscription_2 = pubsub.subscribe(&chan_2)?;
        let message_1 = "A message".to_string();
        let message_2 = "A message".to_string();
        pubsub.publish(&chan_1, message_1.clone()).await?;
        pubsub.publish(&chan_2, message_2.clone()).await?;

        let received_message_1 = subscription_1.next().await;
        assert_eq!(received_message_1, Some(message_1));
        let received_message_2 = subscription_2.next().await;
        assert_eq!(received_message_2, Some(message_2));

        Ok(())
    }

    #[database_macros::test]
    async fn decoders_clean_up_does_not_run_too_early(pool: PgPool) -> anyhow::Result<()> {
        let ctrl = shutdown::Ctrl::new();
        let pubsub = PgPubSub::new(&pool, ctrl.clone()).await?;

        let mut subscription_1 = pubsub.subscribe(&TestChannel)?;
        let mut subscription_2 = pubsub.subscribe(&TestChannel)?;
        let message = "A message".to_string();
        pubsub.publish(&TestChannel, message.clone()).await?;

        let received_message_1 =
            tokio::time::timeout(Duration::from_secs(5), subscription_1.next()).await?;
        assert_eq!(received_message_1, Some(message.clone()));

        drop(subscription_1);

        let received_message_2 =
            tokio::time::timeout(Duration::from_secs(5), subscription_2.next()).await?;
        assert_eq!(received_message_2, Some(message));

        let message = "Second message".to_string();

        pubsub.publish(&TestChannel, message.clone()).await?;
        let received_message_2 =
            tokio::time::timeout(Duration::from_secs(5), subscription_2.next()).await;
        assert_eq!(received_message_2, Ok(Some(message)));

        Ok(())
    }

    #[database_macros::test]
    async fn decoders_get_cleaned_up_after_last_subscription_is_dropped(
        pool: PgPool,
    ) -> anyhow::Result<()> {
        let ctrl = shutdown::Ctrl::new();
        let pubsub = PgPubSub::new(&pool, ctrl.clone()).await?;

        let subscription_1 = pubsub.subscribe(&TestChannel)?;
        let subscription_2 = pubsub.subscribe(&TestChannel)?;
        // Wait for subscriptions to register
        tokio::time::sleep(Duration::from_millis(50)).await;
        assert_eq!(pubsub.query_decoders_size().await?, 1);

        drop(subscription_1);
        drop(subscription_2);

        assert_eq!(pubsub.query_decoders_size().await?, 0);

        Ok(())
    }

    struct TestChannel;

    impl Channel for TestChannel {
        type Message = String;

        type Encoding = JsonEncoding<String>;

        fn name(&self) -> impl Into<String> {
            "test"
        }
    }

    struct DynamicChannel(u32);

    impl Channel for DynamicChannel {
        type Message = String;

        type Encoding = JsonEncoding<String>;

        fn name(&self) -> impl Into<String> {
            self.0.to_string()
        }
    }
}
