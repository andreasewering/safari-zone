use std::any::Any;

use serde::{de::DeserializeOwned, Serialize};

pub trait Channel {
    type Message: Sized + Clone + Any + Unpin + Sync + Send;
    type Encoding: ChannelEncoding<MessageType = Self::Message>;
    fn name(&self) -> impl Into<String>;
    fn encode(msg: Self::Message) -> String {
        Self::Encoding::encode(msg)
    }
    fn decode(
        str: &str,
    ) -> Result<Self::Message, <<Self as Channel>::Encoding as ChannelEncoding>::Error> {
        Self::Encoding::decode(str)
    }
}

pub trait ChannelEncoding {
    type Error: std::error::Error + Unpin + Send + Sync + 'static;
    type MessageType;
    fn decode(str: &str) -> Result<Self::MessageType, Self::Error>;
    fn encode(msg: Self::MessageType) -> String;
}

#[derive(Debug, Clone, Default, Copy)]
pub struct JsonEncoding<T> {
    marker: std::marker::PhantomData<T>,
}

impl<T> ChannelEncoding for JsonEncoding<T>
where
    T: Serialize + DeserializeOwned + Unpin + Clone + Any,
{
    type Error = serde_json::Error;
    type MessageType = T;

    fn decode(str: &str) -> Result<T, Self::Error> {
        serde_json::from_str(str)
    }

    fn encode(msg: T) -> String {
        serde_json::to_string(&msg)
            .unwrap_or_else(|error| panic!("Failed to encode message to JSON: {error}"))
    }
}
