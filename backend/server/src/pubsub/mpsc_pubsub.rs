use futures::Stream;
use parking_lot::RwLock;
use std::{
    any::Any,
    collections::{HashMap, HashSet},
    fmt::Display,
    sync::{
        atomic::{AtomicU64, Ordering},
        Arc, Weak,
    },
};
use tokio::sync::mpsc::{error::SendError, UnboundedReceiver, UnboundedSender};

type AnySender = UnboundedSender<Arc<dyn Any + Send + Sync>>;

type AnyReceiver = UnboundedReceiver<Arc<dyn Any + Send + Sync>>;

#[derive(Clone)]
pub struct MpscPubSub(Arc<MpscPubSubInner>);

pub struct MpscPubSubInner {
    senders: RwLock<HashMap<u64, AnySender>>,
    channel_subs: RwLock<HashMap<String, HashSet<u64>>>,
    next_id: AtomicU64,
}

impl MpscPubSubInner {
    fn number_of_subs(&self, channel_name: &str) -> usize {
        let channel_subs = self.channel_subs.read();
        let Some(subs) = channel_subs.get(channel_name) else {
            return 0;
        };
        subs.len()
    }
}

impl MpscPubSub {
    pub fn new() -> Self {
        Self(
            MpscPubSubInner {
                senders: RwLock::new(HashMap::new()),
                channel_subs: RwLock::new(HashMap::new()),
                next_id: AtomicU64::new(0),
            }
            .into(),
        )
    }

    pub fn subscribe_untyped<T>(&self, channel_name: String) -> Result<Subscription<T>> {
        let MpscPubSubInner {
            senders,
            channel_subs,
            next_id,
        } = &*self.0.clone();
        let (tx, rx) = tokio::sync::mpsc::unbounded_channel();
        let mut senders = senders.write();

        let mut next = next_id.load(Ordering::Relaxed);
        while senders.contains_key(&next) {
            next = next_id.fetch_add(1, Ordering::Relaxed);
        }
        senders.insert(next, tx);
        next_id.fetch_add(1, Ordering::Acquire);
        let subscriber_id = next;
        let mut channel_subs = channel_subs.write();
        let channel_subs = channel_subs
            .entry(channel_name.clone())
            .or_insert_with(HashSet::new);
        channel_subs.insert(subscriber_id);
        tracing::debug!(
            message = "Subscribed to channel",
            channel = channel_name,
            subscriber_id
        );

        let publisher = Arc::downgrade(&self.0);
        Ok(Subscription {
            publisher,
            subscriber_id,
            channel_name,
            receiver: rx,
            phantom_type: std::marker::PhantomData,
        })
    }

    pub fn publish_untyped(
        &self,
        channel_name: &str,
        message: Arc<dyn Any + Sync + Send>,
    ) -> Result<()> {
        let MpscPubSubInner {
            senders,
            channel_subs,
            next_id: _,
        } = &*self.0.clone();
        let channel_subs = channel_subs.read();

        if let Some(sub_ids) = channel_subs.get(channel_name) {
            let senders = senders.read();
            for sub_id in sub_ids {
                if let Some(sender) = senders.get(sub_id) {
                    sender.send(message.clone())?;
                }
            }
        }
        Ok(())
    }
}

pub struct Subscription<T> {
    publisher: Weak<MpscPubSubInner>,
    pub channel_name: String,
    subscriber_id: u64,
    receiver: AnyReceiver,
    phantom_type: std::marker::PhantomData<T>,
}

impl<T> Subscription<T> {
    pub fn is_exclusive_subscription(&self) -> bool {
        let Some(publisher) = self.publisher.upgrade() else {
            return false;
        };
        let number_of_subs = publisher.number_of_subs(&self.channel_name);
        number_of_subs <= 1
    }
}

impl<T: Any + Clone + Unpin> Stream for Subscription<T> {
    type Item = T;

    fn poll_next(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        self.receiver.poll_recv(cx).map(|opt| {
            opt.and_then(|arc| {
                let any = arc.as_ref();
                match any.downcast_ref::<T>() {
                    Some(item) => Some(item.clone()),
                    None => {
                        tracing::error!(
                            "Failed to downcast any from type_id {:?}! This should not happen!",
                            any.type_id()
                        );
                        None
                    }
                }
            })
        })
    }
}

impl<T> Drop for Subscription<T> {
    fn drop(&mut self) {
        tracing::debug!(
            message = "Unsubscribing from subscription",
            subscriber_id = self.subscriber_id
        );
        if let Some(publisher) = self.publisher.upgrade() {
            let mut channel_subs = publisher.channel_subs.write();
            if let Some(subs) = channel_subs.get_mut(&self.channel_name) {
                subs.remove(&self.subscriber_id);
                if subs.is_empty() {
                    channel_subs.remove(&self.channel_name);
                }
            }
            let mut senders = publisher.senders.write();
            senders.remove(&self.subscriber_id);
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Send { error: String },
}

impl std::error::Error for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Send { error } => {
                f.write_fmt(format_args!("Sending to channel failed. {}", error))
            }
        }
    }
}

impl<T> From<SendError<T>> for Error {
    fn from(err: SendError<T>) -> Self {
        Self::Send {
            error: format!("{}", err),
        }
    }
}

#[cfg(test)]
mod tests {
    use futures::StreamExt;

    use super::*;

    #[tokio::test]
    async fn it_works_for_single_subscription() -> Result<()> {
        let publisher = MpscPubSub::new();
        let mut sub = publisher.subscribe_untyped("test".to_string())?;
        publisher.publish_untyped("test", Arc::new(5))?;
        assert_eq!(sub.next().await, Some(5));

        Ok(())
    }

    #[tokio::test]
    async fn it_works_for_multiple_messages() -> Result<()> {
        let publisher = MpscPubSub::new();
        let mut sub = publisher.subscribe_untyped("test".to_string())?;
        publisher.publish_untyped("test", Arc::new(5))?;
        publisher.publish_untyped("test", Arc::new(10))?;
        assert_eq!(sub.next().await, Some(5));
        assert_eq!(sub.next().await, Some(10));
        Ok(())
    }

    #[tokio::test]
    async fn it_works_for_multiple_subscriptions() -> Result<()> {
        let publisher = MpscPubSub::new();
        let mut sub_a = publisher.subscribe_untyped("a".to_string())?;
        let mut sub_b = publisher.subscribe_untyped("b".to_string())?;
        publisher.publish_untyped("a", Arc::new(5))?;
        publisher.publish_untyped("b", Arc::new(10))?;
        publisher.publish_untyped("a", Arc::new(15))?;
        publisher.publish_untyped("b", Arc::new(20))?;
        assert_eq!(sub_a.next().await, Some(5));
        assert_eq!(sub_a.next().await, Some(15));
        assert_eq!(sub_b.next().await, Some(10));
        assert_eq!(sub_b.next().await, Some(20));
        Ok(())
    }
}
