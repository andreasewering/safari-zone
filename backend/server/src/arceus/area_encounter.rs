// Needed because of soa_rs generated code :(
#![allow(dead_code)]

use crate::error::AppResult;
use soa_rs::{Soa, Soars};
use sqlx::PgPool;

use crate::arceus::proto;
use crate::data::{PokemonNumber, SpawnKind};

use super::area_id::AreaId;

#[derive(Debug, Clone, Copy, Soars)]
pub struct AreaEncounter {
    pub probability: f32,
    pub pokemon_number: PokemonNumber,
    pub spawn_kind: SpawnKind,
}

pub async fn get_encounters_from_db(
    area_id: AreaId,
    pool: &PgPool,
) -> sqlx::Result<Vec<AreaEncounter>> {
    sqlx::query_as!(
        AreaEncounter,
        r#"SELECT pokemon_number as "pokemon_number: _", spawn_kind as "spawn_kind: _", probability FROM area_encounters
         WHERE area_id = $1"#,
        area_id as AreaId
    )
    .fetch_all(pool)
    .await
}

pub async fn set_encounters_in_db(
    area_id: AreaId,
    encounters: Soa<AreaEncounter>,
    pool: &sqlx::PgPool,
) -> AppResult<()> {
    let mut transaction = pool.begin().await?;

    sqlx::query!(
        "DELETE FROM area_encounters WHERE area_id = $1",
        area_id as AreaId
    )
    .execute(&mut *transaction)
    .await?;

    sqlx::query!(r#"INSERT INTO area_encounters (area_id, pokemon_number, probability, spawn_kind)
            SELECT $1, pokemon_number as "pokemon_number: PokemonNumber", probability, spawn_kind 
        FROM UNNEST($2::INTEGER[], $3::REAL[], $4::SPAWN_KIND[]) as a(pokemon_number, probability, spawn_kind)"#,
            area_id as AreaId,
            encounters.pokemon_number() as &[PokemonNumber],
            encounters.probability(),
            encounters.spawn_kind() as &[SpawnKind]
        ).execute(&mut *transaction).await?;

    transaction.commit().await?;
    Ok(())
}

impl TryFrom<proto::arceus::Encounter> for AreaEncounter {
    type Error = Box<dyn std::error::Error>;
    fn try_from(value: proto::arceus::Encounter) -> Result<Self, Self::Error> {
        let spawn_kind = proto::arceus::SpawnKind::try_from(value.spawn_kind)?.into();
        let pokemon_number = PokemonNumber::try_from(value.pokemon_number)?;
        Ok(Self {
            probability: value.probability,
            pokemon_number,
            spawn_kind,
        })
    }
}
