use crate::{
    data::{MapId, SupportedLanguage, Translated},
    error::{AppResult, Context as _},
    range::IntRange,
};

use super::area_id::AreaId;

pub struct AreaToCreate {
    pub map_id: MapId,
    pub name: Translated<String>,
    pub parts: Vec<(IntRange, IntRange, IntRange)>,
}

pub async fn create_area(
    pool: &sqlx::PgPool,
    AreaToCreate {
        map_id,
        name,
        parts,
    }: AreaToCreate,
) -> AppResult<AreaId> {
    let mut transaction = pool.begin().await?;
    let row = sqlx::query!(
        r#"INSERT INTO area (map_id)
             VALUES ($1)
             RETURNING id as "id: AreaId""#,
        map_id as MapId,
    )
    .fetch_one(&mut *transaction)
    .await
    .context("Insert into area")?;

    create_area_translations(&mut *transaction, row.id, map_id, name).await?;

    create_area_parts(&mut *transaction, row.id, map_id, parts).await?;

    transaction.commit().await?;

    Ok(row.id)
}

pub async fn create_area_translations<'a, E>(
    executor: E,
    area_id: AreaId,
    map_id: MapId,
    name: Translated<String>,
) -> AppResult<()>
where
    E: sqlx::PgExecutor<'a>,
{
    let (locales, names) = name.into_array_agg();

    sqlx::query!(
        r#"INSERT INTO area_translations (area_id, map_id, locale, area_name)
            SELECT $1, $2, locale, name
            FROM UNNEST($3::LOCALE[], $4::TEXT[]) AS a(locale, name)"#,
        area_id as AreaId,
        map_id as MapId,
        Vec::from(locales) as Vec<SupportedLanguage>,
        Vec::from(names) as Vec<String>
    )
    .execute(executor)
    .await?;

    Ok(())
}

pub async fn create_area_parts<'a, E>(
    executor: E,
    area_id: AreaId,
    map_id: MapId,
    parts: Vec<(IntRange, IntRange, IntRange)>,
) -> AppResult<()>
where
    E: sqlx::PgExecutor<'a>,
{
    let mut ranges_x = Vec::with_capacity(parts.len());
    let mut ranges_y = Vec::with_capacity(parts.len());
    let mut ranges_z = Vec::with_capacity(parts.len());

    for (range_x, range_y, range_z) in parts {
        ranges_x.push(range_x);
        ranges_y.push(range_y);
        ranges_z.push(range_z);
    }

    sqlx::query!(
        r#"INSERT INTO area_part (area_id, map_id, range_x, range_y, range_z)
           SELECT $1, $2, range_x, range_y, range_z 
           FROM UNNEST($3::int4range[], $4::int4range[], $5::int4range[]) AS a(range_x, range_y, range_z)"#,
        area_id as AreaId,
        map_id as MapId,
        ranges_x as Vec<IntRange>,
        ranges_y as Vec<IntRange>,
        ranges_z as Vec<IntRange>
    )
    .execute(executor)
    .await?;

    Ok(())
}

pub struct AreaUpdate {
    pub background_music_track: Option<u32>,
}

pub async fn edit_area<'a, E>(executor: E, area_id: AreaId, update: AreaUpdate) -> AppResult<MapId>
where
    E: sqlx::PgExecutor<'a>,
{
    if update.background_music_track.is_none() {
        let row = sqlx::query!(
            r#"SELECT map_id as "map_id: MapId" FROM area WHERE id = $1"#,
            area_id as AreaId
        )
        .fetch_one(executor)
        .await?;
        return Ok(row.map_id);
    }
    let row = sqlx::query!(
        r#"UPDATE area
        SET background_music_track = COALESCE($1, background_music_track)
        WHERE id = $2
        RETURNING map_id AS "map_id: MapId""#,
        update.background_music_track.map(|u32| u32 as i32),
        area_id as AreaId
    )
    .fetch_one(executor)
    .await?;
    Ok(row.map_id)
}
