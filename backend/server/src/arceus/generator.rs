mod area;
mod area_encounter_worker;
mod area_item_worker;
mod encounter_worker;
mod event;
mod item_worker;
mod timed_instant;

use crate::arceus::generator::area_encounter_worker::start_area_encounter_worker;
use crate::arceus::generator::area_item_worker::start_area_item_worker;
use crate::channels::{ItemMessage, ItemMessageWrapper, PokemonMessage, PokemonMessageWrapper};
use crate::data::{EncounterId, MapId};
use crate::error::AppResult;
use crate::{graceful_shutdown, shutdown};
use dashmap::DashMap;
use encounter_worker::{existing_encounter_worker, get_existing_encounters_in_map};
use event::ShutdownEvent;
use fxhash::FxBuildHasher;
use item_worker::{existing_item_worker, get_existing_items_in_map};
use sqlx::PgPool;
use std::collections::{BTreeSet, HashMap};
use std::sync::Arc;
use std::time::Duration;
pub use timed_instant::TimedInstant;
use tokio::sync::mpsc;
use tokio::sync::mpsc::error::SendError;

#[cfg(test)]
pub use encounter_worker::{EncounterData, insert_encounter};
#[cfg(test)]
pub use item_worker::{ItemData, insert_item};

use self::area::fetch_area;

use super::GenerationContext;
use super::area_id::AreaId;

#[derive(Debug, Clone, PartialEq)]
pub enum Message {
    StartArea(AreaId, MapId),
    StopArea(AreaId),
    StartMap(MapId),
    StopMap(MapId),
    StopEncounter(EncounterId, MapId),
}

#[derive(Clone)]
pub struct SignalSender<K> {
    sender: tokio::sync::mpsc::Sender<Message>,
    active_maps: Arc<DashMap<MapId, BTreeSet<K>, FxBuildHasher>>,
}

impl<K: Ord + core::fmt::Debug + Copy> SignalSender<K> {
    pub async fn start_map(&self, map_id: MapId, key: K) {
        if let Some(mut active_map) = self.active_maps.get_mut(&map_id) {
            let was_inserted = active_map.insert(key);
            if was_inserted {
                tracing::debug!(%map_id, ?key, "Added new resource to already active map");
            } else {
                tracing::warn!(%map_id, ?key, "Started map from already existing resource - this may indicate that stop_map was not called.");
            }
            return;
        }
        {
            tracing::debug!(%map_id, ?key, "Map was not yet active, forwarding start to generator");
            let mut active_map = BTreeSet::new();
            active_map.insert(key);
            self.active_maps.insert(map_id, active_map);
        }

        if let Err(error) = self.sender.send(Message::StartMap(map_id)).await {
            tracing::warn!(%error, %map_id, "Failed to send map start to generator");
        }
    }

    pub async fn stop_map(&self, map_id: MapId, key: &K) {
        let removed = self.active_maps.remove_if_mut(&map_id, |_, active_map| {
            active_map.remove(key);
            active_map.is_empty()
        });
        if removed.is_none() {
            tracing::debug!(%map_id, ?key, "Map stop was intercepted, since other resources are still active on the map.");
            return;
        }
        tracing::debug!(%map_id, ?key, "The last resource was removed from the map, forwarding map stop to generator");
        if let Err(error) = self.sender.send(Message::StopMap(map_id)).await {
            tracing::warn!(%error, %map_id, "Failed to send map stop to generator");
        }
    }

    pub async fn stop_encounter(
        &self,
        encounter_id: EncounterId,
        map_id: MapId,
    ) -> Result<(), SendError<Message>> {
        if !self.active_maps.contains_key(&map_id) {
            return Ok(());
        }
        self.sender
            .send(Message::StopEncounter(encounter_id, map_id))
            .await
    }

    pub(super) async fn start_area(
        &self,
        area_id: AreaId,
        map_id: MapId,
    ) -> Result<(), SendError<Message>> {
        if !self.active_maps.contains_key(&map_id) {
            return Ok(());
        }
        self.sender.send(Message::StartArea(area_id, map_id)).await
    }

    pub(super) async fn stop_area(&self, area_id: AreaId) -> Result<(), SendError<Message>> {
        self.sender.send(Message::StopArea(area_id)).await
    }

    #[cfg(test)]
    pub fn new() -> (SignalSender<K>, impl futures::Stream<Item = Message>) {
        use tokio_stream::wrappers::ReceiverStream;

        let (sender, receiver) = tokio::sync::mpsc::channel::<Message>(10);
        let receiver_stream = ReceiverStream::new(receiver);
        let active_maps = Arc::new(DashMap::with_hasher(FxBuildHasher::default()));
        (
            SignalSender {
                sender,
                active_maps,
            },
            receiver_stream,
        )
    }
}

type ShutdownCtrlMap<K, T> = HashMap<K, shutdown::Ctrl<T>, FxBuildHasher>;

/**
 * General idea - make this lazy: encounters and items are generated based on time and we store
 * in the database when the generator was last running.
 *
 * Need:
 * - table in the database from area_id to timestamp
 * - we do not need a "running" flag since only one generator may be running for a single area
 * - a resumable function/iterator that generates events per time
 * - we get notified when a user logs in and check if a generator is running already for all areas of the given map (in-memory check)
 * - we get notified when a user/the last user logs out and stop the generator for all areas of the given map + write the current timestamp to the database
 * - if the generator is not running yet, we generate all events from $timestamp to $now.
 * - While running, we use the same function to generate new events
 * - This effectively also solves the map caching issue - we init the map cache when the generator is started for a map/all areas on a map
 *
 * How often do we generate items/encounters and how many?
 * - It should not matter how big the area is -> a bigger area should lead to a generation event more often
 * - We can calculate the average spawn rate by the ratio of items per tile and the despawn time:
 *      despawn_time / (items_per_tile * number_of_tiles)
 *
 * - Example: 100 tiles, despawn_time 15 minutes, ratio 0.6 -> Spawn Rate 15min/60 = 15sec
 * - So if we spawn each 15sec for 15min straight, we will generate 60 events
 */
pub fn start_generator<K>(
    ctx: GenerationContext,
    shutdown_ctrl: shutdown::Ctrl,
) -> SignalSender<K> {
    let (sender, mut receiver) = mpsc::channel(10);
    let active_maps = Arc::new(DashMap::with_hasher(FxBuildHasher::default()));

    tokio::spawn(async move {
        let mut area_handles: ShutdownCtrlMap<AreaId, ()> =
            HashMap::with_hasher(FxBuildHasher::default());
        let mut map_handles: ShutdownCtrlMap<MapId, ShutdownEvent> =
            HashMap::with_hasher(FxBuildHasher::default());

        let signal_loop = {
            async move {
                while let Some(message) = receiver.recv().await {
                    tracing::debug!(message = "Generator received message", content = ?message);
                    match message {
                        Message::StartArea(area_id, map_id) => {
                            let Some(map_ctrl) = map_handles.get(&map_id) else {
                                continue;
                            };
                            on_start_area(&ctx, &mut area_handles, map_ctrl.clone(), area_id).await;
                        }
                        Message::StopArea(area_id) => {
                            on_stop_area(&mut area_handles, area_id).await;
                        }
                        Message::StartMap(map_id) => {
                            on_start_map(&ctx, &mut area_handles, &mut map_handles, map_id).await;
                        }
                        Message::StopMap(map_id) => {
                            on_stop_map(&ctx, &mut area_handles, &mut map_handles, map_id).await;
                        }
                        Message::StopEncounter(encounter_id, map_id) => {
                            on_stop_encounter(&mut map_handles, encounter_id, map_id).await;
                        }
                    }
                }
            }
        };

        graceful_shutdown! {
            ctrl: shutdown_ctrl,
            name: "generator::start_generator",
            cleanup: () => {
                tracing::info!("Gracefully shut down generator.");
            },
            _ = signal_loop  => {
                tracing::warn!("Generator exited early. No further messages will be accepted. The generator should only be aborted via the shutdown controller.")
            }
        }
    });

    SignalSender {
        sender,
        active_maps,
    }
}

async fn on_start_map(
    ctx: &GenerationContext,
    area_handles: &mut ShutdownCtrlMap<AreaId, ()>,
    map_handles: &mut ShutdownCtrlMap<MapId, ShutdownEvent>,
    map_id: MapId,
) {
    metrics::gauge!("active_maps").increment(1);
    let now = TimedInstant::now();
    let (existing_encounters_result, existing_items_result, areas_result) = tokio::join!(
        get_existing_encounters_in_map(map_id, now, &ctx.db_connection_pool),
        get_existing_items_in_map(map_id, now, &ctx.db_connection_pool),
        get_area_ids_on_map(&ctx.db_connection_pool, map_id)
    );
    let map_ctrl = shutdown::Ctrl::new();
    if let Some(old_ctrl) = map_handles.insert(map_id, map_ctrl.clone()) {
        old_ctrl
            .shutdown(ShutdownEvent::AllWorkers, Duration::from_secs(1))
            .await;
    };

    match existing_encounters_result {
        Err(error) => {
            tracing::error!(%error, "Failed to get existing encounters");
        }
        Ok((despawned_encounter_ids, spawned_encounters)) => {
            for encounter_id in despawned_encounter_ids {
                let pokemon_channel = ctx.pokemon_channel.clone();
                tokio::spawn(async move {
                    pokemon_channel.send(PokemonMessageWrapper {
                        encounter_id,
                        map_id,
                        message: PokemonMessage::Remove,
                    });
                });
            }
            let ctx = ctx.clone();

            for encounter in spawned_encounters {
                let ctx = ctx.clone();
                let shutdown_ctrl = map_ctrl.clone();
                tokio::spawn(existing_encounter_worker(
                    ctx,
                    shutdown_ctrl,
                    encounter,
                    map_id,
                ));
            }
        }
    }

    match existing_items_result {
        Err(error) => {
            tracing::error!(%error, "Failed to get existing items");
        }
        Ok((despawned_item_ids, spawned_items)) => {
            for item_id in despawned_item_ids {
                let item_channel = ctx.item_channel.clone();
                tokio::spawn(async move {
                    item_channel.send(ItemMessageWrapper {
                        item_id,
                        map_id,
                        msg: ItemMessage::Remove,
                    });
                });
            }
            let ctx = ctx.clone();
            for item in spawned_items {
                let ctx = ctx.clone();
                let shutdown_ctrl = map_ctrl.clone();
                tokio::spawn(existing_item_worker(ctx, shutdown_ctrl, item, map_id));
            }
        }
    }

    match areas_result {
        Err(error) => {
            tracing::error!(%error, %map_id, "Failed to get areas for map");
        }
        Ok(area_ids) => {
            for area_id in area_ids {
                let map_ctrl = map_ctrl.clone();
                let ctx = ctx.clone();
                on_start_area(&ctx, area_handles, map_ctrl, area_id).await;
            }
        }
    }
}

async fn on_stop_map(
    ctx: &GenerationContext,
    area_handles: &mut ShutdownCtrlMap<AreaId, ()>,
    map_handles: &mut ShutdownCtrlMap<MapId, ShutdownEvent>,
    map_id: MapId,
) {
    metrics::gauge!("active_maps").decrement(1);
    match get_area_ids_on_map(&ctx.db_connection_pool, map_id).await {
        Err(error) => {
            tracing::error!(message = "Received error while getting areas for stopping generation.", %error);
        }
        Ok(area_ids) => {
            for area_id in area_ids {
                on_stop_area(area_handles, area_id).await;
            }
        }
    }

    if let Some(map_ctrl) = map_handles.remove(&map_id) {
        map_ctrl
            .shutdown(ShutdownEvent::AllWorkers, Duration::from_secs(1))
            .await
    }
}

async fn on_start_area(
    ctx: &GenerationContext,
    area_handles: &mut ShutdownCtrlMap<AreaId, ()>,
    map_ctrl: shutdown::Ctrl<ShutdownEvent>,
    area_id: AreaId,
) {
    tracing::info!(%area_id, "Starting generation for area");
    let ctx = ctx.clone();
    let area_ctrl = shutdown::Ctrl::new();
    if let Some(old_ctrl) = area_handles.insert(area_id, area_ctrl.clone()) {
        old_ctrl.shutdown((), Duration::from_secs(1)).await;
    };
    if let Err(error) = new_area_generator(ctx, area_ctrl, map_ctrl, area_id).await {
        tracing::error!(%error, %area_id, "Failed to start area generator");
    }
}

async fn on_stop_area(area_handles: &mut ShutdownCtrlMap<AreaId, ()>, area_id: AreaId) {
    tracing::info!(%area_id, "Stopping generation for area");
    if let Some(area_ctrl) = area_handles.remove(&area_id) {
        area_ctrl.shutdown((), Duration::from_secs(1)).await
    }
}

async fn on_stop_encounter(
    map_handles: &mut HashMap<
        MapId,
        shutdown::Ctrl<ShutdownEvent>,
        std::hash::BuildHasherDefault<fxhash::FxHasher>,
    >,
    encounter_id: EncounterId,
    map_id: MapId,
) {
    tracing::info!(%encounter_id, %map_id, "Stopping encounter worker");
    if let Some(map_ctrl) = map_handles.get(&map_id) {
        map_ctrl
            .shutdown(
                ShutdownEvent::JustEncounter(encounter_id),
                Duration::from_secs(1),
            )
            .await;
    }
}

async fn get_area_ids_on_map(pool: &PgPool, map_id: MapId) -> AppResult<Vec<AreaId>> {
    let area_ids = sqlx::query_scalar!(
        r#"SELECT id as "id: AreaId" FROM area WHERE map_id = $1"#,
        map_id as MapId
    )
    .fetch_all(pool)
    .await?;
    Ok(area_ids)
}

#[tracing::instrument(name = "area_generator", skip(ctx, shutdown_ctrl, map_ctrl))]
async fn new_area_generator(
    ctx: GenerationContext,
    shutdown_ctrl: shutdown::Ctrl,
    map_ctrl: shutdown::Ctrl<ShutdownEvent>,
    area_id: AreaId,
) -> anyhow::Result<()> {
    let area = fetch_area(area_id, &ctx.db_connection_pool).await?;

    {
        let ctx = ctx.clone();
        let shutdown_ctrl = shutdown_ctrl.clone();
        let map_ctrl = map_ctrl.clone();
        let area = area.clone();
        tokio::spawn(async move {
            if let Err(error) = start_area_item_worker(ctx, area, shutdown_ctrl, map_ctrl).await {
                tracing::error!(%error, "Failed to start area_item_worker");
            }
        });
    }

    let shutdown_ctrl = shutdown_ctrl.clone();
    let map_ctrl = map_ctrl.clone();
    tokio::spawn(async move {
        if let Err(error) = start_area_encounter_worker(ctx, area, shutdown_ctrl, map_ctrl).await {
            tracing::error!(%error, "Failed to start area_encounter_worker");
        }
    });

    Ok(())
}

#[cfg(test)]
mod tests {
    use anyhow::Context;
    use futures::StreamExt;
    use soa_rs::soa;
    use sqlx::PgPool;
    use std::collections::VecDeque;
    use test_helper::Setup;
    use test_helper::setup;
    use time::OffsetDateTime;
    use tokio::time::Instant;
    use tokio::time::timeout;

    use super::*;
    use crate::arceus::area_encounter::AreaEncounter;
    use crate::arceus::area_item::AreaItem;
    use crate::arceus::test_helper as arceus;
    use crate::channels::ItemMessage;
    use crate::channels::NewEncounter;

    use crate::channels::PokemonMessage;

    use crate::data::Direction;
    use crate::data::Item;
    use crate::data::MoveAbilities;
    use crate::data::MoveAbility;
    use crate::data::PokemonNumber;
    use crate::data::Position;
    use crate::data::SpawnKind;
    use crate::smeargle::test_helper as smeargle;

    #[database_macros::test]
    async fn generate_single_item_deterministically(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool, 123456789).await?;

        let map_id = smeargle::create_map(&pool, "test map").await?;
        let _area_id = arceus::AreaBuilder::new(map_id, -123, 56, -234, 67)
            .with_items(vec![
                AreaItem {
                    item: Item::Pokeball,
                    probability: 0.5,
                },
                AreaItem {
                    item: Item::Superball,
                    probability: 0.5,
                },
            ])
            .persist(&pool)
            .await?;
        setup.set_moveable_positions(map_id, &[(0, 0), (1, 0)]);

        let mut item_event_stream = setup.item_channel.receive();
        let generator = setup.start_generator();
        generator.start_map(map_id, 0).await;
        let item_event = timeout(Duration::from_secs(5), item_event_stream.next())
            .await
            .expect("No timeout");

        assert_eq!(item_event.map(|event| event.map_id), Some(map_id));
        assert!(
            matches!(
                item_event.map(|item_event| item_event.msg),
                Some(ItemMessage::New {
                    item: Item::Pokeball,
                    x: 1.0,
                    y: 0.0,
                    layer_number: 0,
                    despawn_time: _
                })
            ),
            "Item event {:?} did not match the expected event.",
            item_event
        );

        setup.shutdown().await;

        Ok(())
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn generate_and_despawn_multiple_items(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool, 1234567890123).await?;

        let map_id = smeargle::create_map(&pool, "test map").await?;
        let _area_id = arceus::AreaBuilder::new(map_id, -123, 56, -234, 67)
            .with_items(vec![
                AreaItem {
                    item: Item::Pokeball,
                    probability: 0.5,
                },
                AreaItem {
                    item: Item::Superball,
                    probability: 0.5,
                },
            ])
            .persist(&pool)
            .await?;

        let moveable_positions = [(0, 0), (1, 0)];
        setup.set_moveable_positions(map_id, &moveable_positions);

        let item_event_stream = setup.item_channel.receive();
        let generator = setup.start_generator();
        generator.start_map(map_id, 0).await;

        let timestamp_after_start = Instant::now();
        let number_of_events = 100;
        let item_events = timeout(
            Duration::from_secs(20),
            item_event_stream.take(number_of_events).collect::<Vec<_>>(),
        )
        .await
        .expect("No timeout");

        let mut current_items = VecDeque::new();
        for item_event in item_events {
            match item_event.msg {
                ItemMessage::New { .. } => {
                    current_items.push_back(item_event.item_id);
                }
                ItemMessage::Remove => {
                    let items_to_despawn: Vec<(usize, _)> = current_items
                        .iter()
                        .enumerate()
                        .take(3)
                        .filter(|(_, item_id)| *item_id == &item_event.item_id)
                        .collect();
                    if items_to_despawn.is_empty() {
                        panic!(
                            "Received Remove Message for {} when item was not spawned or not in the first 3 items: {:?}",
                            item_event.item_id, current_items
                        );
                    }
                    if items_to_despawn.len() > 1 {
                        panic!(
                            "Received Remove Message for {} but item was apparently spawned multiple times",
                            item_event.item_id
                        )
                    }
                    let (index_of_item_to_despawn, _) = items_to_despawn[0];
                    current_items.remove(index_of_item_to_despawn);
                }
            }
            assert!(
                current_items.len() < 3 * moveable_positions.len(),
                "There should not be significantly more items than moveable_positions * item per tile ratio"
            );
        }

        let passed_duration_until_100_events = Instant::now().duration_since(timestamp_after_start);
        // Why is this the expected time?
        // We expect to generate enough items such that usually 1 item per tile is spawned (due to GENERATION_SETTINGS.item_per_tile_ratio = 1)
        // So this is where ENERATION_SETTINGS.item_despawn_time_in_ms / moveable_positions.len() comes from.
        // We then multiply by number_of_events / 2, since half of the event should be spawn events (the other half being despawns).
        // This is not accurate since there will be items that are not despawned after the n events and there is randomness in play but a good approximation.
        let expected_time = Duration::from_millis(
            setup
                .generation_context
                .generation_settings
                .item_despawn_time_in_ms
                * (number_of_events as u64 / 2)
                / moveable_positions.len() as u64,
        );
        // We expect the approximation to be pretty good (factor 1.2)
        let min_expected_time = Duration::from_secs_f64(expected_time.as_secs_f64() / 1.2);
        let max_expected_time = Duration::from_secs_f64(expected_time.as_secs_f64() * 1.2);
        assert!(
            passed_duration_until_100_events > min_expected_time,
            "Expected generation of {number_of_events} events to take at least {min_expected_time:?} but it took {passed_duration_until_100_events:?} instead."
        );
        assert!(
            passed_duration_until_100_events < max_expected_time,
            "Expected generation of {number_of_events} events to take at most {:?} but it took {passed_duration_until_100_events:?} instead.",
            max_expected_time
        );

        setup.shutdown().await;

        Ok(())
    }

    #[database_macros::test]
    async fn cleanup_despawned_items(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool, 123456789).await?;

        let map_id = smeargle::create_map(&pool, "test map").await?;
        let _area_id = arceus::AreaBuilder::new(map_id, -123, 56, -234, 67)
            .with_items(vec![
                AreaItem {
                    item: Item::Pokeball,
                    probability: 0.5,
                },
                AreaItem {
                    item: Item::Superball,
                    probability: 0.5,
                },
            ])
            .persist(&pool)
            .await?;

        setup.set_moveable_positions(map_id, &[(0, 0), (1, 0)]);

        let mut item_event_stream = setup.item_channel.receive();
        let generator = setup.start_generator();
        generator.start_map(map_id, 0).await;

        let first_event = timeout(Duration::from_millis(1000), item_event_stream.next()).await;
        let generated_item_id = first_event
            .expect("Should have generated item but ran into timeout")
            .expect("Should have generated item but stream ended")
            .item_id;

        // Stop generator and wait for despawn time until starting again
        generator.stop_map(map_id, &0).await;
        tokio::time::sleep(Duration::from_millis(
            setup
                .generation_context
                .generation_settings
                .item_despawn_time_in_ms,
        ))
        .await;
        generator.start_map(map_id, 0).await;
        let second_event = timeout(Duration::from_millis(500), item_event_stream.next()).await;
        let second_event = second_event
            .expect("Expected item despawn event to be fired but ran into timeout")
            .expect("Expected item despawn event to be fired but stream closed");
        assert_eq!(second_event.msg, ItemMessage::Remove);
        assert_eq!(second_event.item_id, generated_item_id);

        setup.shutdown().await;

        Ok(())
    }

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn generate_single_encounter_deterministically(pool: PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool, 123456789).await?;
        // Avoid time travel movement during non-deterministic database connection delay to mess with us
        setup
            .generation_context
            .generation_settings
            .encounter_move_time_base_in_ms = 999999;
        setup
            .generation_context
            .pokemon_config
            .move_duration_base_in_ms = 999999;

        let map_id = smeargle::create_map(&pool, "test map").await?;
        let _area_id = arceus::AreaBuilder::new(map_id, -123, 56, -234, 67)
            .with_encounters(soa![
                AreaEncounter {
                    pokemon_number: PokemonNumber(12),
                    probability: 0.5,
                    spawn_kind: SpawnKind::Grass,
                },
                AreaEncounter {
                    pokemon_number: PokemonNumber(17),
                    probability: 0.5,
                    spawn_kind: SpawnKind::Grass,
                },
            ])
            .persist(&pool)
            .await?;

        setup.set_moveable_positions(map_id, &[(0, 0), (1, 0)]);
        setup.set_positions_to_spawn_kinds(map_id, &[(0, 0), (1, 0)], SpawnKind::Grass);

        let mut encounter_event_stream = setup.pokemon_channel.receive();
        let generator = setup.start_generator();
        generator.start_map(map_id, 0).await;

        let Some(PokemonMessage::New(NewEncounter {
            position,
            direction,
            pokemon_number,
            is_shiny,
            despawn_time,
        })) = timeout(Duration::from_millis(500), encounter_event_stream.next())
            .await
            .context("No encounter event fired")?
            .map(|wrapper| wrapper.message)
        else {
            panic!("Encounter should have been generated.")
        };
        assert_eq!(
            position,
            Position {
                x: 0,
                y: 0,
                layer_number: 0
            }
        );
        assert_eq!(direction, Direction::U);
        assert_eq!(pokemon_number, PokemonNumber(17));
        assert!(is_shiny);
        let now = OffsetDateTime::now_utc();
        let configured_despawn_duration = setup
            .generation_context
            .generation_settings
            .encounter_despawn_time();
        assert!(despawn_time > now);
        assert!(
            despawn_time < now + configured_despawn_duration,
            "Despawn time {despawn_time} was later than expected.
The time now is {now} and the configured despawn duration {configured_despawn_duration:?}."
        );

        setup.shutdown().await;
        Ok(())
    }

    #[database_macros::test]
    async fn generate_and_despawn_multiple_encounters(pool: PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool, 123456789012).await?;

        let map_id = smeargle::create_map(&pool, "test map").await?;
        let _area_id = arceus::AreaBuilder::new(map_id, -123, 56, -234, 67)
            .with_encounters(soa![
                AreaEncounter {
                    pokemon_number: PokemonNumber(12),
                    probability: 0.5,
                    spawn_kind: SpawnKind::Grass,
                },
                AreaEncounter {
                    pokemon_number: PokemonNumber(17),
                    probability: 0.5,
                    spawn_kind: SpawnKind::Grass,
                },
                AreaEncounter {
                    pokemon_number: PokemonNumber(24),
                    probability: 12.0,
                    spawn_kind: SpawnKind::Water,
                },
                AreaEncounter {
                    pokemon_number: PokemonNumber(99),
                    probability: 24.0,
                    spawn_kind: SpawnKind::Water,
                },
            ])
            .persist(&pool)
            .await?;

        setup.set_positions_to_allow_move_abilities(
            map_id,
            &[(0, 0), (1, 0), (0, 1), (1, 1)],
            MoveAbilities::empty() | MoveAbility::Walk,
        );
        setup.set_positions_to_allow_move_abilities(
            map_id,
            &[(2, 0), (3, 0), (2, 1), (3, 1)],
            MoveAbilities::empty() | MoveAbility::Walk,
        );
        let grass_positions = [(0, 0), (1, 0)];
        let water_positions = [(2, 1), (3, 0)];
        setup.set_positions_to_spawn_kinds(map_id, &grass_positions, SpawnKind::Grass);
        setup.set_positions_to_spawn_kinds(map_id, &water_positions, SpawnKind::Water);

        let mut encounter_event_stream = setup.pokemon_channel.receive();
        let generator = setup.start_generator();
        generator.start_map(map_id, 0).await;

        let timestamp_after_start = Instant::now();
        let number_of_spawn_events = 50;
        let mut current_no_of_spawns = 0;
        let mut pokemon_events = Vec::new();
        while let Some(msg) = encounter_event_stream.next().await {
            if let PokemonMessage::New(_) = msg.message {
                current_no_of_spawns += 1;
            }
            pokemon_events.push(msg);
            if current_no_of_spawns >= number_of_spawn_events {
                break;
            }
        }

        let mut current_encounters = VecDeque::new();
        for pokemon_event in pokemon_events {
            match pokemon_event.message {
                PokemonMessage::New(new_encounter) => {
                    current_encounters
                        .push_back((pokemon_event.encounter_id, new_encounter.position));
                }
                PokemonMessage::Remove => {
                    let encounters_to_despawn: Vec<(usize, _)> = current_encounters
                        .iter()
                        .enumerate()
                        .take(3)
                        .filter(|(_, (encounter_id, _))| {
                            *encounter_id == pokemon_event.encounter_id
                        })
                        .collect();
                    if encounters_to_despawn.is_empty() {
                        panic!(
                            "Received Remove Message for {} when encounter was not spawned or not in the first 3 encounters: {:?}",
                            pokemon_event.encounter_id, current_encounters
                        );
                    }
                    if encounters_to_despawn.len() > 1 {
                        panic!(
                            "Received Remove Message for {} but encounter was apparently spawned multiple times",
                            pokemon_event.encounter_id
                        );
                    }
                    let (index_of_encounter_to_despawn, _) = encounters_to_despawn[0];
                    current_encounters.remove(index_of_encounter_to_despawn);
                }
                PokemonMessage::Move {
                    position,
                    direction,
                } => {
                    let affected_encounter = current_encounters
                        .iter_mut()
                        .find(|(encounter_id, _)| encounter_id == &pokemon_event.encounter_id);
                    let Some((_, prev_pos)) = affected_encounter else {
                        panic!(
                            "A move tried to move an encounter with id {} that was never spawned or already despawned. Current encounters: {current_encounters:?}",
                            pokemon_event.encounter_id
                        );
                    };
                    assert_eq!(prev_pos.move_in_dir(direction), position);
                    *prev_pos = position;
                }
                PokemonMessage::Caught { by: _ } => {
                    panic!("Generator should not send Caught message.");
                }
                PokemonMessage::BrokeOut { from: _ } => {
                    panic!("Generator should not send BrokeOut message.");
                }
            }
            assert!(
                current_encounters.len() < 3 * (grass_positions.len() + water_positions.len()),
                "There should not be significantly more encounters than moveable_positions * item per tile ratio"
            );
        }

        let passed_duration_until_nth_encounter =
            Instant::now().duration_since(timestamp_after_start);
        // Why is this the expected time?
        // We expect to generate enough encounters such that usually 1 encounter per tile with spawn kind is spawned (due to GENERATION_SETTINGS.encounter_per_tile_ratio = 1)
        // So this is where GENERATION_SETTINGS.encounter_despawn_time_in_ms / (grass_positions.len() + water_positions.len()) comes from.
        // We then multiply by number_of_spawn_events.
        let expected_time = Duration::from_millis(
            setup
                .generation_context
                .generation_settings
                .encounter_despawn_time_in_ms
                * (number_of_spawn_events as u64)
                / (grass_positions.len() + water_positions.len()) as u64,
        );
        // We expect the approximation to be pretty good (factor 1.2)
        let min_expected_time = Duration::from_secs_f64(expected_time.as_secs_f64() / 1.2);
        let max_expected_time = Duration::from_secs_f64(expected_time.as_secs_f64() * 1.2);
        assert!(
            passed_duration_until_nth_encounter > min_expected_time,
            "Expected generation of {number_of_spawn_events} events to take at least {min_expected_time:?} but it took {passed_duration_until_nth_encounter:?} instead."
        );
        assert!(
            passed_duration_until_nth_encounter < max_expected_time,
            "Expected generation of {number_of_spawn_events} events to take at most {:?} but it took {passed_duration_until_nth_encounter:?} instead.",
            max_expected_time
        );

        setup.shutdown().await;
        Ok(())
    }

    trait StartGenerator {
        fn start_generator(&self) -> SignalSender<i32>;
    }

    impl StartGenerator for Setup {
        fn start_generator(&self) -> SignalSender<i32> {
            start_generator(self.generation_context.clone(), self.shutdown_ctrl.clone())
        }
    }
}

#[cfg(test)]
mod test_helper {
    use std::{sync::Arc, time::Duration};

    use sqlx::PgPool;

    use crate::{
        arceus::{GenerationContext, Settings},
        channels::{ItemChannel, PokemonChannel},
        configuration::{self, PokemonConfig},
        data::{MapId, MoveAbilities, MoveAbility, Position, SpawnKind},
        rng::RngProvider,
        shutdown,
    };

    use crate::smeargle::test_helper as smeargle;

    fn generation_settings() -> Settings {
        let mut settings = Settings::read().expect("Arceus config could not be read");
        settings.encounter_per_tile_ratio = 1.0;
        settings.item_per_tile_ratio = 1.0;
        settings.encounter_despawn_time_in_ms = 1800;
        settings.item_despawn_time_in_ms = 400;
        settings.shiny_rate = 0.5;
        settings.encounter_move_time_base_in_ms = 4000;
        settings
    }

    pub struct Setup {
        pub item_channel: ItemChannel,
        pub pokemon_channel: PokemonChannel,
        pub shutdown_ctrl: shutdown::Ctrl,
        tile_cache: Arc<(
            smeargle::TestMoveRuleProvider,
            smeargle::TestSpawnKindProvider,
        )>,
        pub generation_context: GenerationContext,
    }

    pub async fn setup(pool: &PgPool, seed: u64) -> anyhow::Result<Setup> {
        let pokemon_channel = crate::channels::PokemonChannel::new("pokemon");
        let item_channel = crate::channels::ItemChannel::new("item");
        let shutdown_ctrl = shutdown::Ctrl::new();
        let mut pokemon_config: PokemonConfig = configuration::PokemonConfig::read()?;
        pokemon_config.move_duration_base_in_ms = 2000;

        let move_rule_provider = smeargle::TestMoveRuleProvider::default();
        let spawn_kind_provider = smeargle::TestSpawnKindProvider::default();
        let tile_cache = Arc::new((move_rule_provider, spawn_kind_provider));
        let generation_context = GenerationContext {
            db_connection_pool: pool.clone(),
            pokemon_channel: pokemon_channel.clone(),
            item_channel: item_channel.clone(),
            tile_cache: tile_cache.clone(),
            generation_settings: generation_settings(),
            pokemon_config,
            rng_provider: RngProvider::from_seed(seed),
        };
        Ok(Setup {
            item_channel,
            pokemon_channel,
            shutdown_ctrl,
            tile_cache,
            generation_context,
        })
    }

    impl Setup {
        pub fn set_moveable_positions(&self, map_id: MapId, coords: &[(i32, i32)]) {
            self.set_positions_to_allow_move_abilities(
                map_id,
                coords,
                MoveAbilities::empty() | MoveAbility::Walk,
            )
        }

        pub fn set_positions_to_allow_move_abilities(
            &self,
            map_id: MapId,
            coords: &[(i32, i32)],
            move_rule: MoveAbilities,
        ) {
            self.init_empty_map(map_id);
            self.tile_cache.0.upsert_map(
                map_id,
                coords.iter().map(|(x, y)| {
                    (
                        Position {
                            x: *x,
                            y: *y,
                            layer_number: 0,
                        },
                        move_rule,
                    )
                }),
            );
        }

        pub fn set_positions_to_spawn_kinds(
            &self,
            map_id: MapId,
            coords: &[(i32, i32)],
            spawn_kind: SpawnKind,
        ) {
            self.init_empty_map(map_id);
            self.tile_cache.1.upsert_map(
                map_id,
                coords.iter().map(|(x, y)| {
                    (
                        Position {
                            x: *x,
                            y: *y,
                            layer_number: 0,
                        },
                        spawn_kind,
                    )
                }),
            )
        }

        pub fn init_empty_map(&self, map_id: MapId) {
            self.tile_cache.0.upsert_map(map_id, []);
            self.tile_cache.1.upsert_map(map_id, []);
        }

        pub async fn shutdown(self) {
            let Setup {
                item_channel,
                pokemon_channel,
                shutdown_ctrl: shutdown_controller,
                tile_cache,
                generation_context,
            } = self;
            drop(item_channel);
            drop(pokemon_channel);
            drop(tile_cache);
            drop(generation_context);
            shutdown_controller
                .shutdown((), Duration::from_millis(200))
                .await;
        }
    }
}
