use std::{borrow::Cow, collections::BTreeMap, num::ParseIntError, sync::Arc, time::Duration};

use serde::Deserialize;

#[derive(Debug, Deserialize, Clone)]
pub struct Settings {
    pub audio: AudioSettings,
    pub translation: TranslationSettings,
    /// How many encounters per tile there should be on average,
    /// assuming no intervention by players.
    pub encounter_per_tile_ratio: f32,

    /// How many items per tile there should be on average,
    /// assuming no intervention by players.
    pub item_per_tile_ratio: f32,

    /// How many milliseconds an encounter should be visible.
    pub encounter_despawn_time_in_ms: u64,

    /// How many milliseconds an item should be visible.
    pub item_despawn_time_in_ms: u64,

    /// The time between two Pokémon movements is calculated based on this value,
    /// the move frequency, and the move speed of the given Pokémon.
    pub encounter_move_time_base_in_ms: u64,

    /// How many Pokémon should be shinies.
    /// This is expected to be a value between 0 (no shinies at all) and 1 (every Pokémon is shiny)
    pub shiny_rate: f64,
}

#[derive(Debug, Clone)]
pub struct AudioSettings {
    pub tracks: BTreeMap<u16, AudioTrack>,
    pub events: AudioEvents,
}

#[derive(Debug, Clone, Deserialize)]
pub struct TranslationSettings {
    pub endpoint_url: Arc<str>,
    pub model: Arc<str>,
    pub max_tokens: u32,
    pub temperature: f32,
    pub openai_api_key: Arc<str>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct AudioTrack {
    pub filename: Arc<str>,
    pub start: Option<f32>,
    pub end: Option<f32>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct AudioEvents {
    pub pokemon_obtained: u16,
}

impl Settings {
    #[cfg(test)]
    pub fn read() -> Result<Self, klink::Error> {
        use std::env;

        #[derive(Deserialize)]
        struct WrappedConfig {
            arceus: Settings,
        }
        let wrapped_config: WrappedConfig = temp_env::with_vars(
            [("SZ_ARCEUS__TRANSLATION__OPENAI_API_KEY", Some("test-api-key"))],
            || {
                let root_path = env!("CARGO_MANIFEST_DIR");
                klink::read_config_from_dir(format!("{root_path}/config"))
            },
        )?;
        Ok(wrapped_config.arceus)
    }

    pub fn encounter_despawn_time(&self) -> Duration {
        Duration::from_millis(self.encounter_despawn_time_in_ms)
    }

    pub fn encounter_spawn_frequency(&self, number_of_tiles: u32) -> Duration {
        Duration::from_millis(
            (self.encounter_despawn_time_in_ms as f64
                / (f64::from(self.encounter_per_tile_ratio) * f64::from(number_of_tiles)))
            .ceil() as u64,
        )
    }

    pub fn item_despawn_time(&self) -> Duration {
        Duration::from_millis(self.item_despawn_time_in_ms)
    }

    pub fn item_spawn_frequency(&self, number_of_tiles: u32) -> Duration {
        if number_of_tiles == 0 {
            return Duration::from_millis(self.item_despawn_time_in_ms);
        }
        Duration::from_millis(
            (self.item_despawn_time_in_ms as f64
                / (f64::from(self.item_per_tile_ratio) * f64::from(number_of_tiles)))
            .ceil() as u64,
        )
    }

    pub fn encounter_move_time(&self, move_frequency: u8) -> Duration {
        Duration::from_millis(
            (self.encounter_move_time_base_in_ms as f64 / f64::from(move_frequency)).ceil() as u64,
        )
    }
}

impl<'de> Deserialize<'de> for AudioSettings {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let TomlAudioSettings { tracks, events } = TomlAudioSettings::deserialize(deserializer)?;
        let tracks = tracks
            .into_iter()
            .map(|(track_number, track)| Ok((track_number.parse::<u16>()?, track)))
            .collect::<Result<BTreeMap<u16, AudioTrack>, ParseIntError>>()
            .map_err(serde::de::Error::custom)?;
        let AudioEvents { pokemon_obtained } = events;
        if !tracks.contains_key(&pokemon_obtained) {
            return Err(serde::de::Error::custom(
                "pokemon_obtained event does not have a corresponding audio track.",
            ));
        }
        Ok(AudioSettings { tracks, events })
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct TomlAudioSettings<'a> {
    #[serde(borrow)]
    pub tracks: BTreeMap<Cow<'a, str>, AudioTrack>,
    pub events: AudioEvents,
}
