use sqlx::PgPool;

use crate::data::Item;
use crate::error::AppResult;

use super::area_id::AreaId;
use super::proto::arceus;
use crate::data::proto;

#[derive(Debug, Clone, Copy)]
pub struct AreaItem {
    pub item: Item,
    pub probability: f32,
}

pub async fn get_items_from_db(area_id: AreaId, pool: &PgPool) -> sqlx::Result<Vec<AreaItem>> {
    sqlx::query_as!(
        AreaItem,
        r#"SELECT item as "item: _", probability FROM area_items
         WHERE area_id = $1
         ORDER BY item"#,
        area_id as AreaId
    )
    .fetch_all(pool)
    .await
}

pub async fn set_items_from_db(
    area_id: AreaId,
    items: Vec<AreaItem>,
    pool: &PgPool,
) -> AppResult<()> {
    let mut area_ids = Vec::with_capacity(items.len());
    let mut items_: Vec<Item> = Vec::with_capacity(items.len());
    let mut probabilities = Vec::with_capacity(items.len());

    for item in items {
        area_ids.push(area_id);
        items_.push(item.item);
        probabilities.push(item.probability);
    }
    let mut transaction = pool.begin().await?;

    sqlx::query!(
        "DELETE FROM area_items WHERE area_id = $1",
        area_id as AreaId
    )
    .execute(&mut *transaction)
    .await?;

    sqlx::query!(
        "INSERT INTO area_items (area_id, item, probability)
            SELECT a_id, item, probability 
        FROM UNNEST($1::BIGINT[], $2::SAFARI_ITEM[], $3::REAL[]) as a(a_id, item, probability)",
        area_ids as Vec<AreaId>,
        items_ as Vec<Item>,
        &probabilities,
    )
    .execute(&mut *transaction)
    .await?;

    transaction.commit().await?;
    Ok(())
}

impl TryFrom<arceus::ItemConfig> for AreaItem {
    type Error = prost::UnknownEnumValue;
    fn try_from(value: arceus::ItemConfig) -> Result<Self, Self::Error> {
        let item = proto::Item::try_from(value.item)?.into();
        Ok(Self {
            item,
            probability: value.probability,
        })
    }
}
