use std::fmt::Display;

use serde::{Deserialize, Serialize};
use sqlx::Postgres;

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct AreaId(i64);

impl Display for AreaId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl From<i64> for AreaId {
    fn from(value: i64) -> Self {
        AreaId(value)
    }
}

impl From<AreaId> for i64 {
    fn from(AreaId(i64): AreaId) -> Self {
        i64
    }
}

impl sqlx::Type<Postgres> for AreaId {
    fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
        <i64 as sqlx::Type<Postgres>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, Postgres> for AreaId {
    fn encode_by_ref(
        &self,
        buf: &mut <Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <i64 as sqlx::Encode<Postgres>>::encode(self.0, buf)
    }
}

impl<'r> sqlx::Decode<'r, Postgres> for AreaId {
    fn decode(
        value: <Postgres as sqlx::Database>::ValueRef<'r>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let i64 = <i64 as sqlx::Decode<Postgres>>::decode(value)?;
        Ok(AreaId(i64))
    }
}

impl sqlx::postgres::PgHasArrayType for AreaId {
    fn array_type_info() -> sqlx::postgres::PgTypeInfo {
        <i64 as sqlx::postgres::PgHasArrayType>::array_type_info()
    }
}
