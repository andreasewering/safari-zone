use std::sync::Arc;

use crate::{
    channels::{ItemChannel, PokemonChannel},
    configuration::PokemonConfig,
    rng::RngProvider,
    smeargle::{MoveRuleProvider, SpawnKindProvider, TileCache},
};

use sqlx::PgPool;

use super::config::Settings;

#[derive(Clone)]
pub struct GenerationContext {
    pub pokemon_channel: PokemonChannel,
    pub item_channel: ItemChannel,
    pub tile_cache: Arc<dyn TileDataProvider>,
    pub db_connection_pool: PgPool,
    pub generation_settings: Settings,
    pub pokemon_config: PokemonConfig,
    pub rng_provider: RngProvider,
}

pub trait TileDataProvider: MoveRuleProvider + SpawnKindProvider + Sync + Send {}

impl TileDataProvider for TileCache {}

#[cfg(test)]
use crate::smeargle::test_helper::{TestMoveRuleProvider, TestSpawnKindProvider};
#[cfg(test)]
impl TileDataProvider for (TestMoveRuleProvider, TestSpawnKindProvider) {}
