use std::{collections::BTreeMap, time::Duration};

use anyhow::{bail, Context as _};
use rand_distr::{Bernoulli, Distribution as _, Normal};
use time::OffsetDateTime;

use super::{area::Area, event::ShutdownEvent};
use crate::{
    arceus::{
        area_encounter::{self, AreaEncounter},
        area_id::AreaId,
        generator::{
            encounter_worker::{encounter_worker, EncounterData},
            timed_instant::{IterUntilProgress, TimedInstant},
        },
        GenerationContext,
    },
    data::{Direction, PokemonNumber, Position, SpawnKind},
    graceful_shutdown, rng, shutdown,
};
use rand::{seq::IndexedRandom as _, Rng as _};

#[derive(Debug, Clone)]
pub(super) struct GeneratorMetadata {
    pub last_running: OffsetDateTime,
    pub rng: rng::FastRng,
}

struct AreaEncounterWorker {
    ctx: GenerationContext,
    rng: rng::FastRng,
    encounters: Encounters,
    area: Area,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum AreaEncounterWorkerEventType {
    NewEncounter {
        pokemon_number: PokemonNumber,
        position: Position,
        direction: Direction,
        is_shiny: bool,
    },
}

type Encounters = BTreeMap<SpawnKind, Vec<(f32, PokemonNumber)>>;

#[tracing::instrument(name = "area_encounter_worker", skip_all, fields(
    area_id = %area.area_id,
    map_id = %area.map_id
))]
pub async fn start_area_encounter_worker(
    ctx: GenerationContext,
    area: Area,
    shutdown_ctrl: shutdown::Ctrl,
    map_ctrl: shutdown::Ctrl<ShutdownEvent>,
) -> anyhow::Result<()> {
    let start_of_area_encounter_worker = TimedInstant::now();

    let area_id = area.area_id;
    let map_id = area.map_id;

    let area_encounters =
        area_encounter::get_encounters_from_db(area.area_id, &ctx.db_connection_pool).await;

    let area_encounters = match area_encounters {
        Err(error) => {
            return Err(error.into());
        }
        Ok(area_encounters) => {
            if area_encounters.is_empty() {
                tracing::info!(message = "No encounters configured for area. Encounter generation stopped immediately.", area_id = %area.area_id);
                return Ok(());
            }
            area_encounters
        }
    };
    let encounters = encounters_to_spawn_kind_map(area_encounters);

    let (generator_metadata_result, spawn_kinds_result) = tokio::join!(
        load_generator_metadata(area_id, &ctx),
        ctx.tile_cache.spawn_kinds(map_id)
    );
    let GeneratorMetadata { rng, last_running } = generator_metadata_result?;
    let spawn_kinds = spawn_kinds_result?;

    let mut worker = AreaEncounterWorker {
        ctx: ctx.clone(),
        rng: rng.clone(),
        area,
        encounters,
    };

    // The generator starts lazily to avoid work when no player is playing, so we "time-travel" back to
    // the last time the generator was running.
    // Since everything has a despawn time we can even optimize this and travel to
    // the maximum between the last running time and (now - despawn_time).
    // Everything further back in time would have already despawned anyways.
    // In the following scenario, we can skip the time between last_running and (now - despawn_time)
    // because any generated encounters would have despawned anyways.
    // We time-travel-generate the interval [(now - despawn_time) .. now]
    //
    //         now - despawn_time
    //                v
    // ----------|----|=========|---------|--------------- ...
    //           ^              ^         ^
    //      last_running       now    now + despawn time
    //
    // In this scenario, the last_running marker is between (now - despawn_time) and now.
    // Thus, encounters in the interval [(now - despawn_time) .. last_running] have already been generated.
    // We time-travel generate the interval [last_running .. now]
    //
    //         now - despawn_time
    //                v
    // ---------------|----|=====|---------|--------------- ...
    //                     ^     ^         ^
    //             last_running now    now + despawn time
    //
    // We want to make use of our `TimedInstant` abstraction to avoid despawn times in the past.
    // To do so, we realize that adding `despawn_time` to the above interval makes the interval start at or after `now`.
    // The end of that interval is always `now + despawn_time`.
    // The start is `now` in the first case and `last_running + despawn_time` in the second case.
    let now = TimedInstant::now();
    let first_time_travel_despawn_time = start_of_area_encounter_worker
        .for_future_time(last_running + ctx.generation_settings.encounter_despawn_time())
        .unwrap_or(start_of_area_encounter_worker);
    let max_time_travel_despawn_time = now + ctx.generation_settings.encounter_despawn_time();
    if max_time_travel_despawn_time < first_time_travel_despawn_time {
        bail!("Time travel encounter generation failed since {first_time_travel_despawn_time:?} < {max_time_travel_despawn_time:?}");
    }

    let mut time_travel_despawn_time_iter = first_time_travel_despawn_time.iter_until(
        || worker.next_sync(&spawn_kinds),
        max_time_travel_despawn_time,
    );

    let generator_start_time = last_running
        .max(start_of_area_encounter_worker - ctx.generation_settings.encounter_despawn_time());
    let overshoot_duration = loop {
        let (event, despawn_time) = match time_travel_despawn_time_iter.next() {
            IterUntilProgress::Done { overshoot_duration } => {
                break overshoot_duration;
            }
            IterUntilProgress::Next(event, despawn_time) => (event, despawn_time),
        };
        if let Some(AreaEncounterWorkerEventType::NewEncounter {
            pokemon_number,
            position,
            direction,
            is_shiny,
        }) = event
        {
            let ctx = ctx.clone();
            let map_ctrl = map_ctrl.clone();

            tokio::spawn(encounter_worker(
                ctx,
                map_ctrl,
                EncounterData {
                    pokemon_number,
                    map_id,
                    position,
                    direction,
                    is_shiny,
                    despawn_time,
                },
                rng.split(),
                generator_start_time,
            ));
        }
    };

    let main_loop = async {
        let mut current_async_event_time = start_of_area_encounter_worker + overshoot_duration;
        tracing::debug!(next = %OffsetDateTime::from(current_async_event_time), "Started event loop");
        loop {
            let (opt_event_type, time_of_event) =
                current_async_event_time.advance_with(worker.next()).await;
            tracing::debug!(next = %OffsetDateTime::from(current_async_event_time), event = ?opt_event_type, "Area encounter worker event");

            if let Some(AreaEncounterWorkerEventType::NewEncounter {
                pokemon_number,
                position,
                direction,
                is_shiny,
            }) = opt_event_type
            {
                let ctx = ctx.clone();
                let despawn_time = time_of_event + ctx.generation_settings.encounter_despawn_time();
                let map_ctrl = map_ctrl.clone();
                tokio::spawn(encounter_worker(
                    ctx,
                    map_ctrl,
                    EncounterData {
                        pokemon_number,
                        map_id,
                        position,
                        direction,
                        is_shiny,
                        despawn_time,
                    },
                    rng.split(),
                    time_of_event.into(),
                ));
            }
        }
    };
    graceful_shutdown! {
        ctrl: shutdown_ctrl,
        name: "area_encounter_worker::start_area_encounter_worker",
        cleanup: () => {
            tracing::info!(message = "Area encounter generator is shutting down gracefully...", %area_id);
            let metadata = GeneratorMetadata { last_running: OffsetDateTime::now_utc(), rng: worker.rng };
            if let Err(error) = set_generator_metadata(&ctx.db_connection_pool, area_id, metadata).await {
                tracing::error!(message = "Failed to save area encounter generator metadata", %area_id, %error);
            }
            tracing::info!(message = "Area encounter generator was shutdown gracefully", %area_id);
        },
        _ = main_loop => {
            tracing::error!("Area encounter generator loop exited early somehow. It should be terminated by using the shutdown controller.");
        },
    }

    Ok(())
}

impl AreaEncounterWorker {
    async fn next(&mut self) -> (Option<AreaEncounterWorkerEventType>, Duration) {
        let spawn_kinds = match self.ctx.tile_cache.spawn_kinds(self.area.map_id).await {
            Ok(kinds) => kinds,
            Err(error) => {
                tracing::error!(message = "Failed to load map spawn kinds", map_id = %self.area.map_id, %error);
                return (None, Duration::from_secs(5));
            }
        };
        self.next_sync(&spawn_kinds)
    }

    fn next_sync(
        &mut self,
        spawn_kinds: &[(Position, SpawnKind)],
    ) -> (Option<AreaEncounterWorkerEventType>, Duration) {
        tracing::debug!("next area_encounter_worker");
        let mut spawn_kind_map: BTreeMap<SpawnKind, Vec<Position>> = BTreeMap::new();
        let mut position_count = 0;
        spawn_kinds
            .iter()
            .filter(|(position, _)| self.area.contains(position))
            .map(|(position, spawn_kind)| (spawn_kind, position))
            .for_each(|(key, val)| {
                spawn_kind_map.entry(*key).or_default().push(*val);
                position_count += 1;
            });

        if position_count == 0 {
            // Unlikely to happen. This would mean someone configured encounters for an area
            // and then changed the tiles within the area such that no spawning tiles are inside any more.
            return (None, Duration::from_secs(60));
        }
        let mean_encounter_spawn_frequency = self
            .ctx
            .generation_settings
            .encounter_spawn_frequency(position_count)
            .as_secs_f64();
        let encounter_spawn_frequency = Normal::new(
                mean_encounter_spawn_frequency,
                mean_encounter_spawn_frequency / 4.0,
        )
        .expect(
            "Building normal distribution out of configured encounter_spawn_frequency_in_seconds failed",
        )
        .map(|secs| Duration::from_secs_f64(secs.max(0.001)));

        let next_event = encounter_spawn_frequency.sample(&mut self.rng);
        let spawn_kind = match spawn_kind_map
            .iter()
            .map(|(spawn_kind, positions)| (spawn_kind, positions.len()))
            .collect::<Vec<_>>()
            .choose_weighted(&mut self.rng, |(_, number_of_positions)| {
                *number_of_positions
            }) {
            Err(error) => {
                tracing::error!(message  = "Invalid weight configuration for determining spawn kind of next encounter", %error);
                return (None, next_event);
            }
            Ok((spawn_kind, _)) => *spawn_kind,
        };

        let encounters_of_spawn_kind = match self.encounters.get(spawn_kind) {
            None => {
                return (None, next_event);
            }
            Some(encounters) => encounters,
        };
        let pokemon_number = match encounters_of_spawn_kind
            .choose_weighted(&mut self.rng, |(probability, _)| *probability)
        {
            Ok((_, pokemon_number)) => *pokemon_number,
            Err(error) => {
                tracing::error!(message = "Invalid encounter weight configuration", %error, encounters = ?encounters_of_spawn_kind, ?spawn_kind);
                return (None, next_event);
            }
        };

        let is_shiny = Bernoulli::new(self.ctx.generation_settings.shiny_rate)
            .expect("Building bernoulli distribution out of configured shiny_rate failed")
            .sample(&mut self.rng);
        let direction: Direction = self.rng.sample(rand_distr::StandardUniform);
        let event_type = match spawn_kind_map
            .get(spawn_kind)
            .and_then(|positions| positions.choose(&mut self.rng))
        {
            None => {
                tracing::warn!(message = "No possible position to place encounter for area", area_id = %self.area.area_id, ?spawn_kind);
                None
            }
            Some(position) => Some(AreaEncounterWorkerEventType::NewEncounter {
                pokemon_number,
                position: *position,
                direction,
                is_shiny,
            }),
        };

        (event_type, next_event)
    }
}

async fn load_generator_metadata(
    area_id: AreaId,
    ctx: &GenerationContext,
) -> anyhow::Result<GeneratorMetadata> {
    if let Some(metadata) = sqlx::query_as!(
        GeneratorMetadata,
        "SELECT last_running, rng_state as rng FROM area_encounter_generator_metadata WHERE area_id = $1",
        area_id as AreaId,
    )
    .fetch_optional(&ctx.db_connection_pool)
    .await
    .context("Fetching generator_metadata from the database")?
    {
        return Ok(metadata);
    }

    let rng = ctx.rng_provider.next();
    let metadata = sqlx::query_as!(
        GeneratorMetadata,
        r#"INSERT INTO area_encounter_generator_metadata 
        (area_id, last_running, rng_state)
        VALUES ($1, NOW(), $2)
        RETURNING last_running, rng_state as rng"#,
        area_id as AreaId,
        rng as rng::FastRng,
    )
    .fetch_one(&ctx.db_connection_pool)
    .await
    .context("Creating generator_metadata")?;

    Ok(metadata)
}

pub(super) async fn set_generator_metadata(
    pool: &sqlx::PgPool,
    area_id: AreaId,
    metadata: GeneratorMetadata,
) -> anyhow::Result<()> {
    sqlx::query!(
        "INSERT INTO area_encounter_generator_metadata (area_id, last_running, rng_state)
        VALUES ($1, $2, $3)
        ON CONFLICT (area_id) DO UPDATE
        SET last_running = EXCLUDED.last_running,
            rng_state = EXCLUDED.rng_state",
        area_id as AreaId,
        metadata.last_running,
        metadata.rng as rng::FastRng
    )
    .execute(pool)
    .await?;
    Ok(())
}

fn encounters_to_spawn_kind_map(encounters: Vec<AreaEncounter>) -> Encounters {
    let mut encounters_map: Encounters = BTreeMap::new();

    for AreaEncounter {
        spawn_kind,
        probability,
        pokemon_number,
    } in encounters
    {
        encounters_map
            .entry(spawn_kind)
            .or_default()
            .push((probability, pokemon_number));
    }

    encounters_map
}
