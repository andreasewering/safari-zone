use crate::data::EncounterId;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ShutdownEvent {
    JustEncounter(EncounterId),
    AllWorkers,
}
