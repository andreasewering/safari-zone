use std::sync::Arc;

use crate::{
    arceus::area_id::AreaId,
    data::{MapId, Position},
    range::IntRange,
};

#[derive(Debug, Clone)]
pub struct Area {
    pub parts: Arc<[AreaPart]>,
    pub map_id: MapId,
    pub area_id: AreaId,
}

impl Area {
    pub fn contains(&self, position: &Position) -> bool {
        self.parts.iter().any(|part| part.contains(position))
    }
}

#[derive(Debug, Clone, Copy)]
pub struct AreaPart {
    range_x: IntRange,
    range_y: IntRange,
    range_z: IntRange,
}

impl AreaPart {
    fn contains(&self, position: &Position) -> bool {
        self.range_x.includes(position.x)
            && self.range_y.includes(position.y)
            && self.range_z.includes(position.layer_number)
    }
}

struct AreaEntity {
    id: AreaId,
    map_id: MapId,
    ranges_x: Option<Vec<IntRange>>,
    ranges_y: Option<Vec<IntRange>>,
    ranges_z: Option<Vec<IntRange>>,
}

pub async fn fetch_area(area_id: AreaId, pool: &sqlx::PgPool) -> sqlx::Result<Area> {
    let area_entity = sqlx::query_as!(
        AreaEntity,
        r#"SELECT area.id as "id: _", area.map_id as "map_id: _",
                array_agg(range_x) as "ranges_x: _",
                array_agg(range_y) as "ranges_y: _",
                array_agg(range_z) as "ranges_z: _" 
            FROM area
         JOIN area_part ON area.id = area_part.area_id
         WHERE area.id = $1
         GROUP BY area.id"#,
        area_id as AreaId,
    )
    .fetch_one(pool)
    .await?;

    Ok(area_entity_to_area(area_entity))
}

fn area_entity_to_area(entity: AreaEntity) -> Area {
    let parts = match (entity.ranges_x, entity.ranges_y, entity.ranges_z) {
        (Some(x_ranges), Some(y_ranges), Some(z_ranges)) => {
            itertools::multizip((x_ranges, y_ranges, z_ranges))
                .map(|(range_x, range_y, range_z)| AreaPart {
                    range_x,
                    range_y,
                    range_z,
                })
                .collect()
        }
        _ => Vec::new(),
    };
    Area {
        parts: parts.into(),
        map_id: entity.map_id,
        area_id: entity.id,
    }
}
