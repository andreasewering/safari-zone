use time::OffsetDateTime;

use crate::{
    arceus::GenerationContext,
    channels::{ItemMessage, ItemMessageWrapper},
    data::ItemId,
    error::AppResult,
    graceful_shutdown, shutdown,
};

use crate::data::{Item, MapId};

use super::{event::ShutdownEvent, timed_instant::TimedInstant};

pub struct ItemData {
    pub item: Item,
    pub map_id: MapId,
    pub x: f32,
    pub y: f32,
    pub layer_number: i32,
    pub despawn_time: TimedInstant,
}

/// exposed for tests
pub async fn insert_item(
    pool: &sqlx::PgPool,
    ItemData {
        item,
        map_id,
        x,
        y,
        layer_number,
        despawn_time,
    }: &ItemData,
) -> AppResult<ItemId> {
    let inserted_item_id = sqlx::query_scalar!(
        r#"INSERT INTO item 
                    (item, map_id, x, y, layer_number, despawn_time)
                 VALUES ($1, $2, $3, $4, $5, $6)
                 RETURNING id as "id: ItemId""#,
        *item as Item,
        *map_id as MapId,
        x,
        y,
        layer_number,
        OffsetDateTime::from(*despawn_time)
    )
    .fetch_one(pool)
    .await?;
    Ok(inserted_item_id)
}

pub async fn item_worker(
    ctx: GenerationContext,
    shutdown_ctrl: shutdown::Ctrl<ShutdownEvent>,
    item_data: ItemData,
) {
    let inserted_item_id = match insert_item(&ctx.db_connection_pool, &item_data).await {
        Err(error) => {
            tracing::error!(message = "Failed to insert spawned item", %error);
            return;
        }
        Ok(id) => id,
    };
    existing_item_worker_internal(ctx, shutdown_ctrl, inserted_item_id, item_data).await
}

pub async fn existing_item_worker(
    ctx: GenerationContext,
    shutdown_ctrl: shutdown::Ctrl<ShutdownEvent>,
    spawned_item: SpawnedItem,
    map_id: MapId,
) {
    existing_item_worker_internal(
        ctx,
        shutdown_ctrl,
        spawned_item.id,
        ItemData {
            item: spawned_item.item,
            map_id,
            x: spawned_item.x,
            y: spawned_item.y,
            layer_number: spawned_item.layer_number,
            despawn_time: spawned_item.despawn_time,
        },
    )
    .await;
}

#[tracing::instrument(name = "item_worker", skip(ctx, shutdown_ctrl))]
async fn existing_item_worker_internal(
    ctx: GenerationContext,
    shutdown_ctrl: shutdown::Ctrl<ShutdownEvent>,
    item_id: ItemId,
    ItemData {
        item,
        map_id,
        x,
        y,
        layer_number,
        despawn_time,
    }: ItemData,
) {
    ctx.item_channel.send(ItemMessageWrapper {
        item_id,
        map_id,
        msg: ItemMessage::New {
            item,
            x,
            y,
            layer_number,
            despawn_time: despawn_time.into(),
        },
    });
    let on_despawn = async move {
        tracing::debug!(message = "Despawning item");
        if let Err(error) = sqlx::query!("DELETE FROM item WHERE id = $1", item_id as ItemId)
            .execute(&ctx.db_connection_pool)
            .await
        {
            tracing::error!(message = "Failed to delete item after despawn.", %error);
        }
        ctx.item_channel.send(ItemMessageWrapper {
            item_id,
            map_id,
            msg: ItemMessage::Remove,
        });
    };
    let shutdown_filter = |event: &ShutdownEvent| match event {
        ShutdownEvent::JustEncounter(_) => false,
        ShutdownEvent::AllWorkers => true,
    };

    graceful_shutdown! {
        ctrl: shutdown_ctrl,
        filter: shutdown_filter,
        name: format!("item_{item_id}"),
        cleanup: _ => {
            tracing::debug!(message = "Gracefully shut down item worker");
        },
        _ = despawn_time.sleep_until() => {
            on_despawn.await;
        }
    }
}

#[derive(Debug)]
pub struct SpawnedItem {
    id: ItemId,
    item: Item,
    x: f32,
    y: f32,
    layer_number: i32,
    despawn_time: TimedInstant,
}

pub async fn get_existing_items_in_map(
    map_id: MapId,
    now: TimedInstant,
    pool: &sqlx::PgPool,
) -> AppResult<(Vec<ItemId>, Vec<SpawnedItem>)> {
    let despawned_item_ids: Vec<ItemId> = sqlx::query!(
        r#"DELETE FROM item WHERE item.map_id = $1 AND item.despawn_time < $2 RETURNING id as "id: ItemId""#,
        map_id as MapId,
        OffsetDateTime::from(now)
    )
    .fetch_all(pool)
    .await?
    .into_iter()
    .map(|item| item.id)
    .collect();
    tracing::info!(
        message = "Deleted despawned items",
        amount = despawned_item_ids.len()
    );

    let items = sqlx::query!(
        r#"SELECT id as "id: ItemId", item as "item: Item", x, y, layer_number, despawn_time FROM item WHERE item.map_id = $1"#, map_id as MapId
    )
    .fetch_all(pool)
    .await?.into_iter().map(|item| {
        let despawn_time = now.for_future_time(item.despawn_time)?;
        Ok(SpawnedItem {
            id: item.id,
            item: item.item,
            x: item.x,
            y: item.y,
            layer_number: item.layer_number, despawn_time
        })
    }).collect::<AppResult<Vec<_>>>()?;
    Ok((despawned_item_ids, items))
}
