use std::time::Duration;

use rand::seq::IndexedRandom as _;
use rand_distr::{Distribution, Normal};
use time::OffsetDateTime;

use crate::{
    arceus::{
        area_id::AreaId,
        area_item::{self, AreaItem},
        generator::{
            item_worker::{item_worker, ItemData},
            timed_instant::{IterUntilProgress, TimedInstant},
        },
        GenerationContext,
    },
    data::{Item, MoveAbilities, MoveAbility, Position},
    error::AppResult,
    graceful_shutdown, rng, shutdown,
};

use super::{area::Area, event::ShutdownEvent};
use anyhow::{bail, Context as _};

struct AreaItemWorker {
    ctx: GenerationContext,
    items: Vec<AreaItem>,
    pub rng: rng::FastRng,
    area: Area,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum AreaItemWorkerEventType {
    NewItem { item: Item, position: Position },
}

#[tracing::instrument(name = "area_item_worker", skip_all, fields(
    area_id = %area.area_id,
    map_id = %area.map_id
))]
pub async fn start_area_item_worker(
    ctx: GenerationContext,
    area: Area,
    shutdown_ctrl: shutdown::Ctrl,
    map_ctrl: shutdown::Ctrl<ShutdownEvent>,
) -> anyhow::Result<()> {
    let start_of_area_item_worker = TimedInstant::now();
    let area_items = area_item::get_items_from_db(area.area_id, &ctx.db_connection_pool).await;

    let area_items = match area_items {
        Err(error) => {
            return Err(error.into());
        }
        Ok(area_items) => {
            if area_items.is_empty() {
                tracing::info!(message = "No items configured for area. Item generation stopped immediately.", area_id = %area.area_id);
                return Ok(());
            }
            area_items
        }
    };

    let GeneratorMetadata { rng, last_running } =
        load_generator_metadata(area.area_id, &ctx).await?;
    let area_id = area.area_id;
    let map_id = area.map_id;
    let mut worker = AreaItemWorker {
        ctx: ctx.clone(),
        rng,
        area,
        items: area_items,
    };
    let possible_positions_in_area = worker.possible_positions_in_area().await?;

    // The generator starts lazily to avoid work when no player is playing, so we "time-travel" back to
    // the last time the generator was running.
    // Since everything has a despawn time we can even optimize this and travel to
    // the maximum between the last running time and (now - despawn_time).
    // Everything further back in time would have already despawned anyways.
    let now = TimedInstant::now();
    let first_time_travel_despawn_time = start_of_area_item_worker
        .for_future_time(last_running + ctx.generation_settings.item_despawn_time())
        .unwrap_or(start_of_area_item_worker);
    let max_time_travel_despawn_time = now + ctx.generation_settings.item_despawn_time();
    if max_time_travel_despawn_time < first_time_travel_despawn_time {
        bail!("Time travel item generation failed since {first_time_travel_despawn_time:?} < {max_time_travel_despawn_time:?}");
    }

    let mut time_travel_despawn_time_iter = first_time_travel_despawn_time.iter_until(
        || worker.next_sync(&possible_positions_in_area),
        max_time_travel_despawn_time,
    );

    let overshoot_duration = loop {
        let (event, despawn_time) = match time_travel_despawn_time_iter.next() {
            IterUntilProgress::Done { overshoot_duration } => {
                break overshoot_duration;
            }
            IterUntilProgress::Next(event, despawn_time) => (event, despawn_time),
        };
        if let Some(AreaItemWorkerEventType::NewItem { item, position }) = event {
            let ctx = ctx.clone();
            let map_ctrl = map_ctrl.clone();
            tokio::spawn(item_worker(
                ctx,
                map_ctrl,
                ItemData {
                    item,
                    map_id,
                    x: position.x as f32,
                    y: position.y as f32,
                    layer_number: position.layer_number,
                    despawn_time,
                },
            ));
        }
    };

    let main_loop = async {
        let mut current_async_event_time = start_of_area_item_worker + overshoot_duration;
        tracing::info!(next = %OffsetDateTime::from(current_async_event_time), "Area item generator started event loop");
        loop {
            let (opt_event_type, time_of_event) =
                current_async_event_time.advance_with(worker.next()).await;
            tracing::debug!(next = %OffsetDateTime::from(current_async_event_time), event = ?opt_event_type, "Area item worker event");

            if let Some(AreaItemWorkerEventType::NewItem { item, position }) = opt_event_type {
                tracing::debug!(message = "Generating new item", ?item, ?position);
                let ctx = ctx.clone();
                let despawn_time = time_of_event + ctx.generation_settings.item_despawn_time();
                let map_ctrl = map_ctrl.clone();
                tokio::spawn(item_worker(
                    ctx,
                    map_ctrl,
                    ItemData {
                        item,
                        map_id,
                        x: position.x as f32,
                        y: position.y as f32,
                        layer_number: position.layer_number,
                        despawn_time,
                    },
                ));
            }
        }
    };
    graceful_shutdown! {
        ctrl: shutdown_ctrl,
        name: "area_encounter_worker::start_area_item_worker",
        cleanup: () => {
            tracing::info!(message = "Area item generator is shutting down gracefully...", %area_id);
            let metadata = GeneratorMetadata { last_running: OffsetDateTime::now_utc(), rng: worker.rng };
            if let Err(error) = set_generator_metadata(&ctx.db_connection_pool, area_id, metadata).await {
                tracing::error!(message = "Failed to save item generator metadata", area_id = %area_id, %error);
            }
            tracing::info!(message = "Area item generator was shutdown gracefully", area_id = %area_id);
        },
        _ = main_loop => {
            tracing::error!("Area item generator loop exited early somehow. It should be terminated by using the shutdown controller.");
        },
    }
    Ok(())
}

#[derive(Debug, Clone)]
pub(super) struct GeneratorMetadata {
    pub last_running: OffsetDateTime,
    pub rng: rng::FastRng,
}

async fn load_generator_metadata(
    area_id: AreaId,
    ctx: &GenerationContext,
) -> anyhow::Result<GeneratorMetadata> {
    if let Some(metadata) = sqlx::query_as!(
        GeneratorMetadata,
        "SELECT last_running, rng_state as rng FROM area_item_generator_metadata WHERE area_id = $1",
        area_id as AreaId,
    )
    .fetch_optional(&ctx.db_connection_pool)
    .await
    .context("Fetching generator_metadata from the database")?
    {
        return Ok(metadata);
    }

    let rng = ctx.rng_provider.next();

    let metadata = sqlx::query_as!(
        GeneratorMetadata,
        r#"INSERT INTO area_item_generator_metadata 
        (area_id, last_running, rng_state)
        VALUES ($1, NOW(), $2)
        RETURNING last_running, rng_state as rng"#,
        area_id as AreaId,
        rng as rng::FastRng,
    )
    .fetch_one(&ctx.db_connection_pool)
    .await
    .context("Creating generator_metadata")?;

    Ok(metadata)
}

pub(super) async fn set_generator_metadata(
    pool: &sqlx::PgPool,
    area_id: AreaId,
    metadata: GeneratorMetadata,
) -> anyhow::Result<()> {
    sqlx::query!(
        "INSERT INTO area_item_generator_metadata (area_id, last_running, rng_state)
        VALUES ($1, $2, $3)
        ON CONFLICT (area_id) DO UPDATE
        SET last_running = EXCLUDED.last_running,
            rng_state = EXCLUDED.rng_state",
        area_id as AreaId,
        metadata.last_running,
        metadata.rng as rng::FastRng
    )
    .execute(pool)
    .await?;
    Ok(())
}

impl AreaItemWorker {
    async fn possible_positions_in_area(&self) -> AppResult<Vec<Position>> {
        let possible_positions_on_map: Vec<Position> = self
            .ctx
            .tile_cache
            .get_allowed(self.area.map_id, MoveAbilities::empty() | MoveAbility::Walk)
            .await?;
        let possible_positions_in_area: Vec<Position> = possible_positions_on_map
            .into_iter()
            .filter(|position| self.area.contains(position))
            .collect();
        Ok(possible_positions_in_area)
    }

    async fn next(&mut self) -> (Option<AreaItemWorkerEventType>, Duration) {
        match self.possible_positions_in_area().await {
            Ok(possible_positions_in_area) => self.next_sync(&possible_positions_in_area),
            Err(error) => {
                tracing::error!(message = "Failed to load map move rules", map_id = %self.area.map_id, %error);
                (None, Duration::from_secs(5))
            }
        }
    }

    fn next_sync(
        &mut self,
        possible_positions_in_area: &[Position],
    ) -> (Option<AreaItemWorkerEventType>, Duration) {
        tracing::debug!("next area_item_worker");
        let mean_item_spawn_frequency = self
            .ctx
            .generation_settings
            .item_spawn_frequency(possible_positions_in_area.len() as u32)
            .as_secs_f64();
        let item_spawn_frequency = Normal::new(
            mean_item_spawn_frequency,
            mean_item_spawn_frequency / 4.0,
        )
        .expect(
            "Building normal distribution out of configured item_spawn_frequency_in_seconds failed",
        )
        .map(|secs| Duration::from_secs_f64(secs.max(0.001)));

        let next_event = item_spawn_frequency.sample(&mut self.rng);
        let item = match self
            .items
            .choose_weighted(&mut self.rng, |area_item| area_item.probability)
        {
            Ok(item) => item.item,
            Err(error) => {
                tracing::error!(message = "Invalid item weight configuration", %error, items = ?self.items);
                return (None, next_event);
            }
        };

        let event_type = match possible_positions_in_area.choose(&mut self.rng) {
            None => {
                tracing::warn!(message = "No possible position to place item for area", area_id = %self.area.area_id);
                None
            }
            Some(position) => Some(AreaItemWorkerEventType::NewItem {
                item,
                position: *position,
            }),
        };

        (event_type, next_event)
    }
}
