use std::{
    future::Future,
    ops::{Add, AddAssign, Sub},
    time::Duration,
};

use time::OffsetDateTime;
use tokio::time::Instant;

use crate::error::{AppError, AppResult};

/// A combination of `Instant` and `OffsetDateTime` with the invariant that both
/// represent the same time. This is useful because in some cases we need both
/// `OffsetDateTime` (to serialize a Time to our database) and `Instant` (for tokio's `sleep_until`).
#[derive(Debug, Clone, Copy, Eq)]
pub struct TimedInstant {
    instant: Instant,
    offset_date_time: OffsetDateTime,
}

impl TimedInstant {
    pub fn now() -> Self {
        let instant = Instant::now();
        let offset_date_time = OffsetDateTime::now_utc();
        Self {
            instant,
            offset_date_time,
        }
    }

    /// Tries to find an equivalent `Instant` for a given `OffsetDateTime`.
    /// If the `OffsetDateTime` is in the future (or present) from the perspective of `self`,
    /// it returns a TimedInstant consisting of the given `OffsetDateTime` and the equivalent `Instant`.
    /// Otherwise, an Error is returned.
    pub fn for_future_time(self, future_time: OffsetDateTime) -> AppResult<TimedInstant> {
        match future_time - self {
            Some(duration) => Ok(self + duration),
            None => AppError::inconsistency(format!(
                "Time {future_time} was not in the future relative to {}",
                self.offset_date_time
            ))
            .into(),
        }
    }

    /// Iterate with a given `next` Function that generates an event and a delay duration
    /// until a time-based limit has been reached. This returns an iterator which does not implement the
    /// `Iterator` trait, since the `None` branch transmits an important information:
    /// The duration by which the given limit was overshot, e.g. if the delay duration.
    ///
    /// Example:
    /// - next_fn returns a constant delay of 2 seconds
    /// - limit is 5 seconds into the future of self
    /// - Then I will get three `IterUntilProgress::Next` events
    ///   - self
    ///   - self + 2 seconds
    ///   - self + 4 seconds
    /// - And then the iterator will send a `IterUntilProgress::Done` event with an overshoot duration of 1 second
    pub fn iter_until<F, T>(self, next_fn: F, limit: TimedInstant) -> IterUntil<F>
    where
        F: FnMut() -> (T, Duration),
    {
        IterUntil {
            time: self,
            next_fn,
            limit,
        }
    }

    /// Poll a given future, and advance self for the specified duration.
    /// Before polling, we `sleep_until` self is reached.
    /// This has the significant advantage over a `sleep` for the specified duration
    /// of not delaying the timer after multiple iterations.
    /// Instead, even if polling the given future takes a while,
    /// the duration between events should be consistent/according to the returned duration.
    pub async fn advance_with<Fut, T>(&mut self, next: Fut) -> (T, Self)
    where
        Fut: Future<Output = (T, Duration)>,
    {
        self.sleep_until().await;

        let (t, duration) = next.await;
        self.add_assign(duration);
        (t, *self)
    }

    pub fn sleep_until(self) -> impl Future<Output = ()> {
        tokio::time::sleep_until(self.instant)
    }
}

pub struct IterUntil<F> {
    time: TimedInstant,
    next_fn: F,
    limit: TimedInstant,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum IterUntilProgress<T> {
    Done { overshoot_duration: Duration },
    Next(T, TimedInstant),
}

impl<F, T> IterUntil<F>
where
    F: FnMut() -> (T, Duration),
    T: std::fmt::Debug,
{
    pub fn next(&mut self) -> IterUntilProgress<T> {
        if let Some(overshoot_duration) = self.time - self.limit {
            return IterUntilProgress::Done { overshoot_duration };
        };
        let (t, duration) = (self.next_fn)();
        let progress = IterUntilProgress::Next(t, self.time);
        self.time += duration;
        progress
    }
}

impl PartialEq for TimedInstant {
    fn eq(&self, other: &Self) -> bool {
        self.instant == other.instant
    }
}

impl PartialOrd for TimedInstant {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.instant.partial_cmp(&other.instant)
    }
}

impl From<TimedInstant> for OffsetDateTime {
    fn from(value: TimedInstant) -> Self {
        value.offset_date_time
    }
}

impl Add<Duration> for TimedInstant {
    type Output = TimedInstant;

    fn add(self, rhs: Duration) -> Self::Output {
        Self {
            instant: self.instant + rhs,
            offset_date_time: self.offset_date_time + rhs,
        }
    }
}

impl AddAssign<Duration> for TimedInstant {
    fn add_assign(&mut self, rhs: Duration) {
        self.instant += rhs;
        self.offset_date_time += rhs;
    }
}

impl Sub<TimedInstant> for OffsetDateTime {
    type Output = Option<Duration>;

    fn sub(self, rhs: TimedInstant) -> Self::Output {
        (self - rhs.offset_date_time).try_into().ok()
    }
}

impl Sub<TimedInstant> for TimedInstant {
    type Output = Option<Duration>;

    fn sub(self, rhs: TimedInstant) -> Self::Output {
        (self.offset_date_time - rhs.offset_date_time)
            .try_into()
            .ok()
    }
}

impl Sub<Duration> for TimedInstant {
    type Output = OffsetDateTime;

    fn sub(self, rhs: Duration) -> Self::Output {
        self.offset_date_time - rhs
    }
}

impl Sub<OffsetDateTime> for TimedInstant {
    type Output = Option<Duration>;

    fn sub(self, rhs: OffsetDateTime) -> Self::Output {
        (self.offset_date_time - rhs).try_into().ok()
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use crate::arceus::generator::timed_instant::IterUntilProgress;

    use super::TimedInstant;

    #[test]
    fn present_is_considered_a_future_time() {
        let now = TimedInstant::now();
        let future = now
            .for_future_time(now.offset_date_time)
            .expect("Now is a future time");
        assert_eq!(now, future);
    }

    #[test]
    fn substracting_time_from_itself_is_zero_duration() {
        let now = TimedInstant::now();
        assert_eq!(now - now, Some(Duration::default()));
    }

    #[test]
    fn substracting_time_from_other_time() {
        let now = TimedInstant::now();
        let future = now + Duration::from_millis(1);
        assert_eq!(now - future, None);
        assert_eq!(future - now, Some(Duration::from_millis(1)));
    }

    #[test]
    fn iter_until_consistent_duration() {
        let now = TimedInstant::now();
        let mut n = 0;
        let mut iter = now.iter_until(
            || {
                let m = n;
                n += 1;
                (m, Duration::from_millis(2))
            },
            now + Duration::from_millis(5),
        );
        assert_eq!(iter.next(), IterUntilProgress::Next(0, now));
        assert_eq!(
            iter.next(),
            IterUntilProgress::Next(1, now + Duration::from_millis(2))
        );
        assert_eq!(
            iter.next(),
            IterUntilProgress::Next(2, now + Duration::from_millis(4))
        );
        assert_eq!(
            iter.next(),
            IterUntilProgress::Done {
                overshoot_duration: Duration::from_millis(1)
            }
        );
    }

    #[tokio::test]
    async fn advance_with_example() {
        let now = TimedInstant::now();
        let example_future = || async {
            tokio::time::sleep(Duration::from_millis(100)).await;
            ((), Duration::from_millis(200))
        };
        let mut current = now;
        current.advance_with(example_future()).await;
        current.advance_with(example_future()).await;
        current.advance_with(example_future()).await;

        let future = TimedInstant::now();
        assert!(future > now + Duration::from_millis(500));
        assert!(future < now + Duration::from_millis(600));
    }
}
