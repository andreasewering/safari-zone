use std::time::Duration;

use rand::seq::IndexedRandom as _;
use rand_distr::{Distribution as _, Normal};
use sqlx::PgPool;
use time::OffsetDateTime;

use crate::{
    arceus::{GenerationContext, generator::timed_instant::IterUntilProgress},
    channels::{NewEncounter, PokemonMessage, PokemonMessageWrapper},
    configuration,
    data::{Direction, EncounterId, MapId, PokemonNumber, Position},
    error::AppResult,
    graceful_shutdown, rng, shutdown,
    smeargle::MapMoveRuleProvider,
};

use super::{event::ShutdownEvent, timed_instant::TimedInstant};

#[derive(Debug, Clone, Copy)]
pub struct EncounterData {
    pub pokemon_number: PokemonNumber,
    pub map_id: MapId,
    pub position: Position,
    pub direction: Direction,
    pub is_shiny: bool,
    pub despawn_time: TimedInstant,
}

struct EncounterWorker {
    ctx: GenerationContext,
    encounter_data: EncounterData,
    encounter_id: EncounterId,
    rng: rng::FastRng,
}

#[derive(Debug)]
enum EncounterWorkerEventType {
    Move {
        direction: Direction,
        position: Position,
    },
}

pub async fn encounter_worker(
    ctx: GenerationContext,
    shutdown_ctrl: shutdown::Ctrl<ShutdownEvent>,
    encounter_data: EncounterData,
    rng: rng::FastRng,
    last_running: OffsetDateTime,
) {
    let inserted_encounter_id = match insert_encounter(
        encounter_data,
        rng.clone(),
        last_running,
        &ctx.db_connection_pool,
    )
    .await
    {
        Err(error) => {
            tracing::error!(message = "Failed to insert spawned encounter", %error);
            return;
        }
        Ok(inserted_encounter) => inserted_encounter,
    };

    existing_encounter_worker_internal(
        ctx,
        shutdown_ctrl,
        inserted_encounter_id,
        encounter_data,
        rng,
        last_running,
    )
    .await
}

pub struct SpawnedEncounter {
    id: EncounterId,
    pokemon_number: PokemonNumber,
    x: i32,
    y: i32,
    layer_number: i32,
    direction: Direction,
    is_shiny: bool,
    despawn_time: TimedInstant,
    rng: rng::FastRng,
    last_running: OffsetDateTime,
}

pub async fn get_existing_encounters_in_map(
    map_id: MapId,
    now: TimedInstant,
    pool: &sqlx::PgPool,
) -> AppResult<(Vec<EncounterId>, Vec<SpawnedEncounter>)> {
    let despawned_encounter_ids: Vec<EncounterId> = sqlx::query!(
        r#"DELETE FROM encounter WHERE encounter.map_id = $1 AND encounter.despawn_time < $2
           RETURNING id as "id: EncounterId""#,
        map_id as MapId,
        OffsetDateTime::from(now)
    )
    .fetch_all(pool)
    .await?
    .into_iter()
    .map(|item| item.id)
    .collect();
    tracing::info!(
        message = "Deleted despawned encounters",
        amount = despawned_encounter_ids.len()
    );

    let encounters = sqlx::query!(
        r#"SELECT encounter.id as "id: EncounterId", pokemon_number as "pokemon_number: PokemonNumber", x, y, layer_number,
                  direction as "direction: Direction", is_shiny, despawn_time,
                  rng_state as "rng: rng::FastRng", last_running
           FROM encounter
           JOIN encounter_generator_metadata ON encounter.id = encounter_generator_metadata.encounter_id
           WHERE encounter.map_id = $1"#,
        map_id as MapId
    )
    .fetch_all(pool)
    .await?;
    let encounters = encounters
        .into_iter()
        .map(|encounter| {
            let despawn_time = now.for_future_time(encounter.despawn_time)?;
            let spawned_encounter = SpawnedEncounter {
                id: encounter.id,
                pokemon_number: encounter.pokemon_number,
                x: encounter.x,
                y: encounter.y,
                layer_number: encounter.layer_number,
                direction: encounter.direction,
                is_shiny: encounter.is_shiny,
                despawn_time,
                rng: encounter.rng,
                last_running: encounter.last_running,
            };
            Ok(spawned_encounter)
        })
        .collect::<AppResult<_>>()?;
    Ok((despawned_encounter_ids, encounters))
}

pub async fn existing_encounter_worker(
    ctx: GenerationContext,
    shutdown_monitor: shutdown::Ctrl<ShutdownEvent>,
    encounter: SpawnedEncounter,
    map_id: MapId,
) {
    existing_encounter_worker_internal(
        ctx,
        shutdown_monitor,
        encounter.id,
        EncounterData {
            pokemon_number: encounter.pokemon_number,
            map_id,
            position: Position {
                x: encounter.x,
                y: encounter.y,
                layer_number: encounter.layer_number,
            },
            direction: encounter.direction,
            is_shiny: encounter.is_shiny,
            despawn_time: encounter.despawn_time,
        },
        encounter.rng,
        encounter.last_running,
    )
    .await;
}

#[tracing::instrument(name = "encounter_worker", skip_all, fields(
    encounter_id = %encounter_id,
    map_id = %encounter_data.map_id
))]
async fn existing_encounter_worker_internal(
    ctx: GenerationContext,
    shutdown_ctrl: shutdown::Ctrl<ShutdownEvent>,
    encounter_id: EncounterId,
    encounter_data: EncounterData,
    rng: rng::FastRng,
    last_running: OffsetDateTime,
) {
    let map_id = encounter_data.map_id;
    let mut worker = EncounterWorker {
        ctx: ctx.clone(),
        encounter_data,
        encounter_id,
        rng,
    };

    let now = TimedInstant::now();

    let move_rule_provider = match ctx.tile_cache.move_rules_for_map_id(map_id).await {
        Ok(ok) => ok,
        Err(error) => {
            tracing::error!(%error, "Failed to get move rules for map. Aborting encounter worker.");
            return;
        }
    };

    metrics::gauge!("moving_encounters").increment(1);

    // The generator starts lazily to avoid work when no player is playing, so we "time-travel" back to
    // the last time the generator was running.
    // We don't care about the events since the events mutate the worker anyways and we only care about the most recent position and direction.
    let duration_until_first_async_event = match now - last_running {
        Some(time_travel_duration) => {
            let start = now + worker.first();
            let mut time_travel_iter = start.iter_until(
                || worker.next_sync(move_rule_provider.as_ref()),
                now + time_travel_duration,
            );
            loop {
                match time_travel_iter.next() {
                    IterUntilProgress::Done { overshoot_duration } => {
                        break overshoot_duration;
                    }
                    IterUntilProgress::Next(_, _) => {}
                }
            }
        }
        None => Duration::default(),
    };

    let EncounterData {
        pokemon_number,
        map_id,
        position,
        direction,
        is_shiny,
        despawn_time,
    } = worker.encounter_data;
    ctx.pokemon_channel.send(PokemonMessageWrapper {
        encounter_id,
        map_id,
        message: PokemonMessage::New(NewEncounter {
            position,
            direction,
            pokemon_number,
            is_shiny,
            despawn_time: despawn_time.into(),
        }),
    });

    let movement = {
        async {
            tracing::debug!(message = "Event loop entered", duration = ?duration_until_first_async_event);
            let mut next_event_time = TimedInstant::now() + duration_until_first_async_event;

            loop {
                let (event, _) = next_event_time.advance_with(worker.next()).await;
                tracing::trace!(?event, "Async encounter worker event");

                #[allow(clippy::single_match)]
                // In the future we will likely have more than one event type
                match event {
                    Some(EncounterWorkerEventType::Move {
                        direction,
                        position,
                    }) => {
                        ctx.pokemon_channel.send(PokemonMessageWrapper {
                            map_id,
                            encounter_id,
                            message: PokemonMessage::Move {
                                position,
                                direction,
                            },
                        });
                    }
                    None => {}
                };
            }
        }
    };

    let on_despawn = async {
        tracing::debug!(message = "Despawning encounter");
        if let Err(error) = sqlx::query!(
            "DELETE FROM encounter WHERE id = $1",
            encounter_id as EncounterId
        )
        .execute(&ctx.db_connection_pool)
        .await
        {
            tracing::error!(message = "Failed to delete encounter after despawn.", %error);
        }
        ctx.pokemon_channel.send(PokemonMessageWrapper {
            encounter_id,
            map_id,
            message: PokemonMessage::Remove,
        });
    };

    let shutdown_filter = |event: &ShutdownEvent| match event {
        ShutdownEvent::AllWorkers => true,
        ShutdownEvent::JustEncounter(id) => id == &encounter_id,
    };

    graceful_shutdown! {
        ctrl: shutdown_ctrl,
        filter: shutdown_filter,
        name: format!("encounter_{encounter_id}"),
        cleanup: shutdown_reason => {
            tracing::info!(reason=?shutdown_reason, "Shutting down encounter worker");
            match shutdown_reason {
                ShutdownEvent::JustEncounter(encounter_id) => {
                    if let Err(error) = delete_encounter(&ctx.db_connection_pool, encounter_id).await {
                        tracing::error!(%error, "Failed to delete encounter");
                    }
                },
                ShutdownEvent::AllWorkers => {
                    let saved = save_encounter(&ctx.db_connection_pool, worker.encounter_data, encounter_id, worker.rng).await;
                    if let Err(error) = saved {
                        tracing::error!(%error, "Failed to save encounter");
                    }
                }
            }
            tracing::info!("Gracefully shut down encounter worker");
        },
        _ = movement => {
            tracing::error!("Encounter worker movement loop exited early somehow. It should be terminated by despawn or the shutdown controller.");
        }
        _ = despawn_time.sleep_until() => {
            on_despawn.await;
        }
    }

    metrics::gauge!("moving_encounters").decrement(1);
}

impl EncounterWorker {
    async fn next(&mut self) -> (Option<EncounterWorkerEventType>, Duration) {
        let move_rule_provider = self
            .ctx
            .tile_cache
            .move_rules_for_map_id(self.encounter_data.map_id)
            .await;
        let Ok(move_rule_provider) = move_rule_provider else {
            return (None, Duration::from_secs(5));
        };
        self.next_sync(move_rule_provider.as_ref())
    }

    fn first(&mut self) -> Duration {
        let Some(pokemon_conf) = self
            .ctx
            .pokemon_config
            .get(self.encounter_data.pokemon_number)
            .cloned()
        else {
            tracing::warn!(message = "No pokemon config for encounter", encounter_id = %self.encounter_id, pokemon_number = %self.encounter_data.pokemon_number);
            return Duration::from_secs(5);
        };
        self.next_event(&pokemon_conf)
    }

    fn next_sync<M: MapMoveRuleProvider + Sync + Send + ?Sized>(
        &mut self,
        move_rule_provider: &M,
    ) -> (Option<EncounterWorkerEventType>, Duration) {
        let Some(pokemon_conf) = self
            .ctx
            .pokemon_config
            .get(self.encounter_data.pokemon_number)
            .cloned()
        else {
            tracing::warn!(message = "No pokemon config for encounter", encounter_id = %self.encounter_id, pokemon_number = %self.encounter_data.pokemon_number);
            return (None, Duration::from_secs(5));
        };

        let next_event = self.next_event(&pokemon_conf);

        let possible_moves = move_rule_provider
            .possible_moves(self.encounter_data.position, pokemon_conf.move_ability);

        match possible_moves.choose(&mut self.rng) {
            Some((direction, position)) => {
                self.encounter_data.position = *position;
                self.encounter_data.direction = *direction;
                (
                    Some(EncounterWorkerEventType::Move {
                        direction: *direction,
                        position: *position,
                    }),
                    next_event,
                )
            }
            None => {
                tracing::debug!(message = "Failed to find suitable move", encounter_id = %self.encounter_id);
                (None, next_event)
            }
        }
    }

    fn next_event(&mut self, pokemon_conf: &configuration::Pokemon) -> Duration {
        let move_frequency = Normal::new(
            self.ctx
                .generation_settings
                // TODO adjust move_frequency depending on pokemon nature or other stuff
                .encounter_move_time(60)
                .as_secs_f64(),
            1.0,
        )
        .expect("Building normal distribution out of configured encounter_move_time failed")
        .map(|secs| {
            Duration::from_secs_f64(
                // Needs to be written this way since Duration constructor panics if the value is < 0.
                secs.max(
                    self.ctx
                        .pokemon_config
                        .move_duration(pokemon_conf.move_speed)
                        .as_secs_f64(),
                ),
            )
        });

        move_frequency.sample(&mut self.rng)
    }
}

/// exposed for tests
pub async fn insert_encounter(
    EncounterData {
        pokemon_number,
        map_id,
        position,
        direction,
        is_shiny,
        despawn_time,
    }: EncounterData,
    rng: rng::FastRng,
    last_running: OffsetDateTime,
    pool: &sqlx::PgPool,
) -> AppResult<EncounterId> {
    let mut transaction = pool.begin().await?;

    let encounter_id = sqlx::query!(
        r#"INSERT INTO encounter 
                    (pokemon_number, map_id, x, y, layer_number, direction, is_shiny, despawn_time)
                 VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                 RETURNING id as "id: EncounterId""#,
        pokemon_number as PokemonNumber,
        map_id as MapId,
        position.x,
        position.y,
        position.layer_number,
        direction as Direction,
        is_shiny,
        OffsetDateTime::from(despawn_time),
    )
    .fetch_one(&mut *transaction)
    .await?
    .id;

    set_generator_metadata(
        &mut *transaction,
        encounter_id,
        GeneratorMetadata { last_running, rng },
    )
    .await?;

    transaction.commit().await?;
    Ok(encounter_id)
}

async fn save_encounter(
    pool: &PgPool,
    encounter_data: EncounterData,
    encounter_id: EncounterId,
    rng: rng::FastRng,
) -> AppResult<()> {
    let metadata = GeneratorMetadata {
        last_running: OffsetDateTime::now_utc(),
        rng,
    };
    let mut transaction = pool.begin().await?;

    sqlx::query!(
        "UPDATE encounter
         SET x = $2,
             y = $3,
             layer_number = $4,
             direction = $5
         WHERE id = $1",
        encounter_id as EncounterId,
        encounter_data.position.x,
        encounter_data.position.y,
        encounter_data.position.layer_number,
        encounter_data.direction as Direction
    )
    .execute(&mut *transaction)
    .await?;

    set_generator_metadata(&mut *transaction, encounter_id, metadata).await?;

    transaction.commit().await?;
    Ok(())
}

#[derive(Debug, Clone)]
struct GeneratorMetadata {
    pub last_running: OffsetDateTime,
    pub rng: rng::FastRng,
}

async fn set_generator_metadata<'a, E>(
    pool: E,
    encounter_id: EncounterId,
    metadata: GeneratorMetadata,
) -> AppResult<()>
where
    E: sqlx::PgExecutor<'a>,
{
    sqlx::query!(
        "INSERT INTO encounter_generator_metadata (encounter_id, last_running, rng_state)
        VALUES ($1, $2, $3)
        ON CONFLICT (encounter_id) DO UPDATE
        SET last_running = EXCLUDED.last_running,
            rng_state = EXCLUDED.rng_state",
        encounter_id as EncounterId,
        metadata.last_running,
        metadata.rng as rng::FastRng
    )
    .execute(pool)
    .await?;
    Ok(())
}

async fn delete_encounter(pool: &PgPool, encounter_id: EncounterId) -> AppResult<()> {
    let mut transaction = pool.begin().await?;

    sqlx::query!(
        "DELETE FROM encounter_generator_metadata WHERE encounter_id = $1",
        encounter_id as EncounterId
    )
    .execute(&mut *transaction)
    .await?;

    sqlx::query!(
        "DELETE FROM encounter WHERE id = $1",
        encounter_id as EncounterId
    )
    .execute(&mut *transaction)
    .await?;

    transaction.commit().await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use futures::StreamExt;
    use tokio::time::{Instant, timeout};

    use super::*;
    use crate::{
        arceus::generator,
        data::{MoveAbilities, MoveAbility},
        smeargle,
    };

    #[database_macros::test]
    #[tracing_test::traced_test]
    async fn insert_and_retrieve_encounter(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let last_running = OffsetDateTime::now_utc();
        let setup = generator::test_helper::setup(&pool, 123124125125).await?;

        let map_id = smeargle::test_helper::create_map(&pool, "testmap").await?;

        setup.set_positions_to_allow_move_abilities(
            map_id,
            &[(0, 0)],
            MoveAbilities::empty() | MoveAbility::Walk,
        );
        let map_ctrl = shutdown::Ctrl::new();

        let rng = setup.generation_context.rng_provider.next();

        let encounter_data = EncounterData {
            pokemon_number: PokemonNumber(10),
            map_id,
            position: Position {
                x: 5,
                y: 7,
                layer_number: 1,
            },
            direction: Direction::L,
            is_shiny: true,
            despawn_time: TimedInstant::now() + Duration::from_secs(100),
        };

        let now = Instant::now();
        tokio::spawn(encounter_worker(
            setup.generation_context.clone(),
            map_ctrl.clone(),
            encounter_data,
            rng,
            last_running,
        ));

        let pokemon_stream = setup.pokemon_channel.receive();

        let mut wrapper = StreamWrapper(pokemon_stream);
        let first_event = wrapper.wait_for_event().await;
        assert_eq!(first_event.map_id, map_id);

        let mut first_encounter = match first_event.message {
            PokemonMessage::New(new) => new,
            _ => panic!("Expected PokemonMessage::New"),
        };

        while Instant::now() < now + Duration::from_millis(500) {
            let event = wrapper.wait_for_event().await;
            assert_eq!(event.map_id, map_id);
            assert_eq!(event.encounter_id, first_event.encounter_id);
            match event.message {
                PokemonMessage::New(new_encounter) => {
                    panic!("Got another PokemonMessage::New: {new_encounter:?}")
                }
                PokemonMessage::Caught { by } => {
                    panic!("Got an unexpected PokemonMessage::Caught: {by:?}")
                }
                PokemonMessage::BrokeOut { from } => {
                    panic!("Got an unexpected PokemonMessage::BrokeOut: {from:?}")
                }
                PokemonMessage::Move {
                    position,
                    direction,
                } => {
                    first_encounter.position = position;
                    first_encounter.direction = direction;
                }
                PokemonMessage::Remove => {
                    panic!("Despawn time is too far in the future to get a PokemonMessage::Remove")
                }
            }
        }
        assert_eq!(
            first_encounter,
            NewEncounter {
                position: Position {
                    x: 6,
                    y: 6,
                    layer_number: 1
                },
                direction: Direction::R,
                pokemon_number: PokemonNumber(10),
                is_shiny: true,
                despawn_time: encounter_data.despawn_time.into()
            }
        );

        map_ctrl
            .shutdown(ShutdownEvent::AllWorkers, Duration::from_millis(100))
            .await;
        setup.shutdown().await;

        Ok(())
    }

    struct StreamWrapper<T>(T);

    impl<T, U> StreamWrapper<T>
    where
        T: futures::Stream<Item = U>,
        T: std::marker::Unpin,
    {
        async fn wait_for_event(&mut self) -> U {
            timeout(Duration::from_millis(5000), self.0.next())
                .await
                .expect("no timeout")
                .expect("an event")
        }
    }
}
