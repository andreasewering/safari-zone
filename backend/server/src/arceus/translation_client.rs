use std::iter::zip;

use itertools::Itertools;
use nonempty::NonEmpty;
use serde::{Deserialize, Serialize};

use crate::{
    data::SupportedLanguage,
    error::{AppError, AppResult},
};

use super::config::TranslationSettings;

#[async_trait::async_trait]
pub trait TranslationClient {
    async fn translate(
        &self,
        source_language: SupportedLanguage,
        target_languages: NonEmpty<SupportedLanguage>,
        text: String,
    ) -> AppResult<Vec<(SupportedLanguage, String)>>;
}

#[derive(Debug, Clone)]
pub struct OpenAITranslationClient {
    client: reqwest::Client,
    config: TranslationSettings,
}

impl OpenAITranslationClient {
    pub fn new(client: &reqwest::Client, config: &TranslationSettings) -> Self {
        Self {
            client: client.clone(),
            config: config.clone(),
        }
    }
}

#[async_trait::async_trait]
impl TranslationClient for OpenAITranslationClient {
    async fn translate(
        &self,
        source_language: SupportedLanguage,
        target_languages: NonEmpty<SupportedLanguage>,
        text: String,
    ) -> AppResult<Vec<(SupportedLanguage, String)>> {
        let formatted_target_languages = target_languages
            .iter()
            .map(|lang| lang.to_string())
            .join(", ");
        let request = OpenAIRequest {
            model: self.config.model.to_string(),
            messages: vec![
                OpenAIMessage {
                    role: "system".to_string(),
                    content: "You are a helpful assistant.".to_string()
                },
                OpenAIMessage {
                    role: "user".to_string(),
                    content: format!("Translate the following text from {source_language} into {formatted_target_languages}, and return the translations in the same order, seperated by newlines: \"{text}\". Your response should only contain the translations, not the languages and no extra text.")
                }
            ],
            max_tokens: self.config.max_tokens,
            temperature: self.config.temperature,
        };
        let response = self
            .client
            .post(self.config.endpoint_url.as_ref())
            .bearer_auth(&self.config.openai_api_key)
            .json(&request)
            .send()
            .await?;
        let open_ai_response: OpenAIResponse =
            response.json().await.map_err(AppError::inconsistency)?;

        let newline_seperated_translations = open_ai_response
            .choices
            .into_iter()
            .next()
            .map(|choice| choice.message.content)
            .ok_or(AppError::inconsistency(
                "Expected to get at least one 'choice' back.",
            ))?;

        let translations = zip(
            target_languages.into_iter(),
            newline_seperated_translations
                .split('\n')
                .map(|str| str.trim().to_owned()),
        )
        .collect();

        Ok(translations)
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct OpenAIRequest {
    model: String,
    messages: Vec<OpenAIMessage>,
    max_tokens: u32,
    temperature: f32,
}

#[derive(Debug, Serialize, Deserialize)]
struct OpenAIMessage {
    role: String,
    content: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct OpenAIResponse {
    choices: Vec<OpenAIChoice>,
}

#[derive(Debug, Serialize, Deserialize)]
struct OpenAIChoice {
    message: OpenAIMessage,
}

#[cfg(test)]
pub mod test_utils {
    use crate::error::AppResult;

    use super::TranslationClient;

    pub struct MockTranslationClient;

    #[async_trait::async_trait]
    impl TranslationClient for MockTranslationClient {
        async fn translate(
            &self,
            _source_language: crate::data::SupportedLanguage,
            target_languages: nonempty::NonEmpty<crate::data::SupportedLanguage>,
            text: String,
        ) -> AppResult<Vec<(crate::data::SupportedLanguage, String)>> {
            let translations = target_languages
                .into_iter()
                .map(|lang| (lang, text.clone()))
                .collect();
            Ok(translations)
        }
    }
}

#[cfg(test)]
mod tests {
    use std::net::SocketAddr;

    use axum::{routing::post, Router};
    use tokio::task::JoinHandle;

    use super::*;

    #[tokio::test]
    async fn server_not_available() -> anyhow::Result<()> {
        let client = reqwest::Client::new();
        let config = TranslationSettings {
            endpoint_url: "http://localhost:1234/no-chance".into(),
            model: "i-do-not-care".into(),
            max_tokens: 123,
            temperature: 0.69,
            openai_api_key: "does-not-matter".into(),
        };
        let translation_client = OpenAITranslationClient::new(&client, &config);
        let result = translation_client
            .translate(
                SupportedLanguage::EN,
                nonempty::nonempty![SupportedLanguage::DE],
                "will not succeed".into(),
            )
            .await;
        assert!(result.is_err());

        Ok(())
    }

    #[tokio::test]
    async fn server_responds_with_unexpected_json() -> anyhow::Result<()> {
        #[derive(Debug, Clone, Serialize)]
        struct Unexpected {
            yolo: String,
        }

        let (addr, _server_handle) = start_test_server(Unexpected {
            yolo: "test123".to_string(),
        })
        .await?;
        let client = reqwest::Client::new();
        let config = TranslationSettings {
            endpoint_url: format!("http://{addr}/translations").into(),
            model: "a_model".into(),
            max_tokens: 123,
            temperature: 0.69,
            openai_api_key: "some_key".into(),
        };
        let translation_client = OpenAITranslationClient::new(&client, &config);
        let error = translation_client
            .translate(
                SupportedLanguage::EN,
                nonempty::nonempty![SupportedLanguage::DE],
                "will not succeed".into(),
            )
            .await
            .expect_err("Json should not be of the appropriate format");
        let status = tonic::Status::from(error);
        assert_eq!(status.code(), tonic::Code::FailedPrecondition);

        Ok(())
    }

    async fn start_test_server<T: Serialize + Clone + Sync + Send + 'static>(
        json: T,
    ) -> anyhow::Result<(SocketAddr, ServerHandle)> {
        let handler = move || async move { axum::Json(json) };
        // Build an application with a single route
        let app = Router::new().route("/translations", post(handler));

        // Let the OS choose a ephemeral port
        let addr = SocketAddr::from(([127, 0, 0, 1], 0));
        let listener = tokio::net::TcpListener::bind(addr).await?;
        let addr = listener.local_addr()?;

        // Run the server
        println!("Listening on http://{}", addr);
        let handle =
            tokio::spawn(async move { axum::serve(listener, app.into_make_service()).await });

        Ok((addr, ServerHandle { handle }))
    }

    struct ServerHandle {
        handle: JoinHandle<std::io::Result<()>>,
    }

    impl Drop for ServerHandle {
        fn drop(&mut self) {
            self.handle.abort();
        }
    }
}
