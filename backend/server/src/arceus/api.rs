use super::{
    area_encounter::AreaEncounter,
    area_id::AreaId,
    area_item::AreaItem,
    proto::arceus::{AudioTrack, TranslateResponse},
    queries::{AreaToCreate, AreaUpdate},
    translation_client::TranslationClient,
    Settings,
};
use crate::{
    data::{proto, InvalidPokemonNumberError, MapId, PokemonNumber, SupportedLanguage, Translated},
    error::{AppError, AppResult},
    gardevoir::{self, guard_map_read, AuthCtx},
    range::IntRange,
};
use itertools::multizip;
use nonempty::NonEmpty;
use soa_rs::Soa;
use sqlx::{postgres::types::PgRange, PgPool};
use tonic::{Request, Response, Status};

use super::proto::arceus::{
    arceus_service_server::ArceusService, Area, AreaPart, CreateAreaRequest, CreateAreaResponse,
    DeleteAreaRequest, DeleteAreaResponse, EditAreaRequest, EditAreaResponse, Encounter,
    GetAreaDetailRequest, GetAreaDetailResponse, GetAreasRequest, GetAreasResponse,
    GetAudioTracksRequest, GetAudioTracksResponse, ItemConfig, TranslateRequest,
};
use super::{
    area_encounter::{self},
    area_item::{self},
    generator::SignalSender,
};

pub struct Arceus<K, T> {
    pub auth_ctx: AuthCtx,
    pub db_connection_pool: PgPool,
    pub signal_sender: SignalSender<K>,
    pub translation_client: T,
    pub settings: Settings,
}

#[tonic::async_trait]
impl<K, T> ArceusService for Arceus<K, T>
where
    K: Send + Sync + Ord + core::fmt::Debug + Copy + 'static,
    T: TranslationClient + Send + Sync + 'static,
{
    async fn get_areas(
        &self,
        request: Request<GetAreasRequest>,
    ) -> Result<Response<GetAreasResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        let locale = SupportedLanguage::from_grpc_headers(request.metadata());
        let GetAreasRequest { map_id } = request.into_inner();
        let map_id = MapId::from(map_id);

        guard_map_read(&claims, map_id)?;

        let areas = sqlx::query!(r#"
            WITH translation_agg AS (
                SELECT area_id, area_name AS name
                FROM area_translations
                WHERE locale = $2
            ),
            part_agg AS (
                SELECT area_id, ARRAY_AGG(range_x) AS range_xs, ARRAY_AGG(range_y) AS range_ys, ARRAY_AGG(range_z) AS range_zs
                FROM area_part
                GROUP BY area_id
            )
            SELECT a.id, t.name, p.range_xs, p.range_ys, p.range_zs, a.background_music_track
            FROM area a
            JOIN translation_agg t ON a.id = t.area_id
            JOIN part_agg p ON a.id = p.area_id
            WHERE a.map_id = $1"#,
            map_id as MapId,
            locale as SupportedLanguage
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?
        .into_iter()
        .map(|area| {
            let area_parts = area_part_ranges_to_area_parts(area.range_xs, area.range_ys, area.range_zs);
            let name = area.name;
            Ok(Area {
                id: area.id,
                name,
                background_music_track: area.background_music_track.map(|i32|i32 as u32),
                area_parts
            })
        })
        .collect::<AppResult<_>>()?;
        Ok(Response::new(GetAreasResponse { areas }))
    }

    async fn get_area_detail(
        &self,
        request: Request<GetAreaDetailRequest>,
    ) -> Result<Response<GetAreaDetailResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        gardevoir::guard(&claims, gardevoir::Permission::ReadMapDetails)?;

        let GetAreaDetailRequest { area_id } = request.into_inner();
        let area_id = AreaId::from(area_id);

        struct Entity {
            locales: Option<Vec<SupportedLanguage>>,
            names: Option<Vec<String>>,
            range_xs: Option<Vec<PgRange<i32>>>,
            range_ys: Option<Vec<PgRange<i32>>>,
            range_zs: Option<Vec<PgRange<i32>>>,
            background_music_track: Option<i32>,
        }

        let area = sqlx::query_as!(Entity, r#"
        WITH translation_agg AS (
            SELECT area_id, ARRAY_AGG(locale) as locales, ARRAY_AGG(area_name) AS names 
            FROM area_translations
            GROUP BY area_id
        ),
        part_agg AS (
            SELECT area_id, ARRAY_AGG(range_x) AS range_xs, ARRAY_AGG(range_y) AS range_ys, ARRAY_AGG(range_z) AS range_zs
            FROM area_part
            GROUP BY area_id
        )
        SELECT t.locales as "locales: _", t.names, p.range_xs, p.range_ys, p.range_zs, a.background_music_track
        FROM area a
        JOIN translation_agg t ON a.id = t.area_id
        JOIN part_agg p ON a.id = p.area_id
        WHERE a.id = $1"#,
            area_id as AreaId
        )
        .fetch_one(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        let encounters = area_encounter::get_encounters_from_db(area_id, &self.db_connection_pool)
            .await
            .map_err(AppError::from)?
            .into_iter()
            .map(|encounter| Encounter {
                probability: encounter.probability,
                pokemon_number: u32::from(encounter.pokemon_number),
                spawn_kind: super::proto::arceus::SpawnKind::from(encounter.spawn_kind).into(),
            })
            .collect();

        let items = area_item::get_items_from_db(area_id, &self.db_connection_pool)
            .await
            .map_err(AppError::from)?
            .into_iter()
            .map(|area_item| ItemConfig {
                probability: area_item.probability,
                item: proto::Item::from(area_item.item).into(),
            })
            .collect();

        let area_parts =
            area_part_ranges_to_area_parts(area.range_xs, area.range_ys, area.range_zs);

        let name = (|| {
            let locales = area.locales?;
            let names = area.names?;
            let translated = Translated::from_array_agg(locales, names)?;
            Some(translated)
        })()
        .ok_or(AppError::inconsistency(format!(
            "Not all locales for area {area_id} have a translation."
        )))?;

        Ok(Response::new(GetAreaDetailResponse {
            area_id: area_id.into(),
            name: Some(name.into_proto()),
            area_parts,
            background_music_track: area.background_music_track.map(|i32| i32 as u32),
            encounters,
            items,
        }))
    }

    async fn create_area(
        &self,
        request: Request<CreateAreaRequest>,
    ) -> Result<Response<CreateAreaResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        gardevoir::guard(&claims, gardevoir::Permission::EditAreas)?;

        let CreateAreaRequest {
            map_id,
            name,
            area_parts,
        } = request.into_inner();
        let name = Translated::from_proto(name.ok_or(AppError::bad_request(
            "Name is a required field of CreateAreaRequest.",
        ))?);

        let parts = area_parts
            .into_iter()
            .map(area_part_to_int_ranges)
            .collect();

        let area_id = super::queries::create_area(
            &self.db_connection_pool,
            AreaToCreate {
                map_id: MapId::from(map_id),
                name,
                parts,
            },
        )
        .await?;

        Ok(Response::new(CreateAreaResponse {
            area_id: area_id.into(),
        }))
    }

    async fn edit_area(
        &self,
        request: Request<EditAreaRequest>,
    ) -> Result<Response<EditAreaResponse>, Status> {
        let EditAreaRequest {
            area_id,
            name,
            area_parts,
            background_music_track,
            encounters,
            items,
        } = request.into_inner();
        let area_id = AreaId::from(area_id);

        let mut transaction = self
            .db_connection_pool
            .begin()
            .await
            .map_err(AppError::from)?;

        let map_id = super::queries::edit_area(
            &mut *transaction,
            area_id,
            AreaUpdate {
                background_music_track,
            },
        )
        .await?;

        if let Some(name) = name {
            sqlx::query!(
                "DELETE FROM area_translations WHERE area_id = $1",
                area_id as AreaId
            )
            .execute(&mut *transaction)
            .await
            .map_err(AppError::from)?;

            let name = Translated::from_proto(name);
            super::queries::create_area_translations(&mut *transaction, area_id, map_id, name)
                .await?;
        }

        if let Some(area_parts) = area_parts {
            let parts = area_parts
                .list
                .into_iter()
                .map(area_part_to_int_ranges)
                .collect();
            sqlx::query!(
                "DELETE FROM area_part WHERE area_id = $1",
                area_id as AreaId
            )
            .execute(&mut *transaction)
            .await
            .map_err(AppError::from)?;

            super::queries::create_area_parts(&mut *transaction, area_id, map_id, parts).await?;
        }

        if let Some(encounters) = encounters {
            let area_encounters = encounters
                .list
                .into_iter()
                .filter_map(|encounter| {
                    let spawn_kind =
                        super::proto::arceus::SpawnKind::try_from(encounter.spawn_kind).ok()?;
                    let area_encounter = match PokemonNumber::try_from(encounter.pokemon_number) {
                        Ok(pokemon_number) => AreaEncounter {
                            pokemon_number,
                            probability: encounter.probability,
                            spawn_kind: spawn_kind.into(),
                        },
                        Err(error) => {
                            return Some(Err(error));
                        }
                    };
                    Some(Ok(area_encounter))
                })
                .collect::<Result<Soa<AreaEncounter>, InvalidPokemonNumberError>>()?;

            area_encounter::set_encounters_in_db(
                area_id,
                area_encounters,
                &self.db_connection_pool,
            )
            .await?;
        }

        if let Some(items) = items {
            let area_items = items
                .list
                .into_iter()
                .filter_map(|item| {
                    let item_ = proto::Item::try_from(item.item).ok()?;
                    let area_item = AreaItem {
                        item: item_.into(),
                        probability: item.probability,
                    };
                    Some(area_item)
                })
                .collect();
            area_item::set_items_from_db(area_id, area_items, &self.db_connection_pool).await?;
        }

        transaction.commit().await.map_err(AppError::from)?;

        if let Err(error) = self.signal_sender.start_area(area_id, map_id).await {
            tracing::warn!(message = "Failed to send upsert area to generator", %area_id, %error);
        }

        Ok(Response::new(EditAreaResponse {}))
    }

    async fn delete_area(
        &self,
        request: Request<DeleteAreaRequest>,
    ) -> Result<Response<DeleteAreaResponse>, Status> {
        let DeleteAreaRequest { area_id } = request.into_inner();
        let area_id = AreaId::from(area_id);

        let result = sqlx::query!(r#"DELETE FROM area WHERE id = $1"#, area_id as AreaId)
            .execute(&self.db_connection_pool)
            .await
            .map_err(AppError::from)?;

        if result.rows_affected() > 0 {
            if let Err(error) = self.signal_sender.stop_area(area_id).await {
                tracing::warn!(message = "Failed to send delete area to generator", %area_id, %error);
            }
        }

        Ok(Response::new(DeleteAreaResponse {}))
    }

    async fn get_audio_tracks(
        &self,
        _request: Request<GetAudioTracksRequest>,
    ) -> Result<Response<GetAudioTracksResponse>, Status> {
        let tracks = self
            .settings
            .audio
            .tracks
            .iter()
            .map(|(audio_track_number, track)| AudioTrack {
                audio_track_number: (*audio_track_number).into(),
                filename: track.filename.to_string(),
                start: track.start,
                end: track.end,
            })
            .collect();
        let events = &self.settings.audio.events;

        Ok(Response::new(GetAudioTracksResponse {
            tracks,
            on_pokemon_obtained: events.pokemon_obtained.into(),
        }))
    }

    async fn translate(
        &self,
        request: Request<TranslateRequest>,
    ) -> Result<Response<TranslateResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        gardevoir::guard(&claims, gardevoir::Permission::TranslateApi)?;

        let TranslateRequest {
            source_language,
            target_languages,
            text,
        } = request.into_inner();
        let source_language: SupportedLanguage = proto::Locale::try_from(source_language)
            .map_err(AppError::bad_request)?
            .into();
        let target_languages = target_languages
            .into_iter()
            .map(|lang| {
                let target_language =
                    proto::Locale::try_from(lang).map_err(AppError::bad_request)?;
                Ok(SupportedLanguage::from(target_language))
            })
            .collect::<AppResult<Vec<SupportedLanguage>>>()?;
        let target_languages =
            NonEmpty::try_from(target_languages).map_err(AppError::bad_request)?;

        let translations = self
            .translation_client
            .translate(source_language, target_languages, text)
            .await?
            .into_iter()
            .map(|(language, text)| {
                let language = proto::Locale::from(language) as i32;
                super::proto::arceus::Translation { language, text }
            })
            .collect();
        Ok(Response::new(TranslateResponse { translations }))
    }
}

fn area_part_ranges_to_area_parts(
    ranges_x: Option<Vec<PgRange<i32>>>,
    ranges_y: Option<Vec<PgRange<i32>>>,
    ranges_z: Option<Vec<PgRange<i32>>>,
) -> Vec<AreaPart> {
    match (ranges_x, ranges_y, ranges_z) {
        (Some(x_ranges), Some(y_ranges), Some(z_ranges)) => {
            multizip((x_ranges, y_ranges, z_ranges))
                .filter_map(|(range_x, range_y, range_z)| {
                    let range_x = IntRange::try_from(range_x).ok()?;
                    let range_y = IntRange::try_from(range_y).ok()?;
                    let range_z = IntRange::try_from(range_z).ok()?;
                    Some(AreaPart {
                        top_left_x: range_x.start_inclusive(),
                        top_left_y: range_y.start_inclusive(),
                        bottom_right_x: range_x.end_inclusive(),
                        bottom_right_y: range_y.end_inclusive(),
                        bottom_layer_number: range_z.start_inclusive(),
                        top_layer_number: range_z.end_inclusive(),
                    })
                })
                .collect()
        }
        _ => Vec::new(),
    }
}

fn area_part_to_int_ranges(
    AreaPart {
        top_left_x,
        top_left_y,
        bottom_right_x,
        bottom_right_y,
        bottom_layer_number,
        top_layer_number,
    }: AreaPart,
) -> (IntRange, IntRange, IntRange) {
    (
        IntRange::new(top_left_x, bottom_right_x),
        IntRange::new(top_left_y, bottom_right_y),
        IntRange::new(bottom_layer_number, top_layer_number),
    )
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use futures::{Stream, StreamExt};

    use super::*;
    use crate::arceus::generator::{self, SignalSender};
    use crate::arceus::proto::arceus::AreaParts;
    use crate::arceus::translation_client::test_utils::MockTranslationClient;
    use crate::data::RequestExt as _;
    use crate::gardevoir::{test_helper as gardevoir, Permission};
    use crate::smeargle::test_helper as smeargle;

    struct Setup<R> {
        api: Arceus<i32, MockTranslationClient>,
        signal_receiver: R,
        signal_sender: SignalSender<i32>,
        user: gardevoir::LoggedInUser,
        map_id: MapId,
    }

    async fn setup(
        pool: &sqlx::PgPool,
    ) -> anyhow::Result<Setup<impl Stream<Item = generator::Message> + use<>>> {
        let settings = Settings::read()?;
        let auth_ctx = gardevoir::create_auth_ctx().await?;
        let map_id = smeargle::create_map(pool, "testmap").await?;
        let user = gardevoir::UserBuilder::from_name("user")
            .create(&auth_ctx, pool)
            .await?;

        let (signal_sender, signal_receiver) = SignalSender::new();
        let api = Arceus {
            db_connection_pool: pool.clone(),
            auth_ctx: auth_ctx.clone(),
            signal_sender: signal_sender.clone(),
            settings,
            translation_client: MockTranslationClient,
        };
        Ok(Setup {
            api,
            signal_receiver,
            signal_sender,
            user,
            map_id,
        })
    }

    #[database_macros::test]
    async fn create_area_without_permission(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let status = setup
            .api
            .create_area(setup.user.request(make_create_area_request(setup.map_id)))
            .await
            .expect_err("Should fail because EditAreas permission is missing.");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn get_area_detail_without_permission(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::EditAreas]);

        let CreateAreaResponse { area_id } = setup
            .api
            .create_area(setup.user.request(make_create_area_request(setup.map_id)))
            .await?
            .into_inner();

        let status = setup
            .api
            .get_area_detail(setup.user.request(GetAreaDetailRequest { area_id }))
            .await
            .expect_err("Should fail because ReadMapDetails permission is missing.");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn user_is_on_a_different_map(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let other_map_id = smeargle::create_map(&pool, "othermap").await?;
        setup
            .user
            .patch_permissions(&[Permission::EditAreas, Permission::ReadMapDetails]);
        setup.user.enter_map(other_map_id);

        let CreateAreaResponse { area_id } = setup
            .api
            .create_area(setup.user.request(make_create_area_request(setup.map_id)))
            .await
            .expect("this still works because admin permissions are necessary anyways")
            .into_inner();
        setup
            .api
            .get_area_detail(setup.user.request(GetAreaDetailRequest { area_id }))
            .await
            .expect("this still works because admin permissions are necessary anyways")
            .into_inner();

        // remove admin permissions
        setup.user.patch_permissions(&[]);
        setup
            .api
            .get_areas(setup.user.request(GetAreasRequest {
                map_id: other_map_id.into(),
            }))
            .await
            .expect("No permissions necessary to read areas on the users map");

        setup
            .api
            .get_areas(setup.user.request(GetAreasRequest {
                map_id: other_map_id.into(),
            }))
            .await?;

        let status = setup
            .api
            .get_areas(setup.user.request(GetAreasRequest {
                map_id: setup.map_id.into(),
            }))
            .await
            .expect_err("Should fail because user is on a different map.");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn user_is_on_a_different_map_but_has_read_map_details_permission(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let other_map_id = smeargle::create_map(&pool, "othermap").await?;
        setup
            .user
            .patch_permissions(&[Permission::EditAreas, Permission::ReadMapDetails]);
        setup.user.enter_map(other_map_id);

        let CreateAreaResponse { area_id } = setup
            .api
            .create_area(setup.user.request(make_create_area_request(setup.map_id)))
            .await
            .expect("this still works because admin permissions are necessary anyways")
            .into_inner();
        setup
            .api
            .get_area_detail(setup.user.request(GetAreaDetailRequest { area_id }))
            .await
            .expect("this still works because admin permissions are necessary anyways")
            .into_inner();

        // remove admin permissions
        setup.user.patch_permissions(&[Permission::ReadMapDetails]);
        setup
            .api
            .get_areas(setup.user.request(GetAreasRequest {
                map_id: setup.map_id.into(),
            }))
            .await?;

        Ok(())
    }

    #[database_macros::test]
    async fn create_and_get_area(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        // Usecase: The user is on no map but has admin permissions
        setup
            .user
            .patch_permissions(&[Permission::EditAreas, Permission::ReadMapDetails]);

        let create_area_request = make_create_area_request(setup.map_id);

        let CreateAreaResponse { area_id } = setup
            .api
            .create_area(setup.user.request(create_area_request.clone()))
            .await?
            .into_inner();

        let GetAreaDetailResponse {
            area_id: response_area_id,
            name,
            area_parts,
            background_music_track,
            encounters,
            items,
        } = setup
            .api
            .get_area_detail(setup.user.request(GetAreaDetailRequest { area_id }))
            .await?
            .into_inner();
        assert_eq!(response_area_id, area_id);
        assert_eq!(name, create_area_request.name);
        assert_eq!(area_parts, create_area_request.area_parts);
        assert_eq!(background_music_track, None);
        assert_eq!(encounters, Vec::new());
        assert_eq!(items, Vec::new());

        let GetAreasResponse { areas } = setup
            .api
            .get_areas(
                setup
                    .user
                    .request(GetAreasRequest {
                        map_id: setup.map_id.into(),
                    })
                    .with_locale(SupportedLanguage::DE),
            )
            .await?
            .into_inner();
        let [Area {
            id,
            name,
            area_parts,
            background_music_track,
        }] = areas
            .try_into()
            .expect("Exactly one area should be returned.");

        assert_eq!(id, area_id);
        assert_eq!(name, create_area_request.name.unwrap().de);
        assert_eq!(area_parts, create_area_request.area_parts);
        assert_eq!(background_music_track, None);

        Ok(())
    }

    #[database_macros::test]
    async fn edit_area_name(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup
            .user
            .patch_permissions(&[Permission::EditAreas, Permission::ReadMapDetails]);
        setup.signal_sender.start_map(setup.map_id, 5).await;
        setup.signal_receiver.next().await;

        let create_area_request = make_create_area_request(setup.map_id);
        let CreateAreaResponse { area_id } = setup
            .api
            .create_area(setup.user.request(create_area_request.clone()))
            .await?
            .into_inner();

        let edit_area_request = EditAreaRequest {
            area_id,
            name: Some(proto::Translated {
                de: "Veränderter Name".to_string(),
                en: "Changed name".to_string(),
                fr: "Nom changé".to_string(),
            }),
            area_parts: None,
            background_music_track: None,
            encounters: None,
            items: None,
        };

        let EditAreaResponse {} = setup
            .api
            .edit_area(setup.user.request(edit_area_request.clone()))
            .await?
            .into_inner();

        let GetAreaDetailResponse {
            area_id: response_area_id,
            name,
            area_parts,
            background_music_track,
            encounters,
            items,
        } = setup
            .api
            .get_area_detail(setup.user.request(GetAreaDetailRequest { area_id }))
            .await?
            .into_inner();
        assert_eq!(response_area_id, area_id);
        assert_eq!(name, edit_area_request.name);
        assert_eq!(area_parts, create_area_request.area_parts);
        assert_eq!(background_music_track, None);
        assert_eq!(encounters, Vec::new());
        assert_eq!(items, Vec::new());

        let GetAreasResponse { areas } = setup
            .api
            .get_areas(
                setup
                    .user
                    .request(GetAreasRequest {
                        map_id: setup.map_id.into(),
                    })
                    .with_locale(SupportedLanguage::EN),
            )
            .await?
            .into_inner();
        let [Area {
            id,
            name,
            area_parts,
            background_music_track,
        }] = areas
            .try_into()
            .expect("Exactly one area should be returned.");

        assert_eq!(id, area_id);
        assert_eq!(name, edit_area_request.name.unwrap().en);
        assert_eq!(area_parts, create_area_request.area_parts);
        assert_eq!(background_music_track, None);

        let event =
            tokio::time::timeout(Duration::from_millis(100), setup.signal_receiver.next()).await;
        assert!(
            event.is_ok(),
            "Expected message to have been sent but none has been received"
        );
        assert_eq!(
            event,
            Ok(Some(generator::Message::StartArea(
                area_id.into(),
                setup.map_id
            )))
        );

        Ok(())
    }

    #[database_macros::test]
    async fn edit_area_name_track_and_parts(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup
            .user
            .patch_permissions(&[Permission::EditAreas, Permission::ReadMapDetails]);
        setup.signal_sender.start_map(setup.map_id, 5).await;
        setup.signal_receiver.next().await;

        let CreateAreaResponse { area_id } = setup
            .api
            .create_area(setup.user.request(make_create_area_request(setup.map_id)))
            .await?
            .into_inner();

        let edit_area_request = EditAreaRequest {
            area_id,
            name: Some(proto::Translated {
                de: "Veränderter Name".to_string(),
                en: "Changed name".to_string(),
                fr: "Nom changé".to_string(),
            }),
            area_parts: Some(AreaParts {
                list: vec![AreaPart {
                    top_left_x: 111,
                    top_left_y: 222,
                    bottom_right_x: 333,
                    bottom_right_y: 444,
                    bottom_layer_number: 5,
                    top_layer_number: 5,
                }],
            }),
            background_music_track: Some(3),
            encounters: None,
            items: None,
        };

        let EditAreaResponse {} = setup
            .api
            .edit_area(setup.user.request(edit_area_request.clone()))
            .await?
            .into_inner();

        let GetAreaDetailResponse {
            area_id: response_area_id,
            name,
            area_parts,
            background_music_track,
            encounters,
            items,
        } = setup
            .api
            .get_area_detail(setup.user.request(GetAreaDetailRequest { area_id }))
            .await?
            .into_inner();
        assert_eq!(response_area_id, area_id);
        assert_eq!(name, edit_area_request.name);
        assert_eq!(
            Some(AreaParts { list: area_parts }),
            edit_area_request.area_parts
        );
        assert_eq!(
            background_music_track,
            edit_area_request.background_music_track
        );
        assert_eq!(encounters, Vec::new());
        assert_eq!(items, Vec::new());

        let GetAreasResponse { areas } = setup
            .api
            .get_areas(setup.user.request(GetAreasRequest {
                map_id: setup.map_id.into(),
            }))
            .await?
            .into_inner();
        let [Area {
            id,
            name,
            area_parts,
            background_music_track,
        }] = areas
            .try_into()
            .expect("Exactly one area should be returned.");

        assert_eq!(id, area_id);
        assert_eq!(name, edit_area_request.name.unwrap().en);
        assert_eq!(
            Some(AreaParts { list: area_parts }),
            edit_area_request.area_parts
        );
        assert_eq!(
            background_music_track,
            edit_area_request.background_music_track
        );

        let event =
            tokio::time::timeout(Duration::from_millis(100), setup.signal_receiver.next()).await;
        assert!(
            event.is_ok(),
            "Expected message to have been sent but none has been received"
        );
        assert_eq!(
            event,
            Ok(Some(generator::Message::StartArea(
                area_id.into(),
                setup.map_id
            )))
        );

        Ok(())
    }

    fn make_create_area_request(map_id: MapId) -> CreateAreaRequest {
        CreateAreaRequest {
            map_id: map_id.into(),
            name: Some(proto::Translated {
                en: "Grassy Forest".to_string(),
                de: "Grasiger Wald".to_string(),
                fr: "Forêt Herbeuse".to_string(),
            }),
            area_parts: vec![
                AreaPart {
                    top_left_x: -123,
                    top_left_y: -234,
                    bottom_right_x: 56,
                    bottom_right_y: 67,
                    bottom_layer_number: 0,
                    top_layer_number: 0,
                },
                AreaPart {
                    top_left_x: -100,
                    top_left_y: -236,
                    bottom_right_x: 66,
                    bottom_right_y: -235,
                    bottom_layer_number: 0,
                    top_layer_number: 1,
                },
            ],
        }
    }
}
