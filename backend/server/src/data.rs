pub mod proto {
    include!(concat!(env!("OUT_DIR"), "/data.rs"));
}

mod direction;
mod encounter_id;
mod item;
mod item_id;
mod map_id;
mod move_ability;
mod pokemon_number;
mod pokemon_type;
mod position;
mod spawn_kind;
mod throw;
mod trainer_number;
mod translated;
mod user_id;

pub use direction::{Direction, Directional};
pub use encounter_id::EncounterId;
pub use item::Item;
pub use item_id::ItemId;
pub use map_id::MapId;
pub use move_ability::{MoveAbilities, MoveAbility};
pub use pokemon_number::{InvalidPokemonNumberError, PokemonNumber};
pub use pokemon_type::PokemonType;
pub use position::Position;
pub use spawn_kind::SpawnKind;
pub use throw::Throw;
pub use trainer_number::TrainerNumber;
pub use translated::{SupportedLanguage, Translated};
pub use user_id::UserId;

#[cfg(test)]
pub use translated::RequestExt;
