use std::{
    path::{Path, PathBuf},
    sync::OnceLock,
};

static EXE_DIR: OnceLock<PathBuf> = OnceLock::new();

pub fn os_path(relative_path: &Path) -> PathBuf {
    let mut exe_dir: PathBuf = EXE_DIR
        .get_or_init(|| {
            std::env::current_exe()
                .unwrap()
                .parent()
                .unwrap()
                .to_owned()
        })
        .clone();
    exe_dir.push(normalize_rel_path(relative_path));
    exe_dir
}

fn normalize_rel_path(relative_path: &Path) -> PathBuf {
    relative_path
        .to_string_lossy()
        .split('/')
        .filter(|segment| !segment.is_empty())
        .collect()
}
