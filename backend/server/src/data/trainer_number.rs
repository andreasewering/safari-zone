use std::error::Error;
use std::str::FromStr;
use std::{fmt::Display, num::TryFromIntError};

use serde::{Deserialize, Serialize};
use sqlx::Postgres;

#[derive(Debug, Copy, Clone, Deserialize, Serialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TrainerNumber(u16);

impl Display for TrainerNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl FromStr for TrainerNumber {
    type Err = <u16 as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse().map(TrainerNumber)
    }
}

impl From<TrainerNumber> for u32 {
    fn from(value: TrainerNumber) -> Self {
        value.0.into()
    }
}

impl From<u16> for TrainerNumber {
    fn from(value: u16) -> Self {
        TrainerNumber(value)
    }
}

#[derive(Debug)]
pub struct InvalidTrainerNumberError(TryFromIntError);

impl Display for InvalidTrainerNumberError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Error for InvalidTrainerNumberError {}

impl From<InvalidTrainerNumberError> for tonic::Status {
    fn from(error: InvalidTrainerNumberError) -> Self {
        tracing::debug!(message = "Invalid Trainer number", %error);
        tonic::Status::invalid_argument("Invalid Trainer number")
    }
}

impl TryFrom<u32> for TrainerNumber {
    type Error = InvalidTrainerNumberError;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        Ok(TrainerNumber(
            u16::try_from(value).map_err(InvalidTrainerNumberError)?,
        ))
    }
}

impl sqlx::Type<Postgres> for TrainerNumber {
    fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
        <i32 as sqlx::Type<Postgres>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, Postgres> for TrainerNumber {
    fn encode_by_ref(
        &self,
        buf: &mut <Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <i32 as sqlx::Encode<Postgres>>::encode(self.0.into(), buf)
    }
}

impl<'r> sqlx::Decode<'r, Postgres> for TrainerNumber {
    fn decode(
        value: <Postgres as sqlx::Database>::ValueRef<'r>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let i32 = <i32 as sqlx::Decode<Postgres>>::decode(value)?;
        let u16 = u16::try_from(i32)?;
        Ok(TrainerNumber(u16))
    }
}

impl sqlx::postgres::PgHasArrayType for TrainerNumber {
    fn array_type_info() -> sqlx::postgres::PgTypeInfo {
        <i32 as sqlx::postgres::PgHasArrayType>::array_type_info()
    }
}
