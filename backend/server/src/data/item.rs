use crate::data::proto;

#[derive(sqlx::Type, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[sqlx(type_name = "safari_item", rename_all = "snake_case")]
pub enum Item {
    Pokeball,
    Superball,
    Hyperball,
    Diveball,
    Quickball,
}

impl From<Item> for proto::Item {
    fn from(value: Item) -> Self {
        match value {
            Item::Pokeball => proto::Item::Pokeball,
            Item::Superball => proto::Item::Superball,
            Item::Hyperball => proto::Item::Hyperball,
            Item::Diveball => proto::Item::Diveball,
            Item::Quickball => proto::Item::Quickball,
        }
    }
}

impl From<proto::Item> for Item {
    fn from(value: proto::Item) -> Self {
        match value {
            proto::Item::Pokeball => Item::Pokeball,
            proto::Item::Superball => Item::Superball,
            proto::Item::Hyperball => Item::Hyperball,
            proto::Item::Diveball => Item::Diveball,
            proto::Item::Quickball => Item::Quickball,
        }
    }
}

impl From<Item> for i32 {
    fn from(value: Item) -> Self {
        proto::Item::from(value).into()
    }
}
