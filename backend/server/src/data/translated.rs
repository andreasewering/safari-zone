use std::{cmp::Ordering, convert::Infallible, fmt::Display, iter::zip};

use axum::extract::FromRequestParts;
use http::request::Parts;
use tonic::metadata::MetadataMap;

use super::proto;

macro_rules! supported_languages {
    ($($lang:ident),*) => {
        const LANGUAGES: usize = 0 $(+ {let _ = SupportedLanguage::$lang; 1})*;

        #[derive(Debug, Clone, Copy, PartialEq, Eq, sqlx::Type)]
        #[sqlx(type_name = "locale")]
        pub enum SupportedLanguage {
            $($lang),*
        }

        impl SupportedLanguage {
            pub fn all() -> [SupportedLanguage; LANGUAGES] {
                [$(SupportedLanguage::$lang),*]
            }
        }
    };
}

supported_languages!(DE, EN, FR);

impl Default for SupportedLanguage {
    fn default() -> Self {
        Self::EN
    }
}

impl SupportedLanguage {
    pub fn from_grpc_headers(headers: &MetadataMap) -> Self {
        headers
            .get("accept-language")
            .and_then(|header| header.to_str().ok())
            .map(match_accept_language)
            .unwrap_or_default()
    }

    fn to_str(self) -> &'static str {
        match self {
            SupportedLanguage::DE => "de",
            SupportedLanguage::EN => "en",
            SupportedLanguage::FR => "fr",
        }
    }
}

impl<S> FromRequestParts<S> for SupportedLanguage
where
    S: Sync,
{
    type Rejection = Infallible;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        // Extract the language from the accept-language header
        let language = parts
            .headers
            .get("accept-language")
            .and_then(|header| header.to_str().ok())
            .map(match_accept_language)
            .unwrap_or_default();
        Ok(language)
    }
}

impl Display for SupportedLanguage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.to_str())
    }
}

impl From<proto::Locale> for SupportedLanguage {
    fn from(value: proto::Locale) -> Self {
        match value {
            proto::Locale::En => SupportedLanguage::EN,
            proto::Locale::De => SupportedLanguage::DE,
            proto::Locale::Fr => SupportedLanguage::FR,
        }
    }
}

impl From<SupportedLanguage> for proto::Locale {
    fn from(value: SupportedLanguage) -> Self {
        match value {
            SupportedLanguage::EN => proto::Locale::En,
            SupportedLanguage::DE => proto::Locale::De,
            SupportedLanguage::FR => proto::Locale::Fr,
        }
    }
}

fn match_accept_language(accept_language_header: &str) -> SupportedLanguage {
    let mut accepted_languages = accept_language::parse_with_quality(accept_language_header);
    accepted_languages.sort_by(|(_, q1), (_, q2)| q2.partial_cmp(q1).unwrap_or(Ordering::Less));
    for (language, _) in accepted_languages.into_iter() {
        if let Some(supported_language) = match_language(&language) {
            return supported_language;
        }
    }
    SupportedLanguage::EN
}

fn match_language(language: &str) -> Option<SupportedLanguage> {
    SupportedLanguage::all()
        .into_iter()
        .find(|supported| language.starts_with(supported.to_str()))
}

#[derive(Debug, Clone, Default)]
pub struct Translated<T> {
    pub en: T,
    pub de: T,
    pub fr: T,
}

impl<T> Translated<T> {
    pub fn get(&self, locale: SupportedLanguage) -> &T {
        match locale {
            SupportedLanguage::DE => &self.de,
            SupportedLanguage::EN => &self.en,
            SupportedLanguage::FR => &self.fr,
        }
    }

    pub fn from_array_agg(locales: Vec<SupportedLanguage>, values: Vec<T>) -> Option<Self> {
        let mut translated: Translated<Option<T>> = Default::default();
        for (locale, value) in zip(locales.into_iter(), values.into_iter()) {
            match locale {
                SupportedLanguage::DE => translated.de = Some(value),
                SupportedLanguage::EN => translated.en = Some(value),
                SupportedLanguage::FR => translated.fr = Some(value),
            }
        }
        let de = translated.de?;
        let en = translated.en?;
        let fr = translated.fr?;
        Some(Translated { de, en, fr })
    }

    pub fn into_array_agg(self) -> ([SupportedLanguage; LANGUAGES], [T; LANGUAGES]) {
        (SupportedLanguage::all(), [self.de, self.en, self.fr])
    }
}

impl Translated<String> {
    pub fn into_proto(self) -> super::proto::Translated {
        let Translated { de, en, fr } = self;
        super::proto::Translated { de, en, fr }
    }

    pub fn from_proto(super::proto::Translated { de, en, fr }: super::proto::Translated) -> Self {
        Translated { de, en, fr }
    }
}

impl<T: Clone> Translated<T> {
    #[cfg(test)]
    pub fn from_constant(value: T) -> Self {
        Translated {
            en: value.clone(),
            de: value.clone(),
            fr: value,
        }
    }
}

#[cfg(test)]
pub trait RequestExt {
    fn with_locale(self, locale: SupportedLanguage) -> Self;
}

#[cfg(test)]
impl<T> RequestExt for tonic::Request<T> {
    fn with_locale(mut self, locale: SupportedLanguage) -> Self {
        let locale = match locale {
            SupportedLanguage::DE => "de",
            SupportedLanguage::EN => "en",
            SupportedLanguage::FR => "fr",
        };
        self.metadata_mut().insert(
            "accept-language",
            tonic::metadata::MetadataValue::from_static(locale),
        );
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn accept_language_simple() {
        assert_eq!(match_accept_language("de"), SupportedLanguage::DE);
        assert_eq!(match_accept_language("en"), SupportedLanguage::EN);
    }

    #[test]
    fn accept_language_default() {
        assert_eq!(match_accept_language("ko"), SupportedLanguage::EN);
    }

    #[test]
    fn accept_language_prefix() {
        assert_eq!(match_accept_language("de-DE"), SupportedLanguage::DE);
    }

    #[test]
    fn accept_language_with_quality() {
        assert_eq!(
            match_accept_language("en-US, de-DE;q=0.5"),
            SupportedLanguage::EN
        );
        assert_eq!(
            match_accept_language("ko, de-DE;q=0.5, en-US;q=0.2"),
            SupportedLanguage::DE
        );
        assert_eq!(
            match_accept_language("ko, en-EN;q=0.5, de-DE;q=0.2"),
            SupportedLanguage::EN
        );
    }
}
