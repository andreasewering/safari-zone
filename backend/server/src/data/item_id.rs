use std::fmt::Display;

use serde::{Deserialize, Serialize};
use sqlx::Postgres;

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct ItemId(i64);

impl Display for ItemId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl From<i64> for ItemId {
    fn from(value: i64) -> Self {
        ItemId(value)
    }
}

impl From<ItemId> for i64 {
    fn from(ItemId(i64): ItemId) -> Self {
        i64
    }
}

impl sqlx::Type<Postgres> for ItemId {
    fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
        <i64 as sqlx::Type<Postgres>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, Postgres> for ItemId {
    fn encode_by_ref(
        &self,
        buf: &mut <Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <i64 as sqlx::Encode<Postgres>>::encode(self.0, buf)
    }
}

impl<'r> sqlx::Decode<'r, Postgres> for ItemId {
    fn decode(
        value: <Postgres as sqlx::Database>::ValueRef<'r>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let i64 = <i64 as sqlx::Decode<Postgres>>::decode(value)?;
        Ok(ItemId(i64))
    }
}

impl sqlx::postgres::PgHasArrayType for ItemId {
    fn array_type_info() -> sqlx::postgres::PgTypeInfo {
        <i64 as sqlx::postgres::PgHasArrayType>::array_type_info()
    }
}
