use std::{f32::consts::PI, fmt::Display};

/// Represents throwing an item in a particular direction in a 3-d space.
/// Invariants:
///     - vector_x/vector_y are in the unit square (representing a 2-d direction)
///     - angle_radians is between 0 (throw item exactly in the 2-d plane)
///         and PI (throw item upwards on the z-axis)
///
/// With this, it is possible to easily limit the distance an item can be thrown
/// to avoid cheating.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Throw {
    vector_x: f32,
    vector_y: f32,
    angle_radians: f32,
}

impl Throw {
    pub fn new(
        (vector_x, vector_y): (f32, f32),
        angle_radians: f32,
    ) -> Result<Self, InvalidThrowError> {
        if !(vector_x.abs() <= 1.0 && vector_y.abs() <= 1.0) {
            return Err(InvalidThrowError::InvalidVector(vector_x, vector_y));
        }
        if !(0.0..=PI).contains(&angle_radians) {
            return Err(InvalidThrowError::InvalidAngle(angle_radians));
        }
        Ok(Throw {
            vector_x,
            vector_y,
            angle_radians,
        })
    }

    pub fn vector_x(&self) -> f32 {
        self.vector_x
    }

    pub fn vector_y(&self) -> f32 {
        self.vector_y
    }

    pub fn angle_radians(&self) -> f32 {
        self.angle_radians
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum InvalidThrowError {
    InvalidVector(f32, f32),
    InvalidAngle(f32),
}

#[cfg(test)]
impl quickcheck::Arbitrary for Throw {
    fn arbitrary(g: &mut quickcheck::Gen) -> Self {
        loop {
            let x = f32::arbitrary(g);
            let y = f32::arbitrary(g);
            let angle = f32::arbitrary(g);
            if let Ok(throw) = Self::new((x, y), angle) {
                return throw;
            }
        }
    }
}

impl Display for InvalidThrowError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            InvalidThrowError::InvalidVector(x, y) => f.write_fmt(format_args!(
                "Expected vector_x/vector_y to be normalized between -1 and 1 but got ({x}, {y})"
            )),
            InvalidThrowError::InvalidAngle(angle) => f.write_fmt(format_args!(
                "Expected throw angle to be between 0 and PI but got {angle}"
            )),
        }
    }
}

impl std::error::Error for InvalidThrowError {}

#[cfg(test)]
mod tests {
    use core::f32;

    use crate::data::throw::InvalidThrowError;

    use super::Throw;

    #[test]
    fn vector_out_of_unit_range_is_invalid_throw() {
        assert_eq!(
            Throw::new((0.0, 1.01), 1.3),
            Err(InvalidThrowError::InvalidVector(0.0, 1.01))
        );
        assert_eq!(
            Throw::new((-1.01, 0.0), 1.3),
            Err(InvalidThrowError::InvalidVector(-1.01, 0.0))
        );
        assert_eq!(
            Throw::new((12.345, -2.34), 1.3),
            Err(InvalidThrowError::InvalidVector(12.345, -2.34))
        );
    }

    #[test]
    fn negative_angle_is_invalid_throw() {
        assert_eq!(
            Throw::new((0.0, 1.0), -0.1),
            Err(InvalidThrowError::InvalidAngle(-0.1))
        );
    }

    #[test]
    fn nan_angle_is_invalid_throw() {
        assert!(matches!(
            Throw::new((0.0, 1.0), f32::NAN),
            Err(InvalidThrowError::InvalidAngle(_))
        ));
    }
}
