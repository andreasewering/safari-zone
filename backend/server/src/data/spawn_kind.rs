use crate::arceus::proto::arceus;
use serde::Deserialize;

#[derive(
    sqlx::Type,
    Deserialize,
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    Hash,
    PartialOrd,
    Ord,
    deepsize::DeepSizeOf,
    arbitrary::Arbitrary,
)]
#[sqlx(type_name = "spawn_kind", rename_all = "lowercase")]
pub enum SpawnKind {
    Grass,
    Water,
}

impl From<SpawnKind> for arceus::SpawnKind {
    fn from(value: SpawnKind) -> Self {
        match value {
            SpawnKind::Grass => arceus::SpawnKind::Grass,
            SpawnKind::Water => arceus::SpawnKind::Water,
        }
    }
}

impl From<arceus::SpawnKind> for SpawnKind {
    fn from(value: arceus::SpawnKind) -> Self {
        match value {
            arceus::SpawnKind::Grass => SpawnKind::Grass,
            arceus::SpawnKind::Water => SpawnKind::Water,
        }
    }
}
