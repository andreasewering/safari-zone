use std::{error::Error, fmt::Display, num::TryFromIntError, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::Postgres;

#[derive(Debug, Copy, Clone, Deserialize, Serialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PokemonNumber(pub u16);

impl Display for PokemonNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl FromStr for PokemonNumber {
    type Err = <u16 as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        u16::from_str(s).map(PokemonNumber)
    }
}

#[derive(Debug)]
pub struct InvalidPokemonNumberError(TryFromIntError);

impl Display for InvalidPokemonNumberError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Error for InvalidPokemonNumberError {}

impl From<InvalidPokemonNumberError> for tonic::Status {
    fn from(error: InvalidPokemonNumberError) -> Self {
        tracing::debug!(message = "Invalid pokemon number", %error);
        tonic::Status::invalid_argument("Invalid pokemon number")
    }
}

impl From<PokemonNumber> for u32 {
    fn from(value: PokemonNumber) -> Self {
        value.0.into()
    }
}

impl TryFrom<u32> for PokemonNumber {
    type Error = InvalidPokemonNumberError;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        Ok(PokemonNumber(
            u16::try_from(value).map_err(InvalidPokemonNumberError)?,
        ))
    }
}

impl sqlx::Type<Postgres> for PokemonNumber {
    fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
        <i32 as sqlx::Type<Postgres>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, Postgres> for PokemonNumber {
    fn encode_by_ref(
        &self,
        buf: &mut <Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <i32 as sqlx::Encode<Postgres>>::encode(self.0.into(), buf)
    }
}

impl<'r> sqlx::Decode<'r, Postgres> for PokemonNumber {
    fn decode(
        value: <Postgres as sqlx::Database>::ValueRef<'r>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let i32 = <i32 as sqlx::Decode<Postgres>>::decode(value)?;
        let u16 = u16::try_from(i32)?;
        Ok(PokemonNumber(u16))
    }
}

impl sqlx::postgres::PgHasArrayType for PokemonNumber {
    fn array_type_info() -> sqlx::postgres::PgTypeInfo {
        <i32 as sqlx::postgres::PgHasArrayType>::array_type_info()
    }
}
