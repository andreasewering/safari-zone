use std::ops::{Add, Sub};

use super::Direction;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, deepsize::DeepSizeOf)]
pub struct Position {
    pub x: i32,
    pub y: i32,
    pub layer_number: i32,
}

impl Position {
    pub fn move_in_dir(&self, dir: Direction) -> Self {
        let mut new_position = *self;
        match dir {
            Direction::L => new_position.x -= 1,
            Direction::R => new_position.x += 1,
            Direction::U => new_position.y -= 1,
            Direction::D => new_position.y += 1,
        };
        new_position
    }

    pub fn abs(self) -> Self {
        Self {
            x: self.x.abs(),
            y: self.y.abs(),
            layer_number: self.layer_number.abs(),
        }
    }
}

impl Sub<Position> for Position {
    type Output = Position;

    fn sub(self, rhs: Position) -> Self::Output {
        Position {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            layer_number: self.layer_number - rhs.layer_number,
        }
    }
}

impl Add<Position> for Position {
    type Output = Position;

    fn add(self, rhs: Position) -> Self::Output {
        Position {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            layer_number: self.layer_number + rhs.layer_number,
        }
    }
}
