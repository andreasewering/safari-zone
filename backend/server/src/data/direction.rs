use std::fmt::Display;

use crate::data::proto;
use enumset::EnumSetType;
use rand_distr::Distribution;
use serde::Deserialize;

#[derive(sqlx::Type, Debug, PartialOrd, Ord, Hash, EnumSetType)]
#[sqlx(type_name = "direction")]
pub enum Direction {
    L,
    R,
    U,
    D,
}

static DIRECTIONS: [Direction; 4] = [Direction::L, Direction::R, Direction::U, Direction::D];

impl Direction {
    pub fn iter() -> impl Iterator<Item = Self> {
        DIRECTIONS.into_iter()
    }

    pub fn turn_180(self) -> Self {
        match self {
            Direction::L => Direction::R,
            Direction::R => Direction::L,
            Direction::U => Direction::D,
            Direction::D => Direction::U,
        }
    }
}

#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    deepsize::DeepSizeOf,
    Default,
    Deserialize,
    arbitrary::Arbitrary,
)]
pub struct Directional<T> {
    pub left: T,
    pub right: T,
    pub up: T,
    pub down: T,
}

impl<T> Directional<T> {
    pub fn map<U, F>(self, f: F) -> Directional<U>
    where
        F: Fn(T) -> U,
    {
        Directional {
            left: f(self.left),
            right: f(self.right),
            up: f(self.up),
            down: f(self.down),
        }
    }

    pub fn fold<F>(self, mut f: F) -> T
    where
        F: FnMut(T, T) -> T,
    {
        let temp_1 = f(self.up, self.left);
        let temp_2 = f(temp_1, self.right);
        f(temp_2, self.down)
    }

    pub fn try_map<U, F, E>(self, f: F) -> Result<Directional<U>, E>
    where
        F: Fn(Direction, T) -> Result<U, E>,
    {
        let Directional {
            left,
            right,
            up,
            down,
        } = self.map_with_direction(f);
        Ok(Directional {
            left: left?,
            right: right?,
            up: up?,
            down: down?,
        })
    }

    pub fn map_with_direction<U, F>(self, f: F) -> Directional<U>
    where
        F: Fn(Direction, T) -> U,
    {
        Directional {
            left: f(Direction::L, self.left),
            right: f(Direction::R, self.right),
            up: f(Direction::U, self.up),
            down: f(Direction::D, self.down),
        }
    }

    pub fn turn_180(self) -> Directional<T> {
        Directional {
            left: self.right,
            right: self.left,
            up: self.down,
            down: self.up,
        }
    }

    pub fn get(&self, direction: Direction) -> &T {
        match direction {
            Direction::L => &self.left,
            Direction::R => &self.right,
            Direction::U => &self.up,
            Direction::D => &self.down,
        }
    }

    pub fn combine<U, W, F>(self, other: Directional<U>, f: F) -> Directional<W>
    where
        F: Fn(T, U) -> W,
    {
        Directional {
            left: f(self.left, other.left),
            right: f(self.right, other.right),
            up: f(self.up, other.up),
            down: f(self.down, other.down),
        }
    }

    pub fn from_fn<F>(f: F) -> Self
    where
        F: Fn(Direction) -> T,
    {
        Directional::default().map_with_direction(|dir, ()| f(dir))
    }
}

impl<T: Clone> Directional<T> {
    pub fn from_single(single: T) -> Self {
        Self {
            left: single.clone(),
            right: single.clone(),
            up: single.clone(),
            down: single,
        }
    }
}

impl Distribution<Direction> for rand::distr::StandardUniform {
    fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Direction {
        let index = rng.random_range(0..DIRECTIONS.len() - 1);
        DIRECTIONS[index]
    }
}

impl From<Direction> for proto::Direction {
    fn from(value: Direction) -> Self {
        match value {
            Direction::L => proto::Direction::L,
            Direction::R => proto::Direction::R,
            Direction::U => proto::Direction::U,
            Direction::D => proto::Direction::D,
        }
    }
}

impl From<Direction> for i32 {
    fn from(value: Direction) -> Self {
        proto::Direction::from(value).into()
    }
}

impl From<proto::Direction> for Direction {
    fn from(value: proto::Direction) -> Self {
        match value {
            proto::Direction::L => Direction::L,
            proto::Direction::R => Direction::R,
            proto::Direction::U => Direction::U,
            proto::Direction::D => Direction::D,
        }
    }
}

impl Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let description = match self {
            Direction::L => "left",
            Direction::R => "right",
            Direction::U => "up",
            Direction::D => "down",
        };
        f.write_str(description)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn map_directional() {
        let directional = Directional {
            left: 0,
            right: 1,
            up: 2,
            down: 3,
        };
        assert_eq!(
            directional.map(|x| x + 1),
            Directional {
                left: 1,
                right: 2,
                up: 3,
                down: 4
            }
        );
    }

    #[test]
    fn map_with_direction_get_direction() {
        let directional = Directional {
            left: 0,
            right: 1,
            up: 2,
            down: 3,
        };
        let mapped = directional.map_with_direction(|dir, x| directional.get(dir) + x);
        assert_eq!(
            mapped,
            Directional {
                left: 0,
                right: 2,
                up: 4,
                down: 6
            }
        )
    }

    #[test]
    fn combine_directional() {
        let directional = Directional {
            left: 0,
            right: 1,
            up: 2,
            down: 3,
        };
        assert_eq!(
            directional.combine(directional, |x, y| x + y),
            Directional {
                left: 0,
                right: 2,
                up: 4,
                down: 6
            }
        )
    }
}
