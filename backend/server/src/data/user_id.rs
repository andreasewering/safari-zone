use std::fmt::Display;

use serde::{Deserialize, Serialize};
use sqlx::Postgres;

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct UserId(i64);

impl Display for UserId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl From<i64> for UserId {
    fn from(value: i64) -> Self {
        UserId(value)
    }
}

impl From<u64> for UserId {
    fn from(value: u64) -> Self {
        UserId(value as i64)
    }
}

impl From<UserId> for i64 {
    fn from(UserId(i64): UserId) -> Self {
        i64
    }
}

impl From<UserId> for u64 {
    fn from(UserId(i64): UserId) -> Self {
        i64 as u64
    }
}

impl PartialEq<UserId> for i64 {
    fn eq(&self, other: &UserId) -> bool {
        *self == other.0
    }
}

impl PartialEq<UserId> for u64 {
    fn eq(&self, other: &UserId) -> bool {
        *self == other.0 as u64
    }
}

impl sqlx::Type<Postgres> for UserId {
    fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
        <i64 as sqlx::Type<Postgres>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, Postgres> for UserId {
    fn encode_by_ref(
        &self,
        buf: &mut <Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <i64 as sqlx::Encode<Postgres>>::encode(self.0, buf)
    }
}

impl<'r> sqlx::Decode<'r, Postgres> for UserId {
    fn decode(
        value: <Postgres as sqlx::Database>::ValueRef<'r>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let i64 = <i64 as sqlx::Decode<Postgres>>::decode(value)?;
        Ok(UserId(i64))
    }
}

impl sqlx::postgres::PgHasArrayType for UserId {
    fn array_type_info() -> sqlx::postgres::PgTypeInfo {
        <i64 as sqlx::postgres::PgHasArrayType>::array_type_info()
    }
}
