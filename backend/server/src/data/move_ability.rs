use enumset::{EnumSet, EnumSetType};
use serde::{Deserialize, Serialize};

#[derive(Debug, EnumSetType, Deserialize, Serialize, arbitrary::Arbitrary)]
#[enumset(repr = "u8", serialize_repr = "list")]
// Max 8 cases allowed! If more cases are necessary, we will need to restructure the code
// or use larger integers.
pub enum MoveAbility {
    Walk,
    Swim,
    Fly,
    Run,
    Bike,
}

/// MoveAbility is a bit mask.
/// This means that multiple abilities such as swimming and flying
/// are encoded into a single integer by using multiples of two.
pub type MoveAbilities = EnumSet<MoveAbility>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn move_abilities_work_for_single_move_ability() {
        let no_abilities = MoveAbilities::default();
        assert!(!no_abilities.contains(MoveAbility::Walk));
        assert!(!no_abilities.contains(MoveAbility::Swim));
        assert!(!no_abilities.contains(MoveAbility::Fly));

        let can_walk = no_abilities | MoveAbility::Walk;
        assert!(can_walk.contains(MoveAbility::Walk));
        assert!(!can_walk.contains(MoveAbility::Swim));
        assert!(!can_walk.contains(MoveAbility::Fly));

        let can_swim = no_abilities | MoveAbility::Swim;
        assert!(!can_swim.contains(MoveAbility::Walk));
        assert!(can_swim.contains(MoveAbility::Swim));
        assert!(!can_swim.contains(MoveAbility::Fly));

        let can_fly = no_abilities | MoveAbility::Fly;
        assert!(!can_fly.contains(MoveAbility::Walk));
        assert!(!can_fly.contains(MoveAbility::Swim));
        assert!(can_fly.contains(MoveAbility::Fly));
    }

    #[test]
    fn move_abilities_work_for_multiple_move_abilities() {
        let swim_and_fly = MoveAbilities::default() | MoveAbility::Swim | MoveAbility::Fly;

        assert!(!swim_and_fly.contains(MoveAbility::Walk));
        assert!(swim_and_fly.contains(MoveAbility::Swim));
        assert!(swim_and_fly.contains(MoveAbility::Fly));

        let walk_and_fly = MoveAbilities::default() | MoveAbility::Walk | MoveAbility::Fly;
        assert!(walk_and_fly.contains(MoveAbility::Walk));
        assert!(!walk_and_fly.contains(MoveAbility::Swim));
        assert!(walk_and_fly.contains(MoveAbility::Fly));

        let everything =
            MoveAbilities::default() | MoveAbility::Swim | MoveAbility::Walk | MoveAbility::Fly;
        assert!(everything.contains(MoveAbility::Walk));
        assert!(everything.contains(MoveAbility::Swim));
        assert!(everything.contains(MoveAbility::Fly));
    }
}
