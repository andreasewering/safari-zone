use serde::{Deserialize, Serialize};

use crate::kangaskhan;

#[derive(Debug, Deserialize, Serialize, Clone, Copy)]
pub enum PokemonType {
    // Reserved for moves like Curse in the future
    Unknown,
    Bug,
    Dragon,
    Electric,
    Fairy,
    Fighting,
    Fire,
    Flying,
    Ghost,
    Grass,
    Ground,
    Ice,
    Normal,
    Poison,
    Psychic,
    Rock,
    Steel,
    Water,
}

impl From<kangaskhan::proto::kangaskhan::Type> for PokemonType {
    fn from(value: kangaskhan::proto::kangaskhan::Type) -> Self {
        match value {
            kangaskhan::proto::kangaskhan::Type::Unknown => PokemonType::Unknown,
            kangaskhan::proto::kangaskhan::Type::Bug => PokemonType::Bug,
            kangaskhan::proto::kangaskhan::Type::Dragon => PokemonType::Dragon,
            kangaskhan::proto::kangaskhan::Type::Electric => PokemonType::Electric,
            kangaskhan::proto::kangaskhan::Type::Fairy => PokemonType::Fairy,
            kangaskhan::proto::kangaskhan::Type::Fighting => PokemonType::Fighting,
            kangaskhan::proto::kangaskhan::Type::Fire => PokemonType::Fire,
            kangaskhan::proto::kangaskhan::Type::Flying => PokemonType::Flying,
            kangaskhan::proto::kangaskhan::Type::Ghost => PokemonType::Ghost,
            kangaskhan::proto::kangaskhan::Type::Grass => PokemonType::Grass,
            kangaskhan::proto::kangaskhan::Type::Ground => PokemonType::Ground,
            kangaskhan::proto::kangaskhan::Type::Ice => PokemonType::Ice,
            kangaskhan::proto::kangaskhan::Type::Normal => PokemonType::Normal,
            kangaskhan::proto::kangaskhan::Type::Poison => PokemonType::Poison,
            kangaskhan::proto::kangaskhan::Type::Psychic => PokemonType::Psychic,
            kangaskhan::proto::kangaskhan::Type::Rock => PokemonType::Rock,
            kangaskhan::proto::kangaskhan::Type::Steel => PokemonType::Steel,
            kangaskhan::proto::kangaskhan::Type::Water => PokemonType::Water,
        }
    }
}

impl From<PokemonType> for kangaskhan::proto::kangaskhan::Type {
    fn from(value: PokemonType) -> Self {
        match value {
            PokemonType::Unknown => kangaskhan::proto::kangaskhan::Type::Unknown,
            PokemonType::Bug => kangaskhan::proto::kangaskhan::Type::Bug,
            PokemonType::Dragon => kangaskhan::proto::kangaskhan::Type::Dragon,
            PokemonType::Electric => kangaskhan::proto::kangaskhan::Type::Electric,
            PokemonType::Fairy => kangaskhan::proto::kangaskhan::Type::Fairy,
            PokemonType::Fighting => kangaskhan::proto::kangaskhan::Type::Fighting,
            PokemonType::Fire => kangaskhan::proto::kangaskhan::Type::Fire,
            PokemonType::Flying => kangaskhan::proto::kangaskhan::Type::Flying,
            PokemonType::Ghost => kangaskhan::proto::kangaskhan::Type::Ghost,
            PokemonType::Grass => kangaskhan::proto::kangaskhan::Type::Grass,
            PokemonType::Ground => kangaskhan::proto::kangaskhan::Type::Ground,
            PokemonType::Ice => kangaskhan::proto::kangaskhan::Type::Ice,
            PokemonType::Normal => kangaskhan::proto::kangaskhan::Type::Normal,
            PokemonType::Poison => kangaskhan::proto::kangaskhan::Type::Poison,
            PokemonType::Psychic => kangaskhan::proto::kangaskhan::Type::Psychic,
            PokemonType::Rock => kangaskhan::proto::kangaskhan::Type::Rock,
            PokemonType::Steel => kangaskhan::proto::kangaskhan::Type::Steel,
            PokemonType::Water => kangaskhan::proto::kangaskhan::Type::Water,
        }
    }
}
