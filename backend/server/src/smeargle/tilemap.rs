mod tilemap_config;

use std::collections::{HashMap, HashSet};
use std::path::{Path, PathBuf};

use image::{ImageError, Rgba, RgbaImage};
use nonempty::NonEmpty;

use crate::data::Position;
use crate::smeargle::data::{Area, Layer, TileId};
use crate::smeargle::hash::make_image_hash;
use tilemap_config::TilemapConfigLookup;

pub struct Tilemap {
    pub image: RgbaImage,
    pub hash: String,
    pub identifier: String,
    pub offset: (i32, i32),
    pub layers: Vec<Layer>,
}

pub fn make_tilemap_image(
    cfgs: impl TilemapConfigLookup,
    identifier: String,
    tiles: &NonEmpty<(TileId, Position)>,
) -> Tilemap {
    let (some_tile_id, some_pos) = tiles.head;
    let mut area = Area::new(some_pos.x, some_pos.y);
    let mut layers: HashSet<Layer> = HashSet::new();
    let mut insert = |(tile_id, pos): &(TileId, Position)| {
        area.insert(pos.x, pos.y);
        if let Some(cfg) = cfgs.get_tile(*tile_id) {
            layers.insert(Layer {
                number: pos.layer_number,
                type_: cfg.layer_type,
            });
        }
    };
    insert(&(some_tile_id, some_pos));
    tiles.tail.iter().for_each(insert);

    let mut layers: Vec<Layer> = layers.into_iter().collect();
    layers.sort();
    let layer_map: HashMap<Layer, u32> = layers.clone().into_iter().zip(0..).collect();
    let image_width_multiplier = {
        let dividend = layers.len() as u32;
        dividend.div_ceil(2)
    };

    let image_width = area.width() * image_width_multiplier;
    let mut image = RgbaImage::new(image_width, area.height());
    tiles
        .iter()
        .filter_map(|(tile_id, pos)| {
            let (x, y) = area.relative_to(pos.x, pos.y).ok()?;
            let cfg = cfgs.get_tile(*tile_id)?;
            let layer_index = layer_map.get(&Layer {
                number: pos.layer_number,
                type_: cfg.layer_type,
            })?;
            let px = if layer_index % 2 == 0 {
                tile_id.to_red_green()
            } else {
                tile_id.to_blue_alpha()
            };
            Some((x * image_width_multiplier + layer_index / 2, y, px))
        })
        .for_each(|(x, y, Rgba([r, g, b, a]))| {
            let px_prev = image.get_pixel_mut_checked(x, y);
            if let Some(Rgba([r_prev, g_prev, b_prev, a_prev])) = px_prev {
                if *r_prev == 0 {
                    *r_prev = r;
                }
                if *g_prev == 0 {
                    *g_prev = g;
                }
                if *b_prev == 0 {
                    *b_prev = b;
                }
                if *a_prev == 0 {
                    *a_prev = a;
                }
            }
        });

    let hash = make_image_hash(&image);
    let offset = area.top_left();
    Tilemap {
        image,
        hash,
        identifier,
        offset,
        layers,
    }
}

pub fn tilemap_directory(public_dir: &Path, identifier: &str) -> PathBuf {
    let mut path = PathBuf::new();
    path.push(public_dir);
    path.push("generated");
    path.push(identifier);
    path
}

fn tilemap_path(public_dir: &Path, identifier: &str, hash: &str) -> PathBuf {
    let mut path = tilemap_directory(public_dir, identifier);
    path.push(format!("{}.png", hash));
    path
}

pub fn public_path(public_base_path: &Path, identifier: &str, hash: &str) -> String {
    tilemap_path(public_base_path, identifier, hash)
        .to_string_lossy()
        .to_string()
        .replace('\\', "/")
}

impl Tilemap {
    pub async fn write(self, public_dir: &Path) -> Result<PathBuf, ImageError> {
        let dir = tilemap_directory(public_dir, &self.identifier);
        // ignore error if directory did not exist yet.
        // this is viable since each map only has a single tilemap image.
        let _ = tokio::fs::remove_dir_all(&dir).await;
        tracing::debug!(message = "Deleted directory", ?dir);
        tokio::fs::create_dir_all(dir).await?;

        let path = tilemap_path(public_dir, &self.identifier, &self.hash);
        tracing::debug!(message = "Writing tilemap to path", ?path);

        self.image.save(&path)?;
        Ok(path)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        data::Position,
        smeargle::{data::LayerType, tilemap::tilemap_config::TilemapConfig},
    };

    use super::*;

    fn config() -> impl TilemapConfigLookup {
        [
            (
                TileId::from(1),
                TilemapConfig {
                    layer_type: LayerType::Ground,
                },
            ),
            (
                TileId::from(2),
                TilemapConfig {
                    layer_type: LayerType::Ground,
                },
            ),
            (
                TileId::from(3),
                TilemapConfig {
                    layer_type: LayerType::Decoration,
                },
            ),
            (
                TileId::from(4),
                TilemapConfig {
                    layer_type: LayerType::Decoration,
                },
            ),
            (
                TileId::from(5),
                TilemapConfig {
                    layer_type: LayerType::Overlay,
                },
            ),
        ]
        .into_iter()
        .collect::<HashMap<_, _>>()
    }

    #[test]
    fn can_reconstruct_tiles_from_image() {
        let tiles: NonEmpty<(TileId, Position)> = NonEmpty::from_vec(vec![
            (
                TileId::from(1),
                Position {
                    x: 0,
                    y: 0,
                    layer_number: 0,
                },
            ),
            (
                TileId::from(2),
                Position {
                    x: 1,
                    y: 0,
                    layer_number: 0,
                },
            ),
            (
                TileId::from(3),
                Position {
                    x: 0,
                    y: 0,
                    layer_number: 0,
                },
            ),
            (
                TileId::from(4),
                Position {
                    x: 1,
                    y: 0,
                    layer_number: 0,
                },
            ),
            (
                TileId::from(5),
                Position {
                    x: 1,
                    y: 0,
                    layer_number: 0,
                },
            ),
        ])
        .unwrap();
        let tilemap = make_tilemap_image(config(), "identifier".to_owned(), &tiles);

        assert_eq!(tilemap.image.dimensions(), (4, 1));

        let px_0_0 = tilemap.image.get_pixel(0, 0);
        assert_eq!(TileId::from_red_green(*px_0_0), TileId::from(1));
        assert_eq!(TileId::from_blue_alpha(*px_0_0), TileId::from(3));

        let px_1_0 = tilemap.image.get_pixel(1, 0);
        assert_eq!(*px_1_0, Rgba([0, 0, 0, 0]));

        let px_2_0 = tilemap.image.get_pixel(2, 0);
        assert_eq!(TileId::from_red_green(*px_2_0), TileId::from(2));
        assert_eq!(TileId::from_blue_alpha(*px_2_0), TileId::from(4));

        let px_3_0 = tilemap.image.get_pixel(3, 0);
        assert_eq!(TileId::from_red_green(*px_3_0), TileId::from(5));
    }
}
