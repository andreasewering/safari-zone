use crate::{
    data::{MapId, Position, SpawnKind},
    error::AppResult,
};

#[async_trait::async_trait]
pub trait SpawnKindProvider {
    async fn spawn_kinds_for_map_id(
        &self,
        map_id: MapId,
    ) -> AppResult<Box<dyn MapSpawnKindProvider>>;

    async fn spawn_kinds(&self, map_id: MapId) -> AppResult<Vec<(Position, SpawnKind)>> {
        self.spawn_kinds_for_map_id(map_id)
            .await
            .map(|map_spawn_kind_provider| map_spawn_kind_provider.spawn_kinds())
    }
}

pub trait MapSpawnKindProvider {
    fn spawn_kinds(&self) -> Vec<(Position, SpawnKind)>;
}

#[cfg(test)]
pub mod test_impl {
    use std::{collections::HashMap, fmt::Display, sync::Mutex};

    use fxhash::FxBuildHasher;

    use crate::error::AppError;

    use super::*;

    #[derive(Default)]
    pub struct TestSpawnKindProvider {
        inner: Mutex<HashMap<MapId, TestMapSpawnKindProvider>>,
    }

    impl TestSpawnKindProvider {
        pub fn upsert_map<I: IntoIterator<Item = (Position, SpawnKind)>>(
            &self,
            map_id: MapId,
            spawn_kinds: I,
        ) {
            let mut inner = self
                .inner
                .lock()
                .expect("Failed to acquire lock on TestSpawnKindProvider");
            let entry = inner.entry(map_id).or_default();
            for (position, spawn_kind) in spawn_kinds {
                entry.inner.insert(position, spawn_kind);
            }
        }
    }

    #[derive(Debug, Clone)]
    enum Error {
        NoSpawnKindsLoaded(MapId),
        AcquireLock,
    }

    #[async_trait::async_trait]
    impl SpawnKindProvider for TestSpawnKindProvider {
        async fn spawn_kinds_for_map_id(
            &self,
            map_id: MapId,
        ) -> AppResult<Box<dyn MapSpawnKindProvider>> {
            let spawn_kinds = self
                .inner
                .lock()
                .map_err(|_| Error::AcquireLock)?
                .get(&map_id)
                .cloned()
                .ok_or(Error::NoSpawnKindsLoaded(map_id))?;
            Ok(Box::new(spawn_kinds))
        }
    }

    #[derive(Debug, Clone, Default)]
    struct TestMapSpawnKindProvider {
        inner: HashMap<Position, SpawnKind, FxBuildHasher>,
    }

    impl MapSpawnKindProvider for TestMapSpawnKindProvider {
        fn spawn_kinds(&self) -> Vec<(Position, SpawnKind)> {
            self.inner.clone().into_iter().collect()
        }
    }

    impl FromIterator<(Position, SpawnKind)> for TestMapSpawnKindProvider {
        fn from_iter<T: IntoIterator<Item = (Position, SpawnKind)>>(iter: T) -> Self {
            TestMapSpawnKindProvider {
                inner: HashMap::from_iter(iter),
            }
        }
    }

    impl Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Error::NoSpawnKindsLoaded(map_id) => f.write_fmt(format_args!(
                    "No spawn kinds loaded for map_id '{}'",
                    map_id
                )),
                Error::AcquireLock => {
                    f.write_str("Failed to acquire lock for TestSpawnKindProvider")
                }
            }
        }
    }

    impl std::error::Error for Error {}

    impl From<Error> for AppError {
        fn from(value: Error) -> Self {
            AppError::internal(value.to_string())
        }
    }

    #[async_trait::async_trait]
    impl<S, T> SpawnKindProvider for (T, S)
    where
        S: SpawnKindProvider + Send + Sync,
        T: Send + Sync,
    {
        async fn spawn_kinds_for_map_id(
            &self,
            map_id: MapId,
        ) -> AppResult<Box<dyn MapSpawnKindProvider>> {
            self.1.spawn_kinds_for_map_id(map_id).await
        }
    }
}
