use std::{cmp::Ordering, str::FromStr};

use serde::Deserialize;

#[derive(Debug, Clone, Default)]
pub struct Orders {
    left_to_right: Order,
    top_to_bottom: Order,
}

impl Order {
    fn comparison_fn(&self) -> fn(i32, i32) -> Ordering {
        match self {
            Order::Parallel => |_x1, _x2| Ordering::Equal,
            Order::Ascending => |x1: i32, x2| x1.cmp(&x2),
            Order::Descending => |x1, x2: i32| x2.cmp(&x1),
        }
    }
}

impl Orders {
    pub fn sort(&self, vec: &mut [(i32, i32)]) {
        if let (Order::Parallel, Order::Parallel) = (&self.left_to_right, &self.top_to_bottom) {
            return;
        }
        vec.sort_by(|xy1, xy2| {
            let order_x = self.left_to_right.comparison_fn()(xy1.0, xy2.0);
            let order_y = self.top_to_bottom.comparison_fn()(xy1.1, xy2.1);
            order_x.then(order_y)
        });
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Order {
    Ascending,
    Descending,
    Parallel,
}

impl Default for Order {
    fn default() -> Self {
        Self::Parallel
    }
}

impl FromStr for Orders {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "parallel" => Ok(Orders {
                left_to_right: Order::Parallel,
                top_to_bottom: Order::Parallel,
            }),
            "t->b" => Ok(Orders {
                left_to_right: Order::Parallel,
                top_to_bottom: Order::Ascending,
            }),
            "l->r" => Ok(Orders {
                left_to_right: Order::Ascending,
                top_to_bottom: Order::Parallel,
            }),
            "l->r,t->b" => Ok(Orders {
                left_to_right: Order::Ascending,
                top_to_bottom: Order::Ascending,
            }),
            unknown => Err(format!("Unknown order '{}'", unknown)),
        }
    }
}

impl<'de> Deserialize<'de> for Orders {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let as_string = String::deserialize(deserializer)?;
        Self::from_str(&as_string).map_err(serde::de::Error::custom)
    }
}
