use std::{collections::hash_map::DefaultHasher, hash::{Hash, Hasher}};

use image::RgbaImage;

pub fn make_image_hash(image: &RgbaImage) -> String {
    let mut hasher = DefaultHasher::new();
    image.hash(&mut hasher);
    hasher.finish().to_string()
}
