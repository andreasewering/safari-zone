# Smeargle

## Move Rules

There has been a lot of thought that went into optimizing collision detection and in front- and backend.
Being a tile-based game, we can forgo some of the complexity (e.g. floats) and try to get as much information
across in as least bytes as possible.
In this game, there are different ways of moving (walking, running, riding a bike, swimming, ...)
and depending on the way you are currently moving, you may access different tiles from potentially different directions.
Also, while the game is 2d, there are 3d elements, such as stairs, waterfalls and bridges.
Since you can walk on, under and over a bridge (e.g. another bridge) all coordinates have a z-component to it.
However, you can only have one behaviour when moving in a fixed direction - there cannot be a
stair (moving one layer upwards) and a normal ground connection (same layer). After all, you just press
move in that direction, how should the game decide which way to go?
The concrete movement rules should be determined by the tiles of the map, but these are placed on a fixed z-coordinate.
This complicates path finding as the layer below has to be potentially checked - if there is a stair, you may move downwards (same problem with the layer above if we decide to place the stair on the upper layer).

There are two key observations to make here:

1. Each tile may have a complex set of configuration to specify with what movement type it can be accessed from what side and with what movement type it can be left into each direction.
2. For deciding where to go/not to go, we can use a more compact representation - we only need to know for a concrete movement type: can we go in a certain direction and if yes, do we move upwards, downwards or straight?

### Optimizing the compact representation

- Movement in a certain direction can be represented by 2 bits:
  - 00: no movement
  - 01: upwards movement (layer + 1)
  - 10: downwards movement (layer - 1)
  - 11: straight movement (layer + 0)
- Sending this info for each of the 4 directions (up, left, down, right) needs 8 bits
  - UULLDDRR where each of the bit pairs corresponds to one of the cases above
- Sending this info for each movement type needs 8 bits per movement type (probably under 8)
  - Currently: Walk, Run, Bike, Swim
  - e.g. like this (64-bit int): WWWWWWWW RRRRRRRR BBBBBBBB SSSSSSSS **\_\_\_\_** **\_\_\_\_** **\_\_\_\_** **\_\_\_\_**
  - this does fit in a u32 as well, but we would need to change the code for one additional movement type which is not ideal
- We can also avoid sending any tiles that cannot be accessed by any movement type in any direction
- Alternatively, we could send each direction with a bitmask representing the valid movement types:
  - e.g. UUWRBS\_\_ LLWRBS\_\_ DDWRBS\_\_ RRWRBS\_\_
  - this is more efficient and it expresses the invariant that a single tile connection cannot be an upwards and a downwards slope at the same time

### The tile-wise representation

- Each tile configuration needs a field for each direction
  - left
  - right
  - up
  - down
- and these need to have two sub fields
  - incoming
  - outgoing
- and one field per movement type
  - walking
  - running
  - swimming
  - biking
- and each of these has the same 2 bit information as above
  - 00: no movement
  - 01: upwards movement (layer + 1)
  - 10: downwards movement (layer - 1)
  - 11: straight movement (layer + 0)

We can calculate the optimized representation from the tile-wise representation for a concrete map by evaluating/merging
movement rules between neighbours.
