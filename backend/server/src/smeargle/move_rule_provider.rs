use std::ops::BitOr;

use crate::{
    data::{Direction, MapId, MoveAbilities, Position},
    error::AppResult,
};

use super::DirectionalMoveRules;

#[async_trait::async_trait]
pub trait MoveRuleProvider {
    async fn move_rules_for_map_id(
        &self,
        map_id: MapId,
    ) -> AppResult<Box<dyn MapMoveRuleProvider + Sync + Send>>;

    async fn move_in_dir(
        &self,
        map_id: MapId,
        position: Position,
        move_ability: MoveAbilities,
        direction: Direction,
    ) -> AppResult<Option<Position>> {
        self.move_rules_for_map_id(map_id)
            .await
            .map(|map_move_rule_provider| {
                map_move_rule_provider.move_in_dir(position, move_ability, direction)
            })
    }

    async fn get_allowed(
        &self,
        map_id: MapId,
        move_ability: MoveAbilities,
    ) -> AppResult<Vec<Position>> {
        self.move_rules_for_map_id(map_id)
            .await
            .map(|map_move_rule_provider| map_move_rule_provider.get_allowed(move_ability))
    }
}

pub trait MapMoveRuleProvider {
    fn move_rule_for_position(&self, position: Position) -> Option<DirectionalMoveRules>;
    fn move_rules(&self) -> Vec<(Position, DirectionalMoveRules)>;

    fn move_in_dir(
        &self,
        position: Position,
        move_abilities: MoveAbilities,
        direction: Direction,
    ) -> Option<Position> {
        let mut next_pos = position.move_in_dir(direction);
        let Some(current_rule) = self.move_rule_for_position(position) else {
            // If the pokemon/player somehow managed to get out of bounds, allow any movement.
            // This is a good idea since we can run into race conditions when updating maps while pokemon are moving.
            // Or a pokemon was on a position but now the position does not exist anymore etc.
            return Some(next_pos);
        };
        let current_rule = current_rule.get_rule_for(direction);
        if current_rule.contains_any(move_abilities) {
            next_pos.layer_number += i32::from(current_rule.layer_shift());
            return Some(next_pos);
        }
        None
    }

    fn possible_moves(
        &self,
        position: Position,
        move_ability: MoveAbilities,
    ) -> Vec<(Direction, Position)> {
        Direction::iter()
            .filter_map(|dir| Some((dir, self.move_in_dir(position, move_ability, dir)?)))
            .collect()
    }

    fn get_allowed(&self, move_abilities: MoveAbilities) -> Vec<Position> {
        self.move_rules()
            .into_iter()
            .filter(move |(_, move_rule)| {
                let all_abilities_that_may_enter_via_some_dir = move_rule.fold(BitOr::bitor);
                all_abilities_that_may_enter_via_some_dir.contains_any(move_abilities)
            })
            .map(|(position, _)| position)
            .collect()
    }
}

impl MapMoveRuleProvider for Box<dyn MapMoveRuleProvider + Send + Sync> {
    fn move_rule_for_position(&self, position: Position) -> Option<DirectionalMoveRules> {
        (**self).move_rule_for_position(position)
    }

    fn move_rules(&self) -> Vec<(Position, DirectionalMoveRules)> {
        (**self).move_rules()
    }
}

#[cfg(test)]
pub mod test_impl {
    use std::{collections::HashMap, fmt::Display, sync::Mutex};

    use enumset::EnumSet;
    use fxhash::FxBuildHasher;

    use crate::{error::AppError, smeargle::data::move_rule::MoveRule};

    use super::*;

    #[derive(Default)]
    pub struct TestMoveRuleProvider {
        inner: Mutex<HashMap<MapId, TestMapMoveRuleProvider>>,
    }

    impl TestMoveRuleProvider {
        pub fn upsert_map<I: IntoIterator<Item = (Position, MoveAbilities)>>(
            &self,
            map_id: MapId,
            move_rules: I,
        ) {
            let mut inner = self
                .inner
                .lock()
                .expect("Failed to acquire lock on TestMoveRuleProvider");
            let entry = inner.entry(map_id).or_default();
            for (position, move_rule) in move_rules {
                entry.inner.insert(position, move_rule);
            }
        }
    }

    #[derive(Debug, Clone)]
    enum Error {
        NoMoveRulesLoaded(MapId),
        AcquireLock,
    }

    #[async_trait::async_trait]
    impl MoveRuleProvider for TestMoveRuleProvider {
        async fn move_rules_for_map_id(
            &self,
            map_id: MapId,
        ) -> AppResult<Box<dyn MapMoveRuleProvider + Sync + Send>> {
            let move_rules = self
                .inner
                .lock()
                .map_err(|_| Error::AcquireLock)?
                .get(&map_id)
                .cloned()
                .ok_or(Error::NoMoveRulesLoaded(map_id))?;
            Ok(Box::new(move_rules))
        }
    }

    #[derive(Debug, Clone, Default)]
    pub struct TestMapMoveRuleProvider {
        inner: HashMap<Position, MoveAbilities, FxBuildHasher>,
    }

    impl MapMoveRuleProvider for TestMapMoveRuleProvider {
        fn move_rule_for_position(&self, position: Position) -> Option<DirectionalMoveRules> {
            let allowed_abilities = *self.inner.get(&position)?;
            let move_rule = DirectionalMoveRules::from_fn(|dir| {
                let target_abilities = self
                    .inner
                    .get(&position.move_in_dir(dir))
                    .copied()
                    .unwrap_or(EnumSet::empty());
                MoveRule::default() | (allowed_abilities & target_abilities)
            });
            Some(move_rule)
        }

        fn move_rules(&self) -> Vec<(Position, DirectionalMoveRules)> {
            self.inner
                .clone()
                .into_iter()
                .map(|(position, abilities)| {
                    (
                        position,
                        DirectionalMoveRules::from_single(MoveRule::default() | abilities),
                    )
                })
                .collect()
        }
    }

    impl FromIterator<(Position, MoveAbilities)> for TestMapMoveRuleProvider {
        fn from_iter<T: IntoIterator<Item = (Position, MoveAbilities)>>(iter: T) -> Self {
            TestMapMoveRuleProvider {
                inner: HashMap::from_iter(iter),
            }
        }
    }

    impl Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Error::NoMoveRulesLoaded(map_id) => {
                    f.write_fmt(format_args!("No move rules loaded for map_id '{}'", map_id))
                }
                Error::AcquireLock => {
                    f.write_str("Failed to acquire lock for TestMoveRuleProvider")
                }
            }
        }
    }

    impl std::error::Error for Error {}

    impl From<Error> for AppError {
        fn from(value: Error) -> Self {
            AppError::internal(value.to_string())
        }
    }

    #[async_trait::async_trait]
    impl<M, T> MoveRuleProvider for (M, T)
    where
        M: MoveRuleProvider + Send + Sync,
        T: Send + Sync,
    {
        async fn move_rules_for_map_id(
            &self,
            map_id: MapId,
        ) -> AppResult<Box<dyn MapMoveRuleProvider + Send + Sync>> {
            self.0.move_rules_for_map_id(map_id).await
        }
    }
}
