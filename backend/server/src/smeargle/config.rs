mod animation;
pub mod settings;
mod tile_algorithm;
pub mod tile_config;
mod comparison;
mod neighbour_config;
mod move_rule;
pub mod partial;

pub use tile_config::{TileConfigs};

pub use settings::Settings;
