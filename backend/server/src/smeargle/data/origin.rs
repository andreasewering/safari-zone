use std::fmt::Display;

use serde::Deserialize;

#[derive(
    Clone, Copy, Deserialize, Debug, PartialEq, Eq, deepsize::DeepSizeOf, arbitrary::Arbitrary,
)]
pub struct Origin {
    pub tileset: OriginTileset,
    pub index: u16,
}

#[derive(
    Clone, Copy, Deserialize, Debug, PartialEq, Eq, Hash, deepsize::DeepSizeOf, arbitrary::Arbitrary,
)]
pub enum OriginTileset {
    Advance,
    Animations,
}

static TILESET_ADVANCE: &[u8] = include_bytes!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/resources/tileset-advance.png"
));
static TILESET_ANIMATIONS: &[u8] = include_bytes!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/resources/tileset-animations.png"
));

impl OriginTileset {
    pub fn file(&self) -> &[u8] {
        match self {
            OriginTileset::Advance => TILESET_ADVANCE,
            OriginTileset::Animations => TILESET_ANIMATIONS,
        }
    }

    pub fn iter() -> impl Iterator<Item = Self> {
        [OriginTileset::Advance, OriginTileset::Animations].into_iter()
    }
}

impl Display for OriginTileset {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let display = match self {
            OriginTileset::Advance => "tileset-advance.png",
            OriginTileset::Animations => "tileset-animations.png",
        };
        write!(f, "{}", display)
    }
}
