use serde::Deserialize;

#[derive(
    Deserialize,
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    deepsize::DeepSizeOf,
    arbitrary::Arbitrary,
)]
pub enum LayerType {
    Ground,
    Decoration,
    Overlay,
    Shadow,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Layer {
    pub type_: LayerType,
    pub number: i32,
}

impl Layer {
    pub fn as_i32(&self) -> i32 {
        let layer_type_offset = match self.type_ {
            LayerType::Shadow => 5,
            LayerType::Ground => 10,
            LayerType::Decoration => 20,
            LayerType::Overlay => 50,
        };
        self.number * 100 + layer_type_offset
    }
}

impl PartialOrd for Layer {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Layer {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.as_i32().cmp(&other.as_i32())
    }
}
