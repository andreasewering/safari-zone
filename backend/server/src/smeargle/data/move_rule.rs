use std::ops::BitOr;

use enumset::{EnumSet, EnumSetType, EnumSetTypeWithRepr};
use serde::Deserialize;

use crate::data::{Direction, Directional, MoveAbilities, MoveAbility};

use super::LayerShift;

pub type TileMoveRules = Directional<TileMoveRule>;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct TileMoveRule {
    pub enter: MoveAbilities,
    pub leave: MoveAbilities,
    pub enter_layer_shift: LayerShift,
    pub leave_layer_shift: LayerShift,
}

impl TileMoveRule {
    #[cfg(test)]
    pub fn from_allowed_move_abilities(abilities: MoveAbilities) -> Self {
        Self {
            enter: abilities,
            leave: abilities,
            enter_layer_shift: LayerShift::default(),
            leave_layer_shift: LayerShift::default(),
        }
    }

    pub fn combine(self, other: Self) -> Self {
        Self {
            enter: self.enter & other.enter,
            leave: self.leave & other.leave,
            enter_layer_shift: self.enter_layer_shift.combine(other.enter_layer_shift),
            leave_layer_shift: self.leave_layer_shift.combine(other.leave_layer_shift),
        }
    }

    pub fn opposing_to_move_rule(self, target_move_rule: TileMoveRule) -> MoveRule {
        let allowed_abilities = self.leave & target_move_rule.enter;
        let mut result = allowed_abilities
            .iter()
            .map(InternalMoveRule::from)
            .collect::<MoveRule>();
        match self
            .leave_layer_shift
            .combine(target_move_rule.enter_layer_shift)
        {
            LayerShift::Up => result.0.insert(InternalMoveRule::LayerUp),
            LayerShift::Down => result.0.insert(InternalMoveRule::LayerDown),
            LayerShift::None => false,
        };
        result
    }
}

// See smeargle/README.md on more info on the design here
pub type DirectionalMoveRules = Directional<MoveRule>;

#[derive(Deserialize, Debug, EnumSetType)]
#[enumset(repr = "u8")]
enum InternalMoveRule {
    Walk,
    Swim,
    Fly,
    Run,
    Bike,
    LayerUp,
    LayerDown,
}

const ABILITY_MASK: EnumSet<InternalMoveRule> = {
    use InternalMoveRule::*;
    enumset::enum_set!(Walk | Swim | Fly | Run | Bike)
};

impl DirectionalMoveRules {
    pub fn get_rule_for(&self, direction: Direction) -> MoveRule {
        match direction {
            Direction::L => self.left,
            Direction::R => self.right,
            Direction::U => self.up,
            Direction::D => self.down,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Default)]
pub struct MoveRule(EnumSet<InternalMoveRule>);

impl BitOr<MoveAbility> for MoveRule {
    type Output = MoveRule;
    fn bitor(self, rhs: MoveAbility) -> Self {
        MoveRule(self.0 | InternalMoveRule::from(rhs))
    }
}

impl BitOr<EnumSet<MoveAbility>> for MoveRule {
    type Output = MoveRule;
    fn bitor(mut self, rhs: EnumSet<MoveAbility>) -> Self {
        for ability in rhs.iter() {
            self.0.insert(InternalMoveRule::from(ability));
        }
        self
    }
}

impl BitOr<MoveRule> for MoveRule {
    type Output = MoveRule;
    fn bitor(self, rhs: MoveRule) -> Self {
        MoveRule(self.0 | rhs.0)
    }
}

impl FromIterator<InternalMoveRule> for MoveRule {
    fn from_iter<T: IntoIterator<Item = InternalMoveRule>>(iter: T) -> Self {
        MoveRule(EnumSet::from_iter(iter))
    }
}

impl From<MoveAbility> for InternalMoveRule {
    fn from(value: MoveAbility) -> Self {
        match value {
            MoveAbility::Walk => InternalMoveRule::Walk,
            MoveAbility::Swim => InternalMoveRule::Swim,
            MoveAbility::Fly => InternalMoveRule::Fly,
            MoveAbility::Run => InternalMoveRule::Run,
            MoveAbility::Bike => InternalMoveRule::Bike,
        }
    }
}

impl deepsize::DeepSizeOf for MoveRule {
    fn deep_size_of_children(&self, ctx: &mut deepsize::Context) -> usize {
        self.0.as_repr().deep_size_of_children(ctx)
    }
}

impl MoveRule {
    fn from_repr(repr: u8) -> Self {
        Self(EnumSet::from_repr(
            repr & EnumSet::<InternalMoveRule>::all().as_repr(),
        ))
    }

    fn to_repr(self) -> u8 {
        self.0.as_repr()
    }

    pub fn contains(&self, ability: MoveAbility) -> bool {
        self.0.contains(InternalMoveRule::from(ability))
    }

    pub fn contains_any(&self, abilities: MoveAbilities) -> bool {
        abilities.iter().any(|ability| self.contains(ability))
    }

    pub fn layer_shift(&self) -> i8 {
        // This should be done branchless but maybe rust optimizes it anyways
        let up = self.0.contains(InternalMoveRule::LayerUp);
        let down = self.0.contains(InternalMoveRule::LayerDown);
        match (up, down) {
            (true, false) => 1,
            (false, true) => -1,
            _ => 0,
        }
    }
}

impl From<u32> for DirectionalMoveRules {
    fn from(value: u32) -> Self {
        let [up, left, down, right] = value.to_le_bytes();
        Self {
            up: MoveRule::from_repr(up),
            left: MoveRule::from_repr(left),
            down: MoveRule::from_repr(down),
            right: MoveRule::from_repr(right),
        }
    }
}

impl From<DirectionalMoveRules> for u32 {
    fn from(value: DirectionalMoveRules) -> Self {
        let Directional {
            left,
            right,
            up,
            down,
        } = value;
        Self::from_le_bytes([
            up.to_repr(),
            left.to_repr(),
            down.to_repr(),
            right.to_repr(),
        ])
    }
}

pub fn non_blocked_to_proto(move_rules: DirectionalMoveRules) -> Option<u32> {
    let DirectionalMoveRules {
        up,
        left,
        down,
        right,
    } = move_rules;
    let combined = up | left | down | right;
    let has_no_ability = ABILITY_MASK.is_disjoint(combined.0);
    if has_no_ability {
        return None;
    }
    Some(move_rules.into())
}

fn enumset_arbitrary<'a, T>(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<EnumSet<T>>
where
    T: EnumSetTypeWithRepr,
    <T as enumset::EnumSetTypeWithRepr>::Repr: arbitrary::Arbitrary<'a>,
{
    let repr: <T as EnumSetTypeWithRepr>::Repr = u.arbitrary()?;
    let safe_repr = repr & EnumSet::<T>::all().as_repr();
    Ok(EnumSet::from_repr(safe_repr))
}

impl<'a> arbitrary::Arbitrary<'a> for TileMoveRule {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        Ok(TileMoveRule {
            enter: enumset_arbitrary(u)?,
            leave: enumset_arbitrary(u)?,
            enter_layer_shift: *u.choose(LayerShift::all())?,
            leave_layer_shift: *u.choose(LayerShift::all())?,
        })
    }
}

impl<'a> arbitrary::Arbitrary<'a> for MoveRule {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        Ok(MoveRule(enumset_arbitrary(u)?))
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]
    use elm_rust_binding::ElmRoot;
    use serde::{Deserialize, Serialize};

    use super::*;

    #[test]
    fn combine_move_rules_example() {
        use MoveAbility::*;
        let move_rule = TileMoveRule {
            enter: MoveAbilities::empty() | Walk | Swim | Fly,
            leave: MoveAbilities::empty() | Swim | Fly,
            enter_layer_shift: LayerShift::default(),
            leave_layer_shift: LayerShift::default(),
        };
        let other_move_rule = TileMoveRule {
            enter: MoveAbilities::empty() | Swim | Run,
            leave: MoveAbilities::empty() | Fly | Walk,
            enter_layer_shift: LayerShift::default(),
            leave_layer_shift: LayerShift::default(),
        };
        assert_eq!(
            move_rule.combine(other_move_rule),
            TileMoveRule {
                enter: MoveAbilities::empty() | Swim,
                leave: MoveAbilities::empty() | Fly,
                enter_layer_shift: LayerShift::default(),
                leave_layer_shift: LayerShift::default(),
            }
        );
    }

    #[test]
    fn move_rule_repr_roundtrip() {
        arbtest::arbtest(|u| {
            let rule: MoveRule = u.arbitrary()?;
            assert_eq!(MoveRule::from_repr(rule.to_repr()), rule);
            Ok(())
        });
    }

    #[test]
    fn directional_move_rules_repr_roundtrip() {
        arbtest::arbtest(|u| {
            let rules: DirectionalMoveRules = u.arbitrary()?;
            assert_eq!(DirectionalMoveRules::from(u32::from(rules)), rules);
            Ok(())
        });
    }

    #[test]
    fn combined_move_rule_does_not_allow_more_than_uncombined() {
        arbtest::arbtest(|u| {
            let rule_1: TileMoveRule = u.arbitrary()?;
            let rule_2: TileMoveRule = u.arbitrary()?;
            let move_rule: MoveRule = rule_1.opposing_to_move_rule(rule_2);
            let move_ability: MoveAbility = u.arbitrary()?;

            if rule_1.leave.contains(move_ability) && rule_2.enter.contains(move_ability) {
                assert!(move_rule.contains(move_ability));
            } else {
                assert!(!move_rule.contains(move_ability));
            }
            Ok(())
        });
    }

    #[test]
    fn try_elm() -> Result<(), Box<dyn std::error::Error>> {
        let elm_root = ElmRoot::new(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/../../frontend/src/main"
        ))
        .expect("Elm root path is correct");

        #[derive(Deserialize, Serialize)]
        struct Input {
            position: InputPosition,
            direction: u8,
            moveAbility: u8,
            moveRules: Vec<InputMoveRule>,
        }

        #[derive(Deserialize, Serialize)]
        struct InputPosition {
            x: i32,
            y: i32,
            layerNumber: i32,
        }

        #[derive(Deserialize, Serialize)]
        struct InputMoveRule {
            x: i32,
            y: i32,
            layerNumber: i32,
            rule: u32,
        }

        #[derive(Deserialize, Debug, PartialEq, Eq)]
        struct Output {
            layerNumber: i32,
            success: bool,
        }

        let elmCanMoveFromToCheck = elm_root.prepare("Domain.MoveRule.canMoveFromToCheck")?;

        let rule = DirectionalMoveRules::from_single(MoveRule::default() | MoveAbility::Walk);
        let input = Input {
            position: InputPosition {
                x: 1,
                y: 2,
                layerNumber: 0,
            },
            direction: 0,
            moveAbility: 1,
            moveRules: vec![InputMoveRule {
                x: 1,
                y: 2,
                layerNumber: 0,
                rule: u32::from(rule),
            }],
        };
        let output: Output = elmCanMoveFromToCheck.call(input)?;

        assert_eq!(
            output,
            Output {
                success: true,
                layerNumber: 0
            }
        );
        Ok(())
    }
}
