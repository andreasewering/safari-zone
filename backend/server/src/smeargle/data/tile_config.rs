use nonempty::NonEmpty;

use crate::data::{Position, SpawnKind};

use super::{move_rule::TileMoveRules, LayerType, Origin, TileId};

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct TileConfig {
    pub origin: NonEmpty<Origin>,
    pub tile_id: TileId,
    pub tile_group: String,
    pub move_rule: TileMoveRules,
    pub effects_on_other_tiles: Vec<(Position, EffectOnOtherTilesConfig)>,
    pub layer_type: LayerType,
    pub spawn_kind: Option<SpawnKind>,
    pub keywords: Vec<String>,
    pub animation_id: Option<u32>,
}

#[derive(Debug, PartialEq, Eq, Clone, Default)]
pub struct EffectOnOtherTilesConfig {
    pub move_rule: Option<TileMoveRules>,
    pub spawn_kind: Option<SpawnKind>,
    pub animation_id: Option<u32>,
}

fn nonempty_arbitrary<'a, T>(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<NonEmpty<T>>
where
    T: arbitrary::Arbitrary<'a>,
{
    let t: T = u.arbitrary()?;
    let mut ts: Vec<T> = u.arbitrary()?;
    let mut nonempty = NonEmpty::new(t);
    nonempty.append(&mut ts);
    Ok(nonempty)
}

impl<'a> arbitrary::Arbitrary<'a> for TileConfig {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        let origin = nonempty_arbitrary(u)?;
        let tile_id = u.arbitrary()?;
        let tile_group = u.arbitrary()?;
        let move_rule = u.arbitrary()?;
        let effects_on_other_tiles = vec![];
        let layer_type = u.arbitrary()?;
        let spawn_kind = u.arbitrary()?;
        let keywords = u.arbitrary()?;
        let animation_id = u.arbitrary()?;
        Ok(TileConfig {
            origin,
            tile_id,
            tile_group,
            move_rule,
            effects_on_other_tiles,
            layer_type,
            spawn_kind,
            keywords,
            animation_id,
        })
    }
}
