use std::{
    fmt::{self, Display},
    num::TryFromIntError,
};

use serde::Deserialize;

#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    deepsize::DeepSizeOf,
    arbitrary::Arbitrary,
)]
pub struct TileId(u16);

impl TileId {
    pub fn unwrap(self) -> u16 {
        let TileId(u32) = self;
        u32
    }
    pub fn to_red_green(self) -> image::Rgba<u8> {
        let TileId(n) = self;
        let r = ((n >> 8) & 0xFF) as u8;
        let g = (n & 0xFF) as u8;
        image::Rgba([r, g, 0, 0])
    }

    pub fn to_blue_alpha(self) -> image::Rgba<u8> {
        let TileId(n) = self;
        let b = ((n >> 8) & 0xFF) as u8;
        let a = (n & 0xFF) as u8;
        image::Rgba([0, 0, b, a])
    }

    pub fn from_red_green(image::Rgba([r, g, _, _]): image::Rgba<u8>) -> Self {
        let u16 = (u16::from(r) << 8) | u16::from(g);
        TileId(u16)
    }

    pub fn from_blue_alpha(image::Rgba([_, _, b, a]): image::Rgba<u8>) -> Self {
        let u16 = (u16::from(b) << 8) | u16::from(a);
        TileId(u16)
    }
}

impl sqlx::Type<sqlx::Postgres> for TileId {
    fn type_info() -> <sqlx::Postgres as sqlx::Database>::TypeInfo {
        <i32 as sqlx::Type<sqlx::Postgres>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, sqlx::Postgres> for TileId {
    fn encode_by_ref(
        &self,
        buf: &mut <sqlx::Postgres as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <i32 as sqlx::Encode<sqlx::Postgres>>::encode(self.0.into(), buf)
    }
}

impl sqlx::Decode<'_, sqlx::Postgres> for TileId {
    fn decode(
        value: <sqlx::Postgres as sqlx::Database>::ValueRef<'_>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let tile_id_i32 = <i32 as sqlx::Decode<sqlx::Postgres>>::decode(value)?;
        let tile_id = TileId::try_from(tile_id_i32)?;
        Ok(tile_id)
    }
}

impl sqlx::postgres::PgHasArrayType for TileId {
    fn array_type_info() -> sqlx::postgres::PgTypeInfo {
        <i32 as sqlx::postgres::PgHasArrayType>::array_type_info()
    }
}

impl From<TileId> for u32 {
    fn from(value: TileId) -> Self {
        value.0.into()
    }
}

impl From<u16> for TileId {
    fn from(value: u16) -> Self {
        TileId(value)
    }
}

impl From<TileId> for i32 {
    fn from(value: TileId) -> Self {
        value.0.into()
    }
}

impl TryFrom<u32> for TileId {
    type Error = TryFromIntError;
    fn try_from(value: u32) -> Result<Self, Self::Error> {
        Ok(u16::try_from(value)?.into())
    }
}

impl TryFrom<i32> for TileId {
    type Error = TryFromIntError;
    fn try_from(value: i32) -> Result<Self, Self::Error> {
        Ok(u16::try_from(value)?.into())
    }
}

impl Display for TileId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.unwrap())
    }
}

impl<'de> Deserialize<'de> for TileId {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        u16::deserialize(deserializer).map(TileId)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use quickcheck::{quickcheck, Arbitrary};

    quickcheck! {
        fn encoding_decoding_roundtrips_red_green(tile_id: TileId) -> bool {
            TileId::from_red_green(tile_id.to_red_green()) == tile_id
        }
    }

    quickcheck! {
        fn encoding_decoding_roundtrips_blue_alpha(tile_id: TileId) -> bool {
            TileId::from_blue_alpha(tile_id.to_blue_alpha()) == tile_id
        }
    }

    impl Arbitrary for TileId {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            TileId(u16::arbitrary(g))
        }
    }

    #[test]
    fn encode_example() {
        let tile_id = TileId(12345);

        assert_eq!(tile_id.to_red_green(), image::Rgba([48, 57, 0, 0]));
        assert_eq!(tile_id.to_blue_alpha(), image::Rgba([0, 0, 48, 57]));
    }
}
