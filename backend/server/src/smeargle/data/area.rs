use std::{
    cmp::{max, min},
    fmt::Display,
};

#[derive(Debug, Clone, Copy)]
pub struct Area {
    min_x: i32,
    max_x: i32,
    min_y: i32,
    max_y: i32,
}

impl Area {
    pub fn new(x: i32, y: i32) -> Self {
        Area {
            min_x: x,
            max_x: x,
            min_y: y,
            max_y: y,
        }
    }

    pub fn insert(&mut self, x: i32, y: i32) {
        self.min_x = min(x, self.min_x);
        self.max_x = max(x, self.max_x);
        self.min_y = min(y, self.min_y);
        self.max_y = max(y, self.max_y);
    }

    pub fn width(&self) -> u32 {
        // this is safe since maxX is always >= minX (fields are private)
        u32::try_from(self.max_x - self.min_x + 1).unwrap()
    }

    pub fn height(&self) -> u32 {
        // this is safe since maxY is always >= minY (fields are private)
        u32::try_from(self.max_y - self.min_y + 1).unwrap()
    }

    pub fn top_left(&self) -> (i32, i32) {
        (self.min_x, self.min_y)
    }

    pub fn relative_to(&self, x: i32, y: i32) -> Result<(u32, u32), OutOfBoundsError> {
        let rel_x = u32::try_from(x - self.min_x).map_err(|_err| OutOfBoundsError {
            range: (self.min_x, self.max_x),
            tried: x,
        })?;
        let rel_y = u32::try_from(y - self.min_y).map_err(|_err| OutOfBoundsError {
            range: (self.min_y, self.max_y),
            tried: y,
        })?;
        Ok((rel_x, rel_y))
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OutOfBoundsError {
    range: (i32, i32),
    tried: i32,
}

impl Display for OutOfBoundsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Value {} is out of bounds of range {:?}",
            self.tried, self.range
        )
    }
}
