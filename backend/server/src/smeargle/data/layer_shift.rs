use serde::Deserialize;

use crate::data::Position;

#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub enum LayerShift {
    #[default]
    None,
    Up,
    Down,
}

impl LayerShift {
    pub fn apply(self, position: Position) -> Position {
        Position {
            layer_number: position.layer_number + i32::from(i8::from(self)),
            ..position
        }
    }

    pub fn all() -> &'static [LayerShift] {
        &[LayerShift::None, LayerShift::Up, LayerShift::Down]
    }

    pub fn combine(self, other: LayerShift) -> LayerShift {
        if self != LayerShift::None {
            self
        } else {
            other
        }
    }
}

impl From<LayerShift> for i8 {
    fn from(value: LayerShift) -> Self {
        match value {
            LayerShift::None => 0,
            LayerShift::Up => 1,
            LayerShift::Down => -1,
        }
    }
}

impl<'de> Deserialize<'de> for LayerShift {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let i8 = i8::deserialize(deserializer)?;
        match i8 {
            -1 => Ok(LayerShift::Down),
            0 => Ok(LayerShift::None),
            1 => Ok(LayerShift::Up),
            _ => Err(serde::de::Error::custom(format!(
                "Invalid layer shift {i8}. Expected -1, 0 or 1."
            ))),
        }
    }
}
