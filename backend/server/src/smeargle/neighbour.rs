use std::{cmp::Ordering, ops::Not};

use serde::{Deserialize, Deserializer};

#[rustfmt::skip]
pub const NEIGHBOURS: [Neighbour;8] = [
  TOP_LEFT,     TOP,      TOP_RIGHT,
  LEFT,                       RIGHT,
  BOTTOM_LEFT, BOTTOM, BOTTOM_RIGHT,
];

pub const TOP_LEFT: Neighbour = Neighbour(-1, -1);
pub const TOP: Neighbour = Neighbour(0, -1);
pub const TOP_RIGHT: Neighbour = Neighbour(1, -1);

pub const LEFT: Neighbour = Neighbour(-1, 0);
pub const RIGHT: Neighbour = Neighbour(1, 0);

pub const BOTTOM_LEFT: Neighbour = Neighbour(-1, 1);
pub const BOTTOM: Neighbour = Neighbour(0, 1);
pub const BOTTOM_RIGHT: Neighbour = Neighbour(1, 1);

#[derive(Clone, Copy, Debug)]
pub struct Neighbour(i8, i8);

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub struct NeighbourBitmask(u8);

impl Not for NeighbourBitmask {
    type Output = Self;

    fn not(self) -> Self::Output {
        Self(!self.0)
    }
}

impl PartialOrd for NeighbourBitmask {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if self.0 == other.0 {
            return Some(Ordering::Equal);
        }
        let and = self.0 & other.0;
        if and == self.0 {
            return Some(Ordering::Less);
        }
        if and == other.0 {
            return Some(Ordering::Greater);
        }
        None
    }
}

pub fn bitmask_neighbours<I>(neighbours: I) -> NeighbourBitmask
where
    I: IntoIterator<Item = Neighbour>,
{
    let mut neighbor_mask = 0u8;
    for neighbour in neighbours {
        neighbor_mask |= neighbour.bitmask();
    }
    NeighbourBitmask(neighbor_mask)
}

impl Neighbour {
    fn bitmask(&self) -> u8 {
        let Neighbour(dx, dy) = self;
        let bitshift = ((dx + 1) * 3 + (dy + 1) + 4) % 9;
        1 << bitshift
    }

    pub fn x<T: From<i8>>(&self) -> T {
        T::from(self.0)
    }

    pub fn y<T: From<i8>>(&self) -> T {
        T::from(self.1)
    }
}

impl<'de> Deserialize<'de> for Neighbour {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let neighbour_as_string = String::deserialize(deserializer)?;
        match neighbour_as_string.as_str() {
            "l" => Ok(LEFT),
            "r" => Ok(RIGHT),
            "t" => Ok(TOP),
            "b" => Ok(BOTTOM),
            "tl" => Ok(TOP_LEFT),
            "tr" => Ok(TOP_RIGHT),
            "bl" => Ok(BOTTOM_LEFT),
            "br" => Ok(BOTTOM_RIGHT),
            unknown => Err(serde::de::Error::custom(format!(
                "Unknown neighbour type '{}'.",
                unknown
            ))),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exact_neighbour_bitmask_comparison() {
        assert_eq!(
            bitmask_neighbours([LEFT, TOP, TOP_LEFT]),
            bitmask_neighbours([TOP, TOP_LEFT, LEFT])
        )
    }

    #[test]
    fn less_or_equal_neighbour_bitmask_comparison() {
        let l_t = bitmask_neighbours([LEFT, TOP]);
        let l_t_lt = bitmask_neighbours([LEFT, TOP, TOP_LEFT]);
        let l_lt = bitmask_neighbours([LEFT, TOP_LEFT]);
        assert!(l_t < l_t_lt);
        assert!(l_t_lt > l_t);
        assert!(l_t.partial_cmp(&l_lt).is_none());
    }
}
