use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::Arc;

use crate::data::{MapId, Position};
use crate::gardevoir::{self, guard, AuthCtx, Permission};

use super::proto::smeargle::{
    ChosenTile, DeleteMapRequest, DeleteMapResponse, GetMapsRequest, GetMapsResponse,
    GetTextureMetadataRequest, GetTextureMetadataResponse, GetTileConfigRequest,
    GetTileConfigResponse, GetTileDetailsRequest, GetTileDetailsResponse, GetTilesRequest,
    GetTilesResponse, MapData, Tile, TileConfiguration, TileDetail,
};
use super::{proto, TileCache};

use super::config::settings::Settings;
use super::data::{move_rule, Layer, TileId};
use super::proto::smeargle::smeargle_service_server::SmeargleService;
use super::proto::smeargle::{
    AddStartTileRequest, AddStartTileResponse, CreateMapRequest, CreateMapResponse,
    DeleteStartTileRequest, DeleteStartTileResponse, EditMapRequest, EditMapResponse,
    FindFittingTilesRequest, FindFittingTilesResponse, GetAllStartTilesRequest,
    GetAllStartTilesResponse, GetAnimationConfigRequest, GetAnimationConfigResponse,
    GetStartTilesRequest, GetStartTilesResponse, GetTilesetConfigRequest, GetTilesetConfigResponse,
    MapDetail, StartTile, StartTileWithMap,
};
use super::queries;
use crate::error::{AppError, AppResult};
use sqlx::PgPool;
use tonic::{Request, Response, Status};

pub struct Smeargle {
    pub auth_ctx: AuthCtx,
    pub public_path: PathBuf,
    pub settings: Settings,
    pub db_connection_pool: PgPool,
    pub tileset_path: String,
    pub tile_cache: Arc<TileCache>,
}

#[tonic::async_trait]
impl SmeargleService for Smeargle {
    async fn create_map(
        &self,
        request: Request<CreateMapRequest>,
    ) -> Result<Response<CreateMapResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        gardevoir::guard(&claims, Permission::EditMaps)?;

        let CreateMapRequest { map_name } = request.into_inner();
        let map_id = queries::create_map(&self.db_connection_pool, &map_name).await?;

        Ok(Response::new(CreateMapResponse {
            map_id: map_id.into(),
        }))
    }

    async fn edit_map(
        &self,
        request: Request<EditMapRequest>,
    ) -> Result<Response<EditMapResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        gardevoir::guard(&claims, Permission::EditTiles)?;

        let edit_map_request: EditMapRequest = request.into_inner();
        let map_id = MapId::from(edit_map_request.map_id);
        let tiles: Vec<(TileId, Position)> = edit_map_request
            .chosen_tiles
            .into_iter()
            .map(|chosen_tile| {
                Ok((
                    TileId::try_from(chosen_tile.tile_id).map_err(|err| {
                        AppError::bad_request(format!(
                            "Tile {chosen_tile:?} has an invalid tile_id: {err}"
                        ))
                    })?,
                    Position {
                        x: chosen_tile.x,
                        y: chosen_tile.y,
                        layer_number: chosen_tile.layer_number,
                    },
                ))
            })
            .collect::<AppResult<_>>()?;

        let tiles = nonempty::NonEmpty::try_from(tiles).map_err(AppError::bad_request)?;

        let response = super::set_map_tiles(
            &self.db_connection_pool,
            &self.settings.tiles,
            &self.public_path,
            map_id,
            tiles,
        )
        .await?;

        self.tile_cache.invalidate(map_id).await;

        Ok(Response::new(response))
    }

    async fn find_fitting_tiles(
        &self,
        request: Request<FindFittingTilesRequest>,
    ) -> Result<Response<FindFittingTilesResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        gardevoir::guard(&claims, Permission::EditTiles)?;

        let FindFittingTilesRequest {
            tile_id,
            layer_number,
            positions,
        } = request.into_inner();

        let parsed_tile_id = TileId::try_from(tile_id)
            .map_err(|err| AppError::bad_request(format!("Invalid tile_id {tile_id}: {err}")))?;

        let tile_config = self
            .settings
            .tiles
            .get(parsed_tile_id)
            .ok_or(Status::not_found(""))?;

        let positions: Vec<(i32, i32)> = positions.into_iter().map(|pos| (pos.x, pos.y)).collect();

        let chosen_tiles = match self.settings.tile_algorithms.get(&tile_config.tile_group) {
            Some(algorithm) => algorithm
                .run(positions)
                .into_iter()
                .flat_map(|((x, y), tile_ids)| {
                    tile_ids.into_iter().map(move |option| ChosenTile {
                        x,
                        y,
                        tile_id: u32::from(option.tile_id),
                        layer_number: layer_number + option.relative_layer_number,
                    })
                })
                .collect(),
            None => positions
                .into_iter()
                .map(|(x, y)| ChosenTile {
                    x,
                    y,
                    tile_id,
                    layer_number,
                })
                .collect(),
        };

        Ok(Response::new(FindFittingTilesResponse { chosen_tiles }))
    }

    async fn get_tileset_config(
        &self,
        _request: Request<GetTilesetConfigRequest>,
    ) -> Result<Response<GetTilesetConfigResponse>, Status> {
        // Essentially static response, no need for access control
        Ok(Response::new(GetTilesetConfigResponse {
            tileset_path: self.tileset_path.clone(),
            tile_size_in_px: self.settings.tileset.tile_size.into(),
            tiles_per_row: self.settings.tileset.tiles_per_row.into(),
        }))
    }

    async fn get_animation_config(
        &self,
        request: Request<GetAnimationConfigRequest>,
    ) -> Result<Response<GetAnimationConfigResponse>, Status> {
        // Essentially static response, no need for access control
        let GetAnimationConfigRequest {} = request.into_inner();
        let animations = self
            .settings
            .animations
            .clone()
            .into_iter()
            .map(proto::smeargle::Animation::from)
            .collect();
        Ok(Response::new(GetAnimationConfigResponse { animations }))
    }

    async fn get_maps(
        &self,
        request: Request<GetMapsRequest>,
    ) -> Result<Response<GetMapsResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::ReadMapDetails)?;

        let GetMapsRequest {} = request.into_inner();
        let maps = sqlx::query!("SELECT id, name FROM map")
            .fetch_all(&self.db_connection_pool)
            .await
            .map_err(AppError::from)?
            .into_iter()
            .map(|map| MapData {
                map_id: map.id,
                map_name: map.name,
            })
            .collect();

        Ok(Response::new(GetMapsResponse { maps }))
    }

    async fn delete_map(
        &self,
        request: Request<DeleteMapRequest>,
    ) -> Result<Response<DeleteMapResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        gardevoir::guard(&claims, Permission::EditMaps)?;

        let DeleteMapRequest { map_id } = request.into_inner();
        let map_id = MapId::from(map_id);

        match queries::try_delete_map(&self.db_connection_pool, map_id).await? {
            queries::DeletionStatus::Deleted => {}
            queries::DeletionStatus::NotPossible => {
                return Err(Status::aborted(
                    "There were still relevant entities on the map",
                ));
            }
            queries::DeletionStatus::NothingDeleted => {
                return Ok(Response::new(DeleteMapResponse {}));
            }
        }

        super::delete_tilemap_image(&self.public_path, map_id).await?;
        self.tile_cache.invalidate(map_id).await;

        Ok(Response::new(DeleteMapResponse {}))
    }

    async fn get_texture_metadata(
        &self,
        request: Request<GetTextureMetadataRequest>,
    ) -> Result<Response<GetTextureMetadataResponse>, Status> {
        // We cannot deny any reads here, since a user in creation has to be able to read all maps to display starting tiles
        self.auth_ctx.decode_header(request.metadata())?;
        let GetTextureMetadataRequest { map_id } = request.into_inner();
        let map_id = MapId::from(map_id);

        let response = get_tilemap(&self.db_connection_pool, &self.public_path, map_id).await?;
        Ok(Response::new(response))
    }

    async fn get_tiles(
        &self,
        request: Request<GetTilesRequest>,
    ) -> Result<Response<GetTilesResponse>, Status> {
        // We cannot deny any reads here, since a user in creation has to be able to read all maps to display starting tiles
        self.auth_ctx.decode_header(request.metadata())?;
        let GetTilesRequest { map_id } = request.into_inner();
        let map_id = MapId::from(map_id);

        let map_tile_cache = self.tile_cache.map_tile_cache(map_id).await?;
        let tiles = map_tile_cache
            .iter()
            .filter_map(|(position, tile)| {
                let rule = move_rule::non_blocked_to_proto(tile.move_rule)?;
                Some(Tile {
                    x: position.x,
                    y: position.y,
                    layer_number: position.layer_number,
                    rule,
                    animation_ids: tile.animation_ids,
                })
            })
            .collect();
        Ok(Response::new(GetTilesResponse { tiles }))
    }

    async fn get_tile_details(
        &self,
        request: Request<GetTileDetailsRequest>,
    ) -> Result<Response<GetTileDetailsResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::ReadMapDetails)?;

        let GetTileDetailsRequest { map_id } = request.into_inner();
        let map_id = MapId::from(map_id);

        let tile_details = queries::get_tiles_for_map(&self.db_connection_pool, map_id)
            .await?
            .into_iter()
            .filter_map(|tile| -> Option<TileDetail> {
                let tile_config = self.settings.tiles.get(tile.tile_id)?;
                let layer = Layer {
                    type_: tile_config.layer_type,
                    number: tile.layer_number,
                };

                let animation_id = tile_config.animation_id;
                Some(TileDetail {
                    x: tile.x,
                    y: tile.y,
                    layer_number: tile.layer_number,
                    animation_id,
                    tile_id: tile.tile_id.into(),
                    layer: layer.as_i32(),
                })
            })
            .collect();

        Ok(Response::new(GetTileDetailsResponse { tile_details }))
    }

    async fn get_tile_config(
        &self,
        request: Request<GetTileConfigRequest>,
    ) -> Result<Response<GetTileConfigResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::ReadMapDetails)?;
        GetTileConfigRequest {} = request.into_inner();

        let tile_configs = self
            .settings
            .tiles
            .iter()
            .map(|tile_config| TileConfiguration {
                keywords: tile_config.keywords.clone(),
                layer: Layer {
                    number: 0,
                    type_: tile_config.layer_type,
                }
                .as_i32(),
            })
            .collect();
        Ok(Response::new(GetTileConfigResponse { tile_configs }))
    }

    async fn get_all_start_tiles(
        &self,
        request: Request<GetAllStartTilesRequest>,
    ) -> Result<Response<GetAllStartTilesResponse>, Status> {
        // We cannot deny any reads here, since a user in creation has to be able to display all starting tiles
        self.auth_ctx.decode_header(request.metadata())?;
        let rows = sqlx::query!(
            "SELECT start_tile.id, start_tile.map_id, x, y, layer_number, map.name as map_name,
                    image_hash, offset_x, offset_y, layers
            FROM start_tile
            JOIN map ON start_tile.map_id = map.id
            JOIN tilemap ON start_tile.map_id = tilemap.map_id
            ORDER BY map.name"
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?;

        let mut tiles = Vec::with_capacity(rows.len());
        let mut maps = HashMap::with_capacity(rows.len());

        for row in rows {
            tiles.push(StartTileWithMap {
                start_tile_id: row.id,
                x: row.x,
                y: row.y,
                layer_number: row.layer_number,
                map_id: row.map_id,
            });
            let identifier = row.map_id.to_string();
            maps.insert(
                row.map_id,
                MapDetail {
                    path: super::tilemap::public_path(
                        &self.public_path,
                        &identifier,
                        &row.image_hash,
                    ),
                    layers: row.layers,
                    offset_x: row.offset_x,
                    offset_y: row.offset_y,
                    name: row.map_name,
                },
            );
        }

        Ok(Response::new(GetAllStartTilesResponse { tiles, maps }))
    }

    async fn add_start_tile(
        &self,
        request: Request<AddStartTileRequest>,
    ) -> Result<Response<AddStartTileResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::EditStartTiles)?;

        let AddStartTileRequest {
            x,
            y,
            layer_number,
            map_id,
        } = request.into_inner();

        let start_tile_id = queries::add_start_tile(
            &self.db_connection_pool,
            MapId::from(map_id),
            Position { x, y, layer_number },
        )
        .await?;

        Ok(Response::new(AddStartTileResponse { start_tile_id }))
    }

    async fn get_start_tiles(
        &self,
        request: Request<GetStartTilesRequest>,
    ) -> Result<Response<GetStartTilesResponse>, Status> {
        // Strictly less data than get_all_start_tiles so no need for more access control
        self.auth_ctx.decode_header(request.metadata())?;
        let GetStartTilesRequest { map_id } = request.into_inner();

        let start_tiles = sqlx::query!(
            r#"SELECT id, x, y, layer_number FROM start_tile WHERE map_id = $1"#,
            map_id
        )
        .fetch_all(&self.db_connection_pool)
        .await
        .map_err(AppError::from)?
        .into_iter()
        .map(|start_tile| StartTile {
            start_tile_id: start_tile.id,
            x: start_tile.x,
            y: start_tile.y,
            layer_number: start_tile.layer_number,
        })
        .collect();
        Ok(Response::new(GetStartTilesResponse { start_tiles }))
    }

    async fn delete_start_tile(
        &self,
        request: Request<DeleteStartTileRequest>,
    ) -> Result<Response<DeleteStartTileResponse>, Status> {
        let claims = self.auth_ctx.decode_header(request.metadata())?;
        guard(&claims, Permission::EditStartTiles)?;

        let DeleteStartTileRequest { start_tile_id } = request.into_inner();

        sqlx::query!("DELETE FROM start_tile WHERE id = $1", start_tile_id)
            .execute(&self.db_connection_pool)
            .await
            .map_err(AppError::from)?;

        Ok(Response::new(DeleteStartTileResponse {}))
    }
}

async fn get_tilemap(
    pool: &PgPool,
    public_path: &Path,
    map_id: MapId,
) -> AppResult<GetTextureMetadataResponse> {
    let identifier = map_id.to_string();
    let tilemap = queries::get_tilemap(pool, map_id).await?;
    Ok(GetTextureMetadataResponse {
        path: super::tilemap::public_path(public_path, &identifier, &tilemap.image_hash),
        offset_x: tilemap.offset_x,
        offset_y: tilemap.offset_y,
        layers: tilemap.layers,
    })
}

#[cfg(test)]
mod tests {
    use std::future::Future;

    use super::*;
    use crate::kangaskhan::test_helper as kangaskhan;
    use crate::{gardevoir::test_helper as gardevoir, public_path};

    #[database_macros::test]
    async fn create_map_without_permission(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let status = setup
            .req(
                Smeargle::create_map,
                CreateMapRequest {
                    map_name: "my map".to_string(),
                },
            )
            .await
            .expect_err("Should fail because EditMaps permission is missing.");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn edit_map_without_permission(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;

        let status = setup
            .req(
                Smeargle::edit_map,
                EditMapRequest {
                    map_id: map_id.into(),
                    chosen_tiles: vec![],
                },
            )
            .await
            .expect_err("Should fail because EditTiles permission is missing.");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn edit_map_without_tiles(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;

        setup.user.patch_permissions(&[Permission::EditTiles]);
        let status = setup
            .req(
                Smeargle::edit_map,
                EditMapRequest {
                    map_id: map_id.into(),
                    chosen_tiles: vec![],
                },
            )
            .await
            .expect_err("Should fail because chosen_tiles is empty.");
        assert_eq!(status.code(), tonic::Code::InvalidArgument);
        Ok(())
    }

    #[database_macros::test]
    async fn delete_map_without_permission(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;

        let status = setup
            .req(
                Smeargle::delete_map,
                DeleteMapRequest {
                    map_id: map_id.into(),
                },
            )
            .await
            .expect_err("Should fail because EditMaps permission is missing.");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn get_tiles_without_permission_on_other_map_ok(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;
        let other_map_id = setup.create_map_with_tiles_as_admin("other map").await?;

        setup.user.enter_map(other_map_id);
        let response = setup
            .req(
                Smeargle::get_tiles,
                GetTilesRequest {
                    map_id: map_id.into(),
                },
            )
            .await?
            .into_inner();
        assert!(!response.tiles.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn get_texture_metadata_without_permission_on_other_map_ok(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;
        let other_map_id = setup.create_map_with_tiles_as_admin("other map").await?;

        setup.user.enter_map(other_map_id);
        let response = setup
            .req(
                Smeargle::get_texture_metadata,
                GetTextureMetadataRequest {
                    map_id: map_id.into(),
                },
            )
            .await?
            .into_inner();
        assert!(!response.layers.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn get_tile_details_without_permission_on_same_map_denied(
        pool: sqlx::PgPool,
    ) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;
        setup.user.enter_map(map_id);

        let status = setup
            .req(
                Smeargle::get_tile_details,
                GetTileDetailsRequest {
                    map_id: map_id.into(),
                },
            )
            .await
            .expect_err("Should fail because the user is missing the permission ReadMapDetails");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn get_maps_without_permission_denied(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let status = setup
            .req(Smeargle::get_maps, GetMapsRequest {})
            .await
            .expect_err("Should fail because the user is missing the permission ReadMapDetails");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn create_edit_delete_map(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_name = "my map";
        setup.user.patch_permissions(&[
            Permission::EditMaps,
            Permission::EditTiles,
            Permission::ReadMapDetails,
        ]);
        let CreateMapResponse { map_id } = setup
            .req(
                Smeargle::create_map,
                CreateMapRequest {
                    map_name: map_name.to_string(),
                },
            )
            .await?
            .into_inner();

        let EditMapResponse {
            path,
            layers,
            offset_x,
            offset_y,
        } = setup
            .req(
                Smeargle::edit_map,
                EditMapRequest {
                    map_id,
                    chosen_tiles: vec![
                        ChosenTile {
                            x: 0,
                            y: 5,
                            layer_number: 0,
                            tile_id: 1,
                        },
                        ChosenTile {
                            x: 0,
                            y: 6,
                            layer_number: 0,
                            tile_id: 4,
                        },
                    ],
                },
            )
            .await?
            .into_inner();

        assert!(std::fs::exists(public_path::os_path(Path::new(&path))).unwrap_or(false));
        assert_eq!(layers, vec![10]);
        assert_eq!(offset_x, 0);
        assert_eq!(offset_y, 5);

        let GetMapsResponse { maps } = setup
            .req(Smeargle::get_maps, GetMapsRequest {})
            .await?
            .into_inner();
        assert!(maps.contains(&MapData {
            map_id,
            map_name: map_name.to_string()
        }));

        let texture_metadata = setup
            .req(
                Smeargle::get_texture_metadata,
                GetTextureMetadataRequest { map_id },
            )
            .await?
            .into_inner();
        assert_eq!(path, texture_metadata.path);
        assert_eq!(layers, texture_metadata.layers);
        assert_eq!(offset_x, texture_metadata.offset_x);
        assert_eq!(offset_y, texture_metadata.offset_y);

        let GetTilesResponse { mut tiles } = setup
            .req(Smeargle::get_tiles, GetTilesRequest { map_id })
            .await?
            .into_inner();
        tiles.sort_by_key(|t| t.y);
        assert_eq!(tiles[0].x, 0);
        assert_eq!(tiles[0].y, 5);
        assert_eq!(tiles[0].layer_number, 0);
        assert_eq!(tiles[1].x, 0);
        assert_eq!(tiles[1].y, 6);
        assert_eq!(tiles[1].layer_number, 0);

        let GetTileDetailsResponse { mut tile_details } = setup
            .req(Smeargle::get_tile_details, GetTileDetailsRequest { map_id })
            .await?
            .into_inner();
        tile_details.sort_by_key(|td| td.y);
        assert_eq!(tile_details[0].x, 0);
        assert_eq!(tile_details[0].y, 5);
        assert_eq!(tile_details[0].layer_number, 0);
        assert_eq!(tile_details[0].tile_id, 1);
        assert_eq!(tile_details[1].x, 0);
        assert_eq!(tile_details[1].y, 6);
        assert_eq!(tile_details[1].tile_id, 4);
        assert_eq!(tile_details[1].layer_number, 0);

        let DeleteMapResponse {} = setup
            .req(Smeargle::delete_map, DeleteMapRequest { map_id })
            .await?
            .into_inner();

        assert!(!std::fs::exists(public_path::os_path(Path::new(&path))).unwrap_or(true));

        Ok(())
    }

    #[database_macros::test]
    async fn delete_fails_when_user_is_still_on_map(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::EditMaps]);

        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;
        setup.user.enter_map(map_id);
        kangaskhan::set_user_position(
            &pool,
            &setup.user,
            Position {
                x: 0,
                y: 0,
                layer_number: 0,
            },
        )
        .await?;

        let status = setup
            .req(
                Smeargle::delete_map,
                DeleteMapRequest {
                    map_id: map_id.into(),
                },
            )
            .await
            .expect_err("Should fail because there is a user on the map");
        assert_eq!(status.code(), tonic::Code::Aborted);

        Ok(())
    }

    #[database_macros::test]
    async fn delete_is_idempotent(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::EditMaps]);

        let CreateMapResponse { map_id } = setup
            .req(
                Smeargle::create_map,
                CreateMapRequest {
                    map_name: "my map".to_string(),
                },
            )
            .await?
            .into_inner();

        setup
            .req(Smeargle::delete_map, DeleteMapRequest { map_id })
            .await?;
        // second call is fine, it just does nothing
        setup
            .req(Smeargle::delete_map, DeleteMapRequest { map_id })
            .await?;

        Ok(())
    }

    #[database_macros::test]
    async fn get_animation_config_works(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let GetAnimationConfigResponse { animations } = setup
            .req(Smeargle::get_animation_config, GetAnimationConfigRequest {})
            .await?
            .into_inner();
        assert!(!animations.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn get_tileset_config_works(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let GetTilesetConfigResponse {
            tileset_path,
            tile_size_in_px,
            tiles_per_row,
        } = setup
            .req(Smeargle::get_tileset_config, GetTilesetConfigRequest {})
            .await?
            .into_inner();
        assert!(!tileset_path.is_empty());
        assert!(tile_size_in_px > 0);
        assert!(tiles_per_row > 0);
        Ok(())
    }

    #[database_macros::test]
    async fn get_tile_config_permission_denied(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let status = setup
            .req(Smeargle::get_tile_config, GetTileConfigRequest {})
            .await
            .expect_err("Should fail because the user is missing the permission ReadMapDetails");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn get_tile_config_works(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::ReadMapDetails]);

        let GetTileConfigResponse { tile_configs } = setup
            .req(Smeargle::get_tile_config, GetTileConfigRequest {})
            .await?
            .into_inner();
        assert!(!tile_configs.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn find_fitting_tiles_permission_denied(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let status = setup
            .req(
                Smeargle::find_fitting_tiles,
                FindFittingTilesRequest {
                    tile_id: 1,
                    layer_number: 0,
                    positions: vec![],
                },
            )
            .await
            .expect_err("Should fail because the user is missing the permission EditTiles");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn find_fitting_tiles_works(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        setup.user.patch_permissions(&[Permission::EditTiles]);

        let positions = vec![
            proto::smeargle::Position { x: 0, y: 0 },
            proto::smeargle::Position { x: 1, y: 0 },
            proto::smeargle::Position { x: 2, y: 0 },
        ];
        let FindFittingTilesResponse { mut chosen_tiles } = setup
            .req(
                Smeargle::find_fitting_tiles,
                FindFittingTilesRequest {
                    tile_id: 1,
                    layer_number: 2,
                    positions: positions.clone(),
                },
            )
            .await?
            .into_inner();
        chosen_tiles.sort_by_key(|ct| ct.x);

        assert_eq!(
            chosen_tiles
                .iter()
                .map(|ct| proto::smeargle::Position { x: ct.x, y: ct.y })
                .collect::<Vec<_>>(),
            positions
        );
        assert!(chosen_tiles.iter().all(|ct| ct.layer_number == 2));

        Ok(())
    }

    #[database_macros::test]
    async fn get_all_start_tiles_works(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;

        let GetAllStartTilesResponse { tiles, maps } = setup
            .req(Smeargle::get_all_start_tiles, GetAllStartTilesRequest {})
            .await?
            .into_inner();
        assert!(tiles.is_empty());
        assert!(maps.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn get_start_tiles_for_map_works(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;

        let GetStartTilesResponse { start_tiles } = setup
            .req(
                Smeargle::get_start_tiles,
                GetStartTilesRequest {
                    map_id: map_id.into(),
                },
            )
            .await?
            .into_inner();
        assert!(start_tiles.is_empty());
        Ok(())
    }

    #[database_macros::test]
    async fn add_start_tile_permission_denied(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;

        let status = setup
            .req(
                Smeargle::add_start_tile,
                AddStartTileRequest {
                    map_id: map_id.into(),
                    x: 0,
                    y: 0,
                    layer_number: 0,
                },
            )
            .await
            .expect_err("Should fail because the user is missing the permission EditStartTiles");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn delete_start_tile_permission_denied(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;
        setup.user.patch_permissions(&[Permission::EditStartTiles]);

        let AddStartTileResponse { start_tile_id } = setup
            .req(
                Smeargle::add_start_tile,
                AddStartTileRequest {
                    map_id: map_id.into(),
                    x: 0,
                    y: 0,
                    layer_number: 0,
                },
            )
            .await?
            .into_inner();

        setup.user.patch_permissions(&[]);

        let status = setup
            .req(
                Smeargle::delete_start_tile,
                DeleteStartTileRequest { start_tile_id },
            )
            .await
            .expect_err("Should fail because the user is missing the permission EditStartTiles");
        assert_eq!(status.code(), tonic::Code::PermissionDenied);
        Ok(())
    }

    #[database_macros::test]
    async fn create_read_and_delete_start_tiles(pool: sqlx::PgPool) -> anyhow::Result<()> {
        let mut setup = setup(&pool).await?;
        let map_id = setup.create_map_with_tiles_as_admin("my map").await?;
        setup
            .user
            .patch_permissions(&[Permission::EditMaps, Permission::EditStartTiles]);

        let AddStartTileResponse { start_tile_id } = setup
            .req(
                Smeargle::add_start_tile,
                AddStartTileRequest {
                    map_id: map_id.into(),
                    x: 0,
                    y: 0,
                    layer_number: 0,
                },
            )
            .await?
            .into_inner();

        let GetAllStartTilesResponse { tiles, maps } = setup
            .req(Smeargle::get_all_start_tiles, GetAllStartTilesRequest {})
            .await?
            .into_inner();
        assert_eq!(
            tiles,
            vec![StartTileWithMap {
                start_tile_id,
                x: 0,
                y: 0,
                layer_number: 0,
                map_id: map_id.into()
            }]
        );
        assert_eq!(maps.len(), 1);
        assert!(maps.contains_key(&map_id.into()));

        let GetStartTilesResponse { start_tiles } = setup
            .req(
                Smeargle::get_start_tiles,
                GetStartTilesRequest {
                    map_id: map_id.into(),
                },
            )
            .await?
            .into_inner();
        assert_eq!(
            start_tiles,
            vec![StartTile {
                start_tile_id,
                x: 0,
                y: 0,
                layer_number: 0
            }]
        );

        let DeleteStartTileResponse {} = setup
            .req(
                Smeargle::delete_start_tile,
                DeleteStartTileRequest { start_tile_id },
            )
            .await?
            .into_inner();

        let GetAllStartTilesResponse { tiles, maps } = setup
            .req(Smeargle::get_all_start_tiles, GetAllStartTilesRequest {})
            .await?
            .into_inner();
        assert!(tiles.is_empty());
        assert!(maps.is_empty());

        Ok(())
    }

    struct Setup {
        api: Smeargle,
        user: gardevoir::LoggedInUser,
    }

    impl Setup {
        async fn req<'a, Req, Res, F, Fut>(
            &'a self,
            f: F,
            request: Req,
        ) -> Result<Response<Res>, Status>
        where
            F: Fn(&'a Smeargle, Request<Req>) -> Fut,
            Fut: Future<Output = Result<Response<Res>, Status>>,
        {
            f(&self.api, self.user.request(request)).await
        }

        async fn create_map_with_tiles_as_admin(&self, map_name: &str) -> Result<MapId, Status> {
            let mut user = self.user.clone();
            user.patch_permissions(&[Permission::EditMaps, Permission::EditTiles]);

            let CreateMapResponse { map_id } = self
                .api
                .create_map(user.request(CreateMapRequest {
                    map_name: map_name.to_string(),
                }))
                .await?
                .into_inner();

            self.api
                .edit_map(user.request(EditMapRequest {
                    map_id,
                    chosen_tiles: vec![
                        ChosenTile {
                            x: 0,
                            y: 0,
                            layer_number: 0,
                            tile_id: 1,
                        },
                        ChosenTile {
                            x: 0,
                            y: 1,
                            layer_number: 0,
                            tile_id: 1,
                        },
                    ],
                }))
                .await?;

            Ok(MapId::from(map_id))
        }
    }

    async fn setup(pool: &sqlx::PgPool) -> anyhow::Result<Setup> {
        let settings = Settings::read()?;
        let auth_ctx = gardevoir::create_auth_ctx().await?;
        let user = gardevoir::UserBuilder::from_name("user")
            .create(&auth_ctx, pool)
            .await?;

        let tile_cache: Arc<TileCache> = TileCache::new(pool.clone(), settings.clone()).into();
        let api = Smeargle {
            auth_ctx,
            public_path: PathBuf::new(),
            settings: settings.clone(),
            db_connection_pool: pool.clone(),
            tileset_path: "tileset".to_string(),
            tile_cache,
        };
        Ok(Setup { api, user })
    }
}
