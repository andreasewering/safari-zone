use std::collections::HashMap;

use crate::smeargle::{
    config::TileConfigs,
    data::{LayerType, TileConfig, TileId},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct TilemapConfig {
    pub layer_type: LayerType,
}

pub trait TilemapConfigLookup {
    fn get_tile(&self, tile_id: TileId) -> Option<TilemapConfig>;
}

impl TilemapConfigLookup for &TileConfigs {
    fn get_tile(&self, tile_id: TileId) -> Option<TilemapConfig> {
        let tile_config = self.get(tile_id)?;
        Some(tile_config.clone().into())
    }
}

impl TilemapConfigLookup for HashMap<TileId, TilemapConfig> {
    fn get_tile(&self, tile_id: TileId) -> Option<TilemapConfig> {
        self.get(&tile_id).copied()
    }
}

impl From<TileConfig> for TilemapConfig {
    fn from(
        TileConfig {
            layer_type,
            ..
        }: TileConfig,
    ) -> Self {
        TilemapConfig { layer_type }
    }
}
