use sqlx::PgPool;

use crate::{
    data::{MapId, Position},
    error::{AppError, AppResult},
};

use super::{data::TileId, tilemap};

pub async fn create_map(pool: &PgPool, name: &str) -> AppResult<MapId> {
    let result = sqlx::query!(
        r#"INSERT INTO map (name) VALUES ($1) RETURNING id as "id: MapId""#,
        name
    )
    .fetch_one(pool)
    .await?;
    Ok(result.id)
}

pub async fn add_start_tile(pool: &PgPool, map_id: MapId, position: Position) -> AppResult<i64> {
    let result = sqlx::query!(
        "INSERT INTO start_tile (x, y, layer_number, map_id)
            VALUES ($1, $2, $3, $4)
            RETURNING id",
        position.x,
        position.y,
        position.layer_number,
        map_id as MapId
    )
    .fetch_one(pool)
    .await?;
    Ok(result.id)
}

#[derive(Debug, PartialEq, Eq)]
pub struct TileEntity {
    pub x: i32,
    pub y: i32,
    pub layer_number: i32,
    pub tile_id: TileId,
}

pub async fn get_tiles_for_map(pool: &PgPool, map_id: MapId) -> AppResult<Vec<TileEntity>> {
    let tiles = sqlx::query_as!(
        TileEntity,
        r#"SELECT x, y, layer_number, tile_id as "tile_id: _" FROM tile WHERE map_id = $1"#,
        map_id as MapId
    )
    .fetch_all(pool)
    .await?;
    Ok(tiles)
}

pub async fn insert_tiles_into_map<I>(pool: &PgPool, map_id: MapId, tiles: I) -> AppResult<()>
where
    I: IntoIterator<Item = (TileId, Position)>,
{
    let mut xs = Vec::new();
    let mut ys = Vec::new();
    let mut layer_numbers = Vec::new();
    let mut map_ids = Vec::new();
    let mut tile_ids = Vec::new();

    for (tile_id, pos) in tiles {
        xs.push(pos.x);
        ys.push(pos.y);
        layer_numbers.push(pos.layer_number);
        map_ids.push(map_id);
        tile_ids.push(i32::from(tile_id.unwrap()));
    }
    sqlx::query!(
    r#"INSERT INTO tile (x, y, layer_number, map_id, tile_id)
SELECT x, y, layer_number, map_id, tile_id
FROM UNNEST($1::INTEGER[], $2::INTEGER[], $3::INTEGER[], $4::BIGINT[], $5::INTEGER[]) as a(x, y, layer_number, map_id, tile_id)"#,
    &xs,
    &ys,
    &layer_numbers,
    &map_ids as &[MapId],
    &tile_ids
).execute(pool).await?;
    Ok(())
}

pub async fn delete_tiles_for_map(pool: &PgPool, map_id: MapId) -> AppResult<()> {
    sqlx::query!("DELETE FROM tile WHERE map_id = $1", map_id as MapId)
        .execute(pool)
        .await?;
    Ok(())
}

pub struct TilemapEntity {
    pub image_hash: String,
    pub offset_x: i32,
    pub offset_y: i32,
    pub layers: Vec<i32>,
}

pub async fn get_tilemap(pool: &PgPool, map_id: MapId) -> AppResult<TilemapEntity> {
    let tilemaps = sqlx::query_as!(
        TilemapEntity,
        "SELECT image_hash, offset_x, offset_y, layers FROM tilemap WHERE map_id = $1",
        map_id as MapId
    )
    .fetch_one(pool)
    .await?;

    Ok(tilemaps)
}

pub async fn upsert_tilemap<'a, E>(
    executor: E,
    map_id: MapId,
    tilemap: &tilemap::Tilemap,
) -> AppResult<()>
where
    E: sqlx::PgExecutor<'a>,
{
    sqlx::query!(
      r#"INSERT INTO tilemap (map_id, image_hash, offset_x, offset_y, layers) VALUES ($1, $2, $3, $4, $5)
         ON CONFLICT (map_id) DO UPDATE
         SET image_hash = EXCLUDED.image_hash,
             offset_x = EXCLUDED.offset_x,
             offset_y = EXCLUDED.offset_y,
             layers = EXCLUDED.layers 
      "#,
      map_id as MapId,
      tilemap.hash,
      tilemap.offset.0,
      tilemap.offset.1,
      &tilemap.layers.iter().map(|layer|layer.as_i32()).collect::<Vec<i32>>()
  )
  .execute(executor)
  .await?;
    Ok(())
}

pub(super) enum DeletionStatus {
    NotPossible,
    NothingDeleted,
    Deleted,
}

/// Returns the count of users on a given map.
/// If the map is not in the database, returns `None`.
pub async fn try_delete_map<'a, E>(executor: E, map_id: MapId) -> AppResult<DeletionStatus>
where
    E: sqlx::PgExecutor<'a>,
{
    let deleted_result = sqlx::query!("DELETE FROM map WHERE id = $1", map_id as MapId)
        .execute(executor)
        .await;

    match deleted_result {
        Ok(result) if result.rows_affected() == 0 => Ok(DeletionStatus::NothingDeleted),
        Ok(result) if result.rows_affected() == 1 => Ok(DeletionStatus::Deleted),
        Ok(result) => Err(AppError::inconsistency(format!(
            "Somehow managed to delete more than one map {}",
            result.rows_affected()
        ))),
        Err(err) => {
            if let Some(db_err) = err.as_database_error() {
                if db_err.is_foreign_key_violation() {
                    return Ok(DeletionStatus::NotPossible);
                }
            };
            Err(err.into())
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::smeargle::data::{Layer, LayerType};

    use super::*;

    fn tuples_to_rows<I>(tuples: I) -> Vec<TileEntity>
    where
        I: IntoIterator<Item = (TileId, Position)>,
    {
        tuples
            .into_iter()
            .map(|(tile_id, Position { x, y, layer_number })| TileEntity {
                x,
                y,
                tile_id,
                layer_number,
            })
            .collect()
    }

    #[database_macros::test]
    async fn insert_works_as_expected(pool: PgPool) -> AppResult<()> {
        let map_id = create_map(&pool, "map_name").await?;
        let tiles = [
            (
                TileId::from(1),
                Position {
                    x: 0,
                    y: 1,
                    layer_number: 2,
                },
            ),
            (
                TileId::from(1),
                Position {
                    x: 1,
                    y: 1,
                    layer_number: 2,
                },
            ),
            (
                TileId::from(2),
                Position {
                    x: 2,
                    y: 1,
                    layer_number: 3,
                },
            ),
        ];
        insert_tiles_into_map(&pool, map_id, tiles).await?;

        let entities = get_tiles_for_map(&pool, map_id).await?;

        assert_eq!(entities, tuples_to_rows(tiles));

        Ok(())
    }

    #[database_macros::test]
    async fn delete_works_as_expected(pool: PgPool) -> AppResult<()> {
        let map_id_1 = create_map(&pool, "map_name_1").await?;
        let map_id_2 = create_map(&pool, "map_name_2").await?;
        let tiles_1 = [(
            TileId::from(1),
            Position {
                x: 0,
                y: 1,
                layer_number: 2,
            },
        )];
        let tiles_2 = [(
            TileId::from(2),
            Position {
                x: 0,
                y: 1,
                layer_number: 2,
            },
        )];
        insert_tiles_into_map(&pool, map_id_1, tiles_1).await?;
        insert_tiles_into_map(&pool, map_id_2, tiles_2).await?;
        delete_tiles_for_map(&pool, map_id_1).await?;

        let entities = get_tiles_for_map(&pool, map_id_2).await?;

        assert_eq!(entities, tuples_to_rows(tiles_2));

        Ok(())
    }

    #[database_macros::test]
    async fn tilemap_upsert_updates_all_fields(pool: PgPool) -> AppResult<()> {
        let map_id = create_map(&pool, "map_name").await?;
        let mut tilemap = tilemap::Tilemap {
            hash: "abcdefg123".to_string(),
            identifier: map_id.to_string(),
            image: image::RgbaImage::default(),
            offset: (-5, 10),
            layers: vec![
                Layer {
                    type_: LayerType::Ground,
                    number: 0,
                },
                Layer {
                    type_: LayerType::Decoration,
                    number: 0,
                },
            ],
        };
        upsert_tilemap(&pool, map_id, &tilemap).await?;

        let tilemap_entity = get_tilemap(&pool, map_id).await?;
        assert_eq!(tilemap_entity.image_hash, tilemap.hash);
        assert_eq!(tilemap_entity.offset_x, tilemap.offset.0);
        assert_eq!(tilemap_entity.offset_y, tilemap.offset.1);
        assert_eq!(tilemap_entity.layers.len(), tilemap.layers.len());

        tilemap.hash = "something else".to_string();
        tilemap.offset = (123, 456);
        tilemap.layers = vec![];

        upsert_tilemap(&pool, map_id, &tilemap).await?;
        let maps = sqlx::query!("SELECT id FROM tilemap")
            .fetch_all(&pool)
            .await?;
        assert_eq!(maps.len(), 1);

        let tilemap_entity = get_tilemap(&pool, map_id).await?;
        assert_eq!(tilemap_entity.image_hash, tilemap.hash);
        assert_eq!(tilemap_entity.offset_x, tilemap.offset.0);
        assert_eq!(tilemap_entity.offset_y, tilemap.offset.1);
        assert_eq!(tilemap_entity.layers.len(), tilemap.layers.len());

        Ok(())
    }
}
