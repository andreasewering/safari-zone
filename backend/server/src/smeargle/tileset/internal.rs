use std::{collections::HashMap, error::Error, fmt::Display};

use image::{imageops, DynamicImage, RgbaImage};

use crate::smeargle::data::{Origin, OriginTileset, TileConfig};

use super::tileset_type::Tileset;
use image::{GenericImage, GenericImageView};

/**
 * Place a tile given a *complete* map of origin tilesets to images!
 */
pub fn place_tile(
    images: &HashMap<OriginTileset, DynamicImage>,
    final_tileset: &Tileset,
    final_img: &mut RgbaImage,
    tile: &TileConfig,
    target_tile_size: u32,
) -> anyhow::Result<()> {
    let first = tile.origin.first();
    let rest = tile.origin.tail();

    let mut first_image = origin_to_sub_image(images, first, target_tile_size)?;
    let images: Vec<RgbaImage> = rest
        .iter()
        .map(|origin| origin_to_sub_image(images, origin, target_tile_size))
        .collect::<anyhow::Result<Vec<RgbaImage>>>()?;
    underlay(&mut first_image, &images);

    let (x, y) = final_tileset.index_to_px_coord(tile.tile_id.unwrap() - 1)?;
    final_img.copy_from(&first_image, x, y)?;
    Ok(())
}

#[derive(Debug, Clone)]
pub struct ImageNotLoaded {
    tileset: OriginTileset,
}

impl Display for ImageNotLoaded {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Image from path {} was not loaded for some reason.",
            self.tileset
        )
    }
}

impl Error for ImageNotLoaded {}

fn origin_to_sub_image(
    images: &HashMap<OriginTileset, DynamicImage>,
    origin: &Origin,
    target_tile_size: u32,
) -> anyhow::Result<RgbaImage> {
    let (x, y) = Tileset::from(origin.tileset).index_to_px_coord(origin.index)?;
    let tile_size = Tileset::from(origin.tileset).tile_size;

    let sub_image = images
        .get(&origin.tileset)
        .ok_or(ImageNotLoaded {
            tileset: origin.tileset,
        })?
        .view(x, y, tile_size.into(), tile_size.into())
        .to_image();

    Ok(imageops::resize(
        &sub_image,
        target_tile_size,
        target_tile_size,
        imageops::FilterType::Triangle,
    ))
}

fn underlay(base_image: &mut RgbaImage, underlays: &Vec<RgbaImage>) {
    for x in 0..base_image.width() {
        for y in 0..base_image.height() {
            let px = base_image.get_pixel(x, y);
            if px[3] > 0 {
                continue;
            }
            for underlay in underlays {
                let under_px = underlay.get_pixel(x, y);
                if under_px[3] > 0 {
                    base_image.put_pixel(x, y, *under_px);
                    break;
                }
            }
        }
    }
}
