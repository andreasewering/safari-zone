use std::{error::Error, fmt::Display, ops::Rem};

use crate::smeargle::data::OriginTileset;

#[derive(Debug, Clone)]
pub struct Tileset {
    pub tile_size: u8,
    pub tiles_per_row: u8,
    pub number_of_tiles: u16,
}

#[derive(Debug, Clone)]
pub struct OutOfBoundsError {
    max: u16,
    tried: u16,
}

impl Display for OutOfBoundsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Tileset has {} tiles, but you tried to access the tile at index {}",
            self.max, self.tried
        )
    }
}

impl Error for OutOfBoundsError {}

impl Tileset {
    pub fn index_to_px_coord(&self, index: u16) -> Result<(u32, u32), OutOfBoundsError> {
        if index > self.number_of_tiles - 1 {
            return Err(OutOfBoundsError {
                max: self.number_of_tiles,
                tried: index,
            });
        }
        let x = u32::from(self.tile_size) * u32::from(index).rem(u32::from(self.tiles_per_row));
        let y = u32::from(self.tile_size) * (u32::from(index) / u32::from(self.tiles_per_row));
        Ok((x, y))
    }

    pub fn width_in_px(&self) -> u32 {
        u32::from(self.tile_size) * u32::from(self.tiles_per_row)
    }

    pub fn height_in_px(&self) -> u32 {
        u32::from(self.tile_size)
            * u32::from(self.number_of_tiles).div_ceil(u32::from(self.tiles_per_row))
    }
}

impl From<OriginTileset> for Tileset {
    fn from(origin_tileset: OriginTileset) -> Tileset {
        match origin_tileset {
            OriginTileset::Advance => Tileset {
                tile_size: 16,
                tiles_per_row: 8,
                number_of_tiles: 7984,
            },
            OriginTileset::Animations => Tileset {
                tile_size: 32,
                tiles_per_row: 4,
                number_of_tiles: 16,
            },
        }
    }
}

#[cfg(test)]
pub mod tests {
    use std::cmp::max;

    use quickcheck::{quickcheck, Arbitrary};

    use super::*;

    #[test]
    fn width_calc_works_for_an_example() {
        let tileset = Tileset {
            tile_size: 50,
            tiles_per_row: 3,
            number_of_tiles: 10,
        };
        assert_eq!(tileset.width_in_px(), 150);
    }

    #[test]
    fn width_is_based_on_tiles_per_row_and_not_number_of_tiles() {
        let tileset = Tileset {
            tile_size: 50,
            tiles_per_row: 3,
            number_of_tiles: 2,
        };
        assert_eq!(tileset.width_in_px(), 150);
    }

    #[test]
    fn height_calc_works_for_an_example() {
        let tileset = Tileset {
            tile_size: 50,
            tiles_per_row: 3,
            number_of_tiles: 10,
        };
        assert_eq!(tileset.height_in_px(), 200);
    }

    #[test]
    fn height_calc_works_for_a_filled_row() {
        let tileset = Tileset {
            tile_size: 50,
            tiles_per_row: 3,
            number_of_tiles: 3,
        };
        assert_eq!(tileset.height_in_px(), 50);
    }

    quickcheck! {
        fn width_times_height_is_always_greater_number_of_tiles_times_tile_size(tileset: Tileset) -> bool {
            tileset.width_in_px() * tileset.height_in_px() >= u32::from(tileset.number_of_tiles) * u32::from(tileset.tile_size)
        }
    }

    impl Arbitrary for Tileset {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            // All values need to be > 0 for a tileset to make sense.
            Tileset {
                tile_size: max(u8::arbitrary(g), 1),
                tiles_per_row: max(u8::arbitrary(g) ,1),
                number_of_tiles: max(u16::arbitrary(g), 1),
            }
        }
    }
}
