mod internal;
mod tileset_type;

use std::{collections::HashMap, path::Path};

use image::{ImageReader, RgbaImage};

use anyhow::Result;

use super::{config, data::OriginTileset, hash::make_image_hash};

pub fn write_tileset(public_path: &Path, sorted_config: &config::TileConfigs) -> Result<String> {
    let public_os_path = crate::public_path::os_path(public_path);
    let number_of_tiles: u16 = sorted_config.length().try_into()?;
    let final_tileset = create_final_tileset(number_of_tiles);

    tracing::debug!("Reading origin tilesets");
    let images = OriginTileset::iter()
        .map(
            |origin_tileset| -> Result<(OriginTileset, image::DynamicImage)> {
                let image = ImageReader::new(std::io::Cursor::new(origin_tileset.file()))
                    .with_guessed_format()?
                    .decode()?;
                Ok((origin_tileset, image))
            },
        )
        .collect::<Result<HashMap<OriginTileset, image::DynamicImage>>>()?;

    let mut final_image = RgbaImage::new(final_tileset.width_in_px(), final_tileset.height_in_px());

    for tile in sorted_config.iter() {
        internal::place_tile(
            &images,
            &final_tileset,
            &mut final_image,
            tile,
            u32::from(final_tileset.tile_size),
        )?;
    }
    let hash = make_image_hash(&final_image);
    let path = format!("tileset.{hash}.png");

    std::fs::create_dir_all(&public_os_path)?;
    let image_path = public_os_path.join(&path);

    tracing::info!(message = "Writing tileset to path", path = ?image_path);
    final_image.save(&image_path)?;
    Ok(format!("{}/{path}", public_path.to_str().unwrap()))
}

fn create_final_tileset(number_of_tiles: u16) -> tileset_type::Tileset {
    tileset_type::Tileset {
        tiles_per_row: 8,
        tile_size: 16,
        number_of_tiles,
    }
}
