use std::{collections::HashMap, sync::Arc, time::Duration};

use fxhash::FxBuildHasher;
use moka::{future::Cache, notification::RemovalCause};
use serde::{Deserialize, Serialize};
use sqlx::PgPool;

use crate::{
    data::{MapId, Position, SpawnKind},
    error::AppResult,
    smeargle::data::EffectOnOtherTilesConfig,
    util::cache::deep_size_of_weigher,
};

use super::{
    config::TileConfigs,
    data::{TileId, TileMoveRules},
    move_rule_provider::{MapMoveRuleProvider, MoveRuleProvider},
    spawn_kind_provider::{MapSpawnKindProvider, SpawnKindProvider},
    DirectionalMoveRules, Settings,
};

#[derive(Clone)]
pub struct TileCache {
    inner: Cache<MapId, Arc<MapTileCacheInner>, FxBuildHasher>,
    pool: PgPool,
    tile_configs: TileConfigs,
}

impl TileCache {
    pub(super) async fn map_tile_cache(&self, map_id: MapId) -> AppResult<MapTileCache> {
        let map_cache = self.get(map_id).await?;
        Ok(map_cache)
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct TileCacheSettings {
    /// How many bytes the cache may occupy.
    /// This is neither a hard upper nor a hard lower bound.
    /// The cache should usually occupy less memory but may temporarily exceed the limit specified here.
    /// Instead, this signals that the least frequently used item should (soon) be removed from the cache.
    max_size_in_bytes: u64,

    /// How many seconds it takes for an unused cache entry to be removed
    expiry_time_in_seconds: u64,
}

impl TileCacheSettings {
    fn expiry_time(&self) -> Duration {
        Duration::from_secs(self.expiry_time_in_seconds)
    }
}

impl TileCache {
    pub fn new(pool: PgPool, settings: Settings) -> Self {
        let inner = Cache::builder()
            .max_capacity(settings.tile_cache.max_size_in_bytes)
            .weigher(deep_size_of_weigher)
            .eviction_listener(tracing_eviction_listener)
            .time_to_idle(settings.tile_cache.expiry_time())
            .build_with_hasher(FxBuildHasher::default());
        Self {
            inner,
            pool,
            tile_configs: settings.tiles,
        }
    }

    pub async fn invalidate(&self, map_id: MapId) {
        tracing::info!(message = "Invalidated tile cache", %map_id);
        self.inner.invalidate(&map_id).await;
    }

    async fn get(&self, map_id: MapId) -> Result<MapTileCache, Arc<sqlx::Error>> {
        let tile_cache = self
            .inner
            .try_get_with(map_id, self.force_load_map(map_id))
            .await?;
        Ok(MapTileCache { inner: tile_cache })
    }

    async fn force_load_map(&self, map_id: MapId) -> sqlx::Result<Arc<MapTileCacheInner>> {
        tracing::debug!(message = "Loading tiles for map", %map_id);

        let tiles = sqlx::query_as!(
            TileEntity,
            r#"SELECT x, y, layer_number, tile_id AS "tile_id: _" FROM tile WHERE map_id = $1
               ORDER BY layer_number, x, y"#,
            map_id as MapId
        )
        .fetch_all(&self.pool)
        .await?;
        let tile_cache = tile_entities_to_map_cache(tiles, &self.tile_configs);

        tracing::info!(message = "Loaded tiles for map", %map_id);

        Ok(Arc::new(tile_cache))
    }
}

struct TileEntity {
    x: i32,
    y: i32,
    layer_number: i32,
    tile_id: TileId,
}

type MapTileCacheInner = HashMap<Position, Tile, FxBuildHasher>;

#[derive(Debug, Clone, deepsize::DeepSizeOf)]
pub(super) struct Tile {
    pub(super) move_rule: DirectionalMoveRules,
    pub(super) spawn_kind: Option<SpawnKind>,
    pub(super) animation_ids: Vec<u32>,
}

fn tile_entities_to_map_cache(
    tile_entities: Vec<TileEntity>,
    tile_configs: &TileConfigs,
) -> MapTileCacheInner {
    #[derive(Debug, Clone)]
    struct TempTile {
        move_rule: TileMoveRules,
        spawn_kind: Option<SpawnKind>,
        animation_ids: Vec<u32>,
    }

    let mut temp_tile_lookup: HashMap<Position, TempTile, FxBuildHasher> = Default::default();

    // first iteration: combine tile configs of potentially multiple tiles on the same position
    for tile in &tile_entities {
        let Some(tile_config) = tile_configs.get(tile.tile_id).cloned() else {
            continue;
        };
        let position = Position {
            x: tile.x,
            y: tile.y,
            layer_number: tile.layer_number,
        };
        temp_tile_lookup
            .entry(position)
            .and_modify(|existing| {
                existing.move_rule = existing
                    .move_rule
                    .combine(tile_config.move_rule, |a, b| a.combine(b));
                existing.spawn_kind = existing.spawn_kind.or(tile_config.spawn_kind);
                if let Some(animation_id) = tile_config.animation_id {
                    existing.animation_ids.push(animation_id);
                }
            })
            .or_insert(TempTile {
                move_rule: tile_config.move_rule,
                spawn_kind: tile_config.spawn_kind,
                animation_ids: tile_config.animation_id.into_iter().collect(),
            });
    }

    // second iteration: consider "effects_on_other_tiles"
    for tile in &tile_entities {
        let Some(tile_config) = tile_configs.get(tile.tile_id) else {
            continue;
        };
        tile_config
            .effects_on_other_tiles
            .iter()
            .for_each(|(relative_position, effect)| {
                let absolute_position = Position {
                    x: relative_position.x + tile.x,
                    y: relative_position.y + tile.y,
                    layer_number: relative_position.layer_number + tile.layer_number,
                };
                let Some(temp_tile_ref) = temp_tile_lookup.get_mut(&absolute_position) else {
                    return;
                };
                let EffectOnOtherTilesConfig {
                    move_rule,
                    spawn_kind,
                    animation_id,
                } = effect.clone();

                if let Some(move_rule) = move_rule {
                    temp_tile_ref.move_rule = temp_tile_ref
                        .move_rule
                        .combine(move_rule, |a, b| a.combine(b));
                }
                temp_tile_ref.spawn_kind = temp_tile_ref.spawn_kind.or(spawn_kind);
                if let Some(animation_id) = animation_id {
                    temp_tile_ref.animation_ids.push(animation_id);
                }
            });
    }
    // third iteration: build final representation
    temp_tile_lookup
        .iter()
        .map(|(position, temp_tile)| {
            let move_rule = temp_tile.move_rule.map_with_direction(|direction, rule| {
                let target = rule
                    .leave_layer_shift
                    .apply(position.move_in_dir(direction));

                temp_tile_lookup
                    .get(&target)
                    .map(|target_tile| *target_tile.move_rule.get(direction.turn_180()))
                    .map(|opposing_rule| rule.opposing_to_move_rule(opposing_rule))
                    .unwrap_or_default()
            });
            (
                *position,
                Tile {
                    move_rule,
                    spawn_kind: temp_tile.spawn_kind,
                    animation_ids: temp_tile.animation_ids.clone(),
                },
            )
        })
        .collect::<MapTileCacheInner>()
}

#[async_trait::async_trait]
impl MoveRuleProvider for TileCache {
    async fn move_rules_for_map_id(
        &self,
        map_id: MapId,
    ) -> AppResult<Box<dyn MapMoveRuleProvider + Sync + Send>> {
        let move_rules = self.get(map_id).await?;
        Ok(Box::new(move_rules))
    }
}

#[async_trait::async_trait]
impl SpawnKindProvider for TileCache {
    async fn spawn_kinds_for_map_id(
        &self,
        map_id: MapId,
    ) -> AppResult<Box<dyn MapSpawnKindProvider>> {
        let move_rules = self.get(map_id).await?;
        Ok(Box::new(move_rules))
    }
}

#[derive(deepsize::DeepSizeOf)]
pub struct MapTileCache {
    inner: Arc<MapTileCacheInner>,
}

impl MapTileCache {
    pub(super) fn iter(&self) -> impl Iterator<Item = (Position, Tile)> + use<'_> {
        self.inner.iter().map(|(pos, tile)| (*pos, tile.clone()))
    }
}

impl MapMoveRuleProvider for MapTileCache {
    fn move_rule_for_position(&self, position: Position) -> Option<DirectionalMoveRules> {
        self.inner.get(&position).map(|tile| tile.move_rule)
    }

    fn move_rules(&self) -> Vec<(Position, DirectionalMoveRules)> {
        self.inner
            .iter()
            .map(|(position, tile)| (*position, tile.move_rule))
            .collect()
    }
}

impl MapSpawnKindProvider for MapTileCache {
    fn spawn_kinds(&self) -> Vec<(Position, SpawnKind)> {
        self.inner
            .iter()
            .filter_map(|(position, tile)| Some((*position, tile.spawn_kind?)))
            .collect()
    }
}

fn tracing_eviction_listener<V>(map_id: Arc<MapId>, _: V, removal_cause: RemovalCause) {
    match removal_cause {
        RemovalCause::Expired => tracing::debug!(message = "Cache entry expired", %map_id),
        RemovalCause::Explicit => tracing::info!(message = "Cache entry was invalidated and removed", %map_id),
        RemovalCause::Replaced => tracing::warn!(message = "Cache entry was manually replaced. We should not do this and use the database as a single source of truth.", %map_id),
        RemovalCause::Size => tracing::warn!(message = "Cache entry was removed due to size constraints. This might mean the size constraint is set too low, or we need to spin up more servers to handle the load, or some bug prevents the cache expiry from working."),
    }
}

#[cfg(test)]
mod tests {
    use arbitrary::{Arbitrary, Unstructured};

    use crate::{
        data::{MoveAbilities, MoveAbility},
        smeargle::data::{
            move_rule::{MoveRule, TileMoveRule},
            TileConfig,
        },
    };

    use super::*;

    #[test]
    fn simple_tile_config() -> anyhow::Result<()> {
        let u: [u8; 16] = rand::random();
        let mut u = Unstructured::new(&u);
        let tile_config = TileConfig::arbitrary(&mut u)?;
        let tile_configs = TileConfigs::new(Arc::new([TileConfig {
            tile_id: TileId::from(1),
            move_rule: TileMoveRules::from_single(TileMoveRule::from_allowed_move_abilities(
                MoveAbilities::empty() | MoveAbility::Walk | MoveAbility::Swim,
            )),
            ..tile_config
        }]));
        let map_cache = tile_entities_to_map_cache(
            vec![
                TileEntity {
                    x: 0,
                    y: 0,
                    layer_number: 0,
                    tile_id: TileId::from(1),
                },
                TileEntity {
                    x: 1,
                    y: 0,
                    layer_number: 0,
                    tile_id: TileId::from(1),
                },
            ],
            &tile_configs,
        );
        let tile_0_0 = map_cache
            .get(&Position {
                x: 0,
                y: 0,
                layer_number: 0,
            })
            .expect("tile at 0/0");
        assert_eq!(
            tile_0_0.move_rule,
            DirectionalMoveRules {
                left: MoveRule::default(),
                up: MoveRule::default(),
                down: MoveRule::default(),
                right: MoveRule::default() | MoveAbility::Walk | MoveAbility::Swim
            }
        );

        let tile_1_0 = map_cache
            .get(&Position {
                x: 1,
                y: 0,
                layer_number: 0,
            })
            .expect("tile at 1/0");

        assert_eq!(
            tile_1_0.move_rule,
            DirectionalMoveRules {
                right: MoveRule::default(),
                up: MoveRule::default(),
                down: MoveRule::default(),
                left: MoveRule::default() | MoveAbility::Walk | MoveAbility::Swim
            }
        );

        Ok(())
    }
}
