use std::{
    collections::{HashMap, HashSet},
    fmt::{self, Display},
};

use nonempty::NonEmpty;
use serde::{Deserialize, Deserializer};

use crate::smeargle::{
    data::{TileConfig, TileId},
    neighbour::{self, bitmask_neighbours, Neighbour, NeighbourBitmask, NEIGHBOURS},
    order::Orders,
};

use super::{comparison::Comparison, neighbour_config::NeighbourConfig};

pub type TileAlgorithms = HashMap<String, TileAlgorithm>;

#[derive(Debug, Clone)]
pub struct TileAlgorithm {
    order: Orders,
    defaults: NonEmpty<TileId>,
    options: Vec<AlgorithmOption>,
    overlays: HashMap<TileId, NonEmpty<(Neighbour, AlgorithmOption)>>,
}

#[derive(Debug, Clone)]
pub struct AlgorithmOption {
    tile_id: TileId,
    compare: Comparison,
    match_bitmask: NeighbourBitmask,
    additional_conditions: Vec<(Neighbour, TileId)>,
    relative_layer_number: i32,
}

type TileAlgorithmResult = HashMap<(i32, i32), NonEmpty<MatchingOption>>;

impl TileAlgorithm {
    pub fn run(&self, mut positions: Vec<(i32, i32)>) -> TileAlgorithmResult {
        let mut tiles: TileAlgorithmResult = HashMap::new();

        self.order.sort(&mut positions);
        let position_set: HashSet<&(i32, i32)> = positions.iter().collect();

        for (x, y) in &positions {
            let matching_option =
                find_matching_option(&position_set, &tiles, *x, *y, &self.options)
                    .unwrap_or(default_option(*x, *y, &self.defaults));
            let mut matching_overlay_options = self
                .overlays
                .get(&matching_option.tile_id)
                .iter()
                .flat_map(|overlay_options| overlay_options.iter())
                .filter_map(|(neighbour, overlay_option)| {
                    let matching_option =
                        find_matching_option(&position_set, &tiles, *x, *y, [overlay_option])?;
                    let x = *x + neighbour.x::<i32>();
                    let y = *y + neighbour.y::<i32>();
                    Some((x, y, matching_option))
                })
                .collect::<Vec<_>>();
            matching_overlay_options.push((*x, *y, matching_option));
            for (x, y, matching_option) in matching_overlay_options {
                set_matching_option(&mut tiles, x, y, matching_option)
            }
        }

        tiles
    }
}

fn set_matching_option(
    tiles: &mut TileAlgorithmResult,
    x: i32,
    y: i32,
    matching_option: MatchingOption,
) {
    tiles
        .entry((x, y))
        .and_modify(|tile_ids| tile_ids.push(matching_option))
        .or_insert(NonEmpty::singleton(matching_option));
}

fn find_matching_option<'a, I: IntoIterator<Item = &'a AlgorithmOption>>(
    positions: &HashSet<&(i32, i32)>,
    tiles: &TileAlgorithmResult,
    x: i32,
    y: i32,
    options: I,
) -> Option<MatchingOption> {
    let bitmask = present_neighbour_bitmask(positions, x, y);
    options
        .into_iter()
        .find(|option| {
            match_bitmasks(option.match_bitmask, option.compare, bitmask)
                && check_specific_neighbour_constraints(tiles, x, y, &option.additional_conditions)
        })
        .map(MatchingOption::from)
}

fn default_option(x: i32, y: i32, defaults: &NonEmpty<TileId>) -> MatchingOption {
    let default_index: usize = (x + y)
        .rem_euclid(defaults.len().try_into().unwrap())
        .try_into()
        .unwrap();
    let tile_id = *defaults.get(default_index).unwrap();
    MatchingOption {
        tile_id,
        relative_layer_number: 0,
    }
}

fn present_neighbour_bitmask(positions: &HashSet<&(i32, i32)>, x: i32, y: i32) -> NeighbourBitmask {
    let present_neighbours = NEIGHBOURS
        .into_iter()
        .map(|n| (n, (x + n.x::<i32>(), y + n.y::<i32>())))
        .filter(|(_, position)| positions.contains(position))
        .map(|(n, _)| n);

    neighbour::bitmask_neighbours(present_neighbours)
}

fn match_bitmasks(
    first: NeighbourBitmask,
    comparison: Comparison,
    second: NeighbourBitmask,
) -> bool {
    match comparison {
        Comparison::Exact => first == second,
        Comparison::AtLeast => first <= second,
        Comparison::AtMost => first >= second,
        Comparison::Inverted => !first == second,
        Comparison::AlwaysMatch => true,
    }
}

fn check_specific_neighbour_constraints(
    tiles: &TileAlgorithmResult,
    x: i32,
    y: i32,
    constraints: &[(Neighbour, TileId)],
) -> bool {
    constraints.iter().all(|(neighbour, tile_id_to_match)| {
        let x = x + neighbour.x::<i32>();
        let y = y + neighbour.y::<i32>();
        tiles.get(&(x, y)).is_some_and(|tile_ids| {
            tile_ids
                .iter()
                .any(|option| option.tile_id == *tile_id_to_match)
        })
    })
}

#[derive(Debug, Clone, Copy)]
pub struct MatchingOption {
    pub tile_id: TileId,
    pub relative_layer_number: i32,
}

impl From<&AlgorithmOption> for MatchingOption {
    fn from(value: &AlgorithmOption) -> Self {
        MatchingOption {
            tile_id: value.tile_id,
            relative_layer_number: value.relative_layer_number,
        }
    }
}

pub type TomlAlgorithms = HashMap<String, TomlAlgorithm>;

#[derive(Deserialize, Debug, Clone)]
pub struct TomlAlgorithm {
    #[serde(default)]
    order: Orders,
    default: TomlAlgorithmDefault,
    #[serde(default)]
    options: Vec<TomlAlgorithmOptions>,
    #[serde(default)]
    overlays: HashMap<String, NonEmpty<TomlOverlay>>,
}

#[derive(Debug, Clone)]
struct TomlAlgorithmDefault(NonEmpty<String>);

#[derive(Deserialize, Debug, Clone)]
struct TomlAlgorithmOptions {
    tile: String,
    comparison: Comparison,
    neighbours: Vec<NeighbourConfig>,
    #[serde(default)]
    relative_layer_number: i32,
}

#[derive(Deserialize, Debug, Clone)]
struct TomlOverlay {
    position: Neighbour,
    tile: String,
    #[serde(default)]
    comparison: Comparison,
    #[serde(default)]
    neighbours: Vec<NeighbourConfig>,
    #[serde(default)]
    relative_layer_number: i32,
}

pub fn parse_tile_algorithms(
    algorithms: TomlAlgorithms,
    tiles: &HashMap<String, TileConfig>,
) -> Result<TileAlgorithms, ConfigError> {
    algorithms
        .into_iter()
        .map(|(tile_group, algorithm)| {
            let algo = parse_tile_algorithm(algorithm, &tile_group, tiles)?;
            Ok((tile_group, algo))
        })
        .collect()
}

fn parse_tile_algorithm(
    algorithm: TomlAlgorithm,
    tile_group: &str,
    tiles: &HashMap<String, TileConfig>,
) -> Result<TileAlgorithm, ConfigError> {
    let order = algorithm.order;

    let namespace_tile = |tile_name: &str| format!("{}.{}", tile_group, tile_name);
    let get_tile_id = |tile_name: String| {
        tiles
            .get(&namespace_tile(&tile_name))
            .map(|tile_config| tile_config.tile_id)
            .ok_or(ConfigError::TileReferenceNotFound {
                tile_group: tile_group.to_string(),
                tile_name,
            })
    };

    let defaults = algorithm.default.0.try_map(get_tile_id)?;

    let options = algorithm
        .options
        .into_iter()
        .map(|opts| {
            let tile_id = get_tile_id(opts.tile)?;
            let match_bitmask = bitmask_neighbours(opts.neighbours.iter().map(|n| n.position));

            let additional_conditions = opts
                .neighbours
                .into_iter()
                .filter_map(|n| Some((n.position, n.content?)))
                .map(|(pos, tile_name)| {
                    let tile_id = get_tile_id(tile_name)?;
                    Ok((pos, tile_id))
                })
                .collect::<Result<_, ConfigError>>()?;
            Ok(AlgorithmOption {
                tile_id,
                match_bitmask,
                compare: opts.comparison,
                additional_conditions,
                relative_layer_number: opts.relative_layer_number,
            })
        })
        .collect::<Result<_, ConfigError>>()?;

    let overlays = algorithm
        .overlays
        .into_iter()
        .map(|(tile_name, toml_overlays)| {
            let tile_id = get_tile_id(tile_name)?;
            let overlays = toml_overlays.try_map(|toml_overlay| {
                let tile_id = get_tile_id(toml_overlay.tile)?;
                let match_bitmask =
                    bitmask_neighbours(toml_overlay.neighbours.iter().map(|n| n.position));
                let additional_conditions = toml_overlay
                    .neighbours
                    .into_iter()
                    .filter_map(|n| Some((n.position, n.content?)))
                    .map(|(pos, tile_name)| {
                        let tile_id = get_tile_id(tile_name)?;
                        Ok((pos, tile_id))
                    })
                    .collect::<Result<_, ConfigError>>()?;
                Ok((
                    toml_overlay.position,
                    AlgorithmOption {
                        tile_id,
                        compare: toml_overlay.comparison,
                        match_bitmask,
                        additional_conditions,
                        relative_layer_number: toml_overlay.relative_layer_number,
                    },
                ))
            })?;
            Ok((tile_id, overlays))
        })
        .collect::<Result<_, ConfigError>>()?;

    Ok(TileAlgorithm {
        order,
        defaults,
        options,
        overlays,
    })
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ConfigError {
    TileReferenceNotFound {
        tile_group: String,
        tile_name: String,
    },
}

impl Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ConfigError::TileReferenceNotFound {
                tile_group,
                tile_name,
            } => {
                write!(f, "Tile algorithm configuration for tile group '{tile_group}' includes a reference to the tile '{tile_name}', but the tile configuration
            does not include any tile with the name '{tile_name}'.
            ")
            }
        }
    }
}

impl<'de> Deserialize<'de> for TomlAlgorithmDefault {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct StringOrVec;

        impl<'de> serde::de::Visitor<'de> for StringOrVec {
            type Value = NonEmpty<String>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("string or list of strings")
            }

            fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(nonempty::nonempty![value.to_owned()])
            }

            fn visit_seq<S>(self, visitor: S) -> Result<Self::Value, S::Error>
            where
                S: serde::de::SeqAccess<'de>,
            {
                Deserialize::deserialize(serde::de::value::SeqAccessDeserializer::new(visitor))
            }
        }

        deserializer
            .deserialize_any(StringOrVec {})
            .map(TomlAlgorithmDefault)
    }
}
