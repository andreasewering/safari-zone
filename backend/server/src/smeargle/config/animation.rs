use std::{collections::HashMap, error::Error, fmt::Display};

use serde::Deserialize;

use crate::smeargle::{
    data::{TileConfig, TileId},
    proto,
};

pub type TomlAnimations = HashMap<String, TomlAnimation>;

#[derive(Deserialize, Debug, Clone)]
pub struct TomlAnimation {
    pub animation_id: u32,
    frames: Vec<TomlFrame>,
    rotate: bool,
}

#[derive(Deserialize, Debug, Clone)]
pub struct TomlFrame {
    tile: String,
    opacity: f32,
}

pub type Animations = Vec<Animation>;

#[derive(Deserialize, Debug, Clone)]
pub struct Animation {
    frames: Vec<Frame>,
    rotate: bool,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Frame {
    tile_id: TileId,
    opacity: f32,
}

pub fn parse_animations(
    animations: TomlAnimations,
    tiles: &HashMap<String, TileConfig>,
) -> Result<Animations, ConfigError> {
    let mut animations = animations
        .into_values()
        .map(|animation| -> Result<_, ConfigError> {
            let frames = animation
                .frames
                .into_iter()
                .map(|frame| -> Result<Frame, ConfigError> {
                    let tile = tiles
                        .get(&frame.tile)
                        .ok_or(ConfigError::TileReferenceNotFound(
                            animation.animation_id,
                            frame.tile,
                        ))?;
                    Ok(Frame {
                        tile_id: tile.tile_id,
                        opacity: frame.opacity,
                    })
                })
                .collect::<Result<Vec<Frame>, ConfigError>>()?;
            Ok((
                animation.animation_id,
                Animation {
                    frames,
                    rotate: animation.rotate,
                },
            ))
        })
        .collect::<Result<Vec<(u32, Animation)>, ConfigError>>()?;

    animations.sort_by_key(|(animation_id, _)| *animation_id);

    validate_sorted_animations(animations)
}

fn validate_sorted_animations(
    sorted_animations: Vec<(u32, Animation)>,
) -> Result<Animations, ConfigError> {
    let mut animations = Vec::new();
    for (expected_id, (animation_id, animation)) in sorted_animations.into_iter().enumerate() {
        let expected_id = expected_id as u32;
        if expected_id < animation_id {
            return Err(ConfigError::AnimationConfigShouldNotHaveGaps(expected_id));
        }
        if expected_id > animation_id {
            return Err(ConfigError::AnimationConfigShouldNotHaveDuplicates(
                expected_id,
            ));
        }
        animations.push(animation);
    }
    Ok(animations)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ConfigError {
    AnimationConfigShouldNotHaveGaps(u32),
    AnimationConfigShouldNotHaveDuplicates(u32),
    TileReferenceNotFound(u32, String),
}

impl Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ConfigError::AnimationConfigShouldNotHaveGaps(gap_id) => {
                write!(
                    f,
                    "Animation configuration should not have gaps, but id {} is missing!",
                    gap_id
                )
            }
            ConfigError::AnimationConfigShouldNotHaveDuplicates(duplicate_id) => {
                write!(
                    f,
                    "Animation configuration should not have duplicates, but id {} is assigned twice!",
                    duplicate_id
                )
            }
            ConfigError::TileReferenceNotFound(animation_id, tile) => {
                write!(f, "Animation configuration for animation_id {animation_id} has a frame set to the tile {tile}, but the tile configuration
              does not include any tile with the name {tile}.
              ")
            }
        }
    }
}

impl Error for ConfigError {}

impl From<Frame> for proto::smeargle::animation::Frame {
    fn from(value: Frame) -> Self {
        Self {
            index: u32::from(value.tile_id.unwrap()),
            opacity: value.opacity,
        }
    }
}

impl From<Animation> for proto::smeargle::Animation {
    fn from(value: Animation) -> Self {
        Self {
            rotate: value.rotate,
            frames: value
                .frames
                .into_iter()
                .map(proto::smeargle::animation::Frame::from)
                .collect(),
        }
    }
}
