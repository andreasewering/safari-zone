use serde::Deserialize;

#[derive(Debug, Clone, Copy, Deserialize, Default)]
#[serde(rename_all = "snake_case")]
pub enum Comparison {
    Exact,
    AtLeast,
    AtMost,
    Inverted,
    #[default]
    AlwaysMatch,
}
