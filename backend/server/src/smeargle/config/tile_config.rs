use std::{collections::HashMap, error::Error, fmt::Display, slice::Iter, sync::Arc};

use anyhow::Result;
use nonempty::NonEmpty;
use serde::Deserialize;

use crate::{
    data::{Position, SpawnKind},
    smeargle::data::{
        EffectOnOtherTilesConfig, LayerType, Origin, TileConfig, TileId, TileMoveRules,
    },
};

use super::{animation::TomlAnimations, move_rule::TomlMoveRules, partial::Inherit};

#[derive(Debug, Clone)]
pub struct TileConfigs(Arc<[TileConfig]>);

impl TileConfigs {
    pub fn new(tiles: Arc<[TileConfig]>) -> Self {
        Self(tiles)
    }
    pub fn get(&self, tile_id: TileId) -> Option<&TileConfig> {
        let TileConfigs(configs) = self;
        configs.get::<usize>((tile_id.unwrap() - 1).into())
    }
    pub fn length(&self) -> usize {
        let TileConfigs(configs) = self;
        configs.len()
    }
    pub fn iter(&self) -> Iter<TileConfig> {
        let TileConfigs(configs) = self;
        configs.iter()
    }
}

pub fn sort_tile_configs(map: HashMap<String, TileConfig>) -> Result<Vec<TileConfig>, ConfigError> {
    let mut values: Vec<TileConfig> = Vec::from_iter(map.into_values());
    values.sort_by_key(|config| config.tile_id);
    validate_sorted_config(&values)?;
    Ok(values)
}

pub fn parse_tile_configs(
    toml_config: TomlConfig,
    animations: &TomlAnimations,
) -> Result<HashMap<String, TileConfig>, ConfigError> {
    let mut configs = HashMap::new();
    parse_tile_configs_internal(
        toml_config,
        animations,
        ("".to_string(), TomlConfig::default()),
        "",
        &mut configs,
    )?;
    Ok(configs)
}

fn parse_tile_configs_internal(
    toml_config: TomlConfig,
    animations: &TomlAnimations,
    (path, defaults): (String, TomlConfig),
    parent_path: &str,
    configs: &mut HashMap<String, TileConfig>,
) -> Result<(), ConfigError> {
    if let Some(tile_id) = toml_config.tile_id {
        if let Some((key_of_child, _)) = toml_config.other.into_iter().next() {
            return Err(ConfigError::TileWasNotALeaf(
                tile_id,
                format!("{}.{}", path, key_of_child),
            ));
        }

        let origin = toml_config
            .origin
            .or(defaults.origin)
            .ok_or(ConfigError::MissingOriginForTileId(tile_id))?;

        let effects_on_other_tiles = toml_config
            .effects_on_other_tiles
            .clone()
            .inherit_from(defaults.effects_on_other_tiles.clone())
            .ok_or(ConfigError::MissingEffectsOnOtherTilesForTileId(tile_id))?;
        let effects_on_other_tiles = effects_on_other_tiles
            .into_iter()
            .map(|(position, config)| {
                (
                    Position::from(position),
                    EffectOnOtherTilesConfig::from(config),
                )
            })
            .collect();

        let move_rule = toml_config
            .move_rule
            .inherit_from(defaults.move_rule)
            .ok_or(ConfigError::MissingMoveRuleForTileId(
                tile_id,
                "Missing move_rule".to_string(),
            ))?;
        let move_rule = move_rule
            .into_tile_move_rules()
            .map_err(|msg| ConfigError::MissingMoveRuleForTileId(tile_id, msg))?;

        let layer_type = toml_config
            .layer_type
            .or(defaults.layer_type)
            .ok_or(ConfigError::MissingLayerTypeForTileId(tile_id))?;
        let keywords = toml_config
            .keywords
            .clone()
            .inherit_from(defaults.keywords.clone())
            .ok_or(ConfigError::MissingKeywordsForTileId(tile_id))?;
        let tile_group = parent_path.to_string();
        let spawn_kind = toml_config.spawn_kind.or(defaults.spawn_kind);
        let animation_id = match toml_config.animation.or(defaults.animation) {
            None => None,
            Some(animation) => Some(
                animations
                    .get(&animation)
                    .map(|a| a.animation_id)
                    .ok_or(ConfigError::TileAnimationNotFound(tile_id, animation))?,
            ),
        };

        configs.insert(
            path,
            TileConfig {
                tile_id,
                origin,
                tile_group,
                move_rule: TileMoveRules::from(move_rule),
                effects_on_other_tiles,
                layer_type,
                spawn_kind,
                keywords,
                animation_id,
            },
        );
        return Ok(());
    }

    toml_config
        .other
        .into_iter()
        .try_for_each(|(key, nested)| {
            let new_path = if path.is_empty() {
                key
            } else {
                format!("{}.{}", path, key)
            };
            let defaults = TomlConfig {
                tile_id: None,
                origin: toml_config.origin.clone().or(defaults.origin.clone()),
                move_rule: toml_config
                    .move_rule
                    .clone()
                    .inherit_from(defaults.move_rule.clone()),
                effects_on_other_tiles: toml_config
                    .effects_on_other_tiles
                    .clone()
                    .inherit_from(defaults.effects_on_other_tiles.clone()),
                layer_type: toml_config.layer_type.or(defaults.layer_type),
                spawn_kind: toml_config.spawn_kind.or(defaults.spawn_kind),
                keywords: toml_config
                    .keywords
                    .clone()
                    .inherit_from(defaults.keywords.clone()),
                animation: toml_config.animation.clone().or(defaults.animation.clone()),
                other: HashMap::new(),
            };

            parse_tile_configs_internal(nested, animations, (new_path, defaults), &path, configs)
        })?;
    Ok(())
}

#[derive(Deserialize, Debug, Clone, Default)]
pub struct TomlConfig {
    pub tile_id: Option<TileId>,
    pub origin: Option<NonEmpty<Origin>>,
    pub move_rule: Option<TomlMoveRules>,
    pub effects_on_other_tiles: Option<Vec<(TomlPosition, EffectOnOtherTilesTomlConfig)>>,
    pub layer_type: Option<LayerType>,
    pub spawn_kind: Option<SpawnKind>,
    pub keywords: Option<Vec<String>>,
    pub animation: Option<String>,
    #[serde(flatten)]
    pub other: HashMap<String, TomlConfig>,
}

#[derive(Deserialize, Debug, Clone, Default, Copy)]
pub struct TomlPosition {
    x: i32,
    y: i32,
    layer_number: i32,
}

#[derive(Debug, Clone, Deserialize)]
pub struct EffectOnOtherTilesTomlConfig {
    move_rule: Option<TomlMoveRules>,
    spawn_kind: Option<SpawnKind>,
    animation_id: Option<u32>,
}

impl From<TomlPosition> for Position {
    fn from(TomlPosition { x, y, layer_number }: TomlPosition) -> Self {
        Self { x, y, layer_number }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ConfigError {
    TileConfigShouldNotHaveGaps(u16),
    TileConfigShouldNotHaveDuplicates(u16),
    MissingOriginForTileId(TileId),
    MissingMoveRuleForTileId(TileId, String),
    MissingEffectsOnOtherTilesForTileId(TileId),
    MissingLayerTypeForTileId(TileId),
    MissingKeywordsForTileId(TileId),
    TileWasNotALeaf(TileId, String),
    TileAnimationNotFound(TileId, String),
}

impl Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ConfigError::TileConfigShouldNotHaveGaps(gap_id) => {
                write!(
                    f,
                    "Tile configuration should not have gaps, but id {} is missing!",
                    gap_id
                )
            }
            ConfigError::TileConfigShouldNotHaveDuplicates(duplicate_id) => {
                write!(
                    f,
                    "Tile configuration should not have duplicates, but id {} is assigned twice!",
                    duplicate_id
                )
            }
            ConfigError::MissingOriginForTileId(tile_id) => write!(
                f,
                "Tile configuration for tile_id {} is missing an 'origin' field!",
                tile_id
            ),
            ConfigError::MissingMoveRuleForTileId(tile_id, msg) => write!(
                f,
                "Tile configuration for tile_id {tile_id} is missing a 'move_rule' field! Details: {msg}",
            ),
            ConfigError::MissingEffectsOnOtherTilesForTileId(tile_id) => write!(
                f,
                "Tile configuration for tile_id {} is missing a 'effects_on_other_tiles' field!",
                tile_id
            ),
            ConfigError::MissingLayerTypeForTileId(tile_id) => write!(
                f,
                "Tile configuration for tile_id {} is missing a 'layer_type' field!",
                tile_id
            ),
            ConfigError::MissingKeywordsForTileId(tile_id) => write!(
                f,
                "Tile configuration for tile_id {} is missing a 'keywords' field!",
                tile_id
            ),
            ConfigError::TileWasNotALeaf(tile_id, path) => {
                write!(f, "Tile configuration for tile_id {} has a child under path {path}, this is invalid.
                If the child has a lot in common with its parent, you could consider adding a scope without tile_id,
                which has both tiles as children. 
                ", tile_id)
            }
            ConfigError::TileAnimationNotFound(tile_id, animation) => {
                write!(f, "Tile configuration for tile_id {tile_id} has animation set to {animation}, but the animation configuration
                does not include any animation with the name {animation}
                ")
            }
        }
    }
}

impl Error for ConfigError {}

fn validate_sorted_config(vec: &[TileConfig]) -> Result<(), ConfigError> {
    for (tile, i) in vec.iter().zip(0..) {
        let expected_tile_id = i + 1;
        if expected_tile_id < tile.tile_id.unwrap() {
            return Err(ConfigError::TileConfigShouldNotHaveGaps(expected_tile_id));
        }
        if expected_tile_id > tile.tile_id.unwrap() {
            return Err(ConfigError::TileConfigShouldNotHaveDuplicates(
                expected_tile_id,
            ));
        }
    }
    Ok(())
}

impl From<EffectOnOtherTilesTomlConfig> for EffectOnOtherTilesConfig {
    fn from(value: EffectOnOtherTilesTomlConfig) -> Self {
        let move_rule = value
            .move_rule
            .map(|move_rule| move_rule.inherit_from(TomlMoveRules::allow_all()))
            .and_then(|move_rule| move_rule.into_tile_move_rules().ok());

        EffectOnOtherTilesConfig {
            move_rule,
            spawn_kind: value.spawn_kind,
            animation_id: value.animation_id,
        }
    }
}

#[cfg(test)]
mod tests {
    use nonempty::nonempty;

    use super::*;

    use crate::{
        data::{Directional, MoveAbilities, MoveAbility},
        smeargle::data::{move_rule::TileMoveRule, LayerShift, OriginTileset},
    };

    #[test]
    fn single_tile_config() {
        let toml_config = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        move_rule = { enter_with = ["Swim"], leave_with = ["Swim"], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = []
        layer_type = "Ground"
        spawn_kind = "Water"
        keywords = ["test", "wow"]
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default()).unwrap();
        assert_eq!(
            config,
            HashMap::from([(
                "test_tile".to_string(),
                TileConfig {
                    origin: nonempty![Origin {
                        tileset: OriginTileset::Advance,
                        index: 42
                    }],
                    tile_id: TileId::from(5),
                    move_rule: TileMoveRules::from_single(
                        TileMoveRule::from_allowed_move_abilities(
                            MoveAbilities::empty() | MoveAbility::Swim
                        )
                    ),
                    tile_group: "".to_string(),
                    effects_on_other_tiles: vec![],
                    layer_type: LayerType::Ground,
                    spawn_kind: Some(SpawnKind::Water),
                    keywords: vec!["test".to_string(), "wow".to_string()],
                    animation_id: None
                }
            )])
        )
    }

    #[test]
    fn tile_config_without_origin() {
        let toml_config = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        move_rule = { enter_with = ["Swim"], leave_with = ["Swim"], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = []
        layer_type = "Ground"
        spawn_kind = "Water"
        keywords = ["test", "wow"]
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default());
        assert_eq!(
            config,
            Err(ConfigError::MissingOriginForTileId(TileId::from(5)))
        )
    }

    #[test]
    fn tile_config_with_empty_origin() {
        let config: Result<TomlConfig, _> = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        origin = []
        move_rule = { enter_with = ["Swim"], leave_with = ["Swim"], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = []
        layer_type = "Ground"
        spawn_kind = "Water"
        keywords = ["test", "wow"]
        "#,
        );
        assert!(config.is_err());
        assert!(format!("{}", config.unwrap_err()).contains("at least one"));
    }

    #[test]
    fn tile_config_without_move_rule() {
        let toml_config = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        effects_on_other_tiles = []
        layer_type = "Ground"
        spawn_kind = "Water"
        keywords = ["test", "wow"]
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default());
        assert_eq!(
            config,
            Err(ConfigError::MissingMoveRuleForTileId(
                TileId::from(5),
                "Missing move_rule".to_string()
            ))
        )
    }

    #[test]
    fn tile_config_without_effects_on_other_tiles() {
        let toml_config = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        move_rule = { enter_with = ["Swim"], leave_with = ["Swim"], enter_layer_shift = 0, leave_layer_shift = 0 }
        layer_type = "Ground"
        spawn_kind = "Water"
        keywords = ["test", "wow"]
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default());
        assert_eq!(
            config,
            Err(ConfigError::MissingEffectsOnOtherTilesForTileId(
                TileId::from(5)
            ))
        )
    }

    #[test]
    fn tile_config_without_layer_type() {
        let toml_config = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        move_rule = { enter_with = ["Swim"], leave_with = ["Swim"], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = []
        spawn_kind = "Water"
        keywords = ["test", "wow"]
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default());
        assert_eq!(
            config,
            Err(ConfigError::MissingLayerTypeForTileId(TileId::from(5)))
        )
    }

    #[test]
    fn tile_config_without_keywords() {
        let toml_config = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        move_rule = { enter_with = ["Swim"], leave_with = ["Swim"], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = []
        layer_type = "Ground"
        spawn_kind = "Water"
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default());
        assert_eq!(
            config,
            Err(ConfigError::MissingKeywordsForTileId(TileId::from(5)))
        )
    }

    #[test]
    fn tile_config_with_top_level_defaults() {
        let toml_config = toml::from_str(
            r#"
        move_rule = { enter_with = ["Walk"], leave_with = ["Walk"], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = []
        keywords = []

        [test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        layer_type = "Ground"
        spawn_kind = "Water"
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default()).unwrap();
        assert_eq!(
            config,
            HashMap::from([(
                "test_tile".to_string(),
                TileConfig {
                    origin: nonempty![Origin {
                        tileset: OriginTileset::Advance,
                        index: 42
                    }],
                    tile_id: TileId::from(5),
                    move_rule: TileMoveRules::from_single(
                        TileMoveRule::from_allowed_move_abilities(
                            MoveAbilities::empty() | MoveAbility::Walk
                        )
                    ),
                    tile_group: "".to_string(),
                    effects_on_other_tiles: vec![],
                    layer_type: LayerType::Ground,
                    spawn_kind: Some(SpawnKind::Water),
                    keywords: vec![],
                    animation_id: None
                }
            )])
        )
    }

    #[test]
    fn multiple_tile_configs() {
        let toml_config = toml::from_str(
            r#"
        [test_tile_1]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        layer_type = "Ground"
        spawn_kind = "Water"
        move_rule = { enter_with = ["Walk"], leave_with = ["Walk"], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = [[{ x = 0, y = 0, layer_number = -1 }, { move_rule = [] }]]
        keywords = ["a"]

        [small_tree.test_tile_2]
        tile_id = 6
        origin = [{ tileset = "Advance", index = 100 }]
        layer_type = "Decoration"
        move_rule = { enter_with = [], leave_with = [], enter_layer_shift = 0, leave_layer_shift = 0 }
        effects_on_other_tiles = []
        keywords = ["b"]
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default()).unwrap();
        assert_eq!(
            config,
            HashMap::from([
                (
                    "test_tile_1".to_string(),
                    TileConfig {
                        origin: nonempty![Origin {
                            tileset: OriginTileset::Advance,
                            index: 42
                        }],
                        tile_id: TileId::from(5),
                        move_rule: TileMoveRules::from_single(
                            TileMoveRule::from_allowed_move_abilities(
                                MoveAbilities::empty() | MoveAbility::Walk
                            )
                        ),
                        tile_group: "".to_string(),
                        effects_on_other_tiles: vec![(
                            Position {
                                x: 0,
                                y: 0,
                                layer_number: -1
                            },
                            EffectOnOtherTilesConfig {
                                move_rule: Some(Directional::from_single(
                                    TileMoveRule::from_allowed_move_abilities(
                                        MoveAbilities::empty()
                                    )
                                )),
                                spawn_kind: None,
                                animation_id: None
                            }
                        )],
                        layer_type: LayerType::Ground,
                        spawn_kind: Some(SpawnKind::Water),
                        keywords: vec!["a".to_string()],
                        animation_id: None
                    }
                ),
                (
                    "small_tree.test_tile_2".to_string(),
                    TileConfig {
                        origin: nonempty![Origin {
                            tileset: OriginTileset::Advance,
                            index: 100
                        }],
                        tile_id: TileId::from(6),
                        move_rule: TileMoveRules::from_single(
                            TileMoveRule::from_allowed_move_abilities(MoveAbilities::empty())
                        ),
                        tile_group: "small_tree".to_string(),
                        effects_on_other_tiles: vec![],
                        layer_type: LayerType::Decoration,
                        spawn_kind: None,
                        keywords: vec!["b".to_string()],
                        animation_id: None
                    }
                )
            ])
        )
    }

    #[test]
    fn nested_tile_configs() {
        let toml_config = toml::from_str(
            r#"
        keywords = ["top_level"]
        effects_on_other_tiles = []
        move_rule = { enter_with = ["Walk"], leave_with = ["Walk"], enter_layer_shift = 0, leave_layer_shift = 0 }

        [scope]  
        move_rule = []
        spawn_kind = "Water"
        layer_type = "Ground"
        keywords = ["scoped"]
  
        [scope.test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        keywords = ["direct"]
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default()).unwrap();
        assert_eq!(
            config,
            HashMap::from([(
                "scope.test_tile".to_string(),
                TileConfig {
                    origin: nonempty![Origin {
                        tileset: OriginTileset::Advance,
                        index: 42
                    }],
                    tile_id: TileId::from(5),
                    move_rule: TileMoveRules::from_single(
                        TileMoveRule::from_allowed_move_abilities(MoveAbilities::empty())
                    ),
                    tile_group: "scope".to_string(),
                    effects_on_other_tiles: vec![],
                    layer_type: LayerType::Ground,
                    spawn_kind: Some(SpawnKind::Water),
                    keywords: vec![
                        "direct".to_string(),
                        "scoped".to_string(),
                        "top_level".to_string()
                    ],
                    animation_id: None
                }
            )])
        )
    }

    #[test]
    fn tile_configs_with_id_have_to_be_leaves() {
        let toml_config = toml::from_str(
            r#"
        [test_tile]
        tile_id = 5
        origin = [{ tileset = "Advance", index = 42 }]
        move_rule = { enter_with = [], leave_with = [], enter_layer_shift = 0, leave_layer_shift = 0 }
        layer_type = "Ground"
        effects_on_other_tiles = []
        keywords = []

        [test_tile.sub_tile]
        tile_id = 6
        origin = [{ tileset = "Advance", index = 42 }]
        move_rule = { enter_with = [], leave_with = [], enter_layer_shift = 0, leave_layer_shift = 0 }
        layer_type = "Ground"
        effects_on_other_tiles = []
        keywords = []
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default());
        assert_eq!(
            config,
            Err(ConfigError::TileWasNotALeaf(
                TileId::from(5),
                "test_tile.sub_tile".to_string()
            ))
        )
    }

    #[test]
    fn nesting_move_rules() {
        let toml_config = toml::from_str(
            r#"
        move_rule = { enter_with = ["Swim"], leave_with = ["Walk"], enter_layer_shift = 0, leave_layer_shift = 0 }    

        [test_tile]
        move_rule = { enter_with = ["Fly"], enter_layer_shift = 1 }

        [test_tile.sub_tile]
        tile_id = 6
        origin = [{ tileset = "Advance", index = 42 }]
        move_rule = { left = { leave_with = ["Swim"] } }
        layer_type = "Ground"
        effects_on_other_tiles = []
        keywords = []
        "#,
        )
        .unwrap();
        let config = parse_tile_configs(toml_config, &HashMap::default()).unwrap();
        assert_eq!(
            config,
            HashMap::from([(
                "test_tile.sub_tile".to_string(),
                TileConfig {
                    origin: nonempty![Origin {
                        tileset: OriginTileset::Advance,
                        index: 42
                    }],
                    tile_id: TileId::from(6),
                    move_rule: TileMoveRules {
                        left: TileMoveRule {
                            leave: MoveAbilities::empty() | MoveAbility::Swim,
                            enter: MoveAbilities::empty() | MoveAbility::Fly,
                            enter_layer_shift: LayerShift::Up,
                            leave_layer_shift: LayerShift::None
                        },
                        up: TileMoveRule {
                            leave: MoveAbilities::empty() | MoveAbility::Walk,
                            enter: MoveAbilities::empty() | MoveAbility::Fly,
                            enter_layer_shift: LayerShift::Up,
                            leave_layer_shift: LayerShift::None
                        },
                        right: TileMoveRule {
                            leave: MoveAbilities::empty() | MoveAbility::Walk,
                            enter: MoveAbilities::empty() | MoveAbility::Fly,
                            enter_layer_shift: LayerShift::Up,
                            leave_layer_shift: LayerShift::None
                        },
                        down: TileMoveRule {
                            leave: MoveAbilities::empty() | MoveAbility::Walk,
                            enter: MoveAbilities::empty() | MoveAbility::Fly,
                            enter_layer_shift: LayerShift::Up,
                            leave_layer_shift: LayerShift::None
                        },
                    },
                    tile_group: "test_tile".to_string(),
                    effects_on_other_tiles: vec![],
                    layer_type: LayerType::Ground,
                    spawn_kind: None,
                    keywords: vec![],
                    animation_id: None
                }
            )])
        )
    }
}
