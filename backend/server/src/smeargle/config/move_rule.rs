use serde::{Deserialize, Deserializer};

use crate::{
    data::{Directional, MoveAbilities},
    smeargle::data::{move_rule::TileMoveRule, LayerShift, TileMoveRules},
};

use super::partial::Inherit;

#[derive(Clone, Deserialize, Debug)]
#[serde(untagged)]
pub enum TomlMoveRules {
    #[serde(deserialize_with = "deny_default")]
    Directional(Directional<Option<TomlMoveRule>>),
    Single(TomlMoveRule),
}

#[derive(Debug, Copy, Clone, Deserialize, PartialEq)]
#[serde(untagged)]
pub enum TomlMoveRule {
    #[serde(deserialize_with = "deny_default")]
    Specific(Specific),
    Single(MoveAbilities),
}

fn deny_default<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    D: Deserializer<'de>,
    T: Default + Deserialize<'de> + PartialEq,
{
    let t = T::deserialize(deserializer)?;
    if t == T::default() {
        return Err(serde::de::Error::custom("Default denied"));
    }
    Ok(t)
}

#[derive(Debug, Copy, Clone, Deserialize, Default, PartialEq)]
pub struct Specific {
    enter_with: Option<MoveAbilities>,
    leave_with: Option<MoveAbilities>,
    enter_layer_shift: Option<LayerShift>,
    leave_layer_shift: Option<LayerShift>,
}

impl TomlMoveRules {
    fn into_directional(self) -> Directional<Option<TomlMoveRule>> {
        match self {
            TomlMoveRules::Directional(directional) => directional,
            TomlMoveRules::Single(single) => Directional::from_single(Some(single)),
        }
    }

    pub fn allow_all() -> Self {
        TomlMoveRules::Single(TomlMoveRule::Specific(Specific {
            enter_with: Some(MoveAbilities::all()),
            leave_with: Some(MoveAbilities::all()),
            enter_layer_shift: Some(LayerShift::None),
            leave_layer_shift: Some(LayerShift::None),
        }))
    }

    pub fn into_tile_move_rules(self) -> Result<TileMoveRules, String> {
        self.into_directional().try_map(|dir, rule| {
            rule.ok_or(format!("Missing move_rule for direction {dir}"))?
                .to_tile_move_rule()
        })
    }
}

impl Inherit for TomlMoveRules {
    fn inherit_from(self, default: Self) -> Self {
        let directional = self.into_directional();
        let default = default.into_directional();
        TomlMoveRules::Directional(directional.inherit_from(default))
    }
}

impl Inherit for TomlMoveRule {
    fn inherit_from(self, other: Self) -> Self {
        let specific = self.to_specific();
        let default = other.to_specific();
        Self::Specific(specific.inherit_from(default))
    }
}

impl Inherit for Specific {
    fn inherit_from(self, other: Self) -> Self {
        Specific {
            enter_with: self.enter_with.or(other.enter_with),
            leave_with: self.leave_with.or(other.leave_with),
            enter_layer_shift: self.enter_layer_shift.or(other.enter_layer_shift),
            leave_layer_shift: self.leave_layer_shift.or(other.leave_layer_shift),
        }
    }
}

impl TomlMoveRule {
    fn to_specific(self) -> Specific {
        match self {
            TomlMoveRule::Specific(specific) => specific,
            TomlMoveRule::Single(with) => Specific {
                enter_with: Some(with),
                leave_with: Some(with),
                enter_layer_shift: None,
                leave_layer_shift: None,
            },
        }
    }

    fn to_tile_move_rule(self) -> Result<TileMoveRule, String> {
        let Specific {
            enter_with,
            leave_with,
            enter_layer_shift,
            leave_layer_shift,
        } = self.to_specific();
        Ok(TileMoveRule {
            enter: enter_with.ok_or("Missing enter_with".to_string())?,
            leave: leave_with.ok_or("Missing leave_with".to_string())?,
            enter_layer_shift: enter_layer_shift.ok_or("Missing enter_layer_shift".to_string())?,
            leave_layer_shift: leave_layer_shift.ok_or("Missing leave_layer_shift".to_string())?,
        })
    }
}
