use crate::data::Directional;

pub trait Inherit {
    fn inherit_from(self, other: Self) -> Self;
}

impl<T: Inherit> Inherit for Option<T> {
    fn inherit_from(self, default: Self) -> Self {
        match (self, default) {
            (Some(a), Some(b)) => Some(a.inherit_from(b)),
            (Some(a), None) => Some(a),
            (None, Some(b)) => Some(b),
            (None, None) => None,
        }
    }
}

impl<T> Inherit for Vec<T> {
    fn inherit_from(mut self, mut other: Self) -> Self {
        self.append(&mut other);
        self
    }
}

impl <T: Inherit> Inherit for Directional<T> {
    fn inherit_from(self, other: Self) -> Self {
        self.combine(other, |inner_1, inner_2| inner_1.inherit_from(inner_2))
    }
}
