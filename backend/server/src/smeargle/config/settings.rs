use crate::smeargle::tile_cache::TileCacheSettings;

use super::{
    animation::{self, Animations, TomlAnimations},
    tile_algorithm::{self, TileAlgorithms, TomlAlgorithms},
    tile_config, TileConfigs,
};
use serde::Deserialize;

#[derive(Debug, Deserialize, Clone)]
pub struct TilesetSettings {
    /// How many tiles per row the tileset will be made up of.
    pub tiles_per_row: u8,

    /// Width and height of each individual tile in the tileset.
    pub tile_size: u8,
}

#[derive(Debug, Deserialize, Clone)]
pub struct TomlSettings {
    pub tileset: TilesetSettings,
    pub tiles: tile_config::TomlConfig,
    pub animations: TomlAnimations,
    pub tile_algorithms: TomlAlgorithms,
    pub tile_cache: TileCacheSettings,
}

#[derive(Debug, Clone)]
pub struct Settings {
    /// Settings for the tileset i.e. the image containing all possible tiles/textures.
    pub tileset: TilesetSettings,

    /// Specifies which tiles exist and how they behave.
    pub tiles: TileConfigs,

    /// The list of animations. May be referenced by index in one or multiple `TileConfigs`.
    pub animations: Animations,

    /// How to automatically fill in large areas of similar tiles in a way that it "looks good".
    pub tile_algorithms: TileAlgorithms,

    /// Settings for the in-memory cache storing tile ids for positions.
    pub tile_cache: TileCacheSettings,
}

impl<'de> Deserialize<'de> for Settings {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let TomlSettings {
            tileset,
            tiles,
            animations,
            tile_algorithms,
            tile_cache,
        } = Deserialize::deserialize(deserializer)?;

        let tiles_map = tile_config::parse_tile_configs(tiles, &animations)
            .map_err(serde::de::Error::custom)?;
        let animations = animation::parse_animations(animations, &tiles_map)
            .map_err(serde::de::Error::custom)?;
        let tile_algorithms = tile_algorithm::parse_tile_algorithms(tile_algorithms, &tiles_map)
            .map_err(serde::de::Error::custom)?;

        let sorted = tile_config::sort_tile_configs(tiles_map).map_err(serde::de::Error::custom)?;
        let tiles = tile_config::TileConfigs::new(sorted.into());

        Ok(Settings {
            tileset,
            tiles,
            animations,
            tile_algorithms,
            tile_cache,
        })
    }
}

impl Settings {
    #[cfg(test)]
    pub fn read() -> Result<Self, klink::Error> {
        use std::env;

        #[derive(Deserialize)]
        struct WrappedConfig {
            smeargle: Settings,
        }
        let wrapped_config: WrappedConfig = {
            let root_path = env!("CARGO_MANIFEST_DIR");
            klink::read_config_from_dir(format!("{root_path}/config"))?
        };
        Ok(wrapped_config.smeargle)
    }
}
