use serde::{Deserialize, Deserializer};

use crate::smeargle::neighbour::Neighbour;

#[derive(Debug, Clone)]
pub struct NeighbourConfig {
    pub position: Neighbour,
    pub content: Option<String>,
}

impl<'de> Deserialize<'de> for NeighbourConfig {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(untagged)]
        enum NeighbourConfigVariants {
            String(Neighbour),
            Struct {
                position: Neighbour,
                #[serde(default)]
                content: Option<String>,
            },
        }

        Ok(match NeighbourConfigVariants::deserialize(deserializer)? {
            NeighbourConfigVariants::String(position) => Self {
                position,
                content: None,
            },
            NeighbourConfigVariants::Struct { position, content } => Self { position, content },
        })
    }
}
