use std::{
    collections::{BTreeMap, HashMap},
    sync::Arc,
};

use crate::data::{TrainerNumber, Translated};
use serde::Deserialize;

#[derive(Debug, Clone)]
pub struct Trainer {
    /// The name of the trainer in translated form.
    /// Example: "Captain" (in en.toml), "Kapitän" (in de.toml)
    pub display_name: Translated<Arc<str>>,

    /// Tags associated with the trainer.
    /// References the tags by key configured in `trainer/tags`.
    /// Example: [1]
    pub tags: Arc<[u16]>,
}

#[derive(Debug, Clone)]
pub struct TrainerConfig {
    /// Trainer configurations stored by ID
    trainers: BTreeMap<TrainerNumber, Trainer>,

    /// Tags are content filters - you can define any tags and assign them
    /// to trainers and the user can filter the list of trainers based
    /// on the configured tags.
    /// Example: "Male" (in en.toml), "Männlich" (in de.toml)
    tags: BTreeMap<u16, Translated<Arc<str>>>,
}

impl TrainerConfig {
    #[cfg(test)]
    pub fn read() -> Result<Self, klink::Error> {
        #[derive(Deserialize)]
        struct WrappedConfig {
            trainer: TrainerConfig,
        }
        let root_path = env!("CARGO_MANIFEST_DIR");
        let wrapped_config: WrappedConfig =
            klink::read_config_from_dir(format!("{root_path}/config"))?;
        Ok(wrapped_config.trainer)
    }
    pub fn get(&self, trainer_number: TrainerNumber) -> Option<&Trainer> {
        self.trainers.get(&trainer_number)
    }
    pub fn iter(&self) -> std::collections::btree_map::Iter<TrainerNumber, Trainer> {
        self.trainers.iter()
    }
    pub fn iter_tags(&self) -> std::collections::btree_map::Iter<u16, Translated<Arc<str>>> {
        self.tags.iter()
    }
}

impl IntoIterator for TrainerConfig {
    type Item = (TrainerNumber, Trainer);
    type IntoIter = std::collections::btree_map::IntoIter<TrainerNumber, Trainer>;

    fn into_iter(self) -> Self::IntoIter {
        self.trainers.into_iter()
    }
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct TomlTrainer {
    pub tags: Arc<[u16]>,
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct TrainerTranslation {
    pub display_name: Arc<str>,
}

#[derive(Debug, Deserialize, Clone)]
struct TomlTrainerConfig {
    data: HashMap<String, TomlTrainer>,
    de: HashMap<String, TrainerTranslation>,
    en: HashMap<String, TrainerTranslation>,
    fr: HashMap<String, TrainerTranslation>,
    tags: HashMap<String, TomlTag>,
}

#[derive(Debug, Deserialize, Clone)]
struct TomlTag {
    de: Arc<str>,
    en: Arc<str>,
    fr: Arc<str>,
}

impl<'de> Deserialize<'de> for TrainerConfig {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let toml_trainer_config = TomlTrainerConfig::deserialize(deserializer)?;

        let mut trainers: BTreeMap<TrainerNumber, Trainer> = BTreeMap::new();

        let get_translations = |trainer_number, transform: fn(&TrainerTranslation) -> Arc<str>| {
            let en =
                toml_trainer_config
                    .en
                    .get(&trainer_number)
                    .ok_or(serde::de::Error::custom(format!(
                        "Missing english translation for trainer {}",
                        trainer_number
                    )))?;
            let de =
                toml_trainer_config
                    .de
                    .get(&trainer_number)
                    .ok_or(serde::de::Error::custom(format!(
                        "Missing german translation for trainer {}",
                        trainer_number
                    )))?;
            let fr =
                toml_trainer_config
                    .fr
                    .get(&trainer_number)
                    .ok_or(serde::de::Error::custom(format!(
                        "Missing french translation for trainer {}",
                        trainer_number
                    )))?;
            Ok(Translated {
                de: transform(de),
                en: transform(en),
                fr: transform(fr),
            })
        };

        for (trainer_number, TomlTrainer { tags }) in toml_trainer_config.data.into_iter() {
            let trainer_no: TrainerNumber =
                trainer_number.parse().map_err(serde::de::Error::custom)?;
            let trainer = Trainer {
                display_name: get_translations(trainer_number, |t| t.display_name.clone())?,
                tags,
            };
            trainers.insert(trainer_no, trainer);
        }

        let mut tags: BTreeMap<u16, Translated<Arc<str>>> = BTreeMap::new();

        for (tag_id, toml_tag) in toml_trainer_config.tags.into_iter() {
            let tag_id = tag_id.parse().map_err(serde::de::Error::custom)?;
            let tag = Translated {
                de: toml_tag.de,
                en: toml_tag.en,
                fr: toml_tag.fr,
            };
            tags.insert(tag_id, tag);
        }

        Ok(TrainerConfig { trainers, tags })
    }
}
