use std::collections::BTreeMap;
use std::time::Duration;
use std::{collections::HashMap, sync::Arc};

use crate::data::Translated;
use crate::data::{MoveAbilities, MoveAbility};
use crate::data::{PokemonNumber, PokemonType};
use serde::Deserialize;

#[derive(Debug, Clone)]
pub struct Pokemon {
    /// The display name of the Pokémon.
    /// Example: "Squirtle" (in `en.toml`), "Schiggy" (in `de.toml`).
    pub name: Translated<Arc<str>>,

    /// The description of the Pokémon.
    /// Example: "Charizard flies around the sky in search of powerful opponents. It breathes fire of such great heat that it melts anything. However, it never turns its fiery breath on any opponent weaker than itself."
    /// (in en.toml)
    pub description: Translated<Arc<str>>,

    /// The height of the Pokémon in meters.
    /// Example: 0.5.
    pub height: f32,

    /// The weight of the Pokémon in kilograms.
    /// Example: 60.0.
    pub weight: f32,

    /// The primary type of the Pokémon.
    /// Example: "Water".
    pub primary_type: PokemonType,

    /// The secondary type of the Pokémon.
    /// Many Pokémon just have a single type, in which case this is `None`.
    /// In that case, the field is expected to not be present in the configuration.
    /// Example: "Fire".
    pub secondary_type: Option<PokemonType>,

    /// How likely it is for the Pokémon to be caught.
    /// Higher catch rate means easier to catch.
    /// Other factors such as choice of ball will also determine the final percentage of an attempt.
    /// Example: 100.
    pub catch_rate: u8,

    /// How quickly a Pokémon moves across the map.
    /// Higher move speed means faster movement.
    /// Example: 80
    pub move_speed: u8,

    /// Which tiles a Pokémon can move on.
    /// This is a bitmask dependant on the associated enums order so check the enum
    /// for concrete information.
    /// Example: 3
    pub move_ability: MoveAbilities,
}

#[derive(Debug, Clone)]
pub struct PokemonConfig {
    /// See docs on [`Pokemon`].
    pokemon: BTreeMap<PokemonNumber, Pokemon>,

    /// Which Pokémon should be suggested to the user as "starters" i.e. first Pokémon you get for free.
    starter_pokemon_numbers: Vec<PokemonNumber>,

    /// How many milliseconds it takes for a Pokémon with move speed `1` to travel across a single tile
    pub move_duration_base_in_ms: u32,
}

impl PokemonConfig {
    #[cfg(test)]
    pub fn read() -> Result<Self, klink::Error> {
        #[derive(Deserialize)]
        struct WrappedConfig {
            pokemon: PokemonConfig,
        }
        let root_path = env!("CARGO_MANIFEST_DIR");
        let wrapped_config: WrappedConfig =
            klink::read_config_from_dir(format!("{root_path}/config"))?;
        Ok(wrapped_config.pokemon)
    }

    pub fn get(&self, pokemon_number: PokemonNumber) -> Option<&Pokemon> {
        self.pokemon.get(&pokemon_number)
    }

    pub fn iter(&self) -> std::collections::btree_map::Iter<PokemonNumber, Pokemon> {
        self.pokemon.iter()
    }

    pub fn starters(&self) -> impl Iterator<Item = (PokemonNumber, &Pokemon)> {
        self.starter_pokemon_numbers
            .iter()
            .filter_map(|pokemon_number| {
                let pokemon = self.get(*pokemon_number);
                if pokemon.is_none() {
                    tracing::error!(message = "Starter pokemon not found in configuration. This should have been caught at init time/in tests.", %pokemon_number);
                }
                Some((*pokemon_number, pokemon?))
    })
    }

    pub fn is_starter(&self, pokemon_number: PokemonNumber) -> bool {
        self.starter_pokemon_numbers.contains(&pokemon_number)
    }

    /// Time it takes for a pokemon with a given move_speed stat
    /// to travel a length of one tile.
    /// This is the superior measurement for the backend, but for the frontend,
    /// it would be best to have a multiplier for a millisecond based increment (dt).
    /// We have
    ///     move_duration_in_ms -> 1 tile movement
    /// so
    ///     dt ms will get us `dt / move_duration_in_ms` far
    pub fn move_duration(&self, move_speed: u8) -> Duration {
        Duration::from_secs_f64(
            f64::from(self.move_duration_base_in_ms) / (f64::from(move_speed) * 1000.0),
        )
    }
}

impl IntoIterator for PokemonConfig {
    type Item = (PokemonNumber, Pokemon);
    type IntoIter = std::collections::btree_map::IntoIter<PokemonNumber, Pokemon>;

    fn into_iter(self) -> Self::IntoIter {
        self.pokemon.into_iter()
    }
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct TomlPokemon {
    pub height: f32,
    pub weight: f32,
    pub primary_type: PokemonType,
    pub secondary_type: Option<PokemonType>,
    pub catch_rate: u8,
    pub move_speed: u8,
    pub can_swim: bool,
    pub can_fly: bool,
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct PokemonTranslation {
    pub name: Arc<str>,
    pub description: Arc<str>,
}

#[derive(Debug, Deserialize, Clone)]
struct TomlPokemonConfig {
    data: HashMap<String, TomlPokemon>,
    starter: Vec<PokemonNumber>,
    move_duration_base_in_ms: u32,
    #[serde(flatten)]
    translations: HashMap<String, HashMap<String, PokemonTranslation>>,
}

impl<'de> Deserialize<'de> for PokemonConfig {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let TomlPokemonConfig {
            data,
            starter,
            move_duration_base_in_ms,
            translations,
        } = TomlPokemonConfig::deserialize(deserializer)?;

        let mut pokemon_config: BTreeMap<PokemonNumber, Pokemon> = BTreeMap::new();

        let get_translations = |pokemon_number, transform: fn(&PokemonTranslation) -> Arc<str>| {
            let get_translation = |lang| {
                let lang_translations =
                    translations
                        .get(lang)
                        .ok_or(serde::de::Error::custom(format!(
                            "No translations for language '{lang}'"
                        )))?;
                let translation =
                    lang_translations
                        .get(&pokemon_number)
                        .ok_or(serde::de::Error::custom(format!(
                            "Missing '{lang}' translation for pokemon {pokemon_number}",
                        )))?;
                Ok(transform(translation))
            };
            Ok(Translated {
                en: get_translation("en")?,
                de: get_translation("de")?,
                fr: get_translation("fr")?,
            })
        };

        for (
            pokemon_number,
            TomlPokemon {
                height,
                weight,
                primary_type,
                secondary_type,
                catch_rate,
                move_speed,
                can_swim,
                can_fly,
            },
        ) in data.into_iter()
        {
            let pokemon_no = pokemon_number
                .parse::<PokemonNumber>()
                .map_err(serde::de::Error::custom)?;
            let mut move_ability = MoveAbilities::default() | MoveAbility::Walk;
            if can_swim {
                move_ability.insert(MoveAbility::Swim);
            }
            if can_fly {
                move_ability.insert(MoveAbility::Fly);
            }
            let pokemon = Pokemon {
                name: get_translations(pokemon_number.clone(), |t| t.name.clone())?,
                description: get_translations(pokemon_number, |t| t.description.clone())?,
                height,
                weight,
                primary_type,
                secondary_type,
                catch_rate,
                move_speed,
                move_ability,
            };
            pokemon_config.insert(pokemon_no, pokemon);
        }

        for starter_pokemon_number in &starter {
            // validate that starter pokemon are actually present in the main configuration
            if !pokemon_config.contains_key(starter_pokemon_number) {
                return Err(serde::de::Error::custom(format!("Starter Pokemon {starter_pokemon_number} is invalid since it is not included in the pokemon configuration.")));
            }
        }

        Ok(PokemonConfig {
            pokemon: pokemon_config,
            starter_pokemon_numbers: starter,
            move_duration_base_in_ms,
        })
    }
}
