= Nginx

The load balancer and static file server for the application.

== Built-in Load Balancing

For distributing load among multiple instances of the same server, Nginx offers http://nginx.org/en/docs/http/ngx_http_upstream_module.html[upstream groups].

There are multiple strategies implemented in Nginx and in custom modules how exactly Nginx decides which server to sent a request to. Aside from the basic "round-robin" (each gets the same number of requests), there a more advanced strategies such as http://nginx.org/en/docs/http/ngx_http_upstream_module.html#hash[hashing a part of the request].

This is exactly what we need to implement sharding for our application, using the map_id (if available).

== Limitations

Each backend server needs to know for which map_ids it will receive requests, the hash algorithm needs to be simple and yield the same results in our Rust code as it does in Nginx. 
The directive in Nginx is overly complicated for our usecase, implementing a https://en.wikipedia.org/wiki/Consistent_hashing[consistent hash ring].

== Solution

We implement our own nginx upstream module called "simple_hash" and compile Nginx from source with it. The hashing function is trivial enough to re-implement in Rust.
To be absolutely certain that the same logic is called, we would need a way for Nginx to use a Rust module or alternatively for Rust to make a C FFI call to the Nginx code.

Sadly both approaches are not viable at the moment:

* Nginx modules in Rust are in https://github.com/dcoles/nginx-rs[their] https://github.com/nginxinc/ngx-rust[babysteps] right now
* C FFI does not work in a straight forward way - Nginx uses quite a lot of C macros/preprocessing, which we would need to emulate/reproduce

Due to the latter problem, it also becomes quite hard to test if the hash algorithms are the same (i.e. same inputs -> same outputs).
Our test therefore actually starts up Nginx and tests the property for a fixed amount of randomly generated UUIDs.

== Development

Clone the nginx source code from https://github.com/nginx/nginx
and install "pcre2", then run ./auto/configure from the cloned nginx root directory.