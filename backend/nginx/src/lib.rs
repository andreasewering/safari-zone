/**
 * Adapted and converted from C to Rust: https://github.com/nginx/nginx/blob/master/src/core/ngx_crc32.h
 */
const NGX_CRC32_TABLE_SHORT: [u32; 16] = [
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac, 0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c, 0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c,
];

pub fn ngx_crc32_short(p: &[u8]) -> u32 {
    let mut crc: u32 = 0xffffffff;

    for &c in p.iter() {
        let index1 = ((crc ^ u32::from(c & 0xf)) & 0xf) as usize;
        crc = NGX_CRC32_TABLE_SHORT[index1] ^ (crc >> 4);
        let index2 = ((crc ^ u32::from(c >> 4)) & 0xf) as usize;
        crc = NGX_CRC32_TABLE_SHORT[index2] ^ (crc >> 4);
    }

    crc ^ 0xffffffff
}

#[cfg(test)]
#[test]
fn examples() {
    assert_eq!(ngx_crc32_short("0".as_bytes()) % 5, 4);
    assert_eq!(ngx_crc32_short("a".as_bytes()) % 5, 2);
    assert_eq!(ngx_crc32_short("b".as_bytes()) % 5, 1);
    assert_eq!(ngx_crc32_short("c".as_bytes()) % 5, 0);
}
