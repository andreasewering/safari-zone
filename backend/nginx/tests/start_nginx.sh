#!/bin/sh

# Get the directory of the script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo $SCRIPT_DIR

docker run -p 6800:80 \
 -e SERVER_HOST=host.docker.internal \
 -v "$SCRIPT_DIR/templates":/etc/nginx/templates/ \
 nginx_local
