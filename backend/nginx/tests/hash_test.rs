/**
 * Tests that the hash algorithms implemented in
   nginx_http_upstream_simple_hash_module and
   src yield the same outputs for the same inputs.
 * For our usecase we only care about this working for u64s,
   so we only test random u64s although this should
   theoretically work for any string.

 * The tests assume that there is a container running
   named "nginx_test" exposing port 6800 built with the ../Dockerfile.
 * Use the start_nginx.sh script (or slightly adjust it) to start the container.
*/
use std::{collections::BTreeMap, sync::Arc, time::Duration};

use hyper::{client::HttpConnector, Client, Request, StatusCode};
use nginx::ngx_crc32_short;

static PORTS: [u16; 5] = [6666, 6667, 6668, 6669, 6670];

/// TODO: Retry once issue https://github.com/docker/for-mac/issues/7332 has been resolved
#[tokio::test(flavor = "multi_thread")]
async fn nginx_simple_hash_integration_test() {
    let servers = tokio::spawn(server::start_servers(&PORTS));
    tokio::time::sleep(Duration::from_secs(1)).await;

    let nginx_port = std::env::var("NGINX_PORT").unwrap_or(6800.to_string());
    let nginx_host = std::env::var("NGINX_HOST").unwrap_or("localhost".to_string());
    let amount_of_requests = std::env::var("TEST_SIZE")
        .map(|size| str::parse(&size).unwrap())
        .unwrap_or(100usize);

    let client = Arc::new(Client::new());
    let request_uri = format!("http://{nginx_host}:{nginx_port}/");

    for _ in 0..100 {
        let result = send_request(&client, &request_uri, Some("18295059805903370206"), None).await;
        println!("Warmup {result:?}");
        if result.is_ok() {
            println!("Warmup complete!");
            break;
        }
    }

    println!("----- LEAST CONN START -----");
    let least_conn_result = least_conn_for_no_header_requests(client.clone(), &request_uri).await;
    println!("----- LEAST CONN END -----");

    // Give nginx some time in between tests
    tokio::time::sleep(Duration::from_secs(5)).await;

    println!("----- SAME HASH START -----");
    let same_hash_result = same_hash_result(&client, &request_uri, amount_of_requests).await;
    println!("----- SAME HASH END -----");

    // Give nginx some time in between tests
    tokio::time::sleep(Duration::from_secs(5)).await;

    println!("----- ROUND ROBIN START -----");
    let round_robin_result = round_robin_fallback(&client, &request_uri).await;
    println!("----- ROUND ROBIN END -----");

    let test_results = [same_hash_result, least_conn_result, round_robin_result];

    tokio::time::sleep(Duration::from_secs(1)).await;

    servers.abort();

    for result in test_results {
        result.expect("Test failed.");
    }
}

async fn same_hash_result(
    client: &hyper::Client<HttpConnector>,
    request_uri: &str,
    amount_of_requests: usize,
) -> anyhow::Result<()> {
    for _ in 0..amount_of_requests {
        let client = client.clone();
        let map_id: u64 = rand::random();
        let map_id = map_id.to_string();
        let response = send_request(&client, request_uri, Some(&map_id), None).await?;
        let correct_server_index = ngx_crc32_short(map_id.as_bytes()) % response.total;

        anyhow::ensure!(Some(map_id.to_string()) == response.map_id);
        anyhow::ensure!(
            correct_server_index == response.index,
            "Assertion failed on ID {map_id}.\n
                  Nginx chose server with index {}\n
                  Rust chose server with index {correct_server_index}",
            response.index
        );
    }

    Ok(())
}

async fn least_conn_for_no_header_requests(
    client: Arc<hyper::Client<HttpConnector>>,
    request_uri: &str,
) -> anyhow::Result<()> {
    // This test is currently hard coded to work for exactly 5 servers.
    // The idea is that we send 4 requests where we know which server they will go to that will stall for some duration.
    // While the requests are still ongoing, we send a fifth request that should always hit the server
    // that has not received a request yet.
    let mut open_requests = tokio::task::JoinSet::new();
    for id in [
        "0", // -> 4
        "a", // -> 2
        "b", // -> 1,
        "c", // -> 0
    ] {
        for i in 0..20 {
            let client = client.clone();
            let request_uri = request_uri.to_owned();

            open_requests.spawn(async move {
                tokio::time::sleep(Duration::from_millis(10 * i)).await;
                let request = send_request(
                    &client,
                    &request_uri,
                    Some(id),
                    Some(Duration::from_millis(500)),
                );
                let result = tokio::time::timeout(Duration::from_secs(2), request).await;
                println!("result {result:?}");
            });
        }
    }

    println!("Joinset count: {}", open_requests.len());
    let (_, response) = tokio::join!(
        tokio::time::timeout(Duration::from_secs(20), async {
            let mut count = 0;
            while open_requests.join_next().await.is_some() {
                // wait for all requests to finish
                count += 1;
                println!("Finished request {count}");
            }
        }),
        async {
            tokio::time::sleep(Duration::from_millis(200)).await;
            send_request(&client, request_uri, None, None).await
        }
    );

    anyhow::ensure!(response?.index == 3);

    Ok(())
}

async fn round_robin_fallback(
    client: &hyper::Client<HttpConnector>,
    request_uri: &str,
) -> anyhow::Result<()> {
    let amount_of_requests = 50;
    let mut handled_server_indices: BTreeMap<u32, usize> = BTreeMap::new();

    for _ in 0..amount_of_requests {
        let client = client.clone();
        let response = send_request(&client, request_uri, None, None).await?;

        anyhow::ensure!(response.map_id.is_none());
        handled_server_indices
            .entry(response.index)
            .and_modify(|handled_requests| *handled_requests += 1)
            .or_default();
    }

    for i in 0u32..PORTS.len().try_into().unwrap() {
        let handled_requests = handled_server_indices.get(&i).copied().unwrap_or_default();
        let expected_minimal_handled_requests = amount_of_requests / PORTS.len() - 2;
        let expected_maximal_handled_requests = amount_of_requests / PORTS.len() + 2;
        anyhow::ensure!(handled_requests >= expected_minimal_handled_requests, "Expected at least {expected_minimal_handled_requests}, but server with index {i} only handled {handled_requests}");
        anyhow::ensure!(handled_requests <= expected_maximal_handled_requests, "Expected at most {expected_maximal_handled_requests}, but server with index {i} handled {handled_requests}");
    }

    Ok(())
}

async fn send_request(
    client: &hyper::Client<HttpConnector>,
    request_uri: &str,
    map_id: Option<&str>,
    delay: Option<Duration>,
) -> anyhow::Result<server::ServerResponse> {
    let mut builder = Request::builder();
    if let Some(map_id) = map_id {
        builder = builder.header("x-map-id", map_id);
    }
    let delay = delay.unwrap_or_default();
    let request = builder
        .method("GET")
        .uri(request_uri)
        .body(hyper::Body::from(serde_json::to_vec(
            &server::ServerRequest { delay },
        )?))?;
    println!("Starting request for {map_id:?}");
    let response = client.request(request).await?;
    let status = response.status();
    println!("got response {}", status);
    let body = hyper::body::to_bytes(response.into_body()).await?;
    let body: server::ServerResponse = serde_json::from_slice(&body)?;
    anyhow::ensure!(status == StatusCode::OK);
    println!("Finished request for {map_id:?}: {}", body.index);
    Ok(body)
}

mod server {

    use std::io;
    use std::time::Duration;

    use hyper::service::{make_service_fn, service_fn};
    use hyper::{Body, Request, Response, Server};
    use serde::{Deserialize, Serialize};

    pub async fn start_servers(ports: &[u16]) -> io::Result<()> {
        let total = u32::try_from(ports.len()).unwrap();

        let mut set: tokio::task::JoinSet<Config> = tokio::task::JoinSet::new();
        for (index, port) in ports.iter().enumerate() {
            let index = u32::try_from(index).unwrap();
            let port = *port;
            let cfg = Config { port, index, total };
            set.spawn(async move {
                start_server(cfg).await.unwrap();
                cfg
            });
        }

        while let Some(cfg) = set.join_next().await {
            println!("Stopped server with config {:?}", cfg);
        }
        Ok(())
    }

    #[derive(Debug, Clone, Copy)]
    pub struct Config {
        index: u32,
        total: u32,
        port: u16,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct ServerResponse {
        pub map_id: Option<String>,
        pub total: u32,
        pub index: u32,
    }

    #[derive(Debug, Serialize, Deserialize, Default)]
    pub struct ServerRequest {
        pub delay: Duration,
    }

    async fn handle_request(
        req: Request<Body>,
        cfg: Config,
    ) -> Result<Response<Body>, hyper::Error> {
        let response = match req.headers().get("x-map-id") {
            None => ServerResponse {
                map_id: None,
                total: cfg.total,
                index: cfg.index,
            },
            Some(map_id) => ServerResponse {
                map_id: Some(
                    map_id
                        .to_str()
                        .inspect_err(|err| println!("Non Utf-8 ID {err}"))
                        .expect("Non Utf-8 ID")
                        .to_owned(),
                ),
                total: cfg.total,
                index: cfg.index,
            },
        };

        let body = hyper::body::to_bytes(req.into_body()).await?;
        println!("Body of request: {:?}", String::from_utf8_lossy(&body));
        let body: ServerRequest = serde_json::from_slice(&body)
            .inspect_err(|error| println!("Failed to parse body: {error}"))
            .unwrap_or_default();
        println!("Received request {:?}", body);
        tokio::time::sleep(body.delay).await;

        let response = Response::builder()
            .status(200)
            .header("Content-Type", "application/json")
            .body(Body::from(
                serde_json::to_string(&response)
                    .inspect_err(|err| println!("Serialize to json failed: {err}"))
                    .expect("Serialize to json failed."),
            ));
        Ok(response
            .inspect_err(|err| println!("Built response was invalid: {err}"))
            .expect("Built response was invalid"))
    }

    async fn start_server(cfg: Config) -> io::Result<()> {
        let addr = ([127, 0, 0, 1], cfg.port).into();
        let make_svc = make_service_fn(move |_conn| async move {
            Ok::<_, hyper::Error>(service_fn(move |req| handle_request(req, cfg)))
        });
        let server = Server::bind(&addr).serve(make_svc);
        println!("Listening on http://{}", addr);

        if let Err(e) = server.await {
            eprintln!("server error: {}", e);
        }

        Ok(())
    }
}
