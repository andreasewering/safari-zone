user nginx;
worker_processes auto;

error_log /var/log/nginx/error.log warn;
pid /var/run/nginx.pid;


events {
  worker_connections 1024;
}


http {
  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  log_format main
    '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';

  access_log /var/log/nginx/access.log main;

  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;

  client_body_buffer_size 16K;
  client_header_buffer_size 1k;
  large_client_header_buffers 2 1k;
  client_max_body_size 8m;

  client_body_timeout 12;
  client_header_timeout 12;
  send_timeout 10;

  keepalive_timeout 65;

  gzip on;
  gzip_static on;
  gzip_vary on;

  brotli on;
  # value between 0-11, 4 is a sweetspot for dynamic compression apparently
  brotli_comp_level 4;
  # js is precompressed with webpack compression plugin 
  brotli_static on;

  map $http_accept $webp_suffix {
    default   "";
    "~*webp"  ".webp";
  }

  include /etc/nginx/conf.d/*.conf;
}