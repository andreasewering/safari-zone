use std::{env, path::Path};

use proc_macro::TokenStream;
use quote::{TokenStreamExt as _, quote};

#[proc_macro]
pub fn include_migrations(_input: TokenStream) -> TokenStream {
    let migrations_path = "./migrations"; // Define the path to your migrations
    let path = Path::new(&env::var("CARGO_MANIFEST_DIR").expect("CARGO_MANIFEST_DIR not set"))
        .join(migrations_path);
    let migrations = database::read_migrations(&path)
        .expect("Unable to read migrations directory")
        .into_iter()
        .map(QuoteWrapper)
        .collect::<Vec<_>>();

    let gen_ = quote! {
        vec![#(#migrations),*]
    };

    gen_.into()
}

#[proc_macro_attribute]
pub fn test(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let input_fn = syn::parse_macro_input!(item as syn::ItemFn);

    let migration_setup = quote! {
        database::execute_migrations(&mut conn, database_macros::include_migrations!()).await.unwrap();
    };

    let syn::ItemFn {
        attrs,
        vis,
        sig,
        block,
    } = input_fn;
    let name = sig.ident;
    let ret = sig.output;
    let inputs = sig.inputs;
    let expanded = quote! {
        #(#attrs)*
        #[::sqlx::test(migrations = false)]
        #vis async fn #name(#inputs) #ret {
            let mut conn = pool.acquire().await.unwrap();
            #migration_setup
            drop(conn);
            #block
        }
    };

    TokenStream::from(expanded)
}

struct QuoteWrapper<T>(T);

impl quote::ToTokens for QuoteWrapper<database::Migration> {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        let database::Migration {
            timestamp,
            description,
            up_sql,
            down_sql,
        } = &self.0;

        let ts = quote::quote!(
            ::database::Migration {
               timestamp: #timestamp,
               description: #description.to_owned(),
               up_sql: #up_sql.to_owned(),
               down_sql: #down_sql.to_owned()
            }
        );
        tokens.append_all(ts);
    }
}
