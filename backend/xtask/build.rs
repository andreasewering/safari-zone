fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("cargo:rerun-if-changed=build.rs");

    prost_build::compile_protos(
        &["../../proto/deployment.proto"],
        &["../../proto"],
    )?;

    Ok(())
}
