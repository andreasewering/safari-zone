ARG BASE_IMAGE
FROM ${BASE_IMAGE}

ARG FRONTEND_CONF_PATH

ENV DEBUG_HEADERS ""
# Load balancing config
COPY templates/server.conf.template /etc/nginx/templates/
COPY ${FRONTEND_CONF_PATH} /etc/nginx/conf.d/frontend.locconf
# Copy frontend files
COPY /dist /usr/share/nginx