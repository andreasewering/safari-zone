use std::{
    collections::{BTreeMap, HashMap},
    iter::zip,
    sync::Arc,
};

use indexmap::IndexMap;
use serde::Deserialize;

mod flags {
    xflags::xflags!(
      cmd cfg {

      /// Pull data from remote sources and write it into config files
      cmd pull {
        /// Specify Pokémon numbers
        repeated -p,--pokemon pokemon: u16

        /// Specify languages
        repeated -l,--lang languages: String
      }
      }
    );
}

fn main() -> anyhow::Result<()> {
    match flags::Cfg::from_env_or_exit().subcommand {
        flags::CfgCmd::Pull(flags) => pull(flags),
    }
}

/**
 * I want to deserialize the existing pokemon toml configs in a way where I assume the absolute minimum about its structure.
 * Why? Because the script should sync a part of the configuration, not all. So the script should only be able to write/overwrite data it knows about.
 * By default, the script will sync all pokemon and all languages.
 * Using --pokemon 12 --pokemon 23, the user can only sync the pokemon with numbers 12 and 23.
 * Using --lang en --lang fr, the user can only sync the languages en and fr.
 */
fn pull(flags: flags::Pull) -> anyhow::Result<()> {
    let pokemon_numbers = if flags.pokemon.is_empty() {
        (1..=151).collect()
    } else {
        flags.pokemon
    };

    let languages: Vec<String> = if flags.lang.contains(&"all".to_string()) {
        vec!["de", "en", "fr"]
            .into_iter()
            .map(|str| str.to_owned())
            .collect()
    } else {
        flags.lang.iter().map(|lang| lang.to_lowercase()).collect()
    };
    println!("Syncing languages {languages:?}");

    let config_dir = concat!(env!("CARGO_MANIFEST_DIR"), "/../backend/config");
    let sh = xshell::Shell::new()?;
    let client = reqwest::blocking::Client::new();

    let responses = pokemon_numbers
        .iter()
        .map(|pokemon_number| {
            let pokemon_url = format!("https://pokeapi.co/api/v2/pokemon/{}", pokemon_number);
            let species_url = format!(
                "https://pokeapi.co/api/v2/pokemon-species/{}",
                pokemon_number
            );

            println!("Making request to {pokemon_url}");
            let pokemon: Pokemon = client.get(pokemon_url).send()?.json()?;
            println!("Making request to {species_url}");
            let species: PokemonSpecies = client.get(species_url).send()?.json()?;
            Ok::<_, anyhow::Error>((pokemon, species))
        })
        .collect::<anyhow::Result<Vec<_>>>()?;

    sh.change_dir(config_dir);
    let language_agnostic_config = sh.read_file("pokemon/data.toml")?;
    let existing_pokemon: BTreeMap<String, TomlPokemon> =
        toml::from_str(&language_agnostic_config)?;
    let mut pokemon: BTreeMap<u16, TomlPokemon> = BTreeMap::new();
    existing_pokemon
        .into_iter()
        .filter_map(|(pokemon_number, pokemon)| {
            let pokemon_number: u16 = str::parse(&pokemon_number).ok()?;
            Some((pokemon_number, pokemon))
        })
        .for_each(|(pokemon_number, p)| {
            pokemon.insert(pokemon_number, p);
        });

    for (response, pokemon_number) in zip(
        responses.clone().into_iter(),
        pokemon_numbers.iter().copied(),
    ) {
        let updated = update_pokemon(pokemon.remove(&pokemon_number), response)?;
        pokemon.insert(pokemon_number, updated);
    }
    let new_pokemon: IndexMap<String, TomlPokemon> = pokemon
        .into_iter()
        .map(|(n, p)| (n.to_string(), p))
        .collect();

    let new_language_agnostic_config = toml::to_string(&new_pokemon)?;
    sh.write_file("pokemon/data.toml", new_language_agnostic_config)?;

    for language in languages {
        let lang_config = sh.read_file(format!("pokemon/{language}.toml"))?;

        let existing_pokemon: BTreeMap<String, TomlI18nPokemon> = toml::from_str(&lang_config)?;
        let mut pokemon: BTreeMap<u16, TomlI18nPokemon> = BTreeMap::new();
        existing_pokemon
            .into_iter()
            .filter_map(|(pokemon_number, pokemon)| {
                let pokemon_number: u16 = str::parse(&pokemon_number).ok()?;
                Some((pokemon_number, pokemon))
            })
            .for_each(|(pokemon_number, p)| {
                pokemon.insert(pokemon_number, p);
            });

        for (response, pokemon_number) in zip(
            responses.clone().into_iter(),
            pokemon_numbers.iter().copied(),
        ) {
            let updated = update_i18n_pokemon(
                pokemon_number,
                &language,
                pokemon.remove(&pokemon_number),
                response,
            )?;
            pokemon.insert(pokemon_number, updated);
        }

        let new_pokemon: IndexMap<String, TomlI18nPokemon> = pokemon
            .into_iter()
            .map(|(n, p)| (n.to_string(), p))
            .collect();

        let new_language_agnostic_config = toml::to_string(&new_pokemon)?;
        sh.write_file(
            format!("pokemon/{language}.toml"),
            new_language_agnostic_config,
        )?;
    }

    Ok(())
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct TomlPokemon {
    pub height: f64,
    pub weight: f64,
    pub primary_type: Arc<str>,
    pub secondary_type: Option<Arc<str>>,
    pub catch_rate: u8,
    pub move_speed: u8,
    pub can_swim: bool,
    pub can_fly: bool,
    #[serde(flatten)]
    pub rest: HashMap<String, toml::Value>,
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct TomlI18nPokemon {
    name: String,
    description: String,
    #[serde(flatten)]
    pub rest: HashMap<String, toml::Value>,
}

fn update_pokemon(
    existing: Option<TomlPokemon>,
    (
        Pokemon {
            types,
            weight,
            height,
            stats,
            moves,
        },
        PokemonSpecies { capture_rate, .. },
    ): (Pokemon, PokemonSpecies),
) -> anyhow::Result<TomlPokemon> {
    let Some(primary_type_slot) = types.iter().find(|t| t.slot == 1) else {
        anyhow::bail!("Missing primary type");
    };
    let primary_type = primary_type_slot.poke_type.name.clone();

    let secondary_type = types
        .iter()
        .find(|t| t.slot == 2)
        .map(|t| t.poke_type.name.clone());

    let Some(speed_stat) = stats.iter().find(|s| s.stat.name == "speed") else {
        anyhow::bail!("Missing speed stat");
    };
    let move_speed = speed_stat.base_stat;

    let can_swim = moves.iter().any(|m| m.move_.name.as_ref() == "surf");
    let can_fly = moves.iter().any(|m| m.move_.name.as_ref() == "fly");

    Ok(TomlPokemon {
        weight,
        height,
        primary_type,
        secondary_type,
        catch_rate: capture_rate,
        move_speed,
        can_swim,
        can_fly,
        rest: existing.map(|ex| ex.rest).unwrap_or_default(),
    })
}

fn update_i18n_pokemon(
    pokemon_number: u16,
    lang: &str,
    existing: Option<TomlI18nPokemon>,
    (
        _,
        PokemonSpecies {
            names,
            flavor_text_entries,
            ..
        },
    ): (Pokemon, PokemonSpecies),
) -> anyhow::Result<TomlI18nPokemon> {
    let Some(name) = names
        .into_iter()
        .find(|name| name.language.name == lang)
        .map(|name| name.name)
    else {
        anyhow::bail!("Could not find name of pokemon {pokemon_number} in language {lang}");
    };
    let Some(description) = flavor_text_entries
        .into_iter()
        .find(|entry| entry.language.name == lang)
        .map(|entry| entry.flavor_text.replace("POKéMON", "Pokémon"))
        .map(|flavor_text| normalize_whitespace(&flavor_text))
    else {
        anyhow::bail!("Could not find description of pokemon {pokemon_number} in language {lang}");
    };

    Ok(TomlI18nPokemon {
        description,
        name,
        rest: existing.map(|ex| ex.rest).unwrap_or_default(),
    })
}

#[derive(Deserialize, Clone)]
struct Name {
    name: String,
    language: Language,
}

#[derive(Deserialize, Clone)]
struct FlavorTextEntry {
    flavor_text: String,
    language: Language,
}

#[derive(Deserialize, Clone)]
struct Language {
    name: String,
}

#[derive(Deserialize, Clone)]
struct Pokemon {
    weight: f64,
    height: f64,
    stats: Vec<Stat>,
    types: Vec<TypeSlot>,
    moves: Vec<MoveSlot>,
}

#[derive(Deserialize, Clone)]
struct Stat {
    base_stat: u8,
    stat: StatName,
}

#[derive(Deserialize, Clone)]
struct StatName {
    name: String,
}

#[derive(Deserialize, Clone)]
struct TypeSlot {
    slot: u8,
    #[serde(rename = "type")]
    poke_type: PokeType,
}

#[derive(Deserialize, Clone)]
struct PokeType {
    name: Arc<str>,
}

#[derive(Deserialize, Clone)]
struct MoveSlot {
    #[serde(rename = "move")]
    move_: Move,
}

#[derive(Deserialize, Clone)]
struct Move {
    name: Arc<str>,
}

#[derive(Deserialize, Clone)]
struct PokemonSpecies {
    capture_rate: u8,
    names: Vec<Name>,
    flavor_text_entries: Vec<FlavorTextEntry>,
}

fn normalize_whitespace(input: &str) -> String {
    let mut result = String::new();
    let mut is_whitespace = false;

    for c in input.chars() {
        if c.is_whitespace() {
            if !is_whitespace {
                result.push(' '); // Add a single space on transition to whitespace
                is_whitespace = true;
            }
        } else {
            result.push(c);
            is_whitespace = false;
        }
    }

    result.trim().to_string() // Trim leading/trailing spaces
}
