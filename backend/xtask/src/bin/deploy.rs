use std::{
    ffi::OsStr,
    fmt::Display,
    path::{Path, PathBuf},
};

use anyhow::{anyhow, Context};
use base64::Engine;
use xshell::{cmd, Shell};

pub mod proto {
    include!(concat!(env!("OUT_DIR"), "/deployment.rs"));
}

mod flags {
    xflags::xflags!(
      /// Deploy containers to a kubernetes cluster
      cmd deploy {
        /// Run generated commands without manual verification step
        optional -y,--yes
        /// How many backend instances should be deployed? Defaults to 1
        optional --backend-instances backend_instances: usize
        /// Send a deployment message with this version as a string
        optional --version version: String

        /// Deploy all containers
        default cmd all {}
        /// Deploy the database
        cmd db {}
        cmd backend {}
        cmd frontend {}
      }
    );
}

fn main() -> anyhow::Result<()> {
    dotenvy::dotenv()?;
    dotenvy::from_filename(".env.secret")?;
    let flags = flags::Deploy::from_env_or_exit();
    let sh = Shell::new()?;

    let backend_instances = flags.backend_instances.unwrap_or(1);

    let deploy_plan = match flags.subcommand {
        flags::DeployCmd::Db(_) => default_deploy(&sh, "postgres"),
        flags::DeployCmd::Backend(_) => deploy_backend(&sh, backend_instances),
        flags::DeployCmd::Frontend(_) => deploy_frontend(&sh, backend_instances),
        flags::DeployCmd::All(_) => Ok(default_deploy(&sh, "postgres")?
            .and(deploy_backend(&sh, backend_instances)?)
            .and(deploy_frontend(&sh, backend_instances)?)),
    }?;

    if !flags.yes {
        println!("The following commands will run: \n{}", deploy_plan);
        println!("Does this look ok? Y/N (default is N)");
        let input = xtask::read_line()?;
        if !input.trim().eq_ignore_ascii_case("Y") {
            println!("Aborted deployment. No changes were made.");
            return Ok(());
        }
    }

    deploy_plan.execute()?;

    // TODO add deployment message somewhere

    Ok(())
}

fn backend_name(index: usize) -> String {
    format!("backend-{index}")
}

fn deploy_backend(sh: &Shell, backend_instances: usize) -> anyhow::Result<DeployPlan> {
    let root_deploy_path = k8s_directory().join("backend");

    let instances_total_var = CustomVar {
        key: "INSTANCE_TOTAL",
        value: backend_instances.to_string(),
        file_suffix: None,
    };

    fn own_instances_index_var(index: usize) -> CustomVar {
        CustomVar {
            key: "INSTANCE_OWN_INDEX",
            value: index.to_string(),
            file_suffix: Some(format!("-{index}")),
        }
    }

    fn backend_name_var(index: usize) -> CustomVar {
        CustomVar {
            key: "BACKEND_NAME",
            value: backend_name(index),
            file_suffix: None,
        }
    }

    let custom_vars = (0..backend_instances)
        .map(|index| {
            [
                own_instances_index_var(index),
                backend_name_var(index),
                instances_total_var.clone(),
            ]
        })
        .collect::<Vec<_>>();

    let yml_files: Vec<PathBuf> = get_all_files_recursively(sh, root_deploy_path)?
        .into_iter()
        .filter(is_yml_file)
        .flat_map(|file_path| {
            custom_vars.iter().map({
                let sh = &sh;
                move |custom_vars| copy_to_plan_directory(sh, file_path.clone(), custom_vars)
            })
        })
        .collect::<anyhow::Result<_>>()?;

    Ok(DeployPlan::from_array([
        cmd!(sh, "kubectl delete services -l group=backend"),
        cmd!(sh, "kubectl delete deployments -l group=backend"),
        apply_files_to_cluster(sh, &yml_files),
    ]))
}

fn deploy_frontend(sh: &Shell, backend_instances: usize) -> anyhow::Result<DeployPlan> {
    let root_deploy_path = k8s_directory().join("frontend");

    let generate_servers_string = |port: u16| -> String {
        (0..backend_instances)
            .map(backend_name)
            .map(|server_name| format!("server {server_name}:{port};"))
            .collect::<Vec<_>>()
            .join("\n")
    };

    let custom_vars = [
        CustomVar {
            key: "HTTP_SERVERS",
            value: generate_servers_string(8080),
            file_suffix: None,
        },
        CustomVar {
            key: "GRPC_SERVERS",
            value: generate_servers_string(8081),
            file_suffix: None,
        },
    ];

    let yml_files: Vec<PathBuf> = get_all_files_recursively(sh, root_deploy_path)?
        .into_iter()
        .filter(is_yml_file)
        .map(|file_path| copy_to_plan_directory(sh, file_path, &custom_vars))
        .collect::<anyhow::Result<_>>()?;

    Ok(DeployPlan::from_array([apply_files_to_cluster(
        sh, &yml_files,
    )]))
}

fn default_deploy<P: AsRef<Path>>(sh: &Shell, sub_path: P) -> anyhow::Result<DeployPlan> {
    let root_deploy_path = k8s_directory().join(sub_path);
    let yml_files: Vec<PathBuf> = get_all_files_recursively(sh, root_deploy_path)?
        .into_iter()
        .filter(is_yml_file)
        .map(|file_path| copy_to_plan_directory(sh, file_path, Default::default()))
        .collect::<anyhow::Result<_>>()?;

    Ok(DeployPlan::from_array([apply_files_to_cluster(
        sh, &yml_files,
    )]))
}

struct DeployPlan<'a> {
    commands: Box<dyn Iterator<Item = xshell::Cmd<'a>> + 'a>,
    textual_repr: Vec<String>,
}

impl<'a> DeployPlan<'a> {
    fn from_array<const N: usize>(commands: [xshell::Cmd<'a>; N]) -> DeployPlan<'a> {
        let textual_repr = commands
            .iter()
            .map(|command| format!("{command}"))
            .collect::<Vec<String>>();
        Self {
            commands: Box::new(commands.into_iter()),
            textual_repr,
        }
    }

    fn and(mut self, mut other: DeployPlan<'a>) -> DeployPlan<'a> {
        self.textual_repr.append(&mut other.textual_repr);
        Self {
            commands: Box::new(self.commands.chain(other.commands)),
            textual_repr: self.textual_repr,
        }
    }

    fn execute(self) -> xshell::Result<()> {
        for command in self.commands {
            command.run()?;
        }
        Ok(())
    }
}

impl Display for DeployPlan<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.textual_repr.join("\n"))
    }
}

#[derive(Debug, Clone)]
struct CustomVar {
    key: &'static str,
    value: String,
    file_suffix: Option<String>,
}

impl CustomVar {
    fn append_to_file_name(&self, path: &mut PathBuf) {
        let file_name = path.file_stem().and_then(OsStr::to_str).map(String::from);
        if let (Some(suffix), Some(existing_file_name)) = (&self.file_suffix, file_name) {
            let new_file_name = existing_file_name
                + suffix.as_str()
                + "."
                + path.extension().and_then(OsStr::to_str).unwrap_or("");
            path.set_file_name(new_file_name);
        }
    }
}

fn apply_files_to_cluster<'a>(sh: &'a Shell, yml_files: &[PathBuf]) -> xshell::Cmd<'a> {
    let args = yml_files
        .iter()
        .filter_map(|file_path| file_path.to_str())
        .flat_map(|file_path| ["-f", file_path])
        .collect::<Vec<&str>>();
    cmd!(sh, "kubectl apply {args...}")
}

fn copy_to_plan_directory(
    sh: &Shell,
    absolute_k8s_path: PathBuf,
    custom_vars: &[CustomVar],
) -> anyhow::Result<PathBuf> {
    let relative_k8s_path = absolute_k8s_path
        .strip_prefix(k8s_directory())
        .expect("This function should only be called with subpaths of the k8s directory.");

    let plan_path = match strip_template_infix(relative_k8s_path) {
        Some(mut relative_plan_path) => {
            let template = sh.read_file(&absolute_k8s_path)?;
            for custom_var in custom_vars {
                sh.set_var(custom_var.key, &custom_var.value);
                custom_var.append_to_file_name(&mut relative_plan_path);
            }
            let filled_template = replace_placeholders(sh, &template).context(format!(
                "Failed replacing placeholders in {}",
                absolute_k8s_path.to_string_lossy()
            ))?;
            let absolute_plan_path = plan_directory().join(relative_plan_path);
            sh.write_file(&absolute_plan_path, filled_template)?;
            absolute_plan_path
        }
        None => {
            let absolute_plan_path = plan_directory().join(relative_k8s_path);
            if let Some(parent_dir) = absolute_plan_path.parent() {
                sh.create_dir(parent_dir)?;
            }
            sh.copy_file(&absolute_k8s_path, &absolute_plan_path)?;
            absolute_plan_path
        }
    };
    Ok(plan_path)
}

fn is_yml_file<P>(path: &P) -> bool
where
    P: AsRef<Path>,
{
    path.as_ref()
        .extension()
        .is_some_and(|ext| ext == "yml" || ext == "yaml")
}

fn strip_template_infix(path: &Path) -> Option<PathBuf> {
    let extension = path.extension()?.to_str()?;
    let file_stem = path.file_stem()?.to_str()?;
    let stripped_file_stem = file_stem.strip_suffix(".template")?;
    Some(path.with_file_name(format!("{stripped_file_stem}.{extension}")))
}

fn get_all_files_recursively(sh: &Shell, path: PathBuf) -> xshell::Result<Vec<PathBuf>> {
    let mut paths = Vec::new();
    if path.is_dir() {
        let top_level_paths = sh.read_dir(path)?;
        for path in top_level_paths {
            paths.append(&mut get_all_files_recursively(sh, path)?);
        }
    } else {
        paths.push(path);
    }
    Ok(paths)
}

fn replace_placeholders(sh: &Shell, str: &str) -> anyhow::Result<String> {
    let mut result = String::new();
    let mut chars = str.chars();
    let mut indentation = 0;
    while let Some(char) = chars.next() {
        if char == ' ' {
            indentation += 1;
        }
        if char == '\n' {
            indentation = 0;
        }
        if char == '$' {
            let next = chars.next();
            if let Some('{') = next {
                let mut placeholder = String::new();
                for placeholder_char in chars.by_ref() {
                    if placeholder_char == '}' {
                        break;
                    }
                    placeholder.push(placeholder_char);
                }

                let resolved_placeholder = resolve_placeholder(sh, indentation, &placeholder)?;
                result.push_str(&String::from_utf8(resolved_placeholder)?);
                continue;
            }
        }
        result.push(char);
    }
    Ok(result)
}

fn resolve_placeholder(
    sh: &Shell,
    indentation: usize,
    placeholder: &str,
) -> anyhow::Result<Vec<u8>> {
    if let Some(suffix) = placeholder.strip_prefix("env:") {
        let env_value = sh
            .var(suffix)
            .map_err(|err| anyhow!(err))
            .context(format!("Missing var: {suffix}"))?;
        return Ok(indent_all_except_first_line(
            indentation,
            env_value.into_bytes(),
        ));
    }
    if let Some(suffix) = placeholder.strip_prefix("file:") {
        let contents = sh.read_binary_file(xtask::project_directory().join(suffix))?;
        let indented_contents = indent_all_except_first_line(indentation, contents);
        return Ok(indented_contents);
    }
    if let Some(suffix) = placeholder.strip_prefix("base64:") {
        let contents = resolve_placeholder(sh, 0, suffix)?;
        return Ok(base64::prelude::BASE64_STANDARD
            .encode(contents)
            .into_bytes());
    }
    Ok(format!("${{{placeholder}}}").into_bytes())
}

fn k8s_directory() -> PathBuf {
    xtask::project_directory().join("k8s")
}

fn plan_directory() -> PathBuf {
    xtask::project_directory().join(".plan")
}

fn indent_all_except_first_line(spaces: usize, content: Vec<u8>) -> Vec<u8> {
    let str = match String::from_utf8(content) {
        Ok(str) => str,
        Err(err) => {
            return err.into_bytes();
        }
    };
    let mut lines = str.lines();
    match lines.next() {
        None => Vec::new(),
        Some(first_line) => std::iter::once(first_line.to_owned())
            .chain(lines.map(|line| " ".repeat(spaces) + line))
            .collect::<Vec<String>>()
            .join("\n")
            .into(),
    }
}
