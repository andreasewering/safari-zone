use std::path::{Path, PathBuf};

use anyhow::Context;
use xshell::{cmd, Shell};
use xtask::{query_container_status, ContainerStatus};

mod flags {
    xflags::xflags!(
      cmd app {

      /// Start the application server(s) and an nginx reverse proxy.
      /// This should be the main way to develop the backend.
      cmd start {
        /// Port to use for http requests.
        /// Grpc Requests use (port + 1).
        /// Defaults to 8080.
        /// If this flag is passed multiple times, multiple servers are started.
        /// This is very useful to test the load balancing behaviour locally.
        repeated -p,--port ports: u16
        /// Hard reset, i.e. remove running or stopped containers before starting
        optional -r,--reset
        /// Use the frontend dist folder instead of reverse proxying vite server
        optional -s,--static_frontend
        /// Run docker build and use the local image
        optional -l,--local
        /// Pass args to cargo
        optional -c,--cargo-args cargo_args: String
      }

      /// Subcommand for docker-related commands,
      /// such as building an optimized image, shipping it and running it locally.
      cmd docker {
        /// Build the application in production mode and package it in a docker container.
        cmd build {

        }
      }
      }
    );
}

fn main() -> anyhow::Result<()> {
    match flags::App::from_env_or_exit().subcommand {
        flags::AppCmd::Start(flags) => start(flags),
        flags::AppCmd::Docker(_) => Ok(()),
    }
}

fn start(mut flags: flags::Start) -> anyhow::Result<()> {
    dotenvy::dotenv()?;
    dotenvy::from_filename(concat!(env!("CARGO_MANIFEST_DIR"), "/../../.env.secret"))?;
    if flags.port.is_empty() {
        flags.port.push(8080);
    }

    let sh = Shell::new()?;
    sh.change_dir(env!("CARGO_MANIFEST_DIR"));

    let grpc_servers = flags
        .port
        .iter()
        .map(|port| format!("server host.docker.internal:{};", port + 1))
        .collect::<Vec<String>>()
        .join("\n");
    let http_servers = flags
        .port
        .iter()
        .map(|port| format!("server host.docker.internal:{};", port))
        .collect::<Vec<String>>()
        .join("\n");

    let status = query_container_status(&sh, NGINX_CONTAINER_NAME)?;
    let mut running = false;
    match status {
        ContainerStatus::Removed => {}
        ContainerStatus::Stopped => {
            if !flags.reset {
                println!("{NGINX_CONTAINER_NAME} is stopped and not removed. The container will be removed and recreated.");
            }
            cmd!(sh, "docker rm {NGINX_CONTAINER_NAME}").run()?;
        }
        ContainerStatus::Running => {
            if flags.reset {
                cmd!(sh, "docker rm -f {NGINX_CONTAINER_NAME}").run()?;
            } else {
                println!("{NGINX_CONTAINER_NAME} is already running. Reusing existing container.");
                running = true;
            }
        }
    }

    xtask::generate_certs_for_public_ips(&sh).context("Generating certs")?;

    let mut nginx_image_name =
        std::env::var("CI_BASE_IMAGE_NGINX").context("CI_BASE_IMAGE_NGINX not set")?;
    if flags.local {
        println!("Using locally built docker image");
        nginx_image_name = NGINX_IMAGE_NAME.to_owned();
        let nginx_docker_dir: PathBuf = [env!("CARGO_MANIFEST_DIR"), "..", "nginx"]
            .into_iter()
            .collect();
        cmd!(sh, "docker build -t {NGINX_IMAGE_NAME} {nginx_docker_dir}").run()?;
    } else {
        println!("Using last docker image from master branch");
    }
    let debug_headers = "add_header X-Upstream $upstream_addr;";

    if !running {
        let frontend_dir = xtask::project_directory().join("frontend");
        let command = if flags.static_frontend {
            let frontend_dist_dir: PathBuf =
                xtask::project_directory().join("frontend").join("dist");
            let frontend_dist_dir = frontend_dist_dir.to_string_lossy().to_string();
            println!("Serving built assets from {frontend_dist_dir}");

            cmd!(
            sh,
            "docker run -d -p 3000:80 -p 3443:443
                    -v {frontend_dir}/templates/server.template.conf:/etc/nginx/templates/server.conf.template
                    -v {frontend_dir}/templates/static_frontend.conf:/etc/nginx/conf.d/frontend.locconf 
                    -v {frontend_dir}/certs:/etc/nginx/ssl
                    -v {frontend_dist_dir}:/usr/share/nginx/
                    -e HTTP_SERVERS={http_servers}
                    -e GRPC_SERVERS={grpc_servers}
                    -e DEBUG_HEADERS={debug_headers}
                    -e SSL_CERT_NAME=local-cert
                     --name {NGINX_CONTAINER_NAME}
                     {nginx_image_name}"
        )
        } else {
            println!("Reverse proxying vite dev server.");
            cmd!(
        sh,
        "docker run -d -p 3000:80 -p 3443:443
                -v {frontend_dir}/templates/server.template.conf:/etc/nginx/templates/server.conf.template
                -v {frontend_dir}/templates/vite_proxy.conf:/etc/nginx/conf.d/frontend.locconf
                -v {frontend_dir}/certs:/etc/nginx/ssl
                -e HTTP_SERVERS={http_servers}
                -e GRPC_SERVERS={grpc_servers}
                -e DEBUG_HEADERS={debug_headers}
                -e SSL_CERT_NAME=local-cert
                 --name {NGINX_CONTAINER_NAME}
                 {nginx_image_name}"
    )
        };
        command.run()?;
    }

    let config_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
        .join("../server/config")
        .canonicalize()
        .context("Canonicalizing config dir path")?;

    let mut handles = Vec::new();
    for port in flags.port.into_iter() {
        let grpc_port = (port + 1).to_string();
        let http_port = port.to_string();
        let cargo_args = flags.cargo_args.clone().unwrap_or_default();
        let handle = {
            let mut child = std::process::Command::new(
                Path::new(env!("CARGO_MANIFEST_DIR")).join("src/sh/start.sh"),
            );
            child.args([
                config_dir.to_str().expect("Path was not valid UTF-8"),
                &http_port,
                &grpc_port,
                &cargo_args,
            ]);
            println!("{child:?}");
            child.spawn().expect("Failed to start child process")
        };
        handles.push(handle);
    }
    let process_ids: Vec<nix::unistd::Pid> = handles
        .iter()
        .map(|child| nix::unistd::Pid::from_raw(child.id() as i32))
        .collect();

    ctrlc::set_handler(move || {
        println!("Stopping backend servers. Nginx is left running.");
        for process_id in &process_ids {
            if let Err(error) = nix::sys::signal::kill(*process_id, nix::sys::signal::SIGINT) {
                println!("Failed to kill child process {process_id}: {error}");
            }
        }
    })
    .expect("Error setting Ctrl-C handler");

    for handle in handles {
        handle.wait_with_output()?;
    }
    Ok(())
}

const NGINX_CONTAINER_NAME: &str = "nginx_local";
const NGINX_IMAGE_NAME: &str = "nginx_local";
