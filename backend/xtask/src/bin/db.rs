use std::{fmt::Display, path::PathBuf};

use itertools::Itertools as _;
use serde::Deserialize;
use sqlx::Connection;
use xshell::{cmd, Shell};
use xtask::{ContainerStatus, DbConnectData, EnvIdsDir, ForeignKeyGraph};

mod flags {
    xflags::xflags!(
      cmd db {
      /// Setup your local postgres database. After this, the database
      /// should be ready for development
      cmd setup {
        /// Completely delete the current database if one exists and start from scratch.
        optional -r,--reset
        /// Setup the database on this port. Defaults to "5432".
        optional -p,--port port: u16
        /// Use this user to connect to the database. Defaults to "postgres".
        optional -u,--user user: String
        /// Use this password to connect to the database. Defaults to "postgres".
        optional -w,--password password: String
        /// Connect to this database host. Defaults to "localhost".
        optional -h,--host host: String
        /// Assume the database is already running
        optional --running
      }

      /// Import previously exported data into a running database
      cmd import {
        /// Database table to import data into.
        /// If not specified, all exported data gets imported.
        optional table_name: String
        /// Name of the data to import.
        /// If not specified, all data for the given table is imported.
        optional export_name: String
        /// Use this user to connect to the database. Defaults to "postgres".
        optional -u,--user user: String
        /// Use this password to connect to the database. Defaults to "postgres".
        optional -w,--password password: String
        /// Connect to this database host. Defaults to "localhost".
        optional -h,--host host: String
        /// Connect to this database port. Defaults to "5432".
        optional -p,--port port: u16
      }

      /// Export rows of a table into a csv file.
      /// This is useful to check them into version control,
      /// so you can easily run with the same test users etc. on
      /// different machines without running into migration issues.
      cmd export {
        /// Column to use for filtering data from database.
        /// Defaults to 'id'.
        optional -c,--column column_name: String
        /// Database table to export data from.
        required table_name: String
        /// Id (or value of the --column flags column) of the data to export
        required -i,--id id: String
        /// Name infix of the generated export csv. This should communicate why the data was exported - what is special about it.
        required export_name: String
        /// Use this user to connect to the database. Defaults to "postgres".
        optional -u,--user user: String
        /// Use this password to connect to the database. Defaults to "postgres".
        optional -w,--password password: String
        /// Connect to this database host. Defaults to "localhost".
        optional -h,--host host: String
        /// Connect to this database port. Defaults to "5432".
        optional -p,--port port: u16
      }
      }
    );
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let flags = flags::Db::from_env_or_exit();
    tracing_subscriber::fmt().init();
    let sh = Shell::new()?;
    match flags.subcommand {
        flags::DbCmd::Setup(flags) => setup(flags).await,
        flags::DbCmd::Export(flags) => {
            let (connect_data, export_, id, mut env_ids_dir) = fill_export_flag_defaults(&flags)?;
            export(&sh, connect_data, export_, id, &mut env_ids_dir)
        }
        flags::DbCmd::Import(flags) => {
            let (connect_data, export_dir, mut env_ids_dir) =
                fill_import_flag_defaults(&flags, false)?;
            import(
                &sh,
                connect_data,
                export_dir,
                &mut env_ids_dir,
                flags.table_name.as_deref(),
                flags.export_name.clone(),
            )
        }
    }?;
    Ok(())
}

async fn setup(flags: flags::Setup) -> anyhow::Result<()> {
    let sh = Shell::new()?;

    if !flags.running {
        let mut status = xtask::query_container_status(&sh, POSTGRES_CONTAINER_NAME)?;
        if flags.reset && status != ContainerStatus::Removed {
            cmd!(sh, "docker rm -f {POSTGRES_CONTAINER_NAME}").run()?;
            status = ContainerStatus::Removed;
        }
        match status {
            ContainerStatus::Running => {}
            ContainerStatus::Stopped => {
                cmd!(sh, "docker start {POSTGRES_CONTAINER_NAME}").run()?;
            }
            ContainerStatus::Removed => {
                let port = flags.port.unwrap_or(5432).to_string();
                cmd!(
                sh,
                "docker run -d -p {port}:5432 -e POSTGRES_PASSWORD=postgres --name {POSTGRES_CONTAINER_NAME} {POSTGRES_IMAGE} postgres -c 'xtea.key=12345678901234'"
            )
            .run()?;
            }
        }
    }
    let backend_dir: PathBuf = [env!("CARGO_MANIFEST_DIR"), "..", "server"]
        .into_iter()
        .collect();
    sh.change_dir(&backend_dir);

    let migrations = database::read_migrations(&backend_dir.join("migrations"))?;

    let import_flags = flags::Import {
        table_name: None,
        export_name: None,
        user: flags.user,
        password: flags.password,
        host: flags.host,
        port: flags.port,
    };
    let (db_connect_data, export_dir, mut env_ids_dir) =
        fill_import_flag_defaults(&import_flags, flags.reset)?;

    xtask::retry(|| db_connect_data.create_database(&sh), 200, 5)?;
    let mut conn = sqlx::PgConnection::connect(&db_connect_data.connection_string()).await?;
    database::execute_migrations(&mut conn, migrations.clone()).await?;

    import(
        &sh,
        db_connect_data,
        export_dir,
        &mut env_ids_dir,
        None,
        None,
    )?;

    Ok(())
}

fn default_export_dir() -> PathBuf {
    [env!("CARGO_MANIFEST_DIR"), "exports"]
        .into_iter()
        .collect()
}

fn default_env_ids_dir(db_host: &str) -> PathBuf {
    [env!("CARGO_MANIFEST_DIR"), "env_ids", db_host]
        .into_iter()
        .collect()
}

fn static_env_ids_dir() -> PathBuf {
    [env!("CARGO_MANIFEST_DIR"), "env_ids", "_static"]
        .into_iter()
        .collect()
}

fn fill_export_flag_defaults(
    flags: &flags::Export,
) -> anyhow::Result<(DbConnectData<'_>, Export<ExportDir>, &str, EnvIdsDir)> {
    let db_connect_data = DbConnectData {
        host: flags.host.as_deref().unwrap_or("localhost"),
        port: flags.port.unwrap_or(5432),
        username: flags.user.as_deref().unwrap_or("postgres"),
        password: flags.password.as_deref().unwrap_or("postgres"),
        database_name: DATABASE_NAME,
    };

    let table_name = &flags.table_name;
    let id = &flags.id;
    let export_name = &flags.export_name;
    let column_name = flags.column.as_deref().unwrap_or("id");

    let export_dir = ExportDir::new(default_export_dir())?;
    let export_table = ExportTable::new(table_name).set_export_dir(&export_dir);
    let export = export_table.with_column_and_name(column_name, export_name);

    let env_ids_dir = EnvIdsDir::new(
        default_env_ids_dir(db_connect_data.host),
        static_env_ids_dir(),
    )?;

    Ok((db_connect_data, export, id, env_ids_dir))
}

fn fill_import_flag_defaults(
    flags: &flags::Import,
    reset: bool,
) -> anyhow::Result<(DbConnectData, ExportDir, EnvIdsDir)> {
    let db_connect_data = DbConnectData {
        host: flags.host.as_deref().unwrap_or("localhost"),
        port: flags.port.unwrap_or(5432),
        username: flags.user.as_deref().unwrap_or("postgres"),
        password: flags.password.as_deref().unwrap_or("postgres"),
        database_name: DATABASE_NAME,
    };
    if reset {
        std::fs::remove_dir_all(default_env_ids_dir(db_connect_data.host))?;
    }
    let export_dir = ExportDir::new(default_export_dir())?;
    let env_ids_dir = EnvIdsDir::new(
        default_env_ids_dir(db_connect_data.host),
        static_env_ids_dir(),
    )?;

    Ok((db_connect_data, export_dir, env_ids_dir))
}

fn export(
    sh: &Shell,
    db_connect_data: DbConnectData,
    export: Export<ExportDir>,
    id: &str,
    env_ids_dir: &mut EnvIdsDir,
) -> anyhow::Result<()> {
    let Export {
        table_name,
        column_name,
        export_dir: _,
        export_name,
    } = &export;

    if let Some(export_dir) = export.absolute_export_path().parent() {
        sh.create_dir(export_dir)?;
    }

    let export_file_path = export.absolute_export_path();
    let export_file_path = export_file_path.to_str().unwrap();

    let foreign_key_graph = xtask::get_foreign_keys_from_db(&db_connect_data, sh)?;

    let command = format!(
        "\\copy (select * from {table_name} where {column_name} = {id}) TO STDOUT CSV HEADER",
    );
    let output = db_connect_data.psql_command(sh, &command).output()?;
    let mut reader = csv::Reader::from_reader(output.stdout.as_slice());
    let headers = reader.headers()?.clone();

    let id_ix = headers
        .iter()
        .enumerate()
        .find(|(_, header)| *header == "id")
        .map(|(index, _)| index);

    let fkey_table_references = headers
        .iter()
        .enumerate()
        .filter_map(|(ix, header)| {
            let (referenced_table, _) =
                foreign_key_graph.is_foreign_key(&export.table_name, header)?;
            Some((ix, referenced_table))
        })
        .collect::<Vec<(usize, &str)>>();

    let mut writer = csv::Writer::from_path(export_file_path)?;

    writer.write_record(&headers)?;
    reader
        .into_deserialize::<Vec<String>>()
        .map(|res| res.unwrap())
        .enumerate()
        .try_for_each(|(row_ix, mut values)| -> anyhow::Result<()> {
            for (column_ix, referenced_table) in fkey_table_references.iter() {
                env_ids_dir
                    .id_to_alias(referenced_table, &values[*column_ix])
                    .ok_or(DbError::CouldNotResolveForeignKey {
                        from_table_name: table_name.clone(),
                        to_table_name: referenced_table.to_string(),
                        attempted_id_or_alias: values[*column_ix].clone(),
                    })?
                    .clone_into(&mut values[*column_ix]);
            }
            let export_name_with_index = format!("{export_name}_{row_ix}");

            if let Some(id_ix) = id_ix {
                env_ids_dir.add_mapping(table_name, &values[id_ix], &export_name_with_index);
                values[id_ix] = format!("{export_name}_{row_ix}");
            }
            writer.serialize(values)?;
            Ok(())
        })?;

    println!(
        "
Exported data of {table_name} with {column_name} = {id} to
{export_file_path}"
    );
    Ok(())
}

fn import(
    sh: &Shell,
    db_connect_data: DbConnectData,
    export_dir: ExportDir,
    env_ids_dir: &mut EnvIdsDir,
    table_name: Option<&str>,
    export_name: Option<String>,
) -> anyhow::Result<()> {
    let foreign_key_graph = xtask::get_foreign_keys_from_db(&db_connect_data, sh)?;

    let tables = table_name
        .map(|table_name| vec![ExportTable::new(table_name).set_export_dir(&export_dir)])
        .unwrap_or_else(|| export_dir.tables(sh));

    type ExportFilter = Box<dyn Fn(&Export<ExportDir>) -> bool>;

    let export_filter: ExportFilter = match export_name {
        Some(export_name) => Box::new(move |export| export.export_name == export_name),
        None => Box::new(|_| true),
    };
    let mut exports: Vec<Export<ExportDir>> = tables
        .into_iter()
        .flat_map(|table| table.exports(sh))
        .filter(export_filter)
        .collect();

    foreign_key_graph.sort_by_table(&mut exports, |export| export.table_name.clone());
    for export in exports {
        import_single(sh, export, db_connect_data, &foreign_key_graph, env_ids_dir)?;
    }

    Ok(())
}

fn import_single(
    sh: &Shell,
    export: Export<ExportDir>,
    db_connect_data: DbConnectData,
    foreign_key_graph: &ForeignKeyGraph,
    env_ids_dir: &mut EnvIdsDir,
) -> anyhow::Result<()> {
    let table_name = &export.table_name;
    let column_name = &export.column_name;
    let export_name = &export.export_name;

    let previous_id = env_ids_dir.alias_to_id(table_name, &format!("{export_name}_0"));

    if let Some(id) = &previous_id {
        let command = format!("DELETE FROM {table_name} WHERE {column_name} = {id}");
        db_connect_data.psql_command(sh, &command).run()?;
    }

    let export_file_path = export.absolute_export_path();
    let export_file_path = export_file_path.to_str().unwrap();

    let mut reader = csv::Reader::from_path(export_file_path)?;
    let mut headers = reader.headers()?.deserialize::<Vec<String>>(None)?;

    let mut output_csv = Vec::new();
    let mut writer = csv::Writer::from_writer(&mut output_csv);

    let fkey_table_references = headers
        .iter()
        .enumerate()
        .filter_map(|(ix, header)| {
            let (referenced_table, _) = foreign_key_graph.is_foreign_key(table_name, header)?;
            Some((ix, referenced_table.to_owned()))
        })
        .collect::<Vec<(usize, String)>>();

    let opt_id_column_ix = headers
        .iter()
        .enumerate()
        .find(|(_, header)| *header == "id")
        .map(|(id_column_ix, _)| id_column_ix);

    let reader_iterator = reader
        .into_deserialize::<Vec<String>>()
        .map(|result| result.unwrap());

    if previous_id.is_none() {
        match opt_id_column_ix {
            Some(id_column_ix) => {
                headers.remove(id_column_ix);

                let column_names = headers.join(",");

                const CHUNK_SIZE: usize = 10;

                for (chunk_index, chunk) in
                    reader_iterator.chunks(CHUNK_SIZE).into_iter().enumerate()
                {
                    let values_query_part = chunk
                        .map(|mut values| -> anyhow::Result<String> {
                            for (column_ix, referenced_table) in fkey_table_references.iter() {
                                let alias = &values[*column_ix];
                                env_ids_dir
                                    .alias_to_id(referenced_table, alias)
                                    .ok_or(DbError::CouldNotResolveForeignKey {
                                        from_table_name: table_name.clone(),
                                        to_table_name: referenced_table.to_owned(),
                                        attempted_id_or_alias: alias.to_owned(),
                                    })?
                                    .clone_into(&mut values[*column_ix]);
                            }
                            values.remove(id_column_ix);

                            let joined_column_values = values
                                .into_iter()
                                .map(|value| {
                                    if value.is_empty() {
                                        "null".to_owned()
                                    } else {
                                        format!("'{value}'")
                                    }
                                })
                                .collect::<Vec<_>>()
                                .join(",");
                            Ok(joined_column_values)
                        })
                        .collect::<anyhow::Result<Vec<String>>>()?
                        .join("),(");

                    let query = format!("INSERT INTO {table_name} ({column_names}) VALUES ({values_query_part}) RETURNING id");

                    #[derive(Deserialize)]
                    struct Id {
                        id: String,
                    }

                    let ids = db_connect_data.psql_query::<Id>(sh, &query)?;
                    for (row_ix, id) in ids.into_iter().enumerate() {
                        let absolute_row_ix = (CHUNK_SIZE * chunk_index) + row_ix;
                        let alias = format!("{export_name}_{absolute_row_ix}");
                        env_ids_dir.add_mapping(table_name, &id.id, &alias);
                    }
                }
            }
            None => {
                writer.serialize(headers)?;

                reader_iterator
                    .map(|mut values| -> anyhow::Result<_> {
                        for (column_ix, referenced_table) in fkey_table_references.iter() {
                            let alias = &values[*column_ix];
                            env_ids_dir
                                .alias_to_id(referenced_table, alias)
                                .ok_or(DbError::CouldNotResolveForeignKey {
                                    from_table_name: table_name.clone(),
                                    to_table_name: referenced_table.to_owned(),
                                    attempted_id_or_alias: alias.to_owned(),
                                })?
                                .clone_into(&mut values[*column_ix]);
                        }
                        Ok(values)
                    })
                    .try_for_each(|values| -> anyhow::Result<()> {
                        writer.serialize(values?)?;
                        Ok(())
                    })?;

                drop(writer);

                let command = format!("\\copy {table_name} FROM STDIN csv header");
                db_connect_data
                    .psql_command(sh, &command)
                    .stdin(output_csv)
                    .run()?;
            }
        }

        return Ok(());
    }

    writer.serialize(headers)?;
    let id_column_ix = opt_id_column_ix.expect("Opt id column ix should never be none here.");

    reader_iterator
        .map(|mut values| -> anyhow::Result<_> {
            for (column_ix, referenced_table) in fkey_table_references.iter() {
                let alias = &values[*column_ix];
                env_ids_dir
                    .alias_to_id(referenced_table, alias)
                    .ok_or(DbError::CouldNotResolveForeignKey {
                        from_table_name: table_name.clone(),
                        to_table_name: referenced_table.to_owned(),
                        attempted_id_or_alias: alias.to_owned(),
                    })?
                    .clone_into(&mut values[*column_ix]);
            }
            env_ids_dir
                .alias_to_id(table_name, &values[id_column_ix])
                .unwrap_or_else(|| {
                    panic!(
                        "Could not resolve id, table_name: {}, alias: {}",
                        table_name, values[id_column_ix]
                    )
                })
                .clone_into(&mut values[id_column_ix]);
            Ok(values)
        })
        .try_for_each(|values| -> anyhow::Result<()> {
            writer.serialize(values?)?;
            Ok(())
        })?;

    drop(writer);

    let command = format!("\\copy {table_name} FROM STDIN csv header");
    db_connect_data
        .psql_command(sh, &command)
        .stdin(output_csv)
        .run()?;

    Ok(())
}

const POSTGRES_CONTAINER_NAME: &str = "postgres_local";
const DATABASE_NAME: &str = "safarizone";
const POSTGRES_IMAGE: &str = "registry.gitlab.com/andreasewering/safari-zone/postgres";

#[derive(Debug, PartialEq, Eq)]
enum DbError {
    CouldNotResolveForeignKey {
        from_table_name: String,
        to_table_name: String,
        attempted_id_or_alias: String,
    },
}

impl Display for DbError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DbError::CouldNotResolveForeignKey {
                from_table_name,
                to_table_name,
                attempted_id_or_alias,
            } => f.write_fmt(format_args!(
                "Could not resolve foreign key reference to table '{to_table_name}'
                from table '{from_table_name}'. Attempted to resolve '{attempted_id_or_alias}'"
            )),
        }
    }
}

impl std::error::Error for DbError {}

#[derive(Debug, Clone)]
pub struct ExportDir(PathBuf);

impl ExportDir {
    pub fn new(dir: PathBuf) -> anyhow::Result<Self> {
        std::fs::create_dir_all(&dir)?;
        Ok(Self(dir))
    }

    pub fn tables(&self, sh: &Shell) -> Vec<ExportTable<ExportDir>> {
        sh.read_dir(&self.0)
            .unwrap_or_default()
            .into_iter()
            .filter(|path| path.is_dir())
            .filter_map(|path| {
                path.file_name()
                    .and_then(|table_name| table_name.to_str())
                    .map(|table_name| ExportTable {
                        table_name: table_name.to_owned(),
                        export_dir: self.clone(),
                    })
            })
            .collect()
    }
}

pub struct ExportTable<T: Clone = ()> {
    pub table_name: String,
    pub export_dir: T,
}

impl ExportTable {
    pub fn new(table_name: &str) -> Self {
        ExportTable {
            table_name: table_name.to_owned(),
            export_dir: (),
        }
    }

    pub fn set_export_dir(self, export_dir: &ExportDir) -> ExportTable<ExportDir> {
        ExportTable {
            table_name: self.table_name,
            export_dir: export_dir.clone(),
        }
    }
}

impl<T: Clone> ExportTable<T> {
    pub fn relative_export_dir(&self) -> PathBuf {
        PathBuf::from(&self.table_name)
    }

    pub fn with_column_and_name(&self, column_name: &str, export_name: &str) -> Export<T> {
        let table_name = self.table_name.clone();
        Export {
            table_name,
            column_name: column_name.to_owned(),
            export_dir: self.export_dir.clone(),
            export_name: export_name.to_owned(),
        }
    }
}

impl ExportTable<ExportDir> {
    pub fn absolute_export_dir(&self) -> PathBuf {
        self.export_dir.0.join(self.relative_export_dir())
    }

    pub fn exports(&self, sh: &Shell) -> Vec<Export<ExportDir>> {
        sh.read_dir(self.absolute_export_dir())
            .unwrap_or_default()
            .into_iter()
            .filter(|path| path.is_file())
            .filter_map(|path| {
                let stem = path.file_stem()?.to_str()?;
                let [column_name, export_name]: [&str; 2] =
                    stem.split('.').collect::<Vec<&str>>().try_into().ok()?;

                Some(Export {
                    table_name: self.table_name.clone(),
                    column_name: column_name.to_owned(),
                    export_name: export_name.to_owned(),
                    export_dir: self.export_dir.clone(),
                })
            })
            .collect()
    }
}

#[derive(Debug, Clone)]
pub struct Export<T: Clone = ()> {
    pub table_name: String,
    pub column_name: String,
    pub export_dir: T,
    pub export_name: String,
}

impl Export {
    pub fn set_export_dir(self, export_dir: &ExportDir) -> Export<ExportDir> {
        Export {
            table_name: self.table_name,
            column_name: self.column_name,
            export_dir: export_dir.clone(),
            export_name: self.export_name,
        }
    }
}

impl<T: Clone> Export<T> {
    pub fn relative_export_path(&self) -> PathBuf {
        let column_name = &self.column_name;
        let export_name = &self.export_name;
        let file_name = format!("{column_name}.{export_name}.csv");
        [&self.table_name, &file_name].into_iter().collect()
    }
}

impl Export<ExportDir> {
    pub fn absolute_export_path(&self) -> PathBuf {
        self.export_dir.0.join(self.relative_export_path())
    }
}

#[cfg(test)]
mod tests {
    use serde::{de::DeserializeOwned, Deserialize};

    use super::*;

    #[test]
    fn export_simple_table_single_row_and_reimport_does_nothing() -> anyhow::Result<()> {
        let mut test_id = TestId::new("simple_table_test");

        let connect_data = default_connect_data();
        let sh = Shell::new()?;
        let db_name = test_id.next_db_name();
        let db_guard = create_database(&sh, connect_data, &db_name)?;

        db_guard.run("CREATE TABLE test (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")?;
        db_guard.run("INSERT INTO test (name) VALUES ('a'), ('b'), ('c')")?;

        let export_dir = test_id.export_dir()?;
        let mut env_ids_dir = test_id.env_ids_dir()?;
        let the_export = ExportTable::new("test")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "exp");

        export(
            &sh,
            db_guard.connect_data,
            the_export,
            "2",
            &mut env_ids_dir,
        )?;
        import(
            &sh,
            db_guard.connect_data,
            export_dir,
            &mut env_ids_dir,
            None,
            None,
        )?;

        let count = db_guard.get::<Count>("SELECT COUNT(*) as count FROM test")?;
        assert_eq!(count, vec![Count { count: 3 }]);

        Ok(())
    }

    #[test]
    fn export_simple_table_single_row_reset_and_reimport() -> anyhow::Result<()> {
        let mut test_id = TestId::new("simple_table_test_reset");

        let connect_data = default_connect_data();
        let sh = Shell::new()?;
        let db_name = test_id.next_db_name();
        let db_guard = create_database(&sh, connect_data, &db_name)?;

        db_guard.run("CREATE TABLE test (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")?;
        db_guard.run("INSERT INTO test (name) VALUES ('a'), ('b'), ('c')")?;

        let export_dir = test_id.export_dir()?;
        let mut env_ids_dir = test_id.env_ids_dir()?;
        let the_export = ExportTable::new("test")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "exp");

        export(
            &sh,
            db_guard.connect_data,
            the_export,
            "2",
            &mut env_ids_dir,
        )?;

        // cleanup
        drop(db_guard);
        drop(env_ids_dir);
        test_id.reset_env_ids()?;

        // init new database with the same schema
        let db_name = test_id.next_db_name();
        let db_guard = create_database(&sh, connect_data, &db_name)?;
        db_guard.run("CREATE TABLE test (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")?;
        let mut env_ids_dir = test_id.env_ids_dir()?;

        import(
            &sh,
            db_guard.connect_data,
            export_dir,
            &mut env_ids_dir,
            None,
            None,
        )?;

        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct Simple {
            id: i32,
            name: String,
        }

        let rows = db_guard.get::<Simple>("SELECT id, name FROM test")?;

        assert_eq!(
            rows,
            vec![Simple {
                id: 1,
                name: "b".to_owned()
            }]
        );
        Ok(())
    }

    #[test]
    fn export_table_with_foreign_key_single_row_and_reimport() -> anyhow::Result<()> {
        let mut test_id = TestId::new("fkey_table_test");

        let connect_data = default_connect_data();
        let sh = Shell::new()?;
        let db_name = test_id.next_db_name();
        let db_guard = create_database(&sh, connect_data, &db_name)?;

        db_guard.run("CREATE TABLE ref (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")?;
        db_guard.run("CREATE TABLE test (id SERIAL PRIMARY KEY, fkey INT NOT NULL, FOREIGN KEY (fkey) REFERENCES ref(id) ON DELETE CASCADE)")?;
        db_guard.run("INSERT INTO ref (name) VALUES ('a'), ('b'), ('c')")?;
        db_guard.run("INSERT INTO test (fkey) VALUES (1), (3)")?;

        let mut env_ids_dir = test_id.env_ids_dir()?;
        let export_dir = test_id.export_dir()?;
        let test_export = ExportTable::new("test")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "exp");

        let export_result = export(
            &sh,
            db_guard.connect_data,
            test_export,
            "2",
            &mut env_ids_dir,
        );
        assert_eq!(
            export_result
                .expect_err(
                    "Should have failed because referenced tables need to be exported first"
                )
                .downcast_ref::<DbError>()
                .unwrap(),
            &DbError::CouldNotResolveForeignKey {
                from_table_name: "test".to_owned(),
                to_table_name: "ref".to_owned(),
                attempted_id_or_alias: "3".to_owned()
            }
        );

        let ref_export = ExportTable::new("ref")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "refexp");
        export(
            &sh,
            db_guard.connect_data,
            ref_export,
            "3",
            &mut env_ids_dir,
        )?;

        let new_test_export = ExportTable::new("test")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "exp");

        export(
            &sh,
            db_guard.connect_data,
            new_test_export,
            "2",
            &mut env_ids_dir,
        )?;

        import(
            &sh,
            db_guard.connect_data,
            export_dir,
            &mut env_ids_dir,
            None,
            None,
        )?;

        let ref_count = db_guard.get::<Count>("SELECT COUNT(*) as count FROM ref")?;
        let test_count = db_guard.get::<Count>("SELECT COUNT(*) as count FROM test")?;
        assert_eq!(ref_count, vec![Count { count: 3 }]);
        assert_eq!(test_count, vec![Count { count: 2 }]);

        Ok(())
    }

    #[test]
    fn export_table_with_foreign_key_single_row_reset_and_reimport() -> anyhow::Result<()> {
        let mut test_id = TestId::new("fkey_table_test_reset");

        let connect_data = default_connect_data();
        let sh = Shell::new()?;
        let db_name = test_id.next_db_name();
        let db_guard = create_database(&sh, connect_data, &db_name)?;

        db_guard.run("CREATE TABLE ref (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")?;
        db_guard.run("CREATE TABLE test (id SERIAL PRIMARY KEY, fkey INT NOT NULL, FOREIGN KEY (fkey) REFERENCES ref(id) ON DELETE CASCADE)")?;
        db_guard.run("INSERT INTO ref (name) VALUES ('a'), ('b'), ('c')")?;
        db_guard.run("INSERT INTO test (fkey) VALUES (1), (3)")?;

        let mut env_ids_dir = test_id.env_ids_dir()?;
        let export_dir = test_id.export_dir()?;

        let ref_export = ExportTable::new("ref")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "refexp");
        export(
            &sh,
            db_guard.connect_data,
            ref_export,
            "3",
            &mut env_ids_dir,
        )?;

        let test_export = ExportTable::new("test")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "exp");

        export(
            &sh,
            db_guard.connect_data,
            test_export,
            "2",
            &mut env_ids_dir,
        )?;

        // cleanup
        drop(db_guard);
        drop(env_ids_dir);
        test_id.reset_env_ids()?;

        // init new database with the same schema
        let db_name = test_id.next_db_name();
        let db_guard = create_database(&sh, connect_data, &db_name)?;
        db_guard.run("CREATE TABLE ref (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")?;
        db_guard.run("CREATE TABLE test (id SERIAL PRIMARY KEY, fkey INT NOT NULL, FOREIGN KEY (fkey) REFERENCES ref(id) ON DELETE CASCADE)")?;

        let mut env_ids_dir = test_id.env_ids_dir()?;

        import(
            &sh,
            db_guard.connect_data,
            export_dir,
            &mut env_ids_dir,
            None,
            None,
        )?;

        let ref_count = db_guard.get::<Count>("SELECT COUNT(*) as count FROM ref")?;
        let test_count = db_guard.get::<Count>("SELECT COUNT(*) as count FROM test")?;
        assert_eq!(ref_count, vec![Count { count: 1 }]);
        assert_eq!(test_count, vec![Count { count: 1 }]);

        Ok(())
    }

    #[test]
    fn export_table_based_off_foreign_key_and_reimport() -> anyhow::Result<()> {
        let mut test_id = TestId::new("foreign_key_based");

        let connect_data = default_connect_data();
        let sh = Shell::new()?;
        let db_name = test_id.next_db_name();
        let db_guard = create_database(&sh, connect_data, &db_name)?;

        db_guard.run("CREATE TABLE ref (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")?;
        db_guard.run("CREATE TABLE test (id SERIAL PRIMARY KEY, fkey INT NOT NULL, FOREIGN KEY (fkey) REFERENCES ref(id) ON DELETE CASCADE)")?;
        db_guard.run("INSERT INTO ref (name) VALUES ('a'), ('b'), ('c')")?;
        db_guard.run("INSERT INTO test (fkey) VALUES (1), (1)")?;

        let mut env_ids_dir = test_id.env_ids_dir()?;
        let export_dir = test_id.export_dir()?;

        let ref_export = ExportTable::new("ref")
            .set_export_dir(&export_dir)
            .with_column_and_name("id", "refexp");
        export(
            &sh,
            db_guard.connect_data,
            ref_export,
            "1",
            &mut env_ids_dir,
        )?;

        let test_export = ExportTable::new("test")
            .set_export_dir(&export_dir)
            .with_column_and_name("fkey", "exp");

        export(
            &sh,
            db_guard.connect_data,
            test_export,
            "1",
            &mut env_ids_dir,
        )?;

        import(
            &sh,
            db_guard.connect_data,
            export_dir,
            &mut env_ids_dir,
            None,
            None,
        )?;

        Ok(())
    }

    #[derive(Debug, Deserialize, PartialEq, Eq)]
    struct Count {
        count: i32,
    }

    fn default_connect_data() -> DbConnectData<'static> {
        dotenvy::dotenv().expect(".env not found");
        let conn_str = std::env::var("DATABASE_URL").expect("DATABASE_URL env var to be set");
        let leaked = conn_str.leak();
        DbConnectData::parse(leaked)
            .unwrap_or_else(|err| panic!("Invalid database url {leaked}: {err}"))
    }

    struct TestId {
        name: &'static str,
        db_count: u8,
    }

    impl TestId {
        fn new(name: &'static str) -> Self {
            let test_id = Self { name, db_count: 0 };
            test_id.reset_env_ids().ok();
            std::fs::remove_dir_all(test_id.export_dir_path()).ok();
            test_id
        }
        fn next_db_name(&mut self) -> String {
            let name = format!("{}_{}", self.name, self.db_count);
            self.db_count += 1;
            name
        }

        fn env_ids_dir(&self) -> anyhow::Result<EnvIdsDir> {
            EnvIdsDir::new(self.env_ids_dir_path(), self.static_env_ids_dir_path())
        }

        fn export_dir(&self) -> anyhow::Result<ExportDir> {
            ExportDir::new(self.export_dir_path())
        }

        fn reset_env_ids(&self) -> std::io::Result<()> {
            std::fs::remove_dir_all(self.env_ids_dir_path())?;
            std::fs::remove_dir_all(self.static_env_ids_dir_path())
        }

        fn env_ids_dir_path(&self) -> PathBuf {
            [
                env!("CARGO_MANIFEST_DIR"),
                "test_data",
                self.name,
                "env_ids",
            ]
            .into_iter()
            .collect()
        }

        fn static_env_ids_dir_path(&self) -> PathBuf {
            [
                env!("CARGO_MANIFEST_DIR"),
                "test_data",
                self.name,
                "_static_env_ids",
            ]
            .into_iter()
            .collect()
        }

        fn export_dir_path(&self) -> PathBuf {
            [
                env!("CARGO_MANIFEST_DIR"),
                "test_data",
                self.name,
                "exports",
            ]
            .into_iter()
            .collect()
        }
    }

    struct DbGuard<'a> {
        connect_data: DbConnectData<'a>,
        sh: &'a Shell,
    }

    fn create_database<'a>(
        sh: &'a Shell,
        connect_data: DbConnectData<'a>,
        name: &'a str,
    ) -> anyhow::Result<DbGuard<'a>> {
        connect_data
            .psql_command(sh, &format!("CREATE DATABASE {name}"))
            .run()?;
        let connect_data = DbConnectData {
            database_name: name,
            ..connect_data
        };
        Ok(DbGuard { connect_data, sh })
    }

    impl DbGuard<'_> {
        fn run(&self, command: &str) -> anyhow::Result<()> {
            self.connect_data.psql_command(self.sh, command).run()?;
            Ok(())
        }

        fn get<T: DeserializeOwned>(&self, query: &str) -> anyhow::Result<Vec<T>> {
            self.connect_data.psql_query(self.sh, query)
        }
    }

    impl Drop for DbGuard<'_> {
        fn drop(&mut self) {
            let name = self.connect_data.database_name;
            let connect_data = DbConnectData {
                database_name: "postgres",
                ..self.connect_data
            };
            if let Err(error) = connect_data
                .psql_command(self.sh, &format!("DROP DATABASE {name}"))
                .run()
            {
                panic!("Failed to drop database {name}: {error}");
            }
        }
    }
}
