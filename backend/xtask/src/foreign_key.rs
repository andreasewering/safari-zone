use std::collections::{HashMap, HashSet};

use serde::Deserialize;
use xshell::Shell;

use crate::db_connect_data::DbConnectData;

#[derive(Debug, Deserialize)]
struct ForeignKey {
    table: String,
    column: String,
    referenced_table: String,
    referenced_column: String,
}

#[derive(Debug)]
pub struct ForeignKeyGraph {
    nodes: HashMap<String, HashSet<String>>,
    edges: HashMap<(String, String), (String, String)>,
}

pub fn get_foreign_keys_from_db(
    db_connect_data: &DbConnectData,
    sh: &Shell,
) -> anyhow::Result<ForeignKeyGraph> {
    let foreign_keys = db_connect_data.psql_query::<ForeignKey>(
          sh,
          r#"
      SELECT conrelid::regclass as table, a.attname as column, confrelid::regclass as referenced_table, af.attname as referenced_column
      FROM pg_constraint c JOIN pg_attribute a ON a.attnum = ANY(c.conkey) AND a.attrelid = c.conrelid
      JOIN pg_attribute af ON af.attnum = ANY(c.confkey) AND af.attrelid = c.confrelid WHERE contype = 'f'  
      "#,
      )?;
    Ok(ForeignKeyGraph::from_foreign_keys(foreign_keys))
}

impl ForeignKeyGraph {
    fn new() -> Self {
        ForeignKeyGraph {
            nodes: HashMap::new(),
            edges: HashMap::new(),
        }
    }

    fn from_foreign_keys(keys: Vec<ForeignKey>) -> ForeignKeyGraph {
        let mut graph = Self::new();
        for key in keys {
            graph.add_foreign_key(key);
        }
        graph
    }

    fn add_foreign_key(&mut self, key: ForeignKey) {
        self.nodes
            .entry(key.table.clone())
            .or_default()
            .insert(key.column.clone());
        self.edges.insert(
            (key.table, key.column),
            (key.referenced_table.clone(), key.referenced_column),
        );
        self.nodes.entry(key.referenced_table).or_default();
    }

    pub fn sort_by_table<T, F: Fn(&T) -> String>(&self, ts: &mut [T], to_table: F) {
        let sorted_table_names = self.topological_sort();
        let table_order: HashMap<String, usize> = sorted_table_names
            .into_iter()
            .enumerate()
            .map(|(i, t)| (t, i))
            .collect();
        ts.sort_by(|t1, t2| {
            table_order
                .get(&to_table(t1))
                .cmp(&table_order.get(&to_table(t2)))
        });
    }

    pub fn is_foreign_key(&self, table: &str, column_name: &str) -> Option<(&str, &str)> {
        self.edges
            .get(&(table.to_owned(), column_name.to_owned()))
            .map(|(ref_table, ref_column)| (ref_table.as_ref(), ref_column.as_ref()))
    }

    fn topological_sort(&self) -> Vec<String> {
        let mut visited = HashSet::with_capacity(self.nodes.len());
        let mut stack = Vec::new();

        for (table_name, column_names) in &self.nodes {
            if !visited.contains(table_name.as_str()) {
                self.dfs(table_name.as_str(), column_names, &mut visited, &mut stack);
            }
        }

        stack
    }

    fn dfs(
        &self,
        table_name: &str,
        column_names: &HashSet<String>,
        visited: &mut HashSet<String>,
        stack: &mut Vec<String>,
    ) {
        visited.insert(table_name.to_string());
        let neighbours = column_names
            .iter()
            .filter_map(|column_name| {
                self.edges
                    .get(&(table_name.to_string(), column_name.to_string()))
            })
            .map(|(neighbour, _)| neighbour);
        for neighbour in neighbours {
            if !visited.contains(neighbour.as_str()) {
                if let Some(column_names) = self.nodes.get(neighbour.as_str()) {
                    self.dfs(neighbour, column_names, visited, stack);
                }
            }
        }
        stack.push(table_name.to_string());
    }
}
