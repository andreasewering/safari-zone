use xshell::{cmd, Shell};

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum ContainerStatus {
    Running,
    Stopped,
    Removed,
}

pub fn query_container_status(sh: &Shell, name: &str) -> anyhow::Result<ContainerStatus> {
    let output = cmd!(
        sh,
        "docker ps -a -f name={name} --format '{{json .Status}}'"
    )
    .output()?
    .stdout;
    if output.starts_with(b"\"Up") {
        return Ok(ContainerStatus::Running);
    }
    if output.starts_with(b"\"Exited") || output.starts_with(b"\"Created") {
        return Ok(ContainerStatus::Stopped);
    }
    if output.is_empty() {
        return Ok(ContainerStatus::Removed);
    }
    Err(anyhow::anyhow!(format!(
        "Could not determine container status from docker output '{}'",
        String::from_utf8(output)?
    )))
}
