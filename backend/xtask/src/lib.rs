mod container_status;
mod db_connect_data;
mod env_ids_dir;
mod foreign_key;

use std::{
    future::Future, net::IpAddr, path::PathBuf, str::FromStr, thread::sleep, time::Duration,
};

pub use container_status::*;
pub use db_connect_data::DbConnectData;
pub use env_ids_dir::EnvIdsDir;
pub use foreign_key::{get_foreign_keys_from_db, ForeignKeyGraph};
use xshell::Shell;

pub fn read_line() -> std::io::Result<String> {
    let mut buf = String::new();
    std::io::stdin().read_line(&mut buf)?;
    Ok(buf)
}

pub fn project_directory() -> PathBuf {
    project_directory_helper().unwrap()
}

fn project_directory_helper() -> Option<PathBuf> {
    Some(
        PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .parent()?
            .parent()?
            .to_owned(),
    )
}

pub fn retry<F, E, T>(f: F, initial_backoff_in_ms: u64, max_retries: u32) -> Result<T, E>
where
    F: Fn() -> Result<T, E>,
{
    let mut retry_count: u32 = 0;
    loop {
        match f() {
            Ok(ok) => {
                return Ok(ok);
            }
            Err(err) => {
                if retry_count > max_retries {
                    return Err(err);
                }
                retry_count += 1;
                sleep(Duration::from_millis(
                    initial_backoff_in_ms * 2_u64.pow(retry_count),
                ))
            }
        }
    }
}

pub async fn retry_async<F, Fut, E, T>(
    f: F,
    initial_backoff_in_ms: u64,
    max_retries: u32,
) -> Result<T, E>
where
    F: Fn() -> Fut,
    Fut: Future<Output = Result<T, E>>,
{
    let mut retry_count: u32 = 0;
    loop {
        match f().await {
            Ok(ok) => {
                return Ok(ok);
            }
            Err(err) => {
                if retry_count > max_retries {
                    return Err(err);
                }
                retry_count += 1;
                tokio::time::sleep(Duration::from_millis(
                    initial_backoff_in_ms * 2_u64.pow(retry_count),
                ))
                .await;
            }
        }
    }
}

pub fn generate_certs_for_public_ips(sh: &Shell) -> anyhow::Result<()> {
    let ips = public_ip_addr()?;
    generate_certs(sh, &ips)?;
    Ok(())
}

fn public_ip_addr() -> anyhow::Result<Vec<IpAddr>> {
    use network_interface::{NetworkInterface, NetworkInterfaceConfig as _};
    let network_interfaces = NetworkInterface::show()?;

    let ip_v4s = network_interfaces
        .into_iter()
        .flat_map(|iface| iface.addr.into_iter())
        .map(|addr| addr.ip())
        .filter(|ip| ip.is_ipv4() && IpAddr::from_str("127.0.0.1").ok() != Some(*ip))
        .collect();
    Ok(ip_v4s)
}

fn generate_certs(sh: &Shell, ips: &[IpAddr]) -> anyhow::Result<()> {
    use rcgen::generate_simple_self_signed;

    let subjects: Vec<_> = ips.iter().map(|ip| ip.to_string()).collect();
    let cert = generate_simple_self_signed(subjects)?;
    let private_pem = cert.key_pair.serialize_pem();
    let public_pem = cert.cert.pem();

    let certs_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
        .join("..")
        .join("..")
        .join("frontend")
        .join("certs");
    sh.write_file(certs_dir.join("local-cert.crt"), public_pem)?;
    sh.write_file(certs_dir.join("local-cert.key"), private_pem)?;
    Ok(())
}
