use std::{borrow::Borrow, collections::HashMap, hash::Hash, path::PathBuf};

use serde::{Deserialize, Serialize};

pub struct EnvIdsDir {
    root_path: PathBuf,
    cache: HashMap<String, Mappings>,
}

struct Mappings {
    map: BiHashMap<String, String>,
    is_static: bool,
}

#[derive(Debug, Default)]
pub struct BiHashMap<K, V> {
    from: HashMap<K, V>,
    to: HashMap<V, K>,
}

impl<K: Hash + Eq + Clone, V: Hash + Eq + Clone> BiHashMap<K, V> {
    fn get_value<Q>(&self, key: &Q) -> Option<&V>
    where
        K: Borrow<Q>,
        Q: Hash + Eq + ?Sized,
    {
        self.from.get(key)
    }

    fn get_key<Q>(&self, value: &Q) -> Option<&K>
    where
        V: Borrow<Q>,
        Q: Hash + Eq + ?Sized,
    {
        self.to.get(value)
    }
    fn insert(&mut self, key: K, value: V) {
        self.from.insert(key.clone(), value.clone());
        self.to.insert(value, key);
    }

    fn iter(&self) -> impl Iterator<Item = (&K, &V)> {
        self.from.iter()
    }
}

impl EnvIdsDir {
    pub fn new(root_path: PathBuf, static_path: PathBuf) -> anyhow::Result<Self> {
        std::fs::create_dir_all(&root_path)?;
        std::fs::create_dir_all(&static_path)?;
        let cache = Default::default();
        let mut instance = Self { root_path, cache };
        instance.fill_static_cache(static_path)?;
        instance.fill_cache()?;
        Ok(instance)
    }

    fn fill_static_cache(&mut self, static_path: PathBuf) -> anyhow::Result<()> {
        let mut file_iterator = std::fs::read_dir(&static_path)?;
        while let Some(entry) = file_iterator.next().transpose()? {
            let file_name = entry
                .file_name()
                .into_string()
                .expect("Filenames should be UTF-8");
            let table_name = file_name
                .strip_suffix(".csv")
                .expect("EnvId files have to obey the filename convention tablename.csv")
                .to_owned();
            let reader = csv::Reader::from_path(static_path.join(file_name))?;
            let table_cache = self.cache.entry(table_name).or_insert(Mappings {
                map: Default::default(),
                is_static: true,
            });
            reader.into_deserialize::<IdMapping>().try_for_each(
                |mapping| -> anyhow::Result<()> {
                    let IdMapping { alias, id } = mapping?;
                    table_cache.map.insert(id, alias);
                    Ok(())
                },
            )?;
        }
        Ok(())
    }

    fn fill_cache(&mut self) -> anyhow::Result<()> {
        let mut file_iterator = std::fs::read_dir(&self.root_path)?;
        while let Some(entry) = file_iterator.next().transpose()? {
            let file_name = entry
                .file_name()
                .into_string()
                .expect("Filenames should be UTF-8");
            let table_name = file_name
                .strip_suffix(".csv")
                .expect("EnvId files have to obey the filename convention tablename.csv")
                .to_owned();
            let reader = csv::Reader::from_path(self.root_path.join(file_name))?;
            let table_cache = self.cache.entry(table_name).or_insert(Mappings {
                map: Default::default(),
                is_static: false,
            });
            reader.into_deserialize::<IdMapping>().try_for_each(
                |mapping| -> anyhow::Result<()> {
                    let IdMapping { alias, id } = mapping?;
                    table_cache.map.insert(id, alias);
                    Ok(())
                },
            )?;
        }
        Ok(())
    }

    pub fn alias_to_id(&self, table_name: &str, alias: &str) -> Option<&str> {
        let table_cache = self.cache.get(table_name)?;
        table_cache.map.get_key(alias).map(|id| id.as_str())
    }

    pub fn id_to_alias(&self, table_name: &str, id: &str) -> Option<&str> {
        let table_cache = self.cache.get(table_name)?;
        table_cache.map.get_value(id).map(|alias| alias.as_str())
    }

    pub fn add_mapping(&mut self, table_name: &str, id: &str, alias: &str) {
        let table_cache = self.cache.entry(table_name.to_owned()).or_insert(Mappings {
            map: Default::default(),
            is_static: false,
        });
        table_cache.map.insert(id.to_owned(), alias.to_owned());
    }
}

impl Drop for EnvIdsDir {
    fn drop(&mut self) {
        for (table_name, table_cache) in &self.cache {
            if table_cache.is_static {
                continue;
            }
            let path = self.root_path.join(format!("{table_name}.csv"));
            let mut writer = csv::Writer::from_path(path).unwrap();
            for (id, alias) in table_cache.map.iter() {
                writer
                    .serialize(IdMapping {
                        id: id.to_owned(),
                        alias: alias.to_owned(),
                    })
                    .unwrap()
            }
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct IdMapping {
    alias: String,
    id: String,
}
