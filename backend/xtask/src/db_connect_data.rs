use serde::de::DeserializeOwned;
use xshell::{cmd, Shell};

#[derive(Clone, Copy, Debug)]
pub struct DbConnectData<'a> {
    pub host: &'a str,
    pub port: u16,
    pub username: &'a str,
    pub password: &'a str,
    pub database_name: &'a str,
}

impl<'a> DbConnectData<'a> {
    pub fn create_database(&self, sh: &'a Shell) -> xshell::Result<()> {
        DbConnectData {
            database_name: "postgres",
            ..*self
        }
        .psql_command(sh, &format!("CREATE DATABASE {}", self.database_name))
        .run()
    }

    pub fn parse(conn_str: &'a str) -> Result<Self, &'static str> {
        // Find the start position after the scheme (e.g., "postgresql://")
        let scheme_end = conn_str
            .find("://")
            .ok_or("Missing scheme delimiter '://' in connection string")?;
        let stripped = &conn_str[scheme_end + 3..]; // Skip "://"

        // Find positions of '@', ':', and '/'
        let at_pos = stripped
            .find('@')
            .ok_or("Missing '@' symbol in connection string")?;
        let first_colon_pos = stripped
            .find(':')
            .ok_or("Missing ':' symbol in connection string")?;
        let slash_pos = stripped
            .find('/')
            .ok_or("Missing '/' symbol in connection string")?;

        // Ensure correct order of special characters
        if first_colon_pos > at_pos || at_pos > slash_pos {
            return Err("Malformed connection string");
        }

        // Extract the components based on identified positions
        let (credentials, rest) = stripped.split_at(at_pos);
        let (username, password) =
            credentials.split_at(credentials.find(':').ok_or("Malformed credentials")?);
        let password = &password[1..]; // Remove the colon

        let (host_port, database_name) =
            rest[1..].split_at(rest[1..].find('/').ok_or("Malformed host and port")?);
        let (host, port) = host_port.split_at(host_port.find(':').ok_or("Missing host or port")?);
        let port = &port[1..]; // Remove the colon

        // Parse port number
        let port: u16 = port.parse().map_err(|_| "Invalid port number")?;

        // Create and return DbConnectData struct
        Ok(DbConnectData {
            host,
            port,
            username,
            password,
            database_name: &database_name[1..], // Remove leading '/'
        })
    }

    pub fn connection_string(&self) -> String {
        let DbConnectData {
            host,
            port,
            username,
            password,
            database_name,
        } = self;
        format!("postgres://{username}:{password}@{host}:{port}/{database_name}")
    }

    pub fn psql_command(&self, sh: &'a Shell, command: &'a str) -> xshell::Cmd<'a> {
        let host = self.host;
        let port = self.port.to_string();
        let username = self.username;
        let database_name = self.database_name;
        cmd!(
            sh,
            "psql -h {host} -p {port}
            -U {username}
            -d {database_name}
            -P pager=off
            -q
            --csv
            -c {command}"
        )
        .env("PGPASSWORD", self.password)
    }

    pub fn psql_query<T: DeserializeOwned>(
        &self,
        sh: &Shell,
        query: &str,
    ) -> anyhow::Result<Vec<T>> {
        let output = self.psql_command(sh, query).output()?;
        if !output.stderr.is_empty() {
            println!(
                "STDERR: {}",
                String::from_utf8(output.stderr).expect("stderr was not valid utf-8")
            );
        }
        let reader = csv::Reader::from_reader(output.stdout.as_slice());
        let rows = reader.into_deserialize::<T>().collect::<csv::Result<_>>()?;
        Ok(rows)
    }
}
