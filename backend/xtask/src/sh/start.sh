# This script should be used via the cargo task `cargo app start` and not directly.

set -e

if [ "$#" -ne 4 ]; then
  echo "Usage: $0 <config_dir> <http_port> <grpc_port> <cargo_args>"
  exit 1
fi

CONFIG="$1"
HTTP_PORT="$2"
GRPC_PORT="$3"
CARGO_ARGS="$6"

# Run your Rust application with cargo and pipe the output
env SZ_CONFIG=$CONFIG \
    SZ_SERVER__HTTP_PORT=$HTTP_PORT \
    SZ_SERVER__GRPC_PORT=$GRPC_PORT \
    SZ_ENV="development" \
    SZ_GARDEVOIR__PROVIDERS__GITHUB__CLIENT_SECRET=$GITHUB_CLIENT_SECRET \
    SZ_ARCEUS__TRANSLATION__OPENAI_API_KEY=$OPENAI_API_KEY \
    cargo run --package server --bin server $CARGO_ARGS | while read -r line; do
  # Prepend the http port to each line of output to distinguish logs
  echo "[$HTTP_PORT] $line"
done
