FROM bitnami/kubectl:1.21.13
ARG RUST_VERSION=1.85
ENTRYPOINT []
USER root
RUN apt-get update && apt-get install -y gcc unzip git-crypt && rm -rf /var/lib/apt/lists/*
RUN curl https://sh.rustup.rs -sSf | bash -s -- --default-toolchain=${RUST_VERSION} --profile minimal -y \
  && cd .cargo/bin && rm cargo-clippy cargo-fmt cargo-miri clippy-driver rls rust-analyzer rust-gdb rust-gdbgui rust-lldb rustdoc rustfmt
ENV PATH="/.cargo/bin:${PATH}"
# Add protoc to generate rust code
RUN curl -L -o protoc.zip https://github.com/protocolbuffers/protobuf/releases/download/v21.9/protoc-21.9-linux-x86_64.zip \
  && unzip -o protoc.zip -d /usr/local bin/protoc \
  && unzip -o protoc.zip -d /usr/local include/*
