#!/bin/sh

last_tag=$(git describe --tags --abbrev=0 | sed 's/v//')
new_tag_number=$((last_tag + 1))
new_version="v$new_tag_number"
git tag $new_version
echo $new_version > version.txt
