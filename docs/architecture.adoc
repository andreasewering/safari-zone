= Architecture

To see the diagrams in VSCode, install the https://marketplace.visualstudio.com/items?itemName=asciidoctor.asciidoctor-vscode[Asciidoc Extension] and set 

----
"asciidoc.extensions.enableKroki": true
----

[plantuml, png]
----
[browser] -down-> [nginx]
[nginx] -down-> [backend-1]
[nginx] -down-> [backend-2]
[backend-1] -down-> [postgres]
[backend-2] -down-> [postgres]
[postgres-exporter] -up-> [postgres]
[prometheus] -left-> [postgres-exporter]
[prometheus] -left-> [backend-1]
[prometheus] -left-> [backend-2]
[prometheus] -left-> [nginx-prometheus-exporter]
[nginx-prometheus-exporter] -left-> [nginx]
[grafana] -down-> [prometheus]
[nginx] -down-> [grafana]

note left of [browser]
  This represents the user visiting the site
end note

note left of [nginx]
  - Load Balancer
  - Serves static frontend files
  - Consolidates backends, grafana and frontend urls
end note

note left of [backend-1]
  - Represents one of many instances of the backend server
  - Monolithic architecture
end note

note bottom of [postgres]
  - Relational database
  - Stores all persistent application data
end note

note bottom of [prometheus]
  - Metric collector
  - Exporters help collect data from servers that do not emit prometheus format
end note

note right of [grafana]
  - Frontend for Prometheus
  - Monitoring via Dashboards
end note
----
