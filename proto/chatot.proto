syntax = "proto3";

package chatot;

import "tag.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

// Websocket
message Channel {
  oneof kind {
    sfixed64 group = 2;
    sfixed64 private = 3;
  }
}

message WebsocketMessageToServer {
  message SentMessage {
    Channel channel = 1;
    string text = 2;
  }
  oneof message {
    SentMessage sent_message = 1;
    Channel started_typing = 2;
    Channel stopped_typing = 3;
  }
}

message WebsocketMessageToClient {
  oneof message {
    sfixed64 went_online = 1;
    google.protobuf.Empty sent_message = 2;
    StartedTyping started_typing = 3;
    StoppedTyping stopped_typing = 4;
    sfixed64 went_offline = 5;
    // This indicates that e.g. friend relationship has been ended, or the user has been kicked out of a group.
    Channel channel_closed = 6; 
    WebsocketError error = 10;
  }
}

message StartedTyping {
  sfixed64 from_user_id = 1;
  Channel channel = 2;
}

message StoppedTyping {
  sfixed64 from_user_id = 1;
  Channel channel = 2;
}

enum WebsocketError {
  CannotSentMessageToNonFriend = 0;
  CannotSentMessageToGroupYouHaveNotJoined = 1;
}

// gRPC
service ChatotService {
  // Friends
  rpc GetFriends(GetFriendsRequest) returns (GetFriendsResponse);
  rpc GetFriendRequests(GetFriendRequestsRequest)
      returns (GetFriendRequestsResponse);
  rpc AddFriend(AddFriendRequest) returns (AddFriendResponse);
  rpc RemoveFriend(RemoveFriendRequest) returns (RemoveFriendResponse);
  rpc MarkFriendAsFavorite(MarkFriendAsFavoriteRequest)
      returns (MarkFriendAsFavoriteResponse);
  // Groups
  rpc CreateGroup(CreateGroupRequest) returns (CreateGroupResponse);
  rpc RemoveGroup(RemoveGroupRequest) returns (RemoveGroupResponse);
  rpc GetGroups(GetGroupsRequest) returns (GetGroupsResponse);
  rpc GetGroupDetail(GetGroupDetailRequest) returns (GetGroupDetailResponse);
  rpc AddToGroup(AddToGroupRequest) returns (AddToGroupResponse);
  rpc RequestGroupMembership(RequestGroupMembershipRequest)
      returns (RequestGroupMembershipResponse);
  rpc RemoveFromGroup(RemoveFromGroupRequest) returns (RemoveFromGroupResponse);
  rpc MarkGroupAsFavorite(MarkGroupAsFavoriteRequest)
      returns (MarkGroupAsFavoriteResponse);
  rpc GetChatMessages(GetChatMessagesRequest) returns (GetChatMessagesResponse);
  rpc ParseChatMessage(ParseChatMessageRequest) returns (ParseChatMessageResponse);
}

// Friends
message GetFriendsRequest {}

message GetFriendsResponse { repeated Friend friends = 1; }

message GetFriendRequestsRequest {}

message GetFriendRequestsResponse {
  repeated FriendRequest outgoing = 1;
  repeated FriendRequest incoming = 2;
}

message AddFriendRequest { sfixed64 user_id = 1; }

message AddFriendResponse {}

message RemoveFriendRequest { sfixed64 user_id = 1; }

message RemoveFriendResponse {}

message MarkFriendAsFavoriteRequest {
  sfixed64 user_id = 1;
  bool favorite = 2;
}

message MarkFriendAsFavoriteResponse {}

// Groups
message CreateGroupRequest { string group_name = 1; }

message CreateGroupResponse { Group created = 1; }

message RemoveGroupRequest { sfixed64 group_id = 1; }

message RemoveGroupResponse {}

message GetGroupsRequest {}

message GetGroupsResponse { repeated Group groups = 1; }

message GetGroupDetailRequest { sfixed64 group_id = 1; }

message GetGroupDetailResponse {
  string group_name = 1;
  repeated GroupMember members = 2;
  bool favorite = 3;
}

message AddToGroupRequest {
  sfixed64 user_id = 1;
  sfixed64 group_id = 2;
}

message AddToGroupResponse {}

message RequestGroupMembershipRequest { sfixed64 group_id = 1; }

message RequestGroupMembershipResponse {}

message RemoveFromGroupRequest {
  sfixed64 user_id = 1;
  sfixed64 group_id = 2;
}

message RemoveFromGroupResponse {}

message MarkGroupAsFavoriteRequest {
  sfixed64 group_id = 1;
  bool favorite = 2;
}

message MarkGroupAsFavoriteResponse {}

// Get the last `limit` messages from `since` until `until`
// from all friends and groups of the requesting user
message GetChatMessagesRequest {
  // Only get chat messages which were sent or updated since this timestamp
  optional google.protobuf.Timestamp since = 1;
  // Only get chat messages which were sent or updated before this timestamp
  optional google.protobuf.Timestamp until = 2;
  // Limit the number of received messages for each channel to this number.
  // Note that there is also a max limit configured in `chatot.toml`.
  // If there are more messages than `limit` in a channel,
  // you will get the `limit` last messages (order descending)
  optional uint32 limit = 3;
  // Only get chat messages for a given group chat
  optional sfixed64 group_id = 4;
  // Only get chat messages for a given private chat
  // Setting this together with `group_id` will essentially filter a group chat
  // to including just messages from the given `friend_id` and yourself.
  optional sfixed64 friend_id = 5;
}

message GetChatMessagesResponse {
  repeated FriendChannelMessages friend_messages = 1;
  repeated GroupChannelMessages group_messages = 2;
}

message ParseChatMessageRequest {
  string text = 1;
}

message ParseChatMessageResponse {
  repeated data.Tag tags = 1;
}

// Base Data Types
message Friend {
  sfixed64 user_id = 1;
  string user_name = 2;
  google.protobuf.Timestamp since = 3;
  bool favorite = 4;
  optional google.protobuf.Timestamp deleted_at = 5;
  optional google.protobuf.Timestamp last_seen = 6;
  bool is_online = 7;
}

message FriendRequest {
  sfixed64 user_id = 1;
  string user_name = 2;
  google.protobuf.Timestamp issued_at = 3;
}

message Group {
  sfixed64 group_id = 1;
  string group_name = 2;
  uint32 no_of_group_members = 3;
  string created_by_name = 4;
  sfixed64 created_by_user_id = 5;
  bool favorite = 6;
  optional google.protobuf.Timestamp deleted_at = 7;
}

message GroupMember {
  sfixed64 user_id = 1;
  string user_name = 2;
  enum Role {
    Member = 0;
    Admin = 1;
    Creator = 2;
  }
  Role role = 3;
  google.protobuf.Timestamp since = 4;
  optional google.protobuf.Timestamp deleted_at = 5;
  optional google.protobuf.Timestamp last_seen = 6;
  bool is_online = 7;
}

message FriendChannelMessages {
  sfixed64 friend_id = 1;
  repeated ChatMessage messages = 2;
}

message GroupChannelMessages {
  sfixed64 group_id = 1;
  repeated ChatMessage messages = 2;
}

message ChatMessage {
  string text = 1;
  sfixed64 from_user = 2;
  google.protobuf.Timestamp sent_at = 3;
  optional google.protobuf.Timestamp updated_at = 4;
  repeated data.Tag tags = 5;
}
