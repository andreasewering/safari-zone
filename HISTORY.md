This is a draft for a blog post to be released at some point, probably when this application
is running somewhere public and it is actually playable (there are some decent maps and stuff).

# 5 Years of finding my tech stack

The journey begins in 2020. React is on the rise, Hooks just came out.
People experimented with isomorphic applications (same code running on
front- and backend).

I liked React. Especially the Redux variant where you had a single domain model, the state,
and the view derived from it. It felt cleaner than managing the state by hand in the DOM.
What did not feel clean, however, was the amount of tooling needed to make this a good experience.

1. React itself brings in a huge number of dependencies and a big increase in bundle size.
2. React's state management at the time was awkward, so you bring in Redux and it's React bindings.
3. JavaScript is too easy to make mistakes in, so you bring in Typescript.
4. TypeScript is still too easy to make mistakes in, so you bring in ESLint and a few plugins
5. You inevitably run into errors in your Redux reducer, because you mutated the state (sometimes unknowingly) instead of returning a new, independent state. You bring in Immerjs or a similar solution.
6. There is probably more but I forgot. You still run into runtime errors often, because of Typescripts soundness issues, any-typed DOM APIs like JSON.parse or functions that can throw errors but nothing in the type signature makes that obvious.

Why do I need so many dependencies to make a simple single page app with good developer experience?

## Ever heard of Elm?

Somewhere, I read that Redux was inspired by TEA (The Elm Architecture) and found the Elm programming language. It did not seem very popular and it still isn't, but the docs seemed good and the tagline appealing:

```
A delightful language for reliable web applications
```

I decided to give Elm a try and built a simple website with it, just generating some random Pokémon.
Why did I bother? Elm seemed to address my painpoints:

- Everything is completely immutable. There just are no APIs that mutate data. That also means immutable updates are more ergonomic. No more Immerjs `produce` or `{ ...object, nested: { ...object.nested } }`.
- It enforces a consistent architecture
- It forces the developer to handle all errors
- It's type system is sound
- There is less "legacy" so things like ESLint are less needed
- Components with internal state (like with `useState` in React) are

Here is a quick intro to TEA and Elm's way of doing things:

- A program consists of `init`, `update`, `view` and `subscriptions` functions
- `init` returns an initial `Model` based on user-defined flags that you can pass in from JavaScript and may return a List of `Command`s.
- `Model` is a user-defined type (usually a struct like `{ isLoading : Bool, username : String }`)
- A `Command` is a description for a side-effect, like a HTTP Request to be performed by the Elm runtime. APIs returning a `Command` often take in a parameter that specifies how to feed the result of the side-effect back into the program (as a `Message`).
- `update` takes the current model and a `Message` and returns a new model and a List of `Commands`.
- A `Message` is a user-defined type (usually a union type like `ClickedButton | ClosedOverlay | GotHttpResponse`).
- `view` produces a `Document` (HTML + website title) based on the current `Model`.
- `subscriptions` produce `Subscriptions` based on the current `Model`. These allow you to register tasks in the Elm runtime that return multiple values, like getting the current time each second.

This design has a couple of interesting consequences:

- Since all commands are managed by the runtime, the only way for side-effects to get triggered is by the `init` and `update` functions. All non-deterministic APIs, even "basic" things like "get a random number" are implemented via commands. So the `view` is a pure (side-effect free) function.
- This makes it possible to implement (and it has been implemented) a "time-traveling" debugger that works extremely well. You can even export the history of commands and replay on a different browser or computer and get the same result.
- Most Elm applications look extremely similar
- You cannot get a runtime error originating from your Elm code (except in some edge cases, like comparing two functions with `==`)
- Long functions and files are surprisingly easy to work with. Everything is immutable and side-effect free, so functions cannot influence each other via things like top-level variables. This is important, because functions like the top-level `update` function are bound to get large.

## Choosing a backend

After experimenting with it a while, I began to appreciate Elm more and more.
I began trusting the compiler and refactored with ease and no fear. This was very surprising, since in my day job I had quite the opposite experience with JavaScript and Java.

Feeling like a client-side only application is limited and also unrealistic, I began writing some server code for my application. Given that Elm is designed as a frontend-only language I started with the language I felt most comfortable with, TypeScript.

Although the ecosystem is quite amazing, I felt the same churn as in the frontend.
It moved too fast for me - new frameworks, new major versions, breaking changes for a lot of more or less required dependencies.
The function paradigm first introduced to me by React and now enforced by Elm also grew on me
and I wanted to find a backend language that allowed me to write my code in a similar style.

## Haskell, Attempt 1

Elm inherits many of its traits and its syntax from Haskell, so that was the natural choice for me to try.
It did not turn out well - I could not even solve simple problems, let alone write an HTTP server.
For me, this speaks to the quality of the error messages of the Elm compiler and the docs.
Although both languages employ the pure functional paradigm, Elm was much simpler to get into.
I gave up on Haskell and decided to look for other options.

## Elixir and Phoenix

On the Elm discourse, some people reported that they use a Elixir backend with the Phoenix framework with great results. At first I was hesistant - I was used to statically typed languages and burnt my fingers often enough with JavaScript.
But Elixir was different. First of all, fault tolerance is built-in.
If a process crashes, a supervisor picks it up and starts it again with a consistent state.
Then, great testing, REPL (Read-Eval-Print-Loop), method overloading and macro system make the dev experience good while taking advantage of the strengths of dynamic typing.
It didn't feel as chaotic as JavaScript. The Phoenix Framework and its code generators were providing some much needed structure to the project. Immutability is a great asset for code readability. You just have to keep less information in your head.

A quick summary of what Elixir and Pheonix provide:

## Haskell revisited

Despite the great developer experience of Elixir and it's ecosystem, I was getting frustrated by the lack of static typing. I made simple mistakes that a static type checker easily would have caught. Dialyzer helps, but it is not a replacement. I am not alone with this sentiment either. Since I last tried Elixir, the creator experimented with adding optional static typing to the language and Gleam, a statically typed alternative on the same underlying BEAM machinery has been released.

After doing a couple of simple Advent of Code style exercises in Haskell, I was getting more confident,
plus my Elm skills have been growing (it has been around 2 years since I started with this side project). Haskell's tooling on Windows, which I was using at the time, had improved as well. Guided by tutorials, the compiler error messages, and good documentation on the Servant website (an HTTP framework), I was able to make a breakthrough and replace a part of my Elixir backend with Haskell.

But what exactly was by application capable of doing at that time? Why only a part of my application?
This was not just do Hello world in a few different languages, the project and its capabilities grew as well.
At this point, I had authentication based on our companies Keycloak server built in, the backend was generating images for WebGL-based rendering on the frontend and you could see other players (Multiplayer).
I was using Redis PubSub for messaging because I wanted the possibility for horizontal scaling and Postgres for persistence. The maps the player walked on were already mostly editable.

There was a lot missing though. There was only a single map, no warps via cave entrances or doors,
there was no concept of roles and permissions, and most importantly - there were no Pokémon.

## Exploring other frontend technologies

Elm isn't perfect. It just does not feel as productive as JavaScript does - any state of any element on the page has to be centralized in a single top-level model, any state changes and side effects have to go through the top-level update function etc.
Especially the map editor felt like a daunting task. Elm does not provide the tools to do image manipulation and JavaScript interop is very limited. I decided to build the map editor with Svelte, as it seemed to sidestep some of React's footguns and its huge bundle size by using a compiler-based approach.

While I like Svelte, especially for it's lightweight stores and `$` syntax, I felt the npm ecosystem churn again. I tried to integrate Svelte into Elm by compiling to my Svelte components to WebComponents, but some features were not supported and the active development on their side meant code working at one point stopped working or emitted a warning a month later.
Compare this to Elm - when I started in 2020, the version was 0.19 and right now, its still 0.19.
There were a few patch versions released over the years, but absolutely nothing broke.

I became more comfortable with the available ways of doing JavaScript interop in Elm and eventually removed Svelte from the project again, settling for browser-native WebComponents integrated into my Elm code. Slightly worse dev experience, but no more churn. Browser standards exist for a reason.

## Protocol Buffers and gRPC

Ever since I began working on the project, the method of communication over the wire has been JSON over HTTP.
I read a post from the creator of Elm about its future, which mentioned efficient binary formats for a possible future where Elm could e.g. communicate directly with a database.

The article got me curious. I am in control of all parts of my system - then why am I using string-based serialization with field names and other overhead. There are many solutions, but I found Protocol Buffers to be the most widely supported one. The idea is pretty much:

1. Define a schema for the data that should cross the wire.
2. Generate code on both sides.
3. Use the most efficient format possible - maybe not the most efficient, stuff like backwards compatability with older versions of the schema is nice.

gRPC goes a step further and lets you define `Service`s with `Methods`. Essentially, with the generated code, you can just go `typedResponseBody = service.myMethod(typedRequestBody)`.

I found this absolutely incredible and still do, and think this kind of binary encoding should be the default in backend communication. For frontends its an option but less good, since you may have users that haven't updated their app/website for days and having a self-describing protocol can help there.

Haskells ecosystem had `proto-lens`, which worked reasonably well. Elm had `protoc-gen-elm` which worked mostly well, but had some issues with package names and did not support 64-bit integer types (because Elms Ints are Javascript 53-bit floating point numbers).
Since the original maintainer has mostly moved onto other technologies, I took care of the project and added the missing features.
I did not use gRPC at this point, because I liked `servant`s rest-service-as-a-type approach.

## Microservices

My codebase had grown large enough that I was thinking about how to modularize it better.
At the time, microservices were all the hype and monorepos were also getting popular.
I identified four responsibilities that I could split relatively cleanly:

1. User management, authentication
2. Map configuration and image generation
3. Main game loop
4. Pokémon generation and movement

With `servant`s type-safety, I could easily split the application in four seperately running applications.
Cool. In theory this could help scalability and reduce CI/CD runtimes by only building and deploying what changed.

## Elm revisited

Let's talk about Elm again. It has successfully defended its spot in the tech stack up until now (and up to the present!) although I encountered many pain points. There just wasn't much competition in the area.
I wanted to talk about how I addressed most of the pain points and my current architecture.

### Internationalization

In most languages, there exists some framework where you just say something like `translate('MY_TRANSLATION_KEY')` and it "just works". You specify the actual texts in some other files in JSON format for example and the framework takes care of loading and choosing the correct text.
In Elm this is not as easy. You cannot cheat. You cannot hide side effects.
So what can you do when you cannot cheat? You pass parameters.

```
headline : Html msg
headline = Html.h1 [] [Html.text "My hardcoded text"]
```

becomes

```
headline : Language -> Translations -> Html msg
headline language translations = Html.h1 [] [Html.text (translate language translations.headline) ]
```

This works but it is super ugly and boilerplatey.
So I brought in `html-with-context`, which redefines all of the `elm/html` package to pass a `ctx` parameter to all child nodes up until the leafs like `Html.text` where the parameter is simply ignored.
This allows you to write code like the above like this:

```
headline : Html { language: Language, translations: Translations } msg
headline =
    Html.h1 []
        [ Html.withContext <| \{ language, translations } ->
              Html.text (translate language translations.headline)
        ]
```

which seems worse, but now you do not have to explicitely pass the context to all child components that need it. Add in a type alias like `type alias Html msg = Html.Html { language: Language, translations: Translations } msg` and a reusable function `text key = Html.withContext <| \{ language, translations } -> Html.text (translate language (key translations))` and you get

```
headline : Html msg
headline =
    Html.h1 [] [text .headline]
```

which makes Internationalization just as simple as in any other language.

But... not quite. We skipped over the side effect problem. Right now, this design only works if we hardcode all our translations in some Elm file in a giant struct. Which isn't that bad to be honest. It's typesafe and there are no race conditions at runtime since the translations get loaded at the same time as the rest of the code. Still, if we imagine a large application with many pages which is internationalized to 10+ languages, we might see problems. The approach does not scale.

My solution involved writing my own open source code generator, `travelm-agency`. It takes translation files in `json`, `properties` or `ftl` format and turns them into Elm code. Inspired by protocol buffers, I made the wire format compact, not needing any translation keys and using indices in the generated code instead.
The extra cool thing about using a code generator for this is that I could detect placeholders in the translations and generate functions with the correct amount of parameters. Also, hacky access to the browsers `Intl` API behind the scenes meant I could format e.g. numbers differently depending on the language.

- English? 3.14
- German? 3,14

### Organizing the main model

In other Elm projects on Github, I often saw the "Session pattern" where the main model type is defined like this:

```
type alias Model =
    { session: Session
    , page: PageModel
    }

type PageModel =
    | Overview OverviewModel
    | Detail DetailModel
    ...
```

A pages update function would then get the `Session` and its specific `PageModel` and optionally update both.
What I did not like about this is the inflexability in rendering: imagine we want to animate a transition from `Overview` to `Detail`. With this model, we cannot render both pages at the same time, since there is only one `page` property in the `Model`. You could, of course add something like `previousModel`, but really it's not getting any better.

So I went with a different route and replaced the union type with a struct. I also inlined the session type
so the model is more flat.

```
type alias Model =
    { overviewPage: OverviewModel
    , detailPage: DetailModel
    ...
    , route: Route
    , accessToken: String
    , now: Time.Posix
    ...
    }
```

This almost works but you run into a different problem. Most likely, the `DetailModel` previously had some kind of `id : Int` field. In the previous design that was fine, if you were on the detail page you would pass the parameter from the route in the model on initialization. If you weren't, the `model` wasn't a `DetailModel`.

My solution involves more parameter passing. First, we delete the sync'd fields from the URL from our page models.

## Rust

## The distributed monolith

## Closing thoughts
