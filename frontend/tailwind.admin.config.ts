import { Config } from 'tailwindcss';
import colors from 'tailwindcss/colors.js';

/**
 * Config for admin application. The config for other smaller pages is seperate
 * in `tailwind.minimal.config.ts`, reason being smaller bundle sizes where it matters.
 * @type {import('tailwindcss').Config}
 * */
export default {
  content: [
    './src/components/**/*.ts',
    './src/admin/index.html',
    './src/lib/**/*.elm',
    './src/admin/**/*.elm',
  ],
  darkMode: ['selector', '[theme="dark"]'],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      red: colors.red,
      amber: colors.amber,
      yellow: colors.yellow,
      green: colors.green,
      sky: colors.sky,
      pink: colors.pink,
    },
  },
} satisfies Config;
