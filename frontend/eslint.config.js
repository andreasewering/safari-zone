import ts from 'typescript-eslint';
import js from '@eslint/js';
import { readdirSync, statSync } from 'fs';
import path from 'path';
import url from 'node:url';
import globals from 'globals';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));
const tsConfigScanDepth = 2;

const ignores = ['.idea', '.vscode', 'dist', 'elm-stuff', 'node_modules'];
const normalizedIgnores = ignores.map(path.normalize);

export default ts.config(
  {
    ignores: [...ignores, 'src/generated'],
  },
  {
    files: ['src/**/*.ts'],
    plugins: {
      '@typescript-eslint': ts.plugin,
    },
    extends: [
      js.configs.recommended,
      ...ts.configs.strictTypeChecked,
      ...ts.configs.stylisticTypeChecked,
    ],
    languageOptions: {
      parser: ts.parser,
      parserOptions: {
        project: ['src/tsconfig.json'],
      },
    },
    rules: {
      '@typescript-eslint/consistent-type-definitions': ['error', 'type'],
    },
  },
  {
    files: ['*.js'],
    extends: [js.configs.recommended],
    languageOptions: {
      globals: {
        ...globals.node,
      },
    },
  }
);

export function scanForTsConfigs() {
  const tsConfigList = [];
  function pushTsConfig(absolutePath) {
    tsConfigList.push(path.relative(__dirname, absolutePath));
  }
  scanForTsConfigsRecursive(__dirname, pushTsConfig, 0);
  return tsConfigList;
}

function scanForTsConfigsRecursive(dir, pushTsConfig, depth) {
  const dirEntries = readdirSync(dir);
  for (const entry of dirEntries) {
    const absolutePath = path.join(dir, entry);
    const relativePath = path.normalize(path.relative(__dirname, absolutePath));
    if (normalizedIgnores.some((ignore) => relativePath === ignore)) {
      continue;
    }
    const stat = statSync(absolutePath);
    if (stat.isDirectory() && depth < tsConfigScanDepth) {
      scanForTsConfigsRecursive(absolutePath, pushTsConfig, depth + 1);
    }
    if (stat.isFile() && entry == 'tsconfig.json') {
      pushTsConfig(absolutePath);
    }
  }
}
