module Main.RouteTest exposing (..)

import Expect
import Fuzz exposing (Fuzzer)
import Main.Dex.Route as Dex
import Main.Route as Route
import Test exposing (Test, describe, fuzz)
import Url


listFuzzer : List a -> Fuzzer a
listFuzzer =
    List.map Fuzz.constant >> Fuzz.oneOf


protocolFuzzer : Fuzzer Url.Protocol
protocolFuzzer =
    listFuzzer
        [ Url.Http
        , Url.Https
        ]


urlFuzzer : Fuzzer Url.Url
urlFuzzer =
    Fuzz.map Url.Url protocolFuzzer
        |> Fuzz.andMap Fuzz.string
        |> Fuzz.andMap (Fuzz.maybe Fuzz.int)
        |> Fuzz.andMap Fuzz.string
        |> Fuzz.andMap (Fuzz.maybe Fuzz.string)
        |> Fuzz.andMap (Fuzz.maybe Fuzz.string)


suite : Test
suite =
    describe "Route"
        [ fuzz urlFuzzer "/ is HomePage" <|
            \url ->
                let
                    expectedRoute =
                        case url.path of
                            "" ->
                                Ok (Route.Dex Dex.Overview)

                            "/" ->
                                Ok (Route.Dex Dex.Overview)

                            "/safari" ->
                                Ok Route.Safari

                            "/safari/" ->
                                Ok Route.Safari

                            _ ->
                                Err url
                in
                Route.toRoute url
                    |> Expect.equal expectedRoute
        ]
