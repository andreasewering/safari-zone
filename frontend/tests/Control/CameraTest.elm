module Control.CameraTest exposing (..)

import Camera exposing (Camera)
import Config
import Data.Units exposing (TileUnit(..), WebGLUnit(..), unWebGLUnit)
import Domain.Coordinate exposing (Coordinate)
import Expect exposing (FloatingPointTolerance(..))
import Test exposing (Test, describe, test)


exampleCamera : Coordinate TileUnit -> Camera
exampleCamera player =
    Camera.mkCamera player
        { height = Config.tileSize * 10, width = Config.tileSize * 15 }
        { tileMapSize = { x = TileUnit 50, y = TileUnit 30 }, offset = { x = TileUnit 0, y = TileUnit 0 } }


suite : Test
suite =
    describe "Camera"
        [ test "calculates the expected map offset if player is in the middle of the map" <|
            \_ ->
                Camera.calcTileOffset (exampleCamera { x = TileUnit 8, y = TileUnit 9 })
                    |> Expect.equal { x = TileUnit -0.5, y = TileUnit -4 }
        , test "only shifts the map in the positive slightly" <|
            \_ ->
                Camera.calcTileOffset (exampleCamera { x = TileUnit 2, y = TileUnit 1 })
                    |> Expect.equal { x = TileUnit 1, y = TileUnit 1 }
        , test "does not shift the map over its positive limit" <|
            \_ ->
                Camera.calcTileOffset (exampleCamera { x = TileUnit 48, y = TileUnit 28 })
                    |> Expect.equal { x = TileUnit -37, y = TileUnit -22 }
        , test "player coord has webgl offset of 0/0 if in the middle" <|
            \_ ->
                Camera.calcWebGLCoord (exampleCamera { x = TileUnit 8, y = TileUnit 9 })
                    { x = TileUnit 8, y = TileUnit 9 }
                    |> Expect.equal { x = WebGLUnit 0, y = WebGLUnit 0 }
        , test "top-left has webgl offset of close to -1/1" <|
            \_ ->
                -- -1 / 1 would mean that the player could literally walk to the edge of the screen,
                -- and we want the player to be more central than that, even if less map is visible then
                Camera.calcWebGLCoord (exampleCamera { x = TileUnit 1, y = TileUnit 1 })
                    { x = TileUnit 0, y = TileUnit 0 }
                    |> Expect.all
                        [ .x >> unWebGLUnit >> Expect.within (Absolute 0.01) -0.86
                        , .y >> unWebGLUnit >> Expect.within (Absolute 0.01) 0.8
                        ]
        ]
