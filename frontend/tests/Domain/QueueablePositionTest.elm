module Domain.QueueablePositionTest exposing (..)

import Data.Units exposing (unTileUnit)
import Domain.Coordinate as Coord
import Domain.Direction as Direction
import Domain.Position as Position
import Domain.QueueablePosition as QueueablePosition
import Expect exposing (FloatingPointTolerance(..))
import Fuzz
import Test exposing (Test, describe, fuzz, fuzz2, fuzz3)


suite : Test
suite =
    describe "QueueablePosition"
        [ fuzz3 Fuzz.niceFloat Coord.fuzzer (Fuzz.maybe Direction.fuzzer) "works just like Position for a single move" <|
            \dt startCoord dir ->
                let
                    posResult =
                        Position.init startCoord 0 Direction.default
                            |> Position.move dir 0 dt
                            |> Position.getExact
                in
                QueueablePosition.init startCoord 0 Direction.default
                    |> QueueablePosition.move dir 0 dt
                    |> QueueablePosition.getExact
                    |> Expect.equal posResult
        , fuzz Coord.fuzzer "move to coord triggers small move in the right direction for adjacent tile" <|
            \startCoord ->
                QueueablePosition.init startCoord 0 Direction.default
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord) 0 Direction.D
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal (Coord.toFloat startCoord |> Coord.plus { x = 0, y = 0.1 })
        , fuzz Coord.fuzzer "works correctly for multiple moves" <|
            \startCoord ->
                QueueablePosition.init startCoord 0 Direction.default
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord) 0 Direction.D
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 1, y = 1 } startCoord) 0 Direction.R
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.all
                        [ .x >> Expect.equal (toFloat startCoord.x)
                        , .y >> Expect.within (Absolute 0.00001) (toFloat startCoord.y + 0.2)
                        ]
        , fuzz Coord.fuzzer "delay keeps position from moving until later" <|
            \startCoord ->
                QueueablePosition.initDelayed 1 startCoord 0 Direction.default
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord) 0 Direction.D
                    |> QueueablePosition.move Nothing 0 0.5
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 1, y = 1 } startCoord) 0 Direction.R
                    |> QueueablePosition.move Nothing 0 0.5
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.all
                        [ .x >> Expect.equal (toFloat startCoord.x)
                        , .y >> Expect.within (Absolute 0.00001) (toFloat startCoord.y + 0.5)
                        ]
        , fuzz Coord.fuzzer "move to coord jumps directly to target tile if not adjacent" <|
            \startCoord ->
                let
                    target =
                        Coord.plus { x = 1, y = 1 } startCoord
                in
                QueueablePosition.init startCoord 0 Direction.default
                    |> QueueablePosition.moveToCoord target 0 Direction.U
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal (Coord.toFloat target)
        , fuzz2 Coord.fuzzer Fuzz.niceFloat "stops at target position eventually" <|
            \startCoord dt ->
                QueueablePosition.init startCoord 0 Direction.default
                    |> QueueablePosition.moveToCoord (Coord.plus { x = 0, y = 1 } startCoord) 0 Direction.D
                    |> QueueablePosition.move Nothing 0 dt
                    |> QueueablePosition.getExact
                    |> Coord.map unTileUnit
                    |> Expect.all
                        [ .x >> Expect.equal (toFloat startCoord.x)
                        , .y >> Expect.atMost (toFloat startCoord.y + 1)
                        ]
        ]
