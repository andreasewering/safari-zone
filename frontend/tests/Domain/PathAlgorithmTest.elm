module Domain.PathAlgorithmTest exposing (suite)

import Domain.Direction as Direction
import Domain.MoveRule as MoveRule exposing (MoveRules)
import Domain.PathAlgorithm exposing (findBestPath)
import Expect
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "PathAlgorithm"
        [ test "one tile away" <|
            \_ ->
                findBestPath rules MoveRule.walk ( { x = 1, y = 3 }, 0 ) ( { x = 2, y = 3 }, 0 )
                    |> Expect.equal (Just ( Direction.R, 0 ))
        , test "two tiles away" <|
            \_ ->
                findBestPath rules MoveRule.walk ( { x = 1, y = 3 }, 0 ) ( { x = 3, y = 3 }, 0 )
                    |> Expect.equal (Just ( Direction.R, 0 ))
        , test "diagonal" <|
            \_ ->
                findBestPath rules MoveRule.walk ( { x = 1, y = 3 }, 0 ) ( { x = 3, y = 5 }, 0 )
                    |> Expect.equal (Just ( Direction.D, 0 ))
        ]


rules : MoveRules
rules =
    List.range -5 5
        |> List.concatMap (\x -> List.range -5 5 |> List.map (\y -> { x = x, y = y }))
        |> MoveRule.groundRules 0
