module Domain.ConstrainedPositionTest exposing (..)

import Data.Frame exposing (fuzzMax1Tile)
import Data.Units exposing (unTileUnit)
import Domain.ConstrainedPosition as ConstrainedPosition
import Domain.Coordinate as Coord
import Domain.Direction as Direction
import Domain.MoveRule as MoveRule
import Domain.Position as Position
import Expect
import Fuzz
import Test exposing (Test, describe, fuzz2)


suite : Test
suite =
    describe "ConstrainedPosition"
        [ fuzz2 fuzzMax1Tile Coord.fuzzer "behaves the same as regular position if relevant moverules are ground" <|
            \dt startCoord ->
                let
                    regularPosResult =
                        Position.init startCoord 0 Direction.default
                            |> Position.move (Just Direction.D) 0 dt
                            |> Position.getExact

                    rules =
                        MoveRule.rulesFromProto
                            [ { x = startCoord.x
                              , y = startCoord.y
                              , layerNumber = 0
                              , rule = 2 ^ 16
                              , animationIds = []
                              }
                            , { x = startCoord.x
                              , y = startCoord.y + 1
                              , layerNumber = 0
                              , rule = 12345
                              , animationIds = []
                              }
                            ]
                in
                Position.init startCoord 0 Direction.default
                    |> ConstrainedPosition.move (Just Direction.D) dt rules
                    |> Position.getExact
                    |> Expect.equal regularPosResult
        , fuzz2 Fuzz.float Coord.fuzzer "stops any movement if the surrounding coords are not allowed" <|
            \dt startCoord ->
                Position.init startCoord 0 Direction.default
                    |> ConstrainedPosition.move (Just Direction.D)
                        dt
                        (MoveRule.rulesFromProto
                            [ { x = startCoord.x
                              , y = startCoord.y
                              , layerNumber = 0
                              , rule = 0
                              , animationIds = []
                              }
                            ]
                        )
                    |> Position.getExact
                    |> Coord.map unTileUnit
                    |> Expect.equal (Coord.toFloat startCoord)
        ]
