module Domain.MoveRuleTest exposing (suite)

import Domain.Direction as Direction
import Domain.MoveRule as MoveRule
import Expect
import Test exposing (..)


suite : Test
suite =
    describe "MoveRule"
        [ test "can decode moveRules from Proto" <|
            \_ ->
                let
                    decodedMoveRules =
                        [ { x = 1, y = 2, layerNumber = 0, rule = 5 * 2 ^ 16 + 5 * 2 ^ 24, animationIds = [] }
                        , { x = 2, y = 2, layerNumber = 0, rule = 12345, animationIds = [] }
                        ]
                            |> MoveRule.rulesFromProto
                in
                decodedMoveRules
                    |> Expect.all
                        [ MoveRule.canMoveFromTo ( { x = 1, y = 2 }, 0 ) Direction.R MoveRule.walk
                            >> Expect.equal (Just 0)
                        , MoveRule.canMoveFromTo ( { x = 1, y = 2 }, 0 ) Direction.D MoveRule.swim
                            >> Expect.equal Nothing
                        , MoveRule.canMoveFromTo ( { x = 1, y = 2 }, 0 ) Direction.D MoveRule.walk
                            >> Expect.equal (Just 0)
                        ]
        ]
