module Domain.CuboidTest exposing (..)

import Expect
import Fuzz exposing (Fuzzer)
import Set
import Shared.Types.Cuboid as Cuboid
import Test exposing (Test, describe, fuzz, fuzz3, test)


suite : Test
suite =
    describe "toCuboids"
        [ fuzz (Fuzz.list coordinateFuzzer) "covers all coordinates with rectangles" <|
            \coords ->
                let
                    set =
                        Set.fromList coords

                    cuboids =
                        Cuboid.toCuboids set

                    numberOfRectanglesContainingCoord ( z, x, y ) =
                        List.filter (\c -> c.yMin <= y && c.yMax >= y && c.xMin <= x && c.xMax >= x && c.zMin <= z && c.zMax >= z)
                            cuboids
                            |> List.length
                in
                coords
                    |> List.map numberOfRectanglesContainingCoord
                    |> List.all ((==) 1)
                    |> Expect.equal True
        , fuzz (Fuzz.list coordinateFuzzer) "min values are lesser or equal than max values" <|
            \coords ->
                let
                    set =
                        Set.fromList coords

                    cuboids =
                        Cuboid.toCuboids set
                in
                cuboids
                    |> List.all (\{ xMin, xMax, yMin, yMax, zMin, zMax } -> xMin <= xMax && yMin <= yMax && zMin <= zMax)
                    |> Expect.equal True
        , fuzz (Fuzz.list coordinateFuzzer) "does not cover any extra coordinates" <|
            \coords ->
                let
                    set =
                        Set.fromList coords

                    cuboids =
                        Cuboid.toCuboids set

                    coordsInRectangle c =
                        List.range c.xMin c.xMax
                            |> List.concatMap (\x -> List.range c.yMin c.yMax |> List.map (Tuple.pair x))
                            |> List.concatMap (\( x, y ) -> List.range c.zMin c.zMax |> List.map (\z -> ( z, x, y )))
                in
                List.concatMap coordsInRectangle cuboids
                    |> Set.fromList
                    |> Set.diff set
                    |> Expect.equal Set.empty
        , fuzz3 (Fuzz.intRange 1 100) (Fuzz.intRange 1 100) (Fuzz.intRange 1 100) "cuboid shape coordinates produces single cuboid" <|
            \m n o ->
                let
                    coords =
                        List.range 1 m
                            |> List.concatMap (\x -> List.range 1 n |> List.map (Tuple.pair x))
                            |> List.concatMap (\( x, y ) -> List.range 1 o |> List.map (\z -> ( z, x, y )))

                    cuboids =
                        Set.fromList coords |> Cuboid.toCuboids
                in
                cuboids
                    |> Expect.equal [ { xMin = 1, yMin = 1, zMin = 1, xMax = m, yMax = n, zMax = o } ]
        , test "Shape '|-' results in 3 cuboids " <|
            \_ ->
                let
                    coords =
                        [ ( 0, 1, 1 ), ( 1, 1, 1 ), ( 1, 2, 1 ), ( 2, 1, 1 ) ]

                    cuboids =
                        Set.fromList coords |> Cuboid.toCuboids
                in
                cuboids
                    |> Expect.equal
                        [ { xMin = 1, yMin = 1, zMin = 0, xMax = 1, yMax = 1, zMax = 0 }
                        , { xMin = 1, yMin = 1, zMin = 1, xMax = 2, yMax = 1, zMax = 1 }
                        , { xMin = 1, yMin = 1, zMin = 2, xMax = 1, yMax = 1, zMax = 2 }
                        ]
        , test "Shape '_|_' results in 2 cuboids " <|
            \_ ->
                let
                    coords =
                        [ ( 0, 0, 1 ), ( 0, 1, 1 ), ( 0, 2, 1 ), ( 1, 1, 1 ), ( 2, 1, 1 ) ]

                    cuboids =
                        Set.fromList coords |> Cuboid.toCuboids
                in
                cuboids
                    |> Expect.equal
                        [ { xMin = 0, yMin = 1, zMin = 0, xMax = 2, yMax = 1, zMax = 0 }
                        , { xMin = 1, yMin = 1, zMin = 1, xMax = 1, yMax = 1, zMax = 2 }
                        ]
        ]


coordinateFuzzer : Fuzzer ( Int, Int, Int )
coordinateFuzzer =
    Fuzz.triple (Fuzz.intRange -10 10) (Fuzz.intRange -1000 1000) (Fuzz.intRange -1000 1000)
