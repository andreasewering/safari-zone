module Generators.CustomDict exposing (CustomDictConfig, generateFile)

import Elm
import Elm.Annotation as Type
import Elm.Arg
import Elm.Case
import Gen.ManualDict as ManualDict


type alias CustomDictConfig =
    { keyTypeName : String
    , keyType : Type.Annotation
    , comparableType : Type.Annotation
    , toComparable : Elm.Expression -> Elm.Expression
    }


generateFile : CustomDictConfig -> Elm.File
generateFile config =
    let
        vVar =
            Type.var "v"

        dictTypeName =
            config.keyTypeName ++ "Dict"

        dictAnn =
            dictAnnWith vVar

        dictAnnWith typeVar =
            ManualDict.annotation_.dict config.comparableType
                config.keyType
                typeVar

        customAnn =
            customAnnWith vVar

        customAnnWith typeVar =
            Type.namedWith [] dictTypeName [ typeVar ]

        wrap =
            wrapWith vVar

        wrapWith typeVar dict =
            Elm.apply (Elm.val dictTypeName) [ dict ]
                |> Elm.withType (customAnnWith typeVar)

        unwrap : (Elm.Expression -> Elm.Expression) -> Elm.Expression -> Elm.Expression
        unwrap f dict =
            Elm.Case.custom dict
                customAnn
                [ Elm.Case.branch
                    (Elm.Arg.customType dictTypeName identity
                        |> Elm.Arg.item (Elm.Arg.varWith "d" dictAnn)
                    )
                    f
                ]

        wrapped : (Elm.Expression -> Elm.Expression) -> Elm.Expression -> Elm.Expression
        wrapped f =
            unwrap f >> wrap

        customType =
            Elm.customTypeWith dictTypeName
                [ "v" ]
                [ Elm.variantWith dictTypeName [ dictAnn ] ]
                |> Elm.withDocumentation ("A Dict using " ++ config.keyTypeName ++ " as key.")
                |> Elm.expose

        empty =
            wrap ManualDict.empty
                |> Elm.withType customAnn
                |> Elm.declaration "empty"
                |> Elm.withDocumentation ("Construct an empty " ++ dictTypeName)
                |> Elm.expose

        fromList =
            Elm.fn (Elm.Arg.varWith "list" (Type.list (Type.tuple config.keyType vVar)))
                (ManualDict.call_.fromList (Elm.functionReduced "e" config.toComparable) >> wrap)
                |> Elm.declaration "fromList"
                |> Elm.withDocumentation ("Convert a " ++ dictTypeName ++ " into the list of its entries")
                |> Elm.expose

        toList =
            Elm.fn (Elm.Arg.varWith "dict" customAnn)
                (unwrap ManualDict.toList)
                |> Elm.declaration "toList"
                |> Elm.withDocumentation ("Convert a list of entries to a " ++ dictTypeName)
                |> Elm.expose

        foldl = 
            Elm.fn3 
                (Elm.Arg.varWith "f" <| Type.function [ config.keyType, vVar, Type.var "b" ] (Type.var "b"))
                (Elm.Arg.varWith "acc" <| Type.var "b") 
                (Elm.Arg.varWith "dict" customAnn)        
                (\f acc -> unwrap <| ManualDict.foldl (\k v acc_ -> Elm.apply f 
                    [Elm.withType config.keyType k, Elm.withType vVar v, Elm.withType (Type.var "b") acc_]) acc)
                |> Elm.declaration "foldl"
                |> Elm.withDocumentation ("Iterate over the entries of a " ++ dictTypeName ++ " to construct a new value")
                |> Elm.expose

        values =
            Elm.fn (Elm.Arg.varWith "dict" customAnn)
                (unwrap ManualDict.values)
                |> Elm.declaration "values"
                |> Elm.withDocumentation ("Get the list of values from a " ++ dictTypeName)
                |> Elm.expose

        get =
            Elm.fn2 (Elm.Arg.varWith "k" config.keyType)
                (Elm.Arg.varWith "dict" customAnn)
                (\k -> unwrap <| ManualDict.get config.toComparable k)
                |> Elm.declaration "get"
                |> Elm.withDocumentation ("Get the value of a given key in a " ++ dictTypeName)
                |> Elm.expose

        insert =
            Elm.fn3 (Elm.Arg.var "k")
                (Elm.Arg.var "v")
                (Elm.Arg.var "dict")
                (\k v ->
                    wrapped (ManualDict.insert config.toComparable k v)
                )
                |> Elm.declaration "insert"
                |> Elm.withDocumentation ("Insert an entry into the " ++ dictTypeName ++ ", overriding the previous value if it existed.")
                |> Elm.expose

        update =
            Elm.fn3
                (Elm.Arg.varWith "k" config.keyType)
                (Elm.Arg.varWith "f" <| Type.function [ Type.maybe vVar ] (Type.maybe vVar))
                (Elm.Arg.varWith "dict" customAnn)
                (\k f ->
                    wrapped
                        (ManualDict.update config.toComparable
                            k
                            (\s ->
                                Elm.apply f [ s ] |> Elm.withType (Type.function [ Type.maybe vVar ] (Type.maybe vVar))
                            )
                        )
                )
                |> Elm.declaration "update"
                |> Elm.withDocumentation ("Updates an entry from the " ++ dictTypeName ++ " via the given function. The argument for the function will be `Nothing` when the key is not in the map yet.")
                |> Elm.expose

        map =
            Elm.fn2
                (Elm.Arg.varWith "f" <| Type.function [ config.keyType, vVar ] (Type.var "b"))
                (Elm.Arg.var "dict")
                (\f ->
                    unwrap
                        (ManualDict.map (\a b -> Elm.apply f [ a, Elm.withType vVar b ]))
                        >> wrapWith (Type.var "b")
                )
                |> Elm.declaration "map"
                |> Elm.withDocumentation ("Applies a given function to each entry in a " ++ dictTypeName ++ ". The function receives both key and value of each entry as its arguments.")
                |> Elm.expose

        filter =
            Elm.fn2
                (Elm.Arg.var "f")
                (Elm.Arg.var "dict")
                (\f ->
                    wrapped (ManualDict.call_.filter f)
                )
                |> Elm.declaration "filter"
                |> Elm.withDocumentation ("Filters entries in a " ++ dictTypeName ++ " via the given function. The function receives both key and value of each entry as its arguments.")
                |> Elm.expose

        remove =
            Elm.fn2
                (Elm.Arg.var "k")
                (Elm.Arg.var "dict")
                (\k -> wrapped (ManualDict.remove config.toComparable k))
                |> Elm.declaration "remove"
                |> Elm.withDocumentation ("Remove an entry from the " ++ dictTypeName ++ " by " ++ config.keyTypeName ++ ".")
                |> Elm.expose
    in
    Elm.fileWith [ dictTypeName ]
        { docs = ""
        , aliases = []
        }
        [ Elm.group [ customType ]
        , Elm.group [ empty ]
        , Elm.group
            [ fromList
            , toList
            ]
        , Elm.group
            [ insert
            , remove
            , update
            , filter
            , map
            ]
        , Elm.group
            [ values
            , foldl
            , get
            ]
        ]
