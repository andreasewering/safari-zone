module Generate exposing (main)

{-| Entry point for code generation. Register files to be generated here
-}

import Elm.Annotation as Type
import Gen.AreaId as AreaId
import Gen.AudioTrackNumber as AudioTrackNumber
import Gen.ChatChannel as ChatChannel
import Gen.CodeGen.Generate as Generate
import Gen.GroupId as GroupId
import Gen.Item as Item
import Gen.ItemId as ItemId
import Gen.MapId as MapId
import Gen.PokemonNumber as PokemonNumber
import Gen.TrainerNumber as TrainerNumber
import Gen.UserId as UserId
import Generators.CustomDict


main : Program {} () ()
main =
    Generate.run
        [ Generators.CustomDict.generateFile
            { keyTypeName = "MapId"
            , keyType = MapId.annotation_.mapId
            , comparableType = Type.tuple Type.int Type.int
            , toComparable = MapId.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "UserId"
            , keyType = UserId.annotation_.userId
            , comparableType = Type.tuple Type.int Type.int
            , toComparable = UserId.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "ItemId"
            , keyType = ItemId.annotation_.itemId
            , comparableType = Type.tuple Type.int Type.int
            , toComparable = ItemId.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "GroupId"
            , keyType = GroupId.annotation_.groupId
            , comparableType = Type.tuple Type.int Type.int
            , toComparable = GroupId.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "AudioTrackNumber"
            , keyType = AudioTrackNumber.annotation_.audioTrackNumber
            , comparableType = Type.int
            , toComparable = AudioTrackNumber.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "TrainerNumber"
            , keyType = TrainerNumber.annotation_.trainerNumber
            , comparableType = Type.int
            , toComparable = TrainerNumber.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "Item"
            , keyType = Item.annotation_.item
            , comparableType = Type.int
            , toComparable = Item.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "ChatChannel"
            , keyType = ChatChannel.annotation_.chatChannel
            , comparableType = Type.tuple Type.int (Type.tuple Type.int Type.int)
            , toComparable = ChatChannel.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "AreaId"
            , keyType = AreaId.annotation_.areaId
            , comparableType = Type.tuple Type.int Type.int
            , toComparable = AreaId.toComparable
            }
        , Generators.CustomDict.generateFile
            { keyTypeName = "PokemonNumber"
            , keyType = PokemonNumber.annotation_.pokemonNumber
            , comparableType = Type.int
            , toComparable = PokemonNumber.toComparable
            }
        ]
