# Codegen

This folder is responsible for generating repetitive Elm code.
This is mostly done to overcome limitations of the Elm type system.
For example, Dicts and Sets in Elm can only contain `comparable` types (i.e. you can use `>` and `<`),
which makes sense, but sadly custom types do not inherit this trait.

We use [elm-codegen](https://package.elm-lang.org/packages/mdgriffith/elm-codegen/latest/) to do the heavy lifting.

TODO: explain CLI commands and how to add code here
