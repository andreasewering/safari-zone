import path from 'path';
import gzipPlugin from 'rollup-plugin-gzip';
import { promisify } from 'node:util';
import { defineConfig, Plugin } from 'vite';
import elmPlugin from 'vite-plugin-elm';
import { travelmAgencyPlugin } from 'vite-plugin-travelm-agency';
import { brotliCompress } from 'node:zlib';
import packageJson from './package.json' assert { type: 'json' };
import postcss from './postcss.config.js';
import { staticI18nPlugin } from './vite/i18nPlugin.js';
import { linkPlugin } from './vite/linkPlugin.js';
import { multiSpa } from './vite/multiSpaPlugin.js';
import translations from './src/static-translations.js';

const isProduction = process.env.NODE_ENV === 'production';
const isIntegrationTest = process.env.NODE_ENV === 'int-test';

export default defineConfig({
  root: 'src',
  publicDir: path.resolve(__dirname, 'public'),
  define: {
    __DEV__: !isProduction && !isIntegrationTest,
    __VERSION__: JSON.stringify(packageJson.version),
  },
  resolve: {
    alias: {
      'Main.elm': path.resolve(__dirname, 'src/main/Main.elm'),
      'Admin.elm': path.resolve(__dirname, 'src/admin/Admin.elm'),
    },
  },
  build: {
    outDir: '../dist',
    emptyOutDir: true,
    rollupOptions: {
      input: {
        index: path.resolve(__dirname, 'src', 'index.html'),
        admin: path.resolve(__dirname, 'src', 'admin', 'index.html'),
        login: path.resolve(__dirname, 'src', 'login', 'index.html'),
        signup: path.resolve(__dirname, 'src', 'signup', 'index.html'),
        verification: path.resolve(
          __dirname,
          'src',
          'verification',
          'index.html'
        ),
      },
      output: {
        assetFileNames: (assetInfo) => {
          return assetInfo.name!;
        },
      },
    },
  },
  css: {
    postcss,
  },
  plugins: [
    multiSpa({
      '/admin': 'src/admin/index.html',
    }),
    staticI18nPlugin({
      paths: [
        'login/index.html',
        'signup/index.html',
        'verification/index.html',
        'index.html',
      ],
      defaultLanguage: 'en',
      translations: translations,
    }),
    linkPlugin(),
    // Handle internationalization in Elm
    travelmAgencyPlugin({
      elmPath: 'src/generated/Admin/Translations.elm',
      generatorMode: 'dynamic',
      translationDir: 'src/admin/translations',
      jsonPath: 'translations/admin',
    }),
    travelmAgencyPlugin({
      elmPath: 'src/generated/Main/Translations.elm',
      generatorMode: 'dynamic',
      translationDir: 'src/main/translations',
      jsonPath: 'translations/main',
    }),
    // Convert Elm files to JS
    elmPlugin({
      optimize: isProduction,
      debug: !isProduction && !isIntegrationTest,
    }),
    // Compress files with gzip and save as .gz
    gzipPlugin() as Plugin,
    // Compress files with brotli and save as .br
    gzipPlugin({
      customCompression: promisify(brotliCompress),
      fileName: '.br',
    }) as Plugin,
  ],
  server: {
    host: '0.0.0.0',
    hmr: {
      clientPort: 3443,
      path: '/hmr',
    },
  },
});
