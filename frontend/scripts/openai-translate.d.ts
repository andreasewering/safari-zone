export interface ExecuteRequestParams {
  sourceLanguage: string;
  targetLanguage: string;
  input: string;
}

export function executeRequest(params: ExecuteRequestParams): Promise<string>;
