import dotenv from 'dotenv';

dotenv.config({ path: '../.env.secret' });

export async function executeRequest({
  sourceLanguage,
  targetLanguage,
  input,
}) {
  const response = await fetch('https://api.openai.com/v1/chat/completions', {
    method: 'POST',
    headers: {
      authorization: `Bearer ${process.env.OPENAI_API_KEY}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      model: 'gpt-4o',
      temperature: 0.3,
      max_tokens: input.length * 2,
      messages: [
        { role: 'system', content: 'You are a helpful assistant.' },
        {
          role: 'user',
          content: `Translate the following text from ${sourceLanguage} to ${targetLanguage}: \`\`\`\n${input}\n\`\`\`\n.Your response should only contain the translation, not the languages and no extra text. Do not include the backtick delimiter and the start and end. Use informal speech.`,
        },
      ],
    }),
  });
  const json = await response.json();

  if (!json.choices || !json.choices[0]) {
    throw new Error(
      `Failed to translate via openai API: ${JSON.stringify(json)}`
    );
  }

  return json.choices[0].message.content;
}
