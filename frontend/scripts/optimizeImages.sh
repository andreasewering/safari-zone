# Usage: Call optimizeImages.sh from anywhere.
# Behaviour: Generates a .webp image for each image in the `public/images` folder.
# You need cwebp and parallel installed on your system

# This should be the frontend root directory, regardless of from where the script was called.
cd "${0%/*}/../"

DEST_DIR="${PWD}/public/images"

mkdir -p $DEST_DIR

cd public/images

# create necessary subdirectories in dist directory
find . -name '*.png' | sed -r 's|/[^/]+$||' | sort -u | while read IMAGE_SUB_DIR; do
  echo "Creating directory $DEST_DIR/$IMAGE_SUB_DIR"
  mkdir -p "$DEST_DIR/$IMAGE_SUB_DIR"
done

# run cwebp on all images in parallel
time find . -name "*.png" | parallel -eta cwebp {} -o $DEST_DIR/{}.webp
