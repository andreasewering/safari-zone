// Usage: echo "my text" | node scripts/translate.js en de
//
// Sends a request to openai to translate your text from source to target language.
// Prints out the response translation using stdout.
import { executeRequest } from './openai-translate';

const [, , sourceLanguage, targetLanguage] = process.argv;
process.stderr.write(
  `Translating from ${sourceLanguage} to ${targetLanguage}\n`
);

let input = '';

// Listen for data events from stdin
process.stdin.on('data', (chunk) => {
  input += chunk;
});

// Listen for the end event to know when to stop reading
process.stdin.on('end', async () => {
  // Now input contains the complete data from stdin
  const translation = await executeRequest({
    sourceLanguage,
    targetLanguage,
    input,
  });
  console.log(translation);
});

// Ensure the program keeps listening until stdin is closed
process.stdin.resume();
