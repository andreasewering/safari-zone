import cp from 'node:child_process';
import { readFileSync, writeFileSync } from 'node:fs';
import path from 'node:path';
import * as url from 'node:url';
import { promisify } from 'node:util';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

const exec = promisify(cp.exec);

const adminOutputFile = path.resolve(__dirname, '../src/generated/admin.d.ts');
await exec(
  `elm-ts-interop --definition-dir admin/Admin --definition-module Admin.InteropDefinitions -e Admin --output ${adminOutputFile}`
);
// modify generated file
const adminOutput = readFileSync(adminOutputFile, { encoding: 'utf-8' })
  .replace('as namespace Elm', 'as namespace ElmAdmin')
  .replace('export { Elm }', 'export { ElmAdmin as Elm }');
writeFileSync(adminOutputFile, adminOutput);

await exec(
  'elm-ts-interop --definition-dir src/main --output src/generated/index.d.ts'
);
