source ../.env.secret
curl https://api.openai.com/v1/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d '{
    "model": "gpt-4o",
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "Translate the following text into es, fr, de, and return the translations in the same order, seperated by newlines: \"Evergrande City\". Your response should only contain the translations, not the languages and no extra text."
      }
    ],
    "max_tokens": 150,
    "temperature": 0.3
  }'
