/**
 * Example invocations
 *
 * // Adds "areaEditorHeadline = Area Editor" to admin/translations/messages.en.ftl
 * // and uses OpenAI api to translate into the other languages.
 * // Then adds the corresponding translations in admin/translations/messages.*.ftl.
 * npm run translate -- add areaEditorHeadline --en 'Area Editor' --app admin
 *
 * // Removes "settingsUserIdLabel = ..." from all translation files if present
 * // Prints out the names of the files that were changed.
 * npm run translate -- del settingsUserIdLabel
 *
 * // Changes "dexOverviewTitle = ..." to "dexOverviewTitle = My changed title"
 * // in whatever application it was present in
 * npm run translate -- edit dexOverviewTitle --en 'My changed title'
 *
 * // Changes "dexOverviewTitle = ..." to "dexOverviewTitle = My changed title"
 * // and uses OpenAI api to translate into the other languages.
 * // Then changes the corresponding translations in [*]/translations/messages.*.ftl.
 * npm run translate -- edit dexOverviewTitle --en "My changed title" --auto
 */
import fs from 'node:fs/promises';
import { executeRequest } from './openai-translate.js';

const [, , ...argsv] = process.argv;

type Args = {
  task: 'edit' | 'del' | 'add';
  key: string;
  renamedKey: string | undefined;
  applications: [string, ...string[]];
  autoTranslate: boolean;
  translations: { [K: string]: string | undefined };
};

const args = validateArgs(parseArgs(argsv));
run(args);

async function run(args: Args) {
  switch (args.task) {
    case 'add': {
      if (args.autoTranslate) {
        const [[sourceLanguage, input]] = Object.entries(args.translations) as [
          [string, string],
        ];
        const languages = await determineLanguages(args.applications[0]);
        const translations = await autoTranslate(
          sourceLanguage,
          input,
          languages.filter((lang) => lang !== sourceLanguage)
        );
        args.translations = { ...args.translations, ...translations };
      }
      console.log(`Adding the following translations to the applications ${args.applications}:
${Object.entries(args.translations)
  .map(([k, v]) => `    ${k} = ${v}`)
  .join('\n')}`);

      await iterate(args, addTranslation(args.key));
      return;
    }

    case 'edit': {
      const [firstTranslation] = Object.entries(args.translations) as [
        [string, string],
      ];
      if ((args.autoTranslate || args.renamedKey) && firstTranslation) {
        const [sourceLanguage, input] = firstTranslation;
        const languages = await determineLanguages(args.applications[0]);
        const translations = await autoTranslate(
          sourceLanguage,
          input,
          languages.filter((lang) => lang !== sourceLanguage)
        );
        args.translations = { ...args.translations, ...translations };
      }
      if (!firstTranslation && args.renamedKey) {
        const languages = await determineLanguages(args.applications[0]);
        languages.forEach((language) => {
          args.translations[language] = undefined;
        });
      }
      console.log(`Updating the following translations in the applications ${args.applications}:
${Object.entries(args.translations)
  .map(([k, v]) => `    ${k} = ${v}`)
  .join('\n')}`);

      await iterate(args, editTranslation(args.key, args.renamedKey));
      return;
    }

    case 'del': {
      const languages = await determineLanguages(args.applications[0]);
      languages.forEach((language) => {
        args.translations[language] = undefined;
      });
      console.log(
        `Deleting key ${args.key} for applications ${args.applications} and languages ${Object.keys(args.translations)}`
      );
      await iterate(args, deleteTranslation(args.key));
      return;
    }
  }
}

async function autoTranslate(
  sourceLanguage: string,
  input: string,
  targetLanguages: string[]
) {
  const result: Record<string, string> = {};
  const requests = targetLanguages.map(async (targetLanguage) => {
    const response = await executeRequest({
      sourceLanguage,
      targetLanguage,
      input,
    });
    result[targetLanguage] = response;
  });
  await Promise.all(requests);
  return result;
}

async function determineLanguages(app: string) {
  const filenames = await fs.readdir(`src/${app}/translations`);
  return filenames
    .filter((filename) => filename.endsWith('.ftl'))
    .map((filename) => {
      const segments = filename.split('.');
      return segments[segments.length - 2]!;
    });
}

async function iterate(
  args: Args,
  taskFn: (fileContents: string, value: string | undefined) => string
) {
  const results = await Promise.all(
    args.applications
      .flatMap((app) =>
        Object.entries(args.translations).map(([key, value]) => [
          app,
          key,
          value,
        ])
      )
      .map(async ([app, key, value]) => {
        const filePath = `src/${app}/translations/messages.${key}.ftl`;
        const fileContents = await fs.readFile(filePath, { encoding: 'utf-8' });
        const newFileContents = taskFn(fileContents, value);
        const hasChanged = fileContents !== newFileContents;
        if (hasChanged) {
          await fs.writeFile(filePath, newFileContents, { encoding: 'utf-8' });
        }
        return [hasChanged, filePath];
      })
  );
  results
    .filter(([hasChanged]) => hasChanged)
    .forEach(([, filePath]) => {
      console.log(`Changed: ${filePath}`);
    });
}

function addTranslation(
  key: string
): (fileContents: string, value: string | undefined) => string {
  return (fileContents, value) => {
    const lines = fileContents.split('\n');
    let positionToInsert = lines.length; // Default position at the end

    for (let i = 0; i < lines.length; i++) {
      const line = lines[i]?.trim();
      if (!line || line.startsWith('#')) {
        continue; // Ignore empty lines and comments
      }

      const [currentKey, _currentValue] = line
        .split('=')
        .map((str) => str.trim());
      if (currentKey && currentKey > key) {
        positionToInsert = i;
        break;
      }
    }

    // Compose the new line
    const newLine = `${key} = ${value}`;

    // Insert the new line at the calculated position
    lines.splice(positionToInsert, 0, newLine);

    return lines.join('\n');
  };
}

function editTranslation(
  key: string,
  renamedKey: string | undefined
): (fileContents: string, value: string | undefined) => string {
  return (fileContents, value) =>
    fileContents
      .split('\n')
      .map((line) => {
        const [k, v] = line.split('=');
        if (k?.trimEnd() === key && value) {
          return `${renamedKey ?? key} = ${value}`;
        }
        if (k?.trimEnd() === key && !value) {
          return `${renamedKey} = ${v}`;
        }
        return line;
      })
      .join('\n');
}

function deleteTranslation(key: string): (fileContents: string) => string {
  return (fileContents) =>
    fileContents
      .split('\n')
      .filter((line) => {
        const [k] = line.split('=');
        return k?.trimEnd() !== key;
      })
      .join('\n');
}

function parseArgs(args: string[]): Partial<Args> {
  const [firstArg, ...rest] = args;
  switch (firstArg) {
    case undefined:
      return {};
    case 'edit': {
      return mergeArgs({ task: 'edit' }, parseArgs(rest));
    }
    case 'del': {
      return mergeArgs({ task: 'del' }, parseArgs(rest));
    }
    case 'add': {
      return mergeArgs({ task: 'add' }, parseArgs(rest));
    }
    case '--auto':
      return { autoTranslate: true };
    case '--app': {
      const [application, ...rest2] = rest;
      if (!application || application.startsWith('-')) {
        throw new Error(
          'Did not specify application. Pass "main" or "admin" after the "--app" flag'
        );
      }
      return mergeArgs({ applications: [application] }, parseArgs(rest2));
    }
    case '--rename': {
      const [renamedKey, ...rest2] = rest;
      if (!renamedKey || renamedKey.startsWith('-')) {
        throw new Error(
          'Did not specify rename. Pass "your renamed key" after the "--rename" flag'
        );
      }
      return mergeArgs({ renamedKey }, parseArgs(rest2));
    }
    default: {
      if (!firstArg.startsWith('--')) {
        return mergeArgs({ key: firstArg }, parseArgs(rest));
      }
      const language = firstArg.replace('--', '');
      const [value, ...rest2] = rest;
      if (!value || value.startsWith('-')) {
        throw new Error(
          `Did not specify value for language ${language}. Pass 'your translation' after the "--${language}" flag`
        );
      }
      return mergeArgs(
        { translations: { [language]: value } },
        parseArgs(rest2)
      );
    }
  }
}

function validateArgs(args: Partial<Args>): Args {
  if (!args.task) {
    throw new Error(
      'Please specify task through positional argument: one of [add, del, edit].'
    );
  }
  if (!args.key) {
    throw new Error(
      'Please specify key to work on through positional argument.'
    );
  }
  if (!args.translations && args.task !== 'del') {
    throw new Error(
      "Please specify at least one translation (e.g. --en 'my text'"
    );
  }

  const autoTranslate = args.autoTranslate ?? args.task !== 'edit';

  const applications =
    !args.applications || args.applications.length === 0
      ? (['admin', 'main'] satisfies [string, ...string[]])
      : (args.applications satisfies [string, ...string[]]);
  return {
    task: args.task,
    key: args.key,
    renamedKey: args.renamedKey,
    applications,
    autoTranslate,
    translations: args.translations ?? {},
  };
}

function mergeArgs(a1: Partial<Args>, a2: Partial<Args>): Partial<Args> {
  if (a1.task && a2.task) {
    throw new Error(
      `Already specified task ${a1.task}, but passed additional task ${a2.task}`
    );
  }
  const applications =
    a1.applications && a2.applications
      ? ([...a1.applications, ...a2.applications] satisfies [
          string,
          ...string[],
        ])
      : a1.applications
        ? a1.applications
        : a2.applications
          ? a2.applications
          : undefined;

  return {
    ...a1,
    ...a2,
    applications,
    translations: { ...(a1.translations ?? {}), ...(a2.translations ?? {}) },
  };
}
