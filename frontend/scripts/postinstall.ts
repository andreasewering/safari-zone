/**
 * Generate code from .proto files.
 * We need this to communicate in a type-safe way with the backend.
 * We generate Typescript code (for integrative tests and non-elm-based pages)
 * and Elm Code (for our main application).
 */

import cp from 'node:child_process';
import path from 'node:path';
import * as url from 'node:url';
import { promisify } from 'node:util';
import fs, { mkdirSync } from 'node:fs';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

const exec = promisify(cp.exec);

const globalProtoPath = path.join(__dirname, '..', '..', 'proto');

const globalProtoFiles = [
  'arceus.proto',
  'gardevoir.proto',
  'chatot.proto',
  'direction.proto',
  'error_code.proto',
  'item.proto',
  'kangaskhan.proto',
  'smeargle.proto',
  'tag.proto',
  'translated.proto',
];

mkdirSync('src/generated', { recursive: true });
console.log('[postinstall] Created src/generated folder.');
console.log('[postinstall] Generating code from .proto files.');
const options =
  '--elm_out=src/generated --elm_opt=grpcDevTools --ts_out=src/generated --ts_opt=optimize_code_size';

exec(
  `protoc ${options} --proto_path=${globalProtoPath} ${globalProtoFiles.join(
    ' '
  )}`
)
  .then(() => {
    console.log('[postinstall] Successfully generated code from .proto files.');
    process.exit(0);
  })
  .catch((err: unknown) => {
    console.error(err);
    process.exit(1);
  });

// Write initial empty http interceptor config
fs.writeFileSync(
  path.join(__dirname, '..', 'src', 'dev', 'custom-rules.ts'),
  `import { HttpInterceptorRule } from './http-interceptor-rule';

export const customHttpInterceptorRules: HttpInterceptorRule[] = [];
`
);
