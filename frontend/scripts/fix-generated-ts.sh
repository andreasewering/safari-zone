#!/bin/bash

SED_INPLACE="-i"
if [[ "$OSTYPE" == "darwin"* ]]; then
  SED_INPLACE="-i ''"
fi

for file in src/generated/*.client.ts; do
    sed $SED_INPLACE 's/method,/method!,/g' "$file"
done
