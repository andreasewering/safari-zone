import { Config } from 'tailwindcss';

/**
 * Config for small standalone pages in need of fast render times.
 */
export default {
  content: [
    './src/components/**/*.ts',
    './src/login/index.html',
    './src/signup/index.html',
    './src/verification/index.html',
    './src/verification/verify.js',
  ],
  darkMode: ['selector', '[theme="dark"]'],
  theme: {
    extend: {
      animation: {
        'zoom-in': 'zoom-in 1s ease-in-out',
      },
      keyframes: {
        'zoom-in': {
          from: {
            opacity: '0',
          },
          to: {
            opacity: '1',
          },
        },
      },
    },
  },
  plugins: [],
} satisfies Config;
