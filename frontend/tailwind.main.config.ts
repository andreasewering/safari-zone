import { Config } from 'tailwindcss';
import colors from 'tailwindcss/colors.js';
import plugin from 'tailwindcss/plugin.js';

/**
 * Config for main application. The config for other smaller pages is seperate
 * in `tailwind.minimal.config.ts`, reason being smaller bundle sizes where it matters.
 * @type {import('tailwindcss').Config}
 * */
export default {
  plugins: [
    plugin(({ addVariant }) => {
      addVariant('bgd', '.bgd &');
      addVariant('bgl', '.bgl &');
      addVariant('motion-reduced', '[motion="reduced"] &');
    }),
  ],
  content: [
    './src/index.html',
    './src/main/**/*.elm',
    './src/lib/**/*.elm',
    './src/components/**/*.ts',
  ],
  darkMode: ['selector', '[theme="dark"]'],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      red: colors.red,
      amber: colors.amber,
      yellow: colors.yellow,
      green: colors.green,
      sky: colors.sky,
      pink: colors.pink,
    },
    extend: {
      animation: {
        'zoom-in': 'zoom-in 1s ease-in-out',
      },
      keyframes: {
        'zoom-in': {
          from: {
            opacity: '0',
          },
          to: {
            opacity: '1',
          },
        },
      },
    },
  },
} satisfies Config;
