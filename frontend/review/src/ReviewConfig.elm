module ReviewConfig exposing (config)

{-| Do not rename the ReviewConfig module or the config function, because
`elm-review` will look for these.

To add packages that contain rules, add them to this review project using

    `elm install author/packagename`

when inside the directory containing this file.

-}

import NoUnsafePorts
import NoUnused.Dependencies
import NoUnused.Exports
import NoUnused.Modules
import NoUnused.Parameters
import NoUnused.Patterns
import NoUnused.Variables
import NoUnusedPorts
import NoViolationOfModuleLayerDependency exposing (ModuleLayerDependency(..), ModuleLayer(..))
import Review.Rule exposing (Rule)
import TailwindCss.ClassOrder exposing (classOrder, classProps)
import TailwindCss.ConsistentClassOrder
import TailwindCss.NoCssConflict
import TailwindCss.NoUnknownClasses
import UseMemoizedLazyLambda


config : List Rule
config =
    [ NoUnused.Dependencies.rule
    , NoUnused.Modules.rule
    , NoUnused.Parameters.rule
    , NoUnused.Patterns.rule
    , NoUnused.Variables.rule
    , NoUnsafePorts.rule NoUnsafePorts.onlyIncomingPorts
    , NoUnusedPorts.rule
    , TailwindCss.ConsistentClassOrder.rule (TailwindCss.ConsistentClassOrder.defaultOptions { order = classOrder })
    , TailwindCss.NoUnknownClasses.rule (TailwindCss.NoUnknownClasses.defaultOptions { order = classOrder })
    , UseMemoizedLazyLambda.rule
    -- We want to be able to guarantee that our multiple Elm applications
    -- can share code but also do not import from each others application code.
    -- The idea here is that any imports between `Main.*` <-> `Admin.*` modules are forbidden
    -- Imports from e.g. `Main.*` -> `SomeSharedFile` are fine,
    -- but `SomeSharedFile` -> `Main.*` is not fine (in that case, 
    -- SomeSharedFile should move some or all of its functionality into the `Main` directory)
    , NoViolationOfModuleLayerDependency.rule adminDoesNotDependOnMain
    , NoViolationOfModuleLayerDependency.rule mainDoesNotDependOnAdmin
    ]

adminDoesNotDependOnMain : ModuleLayerDependency
adminDoesNotDependOnMain = 
    ModuleLayerDependency [ DefaultLayer, adminApplication, mainApplication ]

mainDoesNotDependOnAdmin : ModuleLayerDependency
mainDoesNotDependOnAdmin = 
    ModuleLayerDependency [ DefaultLayer, mainApplication, adminApplication ]

adminApplication : ModuleLayer
adminApplication = ModuleLayer [["Admin"]]

mainApplication : ModuleLayer
mainApplication = ModuleLayer [["Main"]]
