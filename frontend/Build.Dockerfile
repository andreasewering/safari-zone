FROM node:22.10-alpine3.20
# Install basic unix packages, webp for image compression, protoc and git
RUN apk update && apk add --no-cache zip curl gzip libwebp-tools parallel git
ARG PROTOC_VERSION=25.3
ARG ELM_VERSION=0.19.1
# Install protoc
RUN curl -LO https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/protoc-${PROTOC_VERSION}-linux-x86_64.zip \
  && unzip protoc-${PROTOC_VERSION}-linux-x86_64.zip -d /usr/local "bin/protoc" "include/*" \
  && rm -f protoc-${PROTOC_VERSION}-linux-x86_64.zip
# Install Elm
RUN curl -L -o elm.gz https://github.com/elm/compiler/releases/download/${ELM_VERSION}/binary-for-linux-64-bit.gz \
  && gunzip elm.gz \
  && chmod +x elm \
  && mv elm /usr/local/bin/
ENTRYPOINT /bin/sh
