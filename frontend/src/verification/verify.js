(async () => {
  const params = new URLSearchParams(window.location.search);
  const verification_id = params.get('verification_id');
  const user_id = params.get('user_id');

  const messageEl = document.getElementById('message');
  const { success, conflict, error, servererror, expired, verifying } =
    window.translations;

  const handle404 = () => {
    messageEl.innerText = error;
    messageEl.classList.add('bg-red-400', 'dark:bg-red-600');
  };

  if (!user_id || !verification_id) {
    handle404();
  } else {
    messageEl.innerText = verifying;
    const response = await fetch(
      `/gardevoir/verify/${user_id}/${verification_id}`,
      {
        method: 'POST',
      }
    );
    switch (response.status) {
      case 401: {
        // verification was correct but expired
        // -> backend resends verification mail
        messageEl.innerText = expired;
        messageEl.classList.add('bg-yellow-400', 'dark:bg-yellow-600');
        break;
      }
      case 404: {
        // verification or user id did not match up
        handle404();
        break;
      }
      case 409: {
        // user was already verified
        messageEl.innerText = conflict;
        messageEl.classList.add('bg-yellow-400', 'dark:bg-yellow-600');
        break;
      }
      case 200: {
        // verification successful
        messageEl.innerText = success;
        messageEl.classList.add('bg-green-400', 'dark:bg-green-600');
        break;
      }
      default: {
        // something went wrong
        messageEl.innerText = servererror;
        messageEl.classList.add('bg-red-400', 'dark:bg-red-600');
      }
    }
  }
})();
