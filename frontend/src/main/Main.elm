module Main exposing (main)

import Array
import AudioTrackNumberDict
import Auth
import BoundedDict
import Browser exposing (Document)
import Browser.Navigation as Nav
import Html
import Interop.Flags
import ItemDict
import Json.Decode as D
import Main.Chat.Model
import Main.Contacts.Detail as ContactsDetail
import Main.Contacts.Overview as ContactsOverview
import Main.Dex.Overview
import Main.Language as Language
import Main.MapSelect
import Main.Model exposing (Model)
import Main.Msg exposing (Msg(..))
import Main.RequestManager as RequestManager
import Main.Route
import Main.Safari.Model
import Main.Safari.Overworld.Transition
import Main.Settings.Overview
import Main.Settings.QrCodeView
import Main.Shared as Shared
import Main.Subscriptions as Subscriptions
import Main.TrainerSelect.Detail
import Main.TrainerSelect.Model
import Main.Translations as Translations
import Main.Update as Update exposing (initializePage)
import Main.View as View
import PokemonNumberDict
import Shared.Background as Background
import Shared.Types.Items as Items
import Shared.UpdateUtil as UpdateUtil
import Shared.View.Theme as Theme
import Task
import Time
import TrainerNumberDict
import Url exposing (Url)
import UserIdDict


init : D.Value -> Url -> Nav.Key -> ( MainModel, Cmd Msg )
init jsFlags url key =
    Interop.Flags.read jsFlags
        |> Result.map
            (\flags ->
                let
                    language =
                        { browser = Translations.languageFromString flags.language |> Maybe.withDefault Translations.En
                        , preferred = flags.preferredLanguage |> Maybe.andThen Translations.languageFromString
                        }

                    route =
                        Main.Route.toRoute url

                    auth =
                        Auth.manageSingle flags.auth flags.now

                    chats =
                        Main.Chat.Model.init flags.now
                            |> (Auth.getMapIdMaybe auth |> Maybe.map Main.Chat.Model.switchMap |> Maybe.withDefault identity)

                    timezone =
                        Time.utc

                    background =
                        Background.fromTime flags.now Time.utc

                    i18n =
                        Translations.init { intl = flags.intl, lang = Language.getActive language, path = "/translations/main" }

                    initialModel : Model
                    initialModel =
                        { contactsModel = ContactsOverview.init
                        , groupDetailModel = ContactsDetail.init
                        , dexModel = Main.Dex.Overview.init
                        , safariModel = Main.Safari.Model.init
                        , settingsModel = Main.Settings.Overview.init
                        , qrCodeViewModel = Main.Settings.QrCodeView.init
                        , trainerSelectModel = Main.TrainerSelect.Model.init
                        , trainerDetailModel = Main.TrainerSelect.Detail.init
                        , mapSelectModel = Main.MapSelect.init
                        , -- I18n
                          i18n = i18n
                        , language = language

                        -- Data from the server
                        , pokemonDataDict = PokemonNumberDict.empty
                        , dexEntries = Array.empty
                        , audioConfig = { tracks = AudioTrackNumberDict.empty, onPokemonObtained = Nothing }
                        , friends = []
                        , friendRequests = { incoming = [], outgoing = [] }
                        , groups = []
                        , groupDetails = BoundedDict.withCapacity 40
                        , areas = BoundedDict.withCapacity 4
                        , moveRules = BoundedDict.withCapacity 4
                        , collectedItems = Items.emptyCollectedItems
                        , animationConfig = Array.empty
                        , animationTileDict = BoundedDict.withCapacity 4
                        , trainerMetadata = TrainerNumberDict.empty
                        , startTiles = []
                        , starterPokemon = []
                        , dexDetails = BoundedDict.withCapacity 10
                        , users = UserIdDict.empty

                        -- Image Textures for WebGL rendering
                        , textures =
                            { tileset = Nothing
                            , tilemaps = BoundedDict.withCapacity 4
                            , pokemonTextures = PokemonNumberDict.empty
                            , trainerTextures = TrainerNumberDict.empty
                            , itemTextures = ItemDict.empty
                            }

                        -- Client side data and configuration
                        , viewport =
                            { width = flags.x
                            , height = flags.y
                            }
                        , colorThemeName = flags.preferredColorTheme |> Maybe.andThen Theme.fromString |> Maybe.withDefault Theme.Light
                        , reducedMotion = flags.reducedMotion
                        , audioIsActive = flags.audioOn
                        , timezone = timezone
                        , version = flags.version
                        , background = background
                        , htmlContext =
                            { i18n = i18n
                            , timezone = timezone
                            , background = Background.toImage background
                            , disabled = False
                            , fontSize = flags.fontSize
                            }

                        -- Authentication
                        , auth = auth

                        -- User Data
                        , userData =
                            { position = Nothing
                            , trainerSprite = Nothing
                            , displayName = ""
                            , partnerPokemon = Nothing
                            }

                        -- Chatting
                        , chats = chats

                        -- Routing
                        , key = key
                        , route = route
                        , isNavigationOpen = False
                        , mapTransitionTimeline = Main.Safari.Overworld.Transition.initTimeline
                        , previousRoute = Nothing

                        -- Error Handling
                        , requestManager = RequestManager.init
                        }

                    withAudioRequest =
                        if initialModel.audioIsActive then
                            (::) (RequestManager.GetAudioTracks {})

                        else
                            identity

                    ( model, cmds ) =
                        UpdateUtil.chain
                            [ initializePage initialModel.route
                            , registerRequests (withAudioRequest [ RequestManager.GetUserData {} ])
                            ]
                            initialModel

                    getI18n =
                        Translations.loadMessages (Shared.GotI18n >> SharedMessage) model.i18n

                    getTimezone =
                        Time.here |> Task.perform GotTimezone
                in
                ( model
                , Cmd.batch [ cmds, getI18n, getTimezone ]
                )
            )
        |> orNoCmd


registerRequests : List RequestManager.ApiCall -> Model -> ( Model, Cmd Msg )
registerRequests calls model =
    let
        ( requestManager, cmds ) =
            RequestManager.register calls model.requestManager
    in
    ( { model | requestManager = requestManager }, cmds |> Cmd.map (Shared.RequestManagerMessage >> SharedMessage) )


orNoCmd : Result x ( a, Cmd msg ) -> ( Result x a, Cmd msg )
orNoCmd r =
    case r of
        Ok ( a, cmd ) ->
            ( Ok a, cmd )

        Err err ->
            ( Err err, Cmd.none )


type alias MainModel =
    Result D.Error Model


update : Msg -> MainModel -> ( MainModel, Cmd Msg )
update msg m =
    case m of
        Err _ ->
            ( m, Cmd.none )

        Ok model ->
            Update.update msg model |> Tuple.mapFirst Ok


subscriptions : MainModel -> Sub Msg
subscriptions m =
    case m of
        Err _ ->
            Sub.none

        Ok model ->
            Subscriptions.subscriptions model


view : MainModel -> Document Msg
view m =
    case m of
        Err err ->
            { title = "Something went wrong", body = [ Html.text <| D.errorToString err ] }

        Ok model ->
            View.view model


main : Program D.Value MainModel Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlRequest = OnUrlRequest
        , onUrlChange = OnUrlChange
        }
