module UserData exposing (UserData, isComplete, isEmpty)

import Domain.Position exposing (Position)
import PokemonNumber exposing (PokemonNumber)
import TrainerNumber exposing (TrainerNumber)


type alias UserData =
    { trainerSprite : Maybe TrainerNumber
    , position : Maybe Position
    , partnerPokemon : Maybe PartnerPokemon
    , displayName : String
    }

type alias PartnerPokemon =
    { pokemonNumber : PokemonNumber, isShiny : Bool }


type alias CompleteUserData =
    { trainerSprite : TrainerNumber, position : Position, partnerPokemon : PartnerPokemon, displayName : String }


isEmpty : UserData -> Bool
isEmpty { trainerSprite, position, partnerPokemon, displayName } =
    trainerSprite == Nothing && position == Nothing && partnerPokemon == Nothing && String.isEmpty displayName


isComplete : UserData -> Maybe CompleteUserData
isComplete { trainerSprite, position, partnerPokemon, displayName } =
    Maybe.map4
        CompleteUserData
        trainerSprite
        position
        partnerPokemon
        (if String.isEmpty displayName then
            Nothing

         else
            Just displayName
        )
