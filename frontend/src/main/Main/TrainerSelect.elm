module Main.TrainerSelect exposing (view)

import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class)
import Main.ErrorHandling as ErrorHandling
import Main.Html exposing (Document)
import Main.Language as Language
import Main.Page404 as Page404
import Main.RequestManager as RequestManager
import Main.Shared as Shared
import Main.TrainerSelect.Detail as Detail
import Main.TrainerSelect.Model exposing (PageModel)
import Main.TrainerSelect.Overview as Overview
import Main.TrainerSelect.Route as TrainerSelect
import Main.Translations as Translations
import TrainerNumber exposing (TrainerNumber)
import TrainerNumberDict


type alias Context msg =
    { route : TrainerSelect.Route
    , liftOverview : Overview.Msg -> msg
    , liftDetail : TrainerNumber -> Detail.Msg -> msg
    , liftShared : Shared.Msg -> msg
    }


type alias Model m =
    Shared.Session
        { m
            | trainerSelectModel : PageModel
            , trainerDetailModel : Detail.PageModel
        }


view :
    Context msg
    -> Model m
    -> Document msg
view ctx model =
    let
        canvasWidth =
            min (floor <| toFloat model.viewport.width * 0.8) (45 * model.htmlContext.fontSize)

        contentShell content =
            div [ class "inline-container flex w-full flex-col items-center gap-2 p-2 dark:text-white" ]
                content
    in
    case ( ctx.route, RequestManager.status (RequestManager.GetTrainers {}) model.requestManager ) of
        ( TrainerSelect.Detail trainerNumber, RequestManager.Success ) ->
            case TrainerNumberDict.get trainerNumber model.trainerMetadata of
                Just _ ->
                    { title = Translations.trainerSelectDetailTitle
                    , body =
                        [ contentShell
                            (Detail.view
                                { trainerNumber = trainerNumber
                                , lift = ctx.liftDetail trainerNumber
                                , liftShared = ctx.liftShared
                                }
                                { height = 150, width = canvasWidth }
                                model
                                ++ [ Overview.viewFilters { lift = ctx.liftOverview, liftShared = ctx.liftShared } model
                                   , Overview.viewSprites (Just trainerNumber) model
                                   ]
                            )
                        ]
                    }

                Nothing ->
                    Page404.view model

        ( TrainerSelect.Overview, _ ) ->
            { title = Translations.trainerSelectOverviewTitle
            , body =
                [ contentShell
                    [ Overview.viewFilters { lift = ctx.liftOverview, liftShared = ctx.liftShared } model
                    , Overview.viewSprites Nothing model
                    ]
                ]
            }

        ( TrainerSelect.Detail _, RequestManager.Loading ) ->
            { title = Translations.trainerSelectDetailLoadingTitle, body = [] }

        ( TrainerSelect.Detail _, RequestManager.NotAsked ) ->
            { title = Translations.trainerSelectDetailLoadingTitle, body = [] }

        ( TrainerSelect.Detail _, RequestManager.Failure err ) ->
            { title = Translations.trainerSelectDetailErrorTitle
            , body =
                case ErrorHandling.handleWebError model { languageToString = Language.toString } err of
                    ErrorHandling.Blocking overlay ->
                        [ ErrorHandling.viewError overlay ]

                    _ ->
                        []
            }
