module Main.MapSelect exposing (Context, Msg, PageModel, init, onPageLeave, onPageLoad, update, updateShared, view)

import Atoms.Html
import Atoms.MapPreview
import Atoms.Modal
import BoundedDict
import Dict
import Html.WithContext exposing (div, h1, p, text)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events exposing (onClick)
import Main.Html exposing (Document, Html)
import Main.RequestManager as RequestManager
import Main.Shared as Shared
import Main.Translations as Translations
import MapId
import MapSelect.Model exposing (Startpoint)
import Proto.Smeargle as Smeargle
import Shared.UpdateUtil as UpdateUtil
import StartTileId exposing (StartTileId)
import Tagged exposing (untag)


type Msg
    = OpenConfirmationOverlay Startpoint
    | CloseConfirmationOverlay
    | SelectStartTile StartTileId


type alias PageModel =
    { isConfirmationOverlayOpen : Maybe Startpoint
    }


type alias Context msg =
    { lift : Msg -> msg, liftShared : Shared.Msg -> msg }


type alias Model m =
    Shared.Session
        { m
            | mapSelectModel : PageModel
        }


init : PageModel
init =
    { isConfirmationOverlayOpen = Nothing }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
        |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
    )


requests : List RequestManager.ApiCall
requests =
    [ RequestManager.GetAllStartTiles {}
    , RequestManager.GetTilesetConfig {}
    ]


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave _ model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.unregister
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
    )


updateShared : Context msg -> Shared.Msg -> Model m -> ( Model m, Cmd msg )
updateShared ctx msg model =
    case msg of
        Shared.GotStartTiles response ->
            UpdateUtil.liftCmd ctx.liftShared (loadRequiredMapsForStartpoints response) model

        _ ->
            ( model, Cmd.none )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ mapSelectModel } as model) =
    case msg of
        OpenConfirmationOverlay startpoint ->
            ( { model | mapSelectModel = { mapSelectModel | isConfirmationOverlayOpen = Just startpoint } }, Cmd.none )

        CloseConfirmationOverlay ->
            ( { model | mapSelectModel = { mapSelectModel | isConfirmationOverlayOpen = Nothing } }, Cmd.none )

        SelectStartTile startTileId ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.ChooseStartTile { startTileId = untag startTileId } ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )


loadRequiredMapsForStartpoints : Smeargle.GetAllStartTilesResponse -> Model m -> ( Model m, Cmd Shared.Msg )
loadRequiredMapsForStartpoints { maps } model =
    let
        loadSingleMap ( mapId, { path, offsetX, offsetY, layers } ) =
            RequestManager.LoadTilemapTexture { mapId = untag mapId } { path = path, offsetX = offsetX, offsetY = offsetY, layers = layers }

        ( requestManager, requestManagerCmds ) =
            Dict.toList maps
                |> List.map (Tuple.mapFirst MapId.fromComparable)
                |> List.map loadSingleMap
                |> RequestManager.register
                |> (|>) model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
        |> Cmd.map Shared.RequestManagerMessage
    )


view : Context msg -> Model m -> Document msg
view ctx ({ mapSelectModel } as model) =
    { title = Translations.mapSelectTitle
    , body =
        [ div [ class "flex w-full flex-col items-center gap-2 p-2" ]
            [ h1 [ class "pokedex-width border-b p-2 text-center text-xl font-bold bgd:border-white bgd:text-white bgl:border-black bgl:text-black" ]
                [ Atoms.Html.text Translations.mapSelectHeadline
                ]
            , model.startTiles
                |> List.map (startpointPreview ctx model)
                |> div [ class "flex flex-wrap justify-center gap-2" ]
            ]
        , case mapSelectModel.isConfirmationOverlayOpen of
            Just startpoint ->
                Atoms.Modal.modal { onClose = ctx.lift CloseConfirmationOverlay, noOp = ctx.liftShared Shared.NoOp }
                    [ p [] [ Atoms.Html.text <| Translations.mapSelectConfirmationText startpoint.mapName ]
                    , Atoms.Html.button
                        [ class "rounded-md bg-red-600 px-4 py-2 text-white"
                        , onClick (ctx.lift <| SelectStartTile startpoint.startTileId)
                        ]
                        [ Atoms.Html.text Translations.mapSelectConfirmationAccept ]
                    ]

            Nothing ->
                Atoms.Html.none
        ]
    }


startpointPreview : Context msg -> Model m -> Startpoint -> Html msg
startpointPreview ctx model startpoint =
    let
        preview =
            case
                ( model.textures.tileset
                , BoundedDict.get startpoint.mapId model.textures.tilemaps
                )
            of
                ( Just tileset, Just tilemaps ) ->
                    Atoms.MapPreview.image
                        { mapSize = { width = 250, height = 250 }
                        , center = { x = startpoint.x, y = startpoint.y }
                        , zoom = 0.3
                        }
                        tileset
                        tilemaps

                _ ->
                    Atoms.Html.none
    in
    div [ class "gap-2 rounded-lg bg-gray-300 p-2 dark:bg-gray-700", onClick (OpenConfirmationOverlay startpoint |> ctx.lift) ]
        [ preview
        , p [ class "text-center" ] [ text startpoint.mapName ]
        ]
