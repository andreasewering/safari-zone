module Main.Contacts.Overview exposing (Context, Msg, PageModel, init, onPageLeave, onPageLoad, update, updateShared, view)

import Atoms.Button
import Atoms.Html
import Atoms.Input
import Atoms.Loading exposing (loadingIndicator)
import Html.WithContext exposing (div, h2, li, node, p)
import Html.WithContext.Attributes as Attributes exposing (class, style)
import Html.WithContext.Events as Events
import Html.WithContext.Keyed as Keyed
import Json.Decode
import Main.Chat.Model as Chat
import Main.Contacts.Route as Contacts
import Main.ErrorHandling as ErrorHandling
import Main.Html as Html exposing (Document, Html)
import Main.Language as Language
import Main.RequestManager as RequestManager
import Main.Route as Route
import Main.Shared as Shared
import Main.Translations as Translations
import Maybe.Extra
import OutsideOf exposing (isOutsideOf)
import Platform.Cmd as Cmd
import PokemonNumberDict
import Proto.Chatot exposing (Friend, FriendRequest, Group)
import Protobuf.Types.Int64 exposing (Int64)
import Protobuf.Utils.Int64 as Int64
import Shared.Types.Error as Error
import Shared.Types.TrainerState as TrainerState
import Shared.View.Icon exposing (iconHtmlWith)
import Shared.View.PokemonImage as PokemonImage
import Shared.View.Trainer as Trainer
import Tagged exposing (tag, untag)
import TrainerNumberDict
import UserId exposing (UserId)
import UserIdDict


type alias PageModel =
    { newGroupName : String
    , newFriendId : String
    , scanningQrCode : Bool
    }


type alias Context msg =
    { lift : Msg -> msg, liftShared : Shared.Msg -> msg }


type alias Model m =
    Shared.Session
        { m
            | contactsModel : PageModel
        }


type Msg
    = NewGroupNameChange String
    | SubmitNewGroup
    | NewFriendIdChange String
    | OpenScanner
    | OpenPrivateChat UserId
    | AcceptFriendRequest UserId
    | DeclineFriendRequest UserId
    | CloseOverlay


init : PageModel
init =
    { newGroupName = ""
    , newFriendId = ""
    , scanningQrCode = False
    }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
        |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
    )


requests : List RequestManager.ApiCall
requests =
    [ RequestManager.GetFriends {}
    , RequestManager.GetFriendRequests {}
    , RequestManager.GetGroups {}
    , RequestManager.GetTrainers {}
    ]


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave _ model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.unregister
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
    )


loadOtherUsersData : Context msg -> List { a | userId : Int64 } -> Model m -> ( Model m, Cmd msg )
loadOtherUsersData ctx userIds model =
    let
        userRequests =
            userIds
                |> List.map (\{ userId } -> RequestManager.GetOtherUserData { userId = userId })

        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                userRequests
                model.requestManager
    in
    ( { model | requestManager = requestManager }, requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared) )


updateShared : Context msg -> Shared.Msg -> Model m -> ( Model m, Cmd msg )
updateShared ctx msg model =
    case msg of
        Shared.LoadedFriends { friends } ->
            loadOtherUsersData ctx friends model

        Shared.LoadedFriendRequests { incoming, outgoing } ->
            loadOtherUsersData ctx (incoming ++ outgoing) model

        Shared.LoadedOtherUserData _ userData ->
            let
                userRequests =
                    userData.partnerPokemon
                        |> Maybe.map (\{ pokemonNumber } -> RequestManager.GetPokemon { pokemonNumber = pokemonNumber })
                        |> Maybe.Extra.toList

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        userRequests
                        model.requestManager
            in
            ( { model | requestManager = requestManager }, requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared) )

        _ ->
            ( model, Cmd.none )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ contactsModel } as model) =
    case msg of
        NewGroupNameChange newName ->
            ( { model | contactsModel = { contactsModel | newGroupName = newName } }, Cmd.none )

        SubmitNewGroup ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.CreateGroup { groupName = contactsModel.newGroupName } ] model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        NewFriendIdChange newId ->
            addFriend ctx { model | contactsModel = { contactsModel | newFriendId = newId } }

        OpenScanner ->
            ( { model | contactsModel = { contactsModel | scanningQrCode = True } }, Cmd.none )

        CloseOverlay ->
            ( { model | contactsModel = { contactsModel | scanningQrCode = False } }, Cmd.none )

        OpenPrivateChat userId ->
            ( { model | chats = Chat.openPrivateChannel userId model.chats }
            , Cmd.none
            )

        AcceptFriendRequest userId ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.AddFriend { userId = untag userId } ] model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        DeclineFriendRequest userId ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.RemoveFriend { userId = untag userId } ] model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )


addFriend : Context msg -> Model m -> ( Model m, Cmd msg )
addFriend ctx ({ contactsModel } as model) =
    case UserId.fromShortString contactsModel.newFriendId of
        Just friendId ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.AddFriend { userId = untag friendId } ] model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        Nothing ->
            ( model, Cmd.none )


view : Context msg -> Model m -> Document msg
view ctx model =
    { title = Translations.contactsHeadline, body = viewBody ctx model }


viewBody : Context msg -> Model m -> List (Html msg)
viewBody ctx ({ contactsModel } as model) =
    let
        friendsStatus =
            RequestManager.status (RequestManager.GetFriends {}) model.requestManager

        friendRequestsStatus =
            RequestManager.status (RequestManager.GetFriendRequests {}) model.requestManager

        heightsInRem =
            heights model

        centeredLoadingIndicator =
            div [ class "flex h-full w-full items-center justify-center" ] [ div [ class "relative w-20" ] [ loadingIndicator ] ]
    in
    [ if contactsModel.scanningQrCode then
        div [ Attributes.attribute "role" "dialog", class "absolute left-1/2 top-1/2 z-10 -translate-x-1/2 -translate-y-1/2 rounded-lg bg-gray-200 p-2 dark:bg-gray-700" ] [ qrCodeScanner (NewFriendIdChange >> ctx.lift) ]

      else
        Atoms.Html.none
    , case ( friendsStatus, friendRequestsStatus ) of
        ( RequestManager.Loading, _ ) ->
            centeredLoadingIndicator

        ( _, RequestManager.Loading ) ->
            centeredLoadingIndicator

        ( _, RequestManager.Failure error ) ->
            case ErrorHandling.handleWebError model { languageToString = Language.toString } error of
                ErrorHandling.AutomaticallyFixable _ _ ->
                    Atoms.Html.none

                ErrorHandling.Blocking overlay ->
                    ErrorHandling.viewErrorSmall overlay

        ( RequestManager.Failure error, _ ) ->
            case ErrorHandling.handleWebError model { languageToString = Language.toString } error of
                ErrorHandling.AutomaticallyFixable _ _ ->
                    Atoms.Html.none

                ErrorHandling.Blocking overlay ->
                    ErrorHandling.viewErrorSmall overlay

        ( RequestManager.Success, RequestManager.Success ) ->
            div
                [ class "flex h-full flex-col items-center gap-2 p-4"
                , isOutsideOf "role" "dialog"
                    |> Json.Decode.andThen
                        (\_ ->
                            if contactsModel.scanningQrCode then
                                Json.Decode.succeed (ctx.lift CloseOverlay)

                            else
                                Json.Decode.fail "Already closed"
                        )
                    |> Events.on "click"
                ]
                [ viewIncomingFriendRequests ctx model heightsInRem.incoming ( model.friendRequests.incoming, friendRequestsStatus )
                , viewFriends ctx model heightsInRem.existing ( model.friends, friendsStatus )
                , viewOutgoingFriendRequests ctx model heightsInRem.outgoing ( model.friendRequests.outgoing, friendRequestsStatus )
                ]

        _ ->
            Atoms.Html.none
    ]


viewGroups : ( List Group, RequestManager.Status e ) -> Html msg
viewGroups ( groups, status ) =
    case status of
        RequestManager.Success ->
            if List.isEmpty groups then
                div [ class "flex justify-center" ]
                    [ p [ class "rounded-lg bg-gray-300 px-4 py-2 dark:bg-gray-700" ]
                        [ Atoms.Html.text Translations.groupsOverviewNoGroupsMessage ]
                    ]

            else
                List.map viewGroup groups
                    |> Keyed.ul
                        [ class "flex flex-col justify-center gap-2 p-2"
                        ]

        _ ->
            Atoms.Html.none


viewGroup : Group -> ( String, Html msg )
viewGroup { groupId, groupName } =
    ( Int64.toUnsignedString groupId
    , li []
        [ Html.buttonLink
            { label = Html.WithContext.text groupName
            , route = Route.Contacts <| Contacts.GroupDetail <| tag groupId
            }
        ]
    )


headline : (Translations.I18n -> String) -> Html msg
headline translation =
    h2 [ class "w-full border-b border-gray-600 p-2 text-center text-xl font-bold dark:border-gray-300" ] [ Atoms.Html.text translation ]


panelStyles : Html.WithContext.Attribute context msg
panelStyles =
    class "flex w-full flex-col items-center rounded-lg bg-gray-200 p-2 transition-all dark:bg-gray-700"


viewIncomingFriendRequests : Context msg -> Model m -> Float -> ( List FriendRequest, RequestManager.Status Error.Error ) -> Html msg
viewIncomingFriendRequests ctx model heightInRem ( friendRequests, status ) =
    let
        incomingHeadline =
            headline Translations.incomingFriendRequestsHeadline
    in
    div [ panelStyles, style "max-height" (String.fromFloat heightInRem ++ "rem") ]
        [ incomingHeadline
        , List.map (viewIncomingFriendRequest ctx model) friendRequests
            |> Keyed.ul [ class "flex w-full flex-col gap-2 overflow-y-scroll pt-2" ]
        ]


viewOutgoingFriendRequests : Context msg -> Model m -> Float -> ( List FriendRequest, RequestManager.Status Error.Error ) -> Html msg
viewOutgoingFriendRequests ctx ({ contactsModel } as model) heightInRem ( friendRequests, status ) =
    let
        outgoingHeadline =
            headline Translations.outgoingFriendRequestsHeadline

        inputIsValidUserId =
            if String.isEmpty contactsModel.newFriendId || Maybe.Extra.isJust (UserId.fromShortString contactsModel.newFriendId) then
                Nothing

            else
                Just Translations.invalidUserIdError

        addNewFriend =
            div [ class "flex gap-2 pt-2" ]
                [ Atoms.Input.text
                    { id = "add-friend-input"
                    , label = Translations.friendsAddUserLabel
                    , onChange = NewFriendIdChange >> ctx.lift
                    , text = contactsModel.newFriendId
                    , error = inputIsValidUserId
                    }
                , Atoms.Button.button
                    { onPress = ctx.lift OpenScanner |> Just
                    , label = Atoms.Html.text Translations.friendsScanUserLabel
                    , loading = False
                    }
                    [ class "whitespace-nowrap" ]
                ]
    in
    div [ panelStyles, style "max-height" (String.fromFloat heightInRem ++ "rem") ]
        [ outgoingHeadline
        , addNewFriend
        , List.map (viewOutgoingFriendRequest ctx model) friendRequests
            |> Keyed.ul [ class "flex w-full flex-col gap-2 overflow-y-scroll pt-2" ]
        ]


incomingFriendRequestContainerStyling : Html.Attribute msg
incomingFriendRequestContainerStyling =
    class "flex w-full items-center gap-2 rounded-lg bg-gray-300 p-2 dark:bg-gray-800"


showPartner : UserId -> Model m -> Html msg
showPartner userId model =
    UserIdDict.get userId model.users
        |> Maybe.andThen (\data -> data.partnerPokemon)
        |> Maybe.andThen
            (\partner ->
                PokemonNumberDict.get partner.pokemonNumber model.pokemonDataDict
                    |> Maybe.map (Tuple.pair partner)
            )
        |> Maybe.map
            (\( { pokemonNumber, isShiny }, pokemon ) ->
                PokemonImage.showImageHtml [ class "h-6 w-6" ]
                    { name = pokemon.name
                    , pokemonNumber = pokemonNumber
                    }
                    (if isShiny then
                        PokemonImage.Shiny

                     else
                        PokemonImage.Normal
                    )
            )
        |> Maybe.withDefault (div [ class "h-6 w-6" ] [])


showTrainer : UserId -> Model m -> Html msg
showTrainer userId model =
    UserIdDict.get userId model.users
        |> Maybe.andThen (\data -> data.trainerSprite)
        |> Maybe.andThen
            (\trainerNumber ->
                TrainerNumberDict.get trainerNumber model.trainerMetadata
                    |> Maybe.map (Tuple.pair trainerNumber)
            )
        |> Maybe.map
            (\( trainerNumber, { displayName } ) ->
                Trainer.trainerSpriteHtml { displaySize = 1.5 }
                    TrainerState.init
                    { displayName = displayName, trainerNumber = trainerNumber }
            )
        |> Maybe.withDefault (div [ class "h-6 w-6" ] [])


viewIncomingFriendRequest : Context msg -> Model m -> FriendRequest -> ( String, Html msg )
viewIncomingFriendRequest ctx model friendRequest =
    let
        friendId =
            tag friendRequest.userId
    in
    ( UserId.toShortString friendId
    , li
        [ incomingFriendRequestContainerStyling
        , class "justify-between"
        ]
        [ iconHtmlWith
            { name = "check-mark"
            , width = 1.5
            , height = 1.5
            , description = Translations.incomingFriendRequestAccept
            }
            [ Events.onClick (AcceptFriendRequest friendId |> ctx.lift) ]
        , div [ class "flex gap-2" ]
            [ showTrainer friendId model
            , Html.WithContext.text friendRequest.userName
            , showPartner friendId model
            ]
        , declineButton ctx friendId
        ]
    )


viewOutgoingFriendRequest : Context msg -> Model m -> FriendRequest -> ( String, Html msg )
viewOutgoingFriendRequest ctx model friendRequest =
    let
        friendId =
            tag friendRequest.userId
    in
    ( UserId.toShortString friendId
    , li [ incomingFriendRequestContainerStyling, class "justify-between" ]
        [ div [ class "w-6" ] [] -- To take up some space to be symmetric with incoming friend requests
        , div [ class "flex gap-2" ]
            [ showTrainer friendId model
            , Html.WithContext.text friendRequest.userName
            , showPartner friendId model
            ]
        , declineButton ctx friendId
        ]
    )


declineButton : Context msg -> UserId -> Html msg
declineButton ctx userId =
    iconHtmlWith
        { name = "cross"
        , width = 1.5
        , height = 1.5
        , description = Translations.incomingFriendRequestDecline
        }
        [ Events.onClick (DeclineFriendRequest userId |> ctx.lift) ]


viewFriends : Context msg -> Model m -> Float -> ( List Friend, RequestManager.Status Error.Error ) -> Html msg
viewFriends ctx model heightInRem ( friends, status ) =
    let
        friendsHeadline =
            headline Translations.friendsHeadline
    in
    div [ panelStyles, style "max-height" (String.fromFloat heightInRem ++ "rem") ]
        [ friendsHeadline
        , List.map (viewFriend ctx model) friends
            |> Keyed.ul
                [ class "flex w-full flex-col gap-2 overflow-y-scroll pt-2"
                ]
        ]


viewFriend : Context msg -> Model m -> Friend -> ( String, Html msg )
viewFriend ctx model friend =
    let
        friendId =
            tag friend.userId

        online =
            isUserOnline model friendId
    in
    ( UserId.toShortString friendId
    , li
        ([ incomingFriendRequestContainerStyling, class "justify-between" ]
            ++ (if online then
                    [ Events.onClick (ctx.lift <| OpenPrivateChat friendId)
                    , class "text-black dark:text-white"
                    ]

                else
                    [ class "text-gray-400 dark:text-gray-600" ]
               )
        )
        [ onlineMarker online
        , div [ class "flex gap-2" ]
            [ showTrainer friendId model
            , Html.WithContext.text friend.userName
            , showPartner friendId model
            ]
        , declineButton ctx friendId
        ]
    )


heights : Model m -> { incoming : Float, outgoing : Float, existing : Float }
heights { viewport, htmlContext, requestManager, friendRequests } =
    let
        friendRequestsStatus =
            RequestManager.status (RequestManager.GetFriendRequests {}) requestManager

        incoming =
            case friendRequestsStatus of
                RequestManager.Success ->
                    List.length friendRequests.incoming
                        |> min 3
                        |> toFloat
                        -- Each friend request takes 3rem height
                        |> (*) 3
                        -- Base height 5rem
                        |> (+) 5

                RequestManager.NotAsked ->
                    0

                RequestManager.Loading ->
                    0

                RequestManager.Failure _ ->
                    10

        outgoing =
            case friendRequestsStatus of
                RequestManager.Success ->
                    List.length friendRequests.outgoing
                        |> min 3
                        |> toFloat
                        |> (*) 3
                        |> (+) 9

                RequestManager.NotAsked ->
                    0

                RequestManager.Loading ->
                    0

                RequestManager.Failure _ ->
                    10

        existing =
            (toFloat viewport.height / toFloat htmlContext.fontSize)
                - outgoing
                - incoming
    in
    { incoming = incoming, outgoing = outgoing, existing = existing }


qrCodeScanner : (String -> msg) -> Html msg
qrCodeScanner onDetect =
    let
        eventDecoder =
            Json.Decode.field "detail" Json.Decode.string
                |> Json.Decode.map onDetect
    in
    node "qr-code-scanner" [ Events.on "detect" eventDecoder ] []


onlineMarker : Bool -> Html msg
onlineMarker isOnline =
    div
        [ class "h-4 w-4 rounded-full"
        , if isOnline then
            class "bg-green-500"

          else
            class "bg-red-600"
        ]
        []


isUserOnline : Model m -> UserId -> Bool
isUserOnline _ _ =
    -- TODO
    True
