module Main.Contacts.Route exposing (..)

import GroupId exposing (GroupId)
import UrlCodec exposing (UrlCodec, buildUnion, chain, s, top, union, variant0, variant1)


type Route
    = Overview
    | GroupDetail GroupId


codec : UrlCodec (Route -> a) a Route
codec =
    union
        (\overview groupDetail value ->
            case value of
                Overview ->
                    overview

                GroupDetail groupId ->
                    groupDetail groupId
        )
        |> variant0 Overview top
        |> variant1 GroupDetail (s "group" |> chain GroupId.urlCodec)
        |> buildUnion
