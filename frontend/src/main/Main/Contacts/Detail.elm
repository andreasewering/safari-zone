module Main.Contacts.Detail exposing
    ( Context
    , Msg
    , PageModel
    , init
    , onPageLeave
    , onPageLoad
    , update
    , updateShared
    , view
    )

import Atoms.Button
import Atoms.Html as Html exposing (text)
import Atoms.Input
import Auth
import BoundedDict
import GroupId exposing (GroupId)
import Html.WithContext exposing (div, h1, span)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Keyed as Keyed
import Main.Contacts.Overview as Overview
import Main.ErrorHandling as ErrorHandling
import Main.Html exposing (Attribute, Document, Html)
import Main.RequestManager as RequestManager
import Main.Shared as Shared
import Main.Translations as Translations
import Proto.Chatot exposing (GetGroupDetailResponse, GroupMember)
import Proto.Chatot.GroupMember.Role as Role
import Protobuf.Utils.Int64 as Int64
import Tagged exposing (tag, untag)
import UserId


type alias PageModel =
    { groupMemberFilter : String
    , addUserIdToGroupInput : String
    }


type alias Model m =
    Shared.Session
        { m
            | contactsModel : Overview.PageModel
            , groupDetailModel : PageModel
        }


type Msg
    = MemberFilterChanged String
    | AddUserIdToGroupInputChanged String
    | SubmitAddUserToGroup GroupId


type alias Context msg =
    { groupId : GroupId, lift : Msg -> msg, liftShared : Shared.Msg -> msg }


init : PageModel
init =
    { groupMemberFilter = ""
    , addUserIdToGroupInput = ""
    }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                (requests ctx.groupId)
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
        |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
    )


requests : GroupId -> List RequestManager.ApiCall
requests groupId =
    [ RequestManager.GetGroupDetail { groupId = untag groupId }
    ]


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.unregister
                (requests ctx.groupId)
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
    )


updateShared : Context msg -> Shared.Msg -> Model m -> ( Model m, Cmd msg )
updateShared _ msg ({ groupDetailModel } as model) =
    case msg of
        Shared.AddedUserToGroup _ _ _ ->
            ( { model
                | groupDetailModel =
                    { groupDetailModel
                        | addUserIdToGroupInput = ""
                    }
              }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ groupDetailModel } as model) =
    case msg of
        MemberFilterChanged newFilter ->
            ( { model | groupDetailModel = { groupDetailModel | groupMemberFilter = newFilter } }, Cmd.none )

        AddUserIdToGroupInputChanged newValue ->
            ( { model | groupDetailModel = { groupDetailModel | addUserIdToGroupInput = newValue } }, Cmd.none )

        SubmitAddUserToGroup groupId ->
            case UserId.fromShortString groupDetailModel.addUserIdToGroupInput of
                Just userId ->
                    let
                        ( requestManager, requestManagerCmds ) =
                            RequestManager.register [ RequestManager.AddToGroup { groupId = untag groupId, userId = untag userId } ] model.requestManager
                    in
                    ( { model | requestManager = requestManager }
                    , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
                    )

                Nothing ->
                    ( model, Cmd.none )


view : Context msg -> Model m -> Document msg
view ctx model =
    { title =
        case ( BoundedDict.get ctx.groupId model.groupDetails, RequestManager.status (RequestManager.GetGroupDetail { groupId = untag ctx.groupId }) model.requestManager ) of
            ( Just details, RequestManager.Success ) ->
                Translations.groupsDetailTitle details.groupName

            ( _, RequestManager.Failure error ) ->
                error.error
                    |> ErrorHandling.title

            _ ->
                Translations.groupsDetailLoadingTitle
    , body = viewBody ctx model
    }


viewBody : Context msg -> Model m -> List (Html msg)
viewBody ctx model =
    case BoundedDict.get ctx.groupId model.groupDetails of
        Just groupDetail ->
            viewDetail ctx model groupDetail

        _ ->
            []


viewDetail : Context msg -> Model m -> GetGroupDetailResponse -> List (Html msg)
viewDetail ctx ({ groupDetailModel } as model) { groupName, members } =
    let
        isCurrentUserAdminOfGroup =
            List.any
                (\{ userId, role } ->
                    Auth.getUserId model.auth
                        == tag userId
                        && role
                        /= Role.Member
                )
                members

        addUserRow =
            if isCurrentUserAdminOfGroup then
                div [ class "flex gap-2" ]
                    [ addUserByIdField ctx model
                    , Atoms.Button.button
                        { label = text Translations.groupsDetailAddUserSubmit
                        , onPress = SubmitAddUserToGroup ctx.groupId |> ctx.lift |> Just
                        , loading = False
                        }
                        []
                    ]

            else
                Html.none

        doesMemberMatchFilter =
            .userName >> String.toLower >> String.contains (String.toLower groupDetailModel.groupMemberFilter)
    in
    [ div [ class "flex h-full w-full flex-col gap-2 p-2" ]
        [ h1 [ class "text-center" ] [ text <| Translations.groupsDetailHeadline groupName ]
        , addUserRow
        , searchField ctx model
        , Keyed.ul [ class "flex h-full w-full flex-col gap-2 rounded-lg bg-white p-2" ] <|
            List.map (\member -> ( Int64.toUnsignedString member.userId, viewMember member ))
                (List.filter doesMemberMatchFilter members)
        ]
    ]


viewMember : GroupMember -> Html msg
viewMember member =
    div
        [ class "flex w-full justify-evenly rounded-lg p-2"
        , memberColor member.role
        ]
        [ Html.WithContext.text member.userName, viewRole member.role ]


memberColor : Role.Role -> Attribute msg
memberColor role =
    case role of
        Role.Creator ->
            class "bg-sky-700"

        Role.Admin ->
            class "bg-gray-600"

        _ ->
            class "bg-gray-400"


addUserByIdField : Context msg -> Model m -> Html msg
addUserByIdField ctx { groupDetailModel } =
    Atoms.Input.text
        { label = Translations.groupsDetailAddUserLabel
        , onChange = AddUserIdToGroupInputChanged >> ctx.lift
        , id = "add-user"
        , text = groupDetailModel.addUserIdToGroupInput
        , error = Nothing
        }


searchField : Context msg -> Model m -> Html msg
searchField ctx { groupDetailModel } =
    Atoms.Input.text
        { label = Translations.groupsDetailFilterLabel
        , onChange = MemberFilterChanged >> ctx.lift
        , id = "search"
        , text = groupDetailModel.groupMemberFilter
        , error = Nothing
        }


viewRole : Role.Role -> Html msg
viewRole role =
    case role of
        Role.Admin ->
            span [ class "text-gray-300" ] [ text Translations.groupsDetailRolesAdmin ]

        Role.Creator ->
            span [ class "text-gray-300" ] [ text Translations.groupsDetailRolesCreator ]

        _ ->
            Html.none
