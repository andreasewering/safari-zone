module Main.Model exposing (Model)

import Main.Contacts.Detail as ContactsDetail
import Main.Contacts.Overview as ContactsOverview
import Main.Dex.Model as Dex
import Main.MapSelect as MapSelect
import Main.Safari.Model as Safari
import Main.Settings.Overview as SettingsOverview
import Main.Settings.QrCodeView as QrCodeView
import Main.Shared exposing (Session)
import Main.TrainerSelect.Detail as TrainerDetail
import Main.TrainerSelect.Model as TrainerSelect


type alias Model =
    Session
        { dexModel : Dex.PageModel
        , safariModel : Safari.PageModel
        , settingsModel : SettingsOverview.PageModel
        , qrCodeViewModel : QrCodeView.PageModel
        , trainerSelectModel : TrainerSelect.PageModel
        , trainerDetailModel : TrainerDetail.PageModel
        , contactsModel : ContactsOverview.PageModel
        , groupDetailModel : ContactsDetail.PageModel
        , mapSelectModel : MapSelect.PageModel
        }
