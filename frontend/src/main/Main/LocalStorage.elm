module Main.LocalStorage exposing (deleteLanguage, saveAudioPreference, saveColorTheme, saveLanguage, saveReducedMotion)

import Interop.LocalStorage.Out exposing (Msg(..))
import InteropDefinitions
import InteropPorts
import Main.Translations as Translations exposing (Language)
import Shared.View.Theme as Theme


languageKey : String
languageKey =
    "preferred_language"


saveLanguage : Language -> Cmd msg
saveLanguage language =
    Set { key = languageKey, value = Translations.languageToString language } |> send


deleteLanguage : Cmd msg
deleteLanguage =
    Delete languageKey |> send


colorThemeKey : String
colorThemeKey =
    "preferred_color_theme"


saveColorTheme : Theme.Name -> Cmd msg
saveColorTheme theme =
    Set { key = colorThemeKey, value = Theme.toString theme } |> send


audioKey : String
audioKey =
    "audio_on"


reducedMotionKey : String
reducedMotionKey =
    "prefers_reduced_motion"


saveAudioPreference : Bool -> Cmd msg
saveAudioPreference on =
    send <|
        if on then
            Set { key = audioKey, value = "true" }

        else
            Delete audioKey


saveReducedMotion : Bool -> Cmd msg
saveReducedMotion reduced =
    send <|
        if reduced then
            Set { key = reducedMotionKey, value = "" }

        else
            Delete reducedMotionKey


send : Interop.LocalStorage.Out.Msg -> Cmd msg
send =
    InteropDefinitions.LocalStorageOut >> InteropPorts.fromElm
