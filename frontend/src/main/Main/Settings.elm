module Main.Settings exposing (Context, view)

import Main.Html exposing (Document)
import Main.Settings.Overview as Overview
import Main.Settings.QrCodeView as QrCodeView
import Main.Settings.Route as Settings
import Main.Shared as Shared
import Main.Translations as Translations


type alias Context msg =
    { route : Settings.Route
    , liftOverview : Overview.Msg -> msg
    , liftQrCodeView : QrCodeView.Msg -> msg
    , liftShared : Shared.Msg -> msg
    }


type alias Model m =
    Shared.Session
        { m
            | settingsModel : Overview.PageModel
            , qrCodeViewModel : QrCodeView.PageModel
        }


view : Context msg -> Model m -> Document msg
view ctx model =
    case ctx.route of
        Settings.Overview ->
            { title = Translations.settingsTitle
            , body = Overview.view { lift = ctx.liftOverview, liftShared = ctx.liftShared } model
            }

        Settings.QrCodeView ->
            { title = Translations.settingsQrCodeTitle
            , body = QrCodeView.view { lift = ctx.liftQrCodeView, liftShared = ctx.liftShared } model
            }
