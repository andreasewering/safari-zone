module Main.Dex exposing (view)

import Animator
import Main.Dex.Appraisal as DexAppraisal
import Main.Dex.Detail as DexDetail
import Main.Dex.Model as Model
import Main.Dex.Route as Dex exposing (Route(..))
import Main.Dex.View.Pokedex as Pokedex
import Main.Html exposing (Document)
import Main.RequestManager as RequestManager
import Main.Shared as Shared
import Main.Translations as Translations
import PokemonNumber exposing (PokemonNumber)


type alias Context msg =
    { route : Dex.Route
    , liftDetail : PokemonNumber -> DexDetail.Msg -> msg
    , liftAppraisal : DexAppraisal.Msg -> msg
    , liftShared : Shared.Msg -> msg
    }


type alias Model m =
    Shared.Session
        { m
            | dexModel : Model.PageModel
        }


view : Context msg -> Model m -> Document msg
view ctx ({ dexModel } as model) =
    let
        ( routeToRender, _ ) =
            Animator.arrived dexModel.transitionTimeline
    in
    case routeToRender of
        Overview ->
            { title = Translations.dexOverviewTitle
            , body =
                [ Pokedex.view
                    []
                    (RequestManager.status (RequestManager.GetDexEntries {}) model.requestManager)
                    { dexEntries = model.dexEntries
                    , errorCtx = model
                    }
                ]
            }

        Appraisal ->
            DexAppraisal.view { lift = ctx.liftAppraisal, liftShared = ctx.liftShared } model

        Detail pokemonNumber ->
            DexDetail.view
                { pokemonNumber = pokemonNumber
                , lift = ctx.liftDetail pokemonNumber
                , liftShared = ctx.liftShared
                }
                model
