module Main.Chat exposing
    ( Context
    , Model
    , update
    , view
    , viewIcon
    )

import Atoms.Html
import Atoms.Modal
import Browser.Dom exposing (focus)
import ChatChannel exposing (ChatChannel)
import DateFormat
import Html.WithContext exposing (div, label, li, ol, span, textarea)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events as Events
import Html.WithContext.Keyed as Keyed
import Interop.Chat
import Interop.WebSocket
import List.Extra
import Main.Chat.Channel as Channel
import Main.Chat.Model as Model exposing (Chats)
import Main.Chat.Msg exposing (Msg(..))
import Main.Html exposing (Attribute, Html)
import Main.Shared as Shared
import Main.Translations as Translations
import Maybe.Extra
import Proto.Chatot.WebsocketMessageToServer.Message
import Proto.Kangaskhan.WebsocketMessageToServer.Message
import Task
import Time



-- Public interface


type alias Model =
    Chats


type alias Context msg =
    { lift : Msg -> msg, liftShared : Shared.Msg -> msg }


update : Context msg -> Msg -> Chats -> ( Chats, Cmd msg )
update ctx msg chats =
    case msg of
        ToggleChat ->
            ( Model.toggle chats, Cmd.none )

        GoToChannel channel ->
            ( Model.selectChannel channel chats, sendStopTyping chats )

        GoToChats ->
            ( Model.deselectChannel chats, sendStopTyping chats )

        Typing channel text ->
            if String.endsWith "\n" text then
                sendMessage ctx channel chats

            else
                ( Model.updateTypedText channel text chats, sendStartTyping channel )

        SendMsg channel ->
            sendMessage ctx channel chats


view : Context msg -> { s | chats : Chats, height : Int } -> Html msg
view ctx session =
    let
        events =
            { onChannelClick = ctx.lift << GoToChannel, onTyping = \channel text -> ctx.lift (Typing channel text) }

        selectedChannelAndChat =
            Model.selectedChannel session.chats

        chatHeader =
            span
                [ class "rounded-lg p-2"
                ]
                [ Atoms.Html.text <|
                    case selectedChannelAndChat of
                        Just { chat } ->
                            chat.chatName

                        Nothing ->
                            Translations.chatHeaderTitle
                ]

        backAction =
            ctx.lift <|
                if Maybe.Extra.isJust selectedChannelAndChat then
                    GoToChats

                else
                    ToggleChat

        innerView =
            case selectedChannelAndChat of
                Just { channel, chat } ->
                    viewChat events channel chat

                Nothing ->
                    Keyed.ul [ class "flex w-full flex-col gap-2 p-1" ] <|
                        viewChannels session.chats events
    in
    if Model.isOpen session.chats then
        Atoms.Modal.modal { noOp = ctx.liftShared Shared.NoOp, onClose = ctx.lift ToggleChat }
            [ div [ class "flex w-full gap-4 font-bold" ]
                [ chatHeader

                -- , Tooltip.viewInfo
                --     { id = tooltipId
                --     , text = tooltipText
                --     , isTooltipOpen = isTooltipOpen
                --     , tooltipAlign = El.below
                --     , onChangeOpen = SetTooltipOpen
                --     }
                --     [ El.alignRight ]
                --     [ El.moveLeft 200 ]
                , Atoms.Html.button [ Events.onClick backAction ] [ Main.Html.backIcon ]
                ]
            , innerView
            ]

    else
        Atoms.Html.none


viewIcon : { s | chats : Chats } -> List (Attribute Msg) -> Html Msg
viewIcon session extraAttrs =
    div extraAttrs
        [ unreadNumber (Model.allUnreadMessages session.chats)
        , Atoms.Html.image
            [ class "w-20", Events.onClick ToggleChat ]
            -- TODO i18n
            { src = "/images/pidgey-mail.png", description = \_ -> "Pidgey Messages" }
        ]



-- Internal Definitions
-- update


sendMessage : Context msg -> ChatChannel -> Chats -> ( Chats, Cmd msg )
sendMessage ctx channel chats =
    Maybe.map
        (\{ currentlyTyped } ->
            case Channel.channelToRecipient channel of
                Channel.Chatot chatotChannel ->
                    Proto.Chatot.WebsocketMessageToServer.Message.SentMessage { channel = Just chatotChannel, text = currentlyTyped }
                        |> Interop.Chat.sendMessage

                Channel.Kangaskhan mapId ->
                    Proto.Kangaskhan.WebsocketMessageToServer.Message.SendChatMessage currentlyTyped
                        |> Interop.WebSocket.sendKangaskhanMsg mapId
        )
        (Model.getChannel channel chats)
        |> Maybe.map (\cmd -> Cmd.batch [ cmd, focusActiveChatInput chats |> Cmd.map ctx.liftShared ])
        |> Maybe.withDefault Cmd.none
        |> Tuple.pair (Model.updateForSendMessage channel chats)


sendStartTyping : ChatChannel -> Cmd msg
sendStartTyping channel =
    case Channel.channelToRecipient channel of
        Channel.Chatot chatotChannel ->
            Proto.Chatot.WebsocketMessageToServer.Message.StartedTyping chatotChannel
                |> Interop.Chat.sendMessage

        Channel.Kangaskhan mapId ->
            Proto.Kangaskhan.WebsocketMessageToServer.Message.StartTyping {}
                |> Interop.WebSocket.sendKangaskhanMsg mapId


sendStopTyping : Chats -> Cmd msg
sendStopTyping chats =
    Model.selectedChannel chats
        |> Maybe.map
            (\selected ->
                case Channel.channelToRecipient selected.channel of
                    Channel.Chatot chatotChannel ->
                        Proto.Chatot.WebsocketMessageToServer.Message.StartedTyping chatotChannel
                            |> Interop.Chat.sendMessage

                    Channel.Kangaskhan mapId ->
                        Proto.Kangaskhan.WebsocketMessageToServer.Message.StartTyping {}
                            |> Interop.WebSocket.sendKangaskhanMsg mapId
            )
        |> Maybe.withDefault Cmd.none


focusActiveChatInput : Chats -> Cmd Shared.Msg
focusActiveChatInput chats =
    Model.selectedChannel chats
        |> Maybe.map (focus << chatInputId << .channel)
        |> Maybe.withDefault (Task.succeed ())
        |> Task.attempt (always Shared.NoOp)



-- View


viewChannels :
    Chats
    ->
        { e
            | onChannelClick : ChatChannel -> msg
        }
    -> List ( String, Html msg )
viewChannels chats events =
    Model.sortedChannels chats
        |> List.map
            (\( channel, chat ) ->
                let
                    unreadMsg =
                        Model.unreadMessagesForChat chat
                in
                ( "c" ++ ChatChannel.toString channel
                , div
                    [ class "w-full text-white"
                    ]
                    [ unreadNumber unreadMsg
                    , div
                        [ class "w-full rounded-lg bg-red-600 p-2"
                        , Events.onClick (events.onChannelClick channel)
                        ]
                        [ Atoms.Html.text chat.chatName
                        ]
                    ]
                )
            )


viewChat :
    { e
        | onTyping : ChatChannel -> String -> msg
    }
    -> ChatChannel
    -> Model.Chat
    -> Html msg
viewChat events channel { messages, currentlyTyped } =
    let
        wasSentInQuickSuccession m1 m2 =
            abs (Time.posixToMillis m1.timestamp - Time.posixToMillis m2.timestamp) < 10000

        groupBySender =
            List.Extra.groupWhile (\m1 m2 -> m1.sender == m2.sender && wasSentInQuickSuccession m1 m2)

        viewSender : Model.Sender -> Html msg
        viewSender sender =
            span []
                [ Atoms.Html.text <|
                    case sender of
                        Model.Self ->
                            Translations.chatSenderYou

                        Model.Other other ->
                            always other
                , Html.WithContext.text ":"
                ]

        viewTimestamp : Time.Posix -> Html msg
        viewTimestamp =
            Atoms.Html.formatTime
                [ DateFormat.hourMilitaryFixed
                , DateFormat.text ":"
                , DateFormat.minuteFixed
                ]

        viewMessage : Model.Message -> Html msg
        viewMessage msg =
            li [] [ Html.WithContext.text msg.text ]

        viewMessageGroup : ( Model.Message, List Model.Message ) -> ( String, Html msg )
        viewMessageGroup ( firstMsg, otherMsgs ) =
            -- message groups are keyed by g + timestamp
            ( "g" ++ String.fromInt (Time.toMillis Time.utc firstMsg.timestamp)
            , ol
                [ class "w-full max-w-sm rounded-lg bg-gray-300 p-2 dark:bg-gray-700"
                , case firstMsg.sender of
                    Model.Self ->
                        class "float-right"

                    Model.Other _ ->
                        class "float-left"
                ]
                (li
                    [ class "flex w-full justify-evenly text-sm text-gray-700 dark:text-gray-300"
                    ]
                    [ viewSender firstMsg.sender, viewTimestamp firstMsg.timestamp ]
                    :: (List.reverse << List.map viewMessage) (firstMsg :: otherMsgs)
                )
            )

        viewInputField : Html msg
        viewInputField =
            let
                id =
                    chatInputId channel
            in
            div []
                [ label [ Html.WithContext.Attributes.for id ] []
                , textarea
                    [ class "rounded-sm"
                    , Html.WithContext.Attributes.rows 2
                    , Html.WithContext.Attributes.id id
                    , Atoms.Html.placeholder Translations.chatInputPlaceholder
                    , Events.onInput (events.onTyping channel)
                    ]
                    [ Html.WithContext.text currentlyTyped ]
                ]
    in
    groupBySender messages
        |> List.map viewMessageGroup
        -- inputField is keyed by "i"
        |> (::) ( "i", viewInputField )
        |> List.reverse
        |> Keyed.ol
            [ class "flex flex-col gap-2 bg-gray-200 p-2 text-black dark:bg-gray-800 dark:text-white"
            , class "h-full w-full rounded-b-lg"
            ]


chatInputId : ChatChannel -> String
chatInputId =
    ChatChannel.toString >> (++) "chat_input_"


unreadNumber : Int -> Html msg
unreadNumber n =
    div
        [ class "rounded-lg bg-red-600 p-2 text-center text-white" ]
        [ Html.WithContext.text (String.fromInt n) ]
