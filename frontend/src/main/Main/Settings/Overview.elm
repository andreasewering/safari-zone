module Main.Settings.Overview exposing (..)

import Atoms.Html
import Auth
import Domain.Direction as Direction
import Html.WithContext exposing (div, h1, h2, node, option, p, select)
import Html.WithContext.Attributes exposing (class, disabled, href, selected, value)
import Html.WithContext.Events as Events exposing (on, onClick)
import Interop.Audio
import Json.Decode
import Main.Html exposing (Html)
import Main.Language as Language
import Main.LocalStorage
import Main.RequestManager as RequestManager
import Main.Route as Route
import Main.Settings.Route as Settings
import Main.Settings.View exposing (userIdQrCode)
import Main.Shared as Shared
import Main.TrainerSelect.Route as TrainerSelect
import Main.Translations as Translations
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import Shared.UpdateUtil as UpdateUtil
import Shared.View.Icon exposing (iconHtmlWith)
import Shared.View.Theme as Theme
import Shared.View.Trainer as Trainer
import Svg.Attributes
import UserId


type alias PageModel =
    { trainerState : TrainerState
    }


init : PageModel
init =
    let
        s =
            TrainerState.init
    in
    { trainerState = { s | direction = Direction.D } }


type alias Model m =
    Shared.Session
        { m
            | settingsModel : PageModel
        }


type Msg
    = LanguageChanged Translations.Language
    | ChangedColorTheme Theme.Name
    | ResetLanguage
    | StartMusic
    | StopMusic
    | ToggleReducedMotion Bool


type alias Context msg =
    { lift : Msg -> msg
    , liftShared : Shared.Msg -> msg
    }


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg model =
    case msg of
        LanguageChanged newLang ->
            let
                ( i18n, loadTranslations ) =
                    UpdateUtil.liftCmd ctx.liftShared (Translations.switchLanguage newLang Shared.GotI18n) model.i18n

                requestManagerCmds =
                    RequestManager.onLanguageChange model.requestManager
            in
            ( { model | i18n = i18n, language = Language.setPreferred newLang model.language }
            , Cmd.batch
                [ loadTranslations
                , Main.LocalStorage.saveLanguage newLang
                , requestManagerCmds
                    |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
                ]
            )

        ResetLanguage ->
            let
                newLang =
                    model.language.browser

                ( i18n, withLoadTranslations ) =
                    if Language.isBrowserLanguagePreferred model.language then
                        let
                            ( newI18n, loadTranslations ) =
                                UpdateUtil.liftCmd ctx.liftShared (Translations.switchLanguage newLang Shared.GotI18n) model.i18n
                        in
                        ( newI18n
                        , \cmds -> Cmd.batch [ loadTranslations, cmds ]
                        )

                    else
                        ( model.i18n, identity )
            in
            ( { model | i18n = i18n, language = Language.removePreferred model.language }
            , withLoadTranslations <| Main.LocalStorage.deleteLanguage
            )

        StartMusic ->
            let
                ( requestManager, loadAudioConfig ) =
                    RequestManager.register [ RequestManager.GetAudioTracks {} ] model.requestManager

                cmds =
                    Cmd.batch
                        [ loadAudioConfig |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
                        , Main.LocalStorage.saveAudioPreference True
                        ]
            in
            ( { model | audioIsActive = True, requestManager = requestManager }
            , cmds
            )

        StopMusic ->
            ( { model | audioIsActive = False }
            , Cmd.batch [ Interop.Audio.stop, Main.LocalStorage.saveAudioPreference False ]
            )

        ToggleReducedMotion reduced ->
            ( { model | reducedMotion = reduced }, Main.LocalStorage.saveReducedMotion reduced )

        ChangedColorTheme theme ->
            ( { model | colorThemeName = theme }, Cmd.none )


panel : List (Html msg) -> Html msg
panel =
    div [ class "flex w-full flex-col gap-4 rounded-lg bg-gray-300 p-4 shadow-lg shadow-black/50 bgd:shadow-white/50 dark:bg-gray-700" ]


view : Context msg -> Model m -> List (Html msg)
view ctx model =
    [ viewContent ctx model ]


viewContent : Context msg -> Model m -> Html msg
viewContent ctx model =
    let
        headline =
            h1
                [ class "border-b p-2 text-center text-xl font-bold bgd:border-white bgd:text-white bgl:border-black bgl:text-black"
                ]
                [ model.userData.displayName |> Translations.settingsHeadline |> Atoms.Html.text ]
    in
    div [ class "flex h-full w-full flex-col items-center p-2 dark:text-white" ]
        [ headline
        , div [ class "grid h-full w-full max-w-4xl grid-cols-1 gap-2 pt-2 md:grid-cols-2 lg:grid-cols-3" ]
            [ userIdInformation ctx model
            , colorThemeSelect ctx model.colorThemeName
            , languageSelect ctx model
            , trainerSpriteInformation model
            , musicPlayer ctx model
            , reducedMotionToggle ctx
            ]
        ]


userIdInformation : Context msg -> Model m -> Html msg
userIdInformation ctx { auth } =
    let
        userId =
            Auth.getUserId auth

        userIdAsString =
            UserId.toShortString userId

        qrCode =
            Atoms.Html.a [ class "contents", href <| Route.fromRoute (Route.Settings Settings.QrCodeView) ]
                [ userIdQrCode userId
                    [ Svg.Attributes.class "max-h-12 w-32 bg-gray-200 dark:bg-gray-800 dark:stroke-gray-200"
                    ]
                ]
    in
    panel
        [ h2 [ class "border-b border-black p-2 text-center text-lg font-bold dark:border-white" ] [ Atoms.Html.text Translations.settingsUserIdLabel ]
        , Atoms.Html.text Translations.settingsUserIdDescription
        , div [ class "flex w-full" ]
            [ div
                [ class "w-full min-w-0 content-center rounded-l-lg bg-gray-200 py-2 pl-4 text-gray-600 dark:bg-gray-800 dark:text-gray-300"
                ]
                [ Atoms.Html.truncatedText userIdAsString
                ]
            , qrCode
            , Atoms.Html.button
                [ class "rounded-r-lg bg-red-600 px-4 py-2 text-white"
                , Shared.CopyToClipboard userIdAsString
                    |> ctx.liftShared
                    |> onClick
                ]
                [ Atoms.Html.text Translations.settingsUserIdCopy ]
            ]
        ]


colorThemeSelect : Context msg -> Theme.Name -> Html msg
colorThemeSelect ctx currentTheme =
    let
        currentThemeTranslation =
            case currentTheme of
                Theme.Dark ->
                    Translations.settingsColorThemeDark

                Theme.Light ->
                    Translations.settingsColorThemeLight
    in
    panel
        [ h2 [ class "border-b border-black p-2 text-center text-lg font-bold dark:border-white" ]
            [ Atoms.Html.text Translations.settingsColorThemeLabel ]
        , p [] [ Atoms.Html.text Translations.settingsColorThemeDescription ]
        , p []
            [ Atoms.Html.text Translations.settingsColorThemeCurrent
            , Html.WithContext.text ": "
            , Atoms.Html.text currentThemeTranslation
            ]
        , themeSwitcher (ChangedColorTheme >> ctx.lift)
        ]


themeSwitcher : (Theme.Name -> msg) -> Html msg
themeSwitcher onChange =
    let
        eventDecoder =
            Json.Decode.field "detail" Json.Decode.string
                |> Json.Decode.andThen
                    (\str ->
                        case Theme.fromString str of
                            Just theme ->
                                Json.Decode.succeed <| onChange theme

                            Nothing ->
                                Json.Decode.fail "no theme"
                    )
    in
    Html.WithContext.node "theme-switcher" [ class "self-center", Events.on "theme-change" eventDecoder ] []


languageSelect : Context msg -> Model m -> Html msg
languageSelect ctx model =
    let
        selectedLanguage =
            Language.getActive model.language

        displayLanguage lang =
            case lang of
                Translations.De ->
                    Translations.settingsLanguageGerman

                Translations.En ->
                    Translations.settingsLanguageEnglish

                Translations.Fr ->
                    Translations.settingsLanguageFrench

        languageOption lang =
            option
                [ value <| Translations.languageToString lang
                , selected (lang == selectedLanguage)
                ]
                [ Atoms.Html.text <| displayLanguage lang ]

        changeEventDecoder =
            Json.Decode.at [ "target", "value" ] Json.Decode.string
                |> Json.Decode.andThen
                    (\value ->
                        case Translations.languageFromString value of
                            Just lang ->
                                Json.Decode.succeed lang

                            Nothing ->
                                Json.Decode.fail ("Unknown language: " ++ value)
                    )
    in
    panel
        [ h2 [ class "border-b border-black p-2 text-center text-lg font-bold dark:border-white" ]
            [ Atoms.Html.text Translations.settingsLanguageLabel ]
        , p [] [ Atoms.Html.text Translations.settingsLanguageDescription ]
        , select
            [ class "bg-gray-200 p-2 dark:bg-gray-800"
            , on "change" (Json.Decode.map (LanguageChanged >> ctx.lift) changeEventDecoder)
            ]
          <|
            List.map languageOption Translations.languages
        ]


trainerSpriteInformation : Model m -> Html msg
trainerSpriteInformation ({ settingsModel } as model) =
    case model.userData.trainerSprite of
        Just trainerSprite ->
            panel
                [ h2 [ class "border-b border-black p-2 text-center text-lg font-bold dark:border-white" ] [ Atoms.Html.text Translations.settingsTrainerLabel ]
                , p [] [ Atoms.Html.text Translations.settingsTrainerSpriteDescription ]
                , div [ class "flex items-center justify-around gap-2" ]
                    [ Trainer.trainerSpriteHtml { displaySize = 4 }
                        settingsModel.trainerState
                        { trainerNumber = trainerSprite
                        , displayName = Translations.settingsTrainerLabel model.i18n
                        }
                    , Main.Html.buttonLink
                        { label = Atoms.Html.text Translations.settingsTrainerChange
                        , route = Route.TrainerSelect TrainerSelect.Overview
                        }
                    ]
                ]

        Nothing ->
            Atoms.Html.none


musicPlayer : Context msg -> Model m -> Html msg
musicPlayer ctx model =
    let
        disablePlay =
            if model.audioIsActive then
                [ disabled True, class "opacity-50" ]

            else
                []

        disableStop =
            if model.audioIsActive then
                []

            else
                [ disabled True, class "opacity-50" ]
    in
    panel
        [ h2 [ class "border-b border-black p-2 text-center text-lg font-bold dark:border-white" ] [ Atoms.Html.text Translations.settingsMusicLabel ]
        , p [] [ Atoms.Html.text Translations.settingsMusicDescription ]
        , div [ class "flex w-full justify-around" ]
            [ Atoms.Html.button [ onClick (ctx.lift StartMusic) ]
                [ iconHtmlWith { name = "play", height = 2.5, width = 2.5, description = Translations.settingsMusicPlay }
                    (class "dark:invert" :: disablePlay)
                ]
            , Atoms.Html.button [ onClick (ctx.lift StopMusic) ]
                [ iconHtmlWith { name = "stop", height = 2.5, width = 2.5, description = Translations.settingsMusicStop }
                    (class "dark:invert" :: disableStop)
                ]
            ]
        ]


reducedMotionToggle : Context msg -> Html msg
reducedMotionToggle ctx =
    panel
        [ h2 [ class "border-b border-black p-2 text-center text-lg font-bold dark:border-white" ] [ Atoms.Html.text Translations.settingsReducedMotionLabel ]
        , p [] [ Atoms.Html.text Translations.settingsReducedMotionDescription ]
        , reducedMotionSlider (ToggleReducedMotion >> ctx.lift)
        ]


reducedMotionSlider : (Bool -> msg) -> Html msg
reducedMotionSlider onChange =
    let
        eventDecoder =
            Json.Decode.field "detail" Json.Decode.bool
                |> Json.Decode.map onChange
    in
    node "reduced-motion-slider" [ on "reduced-motion-change" eventDecoder ] []
