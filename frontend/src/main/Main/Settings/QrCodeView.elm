module Main.Settings.QrCodeView exposing (..)

import Atoms.Html as Html
import Auth
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (attribute, class, style)
import Html.WithContext.Events as Events
import Json.Decode
import Main.Html exposing (Html)
import Main.Nav exposing (mainViewportWidth)
import Main.Route as Route
import Main.Settings.Overview as Overview
import Main.Settings.Route as Settings
import Main.Settings.View exposing (userIdQrCode)
import Main.Shared as Shared
import OutsideOf exposing (isOutsideOf)
import Svg.Attributes


type alias Context msg =
    { lift : Msg -> msg, liftShared : Shared.Msg -> msg }


type Msg
    = CloseOverlay


type alias Model m =
    Shared.Session { m | settingsModel : Overview.PageModel }


type alias PageModel =
    {}


init : PageModel
init =
    {}


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update _ msg model =
    case msg of
        CloseOverlay ->
            ( model, Route.navigate model (Route.Settings Settings.Overview) )


view : Context msg -> Model m -> List (Html msg)
view ctx model =
    [ Overview.viewContent { lift = \_ -> ctx.liftShared Shared.NoOp, liftShared = ctx.liftShared }
        model
        |> Html.disable True
    , div
        [ class "fixed right-0 top-0 flex h-full max-h-svh items-center justify-center p-2"
        , Html.WithContext.withContextAttribute <|
            \{ fontSize } ->
                let
                    width =
                        mainViewportWidth { width = model.viewport.width, fontSize = fontSize }
                in
                style "width" (String.fromInt width ++ "px")
        , Events.on "click"
            (isOutsideOf "role" "dialog"
                |> Json.Decode.map (\_ -> ctx.lift CloseOverlay)
            )
        ]
        [ div [ attribute "role" "dialog", class "flex w-full max-w-md flex-col items-center rounded-lg bg-gray-200 pb-4 shadow-lg dark:bg-gray-800" ]
            [ userIdQrCode (Auth.getUserId model.auth) [ Svg.Attributes.class "dark:stroke-gray-200" ]
            ]
        ]
    ]
