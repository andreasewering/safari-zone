module Main.Settings.View exposing (userIdQrCode)

import Atoms.Html
import Html.WithContext
import Main.Html exposing (Html)
import Main.Translations as Translations
import QRCode
import Svg
import UserId exposing (UserId)


userIdQrCode : UserId -> List (Svg.Attribute msg) -> Html msg
userIdQrCode userId attrs =
    UserId.toShortString userId
        |> QRCode.fromString
        |> Result.map
            (QRCode.toSvg
                attrs
            )
        |> Result.map Html.WithContext.html
        |> Result.withDefault (Atoms.Html.text Translations.qrcodeGenerationFailed)
