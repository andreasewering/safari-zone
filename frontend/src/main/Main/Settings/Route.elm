module Main.Settings.Route exposing (Route(..), codec)

import UrlCodec exposing (UrlCodec, buildUnion, s, top, union, variant0)


type Route
    = Overview
    | QrCodeView


codec : UrlCodec (Route -> a) a Route
codec =
    union
        (\overview qrCodeView value ->
            case value of
                Overview ->
                    overview

                QrCodeView ->
                    qrCodeView
        )
        |> variant0 Overview top
        |> variant0 QrCodeView (s "qr")
        |> buildUnion
