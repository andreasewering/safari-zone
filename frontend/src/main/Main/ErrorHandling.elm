module Main.ErrorHandling exposing
    ( AutomaticFix(..)
    , ErrorDetails
    , ErrorHandler(..)
    , ErrorNotification
    , ErrorNotifications
    , ErrorOverlay
    , Session
    , format
    , formatList
    , genericErrorBody
    , handleWebError
    , notFoundImage
    , serverDownErrorBody
    , timeoutImage
    , title
    , unexpectedDataImage
    , viewError
    , viewErrorSmall
    )

import Atoms.Html
import Auth exposing (AuthManager)
import Grpc
import Html.Attributes
import Html.WithContext exposing (div, h1)
import Html.WithContext.Attributes exposing (class)
import Main.Html exposing (Html)
import Main.Translations as Translations exposing (I18n)
import MapId
import Protobuf.Token
import Shared.Types.Error as Error
import Time
import Url
import UserId
import WebGL.Texture


type alias ErrorDetails =
    { trace : String
    , requestedUrl : String
    }


viewError : ErrorOverlay -> Html msg
viewError overlay =
    div [ class "flex h-fit w-full flex-col items-center gap-4 rounded-lg bg-red-200/70 p-4 dark:bg-red-700/70" ]
        [ h1 [ class "text-xl font-bold" ] [ Atoms.Html.text overlay.title ]
        , Html.WithContext.map never overlay.body
        , case overlay.image of
            Just { src, description } ->
                Atoms.Html.image [] { src = src, description = description }

            Nothing ->
                Atoms.Html.none
        ]


viewErrorSmall : ErrorOverlay -> Html msg
viewErrorSmall overlay =
    div [ class "flex h-fit w-full flex-col items-center gap-4 rounded-lg bg-red-200/70 p-4 dark:bg-red-700/70" ]
        [ h1 [ class "text-xl font-bold" ] [ Atoms.Html.text overlay.title ]
        , Html.WithContext.map never overlay.body
        ]


format : ErrorDetails -> String
format { trace, requestedUrl } =
    "Url: " ++ requestedUrl ++ "\n\nTrace: " ++ trace


formatList : List ErrorDetails -> String
formatList =
    List.map format >> String.join "\n\n\n"


type alias ErrorOverlay =
    { title : I18n -> String
    , body : Html Never
    , image : Maybe { src : String, description : I18n -> String }
    }


type alias ErrorNotification =
    { text : I18n -> String
    , image : Maybe { src : String, description : I18n -> String }
    }


type ErrorHandler
    = Blocking ErrorOverlay
    | AutomaticallyFixable ErrorNotification AutomaticFix


type AutomaticFix
    = Retry
    | RefreshToken


type alias ErrorNotifications =
    List
        { notification : ErrorNotification
        , visibleForMs : Int
        }


type alias Session s l r =
    { s | language : l, timezone : Time.Zone, version : String, route : r, auth : AuthManager }


handleWebError :
    Session s l r
    -> { languageToString : l -> String }
    -> { error : Error.Error, path : String }
    -> ErrorHandler
handleWebError session config error =
    case error.error of
        Error.BadBody errMsg ->
            Blocking
                { title = Translations.deserializationErrorTitle
                , body =
                    genericErrorBody
                        { title = "Unexpected response body on " ++ error.path
                        , description = issueTemplate session config errMsg
                        }
                , image = Just unexpectedDataImage
                }

        Error.BadUrl badUrl ->
            Blocking
                { title = Translations.urlErrorTitle
                , body = genericErrorBody { title = "Requested invalid URL", description = issueTemplate session config badUrl }
                , image = Just unexpectedDataImage
                }

        Error.Timeout ->
            AutomaticallyFixable
                { text = Translations.timeoutNotification
                , image = Just timeoutImage
                }
                Retry

        Error.NetworkError ->
            AutomaticallyFixable { text = Translations.offlineNotification, image = Nothing } Retry

        Error.BadStatus statusDetails ->
            handleBadStatus session config { error = statusDetails, path = error.path }

        Error.TextureError WebGL.Texture.LoadError ->
            AutomaticallyFixable
                { text = Translations.loadTextureErrorNotification
                , image = Just notFoundImage
                }
                Retry

        Error.TextureError (WebGL.Texture.SizeError x y) ->
            Blocking
                { title = Translations.deserializationErrorTitle
                , body =
                    genericErrorBody
                        { title = "Failed to load texture " ++ error.path
                        , description = issueTemplate session config ("Texture Size Error: { x: " ++ String.fromInt x ++ ", y: " ++ String.fromInt y ++ " }")
                        }
                , image = Just unexpectedDataImage
                }

        Error.TokenDecodingFailed err ->
            AutomaticallyFixable { text = \_ -> Protobuf.Token.errorToString err, image = Nothing }
                RefreshToken


issueTemplate :
    Session s l r
    -> { languageToString : l -> String }
    -> String
    -> String
issueTemplate session config details =
    Url.percentEncode <|
        """Please describe the issue in a few words and give some context if needed and not present in the autogenerated details below. 
    
---------------------------------

Issue details generated from the application (please do not modify this):

"""
            ++ ([ ( "Error", details )
                , ( "UserId", Auth.getUserId session.auth |> UserId.toShortString )
                , ( "MapId", Auth.getMapId session.auth |> MapId.toHeader )
                , ( "Version", session.version )
                , ( "Language", session.language |> config.languageToString )
                ]
                    |> List.map (\( key, text ) -> key ++ ": " ++ text)
                    |> String.join "\n"
               )


genericErrorBody : { title : String, description : String } -> Html msg
genericErrorBody issue =
    Atoms.Html.translatedHtml [] <|
        Translations.genericErrorText issue
            [ Html.Attributes.class
                "text-sky-600 underline visited:text-pink-600 hover:text-sky-800"
            , Html.Attributes.target "_blank"
            ]


serverDownErrorBody : Html msg
serverDownErrorBody =
    Atoms.Html.translatedHtml [] <| Translations.serverDownText []


handleBadStatus :
    Session s l r
    -> { languageToString : l -> String }
    -> { error : Error.BadStatusDetails, path : String }
    -> ErrorHandler
handleBadStatus session config error =
    let
        serverDown =
            { title = Translations.serverDownTitle
            , body = serverDownErrorBody
            , image = Nothing
            }

        invalidRequest =
            { title = Translations.invalidRequestTitle
            , body = genericErrorBody { title = "Sent invalid request on " ++ error.path, description = issueTemplate session config error.error.response }
            , image = Just unexpectedDataImage
            }
    in
    case ( error.error.httpMetadata.statusCode, error.error.grpcMetadata.status ) of
        ( 401, _ ) ->
            AutomaticallyFixable { text = Translations.unauthorizedError, image = Nothing } RefreshToken

        ( _, Grpc.Unauthenticated ) ->
            AutomaticallyFixable { text = Translations.unauthorizedError, image = Nothing } RefreshToken

        ( 502, _ ) ->
            Blocking serverDown

        ( 503, _ ) ->
            Blocking serverDown

        ( _, Grpc.Internal ) ->
            Blocking serverDown

        ( _, Grpc.Unavailable ) ->
            Blocking serverDown

        ( 400, _ ) ->
            Blocking invalidRequest

        ( _, Grpc.InvalidArgument ) ->
            Blocking invalidRequest

        ( 200, _ ) ->
            Blocking
                { title = Translations.unknownErrorTitle
                , body = genericErrorBody { title = "Unhandled grpc status code on " ++ error.path, description = issueTemplate session config <| "Received unexpected grpc status code with body: " ++ error.error.response }
                , image = Just unexpectedDataImage
                }

        ( unknownHttpStatus, _ ) ->
            Blocking
                { title = Translations.unknownErrorTitle
                , body =
                    genericErrorBody
                        { title = "Unhandled http status code on " ++ error.path
                        , description =
                            issueTemplate session config <|
                                "Received unexpected status code "
                                    ++ String.fromInt unknownHttpStatus
                                    ++ " with body: "
                                    ++ error.error.response
                        }
                , image = Just unexpectedDataImage
                }


title : Error.Error -> I18n -> String
title err =
    case err of
        Error.BadBody _ ->
            Translations.deserializationErrorTitle

        Error.BadUrl _ ->
            Translations.urlErrorTitle

        Error.Timeout ->
            Translations.unknownErrorTitle

        Error.TextureError _ ->
            Translations.deserializationErrorTitle

        Error.TokenDecodingFailed _ ->
            Translations.deserializationErrorTitle

        Error.NetworkError ->
            Translations.unknownErrorTitle

        Error.BadStatus { grpcMetadata } ->
            case grpcMetadata.status of
                Grpc.InvalidArgument ->
                    Translations.invalidRequestTitle

                Grpc.Unauthenticated ->
                    Translations.unknownErrorTitle

                Grpc.NotFound ->
                    Translations.notFoundTitle

                Grpc.Internal ->
                    Translations.serverDownTitle

                Grpc.Unavailable ->
                    Translations.serverDownTitle

                _ ->
                    Translations.unknownErrorTitle


unexpectedDataImage : { src : String, description : I18n -> String }
unexpectedDataImage =
    { src = "/images/psyduck.png", description = Translations.unexpectedDataImageDescription }


timeoutImage : { src : String, description : I18n -> String }
timeoutImage =
    { src = "/images/slowpoke.png", description = Translations.timeoutImageDescription }


notFoundImage : { src : String, description : I18n -> String }
notFoundImage =
    { src = "/images/slowbro.png", description = Translations.notFoundImageDescription }
