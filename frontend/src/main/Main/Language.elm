module Main.Language exposing
    ( ConfigurableLanguage
    , getActive
    , isBrowserLanguagePreferred
    , removePreferred
    , setPreferred
    , toString
    )

import Main.Translations as Translations
import Shared.Types.Generic exposing (Setter)


type alias ConfigurableLanguage =
    { preferred : Maybe Translations.Language
    , browser : Translations.Language
    }


toString : ConfigurableLanguage -> String
toString =
    getActive >> Translations.languageToString


getActive : ConfigurableLanguage -> Translations.Language
getActive { preferred, browser } =
    preferred |> Maybe.withDefault browser


setPreferred : Translations.Language -> Setter ConfigurableLanguage
setPreferred lang c =
    { c | preferred = Just lang }


removePreferred : Setter ConfigurableLanguage
removePreferred c =
    { c | preferred = Nothing }


isBrowserLanguagePreferred : ConfigurableLanguage -> Bool
isBrowserLanguagePreferred { preferred, browser } =
    preferred == Just browser
