module Main.Msg exposing (Msg(..))

import Browser exposing (UrlRequest)
import GroupId exposing (GroupId)
import Json.Decode
import Main.Chat.Msg as Chat
import Main.Contacts.Detail as ContactsDetail
import Main.Contacts.Overview as ContactsOverview
import Main.Dex.Appraisal as DexAppraisal
import Main.Dex.Detail as DexDetail
import Main.MapSelect as MapSelect
import Main.PokemonSelect as PokemonSelect
import Main.Safari.Overworld as Safari
import Main.Settings.Overview as SettingsOverview
import Main.Settings.QrCodeView as QrCodeView
import Main.Shared as Shared
import Main.TrainerSelect.Detail as TrainerDetail
import Main.TrainerSelect.Overview as TrainerOverview
import PokemonNumber exposing (PokemonNumber)
import Time
import TrainerNumber exposing (TrainerNumber)
import Url exposing (Url)


type Msg
    = -- Navigation
      OnUrlChange Url
    | OnUrlRequest UrlRequest
    | Tick Time.Posix
    | ClickedNav
    | ChatMessage Chat.Msg
    | AuthRefreshTime Time.Posix
    | Logout
    | GotTimezone Time.Zone
    | CheckBackground Time.Posix
    | FailedToDecodeMessageFromJS Json.Decode.Error
      -- Errors
    | CloseErrorOverlay
      -- Resizing
    | Resize Int Int
      -- Pages
    | DexDetailMessage PokemonNumber DexDetail.Msg
    | DexAppraisalMessage DexAppraisal.Msg
    | ContactsMessage ContactsOverview.Msg
    | GroupDetailMessage GroupId ContactsDetail.Msg
    | SafariPageMessage Safari.Msg
    | SettingsMessage SettingsOverview.Msg
    | QrCodeViewMessage QrCodeView.Msg
    | TrainerSelectMessage TrainerOverview.Msg
    | TrainerDetailMessage TrainerNumber TrainerDetail.Msg
    | PokemonSelectMessage PokemonSelect.Msg
    | MapSelectMessage MapSelect.Msg
    | SharedMessage Shared.Msg
