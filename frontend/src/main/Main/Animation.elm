module Main.Animation exposing (..)

import Animator exposing (Animator)
import Main.Model exposing (Model)
import Main.Safari.Overworld.Transition
import Time


animator : Animator Model
animator =
    Animator.animator
        |> Animator.watchingWith .mapTransitionTimeline
            (\t s -> { s | mapTransitionTimeline = t })
            (not << Main.Safari.Overworld.Transition.isSettledState)
        |> Animator.watchingWith (.dexModel >> .transitionTimeline)
            (\t ({ dexModel } as m) -> { m | dexModel = { dexModel | transitionTimeline = t } })
            (always False)
        |> Animator.watchingWith (.safariModel >> .isBackpackOpen)
            (\t ({ safariModel } as m) -> { m | safariModel = { safariModel | isBackpackOpen = t } })
            (always False)


onTick : Time.Posix -> Model -> Model
onTick newTime model =
    Animator.update newTime animator model


subscriptions : (Time.Posix -> msg) -> Model -> Sub msg
subscriptions callback model =
    animator
        |> Animator.toSubscription callback model
