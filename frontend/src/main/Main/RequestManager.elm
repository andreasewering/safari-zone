module Main.RequestManager exposing
    ( ApiCall(..)
    , Events
    , Msg
    , RequestManager
    , Status(..)
    , init
    , invalidate
    , onLanguageChange
    , register
    , status
    , unregister
    , update
    )

import Auth
import Base64
import Dict exposing (Dict)
import Grpc
import Http
import Item exposing (Item)
import Lens exposing (Lens)
import Main.ErrorHandling as ErrorHandling
import Main.Language as Language exposing (ConfigurableLanguage)
import Main.Route exposing (Route)
import Main.Translations as Translations exposing (I18n)
import Maybe.Extra
import Platform exposing (Task)
import PokemonNumber exposing (PokemonNumber)
import Process
import Proto.Arceus as Arceus exposing (GetAreasRequest, GetAreasResponse, GetAudioTracksRequest, GetAudioTracksResponse)
import Proto.Arceus.ArceusService as ArceusService
import Proto.Chatot as Chatot exposing (AddFriendRequest, AddFriendResponse, AddToGroupRequest, AddToGroupResponse, CreateGroupRequest, CreateGroupResponse, GetFriendRequestsRequest, GetFriendRequestsResponse, GetFriendsRequest, GetFriendsResponse, GetGroupDetailRequest, GetGroupDetailResponse, GetGroupsRequest, GetGroupsResponse, RemoveFriendRequest, RemoveFriendResponse)
import Proto.Chatot.ChatotService as ChatotService
import Proto.Gardevoir.GardevoirService as GardevoirService
import Proto.Google.Protobuf as Protobuf
import Proto.Kangaskhan as Kangaskhan exposing (ChooseStartTileRequest, ChooseStartTileResponse, GetDexEntriesRequest, GetDexEntriesResponse, GetDexEntryDetailRequest, GetDexEntryDetailResponse, GetOtherUserDataRequest, GetOtherUserDataResponse, GetPokemonRequest, GetPokemonResponse, GetStarterPokemonRequest, GetStarterPokemonResponse, GetTrainersRequest, GetTrainersResponse, GetUserDataRequest, GetUserDataResponse, GetUserItemsRequest, GetUserItemsResponse, SetPartnerPokemonRequest, SetPartnerPokemonResponse, SetTrainerSpriteRequest, SetTrainerSpriteResponse)
import Proto.Kangaskhan.KangaskhanService as KangaskhanService
import Proto.Smeargle as Smeargle exposing (GetAllStartTilesRequest, GetAllStartTilesResponse, GetAnimationConfigRequest, GetAnimationConfigResponse, GetTextureMetadataRequest, GetTextureMetadataResponse, GetTilesRequest, GetTilesResponse, GetTilesetConfigRequest, GetTilesetConfigResponse)
import Proto.Smeargle.SmeargleService as SmeargleService
import Protobuf.Encode
import Result.Extra
import Shared.Types.Error as Error
import Shared.Types.TrainerSprite as TrainerSprite
import Shared.UpdateUtil as UpdateUtil
import Tagged exposing (untag)
import Task
import Textures.Api
import Textures.Store exposing (Sprite, Tilemaps)
import Time
import TrainerNumber exposing (TrainerNumber)
import WebGL.Texture as Texture exposing (nonPowerOfTwoOptions)


type ApiCall
    = -- Kangaskhan
      GetPokemon GetPokemonRequest
    | GetUserData GetUserDataRequest
    | GetOtherUserData GetOtherUserDataRequest
    | GetDexEntries GetDexEntriesRequest
    | GetDexEntryDetail GetDexEntryDetailRequest
    | GetTrainers GetTrainersRequest
    | GetStarterPokemon GetStarterPokemonRequest
    | GetUserItems GetUserItemsRequest
    | SetTrainerSprite SetTrainerSpriteRequest
    | SetPartnerPokemon SetPartnerPokemonRequest
    | ChooseStartTile ChooseStartTileRequest
      -- Smeargle
    | GetAllStartTiles GetAllStartTilesRequest
    | GetTiles GetTilesRequest
    | GetAnimationConfig GetAnimationConfigRequest
    | GetTilesetConfig GetTilesetConfigRequest
    | GetTextureMetadata GetTextureMetadataRequest
      -- Arceus
    | GetAudioTracks GetAudioTracksRequest
    | GetAreas GetAreasRequest
      -- Chatot
    | GetFriends GetFriendsRequest
    | GetFriendRequests GetFriendRequestsRequest
    | GetGroups GetGroupsRequest
    | GetGroupDetail GetGroupDetailRequest
    | AddFriend AddFriendRequest
    | RemoveFriend RemoveFriendRequest
    | CreateGroup CreateGroupRequest
    | AddToGroup AddToGroupRequest
      -- Textures
    | LoadTilesetTexture GetTilesetConfigResponse -- Do not construct in "user space", instead use `GetTilesetConfig`
    | LoadPokemonTexture PokemonNumber
    | LoadTrainerTexture TrainerNumber
    | LoadItemTexture Item
    | LoadTilemapTexture GetTextureMetadataRequest GetTextureMetadataResponse
      -- Gardevoir
    | RefreshAuth


init : RequestManager
init =
    RequestManager { registered = [], grpc = Dict.empty }


type ApiResponse
    = -- Kangaskhan
      GetPokemonRes GetPokemonRequest (Result Grpc.Error GetPokemonResponse)
    | GetUserDataRes GetUserDataRequest (Result Grpc.Error GetUserDataResponse)
    | GetOtherUserDataRes GetOtherUserDataRequest (Result Grpc.Error GetOtherUserDataResponse)
    | GetDexEntriesRes GetDexEntriesRequest (Result Grpc.Error GetDexEntriesResponse)
    | GetDexEntryDetailRes GetDexEntryDetailRequest (Result Grpc.Error GetDexEntryDetailResponse)
    | GetTrainersRes GetTrainersRequest (Result Grpc.Error GetTrainersResponse)
    | GetStarterPokemonRes GetStarterPokemonRequest (Result Grpc.Error GetStarterPokemonResponse)
    | GetUserItemsRes GetUserItemsRequest (Result Grpc.Error GetUserItemsResponse)
    | SetTrainerSpriteRes SetTrainerSpriteRequest (Result Grpc.Error SetTrainerSpriteResponse)
    | SetPartnerPokemonRes SetPartnerPokemonRequest (Result Grpc.Error SetPartnerPokemonResponse)
    | ChooseStartTileRes ChooseStartTileRequest (Result Grpc.Error ChooseStartTileResponse)
      -- Smeargle
    | GetAllStartTilesRes GetAllStartTilesRequest (Result Grpc.Error GetAllStartTilesResponse)
    | GetTilesRes GetTilesRequest (Result Grpc.Error GetTilesResponse)
    | GetAnimationConfigRes GetAnimationConfigRequest (Result Grpc.Error GetAnimationConfigResponse)
    | GetTextureMetadataRes GetTextureMetadataRequest (Result Grpc.Error GetTextureMetadataResponse)
      -- Arceus
    | GetAudioTracksRes GetAudioTracksRequest (Result Grpc.Error GetAudioTracksResponse)
    | GetAreasRes GetAreasRequest (Result Grpc.Error GetAreasResponse)
    | GetTilesetConfigRes GetTilesetConfigRequest (Result Grpc.Error GetTilesetConfigResponse)
      -- Chatot
    | GetFriendsRes GetFriendsRequest (Result Grpc.Error GetFriendsResponse)
    | GetFriendRequestsRes GetFriendRequestsRequest (Result Grpc.Error GetFriendRequestsResponse)
    | GetGroupsRes GetGroupsRequest (Result Grpc.Error GetGroupsResponse)
    | GetGroupDetailRes GetGroupDetailRequest (Result Grpc.Error GetGroupDetailResponse)
    | AddFriendRes AddFriendRequest (Result Grpc.Error AddFriendResponse)
    | RemoveFriendRes RemoveFriendRequest (Result Grpc.Error RemoveFriendResponse)
    | CreateGroupRes CreateGroupRequest (Result Grpc.Error CreateGroupResponse)
    | AddToGroupRes AddToGroupRequest (Result Grpc.Error AddToGroupResponse)
      -- Textures
    | LoadTilesetTextureRes GetTilesetConfigResponse (Result Texture.Error Sprite)
    | LoadPokemonTextureRes PokemonNumber (Result Texture.Error Sprite)
    | LoadTrainerTextureRes TrainerNumber (Result Texture.Error Sprite)
    | LoadItemTextureRes Item (Result Texture.Error Sprite)
    | LoadTilemapTextureRes GetTextureMetadataRequest GetTextureMetadataResponse (Result Texture.Error Tilemaps)
    | RefreshAuthRes (Result Error.Error Auth.Authentification)


status : ApiCall -> RequestManager -> Status Error.Error
status call manager =
    let
        identifier =
            serializeApiCall call

        get id =
            id
                |> grpcRequestState
                |> .get
                |> (|>) { requestManager = manager }
    in
    get identifier
        -- The status of the most recent subrequest (if any) is the status we are interested in
        |> Maybe.map (\state -> List.head state.subrequests |> Maybe.andThen get |> Maybe.withDefault state)
        |> Maybe.map (toExternalStatus identifier.path)
        |> Maybe.withDefault NotAsked


type Status e
    = Loading
    | Failure { path : String, error : e }
    | Success
    | NotAsked


type alias Events msg =
    { -- Internal messages
      onCallUpdate : Msg -> msg
    , -- If a request failed, this enables the application to react
      onFail : ApiCall -> Error.Error -> msg
    , -- one event per http request, just signal success
      onAuthRefresh : { previousAuth : Auth.AuthManager } -> msg

    -- Kangaskhan
    , onGetPokemon : GetPokemonRequest -> GetPokemonResponse -> msg
    , onGetUserData : GetUserDataRequest -> GetUserDataResponse -> msg
    , onGetOtherUserData : GetOtherUserDataRequest -> GetOtherUserDataResponse -> msg
    , onGetDexEntries : GetDexEntriesRequest -> GetDexEntriesResponse -> msg
    , onGetDexEntryDetail : GetDexEntryDetailRequest -> GetDexEntryDetailResponse -> msg
    , onGetTrainers : GetTrainersRequest -> GetTrainersResponse -> msg
    , onGetStarterPokemon : GetStarterPokemonRequest -> GetStarterPokemonResponse -> msg
    , onGetUserItems : GetUserItemsRequest -> GetUserItemsResponse -> msg
    , onSetTrainerSprite : SetTrainerSpriteRequest -> SetTrainerSpriteResponse -> msg
    , onSetPartnerPokemon : SetPartnerPokemonRequest -> SetPartnerPokemonResponse -> msg
    , onChooseStartTile : ChooseStartTileRequest -> ChooseStartTileResponse -> msg

    -- Smeargle
    , onGetAllStartTiles : GetAllStartTilesRequest -> GetAllStartTilesResponse -> msg
    , onGetTiles : GetTilesRequest -> GetTilesResponse -> msg
    , onGetAnimationConfig : GetAnimationConfigRequest -> GetAnimationConfigResponse -> msg

    -- Arceus
    , onGetAudioTracks : GetAudioTracksRequest -> GetAudioTracksResponse -> msg
    , onGetAreas : GetAreasRequest -> GetAreasResponse -> msg

    -- Chatot
    , onGetFriends : GetFriendsRequest -> GetFriendsResponse -> msg
    , onGetFriendRequests : GetFriendRequestsRequest -> GetFriendRequestsResponse -> msg
    , onGetGroups : GetGroupsRequest -> GetGroupsResponse -> msg
    , onGetGroupDetail : GetGroupDetailRequest -> GetGroupDetailResponse -> msg
    , onAddFriend : AddFriendRequest -> AddFriendResponse -> msg
    , onRemoveFriend : AddFriendRequest -> AddFriendResponse -> msg
    , onCreateGroup : CreateGroupRequest -> CreateGroupResponse -> msg
    , onAddToGroup : AddToGroupRequest -> AddToGroupResponse -> msg

    -- Textures
    , onLoadTilesetTexture : GetTilesetConfigResponse -> Sprite -> msg
    , onLoadPokemonTexture : PokemonNumber -> Sprite -> msg
    , onLoadTrainerTexture : TrainerNumber -> Sprite -> msg
    , onLoadItemTexture : Item -> Sprite -> msg
    , onLoadTilemapTexture : GetTextureMetadataRequest -> GetTextureMetadataResponse -> Tilemaps -> msg
    }


handleError : ApiCall -> Error.Error -> Session s -> ( Session s, Cmd Msg )
handleError apiCall err session =
    case ErrorHandling.handleWebError session { languageToString = Language.toString } { error = err, path = serializeApiCall apiCall |> .path } of
        ErrorHandling.AutomaticallyFixable _ ErrorHandling.Retry ->
            ( session
            , Process.sleep 1000
                |> Task.andThen (\_ -> retryTask apiCall)
                |> Task.perform identity
            )

        ErrorHandling.AutomaticallyFixable _ ErrorHandling.RefreshToken ->
            let
                ( manager, cmds ) =
                    register [ RefreshAuth ] session.requestManager
            in
            ( { session | requestManager = manager }, cmds )

        ErrorHandling.Blocking _ ->
            ( session, Cmd.none )


type alias CacheStrategy =
    { forMillis : Int
    , refreshIfLanguageChanges : Bool
    }


defaultCacheStrategy : CacheStrategy
defaultCacheStrategy =
    { forMillis = 300000 -- 5 Minutes
    , refreshIfLanguageChanges = True
    }


mutationCacheStrategy : CacheStrategy
mutationCacheStrategy =
    { forMillis = 2000, refreshIfLanguageChanges = False }


type alias RetryStrategy =
    { maxRetries : Int, initialBackoffInMillis : Int }


defaultRetryStrategy : RetryStrategy
defaultRetryStrategy =
    { maxRetries = 6, initialBackoffInMillis = 500 }



-- | General idea:
-- This module manages http/grpc requests in the sense that it
-- - Supports tracking progress via Http.track
-- - Tracks starting and finishing times of requests
-- - Allows caching via different strategies
-- - Can automatically resend requests if a token is updated or the preferred language is changed
-- - Automatically handles retries according to a specified error handling strategy
--
-- Most of the code in here should be generic and should not need adjustment when new calls are added
-- The external interface should be slim and consist of the following parts:
-- - Registering/Unregistering requests
-- - Customizing Error Handling and Caching
-- - Getting the status of requests (Loading/Success/Failure)


type RequestManager
    = RequestManager
        { grpc : Dict ( String, String ) GrpcRequestState
        , registered : List ApiCall
        }


type alias GrpcRequestState =
    { cacheStrategy : CacheStrategy
    , retryStrategy : RetryStrategy
    , error : Maybe Error.Error
    , loadingSince : Maybe Time.Posix
    , retryCount : Int
    , lastResponseAt : Maybe Time.Posix
    , language : Maybe Translations.Language
    , subrequests : List RequestIdentifier
    }


initialRequestState : CacheStrategy -> GrpcRequestState
initialRequestState cacheStrat =
    { cacheStrategy = cacheStrat
    , retryStrategy = defaultRetryStrategy
    , error = Nothing
    , loadingSince = Nothing
    , retryCount = 0
    , lastResponseAt = Nothing
    , language = Nothing
    , subrequests = []
    }


toExternalStatus : String -> GrpcRequestState -> Status Error.Error
toExternalStatus path { error, lastResponseAt, loadingSince } =
    case ( error, lastResponseAt, loadingSince ) of
        ( Just e, _, _ ) ->
            Failure { path = path, error = e }

        ( Nothing, Nothing, _ ) ->
            Loading

        ( Nothing, Just _, Nothing ) ->
            Success

        ( Nothing, Just lastResponseAt_, Just loadingSince_ ) ->
            if Time.posixToMillis lastResponseAt_ >= Time.posixToMillis loadingSince_ then
                Success

            else
                Loading


grpcRequestState : RequestIdentifier -> Lens (Maybe GrpcRequestState) { s | requestManager : RequestManager }
grpcRequestState identifier =
    let
        grpcLens : Lens (Dict ( String, String ) GrpcRequestState) RequestManager
        grpcLens =
            { get = \(RequestManager r) -> r.grpc, set = \v (RequestManager r) -> RequestManager { r | grpc = v } }

        requestManagerLens =
            { get = \{ requestManager } -> requestManager, set = \r v -> { v | requestManager = r } }
    in
    requestManagerLens
        |> Lens.compose grpcLens
        |> Lens.compose (Lens.dict <| identifierToComparable identifier)


type Msg
    = TriggerSubrequest RequestIdentifier ApiCall
    | RequestStart { call : ApiCall, now : Time.Posix, isRetry : Bool }
    | RequestEnd RequestIdentifier Time.Posix
    | AuthRefreshed Auth.Authentification
    | GotResponse ApiCall ApiResponse


type alias RequestIdentifier =
    { serializedRequest : String, path : String }


type alias Session s =
    { s
        | auth : Auth.AuthManager
        , i18n : I18n
        , language : ConfigurableLanguage
        , requestManager : RequestManager
        , timezone : Time.Zone
        , version : String
        , route : Route
    }


register : List ApiCall -> RequestManager -> ( RequestManager, Cmd Msg )
register calls manager =
    ( List.foldl registerInternal manager calls
    , List.map begin calls |> Cmd.batch
    )


invalidate : List ApiCall -> RequestManager -> ( RequestManager, Cmd Msg )
invalidate calls =
    UpdateUtil.chain [ unregister calls, register calls ]


unregister : List ApiCall -> RequestManager -> ( RequestManager, Cmd msg )
unregister calls manager =
    ( List.foldl unregisterInternal manager calls
    , List.map (serializeApiCall >> identifierToTracker >> Http.cancel) calls |> Cmd.batch
    )


begin : ApiCall -> Cmd Msg
begin call =
    Time.now |> Task.map (\now -> RequestStart { call = call, now = now, isRetry = False }) |> Task.perform identity


retryTask : ApiCall -> Task e Msg
retryTask call =
    Time.now |> Task.map (\now -> RequestStart { call = call, now = now, isRetry = True })


retry : ApiCall -> Cmd Msg
retry call =
    retryTask call |> Task.perform identity


update : Events msg -> Msg -> Session s -> ( Session s, Cmd msg )
update events msg session =
    case msg of
        TriggerSubrequest mainRequestIdentifier apiCall ->
            ( addSubrequest apiCall
                |> Maybe.map
                |> Lens.update (grpcRequestState mainRequestIdentifier)
                |> (|>) session
            , begin apiCall |> Cmd.map events.onCallUpdate
            )

        RequestStart { call, isRetry, now } ->
            let
                lens =
                    grpcRequestState <| serializeApiCall call

                existing =
                    lens.get session

                activeLanguage =
                    Language.getActive session.language

                shouldCacheBasedOnLanguage =
                    Maybe.map2
                        (\state language ->
                            activeLanguage == language || not state.cacheStrategy.refreshIfLanguageChanges
                        )
                        existing
                        (existing |> Maybe.andThen .language)
                        |> Maybe.withDefault False

                shouldCacheBasedOnTime =
                    Maybe.map2
                        (\state lastResponseAt ->
                            Time.posixToMillis now <= Time.posixToMillis lastResponseAt + state.cacheStrategy.forMillis
                        )
                        existing
                        (existing |> Maybe.andThen .lastResponseAt)
                        |> Maybe.withDefault False
            in
            if not isRetry && shouldCacheBasedOnTime && shouldCacheBasedOnLanguage then
                ( session, Cmd.none )

            else
                ( (setRequestStart now
                    >> setLanguage activeLanguage
                  )
                    |> Maybe.map
                    |> Lens.update lens
                    |> (|>) session
                , executeRequest session call |> Cmd.map (GotResponse call >> events.onCallUpdate)
                )

        RequestEnd identifier time ->
            ( setRequestEnd time
                |> Maybe.map
                |> Lens.update (grpcRequestState identifier)
                |> (|>) session
            , Cmd.none
            )

        GotResponse apiCall response ->
            let
                identifier =
                    serializeApiCall apiCall

                handleErrorResult =
                    Maybe.map (handleError apiCall)
                        >> Maybe.withDefault UpdateUtil.none

                ( newSession, cmds ) =
                    setResponse response
                        |> Maybe.map
                        |> Lens.update (grpcRequestState identifier)
                        |> (|>) session
                        |> handleErrorResult (responseToError response)
            in
            ( newSession
            , Cmd.batch
                [ Time.now |> Task.perform (RequestEnd identifier >> events.onCallUpdate)
                , emitSuccessEvent events response
                , cmds |> Cmd.map events.onCallUpdate
                ]
            )

        AuthRefreshed auth ->
            let
                retries =
                    retrieveRegistered session.requestManager
                        |> List.filter (\( _, state ) -> Maybe.Extra.isJust state.error)
                        |> List.map (Tuple.first >> retry)
                        |> Cmd.batch
                        |> Cmd.map events.onCallUpdate
            in
            ( { session
                | auth =
                    Auth.addToken auth session.auth
                        |> (Maybe.map (\mapId -> Auth.switchMap mapId >> Tuple.first) auth.mapId
                                |> Maybe.withDefault identity
                           )
              }
            , Cmd.batch [ retries, events.onAuthRefresh { previousAuth = session.auth } |> dispatch ]
            )


onLanguageChange : RequestManager -> Cmd Msg
onLanguageChange manager =
    let
        executeWithChangedLanguage =
            retrieveRegistered manager
                |> List.filter (\( _, state ) -> state.cacheStrategy.refreshIfLanguageChanges)
                |> List.map (Tuple.first >> begin)
                |> Cmd.batch
    in
    executeWithChangedLanguage


serializeApiCall : ApiCall -> RequestIdentifier
serializeApiCall call =
    let
        make :
            (req -> Protobuf.Encode.Encoder)
            -> req
            -> Grpc.Rpc req res
            -> { serializedRequest : String, path : String }
        make enc req rpc =
            { serializedRequest = enc req |> encoderToString, path = Grpc.rpcPath rpc }
    in
    case call of
        GetPokemon req ->
            make Kangaskhan.encodeGetPokemonRequest req KangaskhanService.getPokemon

        GetUserData req ->
            make Kangaskhan.encodeGetUserDataRequest req KangaskhanService.getUserData

        GetOtherUserData req ->
            make Kangaskhan.encodeGetOtherUserDataRequest req KangaskhanService.getOtherUserData

        GetDexEntries req ->
            make Kangaskhan.encodeGetDexEntriesRequest req KangaskhanService.getDexEntries

        GetDexEntryDetail req ->
            make Kangaskhan.encodeGetDexEntryDetailRequest req KangaskhanService.getDexEntryDetail

        GetTrainers req ->
            make Kangaskhan.encodeGetTrainersRequest req KangaskhanService.getTrainers

        GetStarterPokemon req ->
            make Kangaskhan.encodeGetStarterPokemonRequest req KangaskhanService.getStarterPokemon

        GetUserItems req ->
            make Kangaskhan.encodeGetUserItemsRequest req KangaskhanService.getUserItems

        SetTrainerSprite req ->
            make Kangaskhan.encodeSetTrainerSpriteRequest req KangaskhanService.setTrainerSprite

        SetPartnerPokemon req ->
            make Kangaskhan.encodeSetPartnerPokemonRequest req KangaskhanService.setPartnerPokemon

        ChooseStartTile req ->
            make Kangaskhan.encodeChooseStartTileRequest req KangaskhanService.chooseStartTile

        GetAllStartTiles req ->
            make Smeargle.encodeGetAllStartTilesRequest req SmeargleService.getAllStartTiles

        GetTiles req ->
            make Smeargle.encodeGetTilesRequest req SmeargleService.getTiles

        GetAnimationConfig req ->
            make Smeargle.encodeGetAnimationConfigRequest req SmeargleService.getAnimationConfig

        GetTextureMetadata req ->
            make Smeargle.encodeGetTextureMetadataRequest req SmeargleService.getTextureMetadata

        GetTilesetConfig req ->
            make Smeargle.encodeGetTilesetConfigRequest req SmeargleService.getTilesetConfig

        GetAudioTracks req ->
            make Arceus.encodeGetAudioTracksRequest req ArceusService.getAudioTracks

        GetAreas req ->
            make Arceus.encodeGetAreasRequest req ArceusService.getAreas

        GetFriends req ->
            make Chatot.encodeGetFriendsRequest req ChatotService.getFriends

        GetFriendRequests req ->
            make Chatot.encodeGetFriendRequestsRequest req ChatotService.getFriendRequests

        GetGroups req ->
            make Chatot.encodeGetGroupsRequest req ChatotService.getGroups

        GetGroupDetail req ->
            make Chatot.encodeGetGroupDetailRequest req ChatotService.getGroupDetail

        AddFriend req ->
            make Chatot.encodeAddFriendRequest req ChatotService.addFriend

        RemoveFriend req ->
            make Chatot.encodeRemoveFriendRequest req ChatotService.removeFriend

        CreateGroup req ->
            make Chatot.encodeCreateGroupRequest req ChatotService.createGroup

        AddToGroup req ->
            make Chatot.encodeAddToGroupRequest req ChatotService.addToGroup

        LoadTilesetTexture config ->
            tilesetIdentifier config

        LoadPokemonTexture pokemonNumber ->
            { serializedRequest = String.fromInt <| untag pokemonNumber, path = "pokemonTexture" }

        LoadTrainerTexture trainerNumber ->
            { serializedRequest = String.fromInt <| untag trainerNumber, path = "trainerTexture" }

        LoadItemTexture item ->
            { serializedRequest = "", path = Item.toSpritePath item }

        LoadTilemapTexture _ metadataRes ->
            { serializedRequest = "", path = metadataRes.path }

        RefreshAuth ->
            make Protobuf.encodeEmpty {} GardevoirService.refresh


executeRequest : Session s -> ApiCall -> Cmd ApiResponse
executeRequest session call =
    let
        make rpc req toRes =
            grpc
                { rpc = rpc
                , body = req
                , session = session
                , identifier = serializeApiCall call
                }
                |> Cmd.map (toRes req)
    in
    case call of
        GetPokemon req ->
            make KangaskhanService.getPokemon req GetPokemonRes

        GetUserData req ->
            make KangaskhanService.getUserData req GetUserDataRes

        GetOtherUserData req ->
            make KangaskhanService.getOtherUserData req GetOtherUserDataRes

        GetDexEntries req ->
            make KangaskhanService.getDexEntries req GetDexEntriesRes

        GetDexEntryDetail req ->
            make KangaskhanService.getDexEntryDetail req GetDexEntryDetailRes

        GetTrainers req ->
            make KangaskhanService.getTrainers req GetTrainersRes

        GetStarterPokemon req ->
            make KangaskhanService.getStarterPokemon req GetStarterPokemonRes

        GetUserItems req ->
            make KangaskhanService.getUserItems req GetUserItemsRes

        SetTrainerSprite req ->
            make KangaskhanService.setTrainerSprite req SetTrainerSpriteRes

        SetPartnerPokemon req ->
            make KangaskhanService.setPartnerPokemon req SetPartnerPokemonRes

        ChooseStartTile req ->
            make KangaskhanService.chooseStartTile req ChooseStartTileRes

        GetAllStartTiles req ->
            make SmeargleService.getAllStartTiles req GetAllStartTilesRes

        GetTiles req ->
            make SmeargleService.getTiles req GetTilesRes

        GetAnimationConfig req ->
            make SmeargleService.getAnimationConfig req GetAnimationConfigRes

        GetTilesetConfig req ->
            make SmeargleService.getTilesetConfig req GetTilesetConfigRes

        GetTextureMetadata req ->
            make SmeargleService.getTextureMetadata req GetTextureMetadataRes

        GetAudioTracks req ->
            make ArceusService.getAudioTracks req GetAudioTracksRes

        GetAreas req ->
            make ArceusService.getAreas req GetAreasRes

        GetFriends req ->
            make ChatotService.getFriends req GetFriendsRes

        GetFriendRequests req ->
            make ChatotService.getFriendRequests req GetFriendRequestsRes

        GetGroups req ->
            make ChatotService.getGroups req GetGroupsRes

        GetGroupDetail req ->
            make ChatotService.getGroupDetail req GetGroupDetailRes

        AddFriend req ->
            make ChatotService.addFriend req AddFriendRes

        RemoveFriend req ->
            make ChatotService.removeFriend req RemoveFriendRes

        CreateGroup req ->
            make ChatotService.createGroup req CreateGroupRes

        AddToGroup req ->
            make ChatotService.addToGroup req AddToGroupRes

        LoadTilesetTexture config ->
            Texture.loadWith
                { nonPowerOfTwoOptions | magnify = Texture.nearest, minify = Texture.nearest }
                config.tilesetPath
                |> Task.map
                    (\texture ->
                        let
                            ( _, height ) =
                                Texture.size texture

                            rows =
                                height // config.tileSizeInPx
                        in
                        { texture = texture
                        , rows = rows
                        , columns = config.tilesPerRow
                        , path = config.tilesetPath
                        }
                    )
                |> Task.attempt (LoadTilesetTextureRes config)

        LoadPokemonTexture pokemonNumber ->
            let
                path =
                    Textures.Api.pokemonTexturePath pokemonNumber

                fromTexture texture =
                    { texture = texture, columns = 4, rows = 4, path = path }
            in
            Texture.load path
                |> Task.map fromTexture
                |> Task.attempt (LoadPokemonTextureRes pokemonNumber)

        LoadTrainerTexture trainerNumber ->
            let
                path =
                    TrainerSprite.trainerNumberToPath trainerNumber

                fromTexture texture =
                    { texture = texture, columns = 4, rows = 4, path = path }
            in
            Texture.load path
                |> Task.map fromTexture
                |> Task.attempt (LoadTrainerTextureRes trainerNumber)

        LoadItemTexture item ->
            let
                path =
                    Item.toSpritePath item

                fromTexture texture =
                    { texture = texture, columns = 1, rows = 17, path = path }
            in
            Texture.loadWith nonPowerOfTwoOptions path
                |> Task.map fromTexture
                |> Task.attempt (LoadItemTextureRes item)

        LoadTilemapTexture metadataReq metadataRes ->
            Texture.loadWith { nonPowerOfTwoOptions | magnify = Texture.nearest } metadataRes.path
                |> Task.map
                    (\texture ->
                        { texture = texture
                        , offset = { x = metadataRes.offsetX, y = metadataRes.offsetY }
                        , layers = metadataRes.layers
                        }
                    )
                |> Task.attempt (LoadTilemapTextureRes metadataReq metadataRes)

        RefreshAuth ->
            Auth.refreshAuth |> Cmd.map RefreshAuthRes


emitSuccessEvent : Events msg -> ApiResponse -> Cmd msg
emitSuccessEvent events response =
    let
        make event req res toCall mapErr =
            case res of
                Ok ok ->
                    dispatch (event req ok)

                Err err ->
                    dispatch (events.onFail (toCall req) (mapErr err))
    in
    case response of
        GetPokemonRes req res ->
            make events.onGetPokemon req res GetPokemon Error.fromGrpcError

        GetUserDataRes req res ->
            make events.onGetUserData req res GetUserData Error.fromGrpcError

        GetOtherUserDataRes req res ->
            make events.onGetOtherUserData req res GetOtherUserData Error.fromGrpcError

        GetDexEntriesRes req res ->
            make events.onGetDexEntries req res GetDexEntries Error.fromGrpcError

        GetDexEntryDetailRes req res ->
            make events.onGetDexEntryDetail req res GetDexEntryDetail Error.fromGrpcError

        GetTrainersRes req res ->
            make events.onGetTrainers req res GetTrainers Error.fromGrpcError

        GetStarterPokemonRes req res ->
            make events.onGetStarterPokemon req res GetStarterPokemon Error.fromGrpcError

        GetUserItemsRes req res ->
            make events.onGetUserItems req res GetUserItems Error.fromGrpcError

        SetTrainerSpriteRes req res ->
            make events.onSetTrainerSprite req res SetTrainerSprite Error.fromGrpcError

        SetPartnerPokemonRes req res ->
            make events.onSetPartnerPokemon req res SetPartnerPokemon Error.fromGrpcError

        ChooseStartTileRes req res ->
            make events.onChooseStartTile req res ChooseStartTile Error.fromGrpcError

        GetAllStartTilesRes req res ->
            make events.onGetAllStartTiles req res GetAllStartTiles Error.fromGrpcError

        GetTilesRes req res ->
            make events.onGetTiles req res GetTiles Error.fromGrpcError

        GetAnimationConfigRes req res ->
            make events.onGetAnimationConfig req res GetAnimationConfig Error.fromGrpcError

        GetTilesetConfigRes req res ->
            make
                (\_ r ->
                    TriggerSubrequest (serializeApiCall <| GetTilesetConfig req)
                        (LoadTilesetTexture r)
                        |> events.onCallUpdate
                )
                req
                res
                GetTilesetConfig
                Error.fromGrpcError

        GetTextureMetadataRes req res ->
            make
                (\re r ->
                    TriggerSubrequest (serializeApiCall <| GetTextureMetadata req)
                        (LoadTilemapTexture re r)
                        |> events.onCallUpdate
                )
                req
                res
                GetTextureMetadata
                Error.fromGrpcError

        GetAudioTracksRes req res ->
            make events.onGetAudioTracks req res GetAudioTracks Error.fromGrpcError

        GetAreasRes req res ->
            make events.onGetAreas req res GetAreas Error.fromGrpcError

        GetFriendsRes req res ->
            make events.onGetFriends req res GetFriends Error.fromGrpcError

        GetFriendRequestsRes req res ->
            make events.onGetFriendRequests req res GetFriendRequests Error.fromGrpcError

        GetGroupsRes req res ->
            make events.onGetGroups req res GetGroups Error.fromGrpcError

        GetGroupDetailRes req res ->
            make events.onGetGroupDetail req res GetGroupDetail Error.fromGrpcError

        AddFriendRes req res ->
            make events.onAddFriend req res AddFriend Error.fromGrpcError

        RemoveFriendRes req res ->
            make events.onRemoveFriend req res RemoveFriend Error.fromGrpcError

        CreateGroupRes req res ->
            make events.onCreateGroup req res CreateGroup Error.fromGrpcError

        AddToGroupRes req res ->
            make events.onAddToGroup req res AddToGroup Error.fromGrpcError

        LoadTilesetTextureRes req res ->
            make events.onLoadTilesetTexture req res LoadTilesetTexture Error.TextureError

        LoadTrainerTextureRes req res ->
            make events.onLoadTrainerTexture req res LoadTrainerTexture Error.TextureError

        LoadItemTextureRes req res ->
            make events.onLoadItemTexture req res LoadItemTexture Error.TextureError

        LoadPokemonTextureRes req res ->
            make events.onLoadPokemonTexture req res LoadPokemonTexture Error.TextureError

        LoadTilemapTextureRes metadataReq metadataRes res ->
            make (events.onLoadTilemapTexture metadataReq)
                metadataRes
                res
                (\_ -> LoadTilemapTexture metadataReq metadataRes)
                Error.TextureError

        RefreshAuthRes (Ok auth) ->
            AuthRefreshed auth |> events.onCallUpdate |> dispatch

        RefreshAuthRes (Err _) ->
            Cmd.none


setRequestStart : Time.Posix -> GrpcRequestState -> GrpcRequestState
setRequestStart time state =
    { state | loadingSince = Just time }


setLanguage : Translations.Language -> GrpcRequestState -> GrpcRequestState
setLanguage lang state =
    { state | language = Just lang }


setRequestEnd : Time.Posix -> GrpcRequestState -> GrpcRequestState
setRequestEnd time state =
    { state | lastResponseAt = Just time }


setResponse : ApiResponse -> GrpcRequestState -> GrpcRequestState
setResponse res state =
    { state | error = responseToError res }


addSubrequest : ApiCall -> GrpcRequestState -> GrpcRequestState
addSubrequest call state =
    { state | subrequests = serializeApiCall call :: state.subrequests }


responseToError : ApiResponse -> Maybe Error.Error
responseToError res =
    case res of
        GetPokemonRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetUserDataRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetOtherUserDataRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetDexEntriesRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetDexEntryDetailRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTrainersRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetStarterPokemonRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetUserItemsRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        SetTrainerSpriteRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        SetPartnerPokemonRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        ChooseStartTileRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetAllStartTilesRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTilesRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetAnimationConfigRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTilesetConfigRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTextureMetadataRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetAreasRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetAudioTracksRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetFriendsRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetFriendRequestsRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetGroupsRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetGroupDetailRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        AddFriendRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        RemoveFriendRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        CreateGroupRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        AddToGroupRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        LoadTilesetTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadPokemonTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadTrainerTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadItemTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadTilemapTextureRes _ _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        RefreshAuthRes r ->
            Result.Extra.error r


cacheStrategy : ApiCall -> CacheStrategy
cacheStrategy call =
    case call of
        SetTrainerSprite _ ->
            mutationCacheStrategy

        SetPartnerPokemon _ ->
            mutationCacheStrategy

        ChooseStartTile _ ->
            mutationCacheStrategy

        AddFriend _ ->
            mutationCacheStrategy

        RemoveFriend _ ->
            mutationCacheStrategy

        AddToGroup _ ->
            mutationCacheStrategy

        CreateGroup _ ->
            mutationCacheStrategy

        GetUserData _ ->
            -- This is explicitely called on start and when a user changes their settings,
            -- so we do not want to cache this
            mutationCacheStrategy

        GetTiles _ ->
            -- Has extra cache invalidation mechanism via websocket
            { defaultCacheStrategy | forMillis = 10000000, refreshIfLanguageChanges = False }

        GetAnimationConfig _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        GetUserItems _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        GetTextureMetadata _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        GetTilesetConfig _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadTilesetTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadPokemonTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadTrainerTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadItemTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadTilemapTexture _ _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        GetOtherUserData _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        GetFriends _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        GetFriendRequests _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        _ ->
            defaultCacheStrategy


retrieveRegistered : RequestManager -> List ( ApiCall, GrpcRequestState )
retrieveRegistered (RequestManager manager) =
    manager.registered
        |> List.filterMap
            (\apiCall ->
                Dict.get
                    (serializeApiCall apiCall
                        |> identifierToComparable
                    )
                    manager.grpc
                    |> Maybe.map (Tuple.pair apiCall)
            )


registerInternal : ApiCall -> RequestManager -> RequestManager
registerInternal call (RequestManager manager) =
    RequestManager
        { manager
            | registered = appendNonDuplicate call manager.registered
            , grpc =
                Dict.update (serializeApiCall call |> identifierToComparable)
                    (Maybe.withDefault (initialRequestState (cacheStrategy call)) >> Just)
                    manager.grpc
        }


unregisterInternal : ApiCall -> RequestManager -> RequestManager
unregisterInternal call (RequestManager manager) =
    let
        identifier =
            serializeApiCall call

        lens =
            Lens.dict <| identifierToComparable identifier
    in
    RequestManager
        { manager
            | registered = List.filter ((/=) call) manager.registered
            , grpc =
                Lens.update lens (\_ -> Nothing)
                    |> (|>) manager.grpc
        }


tilesetIdentifier : GetTilesetConfigResponse -> RequestIdentifier
tilesetIdentifier response =
    { serializedRequest =
        String.fromInt response.tileSizeInPx ++ "_" ++ String.fromInt response.tilesPerRow
    , path = response.tilesetPath
    }


identifierToTracker : RequestIdentifier -> String
identifierToTracker { serializedRequest, path } =
    -- use some seperator like % that is not in the base64 alphabet and should not be in the URL either
    path ++ "%" ++ serializedRequest


identifierToComparable : RequestIdentifier -> ( String, String )
identifierToComparable { path, serializedRequest } =
    ( path, serializedRequest )


type alias GrpcRequestConfig s req res =
    { rpc : Grpc.Rpc req res
    , session : Session s
    , body : req
    , identifier : RequestIdentifier
    }


{-| Perform a gRPC request. The rpc is expected to be generated via `protoc-gen-elm`
and imported from the `generated` folder.
Since this returns a task, you may chain other tasks after it without
having to go through the `update` function.
-}
grpc : GrpcRequestConfig a req res -> Cmd (Result Grpc.Error res)
grpc { rpc, session, body, identifier } =
    let
        acceptLanguage =
            Translations.currentLanguage session.i18n
                |> Translations.languageToString
    in
    Grpc.new rpc body
        |> Auth.addHeaders session.auth
        |> Grpc.addHeader "accept-language" acceptLanguage
        |> Grpc.setTracker (identifierToTracker identifier)
        |> Grpc.toCmd identity


encoderToString : Protobuf.Encode.Encoder -> String
encoderToString =
    Protobuf.Encode.encode
        >> Base64.fromBytes
        >> Maybe.withDefault ""


appendNonDuplicate : a -> List a -> List a
appendNonDuplicate a list =
    case list of
        [] ->
            [ a ]

        first :: rest ->
            if first == a then
                list

            else
                first :: appendNonDuplicate a rest


dispatch : msg -> Cmd msg
dispatch =
    Task.succeed >> Task.perform identity
