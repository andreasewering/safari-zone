module Main.Setup exposing (isRouteForSetupOnly, maybeRedirectToSetup, steps)

import Browser.Navigation as Navigation
import Main.Route as Route exposing (KnownRoute, Route)
import Main.TrainerSelect.Route as TrainerSelect
import Maybe.Extra
import UserData exposing (UserData)



{-
   General setup architecture:
   - There are currently 3 setup pages, which should generally be done in order (otherwise it complicates stuff on the backend)
       1. Choosing your trainer
       2. Choosing your partner pokemon
       3. Choosing your starting location
   - This should be enforced by the navigation
       - The current setup page has to be available though the nav bar
       - The previous setup pages have to be available through the nav bar
       - The settings page should be available though the nav bar
       - Contacts might also make sense to already show
       - Other URLs need to be redirected to a valid URL (the current setup page)
   - After completing a setup step, when the request succeeds, a GetUserData request is made
       - This request automatically causes navigation to the current setup page if the setup is not completed already.
   - After the setup, the user should get redirected to the safari page
-}


maybeRedirectToSetup : { s | key : Navigation.Key, route : Route } -> UserData -> UserData -> Cmd msg
maybeRedirectToSetup session prevUserData response =
    let
        { todo } =
            steps response

        endOfSetupNavigation =
            if Maybe.Extra.isJust (UserData.isComplete response) && not (UserData.isEmpty prevUserData) then
                Route.navigate session Route.Safari

            else
                Cmd.none
    in
    todo
        |> List.head
        |> Maybe.map (Route.replace session)
        -- TODO this does not do the right thing since we always navigate to Safari on page load
        |> Maybe.withDefault endOfSetupNavigation


setupSteps : List ( KnownRoute, UserData -> Bool )
setupSteps =
    [ ( Route.TrainerSelect TrainerSelect.Overview, \{ trainerSprite } -> Maybe.Extra.isJust trainerSprite )
    , ( Route.PokemonSelect, \{ partnerPokemon } -> Maybe.Extra.isJust partnerPokemon )
    , ( Route.MapSelect, \{ position } -> Maybe.Extra.isJust position )
    ]


steps : UserData -> { done : List KnownRoute, todo : List KnownRoute }
steps response =
    let
        ( done, todo ) =
            List.partition (\( _, isDone ) -> isDone response) setupSteps
    in
    { done = List.map Tuple.first done, todo = List.map Tuple.first todo }


isRouteForSetupOnly : KnownRoute -> Bool
isRouteForSetupOnly route =
    case route of
        Route.PokemonSelect ->
            True

        Route.MapSelect ->
            True

        _ ->
            False
