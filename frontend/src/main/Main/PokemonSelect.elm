module Main.PokemonSelect exposing (Context, Msg, onPageLeave, onPageLoad, update, view)

import Atoms.Html
import Atoms.Typewriter as Typewriter
import Html.WithContext exposing (div, h1, h2, p, span, text)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events exposing (onClick)
import Interop.Event as Event
import Main.Dex.Appraisal exposing (professorImage)
import Main.Dex.View.DexDetail exposing (getPokemonTypes, presentType)
import Main.Html exposing (Document, Html)
import Main.RequestManager as RequestManager exposing (RequestManager)
import Main.Shared as Shared
import Main.Translations as Translations
import PokemonNumber exposing (PokemonNumber)
import PokemonSelect.StarterPokemon exposing (StarterPokemon)
import Shared.View.PokemonImage as PokemonImage
import Tagged exposing (untag)


type Msg
    = SelectStarterPokemon PokemonNumber
    | TypewriterAnimationFinished


type alias Model m =
    { m
        | requestManager : RequestManager
        , starterPokemon : List StarterPokemon
    }


type alias Context msg =
    { lift : Msg -> msg, liftShared : Shared.Msg -> msg }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
        |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
    )


requests : List RequestManager.ApiCall
requests =
    [ RequestManager.GetStarterPokemon {}
    ]


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave _ model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.unregister
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
    )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg model =
    case msg of
        SelectStarterPokemon pokemonNumber ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.SetPartnerPokemon
                            { pokemonNumber = untag pokemonNumber }
                        ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        TypewriterAnimationFinished ->
            ( model, Event.typewriterFinished "pokemonSelect" )


view : Context msg -> Model m -> Document msg
view ctx model =
    { title = Translations.pokemonSelectTitle
    , body =
        [ div [ class "flex w-full flex-col items-center gap-2 p-2" ]
            [ h1
                [ class
                    "pokedex-width border-b p-2 text-center text-xl font-bold bgd:border-white bgd:text-white bgl:border-black bgl:text-black"
                ]
                [ Atoms.Html.text Translations.pokemonSelectHeadline ]
            , div [ class "flex w-full max-w-2xl" ]
                [ professorImage [ class "h-fit w-40" ]
                , Typewriter.typewriter
                    { text = Translations.pokemonSelectIntroduction, speed = 10, lines = 7, onFinish = ctx.lift TypewriterAnimationFinished }
                    [ class "speech-bubble-l w-full after:border-r-gray-300 after:dark:border-r-gray-800" ]
                ]
            , h2 [ class "rounded-md bg-gray-300 p-4 text-center dark:bg-gray-700 dark:text-white" ]
                [ Atoms.Html.text Translations.pokemonSelectSelectionHeadline ]
            , div [ class "grid max-w-4xl grid-cols-2 gap-2 sm:grid-cols-3 md:grid-cols-4" ] <| List.map (viewStarter ctx) model.starterPokemon
            ]
        ]
    }


viewStarter : Context msg -> StarterPokemon -> Html msg
viewStarter ctx starter =
    div [ class "flex flex-col items-center gap-2 rounded-lg bg-gray-300 p-2 dark:bg-gray-700" ] <|
        [ PokemonImage.showImageHtml [] { name = starter.name, pokemonNumber = starter.number } PokemonImage.Normal
        , h2 [ class "w-full border-y border-black p-2 text-center text-xl font-bold dark:border-white" ] [ text starter.name ]
        , div [ class "flex justify-center" ] <|
            List.map presentType (getPokemonTypes starter)
        , div [ class "flex w-full" ]
            [ div [ class "flex w-full flex-col items-center" ]
                [ Atoms.Html.text Translations.dexDetailHeightTitle
                , span [ class "text-sky-400" ] [ Atoms.Html.text <| Translations.dexDetailHeightFormat starter.height ]
                ]
            , div [ class "flex w-full flex-col items-center" ]
                [ Atoms.Html.text Translations.dexDetailWeightTitle
                , span [ class "text-sky-400" ] [ Atoms.Html.text <| Translations.dexDetailWeightFormat starter.weight ]
                ]
            ]
        , p [ class "bg-gray-300 p-2 dark:bg-gray-800" ] [ text starter.description ]
        , Atoms.Html.button [ class "mt-auto rounded-md bg-red-600 px-4 py-2 text-white", onClick (SelectStarterPokemon starter.number |> ctx.lift) ]
            [ Atoms.Html.text <| Translations.pokemonSelectSelectionButton starter.name ]
        ]
