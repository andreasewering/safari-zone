module Main.Shared exposing (Msg(..), Session, update)

import Array exposing (Array)
import Auth exposing (AuthManager)
import BoundedDict exposing (BoundedDict)
import Browser.Navigation as Navigation
import Domain.Direction as Direction
import Domain.MoveRule
import Domain.Position as Position
import GroupId exposing (GroupId)
import Grpc
import Http
import Interop.Clipboard
import Interop.WebSocket.In
import InteropDefinitions
import Item exposing (Item)
import ItemDict
import Main.Chat.Model as Chat
import Main.Dex.Types.Detail exposing (DexDetail)
import Main.Html
import Main.Language exposing (ConfigurableLanguage)
import Main.RequestManager as RequestManager exposing (RequestManager)
import Main.Route exposing (KnownRoute, Route)
import Main.Safari.Overworld.Transition as Transition
import Main.Setup
import Main.Translations as Translations
import MapId exposing (MapId)
import MapSelect.Model exposing (Startpoint, responseToStartpoints)
import Maybe.Extra
import PokemonNumber exposing (PokemonNumber)
import PokemonNumberDict exposing (PokemonNumberDict)
import PokemonSelect.StarterPokemon as StarterPokemon exposing (StarterPokemon)
import Proto.Arceus
import Proto.Chatot
import Proto.Kangaskhan exposing (GetOtherUserDataRequest, GetOtherUserDataResponse, GetPokemonResponse, GetStarterPokemonResponse, GetUserDataResponse)
import Proto.Kangaskhan.WebsocketMessageToClient.Message as Message
import Proto.Smeargle
import Shared.Background as Background
import Shared.Types.Animations as Animations
import Shared.Types.Area as Area exposing (Area)
import Shared.Types.Audio exposing (AudioConfig)
import Shared.Types.Error as Error
import Shared.Types.Items as Items exposing (CollectedItems)
import Shared.View.Theme as Theme
import StartTileId exposing (StartTileId)
import Tagged exposing (tag, untag)
import Textures.Store exposing (Sprite, Textures, Tilemaps)
import Time
import TrainerNumber exposing (TrainerNumber)
import TrainerNumberDict exposing (TrainerNumberDict)
import UserData exposing (UserData)
import UserId exposing (UserId)
import UserIdDict exposing (UserIdDict)


type alias Session m =
    { m
        | -- I18n
          language : ConfigurableLanguage
        , i18n : Translations.I18n

        -- Data from the server
        , pokemonDataDict : PokemonNumberDict Proto.Kangaskhan.GetPokemonResponse
        , dexEntries : Array Proto.Kangaskhan.DexEntry
        , audioConfig : AudioConfig
        , friends : List Proto.Chatot.Friend
        , friendRequests : Proto.Chatot.GetFriendRequestsResponse
        , groups : List Proto.Chatot.Group
        , groupDetails : BoundedDict GroupId Proto.Chatot.GetGroupDetailResponse
        , startTiles : List Startpoint
        , starterPokemon : List StarterPokemon
        , animationConfig : Animations.AnimationConfig
        , animationTileDict : BoundedDict MapId Animations.AnimationTileDict
        , areas : BoundedDict MapId (List Area)
        , collectedItems : CollectedItems
        , moveRules : BoundedDict MapId Domain.MoveRule.MoveRules
        , trainerMetadata : TrainerNumberDict Proto.Kangaskhan.Trainer
        , dexDetails : BoundedDict PokemonNumber DexDetail
        , users : UserIdDict UserData

        -- Image Textures for WebGL rendering
        , textures : Textures

        -- Client side data and configuration
        , viewport : Viewport
        , timezone : Time.Zone
        , version : String
        , colorThemeName : Theme.Name
        , reducedMotion : Bool
        , audioIsActive : Bool
        , background : Background.Background
        , htmlContext : Main.Html.Context

        -- Authentication
        , auth : AuthManager

        -- Routing
        , key : Navigation.Key
        , route : Route
        , previousRoute : Maybe KnownRoute
        , isNavigationOpen : Bool
        , mapTransitionTimeline : Transition.Timeline

        -- Chatting
        , chats : Chat.Chats

        -- Error Handling
        , requestManager : RequestManager

        -- User Data
        , userData : UserData
    }


{-| Both height and width are in `px` units
-}
type alias Viewport =
    { height : Int
    , width : Int
    }


type Msg
    = LoadedTileset Sprite
    | LoadedPokemonTexture PokemonNumber Sprite
    | LoadedTrainerTexture TrainerNumber Sprite
    | LoadedItemTexture Item Sprite
    | LoadedTilemapTexture MapId Tilemaps
    | LoadedUserData GetUserDataResponse
    | LoadedOtherUserData GetOtherUserDataRequest GetOtherUserDataResponse
      -- Chatot
    | LoadedFriends Proto.Chatot.GetFriendsResponse
    | LoadedFriendRequests Proto.Chatot.GetFriendRequestsResponse
    | LoadedGroups Proto.Chatot.GetGroupsResponse
    | LoadedGroupDetail GroupId Proto.Chatot.GetGroupDetailResponse
    | AddedFriend UserId Proto.Chatot.AddFriendResponse
    | RemovedFriend UserId Proto.Chatot.AddFriendResponse
    | CreatedNewGroup Proto.Chatot.CreateGroupResponse
    | AddedUserToGroup GroupId UserId Proto.Chatot.AddToGroupResponse
      --
    | GotAudioConfig AudioConfig
      -- Kangaskhan
    | GotDexEntryDetail PokemonNumber Proto.Kangaskhan.GetDexEntryDetailResponse
    | GotDexEntries Proto.Kangaskhan.GetDexEntriesResponse
    | GotPokemonData PokemonNumber GetPokemonResponse
    | GotStarterPokemon GetStarterPokemonResponse
    | GotUserItems Proto.Kangaskhan.GetUserItemsResponse
    | GotTrainerMetadata Proto.Kangaskhan.GetTrainersResponse
    | ChangedTrainerSprite TrainerNumber Proto.Kangaskhan.SetTrainerSpriteResponse
    | SelectedPartnerPokemon PokemonNumber Proto.Kangaskhan.SetPartnerPokemonResponse
    | SelectedStartTile StartTileId Proto.Kangaskhan.ChooseStartTileResponse
      -- Smeargle
    | GotAnimationConfig Animations.AnimationConfig
    | GotMapTiles MapId Proto.Smeargle.GetTilesResponse
    | GotStartTiles Proto.Smeargle.GetAllStartTilesResponse
      -- Arceus
    | GotAreas MapId Proto.Arceus.GetAreasResponse
      --
    | CopyToClipboard String
    | GotI18n (Result Http.Error (Translations.I18n -> Translations.I18n))
    | ReceivedMessageFromJS InteropDefinitions.ToElm
    | AuthRefreshed { previousAuth : Auth.AuthManager }
    | HasLoggedOut (Result Grpc.Error {})
    | RequestManagerMessage RequestManager.Msg
    | RequestFailed RequestManager.ApiCall Error.Error
    | NoOp


update : Msg -> Session m -> ( Session m, Cmd Msg )
update msg ({ htmlContext, textures, userData } as model) =
    case msg of
        RequestManagerMessage requestManagerMsg ->
            RequestManager.update
                { onCallUpdate = RequestManagerMessage
                , onFail = RequestFailed
                , onAuthRefresh = AuthRefreshed
                , onGetTrainers = always GotTrainerMetadata
                , onGetDexEntryDetail = \req -> GotDexEntryDetail (tag req.number)
                , onGetPokemon = \req -> GotPokemonData (tag req.pokemonNumber)
                , onGetUserData = always LoadedUserData
                , onGetOtherUserData = LoadedOtherUserData
                , onGetDexEntries = always GotDexEntries
                , onGetStarterPokemon = always GotStarterPokemon
                , onGetUserItems = always GotUserItems
                , onSetTrainerSprite = \req -> ChangedTrainerSprite (tag req.trainerNumber)
                , onSetPartnerPokemon = \req -> SelectedPartnerPokemon (tag req.pokemonNumber)
                , onChooseStartTile = \req -> SelectedStartTile (tag req.startTileId)
                , onGetAllStartTiles = always GotStartTiles
                , onGetTiles = \req -> GotMapTiles (tag req.mapId)
                , onGetAnimationConfig = always (.animations >> Array.fromList >> GotAnimationConfig)
                , onGetAudioTracks = always (Shared.Types.Audio.fromProto >> GotAudioConfig)
                , onGetAreas = \req -> GotAreas (tag req.mapId)
                , onGetFriends = always LoadedFriends
                , onGetFriendRequests = always LoadedFriendRequests
                , onGetGroups = always LoadedGroups
                , onGetGroupDetail = \req -> LoadedGroupDetail (tag req.groupId)
                , onAddFriend = \req -> AddedFriend (tag req.userId)
                , onRemoveFriend = \req -> RemovedFriend (tag req.userId)
                , onCreateGroup = always CreatedNewGroup
                , onAddToGroup = \req -> AddedUserToGroup (tag req.groupId) (tag req.userId)
                , onLoadTilesetTexture = always LoadedTileset
                , onLoadPokemonTexture = LoadedPokemonTexture
                , onLoadTrainerTexture = LoadedTrainerTexture
                , onLoadItemTexture = LoadedItemTexture
                , onLoadTilemapTexture = \metadataReq _ -> LoadedTilemapTexture (tag metadataReq.mapId)
                }
                requestManagerMsg
                model

        LoadedTileset tileset ->
            ( { model | textures = { textures | tileset = Just tileset } }, Cmd.none )

        LoadedPokemonTexture pokemonNumber sprite ->
            ( { model
                | textures =
                    { textures | pokemonTextures = PokemonNumberDict.insert pokemonNumber sprite textures.pokemonTextures }
              }
            , Cmd.none
            )

        LoadedTrainerTexture trainerNumber sprite ->
            ( { model
                | textures =
                    { textures | trainerTextures = TrainerNumberDict.insert trainerNumber sprite textures.trainerTextures }
              }
            , Cmd.none
            )

        LoadedItemTexture item sprite ->
            ( { model | textures = { textures | itemTextures = ItemDict.insert item sprite textures.itemTextures } }
            , Cmd.none
            )

        LoadedTilemapTexture mapId tilemaps ->
            ( { model | textures = { textures | tilemaps = BoundedDict.insert mapId tilemaps textures.tilemaps } }
            , Cmd.none
            )

        LoadedOtherUserData request response ->
            let
                userId =
                    tag request.userId

                trainerSprite =
                    response.trainer |> Maybe.map (\{ trainerNumber } -> tag trainerNumber)

                partnerPokemon =
                    response.partnerPokemon
                        |> Maybe.map
                            (\{ pokemonNumber, isShiny } ->
                                { pokemonNumber = tag pokemonNumber, isShiny = isShiny }
                            )

                displayName =
                    response.name

                newUserData =
                    { position = Nothing
                    , trainerSprite = trainerSprite
                    , displayName = displayName
                    , partnerPokemon = partnerPokemon
                    }
            in
            ( { model
                | users = UserIdDict.insert userId newUserData model.users
              }
            , Cmd.none
            )

        LoadedUserData response ->
            let
                position =
                    response.position
                        |> Maybe.map
                            (\{ x, y, layerNumber, direction } ->
                                Direction.fromProto direction
                                    |> Maybe.withDefault Direction.default
                                    |> Position.init { x = x, y = y } layerNumber
                            )

                trainerSprite =
                    response.trainer |> Maybe.map (\{ trainerNumber } -> tag trainerNumber)

                partnerPokemon =
                    response.partnerPokemon
                        |> Maybe.map
                            (\{ pokemonNumber, isShiny } ->
                                { pokemonNumber = tag pokemonNumber, isShiny = isShiny }
                            )

                displayName =
                    response.name

                newUserData =
                    { position = position
                    , trainerSprite = trainerSprite
                    , displayName = displayName
                    , partnerPokemon = partnerPokemon
                    }
            in
            ( { model
                | userData = newUserData
              }
            , Main.Setup.maybeRedirectToSetup model userData newUserData
            )

        LoadedFriends response ->
            ( { model | friends = response.friends }
            , Cmd.none
            )

        LoadedFriendRequests response ->
            ( { model | friendRequests = response }
            , Cmd.none
            )

        LoadedGroups response ->
            ( { model | groups = response.groups }
            , Cmd.none
            )

        LoadedGroupDetail groupId response ->
            ( { model | groupDetails = BoundedDict.insert groupId response model.groupDetails }
            , Cmd.none
            )

        AddedFriend _ _ ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate [ RequestManager.GetFriends {}, RequestManager.GetFriendRequests {} ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map RequestManagerMessage
            )

        RemovedFriend _ _ ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate [ RequestManager.GetFriends {}, RequestManager.GetFriendRequests {} ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map RequestManagerMessage
            )

        AddedUserToGroup groupId _ _ ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate [ RequestManager.GetGroupDetail { groupId = untag groupId } ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map RequestManagerMessage
            )

        CreatedNewGroup response ->
            ( { model | groups = model.groups ++ Maybe.Extra.toList response.created }
            , Cmd.none
            )

        GotAudioConfig response ->
            ( { model | audioConfig = response }, Cmd.none )

        GotPokemonData pokemonNumber response ->
            ( { model | pokemonDataDict = PokemonNumberDict.insert pokemonNumber response model.pokemonDataDict }
            , Cmd.none
            )

        CopyToClipboard text ->
            ( model, Interop.Clipboard.copy text )

        NoOp ->
            ( model, Cmd.none )

        GotDexEntryDetail pokemonNumber response ->
            ( { model
                | dexDetails =
                    BoundedDict.insert pokemonNumber
                        (Main.Dex.Types.Detail.fromResponse pokemonNumber response)
                        model.dexDetails
              }
            , Cmd.none
            )

        GotDexEntries response ->
            ( { model | dexEntries = Array.fromList response.entries }
            , Cmd.none
            )

        GotStarterPokemon response ->
            ( { model | starterPokemon = StarterPokemon.fromProto response }
            , Cmd.none
            )

        GotUserItems response ->
            ( { model
                | collectedItems =
                    response.items
                        |> (List.filterMap <|
                                \{ amount, item } ->
                                    item
                                        |> Item.fromProto
                                        |> Maybe.map (\i -> ( i, amount ))
                           )
                        |> Items.collectedItemsFromList
              }
            , Cmd.none
            )

        GotAnimationConfig config ->
            ( { model | animationConfig = config }
            , Cmd.none
            )

        GotMapTiles mapId response ->
            let
                rules =
                    Domain.MoveRule.rulesFromProto response.tiles

                animationTileDict =
                    response.tiles
                        |> List.concatMap filterTilesWithAnimationId
                        |> Animations.tileAnimationDictFromProto

                filterTilesWithAnimationId tile =
                    List.map
                        (\animationId ->
                            { x = tile.x, y = tile.y, layerNumber = tile.layerNumber, animationId = animationId }
                        )
                        tile.animationIds
            in
            ( { model
                | moveRules = BoundedDict.insert mapId rules model.moveRules
                , animationTileDict = BoundedDict.insert mapId animationTileDict model.animationTileDict
              }
            , Cmd.none
            )

        GotAreas mapId response ->
            let
                areas =
                    response.areas |> List.map Area.fromProto
            in
            ( { model | areas = BoundedDict.insert mapId areas model.areas }
            , Cmd.none
            )

        GotI18n (Ok apply) ->
            let
                i18n =
                    apply model.i18n
            in
            ( { model | i18n = i18n, htmlContext = { htmlContext | i18n = i18n } }, Cmd.none )

        GotI18n (Err _) ->
            -- TODO add error handling
            ( model, Cmd.none )

        GotTrainerMetadata trainers ->
            let
                trainerResponseToDict data =
                    data.trainers
                        |> List.map (\trainer -> ( tag trainer.trainerNumber, trainer ))
                        |> TrainerNumberDict.fromList
            in
            ( { model | trainerMetadata = trainerResponseToDict trainers }, Cmd.none )

        ChangedTrainerSprite trainerNumber _ ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate [ RequestManager.GetUserData {} ]
                        model.requestManager
            in
            ( { model
                | requestManager = requestManager
                , userData = { userData | trainerSprite = Just trainerNumber }
              }
            , requestManagerCmds |> Cmd.map RequestManagerMessage
            )

        SelectedPartnerPokemon _ _ ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate [ RequestManager.GetUserData {} ] model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map RequestManagerMessage
            )

        SelectedStartTile _ _ ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate
                        [ RequestManager.GetUserData {}
                        , RequestManager.RefreshAuth
                        ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map RequestManagerMessage
            )

        GotStartTiles response ->
            ( { model | startTiles = responseToStartpoints response }
            , Cmd.none
            )

        AuthRefreshed _ ->
            -- Just here for pages to hook into if needed
            ( model, Cmd.none )

        HasLoggedOut (Ok _) ->
            ( model, Navigation.load "/login/" )

        HasLoggedOut (Err _) ->
            -- TODO error handling
            ( model, Cmd.none )

        ReceivedMessageFromJS (InteropDefinitions.KangaskhanIn (Interop.WebSocket.In.Message mapId (Message.ReloadTiles _))) ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate
                        [ RequestManager.GetTiles { mapId = untag mapId }
                        , RequestManager.GetTextureMetadata { mapId = untag mapId }
                        ]
                        model.requestManager
            in
            ( { model
                | moveRules = BoundedDict.withCapacity 4
                , animationTileDict = BoundedDict.withCapacity 4
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map RequestManagerMessage
            )

        ReceivedMessageFromJS _ ->
            -- TODO wire up chat etc
            ( model, Cmd.none )

        RequestFailed _ _ ->
            -- This just enables pages to react to failures if necessary
            ( model, Cmd.none )
