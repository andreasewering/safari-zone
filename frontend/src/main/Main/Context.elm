module Main.Context exposing (..)

import GroupId exposing (GroupId)
import Main.Chat as Chat
import Main.Contacts.Detail as ContactsDetail
import Main.Contacts.Overview as ContactsOverview
import Main.Dex.Appraisal as DexAppraisal
import Main.Dex.Detail as DexDetail
import Main.Dex.Overview as DexOverview
import Main.MapSelect as MapSelect
import Main.Msg exposing (Msg(..))
import Main.PokemonSelect as PokemonSelect
import Main.Safari.Overworld as Safari
import Main.Settings.Overview as SettingsOverview
import Main.Settings.QrCodeView as QrCodeView
import Main.TrainerSelect.Detail as TrainerDetail
import Main.TrainerSelect.Overview as TrainerOverview
import PokemonNumber exposing (PokemonNumber)
import TrainerNumber exposing (TrainerNumber)


safari : Safari.Context Main.Msg.Msg
safari =
    { lift = SafariPageMessage, liftShared = SharedMessage }


dexOverview : DexOverview.Context Main.Msg.Msg
dexOverview =
    { liftShared = SharedMessage }


dexDetail : PokemonNumber -> DexDetail.Context Main.Msg.Msg
dexDetail pokemonNumber =
    { pokemonNumber = pokemonNumber, lift = DexDetailMessage pokemonNumber, liftShared = SharedMessage }


dexAppraisal : DexAppraisal.Context Main.Msg.Msg
dexAppraisal =
    { lift = DexAppraisalMessage, liftShared = SharedMessage }


contactsOverview : ContactsOverview.Context Main.Msg.Msg
contactsOverview =
    { lift = ContactsMessage, liftShared = SharedMessage }


groupDetail : GroupId -> ContactsDetail.Context Main.Msg.Msg
groupDetail groupId =
    { groupId = groupId
    , lift = GroupDetailMessage groupId
    , liftShared = SharedMessage
    }


trainerSelectOverview : TrainerOverview.Context Main.Msg.Msg
trainerSelectOverview =
    { lift = TrainerSelectMessage, liftShared = SharedMessage }


trainerSelectDetail : TrainerNumber -> TrainerDetail.Context Main.Msg.Msg
trainerSelectDetail trainerNumber =
    { trainerNumber = trainerNumber
    , lift = TrainerDetailMessage trainerNumber
    , liftShared = SharedMessage
    }


pokemonSelect : PokemonSelect.Context Main.Msg.Msg
pokemonSelect =
    { lift = PokemonSelectMessage
    , liftShared = SharedMessage
    }


mapSelect : MapSelect.Context Main.Msg.Msg
mapSelect =
    { lift = MapSelectMessage, liftShared = SharedMessage }


settings : SettingsOverview.Context Main.Msg.Msg
settings =
    { lift = SettingsMessage, liftShared = SharedMessage }


qrcodeView : QrCodeView.Context Main.Msg.Msg
qrcodeView =
    { lift = QrCodeViewMessage, liftShared = SharedMessage }


chat : Chat.Context Main.Msg.Msg
chat =
    { lift = ChatMessage, liftShared = SharedMessage }
