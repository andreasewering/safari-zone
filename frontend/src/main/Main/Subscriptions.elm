module Main.Subscriptions exposing (subscriptions)

import Auth
import Browser.Events
import InteropPorts
import Main.Animation as Animation
import Main.Context as Context
import Main.Dex.Detail
import Main.Dex.Route as Dex
import Main.Model exposing (Model)
import Main.Msg exposing (Msg(..))
import Main.Route as Route
import Main.Safari.Overworld
import Main.Settings.Route as Settings
import Main.Shared as Shared
import Main.TrainerSelect.Detail
import Main.TrainerSelect.Route as TrainerSelect
import Result.Extra
import Time


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        pageSubscriptions =
            case model.route of
                Ok (Route.Dex (Dex.Detail pokemonNumber)) ->
                    Main.Dex.Detail.subscriptions (Context.dexDetail pokemonNumber)

                Ok (Route.Dex _) ->
                    Sub.none

                Ok (Route.Contacts _) ->
                    Sub.none

                Ok Route.Safari ->
                    Main.Safari.Overworld.subscriptions Context.safari model.safariModel

                Ok (Route.Settings Settings.Overview) ->
                    Sub.none

                Ok (Route.Settings Settings.QrCodeView) ->
                    Sub.none

                Ok (Route.TrainerSelect (TrainerSelect.Detail trainerNumber)) ->
                    Main.TrainerSelect.Detail.subscriptions (Context.trainerSelectDetail trainerNumber) model

                Ok (Route.TrainerSelect TrainerSelect.Overview) ->
                    Sub.none

                Ok Route.PokemonSelect ->
                    Sub.none

                Ok Route.MapSelect ->
                    Sub.none

                Err _ ->
                    Sub.none

        interopSub =
            InteropPorts.toElm
                |> Sub.map (Result.Extra.mapBoth FailedToDecodeMessageFromJS (Shared.ReceivedMessageFromJS >> SharedMessage) >> Result.Extra.merge)
    in
    Sub.batch
        [ interopSub
        , Browser.Events.onResize Resize
        , Animation.subscriptions Tick model
        , Auth.refreshReminder model.auth |> Sub.map AuthRefreshTime
        , pageSubscriptions
        , Time.every 10000 CheckBackground
        ]
