module Main.Update exposing (..)

import Auth
import Browser
import Browser.Navigation
import Main.Animation as Animation
import Main.Chat
import Main.Chat.Model
import Main.Contacts.Detail as ContactsDetail
import Main.Contacts.Overview
import Main.Contacts.Route as Contacts
import Main.Context as Context exposing (dexDetail)
import Main.Dex.Appraisal
import Main.Dex.Detail as DexDetail
import Main.Dex.Overview as DexOverview
import Main.Dex.Route as Dex
import Main.MapSelect
import Main.Model exposing (Model)
import Main.Msg exposing (Msg(..))
import Main.PokemonSelect
import Main.RequestManager as RequestManager
import Main.Route as Route exposing (Route)
import Main.Safari.Overworld
import Main.Settings.Overview
import Main.Settings.QrCodeView
import Main.Shared as Shared
import Main.TrainerSelect.Detail
import Main.TrainerSelect.Overview
import Main.TrainerSelect.Route as TrainerSelect
import Shared.Background as Background
import Shared.UpdateUtil as UpdateUtil
import Time
import Url exposing (Url)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ htmlContext } as model) =
    case msg of
        DexDetailMessage pokemonNumber dexMessage ->
            DexDetail.update (Context.dexDetail pokemonNumber) dexMessage model

        DexAppraisalMessage dexMessage ->
            Main.Dex.Appraisal.update Context.dexAppraisal dexMessage model

        ContactsMessage contactsMessage ->
            Main.Contacts.Overview.update Context.contactsOverview contactsMessage model

        GroupDetailMessage groupId groupDetailMessage ->
            ContactsDetail.update (Context.groupDetail groupId) groupDetailMessage model

        SafariPageMessage safariPageMessage ->
            Main.Safari.Overworld.update Context.safari safariPageMessage model

        SettingsMessage settingsMessage ->
            Main.Settings.Overview.update Context.settings settingsMessage model

        QrCodeViewMessage qrCodeMessage ->
            Main.Settings.QrCodeView.update Context.qrcodeView qrCodeMessage model

        TrainerSelectMessage trainerMessage ->
            Main.TrainerSelect.Overview.update trainerMessage model

        TrainerDetailMessage trainerNumber trainerMessage ->
            Main.TrainerSelect.Detail.update (Context.trainerSelectDetail trainerNumber) trainerMessage model

        PokemonSelectMessage pokemonSelectMessage ->
            Main.PokemonSelect.update Context.pokemonSelect pokemonSelectMessage model

        MapSelectMessage mapSelectMessage ->
            Main.MapSelect.update Context.mapSelect mapSelectMessage model

        SharedMessage sharedMessage ->
            UpdateUtil.chain
                [ UpdateUtil.liftCmd SharedMessage (Shared.update sharedMessage)
                , updateShared sharedMessage
                ]
                model

        FailedToDecodeMessageFromJS _ ->
            ( model
            , Cmd.none
            )

        OnUrlChange url ->
            handleRouteChange url model

        OnUrlRequest (Browser.Internal url) ->
            ( model, Url.toString url |> Browser.Navigation.pushUrl model.key )

        OnUrlRequest (Browser.External url) ->
            ( model, Browser.Navigation.load url )

        Resize x y ->
            ( { model
                | viewport =
                    { width = x
                    , height = y
                    }
              }
            , Cmd.none
            )

        Tick time ->
            let
                newModel =
                    onTick time model

                pageUpdate =
                    case model.route of
                        Ok Route.Safari ->
                            Main.Safari.Overworld.onAnimationTick time

                        Ok (Route.Dex (Dex.Detail pokemonNumber)) ->
                            DexDetail.onAnimationTick (dexDetail pokemonNumber) time

                        _ ->
                            UpdateUtil.none
            in
            pageUpdate newModel

        AuthRefreshTime timestamp ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.invalidate
                        [ RequestManager.RefreshAuth ]
                        model.requestManager
            in
            ( { model | auth = Auth.updateTime timestamp model.auth, requestManager = requestManager }
            , Cmd.map (Shared.RequestManagerMessage >> SharedMessage) requestManagerCmds
            )

        Logout ->
            ( model, Shared.HasLoggedOut >> SharedMessage |> Auth.logout )

        ClickedNav ->
            ( { model | isNavigationOpen = not model.isNavigationOpen }, Cmd.none )

        ChatMessage chatMsg ->
            let
                ( updatedChats, effs ) =
                    Main.Chat.update Context.chat chatMsg model.chats
            in
            ( { model | chats = updatedChats }, effs )

        GotTimezone timezone ->
            let
                background =
                    Background.setTimezone timezone model.background
            in
            ( { model
                | timezone = timezone
                , background = background
                , htmlContext = { htmlContext | timezone = timezone, background = Background.toImage background }
              }
            , Cmd.none
            )

        CheckBackground time ->
            let
                background =
                    Background.fromTime time model.timezone
            in
            ( { model | background = background }, Cmd.none )

        CloseErrorOverlay ->
            ( model, Cmd.none )


onTick : Time.Posix -> Model -> Model
onTick time =
    let
        updateChats model =
            { model | chats = Main.Chat.Model.updateTime time model.chats }
    in
    Animation.onTick time
        >> updateChats


handleRouteChange : Url -> Model -> ( Model, Cmd Msg )
handleRouteChange url model =
    let
        newRoute =
            Route.toRoute url
    in
    if newRoute == model.route then
        ( { model
            | isNavigationOpen = False
            , chats = Main.Chat.Model.close model.chats
          }
        , Cmd.none
        )

    else
        UpdateUtil.chain
            [ leavePage model.route, initializePage newRoute ]
            { model
                | route = newRoute
                , previousRoute = model.route |> Result.toMaybe
                , chats = Main.Chat.Model.close model.chats
                , isNavigationOpen = False
            }


initializePage : Route -> Model -> ( Model, Cmd Msg )
initializePage route model =
    let
        onPageLoad =
            case route of
                Ok (Route.Dex Dex.Overview) ->
                    DexOverview.onPageLoad Context.dexOverview

                Ok (Route.Dex (Dex.Detail pokemonNumber)) ->
                    DexDetail.onPageLoad (Context.dexDetail pokemonNumber)

                Ok (Route.Dex Dex.Appraisal) ->
                    Main.Dex.Appraisal.onPageLoad Context.dexAppraisal

                Ok (Route.Contacts Contacts.Overview) ->
                    Main.Contacts.Overview.onPageLoad Context.contactsOverview

                Ok (Route.Contacts (Contacts.GroupDetail groupId)) ->
                    ContactsDetail.onPageLoad (Context.groupDetail groupId)

                Ok Route.Safari ->
                    Main.Safari.Overworld.onPageLoad Context.safari

                Ok (Route.Settings _) ->
                    UpdateUtil.none

                Ok (Route.TrainerSelect TrainerSelect.Overview) ->
                    Main.TrainerSelect.Overview.onPageLoad Context.trainerSelectOverview

                Ok (Route.TrainerSelect (TrainerSelect.Detail trainerNumber)) ->
                    Main.TrainerSelect.Detail.onPageLoad (Context.trainerSelectDetail trainerNumber)

                Ok Route.PokemonSelect ->
                    Main.PokemonSelect.onPageLoad Context.pokemonSelect

                Ok Route.MapSelect ->
                    Main.MapSelect.onPageLoad Context.mapSelect

                Err _ ->
                    UpdateUtil.none
    in
    onPageLoad model


leavePage : Route -> Model -> ( Model, Cmd Msg )
leavePage route model =
    let
        onPageLeave =
            case route of
                Ok Route.Safari ->
                    Main.Safari.Overworld.onPageLeave Context.safari

                Ok (Route.Dex (Dex.Detail pokemonNumber)) ->
                    DexDetail.onPageLeave (Context.dexDetail pokemonNumber)

                Ok (Route.TrainerSelect TrainerSelect.Overview) ->
                    Main.TrainerSelect.Overview.onPageLeave Context.trainerSelectOverview

                Ok (Route.TrainerSelect (TrainerSelect.Detail number)) ->
                    Main.TrainerSelect.Detail.onPageLeave (Context.trainerSelectDetail number)

                Ok Route.MapSelect ->
                    Main.MapSelect.onPageLeave Context.mapSelect

                Ok (Route.Contacts Contacts.Overview) ->
                    Main.Contacts.Overview.onPageLeave Context.contactsOverview

                Ok (Route.Contacts (Contacts.GroupDetail groupId)) ->
                    ContactsDetail.onPageLeave (Context.groupDetail groupId)

                _ ->
                    UpdateUtil.none
    in
    onPageLeave model


updateShared : Shared.Msg -> Model -> ( Model, Cmd Msg )
updateShared msg model =
    case model.route of
        Ok Route.Safari ->
            Main.Safari.Overworld.updateShared Context.safari msg model

        Ok (Route.Dex (Dex.Detail pokemonNumber)) ->
            DexDetail.updateShared (Context.dexDetail pokemonNumber) msg model

        Ok Route.MapSelect ->
            Main.MapSelect.updateShared Context.mapSelect msg model

        Ok (Route.TrainerSelect _) ->
            Main.TrainerSelect.Overview.updateShared Context.trainerSelectOverview msg model

        Ok (Route.Contacts (Contacts.GroupDetail groupId)) ->
            ContactsDetail.updateShared (Context.groupDetail groupId) msg model

        Ok (Route.Contacts Contacts.Overview) ->
            Main.Contacts.Overview.updateShared Context.contactsOverview msg model

        _ ->
            ( model, Cmd.none )
