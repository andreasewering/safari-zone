module Main.Chat.Msg exposing (Msg(..))

import ChatChannel exposing (ChatChannel)


type Msg
    = GoToChannel ChatChannel
    | Typing ChatChannel String
    | SendMsg ChatChannel
    | GoToChats
    | ToggleChat
