module Main.Chat.Channel exposing (Group, User, WsChannel(..), channelToRecipient)

import ChatChannel exposing (ChatChannel(..))
import MapId exposing (MapId)
import Proto.Chatot as Chatot
import Proto.Chatot.Channel.Kind as Kind
import Protobuf.Types.Int64 exposing (Int64)
import Tagged exposing (untag)
import UserId exposing (UserId)


type alias User =
    { userId : UserId, userName : String }


type alias Group =
    { groupId : Int64
    , groupName : String
    }


type WsChannel
    = Chatot Chatot.Channel
    | Kangaskhan MapId


channelToRecipient : ChatChannel -> WsChannel
channelToRecipient channel =
    case channel of
        GroupChannel groupId ->
            Chatot { kind = Just <| Kind.Group <| untag groupId }

        PrivateChannel userId ->
            Chatot { kind = Just <| Kind.Private <| untag userId }

        MapChannel mapId ->
            Kangaskhan mapId
