module Main.Chat.Model exposing
    ( Chat
    , Chats
    , Message
    , Sender(..)
    , addGroupChannel
    , addPrivateChannel
    , allUnreadMessages
    , close
    , deselectChannel
    , getChannel
    , groupChannelById
    , init
    , isOpen
    , open
    , openGroupChannel
    , openPrivateChannel
    , privateChannelById
    , selectChannel
    , selectedChannel
    , sortedChannels
    , switchMap
    , toggle
    , unreadMessagesForChat
    , updateForSendMessage
    , updateTime
    , updateTypedText
    )

import ChatChannel exposing (ChatChannel)
import ChatChannelDict exposing (ChatChannelDict)
import GroupId exposing (GroupId)
import List.Extra
import Main.Chat.Channel as Channel
import Main.Translations as Translations exposing (I18n)
import MapId exposing (MapId)
import Tagged exposing (tag)
import Time
import UserId exposing (UserId)


type Chats
    = Chats
        { channels : ChatChannelDict Chat
        , time : Time.Posix
        , selectedChannel : Maybe ChatChannel
        , isOpen : Bool
        }


init : Time.Posix -> Chats
init time =
    Chats
        { channels = ChatChannelDict.empty
        , time = time
        , selectedChannel = Nothing
        , isOpen = False
        }


type alias Chat =
    { messages : List Message, currentlyTyped : String, active : Bool, chatName : I18n -> String }


type alias Message =
    { text : String
    , timestamp : Time.Posix
    , read : Bool
    , sender : Sender
    }


type Sender
    = Self
    | Other String


defaultChat : (I18n -> String) -> Chat
defaultChat chatName =
    { messages = [], currentlyTyped = "", active = True, chatName = chatName }


selectedChannel : Chats -> Maybe { channel : ChatChannel, chat : Chat }
selectedChannel (Chats chats) =
    chats.selectedChannel
        |> Maybe.andThen
            (\c ->
                ChatChannelDict.get c chats.channels
                    |> Maybe.map (\chat -> { channel = c, chat = chat })
            )


getChannel : ChatChannel -> Chats -> Maybe Chat
getChannel channel (Chats chats) =
    ChatChannelDict.get channel chats.channels


sortedChannels : Chats -> List ( ChatChannel, Chat )
sortedChannels (Chats { channels }) =
    ChatChannelDict.toList channels


updateTime : Time.Posix -> Chats -> Chats
updateTime newTime (Chats chats) =
    Chats { chats | time = newTime }


addGroupChannel : Channel.Group -> Chats -> Chats
addGroupChannel { groupId, groupName } =
    ChatChannel.GroupChannel (tag groupId) |> addChannel (Translations.chatChannelGroup groupName)


addPrivateChannel : Channel.User -> Chats -> Chats
addPrivateChannel { userId, userName } =
    ChatChannel.PrivateChannel userId |> addChannel (Translations.chatChannelPrivate userName)


switchMap : MapId -> Chats -> Chats
switchMap mapId =
    let
        newMapChannel =
            ChatChannel.MapChannel mapId

        markOtherMapsInactive (Chats chats) =
            Chats
                { chats
                    | channels =
                        ChatChannelDict.map
                            (\channel chat ->
                                case channel of
                                    ChatChannel.MapChannel _ ->
                                        { chat | active = False }

                                    _ ->
                                        chat
                            )
                            chats.channels
                }
    in
    markOtherMapsInactive >> addChannel Translations.chatChannelMap newMapChannel


addChannel : (I18n -> String) -> ChatChannel -> Chats -> Chats
addChannel chatName channel (Chats chats) =
    let
        setChannelActive chat =
            { chat | active = True }
    in
    Chats
        { chats
            | channels =
                ChatChannelDict.update channel (Maybe.map setChannelActive >> Maybe.withDefault (defaultChat chatName) >> Just) chats.channels
        }


openGroupChannel : GroupId -> Chats -> Chats
openGroupChannel groupId chats =
    case groupChannelById groupId chats of
        Just channel ->
            openChannel channel chats

        Nothing ->
            chats


openPrivateChannel : UserId -> Chats -> Chats
openPrivateChannel userId chats =
    case privateChannelById userId chats of
        Just channel ->
            openChannel channel chats

        Nothing ->
            chats


openChannel : ChatChannel -> Chats -> Chats
openChannel channel =
    selectChannel channel >> open


close : Chats -> Chats
close (Chats chats) =
    Chats { chats | isOpen = False }


open : Chats -> Chats
open (Chats chats) =
    Chats { chats | isOpen = True }


toggle : Chats -> Chats
toggle (Chats chats) =
    Chats { chats | isOpen = not chats.isOpen }


isOpen : Chats -> Bool
isOpen (Chats chats) =
    chats.isOpen


allUnreadMessages : Chats -> Int
allUnreadMessages (Chats { channels }) =
    ChatChannelDict.values channels |> List.map unreadMessagesForChat |> List.sum


selectChannel : ChatChannel -> Chats -> Chats
selectChannel channel (Chats chats) =
    Chats
        { chats
            | selectedChannel = Just channel
            , channels = ChatChannelDict.update channel (Maybe.map markAllRead) chats.channels
        }


deselectChannel : Chats -> Chats
deselectChannel (Chats chats) =
    Chats { chats | selectedChannel = Nothing }


groupChannelById : GroupId -> Chats -> Maybe ChatChannel
groupChannelById groupId (Chats { channels }) =
    ChatChannelDict.toList channels
        |> List.Extra.find
            (\( channel, _ ) ->
                case channel of
                    ChatChannel.GroupChannel groupId_ ->
                        groupId_ == groupId

                    _ ->
                        False
            )
        |> Maybe.map Tuple.first


privateChannelById : UserId -> Chats -> Maybe ChatChannel
privateChannelById userId (Chats { channels }) =
    ChatChannelDict.toList channels
        |> List.Extra.find
            (\( channel, _ ) ->
                case channel of
                    ChatChannel.PrivateChannel userId_ ->
                        userId_ == userId

                    _ ->
                        False
            )
        |> Maybe.map Tuple.first


markRead : Message -> Message
markRead msg =
    { msg | read = True }


markAllRead : Chat -> Chat
markAllRead chat =
    { chat | messages = List.map markRead chat.messages }


unreadMessagesForChat : Chat -> Int
unreadMessagesForChat =
    .messages >> List.filter (not << .read) >> List.length


updateTypedText : ChatChannel -> String -> Chats -> Chats
updateTypedText channel text (Chats chats) =
    Chats
        { chats
            | channels =
                ChatChannelDict.update channel
                    (Maybe.map <| \chat -> { chat | currentlyTyped = text })
                    chats.channels
        }


updateForSendMessage : ChatChannel -> Chats -> Chats
updateForSendMessage channel (Chats chats) =
    let
        addMessageAndClearTyped : Chat -> Chat
        addMessageAndClearTyped chat =
            { chat
                | currentlyTyped = ""
                , messages = { text = chat.currentlyTyped, timestamp = chats.time, read = True, sender = Self } :: chat.messages
            }
    in
    Chats
        { chats
            | channels =
                ChatChannelDict.update channel
                    (Maybe.map addMessageAndClearTyped)
                    chats.channels
        }
