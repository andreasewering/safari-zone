module Main.Nav exposing (AnimationTimeline, initTimeline, isOpen, mainViewportWidth, mobileNavWidthLimit, toggleNav, view)

import Animator
import Atoms.Html exposing (a, button, none, text)
import Auth exposing (AuthManager)
import Html.WithContext exposing (div, li, nav, p, ul)
import Html.WithContext.Attributes exposing (class, href)
import Html.WithContext.Events as Events
import Main.Contacts.Route as Contacts
import Main.Dex.Route as Dex
import Main.Html
import Main.Route as Route exposing (KnownRoute)
import Main.Settings.Route as Settings
import Main.Setup as Setup
import Main.TrainerSelect.Route as TrainerSelect
import Main.Translations as Translations exposing (I18n)
import Shared.Types.Generic exposing (Setter)
import Shared.View.Icon exposing (iconHtmlWith)
import UserData exposing (UserData)


type alias AnimationTimeline =
    Animator.Timeline Bool


initTimeline : AnimationTimeline
initTimeline =
    Animator.init False


toggleNav : Setter AnimationTimeline
toggleNav timeline =
    Animator.go Animator.verySlowly (not <| isOpen timeline) timeline


type alias NavLink msg =
    { target : Target msg
    , iconName : String
    , iconWidth : Float
    , iconHeight : Float
    , label : I18n -> String
    }


type Target msg
    = Link KnownRoute
    | Action msg


publicLinks : UserData -> List (NavLink msg)
publicLinks userData =
    let
        { done, todo } =
            Setup.steps userData

        safari =
            { target = Link Route.Safari
            , iconName = "compass3"
            , iconWidth = 3
            , iconHeight = 4
            , label = Translations.safariIconLabel
            }

        mapSelect =
            { target = Link Route.MapSelect
            , iconName = "compass3"
            , iconWidth = 3
            , iconHeight = 4
            , label = Translations.safariIconLabel
            }

        dex =
            { target = Link <| Route.Dex Dex.Overview
            , iconName = "pokedex"
            , iconWidth = 4
            , iconHeight = 3.5
            , label = Translations.dexIconLabel
            }

        pokemonSelect =
            { target = Link <| Route.PokemonSelect
            , iconName = "pokedex"
            , iconWidth = 4
            , iconHeight = 3.5
            , label = Translations.dexIconLabel
            }

        settings =
            { target = Link (Route.Settings Settings.Overview)
            , iconName = "settings"
            , iconWidth = 4.5
            , iconHeight = 4.5
            , label = Translations.settingsIconLabel
            }

        trainerSelect =
            { target = Link <| Route.TrainerSelect TrainerSelect.Overview
            , iconName = "pokecap"
            , iconWidth = 3.5
            , iconHeight = 3.5
            , label = Translations.trainerSelectIconLabel
            }

        contacts =
            { target = Link <| Route.Contacts Contacts.Overview
            , iconName = "team"
            , iconWidth = 3.5
            , iconHeight = 3.5
            , label = Translations.groupsIconLabel
            }
    in
    case todo of
        [] ->
            [ safari, dex, settings, trainerSelect, contacts ]

        first :: _ ->
            let
                setupLinks =
                    [ mapSelect, pokemonSelect, trainerSelect ]

                visibleRoutes =
                    first :: done
            in
            List.filter
                (\setupLink ->
                    case setupLink.target of
                        Link link ->
                            List.member link visibleRoutes

                        Action _ ->
                            False
                )
                setupLinks
                ++ [ settings, contacts ]


logoutLink : msg -> NavLink msg
logoutLink onLogout =
    { target = Action onLogout
    , iconName = "logout"
    , iconWidth = 4
    , iconHeight = 4
    , label = Translations.logoutIconLabel
    }


isOpen : AnimationTimeline -> Bool
isOpen =
    Animator.current


mobileNavWidthLimit : number
mobileNavWidthLimit =
    500


mainViewportWidth : { width : Int, fontSize : Int } -> Int
mainViewportWidth { width, fontSize } =
    if width >= mobileNavWidthLimit then
        width - 3 * fontSize

    else
        width


view : { auth : AuthManager, isCurrentlyOpen : Bool, showMobileView : Bool, userData : UserData } -> { onClick : msg, onLogout : msg } -> List (Main.Html.Html msg)
view { isCurrentlyOpen, showMobileView, userData } events =
    let
        iconToNavEl extraLiAttrs icon =
            case icon.target of
                Link link ->
                    li (class "h-16 hover:bg-red-500 dark:hover:bg-red-700" :: extraLiAttrs)
                        [ a [ href (Route.fromRoute link), class "flex h-full w-full items-center gap-6 p-2" ]
                            [ iconHtmlWith { name = icon.iconName, description = icon.label, height = icon.iconHeight, width = icon.iconWidth }
                                [ class "w-8"
                                ]
                            , p [] [ text icon.label ]
                            ]
                        ]

                Action action ->
                    li (class "w-full" :: extraLiAttrs)
                        [ button ([ Events.onClick action, class "flex h-16 w-full items-center gap-6 p-2 hover:bg-red-500 dark:hover:bg-red-700" ] ++ extraLiAttrs)
                            [ iconHtmlWith { name = icon.iconName, description = icon.label, height = icon.iconHeight, width = icon.iconWidth }
                                [ class "w-8" ]
                            , p [] [ text icon.label ]
                            ]
                        ]

        logoutLinkNavEl =
            iconToNavEl [ class "mt-auto" ] (logoutLink events.onLogout)
    in
    [ nav
        [ class "pointer-events-none fixed z-20 flex h-screen overflow-y-auto dark:text-white"
        , class "text-sm transition-all"
        , if showMobileView then
            if isCurrentlyOpen then
                class "translate-x-0"

            else
                class "-translate-x-[12.5rem]"

          else
            class "w-12 overflow-x-hidden hover:w-52"
        ]
        [ ul [ class "pointer-events-auto flex h-full w-52 flex-col bg-gray-300 dark:bg-gray-500" ] <| (List.map (iconToNavEl []) (publicLinks userData) ++ [ logoutLinkNavEl ])
        , if showMobileView then
            div [ class "flex h-full items-center" ]
                [ button [ Events.onClick events.onClick, class "pointer-events-auto h-fit w-8 rounded-r-2xl py-2 dark:bg-gray-500" ]
                    [ iconHtmlWith { name = "arrow", description = \_ -> "Open nav", height = 50, width = 50 }
                        (if isCurrentlyOpen then
                            [ Html.WithContext.Attributes.style "transform" "scale(-1) translateX(0.5rem)" ]

                         else
                            []
                        )
                    ]
                ]

          else
            none
        ]
    , div
        (if showMobileView then
            [ class "fixed z-10 h-full w-full backdrop-blur-sm transition-opacity"
            , if isCurrentlyOpen then
                class "pointer-events-auto opacity-100"

              else
                class "pointer-events-none opacity-0"
            ]

         else
            []
        )
        []
    ]
