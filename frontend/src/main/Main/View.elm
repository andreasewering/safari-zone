module Main.View exposing (..)

import Auth exposing (AuthManager)
import Browser
import Html
import Html.Attributes
import Html.Lazy as Lazy
import Html.WithContext exposing (header, main_)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Lazy
import Main.Chat as Chat
import Main.Contacts.Detail
import Main.Contacts.Overview
import Main.Contacts.Route as Contacts
import Main.Context as Context
import Main.Dex
import Main.Html exposing (mainDomId)
import Main.MapSelect
import Main.Model exposing (Model)
import Main.Msg exposing (Msg(..))
import Main.Nav as Nav
import Main.Page404 as Page404
import Main.PokemonSelect
import Main.Route as Route
import Main.Safari.Overworld
import Main.Settings
import Main.TrainerSelect
import Shared.Background as Background
import UserData exposing (UserData)


view : Model -> Browser.Document Msg
view model =
    let
        document route =
            case route of
                Ok (Route.Dex dexRoute) ->
                    Main.Dex.view
                        { route = dexRoute
                        , liftAppraisal = DexAppraisalMessage
                        , liftDetail = DexDetailMessage
                        , liftShared = SharedMessage
                        }

                Ok (Route.Contacts (Contacts.GroupDetail groupId)) ->
                    Main.Contacts.Detail.view (Context.groupDetail groupId)

                Ok (Route.Contacts Contacts.Overview) ->
                    Main.Contacts.Overview.view Context.contactsOverview

                Ok Route.Safari ->
                    Main.Safari.Overworld.view Context.safari

                Ok (Route.Settings settingsRoute) ->
                    Main.Settings.view
                        { route = settingsRoute
                        , liftOverview = SettingsMessage
                        , liftQrCodeView = QrCodeViewMessage
                        , liftShared = SharedMessage
                        }

                Ok (Route.TrainerSelect trainerRoute) ->
                    Main.TrainerSelect.view
                        { route = trainerRoute
                        , liftDetail = TrainerDetailMessage
                        , liftOverview = TrainerSelectMessage
                        , liftShared = SharedMessage
                        }

                Ok Route.PokemonSelect ->
                    Main.PokemonSelect.view Context.pokemonSelect

                Ok Route.MapSelect ->
                    Main.MapSelect.view Context.mapSelect

                Err _ ->
                    Page404.view

        { title, body } =
            document model.route model

        showMobileNav =
            model.viewport.width < Nav.mobileNavWidthLimit
    in
    { title = title model.i18n
    , body =
        [ Html.WithContext.Lazy.lazy4 sidebar model.auth model.isNavigationOpen showMobileNav model.userData
            |> Html.WithContext.toHtml model.htmlContext
        , Chat.view { lift = Main.Msg.ChatMessage, liftShared = Main.Msg.SharedMessage } { height = model.viewport.height, chats = model.chats }
            |> Html.WithContext.toHtml model.htmlContext
        , main_
            [ Html.WithContext.Attributes.id mainDomId
            , Background.contrastingMarkerClass
            , Html.WithContext.Attributes.class "inline-container relative min-h-svh w-full overflow-x-clip text-black dark:text-white"
            ]
            body
            |> Html.WithContext.toHtml model.htmlContext
        , background model.background
        ]
    }


sidebar : AuthManager -> Bool -> Bool -> UserData -> Main.Html.Html Msg
sidebar auth isCurrentlyOpen showMobileNav userData =
    header
        [ Background.contrastingMarkerClass
        , if showMobileNav then
            class ""

          else
            class "h-1 min-w-[3rem]"
        ]
        (Nav.view { auth = auth, isCurrentlyOpen = isCurrentlyOpen, showMobileView = showMobileNav, userData = userData }
            { onClick = ClickedNav, onLogout = Logout }
        )


background : Background.Background -> Html.Html msg
background =
    Lazy.lazy <|
        \bg ->
            Html.img
                [ Html.Attributes.src <| Background.toUrl bg
                , Html.Attributes.class "fixed left-0 top-0 -z-20 h-screen w-screen object-cover"
                ]
                []
