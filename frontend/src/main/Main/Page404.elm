module Main.Page404 exposing (..)

import Atoms.Html exposing (text)
import Main.Html exposing (Document)
import Main.Translations as Translations


view : m -> Document msg
view _ =
    { title = Translations.notFoundTitle
    , body = [ text Translations.notFoundTitle ]
    }
