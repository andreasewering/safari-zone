module Main.Route exposing (..)

import Browser.Navigation as Navigation
import Main.Contacts.Route as Contacts
import Main.Dex.Route as Dex
import Main.Settings.Route as Settings
import Main.TrainerSelect.Route as TrainerSelect
import Url exposing (Url)
import UrlCodec exposing (UrlCodec, buildUnion, buildUrl, chain, parseUrl, s, top, union, variant0, variant1)


type KnownRoute
    = Dex Dex.Route
    | Safari
    | Settings Settings.Route
    | TrainerSelect TrainerSelect.Route
    | PokemonSelect
    | MapSelect
    | Contacts Contacts.Route


type alias Route =
    Result Url KnownRoute


codec : UrlCodec (KnownRoute -> a) a KnownRoute
codec =
    union
        (\_
          -- root redirect
          dex
          safari
          settings
          trainerSelect
          pokemonSelect
          mapSelect
          contacts
          value
         ->
            case value of
                Dex dexRoute ->
                    dex dexRoute

                Safari ->
                    safari

                Settings settingsRoute ->
                    settings settingsRoute

                TrainerSelect trainerSelectRoute ->
                    trainerSelect trainerSelectRoute

                PokemonSelect ->
                    pokemonSelect

                MapSelect ->
                    mapSelect

                Contacts contactsRoute ->
                    contacts contactsRoute
        )
        |> variant0 (Dex Dex.Overview) top
        |> variant1 Dex (s "dex" |> chain Dex.codec)
        |> variant0 Safari (s "safari")
        |> variant1 Settings (s "settings" |> chain Settings.codec)
        |> variant1 TrainerSelect (s "trainer" |> chain TrainerSelect.codec)
        |> variant0 PokemonSelect (s "starter")
        |> variant0 MapSelect (s "map-select")
        |> variant1 Contacts (s "contacts" |> chain Contacts.codec)
        |> buildUnion


navigate : { s | key : Navigation.Key } -> KnownRoute -> Cmd msg
navigate s r =
    fromRoute r |> Navigation.pushUrl s.key


replace : { s | key : Navigation.Key } -> KnownRoute -> Cmd msg
replace s r =
    fromRoute r |> Navigation.replaceUrl s.key


toRoute : Url -> Route
toRoute url =
    parseUrl codec url
        |> Result.fromMaybe url


fromRoute : KnownRoute -> String
fromRoute =
    buildUrl codec
