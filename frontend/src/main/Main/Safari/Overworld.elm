module Main.Safari.Overworld exposing
    ( Context
    , Msg
    , onAnimationTick
    , onPageLeave
    , onPageLoad
    , subscriptions
    , update
    , updateShared
    , view
    )

import Animator
import Atoms.Html as Html
import Auth
import Browser.Events
import Browser.Navigation
import Data.Units exposing (TileUnit(..))
import Domain.Coordinate
import Domain.Direction as Direction
import Domain.Encounter as Encounter
import Domain.PartnerPokemon as PartnerPokemon
import Domain.Player as Player
import Domain.Position as Position exposing (Position)
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events as Events
import Interop.Event
import Interop.WebSocket
import Interop.WebSocket.In as In
import InteropDefinitions
import Item exposing (Item)
import Json.Decode
import Main.Html exposing (Document)
import Main.Nav as Nav
import Main.RequestManager as RequestManager
import Main.Safari.Model as Safari exposing (Model, PageModel, init)
import Main.Safari.Overworld.Frame
import Main.Safari.Overworld.Gamepad as Gamepad
import Main.Safari.Overworld.Interaction
import Main.Safari.Overworld.Transition exposing (NewMapPosition)
import Main.Safari.Overworld.Types.ActivePokeballs as ActivePokeballs
import Main.Safari.Overworld.Types.ItemDrag as ItemDrag
import Main.Safari.Overworld.View.Bag as Bag
import Main.Safari.Overworld.View.GameCanvas as GameCanvas
import Main.Shared as Shared
import Main.Translations as Translations
import MapId exposing (MapId)
import Maybe.Extra
import OutsideOf exposing (isOutsideOf)
import PokemonNumber exposing (PokemonNumber)
import Proto.Gardevoir
import Proto.Kangaskhan.WebsocketMessageToClient as WebsocketMessageToClient
import Proto.Kangaskhan.WebsocketMessageToClient.Message as Message
import Protobuf.Token
import Shared.Types.Items as Items
import Shared.UpdateUtil as UpdateUtil
import Tagged exposing (tag, untag)
import Time
import TrainerNumber exposing (TrainerNumber)
import UserIdDict


type Msg
    = GamepadMsg Gamepad.Msg
    | Frame Float
    | ToggleBackpack
    | ChooseItem Item
    | DragItem { x : Float, y : Float }
    | ReleaseItem Item


type alias Context msg =
    { lift : Msg -> msg, liftShared : Shared.Msg -> msg }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx ({ safariModel } as model) =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register (requests model) model.requestManager

        setPartner =
            case model.userData.partnerPokemon of
                Just partner ->
                    PartnerPokemon.setPartnerInfo (Auth.getUserId model.auth) partner.pokemonNumber partner.isShiny

                Nothing ->
                    identity

        newModel =
            { model
                | requestManager = requestManager
                , safariModel =
                    { safariModel
                        | partnerPokemons = setPartner safariModel.partnerPokemons
                    }
            }

        ( finalModel, mapCmds ) =
            case getMapPosition newModel of
                Just newMapPosition ->
                    onMapLoad newMapPosition newModel

                Nothing ->
                    ( newModel, Cmd.none )
    in
    ( finalModel
    , Cmd.batch
        [ requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
        , mapCmds |> Cmd.map ctx.liftShared
        ]
    )


getMapPosition : Model m -> Maybe NewMapPosition
getMapPosition model =
    let
        positionToNewMapPosition : Position -> Maybe NewMapPosition
        positionToNewMapPosition pos =
            Maybe.map
                (\mapId ->
                    { mapId = mapId
                    , layerNumber = Position.getCurrentLayerNumber pos
                    , position = Position.getNextTile pos
                    }
                )
                (Auth.getMapIdMaybe model.auth)
    in
    model.userData.position |> Maybe.andThen positionToNewMapPosition


requests : Model m -> List RequestManager.ApiCall
requests model =
    [ RequestManager.GetUserItems {}
    , RequestManager.GetAnimationConfig {}
    , RequestManager.GetTilesetConfig {}
    ]
        ++ Maybe.Extra.toList
            (model.userData.trainerSprite
                |> Maybe.map RequestManager.LoadTrainerTexture
            )
        ++ (model.userData.partnerPokemon
                |> Maybe.map
                    (\{ pokemonNumber } ->
                        [ RequestManager.GetPokemon { pokemonNumber = untag pokemonNumber }
                        , RequestManager.LoadPokemonTexture pokemonNumber
                        ]
                    )
                |> Maybe.withDefault []
           )


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave _ model =
    let
        mapId =
            Auth.getMapIdMaybe model.auth

        ( requestManager, requestManagerCmds ) =
            RequestManager.unregister
                (requests model
                    ++ (Maybe.map mapSpecificRequests mapId |> Maybe.withDefault [])
                )
                model.requestManager

        isOwnPartner userId _ =
            userId == Auth.getUserId model.auth
    in
    ( { model
        | safariModel =
            { init
                | partnerPokemons =
                    UserIdDict.filter isOwnPartner model.safariModel.partnerPokemons
            }
        , requestManager = requestManager
        , mapTransitionTimeline = Main.Safari.Overworld.Transition.initTimeline
      }
    , Cmd.batch
        [ Maybe.map Interop.WebSocket.disconnectFromKangaskhan mapId
            |> Maybe.withDefault Cmd.none
        , requestManagerCmds
        ]
    )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ safariModel } as model) =
    case msg of
        Frame dt ->
            Main.Safari.Overworld.Frame.handleFrame dt model

        GamepadMsg gamepadMsg ->
            ( { model | safariModel = { safariModel | gamepad = Gamepad.update gamepadMsg safariModel.gamepad } }, Cmd.none )

        ToggleBackpack ->
            ( { model
                | safariModel =
                    { safariModel
                        | isBackpackOpen = toggleBackpack safariModel.isBackpackOpen
                        , activeItem = Nothing
                    }
              }
            , Cmd.none
            )

        ChooseItem item ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.LoadItemTexture item ]
                        model.requestManager
            in
            ( { model
                | safariModel =
                    { safariModel
                        | activeItem = Just item
                        , isBackpackOpen = toggleBackpack safariModel.isBackpackOpen
                        , gamepad = Gamepad.init
                    }
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        DragItem drag ->
            ( { model
                | safariModel =
                    { safariModel
                        | itemDrag = ItemDrag.updateDiff drag safariModel.itemDrag
                    }
              }
            , Cmd.none
            )

        ReleaseItem item ->
            Main.Safari.Overworld.Interaction.releaseItem item model


updateShared : Context msg -> Shared.Msg -> Model m -> ( Model m, Cmd msg )
updateShared ctx msg ({ safariModel } as model) =
    case msg of
        Shared.LoadedUserData userData ->
            let
                userRequests =
                    [ Maybe.map (\pokemon -> RequestManager.LoadPokemonTexture (tag pokemon.pokemonNumber)) userData.partnerPokemon
                    , Maybe.map (\pokemon -> RequestManager.GetPokemon { pokemonNumber = pokemon.pokemonNumber }) userData.partnerPokemon
                    , Maybe.map (\trainer -> RequestManager.LoadTrainerTexture (tag trainer.trainerNumber)) userData.trainer
                    ]
                        |> List.filterMap identity

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register userRequests model.requestManager
            in
            { model
                | safariModel =
                    { safariModel
                        | partnerPokemons =
                            Maybe.map
                                (\{ pokemonNumber, isShiny } ->
                                    PartnerPokemon.setPartnerInfo (Auth.getUserId model.auth)
                                        (tag pokemonNumber)
                                        isShiny
                                )
                                userData.partnerPokemon
                                |> Maybe.withDefault identity
                                |> (|>) safariModel.partnerPokemons
                    }
                , requestManager = requestManager
            }
                |> UpdateUtil.chain
                    [ \m -> ( m, requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared) )
                    , UpdateUtil.maybe
                        (\{ x, y, mapId, layerNumber } ->
                            onMapLoad { mapId = tag mapId, layerNumber = layerNumber, position = { x = x, y = y } }
                        )
                        userData.position
                        |> UpdateUtil.liftCmd ctx.liftShared
                    ]

        Shared.ReceivedMessageFromJS (InteropDefinitions.KangaskhanIn (In.Message mapId wsMsg)) ->
            handleWebsocketMessage ctx mapId wsMsg model

        Shared.ReceivedMessageFromJS (InteropDefinitions.KangaskhanIn (In.Connected mapId)) ->
            ( { model
                | safariModel =
                    { safariModel
                        | websocketConnection = Safari.setConnected mapId safariModel.websocketConnection
                    }
              }
            , Cmd.none
            )

        Shared.ReceivedMessageFromJS (InteropDefinitions.KangaskhanIn (In.Closed mapId)) ->
            ( { model
                | safariModel =
                    { safariModel
                        | websocketConnection = Safari.setDisconnected mapId safariModel.websocketConnection
                    }
              }
            , Cmd.none
            )

        Shared.LoadedTileset _ ->
            endSafariTransitionIfAllSettled model

        Shared.LoadedTilemapTexture _ _ ->
            endSafariTransitionIfAllSettled model

        Shared.AuthRefreshed { previousAuth } ->
            if Auth.getMapId previousAuth /= Auth.getMapId model.auth then
                case getMapPosition model of
                    Just newMapPosition ->
                        let
                            ( newModel, cmds ) =
                                onMapLoad newMapPosition model
                        in
                        ( newModel, Cmd.map ctx.liftShared cmds )

                    Nothing ->
                        ( model, Cmd.none )

            else
                ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


onAnimationTick : Time.Posix -> Model m -> ( Model m, Cmd msg )
onAnimationTick _ model =
    if
        Animator.arrived model.mapTransitionTimeline
            == Main.Safari.Overworld.Transition.Finished
            && Animator.current model.mapTransitionTimeline
            == Main.Safari.Overworld.Transition.Finished
    then
        ( model, Interop.Event.mapLoaded (Auth.getMapId model.auth) )

    else
        endSafariTransitionIfAllSettled model


onMapLoad : NewMapPosition -> Model m -> ( Model m, Cmd Shared.Msg )
onMapLoad newMapPosition model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register (mapSpecificRequests newMapPosition.mapId)
                model.requestManager

        ( safariModel, websocketCmds ) =
            connectToWebsocket
                (Auth.switchMap newMapPosition.mapId model.auth |> Tuple.first)
                model.safariModel
    in
    ( { model
        | mapTransitionTimeline = Main.Safari.Overworld.Transition.start newMapPosition model.mapTransitionTimeline
        , requestManager = requestManager
        , safariModel = safariModel
      }
    , Cmd.batch
        [ requestManagerCmds |> Cmd.map Shared.RequestManagerMessage
        , websocketCmds
        ]
    )


mapSpecificRequests : MapId -> List RequestManager.ApiCall
mapSpecificRequests mapId =
    [ RequestManager.GetAreas { mapId = untag mapId }
    , RequestManager.GetTiles { mapId = untag mapId }
    , RequestManager.GetTextureMetadata { mapId = untag mapId }
    ]


toggleBackpack : Animator.Timeline Bool -> Animator.Timeline Bool
toggleBackpack isBackpackOpen =
    Animator.go Animator.quickly
        (not <| Animator.current isBackpackOpen)
        isBackpackOpen


subscriptions : Context msg -> PageModel -> Sub msg
subscriptions ctx model =
    Sub.batch
        [ Sub.map (GamepadMsg >> ctx.lift) <| Gamepad.subscriptions model.gamepad
        , Browser.Events.onAnimationFrameDelta (Frame >> ctx.lift)
        ]


view : Context msg -> Model m -> Document msg
view ctx ({ safariModel } as model) =
    { title = Translations.safariTitle
    , body =
        [ GameCanvas.view model
        , Main.Safari.Overworld.Transition.view model.mapTransitionTimeline
        , div
            [ class "fixed bottom-2 right-2"
            , isOutsideOf "id" Bag.domId
                |> Json.Decode.map
                    (\_ ->
                        if Animator.current safariModel.isBackpackOpen then
                            ctx.lift ToggleBackpack

                        else
                            ctx.liftShared Shared.NoOp
                    )
                |> Events.on "click"
            ]
            [ Bag.view
                { chooseItem = ChooseItem >> ctx.lift
                , toggleBackpack = ctx.lift ToggleBackpack
                , noOp = ctx.liftShared Shared.NoOp
                , releaseItem = ReleaseItem >> ctx.lift
                , dragItem = DragItem >> ctx.lift
                }
                { collectedItems = model.collectedItems
                , isBackpackOpen = safariModel.isBackpackOpen
                , activeItem = safariModel.activeItem
                , itemDrag = safariModel.itemDrag
                }
            ]
        , if model.viewport.width >= Nav.mobileNavWidthLimit then
            Html.none

          else
            Gamepad.view safariModel.gamepad { onEvent = GamepadMsg >> ctx.lift }
        ]
    }


handleWebsocketMessage : Context msg -> MapId -> WebsocketMessageToClient.Message -> Model m -> ( Model m, Cmd msg )
handleWebsocketMessage ctx mapId wsMsg ({ safariModel } as model) =
    let
        onPokemonCatch { encounterId, itemId } =
            ( { model
                | safariModel =
                    { safariModel
                        | activePokeballs =
                            ActivePokeballs.setCatch itemId encounterId safariModel.activePokeballs
                        , encounters =
                            safariModel.encounters
                                |> Encounter.startCatchAnimation encounterId
                    }
              }
            , Cmd.none
            )

        onPokemonBreakOut { encounterId, itemId } =
            ( { model
                | safariModel =
                    { safariModel
                        | activePokeballs =
                            ActivePokeballs.setBreakOut itemId encounterId safariModel.activePokeballs
                        , encounters =
                            safariModel.encounters
                                |> Encounter.startCatchAnimation encounterId
                    }
              }
            , Cmd.none
            )

        onMovePartnerPokemon { coordX, coordY, layerNumber, userId, direction } =
            case Direction.fromProto direction of
                Just dir ->
                    ( { model
                        | safariModel =
                            { safariModel
                                | partnerPokemons =
                                    PartnerPokemon.registerMove (tag userId)
                                        { x = coordX, y = coordY }
                                        layerNumber
                                        dir
                                        safariModel.partnerPokemons
                            }
                      }
                    , Cmd.none
                    )

                Nothing ->
                    ( model, Cmd.none )
    in
    case wsMsg of
        Message.MovePartnerPokemon movePartnerPokemon ->
            onMovePartnerPokemon movePartnerPokemon

        Message.PokemonCaught pokemonCaught ->
            onPokemonCatch { itemId = tag pokemonCaught.itemId, encounterId = pokemonCaught.encounterId }

        Message.PokemonFled _ ->
            --TODO
            ( model, Cmd.none )

        Message.PokemonBrokeOut pokemonBrokeOut ->
            onPokemonBreakOut { itemId = tag pokemonBrokeOut.itemId, encounterId = pokemonBrokeOut.encounterId }

        Message.GeneratedBallId itemId ->
            ( { model
                | safariModel =
                    { safariModel
                        | activePokeballs = ActivePokeballs.assignIdToThrownBall (tag itemId) safariModel.activePokeballs
                    }
              }
            , Cmd.none
            )

        Message.CollectedItem collectedItem ->
            ( Maybe.map
                (\item ->
                    { model | collectedItems = Items.syncItem item collectedItem.amount model.collectedItems }
                )
                (Items.itemFromProto collectedItem.item)
                |> Maybe.withDefault model
            , Cmd.none
            )

        Message.ItemSpawned spawnedItem ->
            Maybe.map
                (\item ->
                    let
                        itemId =
                            tag spawnedItem.itemId

                        ( wasActivePokeball, newPokeballs ) =
                            ActivePokeballs.makeBallMissIfActive itemId safariModel.activePokeballs

                        ( requestManager, requestManagerCmds ) =
                            RequestManager.register [ RequestManager.LoadItemTexture item ] model.requestManager
                    in
                    if wasActivePokeball then
                        ( { model | safariModel = { safariModel | activePokeballs = newPokeballs } }, Cmd.none )

                    else
                        ( { model
                            | safariModel =
                                { safariModel
                                    | spawnedItems =
                                        Items.addSpawnedItem
                                            { layerNumber = spawnedItem.layerNumber
                                            , position = { x = spawnedItem.x, y = spawnedItem.y }
                                            , mapId = mapId
                                            , itemId = itemId
                                            , item = item
                                            }
                                            safariModel.spawnedItems
                                }
                            , requestManager = requestManager
                          }
                        , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
                        )
                )
                (Items.itemFromProto spawnedItem.item)
                |> Maybe.withDefault ( model, Cmd.none )

        Message.ItemDespawned itemId ->
            ( { model | safariModel = { safariModel | spawnedItems = Items.removeSpawnedItem (tag itemId) safariModel.spawnedItems } }
            , Cmd.none
            )

        Message.PlayerMoved { userId, coordX, coordY, layerNumber, direction } ->
            case Direction.fromProto direction of
                Just dir ->
                    ( { model
                        | safariModel =
                            { safariModel
                                | players =
                                    Player.movePlayer
                                        { id = tag userId
                                        , mapId = mapId
                                        , position = { x = coordX, y = coordY }
                                        , layerNumber = layerNumber
                                        , direction = dir
                                        }
                                        safariModel.players
                            }
                      }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        Message.PlayerWentOffline userId ->
            let
                offlineId =
                    tag userId
            in
            ( { model
                | safariModel =
                    { safariModel
                        | players = Player.removePlayer offlineId safariModel.players
                        , partnerPokemons = UserIdDict.remove offlineId safariModel.partnerPokemons
                    }
              }
            , Cmd.none
            )

        Message.PlayerEnteredMap { userId, coordX, coordY, direction, layerNumber, trainerNumber, userName, partnerPokemonNumber, partnerIsShiny } ->
            let
                position =
                    { x = coordX, y = coordY }

                enteredUserId =
                    tag userId

                partnerPokemonNumber_ : PokemonNumber
                partnerPokemonNumber_ =
                    tag partnerPokemonNumber

                trainerNumber_ : TrainerNumber
                trainerNumber_ =
                    tag trainerNumber

                userRequests =
                    [ RequestManager.LoadPokemonTexture partnerPokemonNumber_
                    , RequestManager.GetPokemon { pokemonNumber = partnerPokemonNumber }
                    , RequestManager.LoadTrainerTexture trainerNumber_
                    ]

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register userRequests model.requestManager
            in
            ( { model
                | safariModel =
                    { safariModel
                        | players =
                            Player.addPlayer
                                { id = enteredUserId
                                , name = userName
                                , position = position
                                , direction = Direction.fromProto direction |> Maybe.withDefault Direction.default
                                , layerNumber = layerNumber
                                , trainerNumber = trainerNumber_
                                , mapId = mapId
                                }
                                safariModel.players
                        , partnerPokemons =
                            PartnerPokemon.setPartnerInfo
                                enteredUserId
                                partnerPokemonNumber_
                                partnerIsShiny
                                safariModel.partnerPokemons
                    }
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        Message.PlayerChangedPartnerPokemon { userId, pokemonNumber, isShiny } ->
            let
                pokemonNumber_ : PokemonNumber
                pokemonNumber_ =
                    tag pokemonNumber

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.GetPokemon { pokemonNumber = pokemonNumber }
                        , RequestManager.LoadPokemonTexture pokemonNumber_
                        ]
                        model.requestManager
            in
            ( { model
                | safariModel =
                    { safariModel
                        | partnerPokemons =
                            PartnerPokemon.setPartnerInfo (tag userId) pokemonNumber_ isShiny safariModel.partnerPokemons
                    }
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        Message.PlayerChangedTrainerSprite { userId, trainerNumber } ->
            let
                trainerNumber_ =
                    tag trainerNumber

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.LoadTrainerTexture trainerNumber_ ]
                        model.requestManager
            in
            ( { model
                | safariModel =
                    { safariModel | players = Player.setTrainerNumber trainerNumber_ (tag userId) safariModel.players }
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        Message.PlayerThrewBall throw ->
            case Items.itemFromProto throw.item of
                Just item ->
                    let
                        ( requestManager, requestManagerCmds ) =
                            RequestManager.register [ RequestManager.LoadItemTexture item ] model.requestManager
                    in
                    ( { model
                        | safariModel =
                            { safariModel
                                | activePokeballs =
                                    ActivePokeballs.fromOtherUserThrow
                                        ( throw.angleRadians, { x = throw.vectorX, y = throw.vectorY } )
                                        (Domain.Coordinate.map TileUnit { x = throw.fromX, y = throw.fromY })
                                        item
                                        (tag throw.ballId)
                                        safariModel.activePokeballs
                            }
                        , requestManager = requestManager
                      }
                    , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
                    )

                _ ->
                    ( model, Cmd.none )

        Message.NewEncounter newEncounter ->
            let
                pokemonNumber =
                    tag newEncounter.pokemonNumber

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.GetPokemon { pokemonNumber = newEncounter.pokemonNumber }
                        , RequestManager.LoadPokemonTexture pokemonNumber
                        ]
                        model.requestManager
            in
            ( { model
                | safariModel = { safariModel | encounters = Encounter.insertEncounter mapId newEncounter safariModel.encounters }
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )

        Message.MoveEncounter moveEncounter ->
            ( { model | safariModel = { safariModel | encounters = Encounter.moveEncounter moveEncounter safariModel.encounters } }
            , Cmd.none
            )

        Message.RemoveEncounter encounterId ->
            ( { model | safariModel = { safariModel | encounters = Encounter.removeEncounter encounterId safariModel.encounters } }, Cmd.none )

        Message.EnteredWarp warp ->
            case Protobuf.Token.decode Proto.Gardevoir.decodeAccessTokenClaims warp.newMapToken of
                Ok { claims } ->
                    let
                        position =
                            { x = warp.toX, y = warp.toY }

                        toNewMapPosition newMapId =
                            { mapId = tag newMapId, layerNumber = warp.layerNumber, position = position }

                        authentication =
                            Auth.decodedTokenToAuth { validForSeconds = warp.tokenValidForSeconds, accessToken = warp.newMapToken } claims
                    in
                    claims.mapId
                        |> Maybe.map toNewMapPosition
                        |> UpdateUtil.maybe onMapLoad
                        |> UpdateUtil.liftCmd ctx.liftShared
                        |> (|>)
                            { model
                                | auth =
                                    Auth.addToken authentication model.auth

                                -- TODO previously the position was set to loading here, which we cannot do anymore. Instead we need to mark some request as loading!
                            }

                Err _ ->
                    ( model, Cmd.none )

        Message.NewVersionDeployed version ->
            ( model
            , if model.version /= version then
                Browser.Navigation.reload

              else
                Cmd.none
            )

        _ ->
            ( model, Cmd.none )


endSafariTransitionIfAllSettled : Model m -> ( Model m, Cmd msg )
endSafariTransitionIfAllSettled ({ safariModel, userData } as model) =
    let
        isLoading { mapId } =
            List.any (\req -> RequestManager.status req model.requestManager == RequestManager.Loading)
                [ RequestManager.GetTilesetConfig {}
                , RequestManager.GetTextureMetadata { mapId = untag mapId }
                ]

        finishResult =
            Main.Safari.Overworld.Transition.tryFinish model.mapTransitionTimeline
                |> Maybe.Extra.filter (\( _, newMapPosition ) -> not <| isLoading newMapPosition)
    in
    case finishResult of
        Just ( mapTransitionTimeline, { mapId, position, layerNumber } ) ->
            let
                -- We trust that we did not forget requesting a token when starting the map transition
                auth =
                    Auth.switchMap mapId model.auth |> Tuple.first

                previousMapId =
                    Auth.getMapId model.auth

                ownUserId =
                    Auth.getUserId auth

                newMapId =
                    Auth.getMapId auth

                ( requestManager, requestManagerCmds ) =
                    if previousMapId == newMapId then
                        ( model.requestManager, Cmd.none )

                    else
                        RequestManager.unregister (mapSpecificRequests previousMapId)
                            model.requestManager

                ( newSafariModel, disconnectCmds ) =
                    if previousMapId == newMapId then
                        ( safariModel, Cmd.none )

                    else
                        -- Use the old mapId here on purpose. We want to disconnect from the old map since
                        -- the transition has now finished.
                        disconnectFromWebsocket previousMapId safariModel

                addPartner =
                    case userData.partnerPokemon of
                        Just partner ->
                            PartnerPokemon.setPartnerInfo
                                ownUserId
                                partner.pokemonNumber
                                partner.isShiny

                        Nothing ->
                            identity
            in
            ( { model
                | userData = { userData | position = Just <| Position.init position layerNumber Direction.default }
                , mapTransitionTimeline = mapTransitionTimeline
                , auth = auth
                , requestManager = requestManager
                , safariModel =
                    { newSafariModel
                        | partnerPokemons =
                            Safari.removePartnerPokemonNotOnSameMap model
                                |> addPartner
                        , players = Player.switchMapTo mapId safariModel.players
                        , encounters = Encounter.switchMapTo mapId safariModel.encounters
                    }
              }
            , Cmd.batch [ requestManagerCmds, disconnectCmds ]
            )

        Nothing ->
            ( model, Cmd.none )


connectToWebsocket : Auth.AuthManager -> PageModel -> ( PageModel, Cmd msg )
connectToWebsocket auth safariModel =
    let
        mapId =
            Auth.getMapId auth
    in
    case Safari.getWebsocketStatus mapId safariModel.websocketConnection of
        Safari.Disconnected ->
            ( { safariModel
                | websocketConnection =
                    Safari.setConnecting mapId safariModel.websocketConnection
              }
            , Interop.WebSocket.connectToKangaskhan auth
            )

        Safari.Connecting ->
            ( safariModel, Cmd.none )

        Safari.Connected ->
            ( safariModel, Cmd.none )


disconnectFromWebsocket : MapId -> PageModel -> ( PageModel, Cmd msg )
disconnectFromWebsocket mapId safariModel =
    case Safari.getWebsocketStatus mapId safariModel.websocketConnection of
        Safari.Disconnected ->
            ( safariModel, Cmd.none )

        Safari.Connecting ->
            ( { safariModel
                | websocketConnection =
                    Safari.setDisconnected mapId safariModel.websocketConnection
              }
            , Interop.WebSocket.disconnectFromKangaskhan mapId
            )

        Safari.Connected ->
            ( { safariModel
                | websocketConnection =
                    Safari.setDisconnected mapId safariModel.websocketConnection
              }
            , Interop.WebSocket.disconnectFromKangaskhan mapId
            )
