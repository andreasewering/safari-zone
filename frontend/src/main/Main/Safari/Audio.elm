module Main.Safari.Audio exposing (startAudio)

import AudioTrackNumber exposing (AudioTrackNumber)
import Auth
import BoundedDict
import Domain.Position as Position
import Interop.Audio
import Interop.Audio.Out
import Main.Safari.Model exposing (Model)
import Maybe.Extra
import Shared.Types.Area as Area
import Shared.Types.Audio as Audio


startAudio : Model m -> Cmd msg
startAudio model =
    let
        audioTrackToLoop : AudioTrackNumber -> Maybe Interop.Audio.Out.AudioLoop
        audioTrackToLoop audioTrackNumber =
            model.audioConfig
                |> Audio.getLoop audioTrackNumber
                |> Maybe.map (\loop -> { filename = loop.filename, loopStart = loop.start, loopEnd = loop.end })
    in
    if model.audioIsActive then
        Maybe.Extra.andThen3 Area.getCurrentAudioTrack
            (model.userData.position |> Maybe.map Position.getCurrentLayerNumber)
            (model.userData.position |> Maybe.map Position.comingFrom)
            (BoundedDict.get (Auth.getMapId model.auth) model.areas)
            |> Maybe.andThen audioTrackToLoop
            |> Maybe.map Interop.Audio.loop
            |> Maybe.withDefault Interop.Audio.fadeOutAndStop

    else
        Cmd.none
