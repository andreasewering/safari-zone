module Main.Safari.Model exposing
    ( Model
    , PageModel
    , WebsocketConnectionStatus(..)
    , addAnimation
    , addAnimations
    , getWebsocketStatus
    , init
    , removePartnerPokemonNotOnSameMap
    , setConnected
    , setConnecting
    , setDisconnected
    )

import Animator
import Auth
import BoundedDict
import Domain.Encounter as Encounters exposing (Encounters)
import Domain.PartnerPokemon as PartnerPokemon exposing (PartnerPokemons)
import Domain.Player as Players exposing (Players)
import Item exposing (Item)
import Main.Safari.Overworld.Gamepad exposing (Gamepad)
import Main.Safari.Overworld.Types.ActivePokeballs exposing (ActivePokeballs)
import Main.Safari.Overworld.Types.ItemDrag as ItemDrag exposing (ItemDrag)
import Main.Shared as Shared
import MapId exposing (MapId)
import MapIdDict exposing (MapIdDict)
import Maybe.Extra
import Shared.Types.Animations as Animations exposing (ActiveAnimations)
import Shared.Types.Generic exposing (Setter)
import Shared.Types.Items as Items exposing (SpawnedItems)
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import UserIdDict


type alias PageModel =
    { activeAnimations : Animations.ActiveAnimations
    , activeItem : Maybe Item
    , activePokeballs : ActivePokeballs
    , encounters : Encounters
    , gamepad : Gamepad
    , isBackpackOpen : Animator.Timeline Bool
    , itemDrag : ItemDrag
    , partnerPokemons : PartnerPokemons
    , players : Players
    , spawnedItems : SpawnedItems
    , trainer : TrainerState
    , websocketConnection : WebsocketConnection
    }


type WebsocketConnectionStatus
    = Connected
    | Connecting
    | Disconnected


{-| We want to model this such that only valid constellations are possible to model.

1.  Initially, we are not connected to a websocket.
2.  Once we visit the safari page, we start a connection attempt on a specific map.
3.  When the connected attempt succeeds, we are connected to a specific map.
4.  When we switch maps, we start a connection attempt on a new map, while retaining the connection to the old map.
    Why? Because when the animation for the switch runs, we still want to see players and pokemon move. A sudden stop might look strange (even if the animation is rather quick).
5.  Once the first half animation for the map switch is over (the new map is shown), we can disconnect from the old map.

Therefore:

  - We can only ever be connected to a maximum of two maps
  - There is a need for a "Connecting" and "Connected" state, but not a "Disconnecting" state, since we might
    as well just optimistically call it "Disconnected" (we do not want to send any messages anymore).

-}
type alias WebsocketConnection =
    MapIdDict WebsocketConnectionStatus


setConnected : MapId -> WebsocketConnection -> WebsocketConnection
setConnected mapId =
    MapIdDict.insert mapId Connected


setConnecting : MapId -> WebsocketConnection -> WebsocketConnection
setConnecting mapId =
    MapIdDict.insert mapId Connecting


setDisconnected : MapId -> WebsocketConnection -> WebsocketConnection
setDisconnected mapId =
    MapIdDict.remove mapId


getWebsocketStatus : MapId -> WebsocketConnection -> WebsocketConnectionStatus
getWebsocketStatus mapId =
    MapIdDict.get mapId >> Maybe.withDefault Disconnected


type alias Model m =
    Shared.Session
        { m
            | safariModel : PageModel
        }


init : PageModel
init =
    { trainer = TrainerState.init
    , gamepad = Main.Safari.Overworld.Gamepad.init
    , activePokeballs = []
    , activeItem = Nothing
    , isBackpackOpen = Animator.init False
    , itemDrag = ItemDrag.empty
    , encounters = Encounters.empty
    , spawnedItems = Items.empty
    , activeAnimations = Animations.init
    , partnerPokemons = PartnerPokemon.init
    , players = Players.empty
    , websocketConnection = MapIdDict.empty
    }


addAnimation : MapId -> Animations.Position -> Model m -> Setter ActiveAnimations
addAnimation mapId pos model =
    Maybe.map
        (Animations.addAnimation pos model.animationConfig)
        (BoundedDict.get mapId model.animationTileDict)
        |> Maybe.withDefault identity


addAnimations : MapId -> Model m -> List Animations.Position -> Setter ActiveAnimations
addAnimations mapId model positions animations =
    List.foldl (\pos -> addAnimation mapId pos model) animations positions


removePartnerPokemonNotOnSameMap : Model m -> PartnerPokemons
removePartnerPokemonNotOnSameMap ({ safariModel } as model) =
    let
        currentMapId =
            Auth.getMapId model.auth

        onSameMap { mapId } =
            currentMapId == mapId

        partnerOnSameMap userId _ =
            userId
                == Auth.getUserId model.auth
                || (Players.getPlayer userId safariModel.players
                        |> Maybe.Extra.filter onSameMap
                        |> Maybe.Extra.isJust
                   )
    in
    safariModel.partnerPokemons
        |> UserIdDict.filter partnerOnSameMap
