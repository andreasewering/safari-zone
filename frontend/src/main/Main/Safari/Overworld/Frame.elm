module Main.Safari.Overworld.Frame exposing (handleFrame)

import Api.WebsocketApi
import Auth
import BoundedDict
import Config
import Data.Units exposing (unTileUnit)
import Domain.ConstrainedPosition
import Domain.Coordinate as Coordinate
import Domain.Encounter as Encounter
import Domain.PartnerPokemon as PartnerPokemon
import Domain.Player as Player
import Domain.Position as Position
import Domain.QueueablePosition as QueueablePosition
import Interop.Audio
import Interop.Event as Event
import Main.RequestManager as RequestManager
import Main.Safari.Audio
import Main.Safari.Model as Safari exposing (Model)
import Main.Safari.Overworld.Gamepad as Gamepad
import Main.Safari.Overworld.Types.ActivePokeballs as ActivePokeballs
import Main.Safari.Overworld.Types.ItemDrag as ItemDrag
import Main.Safari.Overworld.View.Bag
import Maybe.Extra
import Shared.Types.Animations as Animations
import Shared.Types.Items as Items
import Shared.Types.TrainerState as TrainerState
import Tagged exposing (untag)


handleFrame : Float -> Model m -> ( Model m, Cmd msg )
handleFrame dt model =
    let
        ( { safariModel } as newModel, movementCmds ) =
            movement dt model

        newItemDrag =
            ItemDrag.commit safariModel.itemDrag

        mapId =
            Auth.getMapId model.auth

        ( newActivePokeballs, { itemsToSpawn, caughtEncounters, brokeOutEncounters } ) =
            ActivePokeballs.update dt safariModel.activePokeballs

        handleBrokeOutEncounters encs =
            List.foldl Encounter.startBreakOutAnimation encs brokeOutEncounters

        toSpawnedItem ( itemId, item, itemPos ) =
            Maybe.map
                (\layerNumber ->
                    { itemId = itemId
                    , item = item
                    , position = itemPos |> Coordinate.map unTileUnit
                    , layerNumber = layerNumber
                    , mapId = Auth.getMapId newModel.auth
                    }
                )
                (newModel.userData.position |> Maybe.map Position.getCurrentLayerNumber)

        ( encounters, encounterAnimations ) =
            Encounter.removeEncounters (List.map .encounterId caughtEncounters) safariModel.encounters
                |> handleBrokeOutEncounters
                |> Encounter.continueMoves dt model.pokemonDataDict

        ( players, playersEnteringNewTile ) =
            Player.continueMoves (Config.trainerSpeed * dt) safariModel.players

        eventCmds =
            playersEnteringNewTile
                |> List.map (\( userId, standingPos ) -> Event.playerMoveComplete userId standingPos)

        updatedModel =
            { newModel
                | safariModel =
                    { safariModel
                        | activePokeballs = newActivePokeballs
                        , itemDrag = newItemDrag
                        , encounters = encounters
                        , spawnedItems = Items.addSpawnedItems (List.filterMap toSpawnedItem itemsToSpawn) safariModel.spawnedItems
                        , activeAnimations =
                            Animations.continueAnimations dt safariModel.activeAnimations
                                |> Safari.addAnimations mapId model encounterAnimations
                        , players = players
                    }
            }

        cmds =
            case ( caughtEncounters |> List.any .wasOwnBall, model.audioConfig.onPokemonObtained ) of
                ( True, Just onPokemonObtained ) ->
                    [ movementCmds, Interop.Audio.start onPokemonObtained.filename ]

                _ ->
                    [ movementCmds ]
    in
    ( updatedModel, Cmd.batch <| eventCmds ++ cmds )


movement : Float -> Model m -> ( Model m, Cmd msg )
movement dt ({ safariModel, userData } as model) =
    let
        mapRules =
            BoundedDict.get mapId model.moveRules

        canMoveManually =
            case
                ( userData.position
                , RequestManager.status
                    (RequestManager.GetTiles { mapId = untag mapId })
                    model.requestManager
                )
            of
                ( Just _, RequestManager.Success ) ->
                    Safari.getWebsocketStatus mapId safariModel.websocketConnection
                        == Safari.Connected

                _ ->
                    False

        mapId =
            Auth.getMapId model.auth

        directionToMoveIn =
            Gamepad.getDirection safariModel.gamepad
                |> Maybe.Extra.filter (\_ -> canMoveManually)
    in
    case ( userData.position, mapRules ) of
        ( Just pos, Just rules ) ->
            let
                newPos =
                    Domain.ConstrainedPosition.move directionToMoveIn (dt * Config.trainerSpeed) rules pos
                        |> setDirectionToMirrorItemDrag

                setDirectionToMirrorItemDrag =
                    ItemDrag.toDirection Main.Safari.Overworld.View.Bag.deadzoneRadius safariModel.itemDrag
                        |> Maybe.map Position.setDirection
                        |> Maybe.withDefault identity

                directionToLookAt =
                    ItemDrag.toDirection Main.Safari.Overworld.View.Bag.deadzoneRadius safariModel.itemDrag
                        |> Maybe.withDefault (Position.getCurrentDirection newPos)

                mayPreviousStandingPosition =
                    Position.isStandingPosition pos

                mayCurrentStandingPosition =
                    Position.isStandingPosition newPos

                mayPreviousPartnerStandingPosition =
                    PartnerPokemon.getPartnerPosition (Auth.getUserId model.auth) safariModel.partnerPokemons
                        |> Maybe.andThen QueueablePosition.isStandingPosition

                newPartnerPokemons =
                    PartnerPokemon.continueMoves dt
                        model.pokemonDataDict
                        safariModel.partnerPokemons

                mayCurrentPartnerStandingPosition =
                    PartnerPokemon.getPartnerPosition (Auth.getUserId model.auth) newPartnerPokemons
                        |> Maybe.andThen QueueablePosition.isStandingPosition

                shouldPartnerMove =
                    Maybe.Extra.isJust mayPreviousStandingPosition && Maybe.Extra.isNothing mayCurrentStandingPosition

                hasMoved =
                    Position.hasMoved pos newPos

                action =
                    if hasMoved then
                        TrainerState.move dt

                    else
                        TrainerState.stop

                eventCmds =
                    case mayCurrentStandingPosition |> Maybe.Extra.filter (\_ -> hasMoved || Position.getCurrentDirection newPos /= Position.getCurrentDirection pos) of
                        Just arrivedPosition ->
                            [ Event.playerMoveComplete (Auth.getUserId model.auth) arrivedPosition ]

                        Nothing ->
                            []

                cmds =
                    case mayPreviousStandingPosition |> Maybe.andThen (\_ -> directionToMoveIn) of
                        Just dir ->
                            if Position.hasMoved pos newPos then
                                Cmd.batch [ Api.WebsocketApi.sendMove mapId dir, Main.Safari.Audio.startAudio model ]

                            else if dir /= Position.getCurrentDirection pos then
                                Api.WebsocketApi.sendTurn mapId dir

                            else
                                Cmd.none

                        Nothing ->
                            Cmd.none

                newAnimations : Animations.ActiveAnimations
                newAnimations =
                    List.filterMap identity
                        [ mayPreviousStandingPosition |> Maybe.Extra.filter (\_ -> shouldPartnerMove)
                        , mayPreviousPartnerStandingPosition |> Maybe.Extra.filter (\_ -> Maybe.Extra.isNothing mayCurrentPartnerStandingPosition)
                        ]
                        |> List.map
                            (\prevPos ->
                                { prevPos | direction = Position.getCurrentDirection newPos }
                            )
                        |> Safari.addAnimations mapId model
                        |> (|>) safariModel.activeAnimations

                newSafariModel =
                    { safariModel
                        | trainer =
                            action safariModel.trainer
                                |> TrainerState.changeDirection directionToLookAt
                        , partnerPokemons = newPartnerPokemons
                        , activeAnimations = newAnimations
                    }
            in
            ( { model
                | safariModel = newSafariModel
                , userData =
                    if newPos == pos then
                        userData

                    else
                        { userData | position = Just newPos }
              }
            , Cmd.batch (cmds :: eventCmds)
            )

        _ ->
            ( model, Cmd.none )
