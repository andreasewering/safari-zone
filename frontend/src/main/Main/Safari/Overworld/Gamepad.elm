module Main.Safari.Overworld.Gamepad exposing (Button(..), Gamepad, Msg(..), getDirection, init, subscriptions, update, view)

import Browser.Events
import Domain.Direction exposing (Direction(..))
import Html.Events.Extra.Pointer as Pointer
import Html.WithContext exposing (div, htmlAttribute)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events as Events
import Json.Decode as D
import Main.Html exposing (Html)
import Maybe.Extra
import Shared.View.Icon exposing (iconHtml)


type Msg
    = Pressed Button
    | Released Button
    | SwitchTo ArrowButton
    | MobileContextMenu
    | ReleaseArrows


type Button
    = Directional ArrowButton


type ArrowButton
    = GoLeft
    | GoRight
    | GoUp
    | GoDown


toArrowButton : Button -> Maybe ArrowButton
toArrowButton button =
    case button of
        Directional arrow ->
            Just arrow


isArrowButton : Button -> Bool
isArrowButton =
    toArrowButton >> Maybe.Extra.isJust


getDirection : Gamepad -> Maybe Direction
getDirection =
    getArrowButtons
        >> List.head
        >> Maybe.map buttonToDirection


buttonToDirection : ArrowButton -> Direction
buttonToDirection button =
    case button of
        GoLeft ->
            L

        GoRight ->
            R

        GoUp ->
            U

        GoDown ->
            D


type Gamepad
    = Gamepad (List Button)


init : Gamepad
init =
    Gamepad []


getArrowButtons : Gamepad -> List ArrowButton
getArrowButtons (Gamepad pad) =
    List.filterMap toArrowButton pad


pressButton : Button -> Gamepad -> Gamepad
pressButton button (Gamepad pad) =
    Gamepad <| button :: pad


releaseButton : Button -> Gamepad -> Gamepad
releaseButton button (Gamepad pad) =
    Gamepad <| List.filter (\btn -> button /= btn) pad


releaseArrows : Gamepad -> Gamepad
releaseArrows (Gamepad pad) =
    Gamepad <| List.filter (not << isArrowButton) pad


switchToArrow : ArrowButton -> Gamepad -> Gamepad
switchToArrow arrowButton =
    releaseArrows >> pressButton (Directional arrowButton)


subscriptions : Gamepad -> Sub Msg
subscriptions (Gamepad pad) =
    Sub.batch
        [ Browser.Events.onKeyDown
            (decodeButton
                |> D.andThen
                    (\button ->
                        if List.member button pad then
                            D.fail "Skip"

                        else
                            D.succeed <| Pressed button
                    )
            )
        , Browser.Events.onKeyUp (D.map Released decodeButton)
        ]


update : Msg -> Gamepad -> Gamepad
update msg =
    case msg of
        Pressed button ->
            pressButton button

        Released button ->
            releaseButton button

        SwitchTo button ->
            switchToArrow button

        ReleaseArrows ->
            releaseArrows

        MobileContextMenu ->
            identity


view : Gamepad -> { onEvent : Msg -> msg } -> Html msg
view _ events =
    Html.WithContext.withContext <|
        \{ fontSize } ->
            div
                [ class "fixed bottom-2 left-2 h-28 w-28 touch-none rounded-3xl bg-gray-300/50"
                , Events.preventDefaultOn "contextmenu" <| D.succeed ( events.onEvent MobileContextMenu, True )
                , htmlAttribute <|
                    Pointer.onDown
                        (\ev ->
                            let
                                ( offsetX, offsetY ) =
                                    ev.pointer.offsetPos
                            in
                            (mouseEventToGamepadMsg fontSize >> Directional >> Pressed >> events.onEvent)
                                { offsetX = round offsetX, offsetY = round offsetY }
                        )
                , htmlAttribute <|
                    Pointer.onMove
                        (\ev ->
                            let
                                ( offsetX, offsetY ) =
                                    ev.pointer.offsetPos
                            in
                            (mouseEventToGamepadMsg fontSize >> SwitchTo >> events.onEvent)
                                { offsetX = round offsetX, offsetY = round offsetY }
                        )
                , htmlAttribute <|
                    Pointer.onLeave (always (ReleaseArrows |> events.onEvent))
                , htmlAttribute <|
                    Pointer.onUp (always (ReleaseArrows |> events.onEvent))
                ]
                [ iconHtml { name = "dpad", width = sizeRem, height = sizeRem, description = \_ -> "Dpad" } ]


sizeRem : number
sizeRem =
    7


mouseEventToGamepadMsg : Int -> MouseEvent -> ArrowButton
mouseEventToGamepadMsg fontSize { offsetX, offsetY } =
    case ( offsetX > offsetY, offsetX + offsetY > fontSize * sizeRem ) of
        ( True, True ) ->
            GoRight

        ( True, False ) ->
            GoUp

        ( False, True ) ->
            GoDown

        ( False, False ) ->
            GoLeft


type alias MouseEvent =
    { offsetX : Int
    , offsetY : Int
    }


decodeButton : D.Decoder Button
decodeButton =
    D.andThen toButton
        (D.field "key" D.string)


toButton : String -> D.Decoder Button
toButton string =
    case String.toLower string of
        "arrowleft" ->
            D.succeed <| Directional GoLeft

        "arrowright" ->
            D.succeed <| Directional GoRight

        "arrowdown" ->
            D.succeed <| Directional GoDown

        "arrowup" ->
            D.succeed <| Directional GoUp

        _ ->
            D.fail "Skip"
