module Main.Safari.Overworld.View.SpriteEntity exposing (..)

import Camera exposing (Camera)
import Data.Units exposing (unWebGLUnit)
import Math.Matrix4 as Mat4
import Math.Vector2 exposing (vec2)
import Math.Vector3 exposing (vec3)
import Shared.Types.SpriteEntityProps exposing (SpriteEntityProps)
import Textures.Store exposing (Sprite)
import WebGL
import WebGL.Common


entity :
    Camera
    -> Sprite
    -> SpriteEntityProps
    -> WebGL.Entity
entity camera { texture, columns, rows } { spriteId, position, opacity, size, rotation } =
    let
        col =
            modBy columns spriteId

        row =
            floor (toFloat spriteId / toFloat columns)

        spritePercent =
            vec2 (1 / toFloat columns) (1 / toFloat rows)

        scalingX =
            size / toFloat (Camera.getScreenWidth camera)

        scalingY =
            size / toFloat (Camera.getScreenHeight camera)

        trans =
            Camera.calcWebGLCoord camera position
    in
    WebGL.entityWith
        WebGL.Common.settings
        WebGL.Common.vertexShader
        WebGL.Common.spriteSheetFragmentShader
        WebGL.Common.mesh
        { perspective =
            Mat4.makeScale3 scalingX scalingY 1
                |> Mat4.translate3 (unWebGLUnit trans.x / scalingX) (unWebGLUnit trans.y / scalingY) 0
                |> Mat4.rotate rotation (vec3 0 0 1)
        , row = toFloat row
        , col = toFloat col
        , spriteSheet = texture
        , spritePercent = spritePercent
        , opacity = opacity
        }
