module Main.Safari.Overworld.View.PokemonEntity exposing (..)

import Domain.Direction as Direction exposing (Direction)
import Domain.Encounter exposing (Action, CurrentAction(..))
import Shared.Types.FrameBased


dirToIndex : Direction -> Action -> Bool -> Int
dirToIndex dir action isShiny =
    let
        normalIndex =
            case ( dir, Shared.Types.FrameBased.current action ) of
                ( Direction.L, StepLeft ) ->
                    0

                ( Direction.L, StepRight ) ->
                    1

                ( Direction.R, StepLeft ) ->
                    4

                ( Direction.R, StepRight ) ->
                    5

                ( Direction.U, StepLeft ) ->
                    8

                ( Direction.U, StepRight ) ->
                    9

                ( Direction.D, StepLeft ) ->
                    12

                ( Direction.D, StepRight ) ->
                    13
    in
    if isShiny then
        normalIndex + 2

    else
        normalIndex
