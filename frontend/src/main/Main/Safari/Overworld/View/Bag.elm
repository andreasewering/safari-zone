module Main.Safari.Overworld.View.Bag exposing (deadzoneRadius, throwingGamepad, view, domId)

import Animator
import Html.Events.Extra.Pointer as Pointer
import Html.WithContext exposing (div, li)
import Html.WithContext.Attributes exposing (class, id, style)
import Html.WithContext.Events as Events
import Html.WithContext.Keyed as Keyed
import Item exposing (Item)
import Json.Decode as D
import Main.Html exposing (Html)
import Main.Safari.Overworld.Types.ItemDrag exposing (ItemDrag)
import Main.Translations as Translations
import Shared.Types.Items as Items
import Shared.View.Icon exposing (iconHtmlWith)
import Shared.View.Sprite as Sprite


type alias Events msg =
    { toggleBackpack : msg
    , chooseItem : Item -> msg
    , noOp : msg
    , releaseItem : Item -> msg
    , dragItem : { x : Float, y : Float } -> msg
    }


domId : String
domId =
    "bag"


view :
    Events msg
    ->
        { collectedItems : Items.CollectedItems
        , isBackpackOpen : Animator.Timeline Bool
        , activeItem : Maybe Item
        , itemDrag : ItemDrag
        }
    -> Html msg
view events ({ collectedItems, isBackpackOpen, activeItem } as model) =
    case activeItem of
        Just item ->
            throwingGamepad events model item

        Nothing ->
            let
                itemList =
                    collectedItems |> Items.collectedItemsToList

                maxHeight =
                    itemDisplaySize * List.length itemList + 1

                animateHeight isOpen =
                    if isOpen then
                        Animator.at <| toFloat maxHeight

                    else
                        Animator.at 0
            in
            div [ id domId, class "flex flex-col items-center" ]
                [ div
                    [ class "flex w-full justify-center overflow-hidden rounded-b-[20px] rounded-t-[50px] bg-gray-700/70"
                    , style "height" <| String.fromInt (floor <| Animator.move isBackpackOpen animateHeight) ++ "rem"
                    , style "transform" "translate(0, 55px)"
                    ]
                    [ Keyed.ul
                        [ class "flex flex-col py-2"
                        ]
                      <|
                        List.map (renderItem events) itemList
                    ]
                , iconHtmlWith
                    { name =
                        if Animator.current isBackpackOpen || Animator.arrived isBackpackOpen then
                            "backpack-open"

                        else
                            "backpack"
                    , width = 4
                    , height = 5.5
                    , description = Translations.safariBagDescription
                    }
                    [ Events.onClick events.toggleBackpack
                    ]
                ]


renderItem : Events msg -> ( Item, Int ) -> ( String, Html msg )
renderItem events ( item, amount ) =
    let
        formattedAmount =
            if amount > 99 then
                "99+"

            else
                String.fromInt amount
    in
    ( Items.serialize item
    , li [ class "flex" ]
        [ renderItemWith [ Events.onClick <| events.chooseItem item ] item
        , Html.WithContext.text formattedAmount
        ]
    )


renderItemWith : List (Main.Html.Attribute msg) -> Item -> Html msg
renderItemWith attrs item =
    Html.WithContext.withContext <|
        \{ i18n } ->
            Sprite.view attrs
                (Items.toDisplaySpriteLocation itemDisplaySize item)
                { displayName = itemTranslation item i18n, imagePath = Item.toSpritePath item }


gamepadRadius : Float
gamepadRadius =
    3.5


deadzoneRadius : Float
deadzoneRadius =
    0.1


throwingGamepad : Events msg -> { m | itemDrag : ItemDrag } -> Item -> Html msg
throwingGamepad events { itemDrag } item =
    div
        [ class "rounded-full bg-gray-300/50"
        , style "width" (String.fromFloat (2 * gamepadRadius) ++ "rem")
        , style "height" (String.fromFloat (2 * gamepadRadius) ++ "rem")
        ]
        [ div
            [ class "relative left-1/2 top-1/2 w-fit"
            , style "transform"
                ("translate(calc("
                    ++ String.fromFloat (itemDrag.x * gamepadRadius)
                    ++ "rem - 50%), calc("
                    ++ String.fromFloat (itemDrag.y * gamepadRadius)
                    ++ "rem - 50%))"
                )
            ]
            [ Html.WithContext.withContext <|
                \{ fontSize } ->
                    renderItemWith
                        [ Html.WithContext.Attributes.style "touch-action" "none"
                        , Events.preventDefaultOn "contextmenu" (D.succeed ( events.noOp, True ))
                        , Html.WithContext.htmlAttribute <| Pointer.onWithOptions "pointerdown" { stopPropagation = True, preventDefault = True } (eventToDrag fontSize >> events.dragItem)
                        , Html.WithContext.htmlAttribute <| Pointer.onWithOptions "pointermove" { stopPropagation = True, preventDefault = True } (eventToDrag fontSize >> events.dragItem)
                        , Html.WithContext.htmlAttribute <| Pointer.onUp (always <| events.releaseItem item)
                        , Html.WithContext.htmlAttribute <| Pointer.onLeave (always <| events.releaseItem item)
                        ]
                        item
            ]
        ]


itemDisplaySize : number
itemDisplaySize =
    4


eventToDrag : Int -> Pointer.Event -> { x : Float, y : Float }
eventToDrag fontSize { pointer } =
    let
        ( offsetX, offsetY ) =
            pointer.offsetPos
    in
    { x = (offsetX - 20.5) / (gamepadRadius * toFloat fontSize)
    , y = (offsetY - 20.5) / (gamepadRadius * toFloat fontSize)
    }


itemTranslation : Item -> Translations.I18n -> String
itemTranslation item =
    case item of
        Item.Pokeball ->
            Translations.itemPokeball

        Item.Superball ->
            Translations.itemSuperball

        Item.Hyperball ->
            Translations.itemHyperball

        Item.Quickball ->
            Translations.itemQuickball

        Item.Diveball ->
            Translations.itemDiveball
