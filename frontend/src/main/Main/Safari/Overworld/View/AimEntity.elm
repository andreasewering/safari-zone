module Main.Safari.Overworld.View.AimEntity exposing (entity)

import Camera exposing (Camera)
import Data.Units exposing (TileUnit(..), unTileUnit, unWebGLUnit)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Main.Safari.Overworld.Types.ItemDrag as ItemDrag
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 exposing (Vec2)
import Math.Vector3 as Vec3 exposing (Vec3, vec3)
import WebGL
import WebGL.Common


size : number
size =
    120



-- | Interpolate between two colors


interpolateColors : Vec3 -> Vec3 -> Float -> Vec3
interpolateColors low high t =
    let
        mult =
            clamp 0 1 t
    in
    vec3
        (mult * Vec3.getX high + (1 - mult) * Vec3.getX low)
        (mult * Vec3.getY high + (1 - mult) * Vec3.getY low)
        (mult * Vec3.getZ high + (1 - mult) * Vec3.getZ low)


arrowColor : Float -> Vec3
arrowColor =
    interpolateColors (vec3 1 1 0) (vec3 1 0 0)


entity : Camera -> ( Float, Coordinate Float ) -> WebGL.Entity
entity camera ( angle, vector ) =
    let
        scalingX =
            size / toFloat (Camera.getScreenWidth camera)

        scalingY =
            size / toFloat (Camera.getScreenHeight camera)

        color =
            arrowColor (angle / ItemDrag.maxAngle)

        trans =
            Camera.calcWebGLCoord camera
                (Camera.getPlayerCoord camera
                    |> Coordinate.map unTileUnit
                    -- Magic constant to be better centered with the player sprite
                    |> Coordinate.plus { x = 0, y = 0.2 }
                    |> Coordinate.map TileUnit
                )

        rotation =
            -- we render the bottom and left borders of a square, we need to rotate accordingly
            -1 * Basics.atan2 vector.y vector.x - pi * 5 / 4
    in
    WebGL.entityWith WebGL.Common.settings
        WebGL.Common.vertexShader
        fragmentShader
        WebGL.Common.mesh
        { perspective =
            Mat4.makeScale3 scalingX scalingY 1
                |> Mat4.translate3 (unWebGLUnit trans.x / scalingX) (unWebGLUnit trans.y / scalingY) 0
                |> Mat4.rotate rotation (vec3 0 0 1)
        , color = color
        , arrowWidth = 0.1
        , arrowSize = 0.3
        }


type alias Uniforms =
    { perspective : Mat4
    , color : Vec3
    , arrowWidth : Float
    , arrowSize : Float
    }


fragmentShader : WebGL.Shader {} Uniforms { vcoord : Vec2 }
fragmentShader =
    [glsl|
        precision mediump float;
        uniform vec3 color;
        uniform float arrowSize;
        uniform float arrowWidth;

        varying vec2 vcoord;

        void main () {
            if(!(vcoord.x < arrowWidth || vcoord.y < arrowWidth))
              discard;
            if (vcoord.x > arrowSize || vcoord.y > arrowSize)
              discard;  
            gl_FragColor = vec4(color, 1.0);
        }
    |]
