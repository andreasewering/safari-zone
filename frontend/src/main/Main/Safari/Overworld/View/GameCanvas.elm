module Main.Safari.Overworld.View.GameCanvas exposing (LayerType(..), constructCamera, orderLayerTypes, view)

import Atoms.Html
import Auth
import BoundedDict
import Camera exposing (Camera)
import Config
import Data.Units exposing (TileUnit(..), unTileUnit)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Direction as Direction
import Domain.Encounter
import Domain.Layer exposing (LayerNumber)
import Domain.PartnerPokemon as PartnerPokemon
import Domain.Player as Players
import Domain.Position as Position exposing (Position)
import Domain.QueueablePosition as QueueablePosition
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class, style)
import ItemDict
import Main.Html exposing (Html)
import Main.Nav exposing (mainViewportWidth)
import Main.RequestManager as RequestManager
import Main.Safari.Model exposing (Model)
import Main.Safari.Overworld.Types.ActivePokeballs as ActivePokeballs exposing (ActivePokeballs)
import Main.Safari.Overworld.Types.ItemDrag as ItemDrag
import Main.Safari.Overworld.View.AimEntity as AimEntity
import Main.Safari.Overworld.View.Bag as Bag
import Main.Safari.Overworld.View.PokemonEntity as PokemonEntity
import Main.Safari.Overworld.View.SpriteEntity as SpriteEntity
import MapId exposing (MapId)
import PokemonNumberDict
import Shared.Types.Animations as Animations
import Shared.Types.Generic exposing (Setter)
import Shared.Types.Items as Items
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import Shared.View.WebGLUtil as WebGLUtil
import Textures.Store exposing (Sprite, Tilemaps)
import TrainerNumberDict
import UserIdDict
import WebGL
import WebGL.Map
import WebGL.Texture exposing (Texture)


type LayerType
    = ItemLayer
    | PlayerLayer
    | OwnPlayerLayer
    | PokemonLayer
    | GroundLayer
    | DecorationLayer
    | OverlayLayer
    | AnimationLayer


type Layer
    = Layer Int


orderLayerTypes : LayerType -> LayerType -> Order
orderLayerTypes l1 l2 =
    if l1 == l2 then
        EQ

    else
        case ( l1, l2 ) of
            ( OverlayLayer, _ ) ->
                GT

            ( _, OverlayLayer ) ->
                LT

            ( OwnPlayerLayer, _ ) ->
                GT

            ( _, OwnPlayerLayer ) ->
                LT

            ( PlayerLayer, _ ) ->
                GT

            ( _, PlayerLayer ) ->
                LT

            ( PokemonLayer, _ ) ->
                GT

            ( _, PokemonLayer ) ->
                LT

            ( ItemLayer, _ ) ->
                GT

            ( _, ItemLayer ) ->
                LT

            ( AnimationLayer, _ ) ->
                GT

            ( _, AnimationLayer ) ->
                LT

            ( DecorationLayer, _ ) ->
                GT

            ( _, DecorationLayer ) ->
                LT

            _ ->
                EQ


orderLayers : Layer -> Layer -> Order
orderLayers (Layer l1) (Layer l2) =
    compare l1 l2


orderEntities : ( Layer, WebGL.Entity ) -> ( Layer, WebGL.Entity ) -> Order
orderEntities ( l1, _ ) ( l2, _ ) =
    orderLayers l1 l2


constructCamera : { s | width : Int, height : Int, position : Position } -> Tilemaps -> Camera
constructCamera model mapTextures =
    let
        ( totalTilesX, totalTilesY ) =
            WebGL.Texture.size mapTextures.texture

        tileMapSize =
            { x = TileUnit <| toFloat totalTilesX, y = TileUnit <| toFloat totalTilesY }

        offset =
            { x = TileUnit <| toFloat mapTextures.offset.x, y = TileUnit <| toFloat mapTextures.offset.y }
    in
    Camera.mkCamera (Position.getExact model.position) model { tileMapSize = tileMapSize, offset = offset }


view : Model m -> Html msg
view ({ safariModel, textures, userData, viewport, htmlContext } as model) =
    let
        mapId =
            Auth.getMapId model.auth

        preconditions =
            Maybe.map3 (\tilemaps tileset pos -> ( tilemaps, tileset, pos ))
                (BoundedDict.get mapId textures.tilemaps)
                textures.tileset
                userData.position
    in
    preconditions
        |> Maybe.map
            (\( mapTextures, tileset, pos ) ->
                let
                    canvasSession =
                        { width = mainViewportWidth { width = viewport.width, fontSize = htmlContext.fontSize }
                        , height = viewport.height
                        , position = pos
                        }

                    camera =
                        constructCamera canvasSession mapTextures

                    playerScreenCoords =
                        Camera.getPlayerCoord camera |> Camera.calcScreenCoord camera

                    mayTrainerNumberToTexture =
                        Maybe.andThen <|
                            \trainerNumber ->
                                TrainerNumberDict.get trainerNumber textures.trainerTextures

                    otherPlayers =
                        Players.getPlayers mapId safariModel.players
                            |> List.filterMap
                                (\{ name, state, position, trainerNumber } ->
                                    trainerNumber
                                        |> mayTrainerNumberToTexture
                                        |> Maybe.map (\texture -> { name = name, state = state, position = position, texture = texture })
                                )

                    tagOtherPlayer { name, position } =
                        playerTag (QueueablePosition.getExact position |> Camera.calcScreenCoord camera) name

                    mapEntities =
                        renderMap { tileset = tileset.texture, mapTextures = mapTextures } camera

                    ownPlayerEntity =
                        case userData.trainerSprite |> mayTrainerNumberToTexture of
                            Just texture ->
                                [ ( ownPlayerLayer <| Position.getCurrentLayerNumber pos
                                  , SpriteEntity.entity camera
                                        texture
                                        { spriteId = TrainerState.stateToIndex safariModel.trainer
                                        , opacity = 1
                                        , position = Camera.getPlayerCoord camera
                                        , size = Config.trainerSize
                                        , rotation = 0
                                        }
                                  )
                                ]

                            _ ->
                                []

                    mayThrow =
                        ItemDrag.dragToThrow Bag.deadzoneRadius safariModel.itemDrag

                    entities =
                        mapEntities
                            ++ renderPartnerPokemon model camera
                            ++ renderVisibleEncounters mapId model camera
                            ++ renderPlayers camera otherPlayers
                            ++ ownPlayerEntity
                            ++ renderPokeballs safariModel.activePokeballs model camera
                            ++ renderItems model camera
                            ++ renderAnimations model camera
                            ++ renderAimEntity mayThrow camera
                in
                Html.WithContext.div
                    [ class "relative"
                    ]
                    ((Html.WithContext.html <|
                        WebGLUtil.entitiesToElement canvasSession
                            []
                        <|
                            List.map Tuple.second <|
                                List.sortWith orderEntities entities
                     )
                        :: List.map tagOtherPlayer otherPlayers
                        ++ [ playerTag playerScreenCoords userData.displayName ]
                    )
            )
        |> Maybe.withDefault Atoms.Html.none


ownPlayerLayer : LayerNumber -> Layer
ownPlayerLayer n =
    Layer (n * 100 + 40)


renderMap : { tileset : Texture, mapTextures : Tilemaps } -> Camera -> List ( Layer, WebGL.Entity )
renderMap model camera =
    let
        layersTotal =
            List.length model.mapTextures.layers
    in
    List.map Layer model.mapTextures.layers
        |> List.indexedMap
            (\layerIndex layer ->
                let
                    entity =
                        WebGL.Map.entity camera
                            1
                            model.mapTextures.offset
                            model.tileset
                            model.mapTextures.texture
                            layersTotal
                            layerIndex
                in
                ( layer, entity )
            )


playerLayer : LayerNumber -> Layer
playerLayer n =
    Layer (n * 100 + 28)


renderPlayers :
    Camera
    -> List { a | texture : Sprite, state : TrainerState, position : QueueablePosition.QueueablePosition }
    -> List ( Layer, WebGL.Entity )
renderPlayers camera =
    List.map
        (\{ texture, state, position } ->
            let
                layerNumber =
                    QueueablePosition.getCurrentLayerNumber position

                entity =
                    SpriteEntity.entity camera
                        texture
                        { spriteId = TrainerState.stateToIndex state
                        , opacity = 1
                        , position = QueueablePosition.getExact position
                        , size = Config.trainerSize
                        , rotation = 0
                        }
            in
            ( playerLayer layerNumber, entity )
        )


partnerPokemonLayer : LayerNumber -> Layer
partnerPokemonLayer n =
    Layer (n * 100 + 25)


renderPartnerPokemon : Model m -> Camera -> List ( Layer, WebGL.Entity )
renderPartnerPokemon model camera =
    Main.Safari.Model.removePartnerPokemonNotOnSameMap model
        |> UserIdDict.values
        |> List.filterMap PartnerPokemon.fullyInitialized
        |> List.filterMap
            (\partner ->
                PokemonNumberDict.get partner.pokemonNumber model.textures.pokemonTextures
                    |> Maybe.map
                        (\texture ->
                            let
                                layer =
                                    partnerPokemonLayer <| QueueablePosition.getCurrentLayerNumber partner.position
                            in
                            ( layer
                            , SpriteEntity.entity camera
                                texture
                                { spriteId = PokemonEntity.dirToIndex (QueueablePosition.getCurrentDirection partner.position) partner.action partner.shinyStatus
                                , opacity = 1
                                , position =
                                    QueueablePosition.getExact partner.position
                                        |> shiftPokemonPosition
                                , size = Config.pokemonSize
                                , rotation = 0
                                }
                            )
                        )
            )


encounterLayer : LayerNumber -> Layer
encounterLayer n =
    Layer (n * 100 + 35)


renderVisibleEncounters : MapId -> Model m -> Camera -> List ( Layer, WebGL.Entity )
renderVisibleEncounters mapId model camera =
    Domain.Encounter.getEncounters mapId model.safariModel.encounters
        |> List.filterMap
            (\enc ->
                PokemonNumberDict.get enc.pokemonNumber model.textures.pokemonTextures
                    |> Maybe.map
                        (\texture ->
                            let
                                layer =
                                    encounterLayer <| QueueablePosition.getCurrentLayerNumber enc.position

                                dir =
                                    QueueablePosition.getCurrentDirection enc.position
                            in
                            ( layer
                            , SpriteEntity.entity camera
                                texture
                                { spriteId = PokemonEntity.dirToIndex dir enc.action enc.shinyStatus
                                , opacity = 1
                                , position = QueueablePosition.getExact enc.position |> shiftPokemonPosition
                                , size = Config.pokemonSize * Domain.Encounter.getSizeModifier enc
                                , rotation = 0
                                }
                            )
                        )
            )


activePokeballLayer : Layer
activePokeballLayer =
    Layer 60


renderPokeballs : ActivePokeballs -> Model m -> Camera -> List ( Layer, WebGL.Entity )
renderPokeballs activePokeballs model camera =
    activePokeballs
        |> List.filterMap
            (\ball ->
                ItemDict.get ball.item model.textures.itemTextures
                    |> Maybe.map
                        (\texture ->
                            let
                                props =
                                    ActivePokeballs.toSpriteProps ball
                            in
                            ( activePokeballLayer
                            , SpriteEntity.entity camera
                                texture
                                { props | position = props.position |> shiftItemPosition }
                            )
                        )
            )


itemLayer : LayerNumber -> Layer
itemLayer n =
    Layer (n * 100 + 30)


renderItems : Model m -> Camera -> List ( Layer, WebGL.Entity )
renderItems ({ safariModel } as model) camera =
    Items.values safariModel.spawnedItems
        |> List.filterMap
            (\item ->
                ItemDict.get item.item model.textures.itemTextures
                    |> Maybe.map
                        (\texture ->
                            ( itemLayer item.layerNumber
                            , SpriteEntity.entity camera
                                texture
                                { spriteId = 0
                                , opacity = 1
                                , position =
                                    item.position
                                        |> Coordinate.map TileUnit
                                        |> shiftItemPosition
                                , size = Config.itemSize
                                , rotation = 0
                                }
                            )
                        )
            )


animationLayer : LayerNumber -> Layer
animationLayer n =
    Layer (n * 100 + 25)


renderAnimations : Model m -> Camera -> List ( Layer, WebGL.Entity )
renderAnimations ({ safariModel, textures } as model) camera =
    case
        ( RequestManager.status (RequestManager.GetAnimationConfig {}) model.requestManager
        , BoundedDict.get (Auth.getMapId model.auth) model.animationTileDict
        , textures.tileset
        )
    of
        ( RequestManager.Success, Just _, Just texture ) ->
            let
                renderSingle : Animations.ActiveAnimation -> ( Layer, WebGL.Entity )
                renderSingle anim =
                    ( animationLayer anim.position.layerNumber
                    , SpriteEntity.entity camera
                        texture
                        { spriteId = anim.index - 1
                        , opacity = anim.opacity
                        , size = Config.tileSize
                        , rotation = Direction.toRadians anim.position.direction
                        , position =
                            anim.position.coord
                                |> Coordinate.map toFloat
                                |> Coordinate.plus { x = anim.xShift, y = anim.yShift }
                                |> Coordinate.map TileUnit
                        }
                    )
            in
            List.map renderSingle (Animations.getActive safariModel.activeAnimations)

        _ ->
            []


aimLayer : LayerNumber -> Layer
aimLayer n =
    Layer ((n + 1) * 100 + 60)


renderAimEntity : Maybe ( Float, Coordinate Float ) -> Camera -> List ( Layer, WebGL.Entity )
renderAimEntity throwTarget camera =
    case throwTarget of
        Just target ->
            [ ( aimLayer 1, AimEntity.entity camera target ) ]

        Nothing ->
            []


playerTag : Coordinate Float -> String -> Html msg
playerTag { x, y } playerName =
    div
        [ class "absolute -translate-x-1/2 rounded-lg bg-sky-300 p-2 text-center opacity-70 dark:bg-sky-700 dark:text-white"
        , style "left" <| String.fromFloat x ++ "px"
        , style "top" <| String.fromFloat (y - Config.trainerSize / 2) ++ "px"
        ]
        [ Html.WithContext.text playerName ]


shiftPokemonPosition : Setter (Coordinate TileUnit)
shiftPokemonPosition =
    Coordinate.map unTileUnit
        >> Coordinate.plus { x = 0, y = 0.15 }
        >> Coordinate.map TileUnit


shiftItemPosition : Setter (Coordinate TileUnit)
shiftItemPosition =
    Coordinate.map unTileUnit
        >> Coordinate.plus { x = 0, y = 0.5 }
        >> Coordinate.map TileUnit
