module Main.Safari.Overworld.Types.ActivePokeballs exposing
    ( ActivePokeballs
    , Pokeball
    , addBall
    , assignIdToThrownBall
    , calcBallDistance
    , calcBallHeight
    , fromOtherUserThrow
    , makeBallMissIfActive
    , setBreakOut
    , setCatch
    , toSpriteProps
    , update
    )

import Config
import Data.Units exposing (TileUnit(..), unTileUnit)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Encounter exposing (EncounterId)
import Item exposing (Item)
import ItemId exposing (ItemId)
import Maybe.Extra
import Shared.Types.FrameBased as FrameBased exposing (FrameBased)
import Shared.Types.SpriteEntityProps exposing (SpriteEntityProps)


type alias ActivePokeballs =
    List Pokeball


type alias Pokeball =
    { start : Coordinate Float
    , angle : Float
    , vector : Coordinate Float
    , timeInSeconds : Float
    , state : FrameBased PokeballState
    , landed : Bool
    , status : BallStatus
    , item : Item
    , belongsToCurrentUser : Bool
    }


type BallStatus
    = Thrown
    | AssignedId ItemId
      -- a thrown ball gets a UUID assigned by the server
      -- this UUID appears again either in a catch/flee message or a item spawned message
    | WillCatch EncounterId
    | WillBreakOut EncounterId
    | Missed ItemId


{-| Assigns a backend generated ID to the _only_ thrown ball of the current user.
This assumes the invariant holds that there is only one ball in status thrown of the current user at a time
-}
assignIdToThrownBall : ItemId -> ActivePokeballs -> ActivePokeballs
assignIdToThrownBall itemId =
    List.map
        (\ball ->
            if ball.status == Thrown && ball.belongsToCurrentUser then
                { ball | status = AssignedId itemId }

            else
                ball
        )


{-| Sets the ball with the given ItemId to the `Missed` status, if it exists. Returns whether a modification was made or not.
-}
makeBallMissIfActive : ItemId -> ActivePokeballs -> ( Bool, ActivePokeballs )
makeBallMissIfActive itemId balls =
    case balls of
        [] ->
            ( False, [] )

        ball :: rest ->
            let
                recursiveCall _ =
                    makeBallMissIfActive itemId rest |> Tuple.mapSecond ((::) ball)
            in
            case ball.status of
                AssignedId id ->
                    if id == itemId then
                        ( True, { ball | status = Missed id } :: rest )

                    else
                        recursiveCall ()

                _ ->
                    recursiveCall ()


setCatch : ItemId -> EncounterId -> ActivePokeballs -> ActivePokeballs
setCatch itemId encounterId =
    List.map
        (\ball ->
            if ball.status == AssignedId itemId then
                { ball | status = WillCatch encounterId }

            else
                ball
        )


setBreakOut : ItemId -> EncounterId -> ActivePokeballs -> ActivePokeballs
setBreakOut itemId encounterId =
    List.map
        (\ball ->
            if ball.status == AssignedId itemId then
                { ball | status = WillBreakOut encounterId }

            else
                ball
        )


toSpriteProps : Pokeball -> SpriteEntityProps
toSpriteProps ball =
    let
        ( position, scaling ) =
            ballMovementAndScaling ball
    in
    { spriteId = stateToIndex ball.state.curr
    , opacity = 1
    , position = position
    , size = Config.itemSize * scaling
    , rotation = 0
    }


type PokeballState
    = Flying Int
    | Open
    | Wobble Int


transitionState : PokeballState -> ( PokeballState, Int )
transitionState state =
    case state of
        Flying i ->
            ( Flying <| i + 1, 30 )

        Open ->
            ( Wobble 0, 400 )

        Wobble i ->
            ( Wobble <| i + 1, 200 )


stateToIndex : PokeballState -> Int
stateToIndex state =
    case state of
        Flying i ->
            modBy 9 i

        Open ->
            10

        Wobble i ->
            11 + modBy 6 i


ballMovementAndScaling : Pokeball -> ( Coordinate TileUnit, Float )
ballMovementAndScaling ({ start, vector } as ball) =
    let
        addFloatCoordToStart =
            Coordinate.plus start >> Coordinate.map TileUnit

        distance =
            calcBallDistance ball

        height =
            calcBallHeight ball

        scale =
            abs (sin (Coordinate.angle vector)) * height / 2 + 1

        yDiff =
            abs (cos (Coordinate.angle vector)) * height

        x =
            vector.x * distance

        y =
            vector.y * distance
    in
    ( addFloatCoordToStart { x = x, y = y - yDiff }, scale )


calcBallDistance : { a | angle : Float, timeInSeconds : Float } -> Float
calcBallDistance { angle, timeInSeconds } =
    Config.startVelocity * cos angle * timeInSeconds


calcBallHeight : { a | angle : Float, timeInSeconds : Float } -> Float
calcBallHeight { angle, timeInSeconds } =
    Config.startVelocity * sin angle * timeInSeconds - 0.5 * Config.gravity * timeInSeconds ^ 2


addBall :
    Item
    -> ( Float, Coordinate Float )
    -> Coordinate TileUnit
    -> ActivePokeballs
    -> ActivePokeballs
addBall item ( angle, vector ) start activePokeballs =
    let
        ball : Pokeball
        ball =
            { start = start |> Coordinate.map unTileUnit
            , angle = angle
            , vector = vector
            , timeInSeconds = 0
            , state = FrameBased.initFrameBasedIrregular (Flying 0) transitionState 1
            , landed = False
            , status = Thrown
            , item = item
            , belongsToCurrentUser = True
            }
    in
    ball :: activePokeballs


fromOtherUserThrow : ( Float, Coordinate Float ) -> Coordinate TileUnit -> Item -> ItemId -> ActivePokeballs -> ActivePokeballs
fromOtherUserThrow ( angle, vector ) startCoord item itemId =
    (::)
        { start = startCoord |> Coordinate.map unTileUnit
        , angle = angle
        , vector = vector
        , timeInSeconds = 0
        , state = FrameBased.initFrameBasedIrregular (Flying 0) transitionState 1
        , landed = False
        , status = AssignedId itemId
        , item = item
        , belongsToCurrentUser = False
        }


type alias FinishedAnimations =
    { itemsToSpawn : List ( ItemId, Item, Coordinate TileUnit )
    , caughtEncounters : List { encounterId : EncounterId, wasOwnBall : Bool }
    , brokeOutEncounters : List EncounterId
    }


noFinishedAnimations : FinishedAnimations
noFinishedAnimations =
    { itemsToSpawn = [], caughtEncounters = [], brokeOutEncounters = [] }


combineFinishedAnimations : FinishedAnimations -> FinishedAnimations -> FinishedAnimations
combineFinishedAnimations a b =
    { itemsToSpawn = a.itemsToSpawn ++ b.itemsToSpawn
    , caughtEncounters = a.caughtEncounters ++ b.caughtEncounters
    , brokeOutEncounters = a.brokeOutEncounters ++ b.brokeOutEncounters
    }


update : Float -> ActivePokeballs -> ( ActivePokeballs, FinishedAnimations )
update dt =
    List.foldr
        (\ball ( balls, finishedAnimations ) ->
            let
                ( updatedBall, singleFinishedAnimations ) =
                    updateOne dt ball
            in
            ( Maybe.Extra.toList updatedBall ++ balls
            , combineFinishedAnimations singleFinishedAnimations finishedAnimations
            )
        )
        ( [], noFinishedAnimations )


updateOne : Float -> Pokeball -> ( Maybe Pokeball, FinishedAnimations )
updateOne dt ball =
    let
        dtInSeconds =
            dt / 1000

        posUpdate =
            { ball
                | timeInSeconds =
                    if ball.landed then
                        ball.timeInSeconds

                    else
                        ball.timeInSeconds + dtInSeconds
            }

        hasJustLanded =
            calcBallHeight posUpdate < 0 && not ball.landed

        updatedBall =
            { posUpdate
                | landed = ball.landed || hasJustLanded
                , state =
                    if hasJustLanded then
                        FrameBased.initFrameBasedIrregular Open transitionState 50

                    else
                        FrameBased.nextFrame dt ball.state
            }

        animationIsReady =
            case FrameBased.current ball.state of
                Wobble i ->
                    i > 10

                _ ->
                    False
    in
    case updatedBall.status of
        Missed itemId ->
            let
                ( endPos, _ ) =
                    ballMovementAndScaling updatedBall
            in
            if updatedBall.landed then
                ( Nothing, { noFinishedAnimations | itemsToSpawn = [ ( itemId, ball.item, endPos ) ] } )

            else
                ( Just updatedBall, noFinishedAnimations )

        WillCatch encounterId ->
            if animationIsReady then
                ( Nothing
                , { noFinishedAnimations
                    | caughtEncounters =
                        [ { encounterId = encounterId, wasOwnBall = updatedBall.belongsToCurrentUser }
                        ]
                  }
                )

            else
                ( Just updatedBall, noFinishedAnimations )

        WillBreakOut encounterId ->
            if animationIsReady then
                ( Nothing, { noFinishedAnimations | brokeOutEncounters = [ encounterId ] } )

            else
                ( Just updatedBall, noFinishedAnimations )

        _ ->
            ( Just updatedBall
            , noFinishedAnimations
            )
