module Main.Safari.Overworld.Types.ItemDrag exposing (ItemDrag, commit, dragToThrow, empty, maxAngle, toDirection, updateDiff)

import Domain.Coordinate exposing (Coordinate)
import Domain.Direction as Direction exposing (Direction)


type alias ItemDrag =
    { x : Float
    , y : Float
    , xDiff : Float
    , yDiff : Float
    }


empty : ItemDrag
empty =
    { x = 0, y = 0, xDiff = 0, yDiff = 0 }


updateDiff : { x : Float, y : Float } -> ItemDrag -> ItemDrag
updateDiff { x, y } drag =
    { drag | xDiff = x, yDiff = y }


commit : ItemDrag -> ItemDrag
commit drag =
    let
        ( dragX, dragY ) =
            clampToCircle 1
                ( drag.x + drag.xDiff, drag.y + drag.yDiff )
    in
    { x = dragX, y = dragY, xDiff = 0, yDiff = 0 }


isInDeadzone : Float -> ItemDrag -> Bool
isInDeadzone deadzoneRadius { x, y } =
    abs x < deadzoneRadius && abs y < deadzoneRadius


dragToThrow : Float -> ItemDrag -> Maybe ( Float, Coordinate Float )
dragToThrow deadzoneRadius ({ x, y } as drag) =
    if isInDeadzone deadzoneRadius drag then
        Nothing

    else
        let
            angle =
                Basics.atan2 -y -x

            distance =
                sqrt (x ^ 2 + y ^ 2)
        in
        Just ( distance * maxAngle, { x = cos angle, y = sin angle } )


maxAngle : Float
maxAngle =
    pi * 3 / 7


toDirection : Float -> ItemDrag -> Maybe Direction
toDirection deadzoneRadius ({ x, y } as drag) =
    if isInDeadzone deadzoneRadius drag then
        Nothing

    else
        -- Direction is intentionally reversed here, since we drag the ball and release in the opposite direction
        case ( x > y, x + y > 0 ) of
            ( True, True ) ->
                Just Direction.L

            ( False, False ) ->
                Just Direction.R

            ( True, False ) ->
                Just Direction.D

            ( False, True ) ->
                Just Direction.U


clampToCircle : Float -> ( Float, Float ) -> ( Float, Float )
clampToCircle radius ( x, y ) =
    let
        distance =
            sqrt (x ^ 2 + y ^ 2)
    in
    if distance > radius then
        let
            angle =
                Basics.atan2 y x
        in
        ( cos angle * radius, sin angle * radius )

    else
        ( x, y )
