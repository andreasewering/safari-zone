module Main.Safari.Overworld.Transition exposing
    ( NewMapPosition
    , Timeline
    , Transition(..)
    , initTimeline
    , isFinished
    , isSettledState
    , start
    , tryFinish
    , view
    )

import Animator
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Html
import Html.Attributes
import Html.WithContext exposing (Html)
import Html.WithContext.Lazy
import MapId exposing (MapId)
import Shared.Types.Generic exposing (Setter)


initTimeline : Animator.Timeline Transition
initTimeline =
    Animator.init LoadingInitial


type alias NewMapPosition =
    { mapId : MapId, layerNumber : LayerNumber, position : Coordinate Int }


type Transition
    = Finished
    | LoadingInitial
    | LoadingNewMap NewMapPosition


type alias Timeline =
    Animator.Timeline Transition


isSettledState : Transition -> Bool
isSettledState transition =
    transition == Finished || transition == LoadingInitial


isFinished : Transition -> Bool
isFinished transition =
    transition == Finished


mapTransitionLengthInMs : number
mapTransitionLengthInMs =
    250


tryFinish : Timeline -> Maybe ( Timeline, NewMapPosition )
tryFinish timeline =
    case ( Animator.arrived timeline, Animator.current timeline ) of
        ( _, Finished ) ->
            Nothing

        ( LoadingNewMap position, _ ) ->
            Just ( Animator.go (Animator.millis mapTransitionLengthInMs) Finished timeline, position )

        _ ->
            Nothing


start : NewMapPosition -> Setter Timeline
start =
    LoadingNewMap
        >> Animator.go (Animator.millis mapTransitionLengthInMs)


view : Timeline -> Html ctx msg
view timeline =
    let
        backgroundSize =
            Animator.linear timeline <|
                \state ->
                    case state of
                        Finished ->
                            Animator.at 15000

                        LoadingInitial ->
                            Animator.at 100

                        LoadingNewMap _ ->
                            Animator.at 100
    in
    Html.WithContext.Lazy.lazy
        html
        backgroundSize


html : Float -> Html ctx msg
html size =
    Html.div
        [ Html.Attributes.class "pointer-events-none fixed left-0 top-0 z-20 h-screen w-screen"
        , Html.Attributes.style "background-size" (String.fromFloat size ++ "% " ++ String.fromFloat size ++ "%")
        , Html.Attributes.style "background-image" "radial-gradient(circle at 50%, transparent, 1%, black, 1%, black)"
        , Html.Attributes.style "background-position" "50%"
        ]
        []
        |> Html.WithContext.html
