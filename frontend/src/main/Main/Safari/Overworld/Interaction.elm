module Main.Safari.Overworld.Interaction exposing (releaseItem)

import Api.WebsocketApi
import Auth exposing (AuthManager)
import Domain.Coordinate exposing (Coordinate)
import Domain.Position as Position exposing (Position)
import Item exposing (Item)
import Main.Safari.Model exposing (PageModel)
import Main.Safari.Overworld.Types.ActivePokeballs as ActivePokeballs
import Main.Safari.Overworld.Types.ItemDrag as ItemDrag
import Main.Safari.Overworld.View.Bag exposing (deadzoneRadius)
import Shared.Types.Items as Items exposing (CollectedItems)


type alias Model m u =
    { m
        | safariModel : PageModel
        , auth : AuthManager
        , userData : { u | position : Maybe Position }
        , collectedItems : CollectedItems
    }


releaseItem : Item -> Model m u -> ( Model m u, Cmd msg )
releaseItem item ({ safariModel } as model) =
    case ItemDrag.dragToThrow deadzoneRadius safariModel.itemDrag of
        Nothing ->
            ( { model | safariModel = { safariModel | itemDrag = ItemDrag.empty, activeItem = Nothing } }, Cmd.none )

        Just throw ->
            let
                mapId =
                    Auth.getMapId model.auth

                newModel =
                    addPokeball item throw model
            in
            ( newModel, Api.WebsocketApi.throwBall mapId item throw )


addPokeball :
    Item
    -> ( Float, Coordinate Float )
    -> Model m u
    -> Model m u
addPokeball item throw ({ safariModel, userData } as model) =
    case userData.position of
        Just pos ->
            let
                activePokeballs =
                    ActivePokeballs.addBall
                        item
                        throw
                        (Position.getExact pos)
                        safariModel.activePokeballs
            in
            { model
                | safariModel =
                    { safariModel
                        | activePokeballs = activePokeballs
                        , itemDrag = ItemDrag.empty
                        , activeItem = Nothing
                    }
                , collectedItems = Items.useCollectedItem item model.collectedItems
            }

        _ ->
            model
