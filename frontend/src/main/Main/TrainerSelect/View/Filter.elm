module Main.TrainerSelect.View.Filter exposing (viewFilter, viewPlaceholder)

import Html.WithContext exposing (li)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events exposing (onClick)
import Main.Html exposing (Html)
import Main.TrainerSelect.Types.Tag exposing (Tag)


viewFilter : { onClick : Int -> msg } -> Tag -> Html msg
viewFilter events { tagId, tagName, active } =
    li
        [ fixedStyling
        , color active
        , onClick (events.onClick tagId)
        ]
        [ Html.WithContext.text tagName ]


viewPlaceholder : Html msg
viewPlaceholder =
    li [ fixedStyling, color False, class "text-transparent" ] [ Html.WithContext.text "........" ]


fixedStyling : Html.WithContext.Attribute context msg
fixedStyling =
    class "rounded-lg px-4 py-2"


color : Bool -> Html.WithContext.Attribute context msg
color active =
    if active then
        class "bg-gray-300 dark:bg-gray-800"

    else
        class "bg-gray-200 dark:bg-gray-700"
