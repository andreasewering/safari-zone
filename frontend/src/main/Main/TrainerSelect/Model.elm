module Main.TrainerSelect.Model exposing (..)

import Domain.Direction
import Main.TrainerSelect.Types.Tag exposing (Tag)
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)


type alias PageModel =
    { filters : List Tag
    , trainerState : TrainerState
    }


init : PageModel
init =
    { filters = []
    , trainerState = initialTrainerState
    }


initialTrainerState : TrainerState
initialTrainerState =
    let
        s =
            TrainerState.init
    in
    { s | direction = Domain.Direction.D }
