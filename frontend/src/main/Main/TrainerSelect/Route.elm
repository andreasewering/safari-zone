module Main.TrainerSelect.Route exposing (..)

import TrainerNumber exposing (TrainerNumber)
import UrlCodec exposing (UrlCodec, buildUnion, top, union, variant0, variant1)


type Route
    = Overview
    | Detail TrainerNumber


codec : UrlCodec (Route -> a) a Route
codec =
    union
        (\overview detail value ->
            case value of
                Overview ->
                    overview

                Detail trainerNumber ->
                    detail trainerNumber
        )
        |> variant0 Overview top
        |> variant1 Detail TrainerNumber.urlCodec
        |> buildUnion
