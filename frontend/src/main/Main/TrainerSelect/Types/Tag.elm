module Main.TrainerSelect.Types.Tag exposing (Tag, toggleTagById)


type alias Tag =
    { tagId : Int
    , tagName : String
    , active : Bool
    }


toggleTagById : Int -> List Tag -> List Tag
toggleTagById tagId =
    List.map
        (\tag ->
            if tag.tagId == tagId then
                toggleTag tag

            else
                tag
        )


toggleTag : Tag -> Tag
toggleTag tag =
    { tag | active = not tag.active }
