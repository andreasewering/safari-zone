module Main.TrainerSelect.Detail exposing
    ( Context
    , Msg
    , PageModel
    , init
    , onPageLeave
    , onPageLoad
    , playground
    , subscriptions
    , update
    , view
    )

import Atoms.Button
import Atoms.Html
import Atoms.Loading exposing (loadingIndicator)
import BoundedDict
import Browser.Events
import Config
import Domain.ConstrainedPosition
import Domain.Direction as Direction
import Domain.Position as Position exposing (Position)
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class, style)
import Main.ErrorHandling as ErrorHandling
import Main.Html exposing (Html)
import Main.Language as Language
import Main.Nav as Nav
import Main.RequestManager as RequestManager
import Main.Safari.Overworld.Gamepad as Gamepad exposing (Gamepad)
import Main.Safari.Overworld.View.GameCanvas
import Main.Safari.Overworld.View.SpriteEntity
import Main.Shared as Shared
import Main.TrainerSelect.Model as TrainerSelect
import Main.TrainerSelect.Overview
import Main.Translations as Translations
import MapId exposing (MapId)
import Maybe.Extra
import Protobuf.Types.Int64 as Int64
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import Shared.View.WebGLUtil as WebGLUtil
import Tagged exposing (tag, untag)
import TrainerNumber exposing (TrainerNumber)
import TrainerNumberDict
import WebGL.Map


type alias PageModel =
    { selectedTrainerState : TrainerState
    , gamepad : Gamepad
    , position : Position
    }


type Msg
    = SelectedTrainer
    | Frame Float
    | GamepadMsg Gamepad.Msg


type alias Model m =
    Shared.Session
        { m
            | trainerDetailModel : PageModel
            , trainerSelectModel : TrainerSelect.PageModel
        }


type alias Context msg =
    { trainerNumber : TrainerNumber
    , lift : Msg -> msg
    , liftShared : Shared.Msg -> msg
    }


init : PageModel
init =
    { selectedTrainerState = TrainerState.init
    , gamepad = Gamepad.init
    , position = Position.init { x = 0, y = 0 } 0 Direction.default
    }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register (requests ctx.trainerNumber) model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
        |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
    )


requests : TrainerNumber -> List RequestManager.ApiCall
requests trainerNumber =
    [ RequestManager.GetTrainers {}
    , RequestManager.GetTiles { mapId = untag testMapId }
    , RequestManager.GetTextureMetadata { mapId = untag testMapId }
    , RequestManager.GetTilesetConfig {}
    , RequestManager.LoadTrainerTexture trainerNumber
    ]


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.unregister (requests ctx.trainerNumber) model.requestManager
    in
    ( { model | requestManager = requestManager }, requestManagerCmds )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ trainerDetailModel } as model) =
    case msg of
        SelectedTrainer ->
            onSelectTrainer ctx model

        Frame dt ->
            ( onFrame dt model, Cmd.none )

        GamepadMsg gamepadMsg ->
            ( { model | trainerDetailModel = { trainerDetailModel | gamepad = Gamepad.update gamepadMsg trainerDetailModel.gamepad } }
            , Cmd.none
            )


onFrame : Float -> Model m -> Model m
onFrame dt ({ trainerDetailModel, trainerSelectModel } as model) =
    let
        gamepadDir =
            Gamepad.getDirection trainerDetailModel.gamepad

        newPos =
            case BoundedDict.get testMapId model.moveRules of
                Just moveRules ->
                    Domain.ConstrainedPosition.move gamepadDir (Config.trainerSpeed * dt) moveRules trainerDetailModel.position

                Nothing ->
                    trainerDetailModel.position

        moveMaybe =
            if Position.hasMoved trainerDetailModel.position newPos then
                TrainerState.move dt

            else
                TrainerState.stop

        setGamepadDirection =
            TrainerState.changeDirection <| Position.getCurrentDirection newPos

        selectedTrainerState =
            moveMaybe trainerDetailModel.selectedTrainerState
                |> setGamepadDirection
    in
    { model
        | trainerDetailModel =
            { trainerDetailModel
                | selectedTrainerState = selectedTrainerState
                , position = newPos
            }
        , trainerSelectModel =
            if model.reducedMotion then
                trainerSelectModel

            else
                Main.TrainerSelect.Overview.onFrame dt trainerSelectModel
    }


subscriptions : Context msg -> Model m -> Sub msg
subscriptions ctx { trainerDetailModel, reducedMotion } =
    let
        shouldStartMoving =
            Gamepad.getDirection trainerDetailModel.gamepad |> Maybe.Extra.isJust

        isCurrentlyMoving =
            trainerDetailModel.selectedTrainerState.moving
    in
    Sub.batch
        [ -- Optimization for the case where the player is not currently moving.
          if not reducedMotion || shouldStartMoving || isCurrentlyMoving then
            Browser.Events.onAnimationFrameDelta (Frame >> ctx.lift)

          else
            Sub.none
        , Gamepad.subscriptions trainerDetailModel.gamepad
            |> Sub.map (GamepadMsg >> ctx.lift)
        ]


view : Context msg -> { height : Int, width : Int } -> Model m -> List (Html msg)
view ctx dimensions model =
    let
        chooseTrainerStatus =
            RequestManager.status
                (RequestManager.SetTrainerSprite { trainerNumber = untag ctx.trainerNumber })
                model.requestManager

        changeTrainerErrorText =
            case chooseTrainerStatus of
                RequestManager.Failure error ->
                    case ErrorHandling.handleWebError model { languageToString = Language.toString } error of
                        ErrorHandling.Blocking overlay ->
                            ErrorHandling.viewErrorSmall overlay

                        _ ->
                            Html.WithContext.text ""

                _ ->
                    Html.WithContext.text ""
    in
    [ playground ctx.trainerNumber { height = 150, width = dimensions.width } model
    , div [ style "width" (String.fromInt dimensions.width ++ "px") ]
        [ Atoms.Button.button
            { onPress = Just (SelectedTrainer |> ctx.lift)
            , label = Atoms.Html.text Translations.trainerSelectDetailSubmit
            , loading =
                RequestManager.status
                    (RequestManager.SetTrainerSprite { trainerNumber = untag ctx.trainerNumber })
                    model.requestManager
                    == RequestManager.Loading
            }
            [ class "float-right" ]
        ]
    , changeTrainerErrorText
    , if model.viewport.width <= Nav.mobileNavWidthLimit then
        Gamepad.view model.trainerDetailModel.gamepad { onEvent = GamepadMsg >> ctx.lift }

      else
        Atoms.Html.none
    ]


playground : TrainerNumber -> { height : Int, width : Int } -> Model m -> Html msg
playground trainerNumber dimensions { trainerDetailModel, textures } =
    let
        canvasSession =
            { height = dimensions.height
            , width = dimensions.width
            , position = trainerDetailModel.position
            }

        trainerSpriteProps pos =
            { spriteId = TrainerState.stateToIndex trainerDetailModel.selectedTrainerState
            , opacity = 1
            , position = Position.getExact pos
            , size = Config.trainerSize
            , rotation = 0
            }

        entities =
            Maybe.map2
                (\tileset mapTextures ->
                    let
                        camera =
                            Main.Safari.Overworld.View.GameCanvas.constructCamera canvasSession mapTextures

                        trainerTexture =
                            TrainerNumberDict.get trainerNumber textures.trainerTextures

                        trainerProps =
                            trainerSpriteProps trainerDetailModel.position

                        trainerEntity =
                            Maybe.map
                                (\texture ->
                                    Main.Safari.Overworld.View.SpriteEntity.entity camera texture trainerProps
                                )
                                trainerTexture
                    in
                    List.indexedMap
                        (\layerIndex _ ->
                            WebGL.Map.entity camera
                                1
                                mapTextures.offset
                                tileset.texture
                                mapTextures.texture
                                (List.length mapTextures.layers)
                                layerIndex
                        )
                        mapTextures.layers
                        ++ Maybe.Extra.toList trainerEntity
                )
                textures.tileset
                (BoundedDict.get testMapId textures.tilemaps)
    in
    div [ class "relative flex items-center justify-center bg-gray-300 bgd:bg-gray-700" ]
        [ if Maybe.Extra.isNothing entities then
            div [ class "absolute h-full w-20" ] [ loadingIndicator ]

          else
            Atoms.Html.none
        , entities
            |> Maybe.withDefault []
            |> WebGLUtil.entitiesToElement canvasSession []
            |> Html.WithContext.html
        ]


onSelectTrainer : Context msg -> Model m -> ( Model m, Cmd msg )
onSelectTrainer ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                [ RequestManager.SetTrainerSprite { trainerNumber = untag ctx.trainerNumber } ]
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
    )


{-| This map id is reserved in the backend as a test map
-}
testMapId : MapId
testMapId =
    tag <| Int64.fromInts -2147483648 0
