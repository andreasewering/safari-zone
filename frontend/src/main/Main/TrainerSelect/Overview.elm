module Main.TrainerSelect.Overview exposing
    ( Context
    , Msg
    , onFrame
    , onPageLeave
    , onPageLoad
    , subscriptions
    , update
    , updateShared
    , viewFilters
    , viewSprites
    )

import Atoms.Html
import Browser.Events
import Html.WithContext exposing (div, li, ul)
import Html.WithContext.Attributes exposing (class, href, style)
import Html.WithContext.Keyed as Keyed
import Html.WithContext.Lazy as Lazy
import Main.ErrorHandling as ErrorHandling
import Main.Html exposing (Attribute, Html)
import Main.Language as Language
import Main.RequestManager as RequestManager
import Main.Route as Route
import Main.Shared as Shared
import Main.TrainerSelect.Model exposing (PageModel)
import Main.TrainerSelect.Route as TrainerSelect
import Main.TrainerSelect.Types.Tag as Tag exposing (Tag)
import Main.TrainerSelect.View.Filter as Filter
import Proto.Kangaskhan exposing (GetTrainersResponse, Trainer)
import Shared.Types.TrainerState as TrainerState
import Shared.View.Trainer as Trainer
import Tagged exposing (tag)
import TrainerNumber exposing (TrainerNumber)
import TrainerNumberDict exposing (TrainerNumberDict)


type Msg
    = ToggleFilter Int
    | Frame Float


type alias Context msg =
    { lift : Msg -> msg, liftShared : Shared.Msg -> msg }


type alias Model m =
    Shared.Session
        { m
            | trainerSelectModel : PageModel
        }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
    )


requests : List RequestManager.ApiCall
requests =
    [ RequestManager.GetTrainers {}, RequestManager.GetTilesetConfig {} ]


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave _ model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.unregister
                requests
                model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds
    )


updateShared : Context msg -> Shared.Msg -> Model m -> ( Model m, Cmd msg )
updateShared _ msg ({ trainerSelectModel } as model) =
    case msg of
        Shared.GotTrainerMetadata trainers ->
            ( { model
                | trainerSelectModel =
                    { trainerSelectModel | filters = trainerResponseToTags trainers }
              }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )


update : Msg -> Model m -> ( Model m, Cmd msg )
update msg model =
    case msg of
        ToggleFilter tagId ->
            ( onToggleFilter tagId model, Cmd.none )

        Frame dt ->
            ( { model | trainerSelectModel = onFrame dt model.trainerSelectModel }, Cmd.none )


trainerResponseToTags : GetTrainersResponse -> List Tag
trainerResponseToTags { tags } =
    tags
        |> List.map (\tag -> { active = False, tagName = tag.displayName, tagId = tag.tagId })


onFrame : Float -> PageModel -> PageModel
onFrame dt trainerSelectModel =
    { trainerSelectModel
        | trainerState = TrainerState.move dt trainerSelectModel.trainerState
    }


onToggleFilter : Int -> Model m -> Model m
onToggleFilter tagId ({ trainerSelectModel } as model) =
    { model | trainerSelectModel = { trainerSelectModel | filters = Tag.toggleTagById tagId trainerSelectModel.filters } }


subscriptions : Context msg -> Model m -> Sub msg
subscriptions ctx _ =
    Browser.Events.onAnimationFrameDelta (Frame >> ctx.lift)


filteredSpriteMetadata :
    { m
        | trainerSpriteMetadata : TrainerNumberDict Trainer
        , filters : List Tag
    }
    -> List Trainer
filteredSpriteMetadata { filters, trainerSpriteMetadata } =
    let
        activeFilters =
            filters
                |> List.filter .active
                |> List.map .tagId

        satisfiesActiveFilters tags =
            List.all (\tag -> List.member tag tags) activeFilters
    in
    TrainerNumberDict.values trainerSpriteMetadata
        |> List.filter (.tagIds >> satisfiesActiveFilters)


viewFilters : Context msg -> Model m -> Html msg
viewFilters ctx ({ trainerSelectModel } as model) =
    let
        filterEvents =
            { onClick = ToggleFilter >> ctx.lift }
    in
    div [ class "flex w-full justify-center" ]
        [ ul [ class "trainer-select-width flex gap-2" ] <|
            case RequestManager.status (RequestManager.GetTrainers {}) model.requestManager of
                RequestManager.Success ->
                    trainerSelectModel.filters
                        |> List.map (Filter.viewFilter filterEvents)

                RequestManager.Loading ->
                    List.repeat 2 Filter.viewPlaceholder

                RequestManager.NotAsked ->
                    List.repeat 2 Filter.viewPlaceholder

                RequestManager.Failure _ ->
                    -- Error should already be handled by viewSprites
                    []
        ]


spriteDisplaySize : number
spriteDisplaySize =
    5


viewSprites : Maybe TrainerNumber -> Model m -> Html msg
viewSprites selectedTrainerNumber ({ trainerSelectModel } as model) =
    let
        viewSprite : Trainer -> ( String, Html msg )
        viewSprite trainer =
            let
                ( trainerState, trainerKind ) =
                    if model.userData.trainerSprite == Just (tag trainer.trainerNumber) then
                        ( trainerSelectModel.trainerState, Current )

                    else if selectedTrainerNumber == Just (tag trainer.trainerNumber) then
                        ( trainerSelectModel.trainerState, Selected )

                    else
                        ( Main.TrainerSelect.Model.initialTrainerState, Unselected )
            in
            ( String.fromInt trainer.trainerNumber
            , viewTrainerSprite trainer trainerKind trainerState
            )

        viewPlaceholder =
            li
                [ trainerWrapperStyling
                , unselectedColors
                ]
                [ div
                    [ style "width" <| String.fromInt spriteDisplaySize ++ "rem"
                    , style "height" <| String.fromInt spriteDisplaySize ++ "rem"
                    ]
                    []
                ]

        listAttrs =
            [ class "trainer-select-width flex flex-wrap gap-2" ]
    in
    case RequestManager.status (RequestManager.GetTrainers {}) model.requestManager of
        RequestManager.Loading ->
            ul listAttrs (List.repeat 14 viewPlaceholder)

        RequestManager.NotAsked ->
            ul listAttrs (List.repeat 14 viewPlaceholder)

        RequestManager.Success ->
            Keyed.ul listAttrs
                (filteredSpriteMetadata
                    { trainerSpriteMetadata = model.trainerMetadata
                    , filters = trainerSelectModel.filters
                    }
                    |> List.map viewSprite
                )

        RequestManager.Failure error ->
            case ErrorHandling.handleWebError model { languageToString = Language.toString } error of
                ErrorHandling.Blocking overlay ->
                    ul listAttrs [ ErrorHandling.viewError overlay ]

                ErrorHandling.AutomaticallyFixable _ _ ->
                    ul listAttrs []


type TrainerKind
    = Unselected
    | Selected
    | Current


viewTrainerSprite : Trainer -> TrainerKind -> TrainerState.TrainerState -> Html msg
viewTrainerSprite =
    Lazy.lazy3 <|
        \trainer trainerKind trainerState ->
            let
                colors =
                    case trainerKind of
                        Current ->
                            class "bg-gray-300 outline-green-500 dark:bg-gray-800"

                        Selected ->
                            class "bg-gray-300 outline-amber-300 dark:bg-gray-800"

                        Unselected ->
                            unselectedColors

                link =
                    Route.TrainerSelect <| TrainerSelect.Detail <| tag trainer.trainerNumber
            in
            li []
                [ Atoms.Html.a
                    [ href (Route.fromRoute link), trainerWrapperStyling, colors ]
                    [ Trainer.trainerSpriteHtml { displaySize = spriteDisplaySize }
                        trainerState
                        { trainerNumber = tag trainer.trainerNumber
                        , displayName = trainer.displayName
                        }
                    ]
                ]


unselectedColors : Attribute msg
unselectedColors =
    class "bg-gray-200 outline-transparent dark:bg-gray-700"


trainerWrapperStyling : Attribute msg
trainerWrapperStyling =
    class "flex rounded-full p-2 outline outline-4 -outline-offset-4"
