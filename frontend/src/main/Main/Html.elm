module Main.Html exposing (Attribute, Context, Document, Html, backIcon, buttonLink, mainDomId)

import Atoms.Html
import Html.WithContext
import Html.WithContext.Attributes exposing (class, href)
import Main.Route exposing (KnownRoute)
import Main.Translations as Translations exposing (I18n)
import Shared.Background exposing (BackgroundImage)
import Shared.View.Icon exposing (iconHtmlWith)
import Time


mainDomId : String
mainDomId =
    "main"


type alias Document msg =
    { title : I18n -> String
    , body : List (Html msg)
    }


type alias Context =
    { i18n : I18n
    , background : BackgroundImage
    , timezone : Time.Zone
    , disabled : Bool
    , fontSize : Int
    }


type alias Html msg =
    Html.WithContext.Html Context msg


type alias Attribute msg =
    Html.WithContext.Attribute Context msg


buttonLink : { route : KnownRoute, label : Html msg } -> Html msg
buttonLink { route, label } =
    Atoms.Html.a [ href (Main.Route.fromRoute route), class "w-fit rounded-md bg-red-600 px-4 py-2 text-white" ]
        [ label ]


backIcon : Html.WithContext.Html { ctx | i18n : I18n, fontSize : Int } msg
backIcon =
    iconHtmlWith { name = "back-arrow", width = 2.5, height = 2.5, description = Translations.backButtonDescription }
        [ class "dark:invert" ]
