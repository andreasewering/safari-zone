module Main.Dex.Route exposing (Route(..), codec)

import PokemonNumber exposing (PokemonNumber)
import UrlCodec exposing (UrlCodec, buildUnion, s, top, union, variant0, variant1)


type Route
    = Overview
    | Appraisal
    | Detail PokemonNumber


codec : UrlCodec (Route -> a) a Route
codec =
    union
        (\overview appraisal detail value ->
            case value of
                Overview ->
                    overview

                Appraisal ->
                    appraisal

                Detail pokemonNumber ->
                    detail pokemonNumber
        )
        |> variant0 Overview top
        |> variant0 Appraisal (s "appraisal")
        |> variant1 Detail PokemonNumber.urlCodec
        |> buildUnion
