module Main.Dex.Appraisal exposing (Context, Msg, onPageLoad, professorImage, update, view)

import Animator
import Animator.Inline
import Array exposing (Array)
import Atoms.Html
import Atoms.Typewriter
import Html
import Html.WithContext exposing (div, h1, htmlAttribute, img)
import Html.WithContext.Attributes exposing (attribute, class, src)
import Html.WithContext.Events as Events
import Interop.Event as Event
import Json.Decode
import Main.Dex.Model as Dex
import Main.Dex.Route as Dex exposing (Route)
import Main.Dex.View.Pokedex as Pokedex
import Main.Html exposing (Attribute, Document, Html)
import Main.RequestManager as RequestManager
import Main.Route as Route
import Main.Shared as Shared
import Main.Translations as Translations exposing (I18n)
import OutsideOf exposing (isOutsideOf)
import Proto.Kangaskhan exposing (DexEntry)


type alias Model m =
    Shared.Session
        { m
            | dexModel : Dex.PageModel
        }


type Msg
    = CloseAppraisal
    | TypewriterAnimationFinished


type alias Context msg =
    { lift : Msg -> msg
    , liftShared : Shared.Msg -> msg
    }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx ({ dexModel } as model) =
    let
        updatedTransitionTimeline =
            case Animator.current dexModel.transitionTimeline of
                ( Dex.Overview, _ ) ->
                    Animator.queue
                        [ Animator.event Animator.quickly ( Dex.Appraisal, Dex.OnPage )
                        ]
                        dexModel.transitionTimeline

                _ ->
                    Animator.go Animator.immediately
                        ( Dex.Appraisal, Dex.OnPage )
                        dexModel.transitionTimeline

        ( requestManager, requestManagerCmds ) =
            RequestManager.register [ RequestManager.GetDexEntries {} ] model.requestManager
    in
    ( { model
        | requestManager = requestManager
        , dexModel = { dexModel | transitionTimeline = updatedTransitionTimeline }
      }
    , requestManagerCmds
        |> Cmd.map
            (Shared.RequestManagerMessage >> ctx.liftShared)
    )


view : Context msg -> Model m -> Document msg
view ctx ({ dexModel } as model) =
    { title = Translations.dexAppraisalTitle
    , body =
        [ div
            [ class "absolute left-0 top-0 z-10 flex h-full w-full items-center justify-center p-2"
            , htmlAttribute <| transformAppraisal dexModel.transitionTimeline model.viewport
            , Events.on "click"
                (isOutsideOf "role" "dialog"
                    |> Json.Decode.map (\_ -> ctx.lift CloseAppraisal)
                )
            ]
            [ dexCompletionInfoModal ctx model ]
        , Atoms.Html.disable True <|
            Pokedex.view
                []
                (RequestManager.status (RequestManager.GetDexEntries {}) model.requestManager)
                { height = model.viewport.height
                , width = model.viewport.width
                , dexEntries = model.dexEntries
                , errorCtx = model
                }
        ]
    }


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update _ msg ({ dexModel } as model) =
    case msg of
        CloseAppraisal ->
            let
                updateTimeline =
                    Animator.queue
                        [ Animator.event Animator.immediately ( Dex.Overview, Dex.OnPage )
                        ]
            in
            ( { model | dexModel = { dexModel | transitionTimeline = updateTimeline dexModel.transitionTimeline } }
            , Route.Dex Dex.Overview |> Route.navigate model
            )

        TypewriterAnimationFinished ->
            ( model, Event.typewriterFinished "appraisal" )


dexCompletionInfoModal : Context msg -> Model m -> Html msg
dexCompletionInfoModal ctx model =
    let
        isPortrait =
            model.viewport.height > model.viewport.width

        sharedStyles =
            class "relative flex w-full rounded-lg bg-gray-200 p-4 dark:bg-gray-700 dark:text-white"

        headline =
            h1
                [ class "border-y border-black p-2 text-center text-xl font-bold dark:border-white" ]
                [ Atoms.Html.text Translations.dexAppraisalHeadline ]

        dexStatus =
            RequestManager.status (RequestManager.GetDexEntries {}) model.requestManager
    in
    if isPortrait then
        div
            [ sharedStyles
            , class "max-h-full max-w-md flex-col items-center gap-2 overflow-auto"
            , attribute "role" "dialog"
            ]
            [ Atoms.Html.button
                [ class "self-start opacity-70"
                , Events.onClick (CloseAppraisal |> ctx.lift)
                ]
                [ Main.Html.backIcon ]
            , headline
            , Atoms.Typewriter.typewriter { text = dexCompletionText dexStatus model.dexEntries, speed = 10, lines = 4, onFinish = ctx.lift TypewriterAnimationFinished } [ class "speech-bubble-b after:border-t-gray-300 after:dark:border-t-gray-800" ]
            , professorImage []
            ]

    else
        div
            [ sharedStyles
            , class "max-w-3xl justify-center"
            , attribute "role" "dialog"
            ]
            [ professorImage [ class "h-fit w-1/3" ]
            , Atoms.Html.button
                [ class "absolute left-0 top-0 p-2 opacity-70"
                , Events.onClick (CloseAppraisal |> ctx.lift)
                ]
                [ Main.Html.backIcon ]
            , div [ class "flex w-full flex-col gap-2" ]
                [ headline
                , Atoms.Typewriter.typewriter { text = dexCompletionText dexStatus model.dexEntries, speed = 10, lines = 4, onFinish = ctx.lift TypewriterAnimationFinished } [ class "speech-bubble-l after:border-r-gray-300 after:dark:border-r-gray-800" ]
                ]
            ]


professorImage : List (Attribute msg) -> Html msg
professorImage attrs =
    img
        ([ Atoms.Html.alt Translations.dexAppraisalProfessorImageDescription, src "/images/professorOak.png" ] ++ attrs)
        []


transformAppraisal : Animator.Timeline ( Route, Dex.TransitionState ) -> { s | width : Int } -> Html.Attribute msg
transformAppraisal timeline _ =
    Animator.Inline.xy timeline
        (\state ->
            case state of
                -- TODO ( Appraisal, Model.Slide Model.Right ) ->
                --     { x = Animator.at <| toFloat -width
                --     , y = Animator.at 0
                --     }
                _ ->
                    { x = Animator.at 0, y = Animator.at 0 }
        )


dexCompletionText : RequestManager.Status e -> Array DexEntry -> I18n -> String
dexCompletionText status dexEntries =
    case status of
        RequestManager.Loading ->
            Translations.dexAppraisalLoading

        RequestManager.NotAsked ->
            Translations.dexAppraisalLoading

        RequestManager.Failure _ ->
            Translations.dexAppraisalError

        RequestManager.Success ->
            let
                number =
                    Array.length dexEntries

                i18nKey =
                    if number < 10 then
                        Translations.dexAppraisal0

                    else if number < 30 then
                        Translations.dexAppraisal1

                    else if number < 60 then
                        Translations.dexAppraisal2

                    else if number < 100 then
                        Translations.dexAppraisal3

                    else if number < 151 then
                        Translations.dexAppraisal4

                    else
                        Translations.dexAppraisal5
            in
            i18nKey
