module Main.Dex.Types.Detail exposing (..)

import PokemonNumber exposing (PokemonNumber)
import Proto.Kangaskhan
import Proto.Kangaskhan.Type exposing (Type)


type alias DexDetail =
    { name : String
    , number : PokemonNumber
    , primaryType : Type
    , secondaryType : Maybe Type
    , height : Float
    , weight : Float
    , description : String
    , amountNormal : Int
    , amountShiny : Int
    }


fromResponse : PokemonNumber -> Proto.Kangaskhan.GetDexEntryDetailResponse -> DexDetail
fromResponse pokemonNumber response =
    { name = response.name
    , number = pokemonNumber
    , primaryType = response.primaryType
    , secondaryType = response.secondaryType
    , height = response.height
    , weight = response.weight
    , description = response.description
    , amountNormal = response.amountNormal
    , amountShiny = response.amountShiny
    }
