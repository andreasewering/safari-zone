module Main.Dex.Detail exposing
    ( Context
    , Msg
    , onAnimationTick
    , onPageLeave
    , onPageLoad
    , subscriptions
    , update
    , updateShared
    , view
    )

import Animator
import Array exposing (Array)
import Atoms.Button
import Atoms.Html
import Atoms.Icon exposing (iconWith)
import Atoms.Loading
import BoundedDict
import Browser.Dom as Dom
import Browser.Events
import Html
import Html.Attributes
import Html.Events.Extra.Touch as Touch
import Html.WithContext exposing (div, h1, htmlAttribute, p, span, withContextAttribute)
import Html.WithContext.Attributes as Attributes exposing (attribute, class, href)
import Html.WithContext.Events as Events
import Html.WithContext.Keyed as Keyed
import Json.Decode
import List.Extra
import Main.Dex.Model as Dex exposing (SlideDirection)
import Main.Dex.Route as Dex exposing (Route)
import Main.Dex.Types.Detail exposing (DexDetail)
import Main.Dex.View.DexDetail as DexDetail
import Main.Dex.View.Pokedex as Pokedex
import Main.Dex.View.TypeColors as TypeColors
import Main.ErrorHandling as ErrorHandling
import Main.Html exposing (Attribute, Document, Html, mainDomId)
import Main.Language as Language
import Main.Nav as Nav
import Main.RequestManager as RequestManager exposing (RequestManager)
import Main.Route as Route
import Main.Shared as Shared
import Main.Translations as Translations
import OutsideOf exposing (isOutsideOf)
import PokemonNumber exposing (PokemonNumber)
import Proto.Kangaskhan exposing (DexEntry)
import Shared.Types.Error as Error
import Shared.View.PokemonImage as PokemonImage
import Tagged exposing (tag, untag)
import Task
import Time


type alias Model m =
    Shared.Session
        { m
            | dexModel : Dex.PageModel
        }


type Msg
    = GotDexEntryElement (Result Dom.Error Dom.Element)
    | GotDexElement SlideDirection (Result Dom.Error Dom.Element)
    | TouchDownOnDetail ( Float, Float )
    | TouchUpOnDetail ( Float, Float )
    | TouchMoveOnDetail ( Float, Float )
    | PressedKey KeyboardEvent
    | CloseDetail
    | ReadyToCloseDetail (Result Dom.Error { pokemonEl : Dom.Element, pokedexEl : Dom.Viewport })
    | ChoosePartnerPokemon


type alias Context msg =
    { pokemonNumber : PokemonNumber
    , lift : Msg -> msg
    , liftShared : Shared.Msg -> msg
    }


type KeyboardEvent
    = ArrowLeft
    | ArrowRight
    | Escape


requests : Context msg -> Model m -> List RequestManager.ApiCall
requests ctx model =
    let
        adjacentIds =
            adjacentDexEntryIds ctx.pokemonNumber model.dexEntries
    in
    RequestManager.GetDexEntries {}
        :: (adjacentIds
                |> adjacentToList
                |> List.filter (\( _, nonDuplicateIfZero ) -> nonDuplicateIfZero == 0)
                |> List.map (\( number, _ ) -> RequestManager.GetDexEntryDetail { number = untag number })
           )


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx ({ dexModel } as model) =
    let
        adjacentIds =
            adjacentDexEntryIds ctx.pokemonNumber model.dexEntries

        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                (requests ctx model)
                model.requestManager

        newModel =
            { model
                | dexModel =
                    { dexModel
                        | transitionTimeline = updateAnimationTimeline dexModel.transitionTimeline
                    }
                , requestManager = requestManager
            }

        ( initAnimation, updateAnimationTimeline ) =
            case model.previousRoute of
                Just (Route.Dex Dex.Overview) ->
                    ( Dom.getElement (Pokedex.getPokemonImageDomId ctx.pokemonNumber)
                        |> Task.attempt (GotDexEntryElement >> ctx.lift)
                    , identity
                    )

                Just (Route.Dex (Dex.Detail prevNumber)) ->
                    let
                        direction =
                            if prevNumber == Tuple.first adjacentIds.right then
                                Dex.Right

                            else
                                Dex.Left
                    in
                    ( Dom.getElement mainDomId
                        |> Task.attempt (GotDexElement direction >> ctx.lift)
                    , identity
                    )

                _ ->
                    ( Cmd.none, Animator.go Animator.immediately ( Dex.Detail ctx.pokemonNumber, Dex.OnPage ) )
    in
    ( newModel
    , Cmd.batch
        [ Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared) requestManagerCmds
        , initAnimation
        , Dom.setViewportOf mainDetailScrollId 0 0 |> Task.attempt (\_ -> ctx.liftShared Shared.NoOp)
        ]
    )


onPageLeave : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLeave ctx model =
    let
        ( requestManager, cmds ) =
            RequestManager.unregister
                (RequestManager.SetPartnerPokemon { pokemonNumber = untag ctx.pokemonNumber }
                    :: requests ctx model
                )
                model.requestManager
    in
    ( { model | requestManager = requestManager }, cmds )


onAnimationTick : Context msg -> Time.Posix -> Model m -> ( Model m, Cmd msg )
onAnimationTick ctx _ ({ dexModel } as model) =
    if
        Animator.arrived dexModel.transitionTimeline
            == ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
            && Animator.current dexModel.transitionTimeline
            == ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
    then
        ( { model | dexModel = { dexModel | detailSwipeMove = Nothing } }, Cmd.none )

    else
        ( model, Cmd.none )


updateShared : Context msg -> Shared.Msg -> Model m -> ( Model m, Cmd msg )
updateShared ctx msg model =
    case msg of
        Shared.GotDexEntries _ ->
            let
                ( requestManager, cmds ) =
                    RequestManager.register (requests ctx model) model.requestManager
            in
            ( { model | requestManager = requestManager }, cmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared) )

        Shared.RequestFailed (RequestManager.SetPartnerPokemon _) _ ->
            ( model
            , Dom.getViewportOf mainDetailScrollId
                |> Task.andThen
                    (\{ scene } -> Dom.setViewportOf mainDetailScrollId 0 scene.height)
                |> Task.attempt (\_ -> ctx.liftShared Shared.NoOp)
            )

        _ ->
            ( model, Cmd.none )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ dexModel } as model) =
    case msg of
        GotDexEntryElement (Ok { element, viewport }) ->
            let
                zoomElement =
                    { left = round element.x
                    , top = round <| element.y - viewport.y
                    , height = round element.height
                    , width = round element.width
                    }
            in
            ( { model
                | dexModel =
                    { dexModel
                        | dexScrollY = round viewport.y
                        , transitionTimeline =
                            Animator.go Animator.immediately
                                ( Dex.Detail ctx.pokemonNumber, Dex.Zoom zoomElement )
                                dexModel.transitionTimeline
                                |> Animator.go Animator.slowly
                                    ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
                    }
              }
            , Cmd.none
            )

        GotDexEntryElement (Err _) ->
            -- TODO add error handling
            ( model, Cmd.none )

        GotDexElement direction (Ok element) ->
            ( { model
                | dexModel =
                    { dexModel
                        | transitionTimeline =
                            Animator.interrupt
                                [ Animator.event Animator.quickly
                                    ( Dex.Detail ctx.pokemonNumber
                                    , Dex.Slide direction
                                        { width = floor element.element.width }
                                    )
                                , Animator.event Animator.immediately
                                    ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
                                ]
                                dexModel.transitionTimeline
                    }
              }
            , Cmd.none
            )

        GotDexElement _ (Err _) ->
            -- TODO add error handling
            ( model, Cmd.none )

        TouchDownOnDetail start ->
            ( { model | dexModel = { dexModel | detailSwipeStart = Just start } }, Cmd.none )

        TouchMoveOnDetail ( moveX, moveY ) ->
            case dexModel.detailSwipeStart of
                Just ( startX, startY ) ->
                    ( { model | dexModel = { dexModel | detailSwipeMove = Just ( moveX - startX, moveY - startY ) } }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        TouchUpOnDetail ( endX, _ ) ->
            case dexModel.detailSwipeStart of
                Nothing ->
                    ( model, Cmd.none )

                Just ( startX, _ ) ->
                    let
                        ids =
                            adjacentDexEntryIds ctx.pokemonNumber model.dexEntries
                    in
                    if startX - endX > 50 && Tuple.second ids.right == 0 then
                        ( { model | dexModel = { dexModel | detailSwipeStart = Nothing } }, Tuple.first ids.right |> Dex.Detail |> Route.Dex |> Route.navigate model )

                    else if endX - startX > 50 && Tuple.second ids.left == 0 then
                        ( { model | dexModel = { dexModel | detailSwipeStart = Nothing } }, Tuple.first ids.left |> Dex.Detail |> Route.Dex |> Route.navigate model )

                    else
                        ( { model | dexModel = { dexModel | detailSwipeStart = Nothing, detailSwipeMove = Nothing } }, Cmd.none )

        PressedKey ArrowLeft ->
            if
                -- Detect if an animation is already running
                Animator.arrived dexModel.transitionTimeline
                    /= ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
                    || Animator.current dexModel.transitionTimeline
                    /= ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
            then
                ( model, Cmd.none )

            else
                case adjacentDexEntryIds ctx.pokemonNumber model.dexEntries |> .left of
                    ( leftNeighbour, 0 ) ->
                        ( model, Route.navigate model (Route.Dex <| Dex.Detail leftNeighbour) )

                    _ ->
                        ( model, Cmd.none )

        PressedKey ArrowRight ->
            if
                -- Detect if an animation is already running
                Animator.arrived dexModel.transitionTimeline
                    /= ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
                    || Animator.current dexModel.transitionTimeline
                    /= ( Dex.Detail ctx.pokemonNumber, Dex.OnPage )
            then
                ( model, Cmd.none )

            else
                case adjacentDexEntryIds ctx.pokemonNumber model.dexEntries |> .right of
                    ( rightNeighbour, 0 ) ->
                        ( model, Route.navigate model (Route.Dex <| Dex.Detail rightNeighbour) )

                    _ ->
                        ( model, Cmd.none )

        PressedKey Escape ->
            onCloseDetail ctx model

        CloseDetail ->
            onCloseDetail ctx model

        ReadyToCloseDetail (Ok element) ->
            onReadyToCloseDetail ctx element model

        ReadyToCloseDetail (Err _) ->
            -- Should never happen unless we remove the ids from the respective elements
            -- If that happens, we have integration tests to cover us
            ( model, Cmd.none )

        ChoosePartnerPokemon ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.GetPokemon { pokemonNumber = untag ctx.pokemonNumber }
                        , RequestManager.LoadPokemonTexture ctx.pokemonNumber
                        , RequestManager.SetPartnerPokemon { pokemonNumber = untag ctx.pokemonNumber }
                        ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
            )


onCloseDetail : Context msg -> Model m -> ( Model m, Cmd msg )
onCloseDetail ctx model =
    ( model
    , Dom.getElement (Pokedex.getPokemonImageDomId ctx.pokemonNumber)
        |> Task.andThen (\pokemon -> Dom.getViewportOf PokemonImage.pokedexId |> Task.map (\pokedex -> { pokedexEl = pokedex, pokemonEl = pokemon }))
        |> Task.attempt (ReadyToCloseDetail >> ctx.lift)
    )


subscriptions : Context msg -> Sub msg
subscriptions ctx =
    let
        supportedKey =
            PressedKey >> ctx.lift >> Json.Decode.succeed
    in
    Browser.Events.onKeyDown
        (Json.Decode.field "key" Json.Decode.string
            |> Json.Decode.andThen
                (\key ->
                    case key of
                        "ArrowLeft" ->
                            supportedKey ArrowLeft

                        "ArrowRight" ->
                            supportedKey ArrowRight

                        "Escape" ->
                            supportedKey Escape

                        _ ->
                            Json.Decode.fail "Unsupported key"
                )
        )


view : Context msg -> Model m -> Document msg
view ctx ({ dexModel } as model) =
    let
        ( _, arrivedTransitionState ) =
            Animator.arrived dexModel.transitionTimeline

        ( _, currentTransitionState ) =
            Animator.current dexModel.transitionTimeline

        onPage =
            case ( currentTransitionState, arrivedTransitionState ) of
                ( Dex.Zoom _, _ ) ->
                    False

                ( _, Dex.Zoom _ ) ->
                    False

                _ ->
                    True

        ids =
            adjacentDexEntryIds ctx.pokemonNumber model.dexEntries

        details =
            adjacentToList ids
                |> List.map
                    (\(( pokemonNumber, _ ) as key) ->
                        ( key
                        , ( BoundedDict.get pokemonNumber model.dexDetails
                          , RequestManager.status
                                (RequestManager.GetDexEntryDetail { number = untag pokemonNumber })
                                model.requestManager
                          )
                        )
                    )
    in
    { title =
        case
            ( BoundedDict.get ctx.pokemonNumber model.dexDetails
            , RequestManager.status (RequestManager.GetDexEntryDetail { number = untag ctx.pokemonNumber }) model.requestManager
            )
        of
            ( Just detail, RequestManager.Success ) ->
                Translations.dexDetailTitle detail.name

            ( _, RequestManager.Failure err ) ->
                ErrorHandling.title err.error

            _ ->
                Translations.dexDetailLoadingTitle
    , body =
        [ Atoms.Html.disable True <|
            Pokedex.view
                []
                (RequestManager.status (RequestManager.GetDexEntries {}) model.requestManager)
                { height = model.viewport.height
                , width = model.viewport.width
                , dexEntries = model.dexEntries
                , errorCtx = model
                }
        , Keyed.node "div"
            [ class "absolute left-[-100%] top-0 z-10 flex h-screen w-[300%] items-center justify-center"
            , htmlAttribute <| transformDetail dexModel.detailSwipeMove dexModel.transitionTimeline model.viewport
            ]
            (details
                |> List.indexedMap
                    (\index ( ( number, uniqueKey ), detailAndStatus ) ->
                        ( String.fromInt (untag number) ++ "_" ++ String.fromInt uniqueKey
                        , div
                            (class "flex h-full w-full items-center justify-center p-2"
                                -- Set opacity to 0 for left and right details
                                :: (case ( index, arrivedTransitionState, currentTransitionState ) of
                                        ( 1, _, _ ) ->
                                            [ Events.on "click"
                                                (isOutsideOf "role" "dialog"
                                                    |> Json.Decode.map (always CloseDetail >> ctx.lift)
                                                )
                                            ]

                                        ( _, Dex.Zoom _, _ ) ->
                                            [ class "opacity-0", attribute "aria-hidden" "true" ]

                                        ( _, _, Dex.Zoom _ ) ->
                                            [ class "opacity-0", attribute "aria-hidden" "true" ]

                                        _ ->
                                            []
                                   )
                            )
                            [ viewDetail ctx model detailAndStatus
                                |> Atoms.Html.disable (index /= 1)
                            ]
                        )
                    )
            )
        , if model.viewport.width < Nav.mobileNavWidthLimit then
            Atoms.Html.none

          else
            div
                [ class "pointer-events-none absolute top-0 z-20 flex h-screen w-full items-center justify-between p-4 transition-opacity"
                , if onPage then
                    class "opacity-70"

                  else
                    class "opacity-0"
                ]
                [ Atoms.Html.a
                    [ href (ids.left |> Tuple.first |> Dex.Detail |> Route.Dex |> Route.fromRoute)
                    , class "hover:-translate-x-1 hover:scale-125"
                    , if Tuple.second ids.left == 0 then
                        class "pointer-events-auto"

                      else
                        class "opacity-0"
                    ]
                    [ iconWith { name = "arrow", height = 100, width = 50, description = Translations.dexDetailPrevious }
                        [ Attributes.style "transform" "scale(-1)"
                        , class "transition-transform dark:invert"
                        ]
                    ]
                , Atoms.Html.a
                    [ href (ids.right |> Tuple.first |> Dex.Detail |> Route.Dex |> Route.fromRoute)
                    , class "z-20 transition-transform hover:translate-x-1 hover:scale-125"
                    , if Tuple.second ids.left == 0 then
                        class "pointer-events-auto"

                      else
                        class "opacity-0"
                    ]
                    [ iconWith { name = "arrow", height = 100, width = 50, description = Translations.dexDetailNext }
                        [ class "dark:invert" ]
                    ]
                ]
        ]
    }


mainDetailScrollId : String
mainDetailScrollId =
    "dex-detail-modal"


viewDetail :
    Context msg
    -> ErrorHandling.Session { model | viewport : { width : Int, height : Int }, requestManager : RequestManager } Language.ConfigurableLanguage Route.Route
    -> ( Maybe DexDetail, RequestManager.Status Error.Error )
    -> Html msg
viewDetail ctx ({ viewport } as model) ( detail, detailStatus ) =
    let
        isLandscape =
            viewport.width > viewport.height

        choosePartnerPokemonStatus =
            RequestManager.status
                (RequestManager.SetPartnerPokemon { pokemonNumber = untag ctx.pokemonNumber })
                model.requestManager

        changePartnerErrorText =
            case choosePartnerPokemonStatus of
                RequestManager.Failure error ->
                    case ErrorHandling.handleWebError model { languageToString = Language.toString } error of
                        ErrorHandling.Blocking overlay ->
                            ErrorHandling.viewErrorSmall overlay

                        _ ->
                            Html.WithContext.text ""

                _ ->
                    Html.WithContext.text ""

        outerElementStyling =
            class "relative flex h-full w-full gap-2 overflow-auto rounded-lg bg-gray-200 px-2 pt-2 opacity-100 dark:bg-gray-700 dark:text-white"

        scrollContainerMarker =
            withContextAttribute
                (\{ disabled } ->
                    if disabled then
                        class ""

                    else
                        Attributes.id mainDetailScrollId
                )

        types =
            Maybe.map DexDetail.getPokemonTypes detail |> Maybe.withDefault []

        gradientStyles =
            case types of
                [ primary, secondary ] ->
                    [ TypeColors.gradientFromFromType primary, TypeColors.gradientToFromType secondary ]

                [ primary ] ->
                    [ TypeColors.gradientFromFromType primary, TypeColors.gradientToFromType primary ]

                _ ->
                    []
    in
    div
        ([ class "relative flex h-full w-full touch-none justify-center rounded-lg bg-gray-400 bg-gradient-to-br p-2"
         , if isLandscape then
            class "max-h-96 max-w-3xl"

           else
            class "max-w-md"
         , withContextAttribute
            (\{ disabled } ->
                if disabled then
                    class ""

                else
                    attribute "role" "dialog"
            )
         , Touch.onWithOptions "touchstart"
            { preventDefault = False, stopPropagation = False }
            (relativePos
                >> Maybe.map (TouchDownOnDetail >> ctx.lift)
                >> Maybe.withDefault (ctx.liftShared Shared.NoOp)
            )
            |> Html.WithContext.htmlAttribute
         , Touch.onWithOptions "touchmove"
            { preventDefault = False, stopPropagation = False }
            (relativePos
                >> Maybe.map (TouchMoveOnDetail >> ctx.lift)
                >> Maybe.withDefault (ctx.liftShared Shared.NoOp)
            )
            |> Html.WithContext.htmlAttribute
         , Touch.onWithOptions "touchend"
            { preventDefault = False, stopPropagation = False }
            (relativePos
                >> Maybe.map (TouchUpOnDetail >> ctx.lift)
                >> Maybe.withDefault (ctx.liftShared Shared.NoOp)
            )
            |> Html.WithContext.htmlAttribute
         ]
            ++ gradientStyles
        )
    <|
        case ( detail, detailStatus ) of
            ( Just dexDetail, RequestManager.Success ) ->
                if isLandscape then
                    [ div [ outerElementStyling, scrollContainerMarker, class "justify-center" ]
                        [ Atoms.Html.button
                            [ class "absolute left-0 top-0 p-2 opacity-70"
                            , Events.onClick (CloseDetail |> ctx.lift)
                            ]
                            [ Main.Html.backIcon ]
                        , div [] (viewImages [ class "h-1/2 max-w-none" ] dexDetail)
                        , div [ class "flex flex-col items-center gap-2 after:-m-2 after:p-1" ]
                            (viewHeadline dexDetail
                                :: viewTextualContents dexDetail
                                ++ [ div [ class "pt-2" ] [ changePartnerButton ctx choosePartnerPokemonStatus ]
                                   , changePartnerErrorText
                                   ]
                            )
                        ]
                    ]

                else
                    [ div [ outerElementStyling, scrollContainerMarker, class "flex-col items-center after:-m-2 after:p-1" ] <|
                        [ Atoms.Html.button
                            [ class "absolute self-start opacity-70"
                            , Events.onClick (CloseDetail |> ctx.lift)
                            ]
                            [ Main.Html.backIcon ]
                        , div [ class "flex w-full" ] (viewImages [ class "w-1/2" ] dexDetail)
                        , viewHeadline dexDetail
                        ]
                            ++ viewTextualContents dexDetail
                            ++ [ div [ class "pt-2" ] [ changePartnerButton ctx choosePartnerPokemonStatus ]
                               , changePartnerErrorText
                               ]
                    ]

            ( _, RequestManager.Failure error ) ->
                [ Atoms.Html.button
                    [ class "absolute left-0 top-0 p-2 opacity-70"
                    , Events.onClick (CloseDetail |> ctx.lift)
                    ]
                    [ Main.Html.backIcon ]
                , case ErrorHandling.handleWebError model { languageToString = Language.toString } error of
                    ErrorHandling.AutomaticallyFixable _ _ ->
                        Atoms.Html.none

                    ErrorHandling.Blocking overlay ->
                        ErrorHandling.viewError overlay
                ]

            _ ->
                [ Atoms.Html.button
                    [ class "absolute left-0 top-0 p-2 opacity-70"
                    , Events.onClick (CloseDetail |> ctx.lift)
                    ]
                    [ Main.Html.backIcon ]
                , div
                    [ class "relative h-full w-20 opacity-100"
                    ]
                    [ Atoms.Loading.loadingIndicator ]
                ]


viewImages : List (Attribute msg) -> DexDetail -> List (Html msg)
viewImages styles { amountNormal, amountShiny, name, number } =
    [ PokemonImage.showImageHtml styles
        { name = name, pokemonNumber = number }
        (if amountNormal > 0 then
            PokemonImage.Normal

         else
            PokemonImage.Missing
        )
    , PokemonImage.showImageHtml styles
        { name = name, pokemonNumber = number }
        (if amountShiny > 0 then
            PokemonImage.Shiny

         else
            PokemonImage.Missing
        )
    ]


relativePos : Touch.Event -> Maybe ( Float, Float )
relativePos event =
    List.head event.changedTouches
        |> Maybe.map (\touch -> touch.clientPos)


viewHeadline : DexDetail -> Html msg
viewHeadline { name } =
    h1 [ class "w-full border-y border-black p-2 text-center text-xl font-bold dark:border-white" ] [ Html.WithContext.text name ]


viewTextualContents : DexDetail -> List (Html msg)
viewTextualContents ({ amountNormal, amountShiny } as detail) =
    [ div [ class "flex justify-center" ] <|
        List.map DexDetail.presentType (DexDetail.getPokemonTypes detail)
    , div [ class "flex w-full" ]
        [ div [ class "flex w-full flex-col items-center" ]
            [ Atoms.Html.text Translations.dexDetailHeightTitle
            , span [ class "text-sky-400" ] [ Atoms.Html.text <| Translations.dexDetailHeightFormat detail.height ]
            ]
        , div [ class "flex w-full flex-col items-center" ]
            [ Atoms.Html.text Translations.dexDetailWeightTitle
            , span [ class "text-sky-400" ] [ Atoms.Html.text <| Translations.dexDetailWeightFormat detail.weight ]
            ]
        ]
    , p [ class "bg-gray-300 p-2 dark:bg-gray-800" ] [ Html.WithContext.text detail.description ]
    , div [ class "flex w-full justify-around" ]
        [ p []
            [ Atoms.Html.text Translations.dexDetailCaught
            , Html.WithContext.text ": "
            , span [ class "text-sky-400" ] [ Html.WithContext.text <| String.fromInt <| amountNormal + amountShiny ]
            ]
        , p []
            [ Atoms.Html.text Translations.dexDetailShiny
            , Html.WithContext.text ": "
            , span [ class "text-sky-400" ] [ Html.WithContext.text <| String.fromInt amountShiny ]
            ]
        ]
    ]


changePartnerButton : Context msg -> RequestManager.Status e -> Html msg
changePartnerButton ctx changePartnerStatus =
    Atoms.Button.button
        { onPress = ctx.lift ChoosePartnerPokemon |> Just
        , label = Atoms.Html.text Translations.dexDetailChangePartner
        , loading = changePartnerStatus == RequestManager.Loading
        }
        []


onReadyToCloseDetail : Context msg -> { pokemonEl : Dom.Element, pokedexEl : Dom.Viewport } -> Model m -> ( Model m, Cmd msg )
onReadyToCloseDetail ctx elements ({ dexModel } as model) =
    let
        dexScrollY =
            Pokedex.calculateScrollHeightForId elements.pokedexEl ctx.pokemonNumber

        zoomElement =
            { top = round elements.pokemonEl.element.y - dexScrollY
            , left = round <| elements.pokemonEl.element.x
            , width = round <| elements.pokemonEl.element.width
            , height = round <| elements.pokemonEl.element.height
            }
    in
    ( { model
        | dexModel =
            { dexModel
                | dexScrollY = dexScrollY
                , transitionTimeline =
                    Animator.queue
                        [ Animator.event Animator.slowly ( Dex.Detail ctx.pokemonNumber, Dex.Zoom zoomElement )
                        , Animator.event Animator.immediately ( Dex.Overview, Dex.OnPage )
                        ]
                        dexModel.transitionTimeline
            }
      }
    , Route.navigate model <| Route.Dex Dex.Overview
    )


type alias Adjacent a =
    { left : a
    , middle : a
    , right : a
    }


adjacentToList : Adjacent a -> List a
adjacentToList { left, middle, right } =
    [ left, middle, right ]


adjacentDexEntryIds : PokemonNumber -> Array DexEntry -> Adjacent ( PokemonNumber, Int )
adjacentDexEntryIds pokemonNumber dexEntries =
    { left = adjacentDexEntry pokemonNumber -1 dexEntries |> Maybe.map (\dexEntry -> ( tag dexEntry.number, 0 )) |> Maybe.withDefault ( pokemonNumber, -1 )
    , middle = ( pokemonNumber, 0 )
    , right = adjacentDexEntry pokemonNumber 1 dexEntries |> Maybe.map (\dexEntry -> ( tag dexEntry.number, 0 )) |> Maybe.withDefault ( pokemonNumber, 1 )
    }


adjacentDexEntry : PokemonNumber -> Int -> Array DexEntry -> Maybe DexEntry
adjacentDexEntry pokemonNumber relativeIndex dexEntries =
    dexEntries
        |> Array.toList
        |> List.Extra.findIndex (\{ number } -> tag number == pokemonNumber)
        |> Maybe.andThen
            (\index ->
                Array.get
                    (modBy
                        (Array.length dexEntries)
                     <|
                        index
                            + relativeIndex
                    )
                    dexEntries
            )


transformDetail : Maybe ( Float, Float ) -> Animator.Timeline ( Route, Dex.TransitionState ) -> { s | width : Int, height : Int } -> Html.Attribute msg
transformDetail detailSwipeMove timeline { width, height } =
    let
        x =
            Animator.move timeline
                (\state ->
                    case state of
                        ( _, Dex.Zoom element ) ->
                            Animator.at (toFloat element.left - (toFloat (width - element.width) / 2))

                        ( _, Dex.Slide Dex.Left mainContainer ) ->
                            Animator.at (toFloat -mainContainer.width)

                        ( _, Dex.Slide Dex.Right mainContainer ) ->
                            Animator.at (toFloat mainContainer.width)

                        _ ->
                            Animator.at
                                (if moveX < 0 then
                                    -1 * sqrt -moveX

                                 else
                                    sqrt moveX
                                )
                )

        y =
            Animator.move timeline
                (\state ->
                    case state of
                        ( _, Dex.Zoom element ) ->
                            Animator.at (toFloat element.top - (toFloat (height - element.height) / 2))

                        _ ->
                            Animator.at 0
                )

        scaleX =
            Animator.move timeline
                (\state ->
                    case state of
                        ( _, Dex.Zoom element ) ->
                            Animator.at (toFloat element.width / toFloat width)

                        _ ->
                            Animator.at 1
                )

        scaleY =
            Animator.move timeline
                (\state ->
                    case state of
                        ( _, Dex.Zoom element ) ->
                            Animator.at (toFloat element.height / toFloat height)

                        _ ->
                            Animator.at 1
                )

        moveX =
            detailSwipeMove |> Maybe.map Tuple.first |> Maybe.withDefault 0
    in
    Html.Attributes.style "transform"
        (("translate(" ++ String.fromFloat x ++ "px, " ++ String.fromFloat y ++ "px)")
            ++ ("scaleX(" ++ String.fromFloat scaleX ++ ")")
            ++ ("scaleY(" ++ String.fromFloat scaleY ++ ")")
        )
