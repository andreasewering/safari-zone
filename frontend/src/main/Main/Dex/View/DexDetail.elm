module Main.Dex.View.DexDetail exposing (getPokemonTypes, presentType)

import Atoms.Html
import Html.WithContext as Html
import Html.WithContext.Attributes exposing (class)
import Main.Dex.View.TypeColors as TypeColors
import Main.Html exposing (Html)
import Main.Translations as Translations exposing (I18n)
import Proto.Kangaskhan.Type exposing (Type(..))


getPokemonTypes : { a | primaryType : Type, secondaryType : Maybe Type } -> List Type
getPokemonTypes { primaryType, secondaryType } =
    case secondaryType of
        Just UNKNOWN ->
            [ primaryType ]

        Just (TypeUnrecognized_ _) ->
            [ primaryType ]

        Just t ->
            [ primaryType, t ]

        Nothing ->
            [ primaryType ]


presentType : Type -> Html msg
presentType t =
    Html.div
        [ class "rounded-full border-2 bg-gray-300 p-2 dark:bg-gray-800"
        , TypeColors.borderFromType t
        ]
        [ Atoms.Html.text <|
            typeToString t
        ]


typeToString : Type -> I18n -> String
typeToString t =
    case t of
        Fire ->
            Translations.pokemonTypeFire

        Grass ->
            Translations.pokemonTypeGrass

        Flying ->
            Translations.pokemonTypeFlying

        Normal ->
            Translations.pokemonTypeNormal

        Poison ->
            Translations.pokemonTypePoison

        Water ->
            Translations.pokemonTypeWater

        Fighting ->
            Translations.pokemonTypeFighting

        Psychic ->
            Translations.pokemonTypePsychic

        Ice ->
            Translations.pokemonTypeIce

        Ground ->
            Translations.pokemonTypeGround

        Rock ->
            Translations.pokemonTypeRock

        Electric ->
            Translations.pokemonTypeElectric

        Steel ->
            Translations.pokemonTypeSteel

        Fairy ->
            Translations.pokemonTypeFairy

        Bug ->
            Translations.pokemonTypeBug

        Dragon ->
            Translations.pokemonTypeDragon

        Ghost ->
            Translations.pokemonTypeGhost

        UNKNOWN ->
            \_ -> "???"

        TypeUnrecognized_ _ ->
            \_ -> "???"
