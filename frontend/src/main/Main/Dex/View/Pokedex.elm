module Main.Dex.View.Pokedex exposing (calculateScrollHeightForId, getPokemonImageDomId, view)

import Array exposing (Array)
import Atoms.Html
import Browser.Dom as Dom
import Html.WithContext as Html
import Html.WithContext.Attributes exposing (class, href)
import Main.Dex.Route as Dex
import Main.Dex.View.Progressbar as Progressbar
import Main.ErrorHandling as ErrorHandling
import Main.Html exposing (Attribute, Html)
import Main.Language as Language exposing (ConfigurableLanguage)
import Main.RequestManager as RequestManager exposing (Status)
import Main.Route as Route
import Main.Translations as Translations
import PokemonNumber exposing (PokemonNumber)
import Proto.Kangaskhan exposing (DexEntry)
import Shared.Background as Background
import Shared.Types.Error exposing (Error)
import Shared.View.PokemonImage as PokemonImage
import Tagged exposing (tag, untag)


getPokemonImageDomId : PokemonNumber -> String
getPokemonImageDomId pokemonNumber =
    "dex-" ++ String.fromInt (untag pokemonNumber)


view :
    List (Attribute msg)
    -> Status Error
    ->
        { session
            | dexEntries : Array DexEntry
            , errorCtx : ErrorHandling.Session s ConfigurableLanguage r
        }
    -> Html msg
view extraAttrs requestStatus session =
    let
        pokedex =
            PokemonImage.showPokedex
                { pokemonAttributes =
                    \pokemon ->
                        let
                            pokemonNumber =
                                tag pokemon.number

                            detailLink =
                                pokemonNumber
                                    |> Dex.Detail
                                    |> Route.Dex
                                    |> Route.fromRoute
                        in
                        [ Html.WithContext.Attributes.id (getPokemonImageDomId pokemonNumber)
                        , href detailLink
                        ]
                            ++ extraAttrs
                , missingEntryAttributes = []
                }
                session.dexEntries
    in
    Html.div [ class "inline-container flex h-full max-h-svh w-full flex-col items-center" ]
        (List.concat
            [ [ headline ]
            , [ pokedexGrade requestStatus session.dexEntries ]
            , case requestStatus of
                RequestManager.Failure error ->
                    case ErrorHandling.handleWebError session.errorCtx { languageToString = Language.toString } error of
                        ErrorHandling.AutomaticallyFixable _ _ ->
                            []

                        ErrorHandling.Blocking overlay ->
                            [ ErrorHandling.viewError overlay ]

                RequestManager.Loading ->
                    [ pokedex ]

                RequestManager.NotAsked ->
                    []

                RequestManager.Success ->
                    [ pokedex ]
            ]
        )


calculateScrollHeightForId : Dom.Viewport -> PokemonNumber -> Int
calculateScrollHeightForId pokedexEl pokemonNumber =
    let
        width =
            floor pokedexEl.scene.width

        pokemonPerRow =
            width // pokemonTileSize

        targetRow =
            untag pokemonNumber // pokemonPerRow

        scrollSuchThatTargetIsAtTop =
            targetRow * pokemonTileSize

        scrollSuchThatTargetIsInMiddle =
            scrollSuchThatTargetIsAtTop - (floor pokedexEl.viewport.height - pokemonTileSize) // 2
    in
    scrollSuchThatTargetIsInMiddle


pokemonTileSize : number
pokemonTileSize =
    120


pokedexGrade : Status e -> Array DexEntry -> Html msg
pokedexGrade status entries =
    let
        total =
            151

        done =
            Array.length entries

        progressbarOpts =
            case status of
                RequestManager.Loading ->
                    Progressbar.Loading

                RequestManager.Success ->
                    Progressbar.Standard { done = done, total = total }

                _ ->
                    Progressbar.Unknown { total = total }
    in
    Atoms.Html.a
        [ href (Route.Dex Dex.Appraisal |> Route.fromRoute), class "flex w-full items-center justify-center p-4" ]
        [ Progressbar.progressbar progressbarOpts
        ]


headline : Html msg
headline =
    Html.h1
        [ class
            "pokedex-width border-b p-2 text-center text-xl font-bold"
        , Background.contrastingText
        , Background.contrastingBorder
        ]
        [ Atoms.Html.text Translations.dexOverviewHeadline ]
