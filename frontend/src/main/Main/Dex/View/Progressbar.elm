module Main.Dex.View.Progressbar exposing (Options(..), progressbar)

import Atoms.Html
import Atoms.Loading
import Html.WithContext as Html
import Html.WithContext.Attributes exposing (class, style)
import Main.Html exposing (Html)


type Options
    = Standard
        { done : Int
        , total : Int
        }
    | Loading
    | Unknown { total : Int }


progressbar : Options -> Html msg
progressbar options =
    let
        completionPercent =
            case options of
                Standard { done, total } ->
                    toFloat done * 100 / toFloat total

                _ ->
                    0

        completionText =
            case options of
                Standard { done, total } ->
                    String.fromInt done ++ " / " ++ String.fromInt total

                Loading ->
                    ""

                Unknown { total } ->
                    "? / " ++ String.fromInt total
    in
    Html.div
        [ class "relative flex h-10 w-2/3 max-w-xl items-center overflow-hidden rounded-full bg-gray-200 dark:bg-gray-700"
        ]
        [ Html.div
            [ class "h-10 bg-sky-300 dark:bg-sky-700"
            , style "width" <| String.fromFloat completionPercent ++ "%"
            ]
            []
        , Html.div [ class "absolute left-1/2 -translate-x-1/2 text-black dark:text-white" ] [ Html.text completionText ]
        , case options of
            Loading ->
                Html.div [ class "absolute left-1/2 h-full w-1/4 -translate-x-1/2" ] [ Atoms.Loading.loadingIndicator ]

            _ ->
                Atoms.Html.none
        ]
