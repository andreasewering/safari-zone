module Main.Dex.View.TypeColors exposing (..)

import Html.WithContext
import Html.WithContext.Attributes exposing (class)
import Proto.Kangaskhan.Type exposing (Type(..))


borderFromType : Type -> Html.WithContext.Attribute ctx msg
borderFromType t =
    case t of
        Fire ->
            class "border-red-600"

        Grass ->
            class "border-green-600"

        Flying ->
            class "border-sky-400"

        Normal ->
            class "border-pink-100"

        Poison ->
            class "border-pink-800"

        Water ->
            class "border-sky-600"

        Fighting ->
            class "border-amber-600"

        Psychic ->
            class "border-pink-500"

        Ice ->
            class "border-sky-200"

        Ground ->
            class "border-amber-400"

        Rock ->
            class "border-amber-800"

        Electric ->
            class "border-amber-200"

        Steel ->
            class "border-gray-400"

        Fairy ->
            class "border-pink-300"

        Bug ->
            class "border-green-300"

        Dragon ->
            class "border-sky-800"

        Ghost ->
            class "border-gray-700"

        UNKNOWN ->
            class ""

        TypeUnrecognized_ _ ->
            class ""


gradientFromFromType : Type -> Html.WithContext.Attribute ctx msg
gradientFromFromType t =
    case t of
        Fire ->
            class "from-red-600"

        Grass ->
            class "from-green-600"

        Flying ->
            class "from-sky-400"

        Normal ->
            class "from-pink-100"

        Poison ->
            class "from-pink-800"

        Water ->
            class "from-sky-600"

        Fighting ->
            class "from-amber-600"

        Psychic ->
            class "from-pink-500"

        Ice ->
            class "from-sky-200"

        Ground ->
            class "from-amber-400"

        Rock ->
            class "from-amber-800"

        Electric ->
            class "from-amber-200"

        Steel ->
            class "from-gray-400"

        Fairy ->
            class "from-pink-300"

        Bug ->
            class "from-green-300"

        Dragon ->
            class "from-sky-800"

        Ghost ->
            class "from-gray-700"

        UNKNOWN ->
            class ""

        TypeUnrecognized_ _ ->
            class ""


gradientToFromType : Type -> Html.WithContext.Attribute ctx msg
gradientToFromType t =
    case t of
        Fire ->
            class "to-red-600"

        Grass ->
            class "to-green-600"

        Flying ->
            class "to-sky-400"

        Normal ->
            class "to-pink-100"

        Poison ->
            class "to-pink-800"

        Water ->
            class "to-sky-600"

        Fighting ->
            class "to-amber-600"

        Psychic ->
            class "to-pink-500"

        Ice ->
            class "to-sky-200"

        Ground ->
            class "to-amber-400"

        Rock ->
            class "to-amber-800"

        Electric ->
            class "to-amber-200"

        Steel ->
            class "to-gray-400"

        Fairy ->
            class "to-pink-300"

        Bug ->
            class "to-green-300"

        Dragon ->
            class "to-sky-800"

        Ghost ->
            class "to-gray-700"

        UNKNOWN ->
            class ""

        TypeUnrecognized_ _ ->
            class ""
