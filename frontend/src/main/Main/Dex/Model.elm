module Main.Dex.Model exposing (..)

import Animator
import Main.Dex.Route as Dex


type alias PageModel =
    { dexScrollY : Int
    , detailSwipeStart : Maybe ( Float, Float )
    , detailSwipeMove : Maybe ( Float, Float )
    , transitionTimeline : Animator.Timeline ( Dex.Route, TransitionState )
    }


type TransitionState
    = OnPage
    | Zoom { top : Int, left : Int, height : Int, width : Int }
    | Slide SlideDirection { width : Int }
    | FadeStart
    | FadeEnd


type SlideDirection
    = Left
    | Right
