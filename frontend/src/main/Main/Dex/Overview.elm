module Main.Dex.Overview exposing (Context, init, onPageLoad)

import Animator
import Browser.Dom as Dom
import Main.Dex.Model as Dex exposing (PageModel, TransitionState)
import Main.Dex.Route as Dex
import Main.RequestManager as RequestManager exposing (RequestManager)
import Main.Shared as Shared
import Shared.View.PokemonImage as PokemonImage
import Task


type alias Model m =
    { m
        | dexModel : PageModel
        , requestManager : RequestManager
    }


type alias Context msg =
    { liftShared : Shared.Msg -> msg
    }


init : PageModel
init =
    { dexScrollY = 0
    , detailSwipeStart = Nothing
    , detailSwipeMove = Nothing
    , transitionTimeline = initialTransitionTimeline
    }


initialTransitionTimeline : Animator.Timeline ( Dex.Route, TransitionState )
initialTransitionTimeline =
    Animator.init ( Dex.Overview, Dex.OnPage )


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx ({ dexModel } as model) =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                [ RequestManager.GetDexEntries {} ]
                model.requestManager
    in
    ( { model
        | dexModel = { dexModel | transitionTimeline = initialTransitionTimeline }
        , requestManager = requestManager
      }
    , Cmd.batch
        [ Dom.setViewportOf PokemonImage.pokedexId 0 (toFloat model.dexModel.dexScrollY)
            |> Task.attempt
                (\_ -> ctx.liftShared Shared.NoOp)
        , requestManagerCmds |> Cmd.map (Shared.RequestManagerMessage >> ctx.liftShared)
        ]
    )
