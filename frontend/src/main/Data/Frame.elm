module Data.Frame exposing (..)

import Fuzz exposing (Fuzzer)


fuzzMax1Tile : Fuzzer Float
fuzzMax1Tile =
    Fuzz.floatRange 0 1
