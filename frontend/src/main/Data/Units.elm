module Data.Units exposing (..)


type TileUnit
    = TileUnit Float


type WebGLUnit
    = WebGLUnit Float


unTileUnit : TileUnit -> Float
unTileUnit (TileUnit t) =
    t


unWebGLUnit : WebGLUnit -> Float
unWebGLUnit (WebGLUnit w) =
    w


addTileUnit : TileUnit -> TileUnit -> TileUnit
addTileUnit (TileUnit t1) (TileUnit t2) =
    TileUnit <| t1 + t2


roundTileUnit : TileUnit -> TileUnit
roundTileUnit (TileUnit t) =
    TileUnit <| toFloat <| round t
