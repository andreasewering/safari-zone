import { lazy } from '../../lib/lazy.js';
import { createCache } from './sizedCache.js';

export type AudioMsg = (Elm.ToElm & { tag: 'audio' })['data'];

type Send = (msg: AudioMsg) => void;

type Receive = (msg: (Elm.FromElm & { tag: 'audio' })['data']) => void;

const context = lazy(() => new AudioContext());

const gainNode = context.derive((ctx) => {
  const gainNode = ctx.createGain();
  gainNode.gain.setValueAtTime(0.5, ctx.currentTime);
  return gainNode;
});

const fetchWithCache = createCache<AudioBuffer>(4)((url: string) =>
  fetch(url)
    .then((res) => res.arrayBuffer())
    .then((buffer) => context.value.decodeAudioData(buffer))
);

const loadAudioAndCreateSource = async (url: string) => {
  const source = context.value.createBufferSource();
  source.buffer = await fetchWithCache(url);
  return source;
};

const fadeOutAndStop = async (node: AudioBufferSourceNode) => {
  gainNode.value.gain.linearRampToValueAtTime(
    Number.EPSILON,
    context.value.currentTime + 2
  );
  await new Promise((resolve) => setTimeout(resolve, 2000));
  node.stop();
};

export const audioPort = (send: Send): Receive => {
  let activeSource: { node: AudioBufferSourceNode; url: string } | undefined;
  let isLoading = false;

  const onTrackEnd = (url: string) => {
    send({ tag: 'ended', url });
  };

  const loadAndStartAudio = async (filename: string) => {
    const url = `/audio/${filename}`;
    const source = await loadAudioAndCreateSource(url);
    source.connect(context.value.destination);
    source.addEventListener('ended', () => {
      onTrackEnd(url);
    });
    source.start();
  };

  const loadAndLoopAudio = async ({
    start,
    end,
    filename,
  }: {
    start: number | null;
    end: number | null;
    filename: string;
  }) => {
    if (isLoading) {
      return;
    }
    const url = `/audio/${filename}`;
    if (activeSource?.url === url) {
      await context.value.resume();
      return;
    }
    isLoading = true;
    if (activeSource) {
      await fadeOutAndStop(activeSource.node);
    }
    const source = await loadAudioAndCreateSource(url);
    source.loopStart = start ?? 0;
    const loopEnd = end ?? source.buffer?.duration;
    if (loopEnd != null) {
      source.loopEnd = loopEnd;
    }
    source.loop = true;
    source.connect(gainNode.value).connect(context.value.destination);
    activeSource = { node: source, url };
    activeSource.node.start();

    gainNode.value.gain.linearRampToValueAtTime(
      0.5,
      context.value.currentTime + 2
    );
    isLoading = false;
  };

  return (msg) => {
    switch (msg.tag) {
      case 'start':
        void loadAndStartAudio(msg.data);
        return;
      case 'loop':
        void loadAndLoopAudio(msg.data);
        return;
      case 'fadeOut':
        if (activeSource) {
          void fadeOutAndStop(activeSource.node);
        }
        activeSource = undefined;
        return;
      case 'stop':
        void context.value.suspend();
        return;
      case 'resume':
        void context.value.resume();
    }
  };
};
