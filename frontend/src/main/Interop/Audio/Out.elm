module Interop.Audio.Out exposing (..)

import TsJson.Encode as TsEncode


type Msg
    = Resume
    | Start String
    | Loop AudioLoop
    | FadeOutAndStop
    | Stop


type alias AudioLoop =
    { filename : String, loopStart : Maybe Float, loopEnd : Maybe Float }


encoder : TsEncode.Encoder Msg
encoder =
    TsEncode.union
        (\vResume vStart vLoop vFadeOut vStop value ->
            case value of
                Resume ->
                    vResume

                Start s ->
                    vStart s

                Loop l ->
                    vLoop l

                FadeOutAndStop ->
                    vFadeOut

                Stop ->
                    vStop
        )
        |> TsEncode.variant0 "resume"
        |> TsEncode.variantTagged "start" TsEncode.string
        |> TsEncode.variantTagged "loop" audioLoopEncoder
        |> TsEncode.variant0 "fadeOut"
        |> TsEncode.variant0 "stop"
        |> TsEncode.buildUnion


audioLoopEncoder : TsEncode.Encoder AudioLoop
audioLoopEncoder =
    TsEncode.object
        [ TsEncode.required "filename" .filename TsEncode.string
        , TsEncode.required "start" .loopStart (TsEncode.maybe TsEncode.float)
        , TsEncode.required "end" .loopEnd (TsEncode.maybe TsEncode.float)
        ]
