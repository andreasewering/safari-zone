module Interop.Audio.In exposing (..)

import TsJson.Decode as TsDecode


type Msg
    = Ended String
    | AudioError String


decoder : TsDecode.Decoder Msg
decoder =
    TsDecode.discriminatedUnion "tag"
        [ ( "ended", TsDecode.field "url" TsDecode.string |> TsDecode.map Ended )
        , ( "error", TsDecode.field "error" TsDecode.string |> TsDecode.map AudioError )
        ]
