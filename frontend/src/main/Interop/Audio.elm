module Interop.Audio exposing (fadeOutAndStop, loop, resume, start, stop)

import Interop.Audio.Out as Out
import InteropDefinitions
import InteropPorts


resume : Cmd msg
resume =
    send Out.Resume


stop : Cmd msg
stop =
    send Out.Stop


fadeOutAndStop : Cmd msg
fadeOutAndStop =
    send Out.FadeOutAndStop


start : String -> Cmd msg
start =
    Out.Start >> send


loop : Out.AudioLoop -> Cmd msg
loop =
    Out.Loop >> send


send : Out.Msg -> Cmd msg
send =
    InteropDefinitions.AudioOut >> InteropPorts.fromElm
