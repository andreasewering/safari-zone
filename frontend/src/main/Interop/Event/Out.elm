module Interop.Event.Out exposing (Msg, encoder)

import Json.Encode as JsEncode
import TsJson.Encode as TsEncode


type alias Msg =
    { eventName : String
    , args : List JsEncode.Value
    }


encoder : TsEncode.Encoder Msg
encoder =
    TsEncode.object
        [ TsEncode.required "name" .eventName TsEncode.string
        , TsEncode.required "args" .args (TsEncode.list TsEncode.value)
        ]
