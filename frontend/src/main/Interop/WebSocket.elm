module Interop.WebSocket exposing (connectToKangaskhan, disconnectFromKangaskhan, sendKangaskhanMsg, sendProto)

import Auth exposing (AuthManager)
import Interop.WebSocket.Out exposing (Msg(..))
import InteropDefinitions
import InteropPorts
import MapId exposing (MapId)
import Proto.Kangaskhan
import Proto.Kangaskhan.WebsocketMessageToServer
import Protobuf.Encode as PE



-- Kangaskhan


kangaskhanWs : AuthManager -> { key : String, url : String }
kangaskhanWs auth =
    let
        mapId =
            Auth.getMapId auth

        token =
            Auth.getToken auth
    in
    { url = "/kangaskhan/ws?token=" ++ token ++ "&map_id=" ++ MapId.toHeader mapId
    , key = kangaskhanKey mapId
    }


kangaskhanKey : MapId -> String
kangaskhanKey mapId =
    InteropDefinitions.kangaskhanPrefix ++ MapId.toHeader mapId


connectToKangaskhan : AuthManager -> Cmd msg
connectToKangaskhan auth =
    Connect (kangaskhanWs auth)
        |> send


disconnectFromKangaskhan : MapId -> Cmd msg
disconnectFromKangaskhan mapId =
    Disconnect { key = kangaskhanKey mapId } |> send


sendKangaskhanMsg : MapId -> Proto.Kangaskhan.WebsocketMessageToServer.Message -> Cmd msg
sendKangaskhanMsg mapId msg =
    { key = kangaskhanKey mapId
    , msg =
        { message = Just msg }
            |> Proto.Kangaskhan.encodeWebsocketMessageToServer
    }
        |> sendProto


sendProto : { key : String, msg : PE.Encoder } -> Cmd msg
sendProto { key, msg } =
    { key = key, msg = PE.encode msg }
        |> Interop.WebSocket.Out.Send
        |> send


send : Interop.WebSocket.Out.Msg -> Cmd msg
send =
    InteropDefinitions.WebsocketOut >> InteropPorts.fromElm
