module Interop.Clipboard exposing (copy)

import InteropDefinitions
import InteropPorts


copy : String -> Cmd msg
copy =
    InteropDefinitions.CopyToClipboard >> InteropPorts.fromElm
