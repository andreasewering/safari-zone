module Interop.Event exposing (mapLoaded, playerMoveComplete, typewriterFinished)

import Domain.Direction as Direction
import Domain.Position exposing (StandingPosition)
import InteropDefinitions
import InteropPorts
import Json.Encode as JsEncode
import MapId exposing (MapId)
import Protobuf.Types.Int64 exposing (Int64)
import Protobuf.Utils.Int64 as Int64
import Tagged exposing (untag)
import UserId exposing (UserId)


playerMoveComplete : UserId -> StandingPosition -> Cmd msg
playerMoveComplete userId position =
    let
        directionAsCoord =
            Direction.directionToCoord position.direction
    in
    InteropDefinitions.EventOut
        { eventName = "playerMoveComplete"
        , args =
            [ taggedInt64ToValue userId
            , JsEncode.object
                [ ( "x", JsEncode.int position.coord.x )
                , ( "y", JsEncode.int position.coord.y )
                , ( "layerNumber", JsEncode.int position.layerNumber )
                , ( "direction", JsEncode.object [ ( "x", JsEncode.int directionAsCoord.x ), ( "y", JsEncode.int directionAsCoord.y ) ] )
                ]
            ]
        }
        |> InteropPorts.fromElm


mapLoaded : MapId -> Cmd msg
mapLoaded mapId =
    InteropDefinitions.EventOut
        { eventName = "mapLoaded"
        , args = [ taggedInt64ToValue mapId ]
        }
        |> InteropPorts.fromElm


typewriterFinished : String -> Cmd msg
typewriterFinished name =
    InteropDefinitions.EventOut
        { eventName = "typewriterFinished"
        , args = [ JsEncode.string name ]
        }
        |> InteropPorts.fromElm


taggedInt64ToValue : Tagged.Tagged Int64 tag -> JsEncode.Value
taggedInt64ToValue userId =
    untag userId |> Int64.toSignedString |> JsEncode.string
