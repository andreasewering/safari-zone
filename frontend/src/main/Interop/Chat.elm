module Interop.Chat exposing (connect, sendMessage)

import Auth exposing (AuthManager)
import Interop.WebSocket exposing (sendProto)
import Interop.WebSocket.Out exposing (Msg(..))
import InteropDefinitions
import InteropPorts
import Proto.Chatot exposing (encodeWebsocketMessageToServer)
import Proto.Chatot.WebsocketMessageToServer exposing (Message)


send : Interop.WebSocket.Out.Msg -> Cmd msg
send =
    InteropDefinitions.WebsocketOut >> InteropPorts.fromElm


chatotWs : AuthManager -> { key : String, url : String }
chatotWs auth =
    let
        token =
            Auth.getToken auth
    in
    { url = "/chatot/ws?token=" ++ token
    , key = chatotKey
    }


chatotKey : String
chatotKey =
    InteropDefinitions.chatotPrefix


sendMessage : Message -> Cmd msg
sendMessage msg =
    sendProto
        { key = chatotKey
        , msg =
            encodeWebsocketMessageToServer { message = Just msg }
        }


connect : AuthManager -> Cmd msg
connect auth =
    Connect (chatotWs auth) |> send
