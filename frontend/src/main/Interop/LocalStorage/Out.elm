module Interop.LocalStorage.Out exposing (..)

import TsJson.Encode as TsEncode exposing (required)


type Msg
    = Set { key : String, value : String }
    | Delete String


encoder : TsEncode.Encoder Msg
encoder =
    TsEncode.union
        (\vSet vDelete value ->
            case value of
                Set s ->
                    vSet s

                Delete d ->
                    vDelete d
        )
        |> TsEncode.variantTagged "set" (TsEncode.object [ required "key" .key TsEncode.string, required "value" .value TsEncode.string ])
        |> TsEncode.variantTagged "delete" (TsEncode.object [ required "key" identity TsEncode.string ])
        |> TsEncode.buildUnion
