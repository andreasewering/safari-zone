import { FromElm } from 'Main.elm';

declare global {
  // eslint-disable-next-line @typescript-eslint/consistent-type-definitions
  interface Window {
    safarizoneEvent?: (eventName: string, ...eventArgs: unknown[]) => void;
  }
}

export const handleEventMsg = (
  event: (FromElm & { tag: 'event' })['data']
): void => {
  try {
    // Events are dispatched after a RAF.
    // This is done to avoid race conditions with screenshot tests, where the screenshot happens
    // directly after the event and before Elm rerenders the DOM.
    window.requestAnimationFrame(() => {
      const eventListener = window.safarizoneEvent;
      if (eventListener) {
        eventListener(event.name, ...event.args);
      }
    });
  } catch (e: unknown) {
    console.error('Event listener threw an error', event, e);
  }
};
