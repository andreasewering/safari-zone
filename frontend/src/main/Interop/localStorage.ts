export const handleLocalStorageMsg = (
  msg: (Elm.FromElm & { tag: 'localStorage' })['data']
) => {
  switch (msg.tag) {
    case 'set':
      window.localStorage.setItem(msg.data.key, msg.data.value);
      return;
    case 'delete':
      window.localStorage.removeItem(msg.data.key);
      return;
  }
};
