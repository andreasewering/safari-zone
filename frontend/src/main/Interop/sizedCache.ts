type WithCache<T> = (
  operation: (key: string) => Promise<T>
) => (key: string) => Promise<T>;

/**
 * Generic cache for asynchronous operations with a maximum size.
 * If the maximum size is reached the member in the cache that was requested last
 * will be removed.
 * @param size The maximum size of cached values;
 * @returns a higher order function that adds caching to a given function.
 * The function may be used multiple times but keep in mind this does share the cache instance.
 */
export const createCache = <T>(size: number): WithCache<T> => {
  const cache = new Map<string, T>();
  const lastUsed: string[] = [];
  const pushLastUsed = (key: string) => {
    const usedBefore = lastUsed[lastUsed.length - 1];
    if (usedBefore !== key) {
      lastUsed.push(key);
    }
    if (lastUsed.length > size) {
      const outOfCache = lastUsed.shift();
      if (outOfCache && outOfCache !== key) {
        cache.delete(outOfCache);
      }
    }
  };

  return (operation) => async (key) => {
    pushLastUsed(key);
    const cachedResult = cache.get(key);
    if (cachedResult) {
      return cachedResult;
    }
    const result = operation(key);
    cache.set(key, await result);
    return result;
  };
};
