module Interop.Flags exposing (Flags, decoder, read)

import Auth
import Intl exposing (Intl)
import Json.Decode as D
import Time
import TsJson.Decode as TsDecode
import TsJson.Decode.Pipeline as TsDecode


type alias Flags =
    { language : String
    , preferredColorTheme : Maybe String
    , preferredLanguage : Maybe String
    , reducedMotion : Bool
    , audioOn : Bool
    , x : Int
    , y : Int
    , now : Time.Posix
    , auth : Auth.Authentification
    , intl : Intl
    , version : String
    , fontSize : Int
    }


decoder : TsDecode.Decoder Flags
decoder =
    TsDecode.succeed Flags
        |> TsDecode.required "language" TsDecode.string
        |> TsDecode.required "preferredColorTheme" (TsDecode.nullable TsDecode.string)
        |> TsDecode.required "preferredLanguage" (TsDecode.nullable TsDecode.string)
        |> TsDecode.required "reducedMotion" TsDecode.bool
        |> TsDecode.required "audioOn" TsDecode.bool
        |> TsDecode.required "x" TsDecode.int
        |> TsDecode.required "y" TsDecode.int
        |> TsDecode.required "now" (TsDecode.int |> TsDecode.map Time.millisToPosix)
        |> TsDecode.required "auth" Auth.decodeAuthentification
        |> TsDecode.required "intl" TsDecode.value
        |> TsDecode.required "version" TsDecode.string
        |> TsDecode.required "fontSize" TsDecode.int


read : D.Value -> Result D.Error Flags
read =
    D.decodeValue <| TsDecode.decoder decoder
