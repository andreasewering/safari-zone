export const handleClipboardMsg = (text: string): void => {
  void window.navigator.clipboard.writeText(text);
};
