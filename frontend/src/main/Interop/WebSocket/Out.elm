module Interop.WebSocket.Out exposing (Msg(..), encoder)

import Array exposing (Array)
import Bytes exposing (Bytes)
import Bytes.Decode
import TsJson.Encode as TsEncode exposing (required)


{-| We seperate two concepts here, "url" and "key".

"key" is supposed to uniquely identify a websocket connection.
For example, regardless of the concrete token used for authentication,
two websocket connections to the same websocket server should be considered the same.

"url" is the concrete url that the Typescript side of the port uses to connect
to a websocket server. It should be an absolute path

  - prefixing with <wss://> is taken care of by the Typescript side.

Currently, a "?token=..." query param is used to authenticate the user,
this should be part of the url, but not of the key.

-}
type Msg
    = Connect { url : String, key : String }
    | Disconnect { key : String }
    | Send { key : String, msg : Bytes }


encoder : TsEncode.Encoder Msg
encoder =
    TsEncode.union
        (\vConnect vDisconnect vSend value ->
            case value of
                Connect c ->
                    vConnect c

                Disconnect d ->
                    vDisconnect d

                Send s ->
                    vSend s
        )
        |> TsEncode.variantTagged "connect"
            (TsEncode.object [ required "url" .url TsEncode.string, required "key" .key TsEncode.string ])
        |> TsEncode.variantTagged "disconnect"
            (TsEncode.object [ required "key" .key TsEncode.string ])
        |> TsEncode.variantTagged "send" (TsEncode.object [ required "key" .key TsEncode.string, required "msg" .msg bytesEncoder ])
        |> TsEncode.buildUnion


bytesEncoder : TsEncode.Encoder Bytes
bytesEncoder =
    TsEncode.map
        (\bytes ->
            Bytes.Decode.decode (Bytes.Decode.loop ( Bytes.width bytes, Array.empty ) listStep) bytes
                |> Maybe.withDefault Array.empty
        )
        (TsEncode.array TsEncode.int)


listStep : ( Int, Array Int ) -> Bytes.Decode.Decoder (Bytes.Decode.Step ( Int, Array Int ) (Array Int))
listStep ( n, xs ) =
    if n <= 0 then
        Bytes.Decode.succeed (Bytes.Decode.Done xs)

    else
        Bytes.Decode.map (\x -> Bytes.Decode.Loop ( n - 1, Array.push x xs )) Bytes.Decode.unsignedInt8
