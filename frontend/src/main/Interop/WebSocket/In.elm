module Interop.WebSocket.In exposing (..)

import Bytes exposing (Bytes)
import Bytes.Encode
import TsJson.Decode as TsDecode
import TsJson.Decode.Pipeline as TsDecode


type Msg msg key
    = Connected key
    | Closed key
    | WsError key String
    | Message key msg


type alias DynamicMsg =
    Msg Bytes String


decodeDynamic : (Bytes -> Maybe msg) -> key -> ( String, DynamicMsg ) -> TsDecode.Decoder (Msg msg key)
decodeDynamic messageDecoder key ( stringKey, dynamicMsg ) =
    let
        describeProtoDecodeError context mayDecoded =
            case mayDecoded of
                Just decoded ->
                    TsDecode.succeed decoded

                Nothing ->
                    TsDecode.fail <| "Failed to decode WS " ++ context ++ " of key '" ++ stringKey
    in
    case dynamicMsg of
        Connected _ ->
            TsDecode.succeed <| Connected key

        Closed _ ->
            TsDecode.succeed <| Closed key

        WsError _ err ->
            TsDecode.succeed <| WsError key err

        Message _ msg ->
            messageDecoder msg
                |> describeProtoDecodeError "message"
                |> TsDecode.map (Message key)


tagDecoder : TsDecode.Decoder DynamicMsg
tagDecoder =
    TsDecode.discriminatedUnion "tag"
        [ ( "connected"
          , TsDecode.field "key" TsDecode.string |> TsDecode.map Connected
          )
        , ( "closed", TsDecode.field "key" TsDecode.string |> TsDecode.map Closed )
        , ( "error"
          , TsDecode.succeed WsError
                |> TsDecode.required "error" TsDecode.string
                |> TsDecode.required "key" TsDecode.string
          )
        , ( "msg"
          , TsDecode.succeed Message
                |> TsDecode.required "key" TsDecode.string
                |> TsDecode.required "msg" bytesDecoder
          )
        ]


decoder : TsDecode.Decoder ( String, DynamicMsg )
decoder =
    TsDecode.succeed Tuple.pair
        |> TsDecode.andMap (TsDecode.field "key" TsDecode.string)
        |> TsDecode.andMap tagDecoder


bytesDecoder : TsDecode.Decoder Bytes
bytesDecoder =
    TsDecode.list TsDecode.int
        |> TsDecode.map (List.map Bytes.Encode.unsignedInt8 >> Bytes.Encode.sequence >> Bytes.Encode.encode)
