const arrToBytes = (arr: number[]): Uint8Array => {
  const bytes = new Uint8Array(new ArrayBuffer(arr.length));
  arr.forEach((value, index) => (bytes[index] = value));
  return bytes;
};

const bufferToArr = (buffer: ArrayBuffer): number[] => {
  const bytes = new Uint8Array(buffer);
  const arr = new Array<number>(bytes.length);
  bytes.forEach((value, index) => (arr[index] = value));
  return arr;
};

const connectWs = (path: string) => {
  const hostPath = `${
    window.location.protocol === 'https:' ? 'wss://' : 'ws://'
  }${window.location.host}${path}`;
  return new WebSocket(hostPath);
};

export type WsMsg = (Elm.ToElm & { tag: 'ws' })['data'];

type Send = (msg: WsMsg) => void;

type Receive = (msg: (Elm.FromElm & { tag: 'ws' })['data']) => void;

type SocketState =
  | { tag: 'connected'; socket: WebSocket }
  | { tag: 'connecting' };

export const websocketPort = (send: Send): Receive => {
  const sockets = new Map<string, SocketState>();

  const connect = (msg: { url: string; key: string }) => {
    const socket = connectWs(msg.url);
    socket.binaryType = 'arraybuffer';
    socket.addEventListener('error', (err) => {
      console.error(err);
      send({
        tag: 'error',
        key: msg.key,
        error: err.type,
      });
    });

    socket.addEventListener('open', () => {
      send({
        tag: 'connected',
        key: msg.key,
      });
    });

    socket.addEventListener('message', (event: MessageEvent<ArrayBuffer>) => {
      send({
        tag: 'msg',
        key: msg.key,
        msg: bufferToArr(event.data),
      });
    });

    socket.addEventListener('close', (event) => {
      if (!event.wasClean || event.code !== 1000) {
        console.warn(event);
      }
      send({
        tag: 'closed',
        key: msg.key,
      });
      sockets.delete(msg.url);
    });

    sockets.set(msg.key, { tag: 'connected', socket });
  };

  const getSocket = (url: string) => {
    const s = sockets.get(url);
    return s && s.tag === 'connected' ? s.socket : undefined;
  };

  return (msg) => {
    if (msg.tag === 'connect') {
      getSocket(msg.data.key)?.close(1000, 'Reconnect');
      connect(msg.data);
    }
    if (msg.tag === 'disconnect') {
      getSocket(msg.data.key)?.close(1000, 'Disconnect');
    }
    if (msg.tag === 'send') {
      const socket = getSocket(msg.data.key);
      if (!socket) {
        console.warn('Tried to send message over closed socket', msg.data);
        return;
      }
      socket.send(arrToBytes(msg.data.msg));
    }
  };
};
