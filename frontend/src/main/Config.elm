module Config exposing (..)

import Domain.Coordinate exposing (Coordinate)


tileSize : number
tileSize =
    64


startVelocity : Float
startVelocity =
    7


gravity : Float
gravity =
    9.81


trainerSize : number
trainerSize =
    110


pokemonSpriteSize : Coordinate number
pokemonSpriteSize =
    { x = 32, y = 32 }


pokemonSize : number
pokemonSize =
    90


itemSize : number
itemSize =
    60


trainerSpeed : Float
trainerSpeed =
    0.005


{-| less is faster here
-}
trainerAnimationSpeed : number
trainerAnimationSpeed =
    200
