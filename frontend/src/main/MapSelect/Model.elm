module MapSelect.Model exposing (..)

import Dict
import MapId exposing (MapId)
import Proto.Smeargle as Smeargle
import Protobuf.Types.Int64 as Int64
import StartTileId exposing (StartTileId)
import Tagged exposing (tag)


type alias Startpoint =
    { startTileId : StartTileId
    , x : Int
    , y : Int
    , layerNumber : Int
    , mapId : MapId
    , mapName : String
    }


responseToStartpoints : Smeargle.GetAllStartTilesResponse -> List Startpoint
responseToStartpoints { tiles, maps } =
    let
        tileToStartpoint { startTileId, x, y, layerNumber, mapId } =
            { startTileId = tag startTileId
            , x = x
            , y = y
            , layerNumber = layerNumber
            , mapId = tag mapId
            , mapName =
                Dict.get (Int64.toInts mapId) maps
                    |> Maybe.map .name
                    |> Maybe.withDefault ""
            }
    in
    List.map tileToStartpoint tiles
