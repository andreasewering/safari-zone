-repository-url = https://gitlab.com/andreasewering/safari-zone
-issue-url = { -repository-url }/-/issues/new?issue[title]={ $title }&issue[description]={ $description }

# Generic
backButtonDescription = Zurück
cancelLabel = Abbrechen
incomingFriendRequestAccept = Akzeptieren
incomingFriendRequestDecline = Ablehnen
incomingFriendRequestsHeadline = Eingehende Freundschaftsanfragen
invalidUserIdError = Ungültige Benutzer-ID
navIconDescription = Navigation
outgoingFriendRequestsHeadline = Ausgehende Freundschaftsanfragen
qrcodeGenerationFailed = QR-Code-Erstellung fehlgeschlagen
safariIconLabel = Erkunden
dexIconLabel = Pokédex
settingsIconLabel = Einstellungen
settingsQrCodeScanTitle = QR-Code scannen
settingsQrCodeTitle = Deine Benutzer-ID
settingsReducedMotionDescription = Animationen und Übergänge vereinfachen oder entfernen
settingsReducedMotionLabel = Reduzierte Bewegung
trainerSelectIconLabel = Trainerauswahl
groupsIconLabel = Kontakte
mapEditorIconLabel = Karteneditor
logoutIconLabel = Ausloggen

# Errors
notFoundTitle = Nichts zu finden
notFoundImageDescription = Ein Lahmus
deserializationErrorTitle = Unerwartete Daten vom Server
unauthorizedError = Dein Login ist abgelaufen. Eine neues Token wird angefragt...
websocketConnectionErrorNotification = Verbindung zum Server abgebrochen. Eine neue Verbindung wird aufgebaut...
unexpectedDataImageDescription = Ein Enton mit Kopfschmerzen
urlErrorTitle = Invalide Url angefragt
serverDownTitle = Ein Server ist down
serverDownText = Falls das Problem länger bestehen bleibt, schreibe eine <a href="mailto:support@safari-zone.app">Mail</a> an das Entwicklerteam.
invalidRequestTitle = Der Server hat die Anfrage nicht verstanden.
unknownErrorTitle = Unbekannter Fehler
loadTextureErrorNotification = Konnte Textur nicht laden. Die Anfrage wird automatisiert neu gestartet.
timeoutNotification = Anfrage an Server hat zu lange gedauert. Die Anfrage wird automatisiert neu gestartet.
timeoutImageDescription = Ein Flegmon
offlineNotification = Du scheinst offline zu sein. Verbindung wird wiederhergestellt...
genericErrorText = Das sollte nicht passiert sein. Bitte erstelle ein Ticket <a href="{-issue-url}">hier</a>.
copyErrorButton = Fehlermeldung kopieren
retryButton = Nochmal versuchen

# Items
itemPokeball = Pokéball
itemSuperball = Superball
itemHyperball = Hyperball
itemDiveball = Tauchball
itemQuickball = Flottball

# Chat
chatChannelMap = Kartenweiter Chat
chatChannelPrivate = { $name } (Privat)
chatChannelGroup = { $name } (Gruppe)

chatSenderYou = Du
chatSenderInfo = Info

chatHeaderTitle = Chats
chatHeaderInfoGeneral = Hier kannst du mit anderen Spielern chatten. Nachrichten werden nicht gespeichert, erreichen also nur andere online Spieler. Es gibt einen globalen Chat, sowie private Chats mit deinen Freunden und Gruppen.
chatHeaderInfoGlobal = Nachrichten in diesem Chat erreichen alle anderen online Spieler.
chatHeaderInfoGroup = Nur online Mitglieder dieser Gruppe empfangen die Nachrichten.
chatHeaderInfoPrivate = Nachrichten kommen nur an, wenn {$friendName} auch online ist und dich als Freund hinzugefügt hat.

chatInputPlaceholder = Zum Chatten hier tippen.
chatInputLabel = Hier Chatten 

# Contacts
contactsHeadline = Kontakte

# Dex
dexAppraisalTitle = Pokédex Bewertung
dexAppraisalProfessorImageDescription = Professor Eich
dexAppraisalLoading = Dein Pokédex ist noch am Laden. Wie wäre es mit einer Tasse Tee?
dexAppraisalError = Hmm, dein Pokédex scheint kaputt zu sein. Lass mich versuchen ihn zu reparieren...
dexAppraisal0 = Jede Reise hat einen Anfang. Genieße es die Welt der Pokémon zu entdecken!
dexAppraisal1 = Du hast deine ersten Pokémon gefangen, gut gemacht. Aber es gibt noch viel zu entdecken!
dexAppraisal2 = Du machst ganz schön schnelle Fortschritte! Wusstest du dass sich viele Pokémon entwickeln können?
dexAppraisal3 = Das ist eine beeindruckende Sammlung von Pokémon. Danke für deine harte Arbeit.
dexAppraisal4 = Außerordentlich! Du hast einige der seltensten Pokémon dieses Planeten gefangen.
dexAppraisal5 = Ich kann meinen Augen kaum glauben! Ein vollständiger Pokédex! Ich habe nie damit gerechnet mein Lebenswerk zu vollenden zu können. Ich danke dir von ganzem Herzen.
dexAppraisalHeadline = Professor Eichs Bewertung

dexDetailTitle = Dex: { $pokemonName }
dexDetailTitleFallback = Unbekanntes Pokémon
dexDetailLoadingTitle = Lade Dexeintrag...
dexDetailHeightTitle = Größe
dexDetailHeightFormat = { NUMBER($height, maximumFractionDigits: 2) } m
dexDetailWeightTitle = Gewicht
dexDetailWeightFormat = { NUMBER($height, maximumFractionDigits: 2) } kg
dexDetailCaught = Gefangen
dexDetailShiny = Schillernd
dexDetailChangePartner = Als Partner wählen
dexDetailNext = Zum nächsten Dexeintrag
dexDetailPrevious = Zum vorigen Dexeintrag
# Pokemon Types
pokemonTypeBug = Käfer
pokemonTypeDragon = Drache
pokemonTypeElectric = Elektro
pokemonTypeFairy = Fee
pokemonTypeFighting = Kampf
pokemonTypeFire = Feuer
pokemonTypeFlying = Flug
pokemonTypeGhost = Geist
pokemonTypeGrass = Pflanze
pokemonTypeGround = Boden
pokemonTypeIce = Eis
pokemonTypeNormal = Normal
pokemonTypePoison = Gift
pokemonTypePsychic = Psycho
pokemonTypeRock = Gestein
pokemonTypeSteel = Stahl
pokemonTypeWater = Wasser

dexOverviewHeadline = Pokédex
dexOverviewTitle = Pokédex

# Friends
friendsHeadline = Deine Freunde
friendsAddUserLabel = User-ID
friendsScanUserLabel = Scan QR-Code
friendsGoToChatDescription = Mit {$friendName} chatten

# Groups
groupsDetailTitle = {$groupName}
groupsDetailLoadingTitle = Lade Gruppe...
groupsDetailHeadline = Gruppeninformationen für {$groupName}
groupsDetailRolesAdmin = Administrator
groupsDetailRolesCreator = Gründer
groupsDetailFilterLabel = Suchbegriff
groupsDetailAddUserLabel = User-ID
groupsDetailAddUserSubmit = User hinzufügen

groupsOverviewCreate = Neue Gruppe erstellen
groupsOverviewNameInputLabel = Gruppenname
groupsOverviewHeadline = Deine Gruppen
groupsOverviewNoGroupsMessage = Keine Gruppen gefunden. Möchtest du eine Neue erstellen?

# Map Select
mapSelectTitle = Startpunkt-Auswahl
mapSelectHeadline = Wo soll dein Abenteuer starten?
mapSelectConfirmationText = Möchtest du in { $mapName } beginnen? 
mapSelectConfirmationAccept = Auf geht's! 

# Pokemon Select
pokemonSelectTitle = Pokémonauswahl
pokemonSelectHeadline = Wähle deinen Partner
pokemonSelectIntroduction = Es ist gefährlich in der Wildnis!
  Du solltest einen Partner für deine Reise auswählen. Er wird dich stets begleiten
  und dein erstes Pokémon sein.
pokemonSelectSelectionHeadline = Welches dieser Pokémon möchtest du auf deine Reise mitnehmen?
pokemonSelectSelectionButton = Ich wähle { $name }!

# Safari
safariTitle = Erkunde die Welt
safariBagDescription = Rucksack

# Settings
settingsTitle = Einstellungen
settingsHeadline = {$username}'s Einstellungen
settingsUserIdLabel = Deine User-ID
settingsUserIdDescription = Freunde können dich anhand deiner User-ID Gruppen hinzufügen oder mit dir chatten.
settingsUserIdCopy = Kopieren
settingsColorThemeLabel = Farbschema
settingsColorThemeDescription = Wähle zwischen hellem und dunklen Modus.
settingsColorThemeCurrent = Aktuelles Farbschema
settingsColorThemeLight = Hell
settingsColorThemeDark = Dunkel
settingsTrainerLabel = Aktueller Trainersprite
settingsTrainerSpriteDescription = So sieht dein Avatar aus.
settingsTrainerChange = Ändern
settingsLanguageLabel = Aktuelle Sprache
settingsLanguageDescription = In dieser Sprache werden alle Inhalte dieser Seite angezeigt.
settingsLanguageGerman = Deutsch
settingsLanguageEnglish = Englisch
settingsLanguageFrench = Französisch
settingsLanguageReset = Auf Browser-Sprache zurücksetzen
settingsMusicLabel = Hintergrundmusik
settingsMusicDescription = Konfiguration von Musik und Sound-Effekten 
settingsMusicPlay = Musik spielen
settingsMusicStop = Musik stoppen

# Trainer Select
trainerSelectDetailTitle = Trainerdetails
trainerSelectDetailLoadingTitle = Lade Trainer...
trainerSelectDetailErrorTitle = Konnte Trainer nicht laden
trainerSelectDetailSubmit = Auswahl bestätigen

trainerSelectOverviewTitle = Trainer auswählen
