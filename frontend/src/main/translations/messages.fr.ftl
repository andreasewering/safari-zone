-repository-url = https://gitlab.com/andreasewering/safari-zone
-issue-url = { -repository-url }/-/issues/new?issue[title]={ $title }&issue[description]={ $description }

# Générique
backButtonDescription = Retour
cancelLabel = Annuler
incomingFriendRequestAccept = Accepte
incomingFriendRequestDecline = Déclin
incomingFriendRequestsHeadline = Demandes d'ami en attente
invalidUserIdError = ID utilisateur invalide
navIconDescription = Navigation
outgoingFriendRequestsHeadline = Demandes d'amis envoyées
qrcodeGenerationFailed = Échec de la génération du code QR
safariIconLabel = Découvrir
dexIconLabel = Pokédex
settingsIconLabel = Paramètres
settingsQrCodeScanTitle = Scan de code QR
settingsQrCodeTitle = Ton identifiant utilisateur
settingsReducedMotionDescription = Simplifie ou enlève les animations et transitions
settingsReducedMotionLabel = Mouvement réduit
trainerSelectIconLabel = Sélection du dresseur
groupsIconLabel = Contacts
mapEditorIconLabel = Éditeur de carte
logoutIconLabel = Déconnexion

# Erreurs
notFoundTitle = Non trouvé
notFoundImageDescription = Un flagadoss
deserializationErrorTitle = Données inattendues du serveur
unauthorizedError = Votre connexion a expiré. Tentative d'obtenir un nouveau jeton d'authentification...
websocketConnectionErrorNotification = Connexion au serveur perdue. Tentative de reconnexion...
unexpectedDataImageDescription = Un psykokwak avec un mal de tête
urlErrorTitle = URL demandée invalide
serverDownTitle = Un serveur est en panne
serverDownText = Si le problème persiste, veuillez écrire un <a href="mailto:support@safari-zone.app">mail</a> à l'équipe de développement.
invalidRequestTitle = Le serveur n'a pas compris une requête.
unknownErrorTitle = Erreur inconnue
loadTextureErrorNotification = Échec du chargement de la texture. La requête sera réessayée automatiquement.
timeoutNotification = La requête au serveur a expiré. La requête sera réessayée automatiquement.
timeoutImageDescription = Un ramoloss
offlineNotification = Vous semblez être hors ligne. Tentative de reconnexion...
genericErrorText = Cela ne devrait pas être arrivé. Veuillez ouvrir un problème <a href="{-issue-url}">ici</a>.
copyErrorButton = Copier le message d'erreur
retryButton = Réessayer

# Objets
itemPokeball = Poké Ball
itemSuperball = Super Ball
itemHyperball = Hyper Ball
itemDiveball = Scuba Ball
itemQuickball = Rapide Ball

# Chat
chatChannelMap = Chat pour cette carte
chatChannelPrivate = {$name} (privé)
chatChannelGroup = {$name} (groupe)

chatSenderYou = Toi
chatSenderInfo = Info

chatHeaderTitle = Chats
chatHeaderInfoGeneral = Vous pouvez discuter ici avec d'autres joueurs. Les messages ne sont pas conservés, ils ne parviennent donc qu'aux autres joueurs en ligne. Il y a un chat global ainsi que des chats privés avec vos groupes et amis.
chatHeaderInfoGlobal = Les messages dans ce chat sont envoyés à tous les autres joueurs en ligne.
chatHeaderInfoGroup = Les messages ne sont envoyés qu'aux membres en ligne de ce groupe.
chatHeaderInfoPrivate = Pour que {$friendName} reçoive vos messages, il/elle doit être en ligne et vous avoir ajouté en tant qu'ami.

chatInputPlaceholder = Tapez ici pour discuter.
chatInputLabel = Discutez ici  

# Contacts
contactsHeadline = Contacts

# Dex
dexAppraisalTitle = Évaluation du Pokédex
dexAppraisalProfessorImageDescription = Professeur Chen
dexAppraisalLoading = Votre Pokédex est encore en cours de chargement. Que diriez-vous d'une tasse de thé ?
dexAppraisalError = Hmm, votre Pokédex semble être cassé. Laissez-moi essayer de le réparer...
dexAppraisal0 = Chaque voyage a un début. Profitez de l'exploration du monde des Pokémon !
dexAppraisal1 = Bon travail pour attraper ces Pokémon, mais il reste beaucoup à explorer.
dexAppraisal2 = Vous progressez bien ! Saviez-vous que de nombreux Pokémon peuvent évoluer ?
dexAppraisal3 = Voilà une collection impressionnante de Pokémon. Merci pour votre travail acharné.
dexAppraisal4 = Extraordinaire ! Vous avez réussi à attraper certains des Pokémon les plus rares de la planète.
dexAppraisal5 = Je n'en crois pas mes yeux ! C'est un Pokédex complet ! Je n'aurais jamais pensé voir le jour où je terminerais le travail de ma vie. Je vous remercie sincèrement du fond du cœur.
dexAppraisalHeadline = Évaluation du Professeur Chen

dexDetailTitle = Dex : { $pokemonName }
dexDetailTitleFallback = Pokémon inconnu
dexDetailLoadingTitle = Chargement de l'entrée Dex...
dexDetailHeightTitle = Taille
dexDetailHeightFormat = { NUMBER($height, maximumFractionDigits: 2) } m
dexDetailWeightTitle = Poids
dexDetailWeightFormat = { NUMBER($height, maximumFractionDigits: 2) } kg
dexDetailCaught = Capturé
dexDetailShiny = Brillant
dexDetailChangePartner = Choisir comme partenaire
dexDetailNext = Vers l'entrée Dex suivante
dexDetailPrevious = Vers l'entrée Dex précédente

# Types de Pokémon
pokemonTypeBug = Insecte
pokemonTypeDragon = Dragon
pokemonTypeElectric = Électrique
pokemonTypeFairy = Fée
pokemonTypeFighting = Combat
pokemonTypeFire = Feu
pokemonTypeFlying = Vol
pokemonTypeGhost = Spectre
pokemonTypeGrass = Plante
pokemonTypeGround = Sol
pokemonTypeIce = Glace
pokemonTypeNormal = Normal
pokemonTypePoison = Poison
pokemonTypePsychic = Psy
pokemonTypeRock = Roche
pokemonTypeSteel = Acier
pokemonTypeWater = Eau

dexOverviewHeadline = Pokédex
dexOverviewTitle = Pokédex

# Amis
friendsHeadline = Tes amis
friendsAddUserLabel = ID utilisateur
friendsScanUserLabel = Scannez le code QR
friendsGoToChatDescription = Discuter avec {$friendName}

# Groupes
groupsDetailTitle = {$groupName}
groupsDetailLoadingTitle = Chargement du groupe...
groupsDetailHeadline = Configuration de {$groupName}
groupsDetailRolesAdmin = Admin
groupsDetailRolesCreator = Fondateur
groupsDetailFilterLabel = Requête
groupsDetailAddUserLabel = ID utilisateur
groupsDetailAddUserSubmit = Ajouter un utilisateur

groupsOverviewCreate = Créer un nouveau groupe
groupsOverviewNameInputLabel = Nom du groupe
groupsOverviewHeadline = Tes groupes
groupsOverviewNoGroupsMessage = Aucun groupe trouvé. Tu veux en créer un nouveau ?

# Sélection de carte
mapSelectTitle = Sélection du point de départ
mapSelectHeadline = Où commence ton aventure ?
mapSelectConfirmationText = Veux-tu commencer sur { $mapName } ? 
mapSelectConfirmationAccept = Allons-y !

# Sélection de Pokémon
pokemonSelectTitle = Sélection de Pokémon
pokemonSelectHeadline = Sélectionne ton partenaire
pokemonSelectIntroduction = C'est dangereux dans la nature !
  Tu devrais prendre un partenaire avec toi dans ton voyage. Il sera avec toi à tout moment
  et sera ton premier Pokémon.
pokemonSelectSelectionHeadline = Lequel de ces Pokémon veux-tu emmener avec toi ?
pokemonSelectSelectionButton = Je choisis { $name } !

# Safari
safariTitle = Découvrir le monde
safariBagDescription = Sac à dos

# Settings
settingsTitle = Paramètres
settingsHeadline = Paramètres de {$username}
settingsUserIdLabel = Ton ID utilisateur
settingsUserIdDescription = Les amis peuvent utiliser l'ID utilisateur pour t'ajouter à des groupes ou discuter avec toi.
settingsUserIdCopy = Copier
settingsColorThemeLabel = Thème de couleur
settingsColorThemeDescription = Choisis entre le mode clair et le mode sombre.
settingsColorThemeCurrent = Thème actuel
settingsColorThemeLight = Clair
settingsColorThemeDark = Sombre
settingsTrainerLabel = Sprite du dresseur actuel
settingsTrainerSpriteDescription = Voici à quoi ressemble ton avatar.
settingsTrainerChange = Changer
settingsLanguageLabel = Langue actuelle
settingsLanguageDescription = La langue dans laquelle tout le contenu de cette page est affiché.
settingsLanguageGerman = Allemand
settingsLanguageEnglish = Anglais
settingsLanguageFrench = Français
settingsLanguageReset = Réinitialiser à la langue par défaut du navigateur
settingsMusicLabel = Musique de fond
settingsMusicDescription = Configuration de la musique et des effets sonores 
settingsMusicPlay = Jouer la musique
settingsMusicStop = Arrêter la musique

# Sélection du dresseur
trainerSelectDetailTitle = Détail du dresseur
trainerSelectDetailLoadingTitle = Chargement du dresseur...
trainerSelectDetailErrorTitle = Impossible de charger les dresseurs
trainerSelectDetailSubmit = Confirmer le choix

trainerSelectOverviewTitle = Choisir le dresseur
