-repository-url = https://gitlab.com/andreasewering/safari-zone
-issue-url = { -repository-url }/-/issues/new?issue[title]={ $title }&issue[description]={ $description }

# Generic
backButtonDescription = Back
cancelLabel = Cancel
incomingFriendRequestAccept = Accept
incomingFriendRequestDecline = Decline
incomingFriendRequestsHeadline = Incoming friend requests
invalidUserIdError = Invalid user id
navIconDescription = Navigation
outgoingFriendRequestsHeadline = Outgoing friend requests
qrcodeGenerationFailed = QR Code generation failed
safariIconLabel = Discover
dexIconLabel = Pokédex
settingsIconLabel = Settings
settingsQrCodeScanTitle = Scanning QR Code
settingsQrCodeTitle = Your user id
settingsReducedMotionDescription = Simplify or remove animations and transitions
settingsReducedMotionLabel = Reduced Motion
trainerSelectIconLabel = Trainer Selection
groupsIconLabel = Contacts
mapEditorIconLabel = Map Editor
logoutIconLabel = Logout

# Errors
notFoundTitle = Not found
notFoundImageDescription = A slowbro
deserializationErrorTitle = Unexpected data from server
unauthorizedError = Your login has expired. Trying to get a new authentication token...
websocketConnectionErrorNotification = Lost connection to server. Trying to reconnect...
unexpectedDataImageDescription = A psyduck with a headache
urlErrorTitle = Requested invalid url
serverDownTitle = A server is down
serverDownText = If the problem persists, please write a <a href="mailto:support@safari-zone.app">mail</a> to the development team.
invalidRequestTitle = The server failed to understand a request.
unknownErrorTitle = Unknown error
loadTextureErrorNotification = Failed to load texture. The request will be retried automatically.
timeoutNotification = Request to server timed out. The request will be retried automatically.
timeoutImageDescription = A slowpoke
offlineNotification = You appear to be offline. Trying to reconnect...
genericErrorText = This shouldn't have happened. Please open an issue <a href="{-issue-url}">here</a>.
copyErrorButton = Copy error message
retryButton = Try again# Items

# Items
itemPokeball = Poké Ball
itemSuperball = Great Ball
itemHyperball = Ultra Ball
itemDiveball = Dive Ball
itemQuickball = Quick Ball

# Chat
chatChannelMap = Chat for this map
chatChannelPrivate = {$name} (private)
chatChannelGroup = {$name} (group)

chatSenderYou = You
chatSenderInfo = Info

chatHeaderTitle = Chats
chatHeaderInfoGeneral = You may chat here with other players. Messages are not persisted, so they only reach other online players. There is a global chat as well as private chats with your groups and friends.
chatHeaderInfoGlobal = Messages in this chat are sent to all other online players.
chatHeaderInfoGroup = Messages are only sent to online members of this group.
chatHeaderInfoPrivate = In order for {$friendName} to receive your messages, he/she needs to be online and has you added as their friend.

chatInputPlaceholder = Type here to chat.
chatInputLabel = Chat here  

# Contacts
contactsHeadline = Contacts

# Dex
dexAppraisalTitle = Pokédex appraisal
dexAppraisalProfessorImageDescription = Professor Oak
dexAppraisalLoading = Your Pokédex is still loading. How about a cup of tea?
dexAppraisalError = Hmm, your Pokédex seems to be broken. Let me try to repair it...
dexAppraisal0 = Every journey has a start. Enjoy exploring the world of Pokémon!
dexAppraisal1 = Good job catching those Pokémon but there is a lot left to explore.
dexAppraisal2 = You are making quite the progress! Did you know that many Pokémon can evolve?
dexAppraisal3 = Now that is an impressive collection of Pokémon. Thank you for your hard work.
dexAppraisal4 = Extraordinary! You managed to catch some of the rarest Pokémon on the planet.
dexAppraisal5 = I cannot believe my eyes! This is a completed Pokédex! I never thought I'd see the day of completing my lifes work. I sincerely thank you from the bottom of my heart.
dexAppraisalHeadline = Professor Oaks Appraisal

dexDetailTitle = Dex: { $pokemonName }
dexDetailTitleFallback = Unknown pokémon
dexDetailLoadingTitle = Loading Dex Entry...
dexDetailHeightTitle = Height
dexDetailHeightFormat = { NUMBER($height, maximumFractionDigits: 2) } m
dexDetailWeightTitle = Weight
dexDetailWeightFormat = { NUMBER($height, maximumFractionDigits: 2) } kg
dexDetailCaught = Caught
dexDetailShiny = Shiny
dexDetailChangePartner = Choose as partner
dexDetailNext = To next dex entry
dexDetailPrevious = To previous dex entry
# Pokemon types
pokemonTypeBug = Bug
pokemonTypeDragon = Dragon
pokemonTypeElectric = Electric
pokemonTypeFairy = Fairy
pokemonTypeFighting = Fighting
pokemonTypeFire = Fire
pokemonTypeFlying = Flying
pokemonTypeGhost = Ghost
pokemonTypeGrass = Grass
pokemonTypeGround = Ground
pokemonTypeIce = Ice
pokemonTypeNormal = Normal
pokemonTypePoison = Poison
pokemonTypePsychic = Psychic
pokemonTypeRock = Rock
pokemonTypeSteel = Steel
pokemonTypeWater = Water

dexOverviewHeadline = Pokédex
dexOverviewTitle = Pokédex

# Friends
friendsHeadline = Your friends
friendsAddUserLabel = User-ID
friendsScanUserLabel = Scan QR code
friendsGoToChatDescription = Chat with {$friendName}

# Groups
groupsDetailTitle = {$groupName}
groupsDetailLoadingTitle = Loading Group...
groupsDetailHeadline = {$groupName} configuration
groupsDetailRolesAdmin = Admin
groupsDetailRolesCreator = Founder
groupsDetailFilterLabel = Query
groupsDetailAddUserLabel = User-ID
groupsDetailAddUserSubmit = Add user

groupsOverviewCreate = Create new group
groupsOverviewNameInputLabel = Group name
groupsOverviewHeadline = Your groups
groupsOverviewNoGroupsMessage = No groups found. Wanna create a new one?

# Map Select
mapSelectTitle = Starting point selection
mapSelectHeadline = Where does your adventure start?
mapSelectConfirmationText = Do you want to start on { $mapName }? 
mapSelectConfirmationAccept = Let's go!

# Pokemon Select
pokemonSelectTitle = Pokémon Selection
pokemonSelectHeadline = Select your partner
pokemonSelectIntroduction = It is dangerous in the wild!
  You should take a partner with you on your journey. It will be with you at all times
  and be your first Pokémon.
pokemonSelectSelectionHeadline = Which of these Pokémon do you want to take with you?
pokemonSelectSelectionButton = I choose { $name }!

# Safari
safariTitle = Discover the world
safariBagDescription = Backpack

# Settings
settingsTitle = Settings
settingsHeadline = {$username}'s settings
settingsUserIdLabel = Your User-ID
settingsUserIdDescription = Friends can use the User-ID to add you to groups or chat with you.
settingsUserIdCopy = Copy
settingsColorThemeLabel = Color theme
settingsColorThemeDescription = Choose between light and dark mode.
settingsColorThemeCurrent = Current theme
settingsColorThemeLight = Light
settingsColorThemeDark = Dark
settingsTrainerLabel = Current trainer sprite
settingsTrainerSpriteDescription = This is how your avatar looks.
settingsTrainerChange = Change
settingsLanguageLabel = Current language
settingsLanguageDescription = The language in which all content of this page is displayed.
settingsLanguageGerman = German
settingsLanguageEnglish = English
settingsLanguageFrench = French
settingsLanguageReset = Reset to browser default
settingsMusicLabel = Background music
settingsMusicDescription = Configuration of music and sound effects 
settingsMusicPlay = Play music
settingsMusicStop = Stop music

# Trainer Select
trainerSelectDetailTitle = Trainer Detail
trainerSelectDetailLoadingTitle = Loading Trainer...
trainerSelectDetailErrorTitle = Could not load trainers
trainerSelectDetailSubmit = Confirm choice

trainerSelectOverviewTitle = Choose trainer
