module Shared.Background exposing (Background, BackgroundImage, contrastingBorder, contrastingMarkerClass, contrastingText, fromTime, setTimezone, toImage, toUrl)

import Html.WithContext
import Html.WithContext.Attributes exposing (class)
import Time


type BackgroundImage
    = Morning
    | Day
    | Dusk
    | Night


type Background
    = Background
        { image : BackgroundImage
        , time : Time.Posix
        }


fromTime : Time.Posix -> Time.Zone -> Background
fromTime time timezone =
    let
        hour =
            Time.toHour timezone time

        image =
            if hour >= 5 && hour <= 10 then
                Morning

            else if hour >= 11 && hour <= 16 then
                Day

            else if hour >= 17 && hour <= 22 then
                Dusk

            else
                Night
    in
    Background { image = image, time = time }


setTimezone : Time.Zone -> Background -> Background
setTimezone zone (Background { time }) =
    fromTime time zone


toUrl : Background -> String
toUrl =
    toImage >> imageToUrl


toImage : Background -> BackgroundImage
toImage (Background { image }) =
    image


imageToUrl : BackgroundImage -> String
imageToUrl image =
    let
        urlSuffix =
            case image of
                Morning ->
                    "morning"

                Day ->
                    "day"

                Dusk ->
                    "dusk"

                Night ->
                    "night"
    in
    "/images/safari-zone-" ++ urlSuffix ++ ".png"


contrastingMarkerClass : Html.WithContext.Attribute { ctx | background : BackgroundImage } msg
contrastingMarkerClass =
    Html.WithContext.withContextAttribute
        (\{ background } ->
            case background of
                Morning ->
                    class "bgl"

                Day ->
                    class "bgl"

                Dusk ->
                    class "bgd"

                Night ->
                    class "bgd"
        )


contrastingText : Html.WithContext.Attribute { ctx | background : BackgroundImage } msg
contrastingText =
    Html.WithContext.withContextAttribute
        (\{ background } ->
            case background of
                Morning ->
                    class "text-black"

                Day ->
                    class "text-black"

                Dusk ->
                    class "text-white"

                Night ->
                    class "text-white"
        )


contrastingBorder : Html.WithContext.Attribute { ctx | background : BackgroundImage } msg
contrastingBorder =
    Html.WithContext.withContextAttribute
        (\{ background } ->
            case background of
                Morning ->
                    class "border-black"

                Day ->
                    class "border-black"

                Dusk ->
                    class "border-white"

                Night ->
                    class "border-white"
        )
