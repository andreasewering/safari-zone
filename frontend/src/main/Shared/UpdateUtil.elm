module Shared.UpdateUtil exposing (chain, liftCmd, liftModel, maybe, none, when, withCmds)

import Lens exposing (Lens)


none : s -> ( s, Cmd msg )
none s =
    ( s, Cmd.none )


chain : List (s -> ( s, Cmd msg )) -> s -> ( s, Cmd msg )
chain effects session =
    let
        go : (s -> ( s, Cmd msg )) -> ( s, List (Cmd msg) ) -> ( s, List (Cmd msg) )
        go f ( s, cmds ) =
            f s |> Tuple.mapSecond (\cmd -> cmd :: cmds)
    in
    List.foldl go ( session, [] ) effects
        |> Tuple.mapSecond Cmd.batch


withCmds : List (Cmd msg) -> (s -> ( s, Cmd msg )) -> s -> ( s, Cmd msg )
withCmds cmds f s =
    let
        ( s2, cmd ) =
            f s
    in
    ( s2, Cmd.batch (cmd :: cmds) )


liftCmd : (msg -> msg2) -> (s -> ( s, Cmd msg )) -> s -> ( s, Cmd msg2 )
liftCmd lift f s =
    let
        ( s2, cmd ) =
            f s
    in
    ( s2, Cmd.map lift cmd )


liftModel : Lens s m -> (s -> ( s, Cmd msg )) -> m -> ( m, Cmd msg )
liftModel lens f m =
    let
        ( s, cmd ) =
            f (lens.get m)
    in
    ( lens.set s m, cmd )


maybe : (a -> s -> ( s, Cmd msg )) -> Maybe a -> s -> ( s, Cmd msg )
maybe f mayA =
    Maybe.map f mayA |> Maybe.withDefault none


when : Bool -> (s -> ( s, Cmd msg )) -> s -> ( s, Cmd msg )
when bool =
    if bool then
        identity

    else
        always none
