module Shared.Bezier exposing (cubic)

import Basics exposing (Float)


{-| Cubic bezier curve formula - see <https://cubic-bezier.com> how this function looks
-}
cubic : ( Float, Float ) -> (Float -> Float)
cubic ( x0, x1 ) t =
    3 * (1 - t) ^ 2 * t * x0 + 3 * (1 - t) * t ^ 2 * x1 + t ^ 3



-- 3 * (1 - t)^2 * t * x1 + 3 * (1 - t)^2 * x2 + t^3 * 1;
