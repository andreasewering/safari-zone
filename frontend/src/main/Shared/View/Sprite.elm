module Shared.View.Sprite exposing (view)

import Html.WithContext exposing (Attribute, Html)
import Html.WithContext.Attributes exposing (class, style, title)


view :
    List (Attribute { ctx | fontSize : Int } msg)
    -> { row : Int, column : Int, columns : Int, spriteSize : Int, displaySize : Float }
    -> { displayName : String, imagePath : String }
    -> Html { ctx | fontSize : Int } msg
view extraAttrs { row, column, columns, spriteSize, displaySize } { displayName, imagePath } =
    let
        posX =
            -row * spriteSize

        posY =
            -column * spriteSize
    in
    Html.WithContext.div
        ([ class "flex items-center justify-center"
         , style "width" (String.fromFloat displaySize ++ "rem")
         , style "height" (String.fromFloat displaySize ++ "rem")
         , style "background-size" (String.fromInt (columns * 100) ++ "%")
         , style "background-image" ("url(" ++ imagePath ++ ")")
         , style "background-position" (String.fromInt posX ++ "px" ++ " " ++ String.fromInt posY ++ "px")
         , title displayName
         ]
            ++ extraAttrs
        )
        []
