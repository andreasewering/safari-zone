module Shared.View.PokemonImage exposing (ImageVariation(..), pokedexId, showImageHtml, showPokedex)

import Array exposing (Array)
import Atoms.Html
import Html.WithContext as Html exposing (Attribute, Html, img, li)
import Html.WithContext.Attributes exposing (alt, class, id, src)
import PokemonNumber exposing (PokemonNumber)
import Proto.Kangaskhan exposing (DexEntry)
import Tagged exposing (tag, untag)


type alias Displayable a =
    { a | name : String, pokemonNumber : PokemonNumber }


type ImageVariation
    = Shiny
    | Normal
    | Missing


imageSrc : ImageVariation -> PokemonNumber -> String
imageSrc v pokemonNumber =
    "/images/dex/"
        ++ (case v of
                Shiny ->
                    "shiny"

                Normal ->
                    "normal"

                Missing ->
                    "missing"
           )
        ++ "/"
        ++ String.fromInt (untag pokemonNumber)
        ++ ".png"


showImageHtml : List (Attribute ctx msg) -> Displayable a -> ImageVariation -> Html ctx msg
showImageHtml attributes { pokemonNumber, name } variation =
    img ([ src <| imageSrc variation pokemonNumber, alt name ] ++ attributes) []


showNumber : Int -> Html ctx msg
showNumber number =
    li [ class "flex h-24 w-24 items-center justify-center bg-gray-200/80 text-gray-700 dark:bg-gray-700/80 dark:text-gray-200" ]
        [ Html.text (String.fromInt number) ]


show : PokedexAttributes { ctx | disabled : Bool } msg -> DexEntry -> Html { ctx | disabled : Bool } msg
show { pokemonAttributes } dexEntry =
    let
        isShiny =
            dexEntry.amountShiny > 0

        variation =
            if isShiny then
                Shiny

            else
                Normal

        imageStyles =
            if isShiny then
                class "h-24 w-24 bg-yellow-300 dark:bg-yellow-600"

            else
                class "h-24 w-24 bg-green-300 dark:bg-green-600"
    in
    li []
        [ Atoms.Html.a
            (pokemonAttributes dexEntry)
            [ img
                [ src <| imageSrc variation (tag dexEntry.number)
                , alt dexEntry.pokemonName
                , imageStyles
                ]
                []
            ]
        ]


type alias PokedexAttributes ctx msg =
    { pokemonAttributes : DexEntry -> List (Attribute ctx msg)
    , missingEntryAttributes : List (Attribute ctx msg)
    }


pokedexId : String
pokedexId =
    "pokedex"


showPokedex : PokedexAttributes { ctx | disabled : Bool } msg -> Array DexEntry -> Html { ctx | disabled : Bool } msg
showPokedex attrs entries =
    entries
        |> Array.toList
        |> List.sortBy .number
        |> renderEntries attrs
        |> Html.ul [ class "pokedex-width flex flex-wrap overflow-y-scroll rounded-lg", id pokedexId ]


renderEntries :
    PokedexAttributes { ctx | disabled : Bool } msg
    -> List DexEntry
    -> List (Html { ctx | disabled : Bool } msg)
renderEntries attrs entries =
    let
        go n entrs =
            case entrs of
                [] ->
                    List.range n 151 |> List.map showNumber

                entry :: rest ->
                    (List.range n (entry.number - 1)
                        |> List.map showNumber
                    )
                        ++ show attrs entry
                        :: go (entry.number + 1) rest
    in
    go 1 entries
