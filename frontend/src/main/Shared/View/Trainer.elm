module Shared.View.Trainer exposing (trainerSpriteHtml)

import Html.WithContext exposing (Html)
import Shared.Types.TrainerSprite as TrainerSprite
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import Shared.View.Sprite as Sprite
import TrainerNumber exposing (TrainerNumber)


trainerSpriteHtml : { displaySize : Float } -> TrainerState -> { d | displayName : String, trainerNumber : TrainerNumber } -> Html { ctx | fontSize : Int } msg
trainerSpriteHtml { displaySize } state trainer =
    let
        index =
            TrainerState.stateToIndex state
    in
    Sprite.view []
        { row = indexToOffsetX index, column = indexToOffsetY index, spriteSize = 64, displaySize = displaySize, columns = 4 }
        { imagePath = TrainerSprite.trainerNumberToPath trainer.trainerNumber, displayName = trainer.displayName }


indexToOffsetX : Int -> Int
indexToOffsetX =
    modBy 4


indexToOffsetY : Int -> Int
indexToOffsetY i =
    clamp 0 3 <| i // 4
