module Shared.View.Theme exposing (..)


type Name
    = Light
    | Dark


themes : List Name
themes =
    [ Light, Dark ]


toString : Name -> String
toString theme =
    case theme of
        Dark ->
            "dark"

        Light ->
            "light"


fromString : String -> Maybe Name
fromString str =
    case str of
        "dark" ->
            Just Dark

        "light" ->
            Just Light

        _ ->
            Nothing
