module Shared.View.Icon exposing (iconHtml, iconHtmlWith)

import Html.WithContext
import Html.WithContext.Attributes


iconHtml :
    { name : String, height : Float, width : Float, description : i18n -> String }
    -> Html.WithContext.Html { ctx | i18n : i18n, fontSize : Int } msg
iconHtml options =
    iconHtmlWith options []


iconHtmlWith :
    { name : String, height : Float, width : Float, description : i18n -> String }
    -> List (Html.WithContext.Attribute { ctx | i18n : i18n, fontSize : Int } msg)
    -> Html.WithContext.Html { ctx | i18n : i18n, fontSize : Int } msg
iconHtmlWith { name, height, width, description } extraAttrs =
    Html.WithContext.withContext <|
        \{ fontSize } ->
            Html.WithContext.img
                ([ Html.WithContext.Attributes.width (round <| toFloat fontSize * width)
                 , Html.WithContext.Attributes.height (round <| toFloat fontSize * height)
                 , Html.WithContext.Attributes.src <| nameToSrc name
                 , Html.WithContext.withContextAttribute <| \{ i18n } -> Html.WithContext.Attributes.alt (description i18n)
                 ]
                    ++ extraAttrs
                )
                []


nameToSrc : String -> String
nameToSrc name =
    "/icons/" ++ name ++ ".svg"
