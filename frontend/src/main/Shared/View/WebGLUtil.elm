module Shared.View.WebGLUtil exposing (..)

import Html exposing (Html)
import Html.Attributes exposing (class)
import WebGL


entitiesToElement : { s | width : Int, height : Int } -> List (Html.Attribute msg) -> List WebGL.Entity -> Html msg
entitiesToElement session attrs =
    -- preserveDrawingBuffer supposedely makes performance worse.
    -- If that even becomes a problem, we can make this dependent on a query param for example (it is needed for integration tests)
    WebGL.toHtmlWith [ WebGL.preserveDrawingBuffer, WebGL.alpha True, WebGL.antialias, WebGL.depth 1 ]
        ([ Html.Attributes.width session.width
         , Html.Attributes.height session.height
         , class "pointer-events-none"
         ]
            ++ attrs
        )
