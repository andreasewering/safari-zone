module Shared.Types.TrainerState exposing (TrainerState, changeDirection, init, move, stateToIndex, stop)

import Config
import Domain.Direction as Direction exposing (Direction)
import Shared.Types.FrameBased as FrameBased exposing (FrameBased)


type alias TrainerState =
    { direction : Direction
    , moving : Bool
    , action : Action
    }


stop : TrainerState -> TrainerState
stop ts =
    { ts | moving = False, action = FrameBased.immediately ts.action Standing1 }


init : TrainerState
init =
    { direction = Direction.D
    , moving = False
    , action = FrameBased.initFrameBased Standing1 rotate Config.trainerAnimationSpeed
    }


move : Float -> TrainerState -> TrainerState
move dt ts =
    { ts
        | moving = True
        , action =
            if ts.moving then
                FrameBased.nextFrame dt ts.action

            else
                FrameBased.immediately ts.action Moving1
    }


changeDirection : Direction -> TrainerState -> TrainerState
changeDirection direction ts =
    { ts | direction = direction }


type alias Action =
    FrameBased CurrentAction


type CurrentAction
    = Standing1
    | Moving1
    | Standing2
    | Moving2


rotate : CurrentAction -> CurrentAction
rotate action =
    case action of
        Standing1 ->
            Moving1

        Moving1 ->
            Standing2

        Standing2 ->
            Moving2

        Moving2 ->
            Standing1


stateToIndex : TrainerState -> Int
stateToIndex { direction, action } =
    case ( direction, FrameBased.current action ) of
        ( Direction.D, Standing1 ) ->
            0

        ( Direction.D, Standing2 ) ->
            0

        ( Direction.D, Moving1 ) ->
            1

        ( Direction.D, Moving2 ) ->
            3

        ( Direction.L, Standing1 ) ->
            4

        ( Direction.L, Standing2 ) ->
            4

        ( Direction.L, Moving1 ) ->
            5

        ( Direction.L, Moving2 ) ->
            7

        ( Direction.R, Standing1 ) ->
            8

        ( Direction.R, Standing2 ) ->
            8

        ( Direction.R, Moving1 ) ->
            9

        ( Direction.R, Moving2 ) ->
            11

        ( Direction.U, Standing1 ) ->
            12

        ( Direction.U, Standing2 ) ->
            12

        ( Direction.U, Moving1 ) ->
            13

        ( Direction.U, Moving2 ) ->
            15
