module Shared.Types.Items exposing
    ( CollectedItems
    , SpawnedItems
    , addSpawnedItem
    , addSpawnedItems
    , collectedItemsFromList
    , collectedItemsToList
    , deserialize
    , empty
    , emptyCollectedItems
    , itemFromProto
    , itemToProto
    , removeSpawnedItem
    , serialize
    , syncItem
    , toDisplaySpriteLocation
    , useCollectedItem
    , values
    )

import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Item exposing (Item(..))
import ItemId exposing (ItemId)
import MapId exposing (MapId)
import Proto.Data.Item


type CollectedItems
    = CollectedItems (Dict String Int)


emptyCollectedItems : CollectedItems
emptyCollectedItems =
    CollectedItems Dict.empty


type alias SpawnedItem =
    { itemId : ItemId
    , item : Item
    , position : Coordinate Float
    , layerNumber : LayerNumber
    , mapId : MapId
    }


type SpawnedItems
    = SpawnedItems (Dict ( Int, Int ) SpawnedItem)


empty : SpawnedItems
empty =
    SpawnedItems Dict.empty


values : SpawnedItems -> List SpawnedItem
values (SpawnedItems items) =
    Dict.values items


addSpawnedItem : SpawnedItem -> SpawnedItems -> SpawnedItems
addSpawnedItem spawnedItem (SpawnedItems items) =
    Dict.insert (ItemId.toComparable spawnedItem.itemId) spawnedItem items |> SpawnedItems


addSpawnedItems : List SpawnedItem -> SpawnedItems -> SpawnedItems
addSpawnedItems items spawnedItems =
    List.foldl addSpawnedItem spawnedItems items


removeSpawnedItem : ItemId -> SpawnedItems -> SpawnedItems
removeSpawnedItem itemId (SpawnedItems items) =
    Dict.remove (ItemId.toComparable itemId) items |> SpawnedItems


collectedItemsFromList : List ( Item, Int ) -> CollectedItems
collectedItemsFromList =
    List.map (Tuple.mapFirst serialize) >> Dict.fromList >> CollectedItems


collectedItemsToList : CollectedItems -> List ( Item, Int )
collectedItemsToList (CollectedItems items) =
    Dict.toList items
        |> List.filterMap
            (\( itemStr, amount ) -> Maybe.map (\item -> ( item, amount )) (deserialize itemStr))
        |> List.filter (\( _, amount ) -> amount > 0)


syncItem : Item -> Int -> CollectedItems -> CollectedItems
syncItem item amount (CollectedItems items) =
    Dict.insert (serialize item) amount items |> CollectedItems


useCollectedItem : Item -> CollectedItems -> CollectedItems
useCollectedItem item (CollectedItems items) =
    Dict.update (serialize item)
        (Maybe.andThen <|
            \amount ->
                case amount of
                    1 ->
                        Nothing

                    n ->
                        Just (n - 1)
        )
        items
        |> CollectedItems


toDisplaySpriteLocation : Float -> Item -> { row : Int, column : Int, spriteSize : Int, displaySize : Float, columns : Int }
toDisplaySpriteLocation displaySize item =
    case item of
        Pokeball ->
            { row = 0, column = 0, spriteSize = 41, displaySize = displaySize, columns = 1 }

        Superball ->
            { row = 0, column = 0, spriteSize = 41, displaySize = displaySize, columns = 1 }

        Hyperball ->
            { row = 0, column = 0, spriteSize = 41, displaySize = displaySize, columns = 1 }

        Diveball ->
            { row = 0, column = 0, spriteSize = 41, displaySize = displaySize, columns = 1 }

        Quickball ->
            { row = 0, column = 0, spriteSize = 41, displaySize = displaySize, columns = 1 }


serialize : Item -> String
serialize item =
    case item of
        Pokeball ->
            "pb"

        Superball ->
            "sb"

        Hyperball ->
            "hb"

        Quickball ->
            "qb"

        Diveball ->
            "db"


deserialize : String -> Maybe Item
deserialize str =
    case str of
        "pb" ->
            Just Pokeball

        "sb" ->
            Just Superball

        "hb" ->
            Just Hyperball

        "qb" ->
            Just Quickball

        "db" ->
            Just Diveball

        _ ->
            Nothing


itemFromProto : Proto.Data.Item.Item -> Maybe Item
itemFromProto item =
    case item of
        Proto.Data.Item.Pokeball ->
            Just Pokeball

        Proto.Data.Item.Superball ->
            Just Superball

        Proto.Data.Item.Hyperball ->
            Just Hyperball

        Proto.Data.Item.Diveball ->
            Just Diveball

        Proto.Data.Item.Quickball ->
            Just Quickball

        Proto.Data.Item.ItemUnrecognized_ _ ->
            Nothing


itemToProto : Item -> Proto.Data.Item.Item
itemToProto item =
    case item of
        Pokeball ->
            Proto.Data.Item.Pokeball

        Superball ->
            Proto.Data.Item.Superball

        Hyperball ->
            Proto.Data.Item.Hyperball

        Diveball ->
            Proto.Data.Item.Diveball

        Quickball ->
            Proto.Data.Item.Quickball
