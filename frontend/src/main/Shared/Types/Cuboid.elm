module Shared.Types.Cuboid exposing (Cuboid, fromCuboids, fromProto, isInCuboid, toCuboids, toLayers, toProto)

import List.NonEmpty as NonEmpty exposing (NonEmpty)
import Proto.Arceus exposing (AreaPart)
import Set exposing (Set)


type alias Cuboid =
    { xMin : Int, xMax : Int, yMin : Int, yMax : Int, zMin : Int, zMax : Int }


toCuboids : Set ( Int, Int, Int ) -> List Cuboid
toCuboids set =
    let
        list =
            Set.toList set
    in
    case NonEmpty.fromList list of
        Just nonEmpty ->
            toLayers nonEmpty |> layersToCuboids |> NonEmpty.reverse |> NonEmpty.toList

        Nothing ->
            []


fromCuboids : List Cuboid -> Set ( Int, Int, Int )
fromCuboids =
    List.map fromCuboid >> List.foldl Set.union Set.empty


fromCuboid : Cuboid -> Set ( Int, Int, Int )
fromCuboid { xMin, xMax, yMin, yMax, zMin, zMax } =
    List.range xMin xMax
        |> List.concatMap (\x -> List.range yMin yMax |> List.map (Tuple.pair x))
        |> List.concatMap (\( x, y ) -> List.range zMin zMax |> List.map (\z -> ( z, x, y )))
        |> Set.fromList


isInCuboid : ( Int, Int, Int ) -> Cuboid -> Bool
isInCuboid ( x, y, z ) { xMin, xMax, yMin, yMax, zMin, zMax } =
    x >= xMin && x <= xMax && y >= yMin && y <= yMax && z >= zMin && z <= zMax


fromProto : AreaPart -> Cuboid
fromProto { topLeftX, topLeftY, bottomRightX, bottomRightY, topLayerNumber, bottomLayerNumber } =
    { yMin = topLeftY
    , xMin = topLeftX
    , yMax = bottomRightY
    , xMax = bottomRightX
    , zMax = topLayerNumber
    , zMin = bottomLayerNumber
    }


toProto : Cuboid -> AreaPart
toProto { xMin, xMax, yMin, yMax, zMin, zMax } =
    { topLeftX = xMin
    , topLeftY = yMin
    , bottomRightX = xMax
    , bottomRightY = yMax
    , topLayerNumber = zMax
    , bottomLayerNumber = zMin
    }


type alias Column =
    { lines : NonEmpty Line, x : Int }


type alias Line =
    { yMin : Int, yMax : Int }


type alias Layer =
    { rectangles : NonEmpty Rectangle, z : Int }


type alias Rectangle =
    { xMin : Int, xMax : Int, yMin : Int, yMax : Int }


toLayers : NonEmpty ( Int, Int, Int ) -> NonEmpty Layer
toLayers ( ( firstZ, firstX, firstY ), rest ) =
    List.foldl
        (\( z, x, y ) ({ currentCol, completedCols } as acc) ->
            let
                ( lastPart, otherParts ) =
                    currentCol.lines
            in
            if z /= acc.currentZ then
                { currentCol = { lines = NonEmpty.singleton { yMin = y, yMax = y }, x = x }
                , currentZ = z
                , completedCols = []
                , completedLayers = { rectangles = columnsToRectangles <| NonEmpty.reverse ( currentCol, acc.completedCols ), z = acc.currentZ } :: acc.completedLayers
                }

            else if x /= acc.currentCol.x then
                { acc
                    | currentCol = { lines = NonEmpty.singleton { yMin = y, yMax = y }, x = x }
                    , completedCols = { currentCol | lines = NonEmpty.reverse currentCol.lines } :: completedCols
                }

            else if y == lastPart.yMax + 1 then
                { acc | currentCol = { currentCol | lines = ( { lastPart | yMax = y }, otherParts ) } }

            else
                { acc | currentCol = { currentCol | lines = NonEmpty.cons { yMin = y, yMax = y } currentCol.lines } }
        )
        { currentCol = { lines = NonEmpty.singleton { yMin = firstY, yMax = firstY }, x = firstX }
        , currentZ = firstZ
        , completedCols = []
        , completedLayers = []
        }
        rest
        |> (\{ currentCol, currentZ, completedCols, completedLayers } ->
                ( { rectangles = columnsToRectangles <| NonEmpty.reverse ( currentCol, completedCols ), z = currentZ }, completedLayers )
           )
        |> NonEmpty.reverse


columnsToRectangles : NonEmpty Column -> NonEmpty Rectangle
columnsToRectangles =
    combineNDim
        { compare = compare2Dim
        , extend = \col rectangle -> { rectangle | xMax = col.x }
        , create = \col line -> { xMin = col.x, xMax = col.x, yMin = line.yMin, yMax = line.yMax }
        , extract = .lines
        , diff = \rectangle col -> col.x - rectangle.xMax
        }


layersToCuboids : NonEmpty Layer -> NonEmpty Cuboid
layersToCuboids =
    combineNDim
        { compare = compare3Dim
        , extend = \layer cuboid -> { cuboid | zMax = layer.z }
        , create =
            \layer rectangle ->
                { zMin = layer.z
                , zMax = layer.z
                , xMin = rectangle.xMin
                , xMax = rectangle.xMax
                , yMin = rectangle.yMin
                , yMax = rectangle.yMax
                }
        , extract = .rectangles
        , diff = \cuboid layer -> layer.z - cuboid.zMax
        }


combineNDim :
    { compare : dim_n_minus_one -> dim_n -> Order
    , extend : dim_n_one_fixed -> dim_n -> dim_n
    , create : dim_n_one_fixed -> dim_n_minus_one -> dim_n
    , extract : dim_n_one_fixed -> NonEmpty dim_n_minus_one
    , diff : dim_n -> dim_n_one_fixed -> Int
    }
    -> NonEmpty dim_n_one_fixed
    -> NonEmpty dim_n
combineNDim config ( firstCol, restCols ) =
    List.foldl
        (\col ( furthestRightEndingRectangles, rectanglesFurtherLeft ) ->
            let
                rec :
                    dim_n_minus_one
                    -> { bigger : List dim_n, smaller : List dim_n, extended : List dim_n }
                    -> { bigger : List dim_n, smaller : List dim_n, extended : NonEmpty dim_n }
                rec colPart { bigger, smaller, extended } =
                    let
                        extensionResult =
                            tryExtend config.compare colPart bigger

                        newSmaller =
                            smaller ++ extensionResult.smaller

                        newBigger =
                            extensionResult.bigger

                        newExtended =
                            case extensionResult.perfectMatch of
                                Just matchingRectangle ->
                                    ( config.extend col matchingRectangle, extended )

                                Nothing ->
                                    ( config.create col colPart, extended )
                    in
                    { bigger = newBigger, smaller = newSmaller, extended = newExtended }

                tryExtendRectangles rectanglesWithRight_X_Minus_1 =
                    let
                        ( firstLine, restLines ) =
                            config.extract col

                        firstResult =
                            rec firstLine { bigger = rectanglesWithRight_X_Minus_1, smaller = [], extended = [] }

                        recWrapper c { bigger, smaller, extended } =
                            rec c { bigger = bigger, smaller = smaller, extended = NonEmpty.toList extended }
                    in
                    List.foldl
                        recWrapper
                        firstResult
                        restLines

                firstFurthestRightEndingRectangle =
                    NonEmpty.head furthestRightEndingRectangles
            in
            if config.diff firstFurthestRightEndingRectangle col == 1 then
                let
                    { bigger, smaller, extended } =
                        tryExtendRectangles <| NonEmpty.toList furthestRightEndingRectangles
                in
                ( extended, smaller ++ bigger ++ rectanglesFurtherLeft )

            else
                ( config.extract col |> NonEmpty.map (config.create col), NonEmpty.toList furthestRightEndingRectangles ++ rectanglesFurtherLeft )
        )
        ( config.extract firstCol |> NonEmpty.map (config.create firstCol), [] )
        restCols
        |> (\( ( lastRectangle1, lastRectangleRest ), prevRectangles ) -> ( lastRectangle1, lastRectangleRest ++ prevRectangles ))


compare2Dim : Line -> Rectangle -> Order
compare2Dim line rectangle =
    if rectangle.yMax == line.yMax && rectangle.yMin == line.yMin then
        EQ

    else if rectangle.yMax <= line.yMax then
        GT

    else
        LT


compare3Dim : Rectangle -> Cuboid -> Order
compare3Dim rectangle cuboid =
    if cuboid.yMax == rectangle.yMax && cuboid.yMin == rectangle.yMin && cuboid.xMax == rectangle.xMax && cuboid.xMin == rectangle.xMin then
        EQ

    else if cuboid.yMax <= rectangle.yMax then
        -- TODO figure out if this is the correct predicate by writing tests
        GT

    else
        LT


{-| Try to extend some n dimensional structure with a n-1 dimensional structure.
Consider the n = 2 case:

     _______
     |      |
     |      |
     |______|

        ______
        |    |      |
        |____|      |

        ^^^        ^
        n dim      (n - 1) dim
          _______
         |_______|



    Here, the comparison function would look like this:

        compare : { top : Int, bottom: Int } -> { top : Int, bottom : Int, left : Int, right : Int } -> Order
        compare line rectangle =
            if rectangle.top == line.top && rectangle.bottom == line.bottom then
                EQ

            else if rectangle.bottom <= line.bottom then
                GT

            else
                LT

    In the displayed case, we would look at the first rectangle and the second case would match, i.e. the comparison function
    determines the line to be greater than the rectangle.
    Then, we would recursively look at the next rectangle. In this case, the comparison function determines the two objects as equal.
    This means we would return the first rectangle in the "smaller" list, the second rectangle as the "perfectMatch"
    and the third rectangle in the "bigger" list.

-}
tryExtend : (dim_n_minus_one -> dim_n -> Order) -> dim_n_minus_one -> List dim_n -> { smaller : List dim_n, perfectMatch : Maybe dim_n, bigger : List dim_n }
tryExtend comparisonFunction dimNMinusOne =
    let
        helper smallerDimNs restDimNs =
            case restDimNs of
                [] ->
                    { smaller = smallerDimNs, perfectMatch = Nothing, bigger = [] }

                first :: rest ->
                    case comparisonFunction dimNMinusOne first of
                        EQ ->
                            { smaller = smallerDimNs, perfectMatch = Just first, bigger = rest }

                        GT ->
                            helper (first :: smallerDimNs) rest

                        LT ->
                            { smaller = first :: smallerDimNs, perfectMatch = Nothing, bigger = rest }
    in
    helper []
