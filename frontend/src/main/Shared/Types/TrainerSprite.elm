module Shared.Types.TrainerSprite exposing (trainerNumberToPath)

import Tagged exposing (untag)
import TrainerNumber exposing (TrainerNumber)


trainerNumberToPath : TrainerNumber -> String
trainerNumberToPath trainerNumber =
    "/images/trainer/" ++ String.fromInt (untag trainerNumber) ++ ".png"
