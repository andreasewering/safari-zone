module Shared.Types.Animations exposing
    ( ActiveAnimation
    , ActiveAnimations
    , AnimationConfig
    , AnimationTileDict
    , Position
    , addAnimation
    , continueAnimations
    , fromProto
    , getActive
    , init
    , tileAnimationDictFromProto
    )

import Array exposing (Array)
import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Direction as Direction exposing (Direction)
import Domain.Layer exposing (LayerNumber)
import Domain.Position exposing (Position)
import Proto.Smeargle
import Shared.Types.FrameBased as FrameBased exposing (FrameBased, initFrameBased)


type alias AnimationConfig =
    Array Proto.Smeargle.Animation


type alias AnimationFrames =
    FrameBased (Maybe AnimationFrame)


type alias AnimationFrame =
    { arrayIndex : Int
    , index : Int
    , opacity : Float
    }


type alias ActiveAnimations =
    List Animation


getByPosition : Position -> AnimationTileDict -> Maybe Int
getByPosition pos =
    Dict.get ( pos.coord.x, pos.coord.y, pos.layerNumber )


type alias AnimationTileDict =
    Dict ( Int, Int, Int ) Int


type alias ActiveAnimation =
    { position : Position
    , xShift : Float
    , yShift : Float
    , index : Int
    , opacity : Float
    }


getActive : ActiveAnimations -> List ActiveAnimation
getActive =
    List.filterMap
        (\{ position, frames, xShift, yShift } ->
            Maybe.map
                (\{ index, opacity } ->
                    ActiveAnimation position xShift yShift index opacity
                )
                frames.curr
        )


type alias Animation =
    { position : Position
    , xShift : Float
    , yShift : Float
    , frames : AnimationFrames
    }


type alias Position =
    { coord : Coordinate Int, layerNumber : LayerNumber, direction : Direction }


init : ActiveAnimations
init =
    []


addAnimation :
    Position
    -> AnimationConfig
    -> AnimationTileDict
    -> ActiveAnimations
    -> ActiveAnimations
addAnimation position config tileDict =
    let
        ( xShift, yShift ) =
            calculateShift position.direction
    in
    getByPosition position tileDict
        |> Maybe.andThen (\key -> getAnimation key config)
        |> Maybe.map
            (\frames ->
                (::)
                    { frames = frames
                    , xShift = xShift
                    , yShift = yShift
                    , position = position
                    }
            )
        |> Maybe.withDefault identity


continueAnimations : Float -> ActiveAnimations -> ActiveAnimations
continueAnimations dt =
    List.filterMap (continueAnimation dt)


continueAnimation : Float -> Animation -> Maybe Animation
continueAnimation dt animation =
    let
        next =
            FrameBased.nextFrame dt animation.frames
    in
    Maybe.map (always { animation | frames = next }) next.curr


getAnimation : Int -> AnimationConfig -> Maybe AnimationFrames
getAnimation animationId =
    Array.get animationId >> Maybe.map singleFromProto


{-| Shift to correct the animations placement to be under the players feet
-}
calculateShift : Direction -> ( Float, Float )
calculateShift dir =
    case dir of
        Direction.U ->
            ( 0, 0.5 )

        Direction.D ->
            ( 0, 0.5 )

        Direction.L ->
            ( 0, 0.5 )

        Direction.R ->
            ( 0, 0.5 )


fromProto : List Proto.Smeargle.Animation -> Array AnimationFrames
fromProto =
    List.map singleFromProto
        >> Array.fromList


singleFromProto : Proto.Smeargle.Animation -> AnimationFrames
singleFromProto { frames } =
    let
        frameRate =
            200

        frameArr =
            Array.fromList frames
                |> Array.indexedMap
                    (\i frame ->
                        { arrayIndex = i
                        , index = frame.index
                        , opacity = frame.opacity
                        }
                    )

        next =
            Maybe.andThen <|
                \{ arrayIndex } ->
                    Array.get (arrayIndex + 1) frameArr
    in
    initFrameBased (Array.get 0 frameArr) next frameRate


tileAnimationDictFromProto : List { t | x : Int, y : Int, layerNumber : Int, animationId : Int } -> AnimationTileDict
tileAnimationDictFromProto =
    List.map
        (\{ x, y, layerNumber, animationId } ->
            ( ( x, y, layerNumber ), animationId )
        )
        >> Dict.fromList
