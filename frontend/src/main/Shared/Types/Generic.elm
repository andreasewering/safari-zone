module Shared.Types.Generic exposing (..)


type alias Setter a =
    a -> a


type alias SetAndDo set do =
    set -> ( set, do )


type alias Lift s a =
    Setter s -> Setter a


getAndSet : (a -> s) -> (s -> Setter a) -> Lift s a
getAndSet getter setter modify a =
    setter (getter a |> modify) a
