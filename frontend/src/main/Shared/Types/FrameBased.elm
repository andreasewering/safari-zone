module Shared.Types.FrameBased exposing (..)


type alias FrameBased a =
    { curr : a
    , next : a -> ( a, Int )
    , maxFrames : Float
    , currFrames : Float
    }


current : FrameBased a -> a
current =
    .curr


initFrameBased : a -> (a -> a) -> Int -> FrameBased a
initFrameBased curr next maxFrames =
    initFrameBasedIrregular curr (\a -> ( next a, maxFrames )) maxFrames


initFrameBasedIrregular : a -> (a -> ( a, Int )) -> Int -> FrameBased a
initFrameBasedIrregular curr next maxFrames =
    { curr = curr
    , next = next
    , maxFrames = toFloat maxFrames
    , currFrames = 0
    }


nextFrame : Float -> FrameBased a -> FrameBased a
nextFrame dt fb =
    let
        newCurrFrames =
            fb.currFrames + dt
    in
    if newCurrFrames >= fb.maxFrames then
        let
            ( next, nextMaxFrames ) =
                fb.next fb.curr
        in
        { fb | curr = next, currFrames = fb.maxFrames - newCurrFrames, maxFrames = toFloat nextMaxFrames }

    else
        { fb | currFrames = newCurrFrames }


immediately : FrameBased a -> a -> FrameBased a
immediately fb immediate =
    { fb | curr = immediate, currFrames = 0 }
