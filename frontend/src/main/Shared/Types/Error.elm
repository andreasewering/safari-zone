module Shared.Types.Error exposing (..)

import Base64
import Grpc
import Http
import Protobuf.Token
import WebGL.Texture as WebGL



-- | Central Error Type for most errors that can occur


type Error
    = BadUrl String
    | Timeout
    | NetworkError
    | BadStatus BadStatusDetails
    | BadBody String
    | TextureError WebGL.Error
    | TokenDecodingFailed Protobuf.Token.Error


type alias BadStatusDetails =
    { httpMetadata : Http.Metadata
    , response : String
    , grpcMetadata : { status : Grpc.GrpcStatus, message : String }
    }


fromGrpcError : Grpc.Error -> Error
fromGrpcError err =
    case err of
        Grpc.BadUrl badUrl ->
            BadUrl badUrl

        Grpc.Timeout ->
            Timeout

        Grpc.NetworkError ->
            NetworkError

        Grpc.BadStatus { metadata, errMessage, status, response } ->
            BadStatus
                { httpMetadata = metadata
                , grpcMetadata = { status = status, message = errMessage }
                , response =
                    Base64.fromBytes response
                        |> Maybe.map ((++) "Bytes in base64 format: ")
                        |> Maybe.withDefault "Bytes -> Base64 Conversion failed"
                }

        Grpc.BadBody bytes ->
            Base64.fromBytes bytes
                |> Maybe.map ((++) "Bytes in base64 format: ")
                |> Maybe.withDefault "Bytes -> Base64 Conversion failed"
                |> BadBody

        Grpc.UnknownGrpcStatus _ ->
            NetworkError
