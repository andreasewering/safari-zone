module Shared.Types.SpriteEntityProps exposing (..)

import Data.Units exposing (TileUnit)
import Domain.Coordinate exposing (Coordinate)


type alias SpriteEntityProps =
    { spriteId : Int
    , opacity : Float
    , position : Coordinate TileUnit
    , size : Float
    , rotation : Float
    }
