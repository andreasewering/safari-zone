module Shared.Types.Area exposing (..)

import AreaId exposing (AreaId)
import AudioTrackNumber exposing (AudioTrackNumber)
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Proto.Arceus
import Shared.Types.Cuboid as Cuboid exposing (Cuboid, isInCuboid)
import Tagged exposing (tag)


type alias Area =
    { id : AreaId
    , name : String
    , areaParts : List Cuboid
    , backgroundMusicTrack : Maybe AudioTrackNumber
    }


getCurrentAudioTrack : LayerNumber -> Coordinate Int -> List Area -> Maybe AudioTrackNumber
getCurrentAudioTrack layerNumber { x, y } areas =
    let
        isInArea { areaParts } =
            areaParts
                |> List.any (isInCuboid ( x, y, layerNumber ))
    in
    -- We might be able to improve performance here by abusing sorted rectangles
    -- Not sure how we would store it optimally though and it probably only matters
    -- for large size maps
    List.filter isInArea areas
        |> List.head
        |> Maybe.andThen .backgroundMusicTrack


fromProto : Proto.Arceus.Area -> Area
fromProto area =
    { id = tag area.id
    , name = area.name
    , areaParts = List.map Cuboid.fromProto area.areaParts
    , backgroundMusicTrack = Maybe.map tag area.backgroundMusicTrack
    }
