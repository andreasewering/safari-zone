module Shared.Types.Audio exposing (AudioConfig, fromProto, getLoop, getLoopNames)

import AudioTrackNumber exposing (AudioTrackNumber)
import AudioTrackNumberDict exposing (AudioTrackNumberDict)
import Proto.Arceus as Arceus
import Tagged exposing (tag)


type alias AudioConfig =
    { tracks : AudioTrackNumberDict AudioTrack
    , onPokemonObtained : Maybe AudioTrack
    }


type alias AudioTrack =
    { start : Maybe Float, end : Maybe Float, filename : String }


getLoopNames : AudioConfig -> List ( AudioTrackNumber, String )
getLoopNames { tracks } =
    AudioTrackNumberDict.toList tracks |> List.map (Tuple.mapSecond .filename)


getLoop : AudioTrackNumber -> AudioConfig -> Maybe AudioTrack
getLoop audioTrackNumber { tracks } =
    AudioTrackNumberDict.get audioTrackNumber tracks


fromProto : Arceus.GetAudioTracksResponse -> AudioConfig
fromProto { tracks, onPokemonObtained } =
    List.map singleFromProto tracks
        |> List.foldl
            (\( audioTrackNumber, track ) config ->
                if audioTrackNumber == tag onPokemonObtained then
                    { config | onPokemonObtained = Just track }

                else
                    { config | tracks = AudioTrackNumberDict.insert audioTrackNumber track config.tracks }
            )
            { tracks = AudioTrackNumberDict.empty, onPokemonObtained = Nothing }


singleFromProto : Arceus.AudioTrack -> ( AudioTrackNumber, AudioTrack )
singleFromProto { audioTrackNumber, filename, start, end } =
    ( tag audioTrackNumber, { filename = filename, start = start, end = end } )
