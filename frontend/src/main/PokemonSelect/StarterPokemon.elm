module PokemonSelect.StarterPokemon exposing (StarterPokemon, fromProto)

import PokemonNumber exposing (PokemonNumber)
import Proto.Kangaskhan as Kangaskhan
import Proto.Kangaskhan.Type exposing (Type)
import Tagged exposing (tag)


type alias StarterPokemon =
    { name : String
    , number : PokemonNumber
    , primaryType : Type
    , secondaryType : Maybe Type
    , height : Float
    , weight : Float
    , description : String
    }


fromProto : Kangaskhan.GetStarterPokemonResponse -> List StarterPokemon
fromProto { pokemon } =
    List.map singleFromProto pokemon


singleFromProto : Kangaskhan.StarterPokemon -> StarterPokemon
singleFromProto starter =
    { name = starter.name
    , number = tag starter.number
    , primaryType = starter.primaryType
    , secondaryType = starter.secondaryType
    , height = starter.height
    , weight = starter.weight
    , description = starter.description
    }
