module InteropDefinitions exposing (Flags, FromElm(..), ToElm(..), chatotPrefix, interop, kangaskhanPrefix)

import Interop.Audio.In
import Interop.Audio.Out
import Interop.Event.Out
import Interop.Flags
import Interop.LocalStorage.Out
import Interop.WebSocket.In
import Interop.WebSocket.Out
import List.Extra
import MapId exposing (MapId)
import Proto.Chatot
import Proto.Chatot.WebsocketMessageToClient
import Proto.Kangaskhan
import Proto.Kangaskhan.WebsocketMessageToClient
import Protobuf.Decode as PD
import TsJson.Decode as TsDecode exposing (Decoder)
import TsJson.Encode as TsEncode exposing (Encoder)


chatotPrefix : String
chatotPrefix =
    "chatot"


kangaskhanPrefix : String
kangaskhanPrefix =
    "kangaskhan:"


decodeKangaskhan : String -> Maybe MapId
decodeKangaskhan str =
    if String.startsWith kangaskhanPrefix str then
        str
            |> String.dropLeft (String.length kangaskhanPrefix)
            |> MapId.fromHeader

    else
        Nothing


decodeChatot : String -> Maybe ()
decodeChatot str =
    if String.startsWith chatotPrefix str then
        Just ()

    else
        Nothing


type alias Flags =
    Interop.Flags.Flags


interop :
    { toElm : Decoder ToElm
    , fromElm : Encoder FromElm
    , flags : Decoder Flags
    }
interop =
    { toElm = toElm
    , fromElm = fromElm
    , flags = Interop.Flags.decoder
    }


type FromElm
    = WebsocketOut Interop.WebSocket.Out.Msg
    | CopyToClipboard String
    | LocalStorageOut Interop.LocalStorage.Out.Msg
    | AudioOut Interop.Audio.Out.Msg
    | EventOut Interop.Event.Out.Msg


type ToElm
    = ChatotIn
        (Interop.WebSocket.In.Msg
            Proto.Chatot.WebsocketMessageToClient.Message
            ()
        )
    | KangaskhanIn
        (Interop.WebSocket.In.Msg
            Proto.Kangaskhan.WebsocketMessageToClient.Message
            MapId
        )
    | AudioIn Interop.Audio.In.Msg


fromElm : Encoder FromElm
fromElm =
    TsEncode.union
        (\vWsOut vClipboard vLocalStorage vAudio vEvent value ->
            case value of
                WebsocketOut w ->
                    vWsOut w

                CopyToClipboard c ->
                    vClipboard c

                LocalStorageOut l ->
                    vLocalStorage l

                AudioOut a ->
                    vAudio a

                EventOut e ->
                    vEvent e
        )
        |> TsEncode.variantTagged "ws" Interop.WebSocket.Out.encoder
        |> TsEncode.variantTagged "clipboard" TsEncode.string
        |> TsEncode.variantTagged "localStorage" Interop.LocalStorage.Out.encoder
        |> TsEncode.variantTagged "audio" Interop.Audio.Out.encoder
        |> TsEncode.variantTagged "event" Interop.Event.Out.encoder
        |> TsEncode.buildUnion


toElm : Decoder ToElm
toElm =
    TsDecode.discriminatedUnion "tag"
        [ ( "ws"
          , TsDecode.field "data" Interop.WebSocket.In.decoder
                |> TsDecode.andThen
                    (TsDecode.andThenInit classifyDynamic)
          )
        , ( "audio"
          , TsDecode.field "data" Interop.Audio.In.decoder |> TsDecode.map AudioIn
          )
        ]


classifyDynamic : ( String, Interop.WebSocket.In.DynamicMsg ) -> Decoder ToElm
classifyDynamic dynamic =
    List.Extra.findMap ((|>) dynamic) decoders
        |> Maybe.withDefault (TsDecode.fail <| "Received WS message of unknown key: " ++ Tuple.first dynamic)


decoders : List (( String, Interop.WebSocket.In.DynamicMsg ) -> Maybe (Decoder ToElm))
decoders =
    let
        toDecoder decodeKey decodeWsMessage toElmConstructor =
            \dynamic ->
                decodeKey (Tuple.first dynamic)
                    |> Maybe.map
                        (\key ->
                            Interop.WebSocket.In.decodeDynamic
                                (PD.decode decodeWsMessage
                                    >> Maybe.andThen .message
                                )
                                key
                                dynamic
                                |> TsDecode.map toElmConstructor
                        )
    in
    [ toDecoder decodeKangaskhan Proto.Kangaskhan.decodeWebsocketMessageToClient KangaskhanIn
    , toDecoder decodeChatot Proto.Chatot.decodeWebsocketMessageToClient ChatotIn
    ]
