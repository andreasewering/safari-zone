module Textures.Api exposing (pokemonTexturePath)

import PokemonNumber exposing (PokemonNumber)
import Tagged exposing (untag)


pokemonTexturePath : PokemonNumber -> String
pokemonTexturePath pokemonNumber =
    "/images/overworld/"
        ++ (String.fromInt <| untag pokemonNumber)
        ++ ".png"
