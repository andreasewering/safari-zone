module Textures.Store exposing
    ( Sprite
    , Textures
    , Tilemaps
    )

import BoundedDict exposing (BoundedDict)
import Domain.Coordinate exposing (Coordinate)
import ItemDict exposing (ItemDict)
import MapId exposing (MapId)
import PokemonNumberDict exposing (PokemonNumberDict)
import TrainerNumberDict exposing (TrainerNumberDict)
import WebGL.Texture exposing (Texture)


type alias Textures =
    { tileset : Maybe Sprite
    , tilemaps : BoundedDict MapId Tilemaps
    , pokemonTextures : PokemonNumberDict Sprite
    , trainerTextures : TrainerNumberDict Sprite
    , itemTextures : ItemDict Sprite
    }


type alias Tilemaps =
    { texture : Texture
    , offset : Coordinate Int
    , layers : List Int
    }


type alias Sprite =
    { texture : Texture
    , rows : Int
    , columns : Int
    , path : String
    }
