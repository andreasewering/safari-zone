module Domain.Direction exposing (..)

import Domain.Coordinate exposing (Coordinate)
import Fuzz
import Proto.Data.Direction


type Direction
    = L
    | R
    | U
    | D


default : Direction
default =
    D


all : List Direction
all =
    [ L, U, R, D ]


directionToCoord : Direction -> Coordinate number
directionToCoord dir =
    case dir of
        L ->
            { x = -1, y = 0 }

        R ->
            { x = 1, y = 0 }

        U ->
            { x = 0, y = -1 }

        D ->
            { x = 0, y = 1 }


coordToDirection : Coordinate Int -> Maybe Direction
coordToDirection { x, y } =
    -- workaround, you cannot match on negative ints :/
    case ( x + 1, y + 1 ) of
        ( 0, 1 ) ->
            Just L

        ( 2, 1 ) ->
            Just R

        ( 1, 0 ) ->
            Just U

        ( 1, 2 ) ->
            Just D

        _ ->
            Nothing


toRadians : Direction -> Float
toRadians dir =
    case dir of
        U ->
            0

        L ->
            pi / 2

        D ->
            pi

        R ->
            3 * pi / 2


fromProto : Proto.Data.Direction.Direction -> Maybe Direction
fromProto dir =
    case dir of
        Proto.Data.Direction.L ->
            Just L

        Proto.Data.Direction.R ->
            Just R

        Proto.Data.Direction.U ->
            Just U

        Proto.Data.Direction.D ->
            Just D

        _ ->
            Nothing


toProto : Direction -> Proto.Data.Direction.Direction
toProto dir =
    case dir of
        L ->
            Proto.Data.Direction.L

        R ->
            Proto.Data.Direction.R

        U ->
            Proto.Data.Direction.U

        D ->
            Proto.Data.Direction.D


fuzzer : Fuzz.Fuzzer Direction
fuzzer =
    Fuzz.oneOf <| List.map Fuzz.constant all
