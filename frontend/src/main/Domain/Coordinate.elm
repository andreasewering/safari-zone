module Domain.Coordinate exposing (..)

import Basics
import Fuzz


type alias Coordinate number =
    { x : number
    , y : number
    }


angle : Coordinate Float -> Float
angle { x, y } =
    Basics.atan2 y x


minus : Coordinate number -> Coordinate number -> Coordinate number
minus c1 c2 =
    { x = c1.x - c2.x, y = c1.y - c2.y }


manhattanDistance : Coordinate number -> Coordinate number -> number
manhattanDistance c1 c2 =
    abs (c2.x - c1.x) + abs (c2.y - c1.y)


distance : Coordinate Float -> Float
distance { x, y } =
    sqrt (x ^ 2 + y ^ 2)


plus : Coordinate number -> Coordinate number -> Coordinate number
plus c1 c2 =
    { x = c1.x + c2.x, y = c1.y + c2.y }


times : number -> Coordinate number -> Coordinate number
times n c1 =
    { x = c1.x * n, y = c1.y * n }


map : (a -> b) -> Coordinate a -> Coordinate b
map f { x, y } =
    { x = f x, y = f y }


toFloat : Coordinate Int -> Coordinate Float
toFloat =
    map Basics.toFloat


divide : Coordinate Float -> Float -> Coordinate Float
divide { x, y } d =
    { x = x / d, y = y / d }


toTuple : Coordinate number -> ( number, number )
toTuple { x, y } =
    ( x, y )


fromTuple : ( number, number ) -> Coordinate number
fromTuple ( x, y ) =
    { x = x, y = y }


cartesianProduct : List a -> List a -> List (Coordinate a)
cartesianProduct lx ly =
    List.concatMap (\x -> List.map (\y -> { x = x, y = y }) ly) lx


fuzzer : Fuzz.Fuzzer (Coordinate Int)
fuzzer =
    Fuzz.map2 Coordinate Fuzz.int Fuzz.int
