module Domain.PathAlgorithm exposing (..)

import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Direction as Direction exposing (Direction)
import Domain.Layer exposing (LayerNumber)
import Domain.MoveRule as MoveRule exposing (MoveRules)
import Maybe.Extra
import Set exposing (Set)



-- Sample algorithm, see https://en.wikipedia.org/wiki/Pathfinding
-- Can be probably tuned to be more performant with a metric for each coordinate based on the manhattan distance
-- to the target -> avoid negative implications of the current BFS like approach for large distances


findBestPath :
    MoveRules
    -> MoveRule.MoveAbility
    -> ( Coordinate Int, LayerNumber )
    -> ( Coordinate Int, LayerNumber )
    -> Maybe ( Direction, LayerNumber )
findBestPath rules moveAbility ( currentCoord, currentLayerNumber ) ( targetCoord, targetLayerNumber ) =
    let
        target =
            ( targetCoord.x, targetCoord.y, targetLayerNumber )

        step : Set ( Int, Int, LayerNumber ) -> ( Int, Int, LayerNumber ) -> Set ( Int, Int, LayerNumber )
        step previouslyVisited ( x, y, layerNumber ) =
            List.filterMap
                (\dir ->
                    MoveRule.canMoveFromTo ( { x = x, y = y }, layerNumber ) dir moveAbility rules
                        |> Maybe.map
                            (\nextLayerNumber ->
                                let
                                    vector =
                                        Direction.directionToCoord dir
                                in
                                ( x + vector.x
                                , y + vector.y
                                , nextLayerNumber
                                )
                            )
                        |> Maybe.Extra.filter
                            (\nextCoord ->
                                not <| Set.member nextCoord previouslyVisited
                            )
                )
                Direction.all
                |> Set.fromList

        loopSingle : Set ( Int, Int, LayerNumber ) -> ( Int, Int, LayerNumber ) -> Result (Set ( Int, Int, LayerNumber )) ( Int, Int, LayerNumber )
        loopSingle previouslyVisited pos =
            let
                visitedNow =
                    step previouslyVisited pos
            in
            if Set.member ( currentCoord.x, currentCoord.y, currentLayerNumber ) visitedNow then
                Ok pos

            else
                Err visitedNow

        loop : Set ( Int, Int, LayerNumber ) -> List ( Int, Int, LayerNumber ) -> Maybe ( Coordinate Int, LayerNumber )
        loop previouslyVisited positions =
            let
                iteration : Result (Set ( Int, Int, LayerNumber )) ( Int, Int, LayerNumber )
                iteration =
                    List.map (loopSingle previouslyVisited) positions
                        |> List.foldl
                            (\res acc ->
                                case ( res, acc ) of
                                    ( _, Ok pos ) ->
                                        Ok pos

                                    ( Ok pos, _ ) ->
                                        Ok pos

                                    ( Err set1, Err set2 ) ->
                                        Err <| Set.union set1 set2
                            )
                            (Err Set.empty)
            in
            case iteration of
                Ok ( x, y, layerNumber ) ->
                    Just ( { x = x, y = y }, layerNumber )

                Err visited ->
                    if Set.isEmpty visited then
                        Nothing

                    else
                        loop (Set.union visited previouslyVisited) (Set.toList visited)
    in
    loop (Set.singleton target) [ target ]
        |> Maybe.andThen
            (\( coord, layerNumber ) ->
                Coordinate.minus coord currentCoord
                    |> Direction.coordToDirection
                    |> Maybe.map (\dir -> ( dir, layerNumber ))
            )
