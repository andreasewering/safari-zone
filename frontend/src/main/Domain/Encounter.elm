module Domain.Encounter exposing
    ( Action
    , CurrentAction(..)
    , Encounter
    , EncounterId
    , Encounters
    , continueMoves
    , empty
    , getEncounter
    , getEncounters
    , getSizeModifier
    , insertEncounter
    , modifyPosition
    , moveEncounter
    , removeEncounter
    , removeEncounters
    , rotate
    , startBreakOutAnimation
    , startCatchAnimation
    , switchMapTo
    )

import Config
import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Direction as Direction exposing (Direction)
import Domain.Layer exposing (LayerNumber)
import Domain.Position as Position
import Domain.QueueablePosition as QueueablePosition exposing (QueueablePosition)
import MapId exposing (MapId)
import Maybe.Extra
import PokemonNumber exposing (PokemonNumber)
import PokemonNumberDict exposing (PokemonNumberDict)
import Proto.Kangaskhan exposing (GetPokemonResponse)
import Proto.Kangaskhan.WebsocketMessageToClient
import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Set
import Shared.Bezier as Bezier
import Shared.Types.FrameBased as FrameBased exposing (FrameBased)
import Shared.Types.Generic exposing (Lift, Setter)
import Tagged exposing (tag)


type alias EncounterId =
    Int64


{-| Floats are between 0 and 1 and represent the animation progress
-}
type CatchAnimation
    = NoCatch
    | IntoBall Float
    | OutOfBall Float


type alias Encounter =
    { position : QueueablePosition
    , encounterId : EncounterId
    , pokemonNumber : PokemonNumber
    , shinyStatus : Bool
    , action : Action
    , direction : Direction
    , catchAnimation : CatchAnimation

    {- Necessary here, because of switching maps:
       We need to render pokemon of the old map when the new map (and pokemon on that map) is loading
    -}
    , mapId : MapId
    }


type alias Action =
    FrameBased CurrentAction


type CurrentAction
    = StepLeft
    | StepRight


rotate : CurrentAction -> CurrentAction
rotate a =
    case a of
        StepLeft ->
            StepRight

        StepRight ->
            StepLeft


type Encounters
    = Encounters (Dict ( Int, Int ) Encounter)


empty : Encounters
empty =
    Encounters Dict.empty


modifyPosition : Lift QueueablePosition { e | position : QueueablePosition }
modifyPosition modify e =
    { e | position = modify e.position }


getEncounters : MapId -> Encounters -> List Encounter
getEncounters mapId (Encounters encounters) =
    let
        onCurrentMap encounter =
            encounter.mapId == mapId
    in
    Dict.values encounters |> List.filter onCurrentMap


getEncounter : EncounterId -> Encounters -> Maybe Encounter
getEncounter encounterId (Encounters encounters) =
    Dict.get (Int64.toInts encounterId) encounters


getSizeModifier : Encounter -> Float
getSizeModifier encounter =
    case encounter.catchAnimation of
        NoCatch ->
            1

        IntoBall n ->
            Bezier.cubic ( 0.25, 0.75 ) <| 1 - n

        OutOfBall n ->
            Bezier.cubic ( 0.25, 0.75 ) n


removeEncounters : List EncounterId -> Setter Encounters
removeEncounters list (Encounters encounters) =
    let
        idsToRemove =
            List.map Int64.toInts list |> Set.fromList
    in
    Dict.filter (\id _ -> not <| Set.member id idsToRemove) encounters
        |> Encounters


insertEncounter : MapId -> Proto.Kangaskhan.WebsocketMessageToClient.NewEncounter -> Encounters -> Encounters
insertEncounter mapId { pokemonNumber, encounterId, coordX, coordY, layerNumber, isShiny, direction } (Encounters encounters) =
    Dict.insert (Int64.toInts encounterId)
        { position =
            Direction.fromProto direction
                |> Maybe.withDefault Direction.default
                |> QueueablePosition.init { x = coordX, y = coordY } layerNumber
        , mapId = mapId
        , encounterId = encounterId
        , pokemonNumber = tag pokemonNumber
        , shinyStatus = isShiny
        , action = FrameBased.initFrameBased StepLeft rotate Config.trainerAnimationSpeed
        , direction = Direction.D
        , catchAnimation = NoCatch
        }
        encounters
        |> Encounters


moveEncounter : Proto.Kangaskhan.WebsocketMessageToClient.MoveEncounter -> Encounters -> Encounters
moveEncounter { coordX, coordY, layerNumber, encounterId, direction } (Encounters encounters) =
    Encounters <|
        case Direction.fromProto direction of
            Just dir ->
                Dict.update (Int64.toInts encounterId)
                    (Maybe.map (updateEncounterWithMove { x = coordX, y = coordY } layerNumber dir))
                    encounters

            Nothing ->
                encounters


startCatchAnimation : EncounterId -> Encounters -> Encounters
startCatchAnimation encId (Encounters encounters) =
    Dict.update (Int64.toInts encId) (Maybe.map <| \enc -> { enc | catchAnimation = IntoBall 0 }) encounters
        |> Encounters


startBreakOutAnimation : EncounterId -> Encounters -> Encounters
startBreakOutAnimation encId (Encounters encounters) =
    Dict.update (Int64.toInts encId) (Maybe.map <| \enc -> { enc | catchAnimation = OutOfBall 0 }) encounters
        |> Encounters


removeEncounter : EncounterId -> Encounters -> Encounters
removeEncounter encounterId (Encounters encounters) =
    Dict.remove (Int64.toInts encounterId) encounters
        |> Encounters


continueCatchAnimation : Float -> CatchAnimation -> CatchAnimation
continueCatchAnimation dt catchAnimation =
    case catchAnimation of
        NoCatch ->
            NoCatch

        IntoBall n ->
            IntoBall (min 1 <| n + dt / 200)

        OutOfBall n ->
            let
                nextN =
                    n + dt / 100
            in
            if nextN > 1 then
                NoCatch

            else
                OutOfBall nextN


continueMoves : Float -> PokemonNumberDict GetPokemonResponse -> Encounters -> ( Encounters, List Position.StandingPosition )
continueMoves dt pokemonDict (Encounters encounters) =
    let
        ( newEncounters, animationPositions ) =
            Dict.foldl
                (\key enc ( dict, positions ) ->
                    let
                        moveDurationInMs =
                            PokemonNumberDict.get enc.pokemonNumber pokemonDict
                                |> Maybe.map .moveDurationInMs
                                |> Maybe.withDefault 0

                        position =
                            QueueablePosition.continueMove (dt / moveDurationInMs) enc.position

                        startedMoveOnTile =
                            -- TODO this is not working correctly currently
                            -- It only triggers right at the start of an encounter spawn
                            -- Probably it just keeps "moving" and never "standing".
                            QueueablePosition.isStandingPosition enc.position
                                |> Maybe.Extra.filter (\_ -> QueueablePosition.hasMoved enc.position position)
                    in
                    ( Dict.insert key
                        { enc
                            | position = position
                            , catchAnimation = continueCatchAnimation dt enc.catchAnimation
                        }
                        dict
                    , Maybe.Extra.cons startedMoveOnTile positions
                    )
                )
                ( Dict.empty, [] )
                encounters
    in
    ( Encounters newEncounters, animationPositions )


switchMapTo : MapId -> Setter Encounters
switchMapTo mapId (Encounters encounters) =
    Dict.filter (\_ encounter -> encounter.mapId == mapId) encounters |> Encounters


updateEncounterWithMove : Coordinate Int -> LayerNumber -> Direction -> Setter { e | position : QueueablePosition, direction : Direction }
updateEncounterWithMove coord layerNumber dir encounter =
    let
        newPos =
            QueueablePosition.moveToCoord coord layerNumber dir encounter.position
    in
    { encounter
        | position = newPos
        , direction =
            QueueablePosition.getCurrentDirection newPos
    }
