module Domain.QueueablePosition exposing
    ( QueueablePosition
    , continueMove
    , getCurrentDirection
    , getCurrentLayerNumber
    , getExact
    , hasMoved
    , init
    , initDelayed
    , isStandingPosition
    , move
    , moveInDir
    , moveToCoord
    , setTile
    )

import Data.Units exposing (TileUnit)
import Domain.Coordinate as Coord exposing (Coordinate)
import Domain.Direction exposing (Direction)
import Domain.Layer exposing (LayerNumber)
import Domain.Position exposing (Position(..), StandingPosition)


type alias QueueablePosition =
    { position : Position
    , directionQueue : List QueuedPositionChange
    , delay : Int
    }


type QueuedPositionChange
    = Walk Direction LayerNumber
    | Turn Direction


init : Coordinate Int -> LayerNumber -> Direction -> QueueablePosition
init =
    initDelayed 0


initDelayed : Int -> Coordinate Int -> LayerNumber -> Direction -> QueueablePosition
initDelayed delay c layerNumber direction =
    { position = Domain.Position.init c layerNumber direction
    , directionQueue = []
    , delay = delay
    }


continueMove : Float -> QueueablePosition -> QueueablePosition
continueMove dt pos =
    move Nothing (getCurrentLayerNumber pos) dt pos


addTurn : Direction -> QueueablePosition -> QueueablePosition
addTurn dir pos =
    { pos | directionQueue = pos.directionQueue ++ [ Turn dir ] }


move : Maybe Direction -> LayerNumber -> Float -> QueueablePosition -> QueueablePosition
move maybeNewDir layerNumber dt pos =
    let
        { position, directionQueue, delay } =
            pos

        addToQueue q =
            case maybeNewDir of
                Just d ->
                    q
                        ++ [ Walk d layerNumber
                           ]

                Nothing ->
                    q

        moveInMayDir mayDir newLayerNumber =
            Domain.Position.move mayDir newLayerNumber dt position
    in
    case ( directionQueue, position ) of
        ( (Walk queuedDir queuedLayerNumber) :: restDirs, OnTile _ ) ->
            if List.length restDirs >= delay then
                { pos | position = moveInMayDir (Just queuedDir) queuedLayerNumber, directionQueue = addToQueue restDirs }

            else
                { pos | directionQueue = addToQueue directionQueue }

        ( (Turn queuedDir) :: restDirs, OnTile _ ) ->
            move maybeNewDir layerNumber dt { pos | position = Domain.Position.setDirection queuedDir position, directionQueue = restDirs }

        ( [], OnTile _ ) ->
            if delay > 0 then
                { pos | directionQueue = addToQueue directionQueue }

            else
                { pos | position = moveInMayDir maybeNewDir layerNumber }

        ( _, Moving _ ) ->
            { pos | position = moveInMayDir maybeNewDir layerNumber, directionQueue = addToQueue directionQueue }


hasMoved : QueueablePosition -> QueueablePosition -> Bool
hasMoved qp1 qp2 =
    Domain.Position.hasMoved qp1.position qp2.position


moveToCoord : Coordinate Int -> LayerNumber -> Direction -> QueueablePosition -> QueueablePosition
moveToCoord coord layerNumber direction pos =
    let
        target =
            getTarget pos
    in
    if target == coord then
        addTurn direction pos

    else
        case target |> Coord.minus coord |> Domain.Direction.coordToDirection of
            Just dir ->
                moveInDir dir layerNumber pos

            Nothing ->
                setTile coord layerNumber pos


moveInDir : Direction -> LayerNumber -> QueueablePosition -> QueueablePosition
moveInDir dir layerNumber pos =
    move (Just dir) layerNumber 0.1 pos


isStandingPosition : QueueablePosition -> Maybe StandingPosition
isStandingPosition =
    .position >> Domain.Position.isStandingPosition


getTarget : QueueablePosition -> Coordinate Int
getTarget { position, directionQueue } =
    let
        go dirs coord =
            case dirs of
                [] ->
                    coord

                (Walk dir _) :: rest ->
                    Domain.Direction.directionToCoord dir
                        |> Coord.plus coord
                        |> go rest

                (Turn _) :: rest ->
                    go rest coord
    in
    go directionQueue (Domain.Position.getNextTile position)


getExact : QueueablePosition -> Coordinate TileUnit
getExact =
    .position >> Domain.Position.getExact


getCurrentLayerNumber : QueueablePosition -> LayerNumber
getCurrentLayerNumber =
    .position >> Domain.Position.getCurrentLayerNumber


getCurrentDirection : QueueablePosition -> Direction
getCurrentDirection pos =
    case ( pos.delay > 0, pos.directionQueue, pos.position ) of
        ( True, (Walk dir _) :: _, OnTile _ ) ->
            dir

        _ ->
            pos.position |> Domain.Position.getCurrentDirection


setTile : Coordinate Int -> LayerNumber -> QueueablePosition -> QueueablePosition
setTile coord layerNumber pos =
    { pos | position = Domain.Position.setTile coord layerNumber pos.position, directionQueue = [] }
