module Domain.MoveRule exposing
    ( DirectionalMoveRule
    , MoveAbility
    , MoveRule(..)
    , MoveRules
    , canMoveFromTo
    , canMoveFromToCheck
    , getMovableDirections
    , groundRules
    , inDirection
    , internalMoveAbilityMembers
    , layerNumberShift
    , noMoveAbility
    , ruleFromProto
    , rulesFromProto
    , swim
    , walk
    )

import Bitwise
import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Direction as Direction exposing (Direction)
import Domain.Layer exposing (LayerNumber)
import EnumSet exposing (EnumSet)
import EnumSetMembers exposing (EnumSetMembers)
import Proto.Smeargle


{-| Creatively used bitset to represent valid movement options for a given entity.
The Integer is 8 bits of the format

    WSFRB___

    with the semantic
        W = 1 => Can walk
        S = 1 => Can swim
        F = 1 => Can fly
        R = 1 => Can run
        B = 1 => Can bike

-}
type alias MoveAbility =
    EnumSet MoveType


type MoveType
    = Walk
    | Swim
    | Fly
    | Run
    | Bike


walk : MoveAbility
walk =
    EnumSet.fromList moveAbilityMembers [ Walk ]


swim : MoveAbility
swim =
    EnumSet.fromList moveAbilityMembers [ Swim ]


moveAbilityMembers : EnumSetMembers MoveType
moveAbilityMembers =
    EnumSetMembers.contiguous [ Walk, Swim, Fly, Run, Bike ]


noMoveAbility : MoveAbility
noMoveAbility =
    EnumSet.fromInt 0


{-| Creatively used bitset to represent movement options for a given tile.
The format is

        8 bytes for left    8 bytes for right
                V                 V
    UUUUUUUU LLLLLLLL DDDDDDDD RRRRRRRR
        ^                 ^
    8 bytes for up     8 bytes for down

where the bytes for each direction have the following format:

    WSFRBUD_

    with the semantic
        W = 1 => Can move by walking
        S = 1 => Can move by swimming
        F = 1 => Can move by flying
        R = 1 => Can move by running
        B = 1 => Can move by biking
        U = 1 => Should move a layer up during the movement
        D = 1 => Should move a layer down during the movement

-}
type MoveRule
    = MoveRule Int


inDirection : Direction -> MoveRule -> DirectionalMoveRule
inDirection direction (MoveRule rule) =
    let
        shift =
            case direction of
                Direction.U ->
                    0

                Direction.L ->
                    8

                Direction.D ->
                    16

                Direction.R ->
                    24
    in
    Bitwise.shiftRightBy shift rule
        |> Bitwise.and 255
        |> EnumSet.fromInt


{-| 8 bytes in the WSFRBUD\_ format described above
-}
type alias DirectionalMoveRule =
    EnumSet InternalMoveAbility


type InternalMoveAbility
    = MoveT MoveType
    | LayerUp
    | LayerDown


internalMoveAbilityMembers : EnumSetMembers InternalMoveAbility
internalMoveAbilityMembers =
    EnumSetMembers.contiguous
        [ MoveT Walk
        , MoveT Swim
        , MoveT Fly
        , MoveT Run
        , MoveT Bike
        , LayerUp
        , LayerDown
        ]


layerNumberShift : DirectionalMoveRule -> Int
layerNumberShift m =
    case
        ( EnumSet.contains internalMoveAbilityMembers LayerUp m
        , EnumSet.contains internalMoveAbilityMembers LayerDown m
        )
    of
        ( True, False ) ->
            1

        ( False, True ) ->
            -1

        _ ->
            0


type MoveRules
    = MoveRules (Dict InternalKey MoveRule)


type alias InternalKey =
    -- (X Coord, Y Coord, LayerNumber)
    ( Int, Int, LayerNumber )


rulesFromProto : List Proto.Smeargle.Tile -> MoveRules
rulesFromProto =
    List.map ruleFromProto
        >> Dict.fromList
        >> MoveRules


ruleFromProto : Proto.Smeargle.Tile -> ( InternalKey, MoveRule )
ruleFromProto { x, y, layerNumber, rule } =
    ( ( x, y, layerNumber ), MoveRule rule )


{-| This is to sanity check our implementation against the server side rust implementation.
Do not use in Frontend code.
-}
canMoveFromToCheck : { position : { x : Int, y : Int, layerNumber : Int }, direction : Int, moveAbility : Int, moveRules : List { x : Int, y : Int, layerNumber : Int, rule : Int } } -> { layerNumber : Int, success : Bool }
canMoveFromToCheck inputs =
    let
        coordinate =
            { x = inputs.position.x, y = inputs.position.y }

        layerNumber =
            inputs.position.layerNumber

        direction =
            case inputs.direction of
                0 ->
                    Direction.L

                1 ->
                    Direction.U

                2 ->
                    Direction.R

                _ ->
                    Direction.D

        moveAbility =
            EnumSet.fromInt inputs.moveAbility

        moveRules =
            inputs.moveRules
                |> List.map (\rule -> ( ( rule.x, rule.y, rule.layerNumber ), MoveRule rule.rule ))
                |> Dict.fromList
                |> MoveRules

        mapSuccess optLayerNumber =
            case optLayerNumber of
                Just n ->
                    { layerNumber = n, success = True }

                Nothing ->
                    { layerNumber = 0, success = False }
    in
    canMoveFromTo ( coordinate, layerNumber ) direction moveAbility moveRules
        |> mapSuccess


canMoveFromTo :
    ( Coordinate Int, LayerNumber )
    -> Direction
    -> MoveAbility
    -> MoveRules
    -> Maybe LayerNumber
canMoveFromTo ( currentCoord, currentLayerNumber ) direction moveAbility rules =
    let
        mayRule =
            getRule ( currentCoord.x, currentCoord.y, currentLayerNumber ) rules
                |> Maybe.map (inDirection direction)

        moveAbilityAsRule =
            EnumSet.map MoveT
                moveAbilityMembers
                internalMoveAbilityMembers
                moveAbility
    in
    case mayRule of
        Just rule ->
            if EnumSet.intersect moveAbilityAsRule rule |> EnumSet.isEmpty then
                Nothing

            else
                Just <| currentLayerNumber + layerNumberShift rule

        _ ->
            Nothing


getMovableDirections :
    ( Coordinate Int, LayerNumber )
    -> MoveAbility
    -> MoveRules
    -> List ( Direction, LayerNumber )
getMovableDirections pos abilities rules =
    Direction.all
        |> List.filterMap
            (\dir ->
                canMoveFromTo pos dir abilities rules
                    |> Maybe.map (Tuple.pair dir)
            )


getRule : InternalKey -> MoveRules -> Maybe MoveRule
getRule pos (MoveRules moveRules) =
    Dict.get pos moveRules



-- Test Utilities


groundRules : Int -> List { x : Int, y : Int } -> MoveRules
groundRules layerNumber coords =
    coords
        -- Magic Constant generated in Backend - movement in all 4 directions allowing Fly, Walk, Run and Bike
        |> List.map (\{ x, y } -> ( ( x, y, layerNumber ), MoveRule 488447261 ))
        |> Dict.fromList
        |> MoveRules
