module Domain.Player exposing
    ( Player
    , PlayerChanged
    , Players
    , addPlayer
    , continueMoves
    , empty
    , getPlayer
    , getPlayers
    , movePlayer
    , removePlayer
    , setTrainerNumber
    , switchMapTo
    )

import Domain.Coordinate exposing (Coordinate)
import Domain.Direction exposing (Direction)
import Domain.Layer exposing (LayerNumber)
import Domain.Position exposing (StandingPosition)
import Domain.QueueablePosition as QueueablePosition exposing (QueueablePosition)
import MapId exposing (MapId)
import Maybe.Extra
import Shared.Types.Generic exposing (Setter)
import Shared.Types.TrainerState as TrainerState exposing (TrainerState)
import TrainerNumber exposing (TrainerNumber)
import UserId exposing (UserId)
import UserIdDict exposing (UserIdDict)


type alias Player =
    { id : UserId
    , name : String
    , position : QueueablePosition
    , state : TrainerState
    , trainerNumber : Maybe TrainerNumber

    {- Necessary here, because of switching maps:
       We need to render players of the old map when the new map (and players on that map) is loading
    -}
    , mapId : MapId
    }


type alias PlayerChanged =
    { id : UserId
    , position : Coordinate Int
    , layerNumber : LayerNumber
    , direction : Direction
    , mapId : MapId
    }


type alias PlayerConnected =
    { id : UserId
    , name : String
    , position : Coordinate Int
    , direction : Direction
    , layerNumber : LayerNumber
    , trainerNumber : TrainerNumber
    , mapId : MapId
    }


type alias Players =
    UserIdDict Player


empty : Players
empty =
    UserIdDict.empty


{-| Move a player identified by his id to the given position.
If the player does not exist yet, he or she is created with default values to avoid race conditions
with similar functions. For example on page load, the move might arrive before the loading of connected players.
-}
movePlayer : PlayerChanged -> Setter Players
movePlayer { id, mapId, position, layerNumber, direction } players =
    UserIdDict.update id
        (\val ->
            case val of
                Just player ->
                    let
                        newPos =
                            QueueablePosition.moveToCoord position layerNumber direction player.position

                        dir =
                            QueueablePosition.getCurrentDirection newPos
                    in
                    Just { player | position = newPos, state = TrainerState.changeDirection dir player.state }

                _ ->
                    Just
                        { id = id
                        , mapId = mapId
                        , position = QueueablePosition.init position layerNumber direction
                        , state = TrainerState.init
                        , trainerNumber = Nothing
                        , name = ""
                        }
        )
        players


addPlayer : PlayerConnected -> Setter Players
addPlayer { id, mapId, name, position, layerNumber, trainerNumber, direction } players =
    UserIdDict.update id
        (\val ->
            case val of
                Just player ->
                    Just { player | trainerNumber = Just trainerNumber, name = name }

                _ ->
                    Just
                        { id = id
                        , position = QueueablePosition.init position layerNumber direction
                        , state = TrainerState.init
                        , trainerNumber = Just trainerNumber
                        , name = name
                        , mapId = mapId
                        }
        )
        players


removePlayer : UserId -> Setter Players
removePlayer =
    UserIdDict.remove


{-| Switch the map, implying marking all players to be on another map until proven otherwise (by a websocket message)
-}
switchMapTo : MapId -> Setter Players
switchMapTo mapId =
    UserIdDict.filter (\_ player -> player.mapId == mapId)


continueMoves : Float -> Players -> ( Players, List ( UserId, StandingPosition ) )
continueMoves dt players =
    UserIdDict.foldl
        (\userId player ( playersAcc, standingPositionsAcc ) ->
            let
                updatedPlayer =
                    movePlayerDt dt player

                updatedPlayers =
                    UserIdDict.insert userId updatedPlayer playersAcc
            in
            case
                QueueablePosition.isStandingPosition updatedPlayer.position
                    |> Maybe.Extra.filter (\_ -> QueueablePosition.hasMoved player.position updatedPlayer.position || QueueablePosition.getCurrentDirection player.position /= QueueablePosition.getCurrentDirection updatedPlayer.position)
            of
                Just standingPosition ->
                    ( updatedPlayers, ( userId, standingPosition ) :: standingPositionsAcc )

                Nothing ->
                    ( updatedPlayers, standingPositionsAcc )
        )
        ( UserIdDict.empty, [] )
        players


movePlayerDt : Float -> Player -> Player
movePlayerDt dt player =
    let
        newPos =
            QueueablePosition.continueMove dt player.position

        action =
            if QueueablePosition.hasMoved player.position newPos then
                TrainerState.move dt

            else
                TrainerState.stop

        newState =
            player.state
                |> TrainerState.changeDirection (QueueablePosition.getCurrentDirection newPos)
                |> action
    in
    { player | position = newPos, state = newState }


getPlayer : UserId -> Players -> Maybe Player
getPlayer =
    UserIdDict.get


getPlayers : MapId -> Players -> List Player
getPlayers mapId players =
    let
        onCurrentMap player =
            player.mapId == mapId
    in
    UserIdDict.values players |> List.filter onCurrentMap


setTrainerNumber : TrainerNumber -> UserId -> Setter Players
setTrainerNumber trainerNumber userId =
    UserIdDict.update userId
        (Maybe.map <|
            \player ->
                { player | trainerNumber = Just trainerNumber }
        )
