module Domain.PartnerPokemon exposing
    ( PartnerPokemon
    , PartnerPokemons
    , continueMoves
    , fullyInitialized
    , getPartnerPosition
    , init
    , registerMove
    , setPartnerInfo
    )

import Config
import Domain.Coordinate exposing (Coordinate)
import Domain.Direction exposing (Direction)
import Domain.Encounter as Encounter exposing (Action, CurrentAction(..))
import Domain.Layer exposing (LayerNumber)
import Domain.QueueablePosition as QueueablePosition exposing (QueueablePosition)
import PokemonNumber exposing (PokemonNumber)
import PokemonNumberDict exposing (PokemonNumberDict)
import Proto.Kangaskhan
import Shared.Types.FrameBased as FrameBased
import Shared.Types.Generic exposing (Setter)
import UserId exposing (UserId)
import UserIdDict exposing (UserIdDict)


type alias PartnerPokemon =
    { position : QueueablePosition
    , pokemonNumber : PokemonNumber
    , shinyStatus : Bool
    , action : Action
    }


type PartnerPokemonState
    = JustPosition
        { position : QueueablePosition
        , action : Action
        }
    | JustPokemon
        { pokemonNumber : PokemonNumber
        , shinyStatus : Bool
        }
    | FullyInitialized PartnerPokemon


type alias PartnerPokemons =
    UserIdDict PartnerPokemonState


init : PartnerPokemons
init =
    UserIdDict.empty


initAction : Action
initAction =
    FrameBased.initFrameBased StepLeft
        Encounter.rotate
        Config.trainerAnimationSpeed


fullyInitialized : PartnerPokemonState -> Maybe PartnerPokemon
fullyInitialized state =
    case state of
        FullyInitialized p ->
            Just p

        _ ->
            Nothing


registerMove : UserId -> Coordinate Int -> LayerNumber -> Direction -> Setter PartnerPokemons
registerMove userId coord layerNumber direction =
    UserIdDict.update userId <|
        \partner ->
            Just <|
                case partner of
                    Nothing ->
                        JustPosition { position = QueueablePosition.init coord layerNumber direction, action = initAction }

                    Just (JustPosition p) ->
                        JustPosition { p | position = QueueablePosition.moveToCoord coord layerNumber direction p.position }

                    Just (JustPokemon p) ->
                        FullyInitialized
                            { pokemonNumber = p.pokemonNumber
                            , shinyStatus = p.shinyStatus
                            , position = QueueablePosition.init coord layerNumber direction
                            , action = initAction
                            }

                    Just (FullyInitialized p) ->
                        FullyInitialized
                            { p
                                | position = QueueablePosition.moveToCoord coord layerNumber direction p.position
                            }


setPartnerInfo : UserId -> PokemonNumber -> Bool -> Setter PartnerPokemons
setPartnerInfo userId pokemonNumber isShiny =
    UserIdDict.update userId <|
        \partner ->
            Just <|
                case partner of
                    Nothing ->
                        JustPokemon { pokemonNumber = pokemonNumber, shinyStatus = isShiny }

                    Just (JustPokemon _) ->
                        JustPokemon { pokemonNumber = pokemonNumber, shinyStatus = isShiny }

                    Just (JustPosition p) ->
                        FullyInitialized
                            { pokemonNumber = pokemonNumber
                            , shinyStatus = isShiny
                            , position = p.position
                            , action = p.action
                            }

                    Just (FullyInitialized p) ->
                        FullyInitialized
                            { p
                                | pokemonNumber = pokemonNumber
                                , shinyStatus = isShiny
                            }


continueMoves : Float -> PokemonNumberDict Proto.Kangaskhan.GetPokemonResponse -> Setter PartnerPokemons
continueMoves dt pokemonDataDict =
    UserIdDict.map
        (\_ partner ->
            case partner of
                FullyInitialized p ->
                    case PokemonNumberDict.get p.pokemonNumber pokemonDataDict of
                        Just pokemonData ->
                            let
                                dtModified =
                                    min (dt / pokemonData.moveDurationInMs) (dt * Config.trainerSpeed)
                            in
                            continuePartnerMove dtModified p
                                |> FullyInitialized

                        _ ->
                            partner

                _ ->
                    partner
        )


continuePartnerMove :
    Float
    -> PartnerPokemon
    -> PartnerPokemon
continuePartnerMove dt partner =
    { partner | position = QueueablePosition.continueMove dt partner.position, action = FrameBased.nextFrame dt partner.action }


getPartnerPosition : UserId -> PartnerPokemons -> Maybe QueueablePosition
getPartnerPosition userId pokemons =
    UserIdDict.get userId pokemons
        |> Maybe.andThen
            (\state ->
                case state of
                    JustPokemon _ ->
                        Nothing

                    JustPosition p ->
                        Just p.position

                    FullyInitialized p ->
                        Just p.position
            )
