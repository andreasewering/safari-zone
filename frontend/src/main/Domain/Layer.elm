module Domain.Layer exposing (..)


type alias LayerNumber =
    Int


initialLayerNumber : Int
initialLayerNumber =
    0
