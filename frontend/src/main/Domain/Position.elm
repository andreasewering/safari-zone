module Domain.Position exposing
    ( Position(..)
    , StandingPosition
    , comingFrom
    , getCurrentDirection
    , getCurrentLayerNumber
    , getExact
    , getNextTile
    , hasMoved
    , init
    , interact
    , isStandingPosition
    , move
    , setDirection
    , setTile
    )

import Data.Units exposing (TileUnit(..), unTileUnit)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Direction exposing (Direction(..))
import Domain.Layer exposing (LayerNumber)


type Position
    = OnTile StandingPosition
    | Moving { coord : Coordinate Float, oldLayerNumber : LayerNumber, newLayerNumber : LayerNumber, direction : Direction }


type alias StandingPosition =
    { coord : Coordinate Int, layerNumber : LayerNumber, direction : Direction }



-- Coordinate is tile coordinate


init : Coordinate Int -> LayerNumber -> Direction -> Position
init c layerNumber direction =
    OnTile { coord = c, layerNumber = layerNumber, direction = direction }


floorCoord : Direction -> Coordinate Float -> Coordinate Int
floorCoord dir =
    Coordinate.map
        (case dir of
            L ->
                ceiling

            R ->
                floor

            U ->
                ceiling

            D ->
                floor
        )


comingFrom : Position -> Coordinate Int
comingFrom pos =
    getExact pos |> Coordinate.map unTileUnit |> floorCoord (getCurrentDirection pos)


ceilCoord : Direction -> Coordinate Float -> Coordinate Int
ceilCoord dir =
    Coordinate.map
        (case dir of
            L ->
                floor

            R ->
                ceiling

            U ->
                floor

            D ->
                ceiling
        )


isStandingPosition : Position -> Maybe StandingPosition
isStandingPosition pos =
    case pos of
        OnTile standing ->
            Just standing

        Moving _ ->
            Nothing


getCurrentDirection : Position -> Direction
getCurrentDirection pos =
    case pos of
        OnTile { direction } ->
            direction

        Moving { direction } ->
            direction


getCurrentLayerNumber : Position -> LayerNumber
getCurrentLayerNumber pos =
    case pos of
        OnTile { layerNumber } ->
            layerNumber

        Moving { oldLayerNumber, newLayerNumber } ->
            max oldLayerNumber newLayerNumber


getNextTile : Position -> Coordinate Int
getNextTile pos =
    case pos of
        Moving { coord, direction } ->
            ceilCoord direction coord

        OnTile { coord } ->
            coord


move : Maybe Direction -> LayerNumber -> Float -> Position -> Position
move maybeNewDir nextLayerNumber dt pos =
    case ( pos, maybeNewDir ) of
        ( Moving moving, _ ) ->
            let
                newPos =
                    Domain.Direction.directionToCoord moving.direction
                        |> Coordinate.times (min dt 1)
                        |> Coordinate.plus moving.coord
            in
            if floorCoord moving.direction newPos == floorCoord moving.direction moving.coord then
                Moving { moving | coord = newPos }

            else
                OnTile { coord = floorCoord moving.direction newPos, layerNumber = moving.newLayerNumber, direction = moving.direction }

        ( OnTile onTile, Just dir ) ->
            let
                newPos =
                    Domain.Direction.directionToCoord dir
                        |> Coordinate.times dt
                        |> Coordinate.plus (Coordinate.toFloat onTile.coord)

                flooredTarget =
                    floorCoord dir newPos
            in
            if dt == 0 then
                OnTile { onTile | direction = dir }

            else if flooredTarget == onTile.coord then
                Moving
                    { coord = newPos
                    , oldLayerNumber = onTile.layerNumber
                    , newLayerNumber = nextLayerNumber
                    , direction = dir
                    }

            else
                OnTile { coord = flooredTarget, layerNumber = nextLayerNumber, direction = dir }

        ( OnTile _, Nothing ) ->
            pos


hasMoved : Position -> Position -> Bool
hasMoved pos1 pos2 =
    getExact pos1 /= getExact pos2


getExact : Position -> Coordinate TileUnit
getExact pos =
    case pos of
        Moving { coord } ->
            Coordinate.map TileUnit coord

        OnTile { coord } ->
            Coordinate.map (toFloat >> TileUnit) coord


setTile : Coordinate Int -> LayerNumber -> Position -> Position
setTile coord layerNumber pos =
    OnTile { coord = coord, layerNumber = layerNumber, direction = getCurrentDirection pos }


setDirection : Direction -> Position -> Position
setDirection dir pos =
    case pos of
        Moving m ->
            Moving { m | direction = dir }

        OnTile o ->
            OnTile { o | direction = dir }


interact : Position -> Maybe (Coordinate Int)
interact position =
    case position of
        Moving _ ->
            Nothing

        OnTile { coord, direction } ->
            Domain.Direction.directionToCoord direction
                |> Coordinate.plus coord
                |> Just
