module Domain.ConstrainedPosition exposing (move)

import Domain.Direction exposing (Direction)
import Domain.MoveRule as MoveRule exposing (MoveRules, canMoveFromTo)
import Domain.Position exposing (Position(..))


move : Maybe Direction -> Float -> MoveRules -> Position -> Position
move dir dt rules pos =
    case ( pos, dir ) of
        ( Moving { newLayerNumber }, _ ) ->
            Domain.Position.move dir newLayerNumber dt pos

        ( OnTile { coord, layerNumber }, Just newDir ) ->
            let
                mayNextLayerNumber =
                    canMoveFromTo ( coord, layerNumber ) newDir MoveRule.walk rules

                nextLayerNumber =
                    mayNextLayerNumber |> Maybe.withDefault layerNumber

                constrainedDt =
                    case mayNextLayerNumber of
                        Just _ ->
                            dt

                        Nothing ->
                            0
            in
            Domain.Position.move dir nextLayerNumber constrainedDt pos

        ( OnTile _, Nothing ) ->
            pos
