module Api.WebsocketApi exposing (..)

import Domain.Coordinate exposing (Coordinate)
import Domain.Direction as Direction exposing (Direction)
import Interop.WebSocket
import Item exposing (Item)
import MapId exposing (MapId)
import Proto.Kangaskhan.WebsocketMessageToServer.Message


sendMove : MapId -> Direction -> Cmd msg
sendMove mapId =
    Direction.toProto
        >> Proto.Kangaskhan.WebsocketMessageToServer.Message.PlayerMove
        >> Interop.WebSocket.sendKangaskhanMsg mapId


sendTurn : MapId -> Direction -> Cmd msg
sendTurn mapId =
    Direction.toProto
        >> Proto.Kangaskhan.WebsocketMessageToServer.Message.PlayerTurn
        >> Interop.WebSocket.sendKangaskhanMsg mapId


throwBall : MapId -> Item -> ( Float, Coordinate Float ) -> Cmd msg
throwBall mapId item ( angle, { x, y } ) =
    Proto.Kangaskhan.WebsocketMessageToServer.Message.ThrowBall
        { vectorX = x
        , vectorY = y
        , angleRadians = angle
        , item = Item.toProto item
        }
        |> Interop.WebSocket.sendKangaskhanMsg mapId
