import { GrpcWebFetchTransport } from '@protobuf-ts/grpcweb-transport';
import { GardevoirServiceClient } from '../generated/gardevoir.client.js';

const rpc = new GrpcWebFetchTransport({ baseUrl: window.location.origin });
const gardevoir = new GardevoirServiceClient(rpc);

let activeMessageEl: HTMLElement | null = null;

async function onSubmit() {
  const username = document.querySelector<HTMLInputElement>('#username')?.value;
  const email = document.querySelector<HTMLInputElement>('#email')?.value;
  const password = document.querySelector<HTMLInputElement>('#password')?.value;
  if (!username || !email || !password) {
    return;
  }

  const signupButton = document.querySelector('form > button');
  signupButton?.setAttribute('disabled', '');
  const loadingIndicator = document.querySelector('loading-indicator');
  loadingIndicator?.classList.toggle('hidden');

  const messageEls = document.querySelectorAll('[id^="error_"]');
  messageEls.forEach((el) => {
    el.classList.add('h-0');
    el.classList.remove('px-4', 'py-2');
  });
  const transitionEnd = activeMessageEl
    ? waitForTransition(activeMessageEl)
    : Promise.resolve();

  const request = gardevoir.register({ username, email, password });
  const [response] = await Promise.allSettled([request, transitionEnd]);

  loadingIndicator?.classList.toggle('hidden');
  signupButton?.removeAttribute('disabled');

  if (response.status === 'rejected') {
    const errorMessageEl = getErrorMessageElement(response.reason);

    errorMessageEl?.classList.remove('h-0');
    errorMessageEl?.classList.add('px-4', 'py-2');
    activeMessageEl = errorMessageEl;
    return;
  }

  const successMessageEl = document.getElementById('success');
  successMessageEl?.classList.remove('h-0');
  successMessageEl?.classList.add('px-4', 'py-2');
  activeMessageEl = successMessageEl;
}

document.querySelector('form')?.addEventListener('submit', (event) => {
  event.preventDefault();
  void onSubmit();
});

function getErrorMessageElement(reason: unknown): HTMLElement | null {
  const defaultErrorMessageEl = document.getElementById('error_DEFAULT');
  if (
    typeof reason !== 'object' ||
    reason === null ||
    !('code' in reason) ||
    typeof reason.code !== 'string'
  ) {
    return defaultErrorMessageEl;
  }
  return (
    document.getElementById(`error_${reason.code}`) ?? defaultErrorMessageEl
  );
}

function waitForTransition(element: HTMLElement): Promise<void> {
  return new Promise((resolve) => {
    element.addEventListener(
      'transitionend',
      () => {
        resolve();
      },
      { once: true }
    );
  });
}
