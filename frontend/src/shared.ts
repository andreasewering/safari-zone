export type Token = string;

export type Provider = 'andrena' | 'local';

export type Authentification = {
  access_token: Token;
  lifespan: number;
};

// expected form: #something=value&else=other...
export function hashToMap(hash: string): Record<string, string> {
  return Object.fromEntries<string>(
    hash
      .slice(1)
      .split('&')
      .map((pair) => pair.split('='))
      .filter((pair): pair is [string, string] => pair.length == 2)
  );
}

export function mapToHash(map: Record<string, string | number>): string {
  return (
    '#' +
    Object.entries(map)
      .map(([key, value]) => `${key}=${value.toString()}`)
      .join('&')
  );
}

export const redirectTo = (url: string): Promise<never> => {
  window.location.href = url;
  return new Promise(() => {
    // This is fine since we leave the page so any JS after this will not execute.
  });
};

export type PromiseSettledResultWithError<T, E> =
  | PromiseFulfilledResult<T>
  | (Pick<PromiseRejectedResult, 'status'> & { reason: E });
