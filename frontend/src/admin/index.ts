import intl_proxy from 'intl-proxy';
import { refreshLocalToken } from '../login/login-handler.js';
import '../components/loading-indicator.js';
import '../components/tile-app.js';
import { Authentification, hashToMap, redirectTo } from '../shared.js';
import { getCurrentThemeName } from '../components/theme.js';
import { Elm as ElmBundle } from 'Admin.elm';
import { AudioMsg, audioPort } from '../main/Interop/audio.js';
import { lazy } from '../lib/lazy.js';
import { UnloadInterceptor } from './unsaved-changes.js';

// Will be provided by vite
declare const __VERSION__: string;
declare const __DEV__: boolean;

if (__DEV__) {
  await import('../generated/Proto/dev-tools.mjs').then(() => {
    console.warn('Grpc Dev Tools enabled.');
  });
}

const preferredLanguage =
  window.localStorage.getItem('preferred_language') ?? null;
const language =
  window.navigator.language || (window.navigator.languages[0] ?? 'en');

export type AdminConfig = {
  auth: Authentification;
  language: string;
  preferredLanguage: string | null;
  preferredColorTheme: string;
  version: string;
};

const elm = ElmBundle.Admin;

const initialPath = window.location.pathname + window.location.search;

export const { lifespan, access_token } = hashToMap(window.location.hash);

const authenticateViaToken = async (): Promise<
  Authentification | undefined
> => {
  const intLifespan = Number.parseInt(lifespan ?? '');

  if (!Number.isNaN(intLifespan) && access_token) {
    return { access_token, lifespan: intLifespan };
  }
  return refreshLocalToken();
};

const handleAuthentification = async (): Promise<Authentification> => {
  const auth = await authenticateViaToken();
  if (auth) {
    return auth;
  }
  return redirectTo(`/login/?r=${initialPath}`);
};

export const startSPA = ({
  auth,
  language,
  preferredLanguage,
  preferredColorTheme,
  version,
}: AdminConfig) => {
  const fontSize = parseInt(
    getComputedStyle(document.documentElement).fontSize,
    10
  );

  const app = elm.init({
    flags: {
      auth,
      intl: intl_proxy,
      language,
      preferredLanguage,
      preferredColorTheme,
      audioOn: false,
      now: new Date().getTime(),
      x: window.innerWidth,
      y: window.innerHeight,
      version,
      fontSize,
    },
  });

  const { interopFromElm, interopToElm } = app.ports;

  const sendAudio = (audioMsg: AudioMsg) => {
    interopToElm.send({ tag: 'audio', data: audioMsg });
  };

  const handleAudioMsg = audioPort(sendAudio);

  const unloadInterceptor = lazy(
    () =>
      new UnloadInterceptor(() => {
        interopToElm.send({ tag: 'unload_page' });
      })
  );

  interopFromElm.subscribe((fromElm) => {
    switch (fromElm.tag) {
      case 'audio':
        handleAudioMsg(fromElm.data);
        return;
      case 'unsaved_changes':
        unloadInterceptor.value.intercept(fromElm.data);
    }
  });
};

void (async () => {
  const auth = await handleAuthentification();
  startSPA({
    auth,
    language,
    preferredLanguage,
    preferredColorTheme: getCurrentThemeName(),
    version: __VERSION__,
  });
})();
