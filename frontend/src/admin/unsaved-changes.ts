export class UnloadInterceptor {
  #stopUnload = false;

  constructor(beforeUnload: () => void) {
    window.addEventListener('beforeunload', (event) => {
      if (this.#stopUnload) {
        event.preventDefault();
        beforeUnload();
      }
    });
  }

  intercept(doIntercept: boolean) {
    this.#stopUnload = doIntercept;
  }
}
