module Admin exposing (..)

import Admin.AreaEditor
import Admin.AreaEditor.Model
import Admin.AreaEditor.Route as AreaEditor
import Admin.Detail
import Admin.Detail.Model
import Admin.Flags
import Admin.InteropDefinitions
import Admin.InteropPorts
import Admin.Language as Language
import Admin.Overview
import Admin.Page404 as Page404
import Admin.RequestManager as RequestManager
import Admin.Route as Route exposing (KnownRoute)
import Admin.Session as Session exposing (Session)
import Admin.Translations as Translations
import AreaId exposing (AreaId)
import AreaIdDict
import AudioTrackNumberDict
import Auth exposing (Authentification)
import BoundedDict
import Browser exposing (Document, UrlRequest)
import Browser.Events
import Browser.Navigation as Navigation
import Dict
import Html
import Html.WithContext
import ItemDict
import Json.Decode as D
import MapId exposing (MapId)
import PokemonNumberDict
import Shared.Background as Background
import Shared.Types.Error as Error
import Shared.UpdateUtil as UpdateUtil
import Shared.View.Theme as Theme
import Task
import Time
import TrainerNumberDict
import Url exposing (Url)


type Msg
    = DetailMsg MapId Admin.Detail.Msg
    | OverviewMsg Admin.Overview.Msg
    | AreaEditorMsg MapId AreaId AreaEditor.Route Admin.AreaEditor.Msg
    | SharedMsg Session.Msg
      -- Navigation
    | OnUrlChange Url
    | OnUrlRequest UrlRequest
      -- Auth
    | AuthRefreshTime Time.Posix
    | AuthRefreshed (Result Error.Error Authentification)
    | ReceivedMessageFromJS (Result D.Error Admin.InteropDefinitions.ToElm)


type alias Model =
    Session
        { detailModel : Admin.Detail.Model.PageModel
        , overviewModel : Admin.Overview.PageModel
        , areaEditorModel : Admin.AreaEditor.Model.PageModel
        }


initializePage : KnownRoute -> Model -> ( Model, Cmd Msg )
initializePage route model =
    case route of
        Route.Overview ->
            Admin.Overview.onPageLoad overviewCtx model

        Route.Detail mapId ->
            Admin.Detail.onPageLoad (detailCtx mapId) model

        Route.EditArea mapId areaId areaEditorRoute ->
            Admin.AreaEditor.onPageLoad (editAreaCtx mapId areaId areaEditorRoute) model

        Route.Logout ->
            ( model, Auth.logout (\_ -> SharedMsg Session.NoOp) )


init : D.Value -> Url -> Navigation.Key -> ( MainModel, Cmd Msg )
init jsFlags url key =
    Admin.Flags.read jsFlags
        |> Result.map
            (\flags ->
                let
                    language =
                        { browser = Translations.languageFromString flags.language |> Maybe.withDefault Translations.En
                        , preferred = flags.preferredLanguage |> Maybe.andThen Translations.languageFromString
                        }

                    initialModel : Model
                    initialModel =
                        { -- Internationalization
                          language = language
                        , i18n = Translations.init { lang = Language.getActive language, path = "/translations/admin" }

                        -- Data from the server
                        , audioConfig = { tracks = AudioTrackNumberDict.empty, onPokemonObtained = Nothing }
                        , maps = []
                        , tileConfig = Dict.empty
                        , areas = BoundedDict.withCapacity 10
                        , areaDetails = AreaIdDict.empty
                        , savedTiles = BoundedDict.withCapacity 10
                        , startTiles = BoundedDict.withCapacity 10
                        , pokemon = PokemonNumberDict.empty

                        -- WebGL Textures
                        , tileset = Nothing
                        , tilemaps = BoundedDict.withCapacity 10
                        , pokemonTextures = PokemonNumberDict.empty
                        , trainerTextures = TrainerNumberDict.empty
                        , itemTextures = ItemDict.empty

                        -- Client side data
                        , width = flags.x
                        , height = flags.y
                        , fontSize = flags.fontSize
                        , colorThemeName = flags.preferredColorTheme |> Maybe.andThen Theme.fromString |> Maybe.withDefault Theme.Light
                        , timezone = Time.utc
                        , version = flags.version
                        , background = Background.fromTime flags.now Time.utc

                        -- Authentication
                        , auth = Auth.manageSingle flags.auth flags.now
                        , requestManager = RequestManager.init

                        -- Routing
                        , route = Route.toRoute url
                        , key = key

                        -- Page Models
                        , overviewModel = Admin.Overview.init
                        , detailModel = Admin.Detail.Model.init
                        , areaEditorModel = Admin.AreaEditor.Model.init
                        }

                    ( newModel, cmd ) =
                        case initialModel.route of
                            Err _ ->
                                ( initialModel, Cmd.none )

                            Ok route ->
                                initializePage route initialModel

                    getI18n =
                        Translations.loadMessages (Session.GotI18n >> SharedMsg) newModel.i18n

                    getTimezone =
                        Time.here |> Task.perform (Session.GotTimezone >> SharedMsg)
                in
                ( newModel
                , Cmd.batch [ cmd, getI18n, getTimezone ]
                )
            )
        |> orNoCmd


orNoCmd : Result x ( a, Cmd msg ) -> ( Result x a, Cmd msg )
orNoCmd r =
    case r of
        Ok ( a, cmd ) ->
            ( Ok a, cmd )

        Err err ->
            ( Err err, Cmd.none )


type alias MainModel =
    Result D.Error Model


update : Msg -> MainModel -> ( MainModel, Cmd Msg )
update msg m =
    case m of
        Err _ ->
            ( m, Cmd.none )

        Ok model ->
            updateInner msg model |> Tuple.mapFirst Ok


updateInner : Msg -> Model -> ( Model, Cmd Msg )
updateInner msg model =
    case msg of
        OverviewMsg overviewMsg ->
            Admin.Overview.update overviewCtx overviewMsg model

        DetailMsg mapId detailMsg ->
            Admin.Detail.update (detailCtx mapId) detailMsg model

        AreaEditorMsg mapId areaId route areaEditorMsg ->
            Admin.AreaEditor.update (editAreaCtx mapId areaId route) areaEditorMsg model

        SharedMsg sharedMsg ->
            onSharedMessage sharedMsg model

        OnUrlChange url ->
            let
                newRoute =
                    Route.toRoute url

                newModel =
                    { model | route = newRoute }
            in
            case newRoute of
                Err _ ->
                    ( newModel, Cmd.none )

                Ok route ->
                    initializePage route newModel

        OnUrlRequest (Browser.Internal url) ->
            ( model, Url.toString url |> Navigation.pushUrl model.key )

        OnUrlRequest (Browser.External url) ->
            ( model, Navigation.load url )

        AuthRefreshTime timestamp ->
            ( { model | auth = Auth.updateTime timestamp model.auth }
            , Auth.refreshAuth
                |> Cmd.map AuthRefreshed
            )

        AuthRefreshed (Ok auth) ->
            ( { model | auth = Auth.addToken auth model.auth }, Cmd.none )

        AuthRefreshed (Err _) ->
            ( model, Route.navigate model.key Route.Logout )

        _ ->
            ( model, Cmd.none )


onSharedMessage : Session.Msg -> Model -> ( Model, Cmd Msg )
onSharedMessage msg model =
    let
        forPage =
            case model.route of
                Ok Route.Overview ->
                    Admin.Overview.updateShared overviewCtx msg

                Ok (Route.Detail mapId) ->
                    Admin.Detail.updateShared (detailCtx mapId) msg

                Ok (Route.EditArea mapId areaId editRoute) ->
                    Admin.AreaEditor.updateShared (editAreaCtx mapId areaId editRoute) msg

                _ ->
                    UpdateUtil.none
    in
    UpdateUtil.chain
        [ Session.update msg
            |> UpdateUtil.liftCmd SharedMsg
        , forPage
        ]
        model


subscriptions : MainModel -> Sub Msg
subscriptions m =
    case m of
        Err _ ->
            Sub.none

        Ok model ->
            Sub.batch
                [ Browser.Events.onResize (\x y -> Session.Resize x y |> SharedMsg)
                , Auth.refreshReminder model.auth |> Sub.map AuthRefreshTime
                , Admin.InteropPorts.toElm |> Sub.map ReceivedMessageFromJS
                ]


view : MainModel -> Document Msg
view m =
    case m of
        Err err ->
            { title = "Something went wrong", body = [ Html.text <| D.errorToString err ] }

        Ok model ->
            viewInner model


viewInner : Model -> Document Msg
viewInner model =
    let
        context =
            { i18n = model.i18n
            , timezone = model.timezone
            , background = Background.toImage model.background
            , disabled = False
            , fontSize = model.fontSize
            }

        { title, body } =
            case model.route of
                Ok Route.Overview ->
                    Admin.Overview.view overviewCtx model

                Ok (Route.Detail mapId) ->
                    Admin.Detail.view (detailCtx mapId) model

                Ok (Route.EditArea mapId areaId areaEditorRoute) ->
                    Admin.AreaEditor.view (editAreaCtx mapId areaId areaEditorRoute) model

                Err _ ->
                    Page404.view ()

                Ok Route.Logout ->
                    Page404.view ()
    in
    { title = title model.i18n, body = List.map (Html.WithContext.toHtml context) body }


overviewCtx : Admin.Overview.Context Msg
overviewCtx =
    { lift = OverviewMsg, liftShared = SharedMsg }


detailCtx : MapId -> Admin.Detail.Context Msg
detailCtx mapId =
    { mapId = mapId, lift = DetailMsg mapId, liftShared = SharedMsg }


editAreaCtx : MapId -> AreaId -> AreaEditor.Route -> Admin.AreaEditor.Context Msg
editAreaCtx mapId areaId route =
    { areaId = areaId, mapId = mapId, route = route, lift = AreaEditorMsg mapId areaId route, liftShared = SharedMsg }


main : Program D.Value (Result D.Error Model) Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlRequest = OnUrlRequest
        , onUrlChange = OnUrlChange
        }
