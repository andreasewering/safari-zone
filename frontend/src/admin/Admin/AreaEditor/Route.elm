module Admin.AreaEditor.Route exposing (..)

import UrlCodec exposing (UrlCodec, buildUnion, s, top, union, variant0)


type Route
    = Layout
    | Metadata
    | Encounters
    | Items


codec : UrlCodec (Route -> a) a Route
codec =
    union
        (\layout metadata encounters items value ->
            case value of
                Layout ->
                    layout

                Metadata ->
                    metadata

                Encounters ->
                    encounters

                Items ->
                    items
        )
        |> variant0 Layout top
        |> variant0 Metadata (s "metadata")
        |> variant0 Encounters (s "encounters")
        |> variant0 Items (s "items")
        |> buildUnion
