module Admin.AreaEditor.Model exposing (AreaDetail, EncounterDragId, EncounterDragState, EncounterDropId, ItemDragId, ItemDragState, ItemDropId, PageModel, init, setAreaDetail)

import Admin.Detail.Grid as Grid
import Admin.Detail.Types.Encounters as Encounters exposing (Encounters)
import Admin.Detail.Types.ItemProbabilities as ItemProbabilities exposing (ItemProbabilities)
import Admin.Detail.Types.Selection as Selection exposing (Selection)
import Admin.LanguageDict as LanguageDict exposing (LanguageDict)
import Admin.Translations as Translations
import Admin.View.DragAndDrop as DragAndDrop
import AreaId exposing (AreaId)
import AreaIdDict exposing (AreaIdDict)
import AudioTrackNumber exposing (AudioTrackNumber)
import Config
import Dict
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Item exposing (Item)
import PokemonNumber exposing (PokemonNumber)
import Proto.Arceus exposing (GetAreaDetailResponse)
import Proto.Arceus.SpawnKind exposing (SpawnKind)
import Proto.Kangaskhan.Type exposing (Type)
import Shared.Types.Cuboid as Rectangle exposing (Cuboid)
import Tagged exposing (tag)


type alias PageModel =
    { name : LanguageDict String
    , audioTrackNumber : Maybe AudioTrackNumber
    , encounterProbabilities : Encounters
    , itemProbabilities : ItemProbabilities
    , parts : List Cuboid
    , requestedTranslation : Maybe ( Translations.Language, String )
    , dragState : Grid.DragState
    , gridMode : Grid.GridMode
    , selection : Selection
    , tileSize : Int
    , offset : Coordinate Int
    , layerNumber : LayerNumber
    , encounterDragState : EncounterDragState
    , encounterSearchInput : String
    , encounterSearchType : Maybe Type
    , itemDragState : ItemDragState
    }


type alias EncounterDragState =
    DragAndDrop.State EncounterDragId EncounterDropId


type alias EncounterDragId =
    ( PokemonNumber, Maybe SpawnKind )


type alias EncounterDropId =
    Maybe SpawnKind


type alias ItemDragState =
    DragAndDrop.State ItemDragId ItemDropId


type alias ItemDragId =
    ( Item, Maybe () )


type alias ItemDropId =
    Maybe ()


type alias AreaDetail =
    { name : String
    , audioTrackNumber : Maybe AudioTrackNumber
    , encounterProbabilities : Encounters
    , itemProbabilities : ItemProbabilities
    , parts : List Cuboid
    }


init : PageModel
init =
    { name = LanguageDict.constant ""
    , audioTrackNumber = Nothing
    , encounterProbabilities = Dict.empty
    , itemProbabilities = ItemProbabilities.init
    , parts = []
    , requestedTranslation = Nothing
    , gridMode = Grid.SelectMulti
    , dragState = Grid.WaitingForDrag
    , offset = { x = 0, y = 0 }
    , layerNumber = 0
    , selection = Selection.init
    , tileSize = Config.tileSize
    , encounterDragState = DragAndDrop.init
    , encounterSearchInput = ""
    , encounterSearchType = Nothing
    , itemDragState = DragAndDrop.init
    }


setAreaDetail :
    AreaId
    -> { m | areaEditorModel : PageModel, areaDetails : AreaIdDict GetAreaDetailResponse }
    -> { m | areaEditorModel : PageModel, areaDetails : AreaIdDict GetAreaDetailResponse }
setAreaDetail areaId ({ areaEditorModel, areaDetails } as model) =
    let
        newAreaEditorModel =
            AreaIdDict.get areaId areaDetails |> Maybe.map (\proto -> fromProto proto areaEditorModel)
    in
    { model
        | areaEditorModel = newAreaEditorModel |> Maybe.withDefault areaEditorModel
    }


fromProto : GetAreaDetailResponse -> PageModel -> PageModel
fromProto response model =
    let
        parts =
            List.map Rectangle.fromProto response.areaParts
    in
    { model
        | name = response.name |> Maybe.withDefault model.name
        , audioTrackNumber = Maybe.map tag response.backgroundMusicTrack
        , encounterProbabilities = Encounters.fromProto response.encounters
        , itemProbabilities = ItemProbabilities.fromProto response.items
        , parts = parts
        , selection =
            Rectangle.fromCuboids parts
                |> Selection.fromCoords
    }
