module Admin.InteropDefinitions exposing (Flags, FromElm(..), ToElm(..), interop)

import Admin.Flags
import Interop.Audio.In
import Interop.Audio.Out
import TsJson.Decode as TsDecode exposing (Decoder)
import TsJson.Encode as TsEncode exposing (Encoder)


type alias Flags =
    Admin.Flags.Flags


interop :
    { toElm : Decoder ToElm
    , fromElm : Encoder FromElm
    , flags : Decoder Flags
    }
interop =
    { toElm = toElm
    , fromElm = fromElm
    , flags = Admin.Flags.decoder
    }


type FromElm
    = AudioOut Interop.Audio.Out.Msg
    | RegisterUnsavedChanges Bool


type ToElm
    = AudioIn Interop.Audio.In.Msg
    | UnloadingPage


fromElm : Encoder FromElm
fromElm =
    TsEncode.union
        (\vAudio vUnsavedChanges value ->
            case value of
                AudioOut a ->
                    vAudio a

                RegisterUnsavedChanges props ->
                    vUnsavedChanges props
        )
        |> TsEncode.variantTagged "audio" Interop.Audio.Out.encoder
        |> TsEncode.variantTagged "unsaved_changes"
            TsEncode.bool
        |> TsEncode.buildUnion


toElm : Decoder ToElm
toElm =
    TsDecode.discriminatedUnion "tag"
        [ ( "audio"
          , TsDecode.field "data" Interop.Audio.In.decoder |> TsDecode.map AudioIn
          )
        , ( "unload_page"
          , TsDecode.succeed UnloadingPage
          )
        ]
