module Admin.Language exposing
    ( ConfigurableLanguage
    , fromProto
    , getActive
    , isBrowserLanguagePreferred
    , removePreferred
    , setPreferred
    , toProto
    , toString
    )

import Admin.Translations as Translations
import Proto.Data.Locale exposing (Locale)
import Shared.Types.Generic exposing (Setter)


type alias ConfigurableLanguage =
    { preferred : Maybe Translations.Language
    , browser : Translations.Language
    }


toString : ConfigurableLanguage -> String
toString =
    getActive >> Translations.languageToString


getActive : ConfigurableLanguage -> Translations.Language
getActive { preferred, browser } =
    preferred |> Maybe.withDefault browser


setPreferred : Translations.Language -> Setter ConfigurableLanguage
setPreferred lang c =
    { c | preferred = Just lang }


removePreferred : Setter ConfigurableLanguage
removePreferred c =
    { c | preferred = Nothing }


isBrowserLanguagePreferred : ConfigurableLanguage -> Bool
isBrowserLanguagePreferred { preferred, browser } =
    preferred == Just browser


fromProto : Locale -> Maybe Translations.Language
fromProto locale =
    case locale of
        Proto.Data.Locale.DE ->
            Just Translations.De

        Proto.Data.Locale.EN ->
            Just Translations.En

        Proto.Data.Locale.FR ->
            Just Translations.Fr

        Proto.Data.Locale.LocaleUnrecognized_ _ ->
            Nothing


toProto : Translations.Language -> Locale
toProto language =
    case language of
        Translations.De ->
            Proto.Data.Locale.DE

        Translations.En ->
            Proto.Data.Locale.EN

        Translations.Fr ->
            Proto.Data.Locale.FR
