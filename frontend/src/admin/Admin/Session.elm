module Admin.Session exposing (Msg(..), Session, update)

import Admin.Detail.Model exposing (Pokemon)
import Admin.Detail.Types.Area as Area exposing (Area)
import Admin.Detail.Types.TileConfig as TileConfig exposing (TileConfig)
import Admin.Detail.Types.TileDict as TileDict exposing (TileDict)
import Admin.Language as Language exposing (ConfigurableLanguage)
import Admin.LanguageDict as LanguageDict
import Admin.RequestManager as RequestManager exposing (RequestManager)
import Admin.Route exposing (Route)
import Admin.Translations as Translations exposing (I18n)
import AreaId exposing (AreaId)
import AreaIdDict exposing (AreaIdDict)
import Auth exposing (AuthManager)
import BoundedDict exposing (BoundedDict)
import Browser.Navigation as Navigation
import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Http
import Item exposing (Item)
import ItemDict exposing (ItemDict)
import MapId exposing (MapId)
import PokemonNumber exposing (PokemonNumber)
import PokemonNumberDict exposing (PokemonNumberDict)
import Proto.Arceus
import Proto.Kangaskhan
import Proto.Smeargle
import Shared.Background as Background
import Shared.Types.Audio exposing (AudioConfig)
import Shared.View.Theme as Theme
import Tagged exposing (tag)
import Textures.Store exposing (Sprite, Tilemaps)
import Time
import TrainerNumber exposing (TrainerNumber)
import TrainerNumberDict exposing (TrainerNumberDict)


type Msg
    = LoadedAudioConfig AudioConfig
    | ReceivedMaps Proto.Smeargle.GetMapsResponse
    | GotTiles MapId Proto.Smeargle.GetTileDetailsResponse
    | GotTileConfig Proto.Smeargle.GetTileConfigResponse
    | GotAreas MapId Proto.Arceus.GetAreasResponse
    | GotAreaDetail AreaId Proto.Arceus.GetAreaDetailResponse
    | GotStartTiles MapId Proto.Smeargle.GetStartTilesResponse
    | GotPokemonData Proto.Kangaskhan.GetAllPokemonResponse
    | CreatedMap Proto.Smeargle.CreateMapRequest Proto.Smeargle.CreateMapResponse
    | GotTranslation Proto.Arceus.TranslateRequest Proto.Arceus.TranslateResponse
    | GotTilesChosenByStrategy Proto.Smeargle.FindFittingTilesResponse
    | GotSaveTileResponse MapId Proto.Smeargle.EditMapResponse
    | GotCreateAreaResponse Proto.Arceus.CreateAreaRequest Proto.Arceus.CreateAreaResponse
    | GotCreateWarpTileResponse Proto.Kangaskhan.AddWarpResponse
    | DeletedWarp Proto.Kangaskhan.DeleteWarpResponse
    | GotMarkStartTileResponse Proto.Smeargle.AddStartTileResponse
    | GotEditAreaResponse Proto.Arceus.EditAreaRequest Proto.Arceus.EditAreaResponse
    | LoadedTileset Sprite
    | LoadedPokemonTexture PokemonNumber Sprite
    | LoadedTrainerTexture TrainerNumber Sprite
    | LoadedItemTexture Item Sprite
    | LoadedTilemapTexture MapId Tilemaps
    | Resize Int Int
    | GotI18n (Result Http.Error (I18n -> I18n))
    | GotTimezone Time.Zone
    | RequestManagerMessage RequestManager.Msg
      -- Do nothing
    | NoOp


type alias Session s =
    { s
        | -- Internationalization
          language : ConfigurableLanguage
        , i18n : Translations.I18n

        -- Data from the server
        , audioConfig : AudioConfig
        , maps : List Proto.Smeargle.MapData
        , tileConfig : TileConfig
        , savedTiles : BoundedDict MapId TileDict
        , areas : BoundedDict MapId (List Area)
        , startTiles : BoundedDict MapId (Dict LayerNumber (List (Coordinate Int)))
        , pokemon : PokemonNumberDict Pokemon
        , areaDetails : AreaIdDict Proto.Arceus.GetAreaDetailResponse

        -- Image Textures for WebGL rendering
        , tileset : Maybe Sprite
        , tilemaps : BoundedDict MapId Tilemaps
        , pokemonTextures : PokemonNumberDict Sprite
        , trainerTextures : TrainerNumberDict Sprite
        , itemTextures : ItemDict Sprite

        -- Client side data
        , height : Int
        , width : Int
        , fontSize : Int
        , timezone : Time.Zone
        , colorThemeName : Theme.Name
        , version : String
        , background : Background.Background

        -- Routing
        , route : Route
        , key : Navigation.Key

        -- Authentication
        , auth : AuthManager
        , requestManager : RequestManager
    }


update : Msg -> Session m -> ( Session m, Cmd Msg )
update msg model =
    case msg of
        RequestManagerMessage requestManagerMsg ->
            RequestManager.update
                { onCallUpdate = RequestManagerMessage
                , onGetAudioTracks = always (Shared.Types.Audio.fromProto >> LoadedAudioConfig)
                , onGetTileDetails = \req -> GotTiles (tag req.mapId)
                , onGetMaps = always ReceivedMaps
                , onGetAreas = \req -> GotAreas (tag req.mapId)
                , onGetAreaDetail = \req -> GotAreaDetail (tag req.areaId)
                , onGetStartTiles = \req -> GotStartTiles (tag req.mapId)
                , onGetTileConfig = always GotTileConfig
                , onGetAllPokemon = always GotPokemonData
                , onLoadTilesetTexture = always LoadedTileset
                , onLoadTilemapTexture = \req _ -> LoadedTilemapTexture (tag req.mapId)
                , onLoadItemTexture = LoadedItemTexture
                , onLoadPokemonTexture = LoadedPokemonTexture
                , onLoadTrainerTexture = LoadedTrainerTexture
                , onCreateMap = CreatedMap
                , onEditMap = \req -> GotSaveTileResponse (tag req.mapId)
                , onTranslate = GotTranslation
                , onAddStartTile = always GotMarkStartTileResponse
                , onAddWarp = always GotCreateWarpTileResponse
                , onDeleteWarp = always DeletedWarp
                , onCreateArea = GotCreateAreaResponse
                , onEditArea = GotEditAreaResponse
                , onFindFittingTiles = always GotTilesChosenByStrategy
                }
                requestManagerMsg
                model

        ReceivedMaps response ->
            ( { model | maps = response.maps }
            , Cmd.none
            )

        CreatedMap req { mapId } ->
            ( { model | maps = { mapId = mapId, mapName = req.mapName } :: model.maps }, Cmd.none )

        GotTilesChosenByStrategy _ ->
            -- Is interesting for pages, not for shared data
            ( model
            , Cmd.none
            )

        GotSaveTileResponse _ _ ->
            ( model
            , Cmd.none
            )

        GotEditAreaResponse _ _ ->
            ( model
            , Cmd.none
            )

        GotCreateAreaResponse request response ->
            let
                name =
                    request.name
                        |> Maybe.withDefault (LanguageDict.constant "")
                        |> LanguageDict.get (Language.getActive model.language)
            in
            ( { model
                | areas =
                    model.areas
                        |> BoundedDict.update (tag request.mapId)
                            (Maybe.map
                                (\areas ->
                                    Area.singleFromProto
                                        { id = response.areaId
                                        , areaParts = request.areaParts
                                        , name = name
                                        , backgroundMusicTrack = Nothing
                                        }
                                        :: areas
                                )
                            )
              }
            , Cmd.none
            )

        GotCreateWarpTileResponse _ ->
            ( model, Cmd.none )

        DeletedWarp _ ->
            ( model, Cmd.none )

        GotMarkStartTileResponse _ ->
            ( model, Cmd.none )

        LoadedTileset tileset ->
            ( { model | tileset = Just tileset }, Cmd.none )

        LoadedPokemonTexture pokemonNumber sprite ->
            ( { model | pokemonTextures = PokemonNumberDict.insert pokemonNumber sprite model.pokemonTextures }
            , Cmd.none
            )

        LoadedTrainerTexture trainerNumber sprite ->
            ( { model | trainerTextures = TrainerNumberDict.insert trainerNumber sprite model.trainerTextures }
            , Cmd.none
            )

        LoadedItemTexture item sprite ->
            ( { model | itemTextures = ItemDict.insert item sprite model.itemTextures }
            , Cmd.none
            )

        LoadedTilemapTexture mapId tilemaps ->
            ( { model | tilemaps = BoundedDict.insert mapId tilemaps model.tilemaps }
            , Cmd.none
            )

        LoadedAudioConfig audioConfig ->
            ( { model | audioConfig = audioConfig }, Cmd.none )

        Resize width height ->
            ( { model | width = width, height = height }, Cmd.none )

        GotI18n (Ok applyTranslations) ->
            ( { model | i18n = applyTranslations model.i18n }, Cmd.none )

        GotI18n (Err _) ->
            ( model, Cmd.none )

        GotTimezone timezone ->
            ( { model | timezone = timezone }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )

        GotTiles mapId response ->
            ( { model
                | savedTiles =
                    response.tileDetails
                        |> TileDict.fromProto
                        |> BoundedDict.insert mapId
                        |> (|>) model.savedTiles
              }
            , Cmd.none
            )

        GotTileConfig response ->
            ( { model | tileConfig = response.tileConfigs |> TileConfig.fromProto }
            , Cmd.none
            )

        GotAreas mapId response ->
            ( { model
                | areas =
                    response.areas
                        |> Area.fromProto
                        |> BoundedDict.insert mapId
                        |> (|>) model.areas
              }
            , Cmd.none
            )

        GotAreaDetail areaId response ->
            ( { model
                | areaDetails =
                    response
                        |> AreaIdDict.insert areaId
                        |> (|>) model.areaDetails
              }
            , Cmd.none
            )

        GotStartTiles mapId response ->
            ( { model
                | startTiles =
                    response.startTiles
                        |> startTilesToDict
                        |> BoundedDict.insert mapId
                        |> (|>) model.startTiles
              }
            , Cmd.none
            )

        GotPokemonData response ->
            ( { model
                | pokemon =
                    response.pokemon
                        |> List.map
                            (\p -> ( tag p.number, { name = p.name, pokemonNumber = tag p.number, primaryType = p.primaryType, secondaryType = p.secondaryType } ))
                        |> PokemonNumberDict.fromList
              }
            , Cmd.none
            )

        GotTranslation _ _ ->
            ( model, Cmd.none )


startTilesToDict : List Proto.Smeargle.StartTile -> Dict LayerNumber (List (Coordinate Int))
startTilesToDict =
    let
        addToDict ( layerNumber, pos ) =
            Dict.update layerNumber <|
                \existing ->
                    case existing of
                        Just positions ->
                            Just <| pos :: positions

                        Nothing ->
                            Just [ pos ]
    in
    List.map (\{ layerNumber, x, y } -> ( layerNumber, { x = x, y = y } ))
        >> List.foldl addToDict Dict.empty
