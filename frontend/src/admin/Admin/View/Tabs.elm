module Admin.View.Tabs exposing (Tab, view)

import Admin.Html exposing (Html)
import Admin.Route as Route exposing (KnownRoute, Route)
import Admin.Translations exposing (I18n)
import Atoms.Html as Html
import Html.WithContext exposing (li, nav, ol)
import Html.WithContext.Attributes exposing (class, href)


type alias Tab =
    { route : KnownRoute
    , label : I18n -> String
    }


view : Route -> List Tab -> Html msg
view currentRoute tabs =
    nav [ class "relative z-10 w-full" ]
        [ ol [ class "flex w-full" ] <|
            List.map
                (\tab ->
                    li
                        [ class "w-full border-r-2 border-solid border-gray-700 last:border-r-0"
                        ]
                        [ Html.a
                            [ href <| Route.fromRoute tab.route
                            , class "block w-full px-2 py-4 text-center"
                            , if currentRoute == Ok tab.route then
                                class "bg-gray-400"

                              else
                                class "bg-gray-300"
                            ]
                            [ Html.text tab.label ]
                        ]
                )
                tabs
        ]
