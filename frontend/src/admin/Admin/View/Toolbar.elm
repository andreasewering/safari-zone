module Admin.View.Toolbar exposing (addAreaCreation, addEraserButton, addGridTools, addLayerTools, addManualTileSelectionButton, addSaveButton, addStartTileCreation, addTileSelector, addWarpConfirmation, addWarpCreation, addZoom, build, builder, maybe, unless)

import Admin.Detail.Grid as Grid
import Admin.Detail.Types.Dialog as Dialog
import Admin.Detail.Types.TileConfig exposing (TileConfig)
import Admin.Detail.Types.TileStrategy as TileStrategy exposing (TileStrategy)
import Admin.Html exposing (Html)
import Admin.Translations as Translations exposing (I18n)
import Atoms.Button
import Atoms.Html
import Atoms.Input
import Dict
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class, style)
import Html.WithContext.Events as Events
import Html.WithContext.Keyed
import Maybe.Extra as Maybe
import Shared.View.Icon exposing (iconHtmlWith)
import Textures.Store exposing (Sprite)
import WebGL.Texture


type alias ToolbarComponentBuilder msg =
    { actionButtons : List (Html msg)
    , tileSelector : Maybe (Html msg)
    }


builder : ToolbarComponentBuilder msg
builder =
    { actionButtons = [], tileSelector = Nothing }


maybe : Maybe a -> (a -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg) -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
maybe mayA builderOp =
    case mayA of
        Just a ->
            builderOp a

        Nothing ->
            identity


unless : Bool -> (ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg) -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
unless bool builderOp =
    if bool then
        identity

    else
        builderOp


addGridTools : { currentDragMode : Grid.GridMode, switchDragMode : Grid.GridMode -> msg } -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addGridTools config =
    addActionButtons <|
        List.map (gridTool config config.currentDragMode)
            [ { forMode = Grid.Drag, description = Translations.mapCreatorDetailDragToolDescription, icon = "drag-hand" }
            , { forMode = Grid.SelectSingle, description = Translations.mapCreatorDetailSelectToolDescription, icon = "select-cursor" }
            , { forMode = Grid.SelectMulti, description = Translations.mapCreatorDetailSelectMultiToolDescription, icon = "select-cursor" }
            ]


addAreaCreation : { dialog : Maybe Dialog.Dialog, openAreaDialog : msg } -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addAreaCreation config =
    addActionButtons
        [ createAreaButton config
            (Maybe.andThen Dialog.isCreateAreaDialog config.dialog)
        ]


addWarpCreation : { openWarpDialog : msg } -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addWarpCreation config =
    addActionButtons [ addWarpTileButton config ]


addWarpConfirmation : { confirmWarp : msg } -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addWarpConfirmation config =
    addActionButtons [ confirmWarpButton config ]


addStartTileCreation : { markStartTile : msg } -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addStartTileCreation config =
    addActionButtons [ addStartTileButton config ]


addLayerTools : { goUpLayer : msg, goDownLayer : msg } -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addLayerTools config =
    addActionButtons
        [ upLayerButton config
        , downLayerButton config
        ]


addZoom : { zoomBy : Float -> msg } -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addZoom config =
    addActionButtons
        [ zoomInButton config
        , zoomOutButton config
        ]


addSaveButton :
    { save : msg }
    -> ToolbarComponentBuilder msg
    -> ToolbarComponentBuilder msg
addSaveButton config =
    addActionButtons [ saveButton config ]


addEraserButton :
    { delete : msg }
    -> ToolbarComponentBuilder msg
    -> ToolbarComponentBuilder msg
addEraserButton config =
    addActionButtons [ eraserButton config ]


addManualTileSelectionButton :
    { current : TileStrategy, switchTileStrategy : TileStrategy -> msg }
    -> ToolbarComponentBuilder msg
    -> ToolbarComponentBuilder msg
addManualTileSelectionButton config =
    addActionButtons [ manualTileSelectionButton config ]


addTileSelector :
    { tileSearchInput : String
    , tileConfig : TileConfig
    , tileset : Sprite
    , clickedTileType : Int -> msg
    , changeTileSearch : String -> msg
    }
    -> ToolbarComponentBuilder msg
    -> ToolbarComponentBuilder msg
addTileSelector config toolbar =
    { toolbar
        | tileSelector =
            Just <|
                selectorApp config config.tileSearchInput config.tileConfig config.tileset
    }


addActionButtons : List (Html msg) -> ToolbarComponentBuilder msg -> ToolbarComponentBuilder msg
addActionButtons buttons toolbar =
    { toolbar
        | actionButtons =
            toolbar.actionButtons
                ++ buttons
    }


build : ToolbarComponentBuilder msg -> Html msg
build toolbar =
    div
        [ class "absolute left-0 top-0 z-20 flex w-40 flex-col gap-2 rounded-lg bg-gray-200 p-2 dark:bg-gray-800"
        ]
        (div [ class "flex w-full flex-wrap" ] toolbar.actionButtons :: Maybe.toList toolbar.tileSelector)


upLayerButton : { a | goUpLayer : msg } -> Html msg
upLayerButton events =
    iconHtmlWith { name = "up-arrow", width = 2, height = 2, description = Translations.mapCreatorDetailGoUpLayerDescription }
        [ Events.onClick events.goUpLayer, class "flex items-center justify-center p-2" ]


downLayerButton : { a | goDownLayer : msg } -> Html msg
downLayerButton events =
    iconHtmlWith { name = "down-arrow", width = 2, height = 2, description = Translations.mapCreatorDetailGoDownLayerDescription }
        [ Events.onClick events.goDownLayer, class "flex items-center justify-center p-2" ]


zoomInButton : { a | zoomBy : Float -> msg } -> Html msg
zoomInButton events =
    iconHtmlWith { name = "zoom-in", width = 2, height = 2, description = Translations.mapCreatorDetailZoomInDescription }
        [ Events.onClick (events.zoomBy 1.2), class "flex items-center justify-center p-2" ]


zoomOutButton : { a | zoomBy : Float -> msg } -> Html msg
zoomOutButton events =
    iconHtmlWith { name = "zoom-out", width = 2, height = 2, description = Translations.mapCreatorDetailZoomOutDescription }
        [ Events.onClick (events.zoomBy <| 1 / 1.2), class "flex items-center justify-center p-2" ]


manualTileSelectionButton : { current : TileStrategy, switchTileStrategy : TileStrategy -> msg } -> Html msg
manualTileSelectionButton { current, switchTileStrategy } =
    let
        bgColor =
            if current == TileStrategy.Manual then
                class "bg-gray-500"

            else
                class "bg-gray-300"

        description =
            case current of
                TileStrategy.Manual ->
                    Translations.mapCreatorDetailDisableAutomaticTileSelection

                TileStrategy.AskServer ->
                    Translations.mapCreatorDetailEnableAutomaticTileSelection
    in
    iconHtmlWith { name = "manual-tile-selection", width = 2, height = 2, description = description }
        [ Events.onClick (switchTileStrategy <| TileStrategy.next current), bgColor, class "flex items-center justify-center p-2" ]


eraserButton : { a | delete : msg } -> Html msg
eraserButton events =
    iconHtmlWith { name = "eraser", width = 2, height = 2, description = Translations.mapCreatorDetailDeleteDescription }
        [ Events.onClick events.delete, class "p-2" ]


gridTool : { a | switchDragMode : Grid.GridMode -> msg } -> Grid.GridMode -> { forMode : Grid.GridMode, description : I18n -> String, icon : String } -> Html msg
gridTool events currentMode cfg =
    let
        bgColor =
            if currentMode == cfg.forMode then
                class "bg-gray-500"

            else
                class "bg-gray-300"
    in
    iconHtmlWith { name = cfg.icon, width = 2, height = 2, description = cfg.description }
        [ Events.onClick (events.switchDragMode cfg.forMode), bgColor, class "flex items-center justify-center p-2" ]


createAreaButton : { a | openAreaDialog : msg } -> Maybe Dialog.CreateAreaDialogProps -> Html msg
createAreaButton events mayDialog =
    case mayDialog of
        Just _ ->
            Atoms.Html.none

        Nothing ->
            Atoms.Button.button
                { onPress = events.openAreaDialog |> Just
                , label = Atoms.Html.text Translations.mapCreatorDetailAreaDialogOpen
                , loading = False
                }
                []


addWarpTileButton : { a | openWarpDialog : msg } -> Html msg
addWarpTileButton events =
    Atoms.Button.button
        { onPress = events.openWarpDialog |> Just
        , label = Atoms.Html.text Translations.mapCreatorDetailCreateWarp
        , loading = False
        }
        []


confirmWarpButton : { a | confirmWarp : msg } -> Html msg
confirmWarpButton events =
    Atoms.Button.button
        { onPress = events.confirmWarp |> Just
        , label = Atoms.Html.text Translations.mapCreatorDetailCompleteWarp
        , loading = False
        }
        []


addStartTileButton : { a | markStartTile : msg } -> Html msg
addStartTileButton events =
    Atoms.Button.button
        { onPress = events.markStartTile |> Just
        , label = Atoms.Html.text Translations.mapCreatorDetailCreateStartTile
        , loading = False
        }
        []


saveButton : { a | save : msg } -> Html msg
saveButton events =
    iconHtmlWith { name = "save", width = 2, height = 2, description = Translations.mapCreatorDetailSaveDescription }
        [ Events.onClick <| events.save, class "flex items-center justify-center p-2" ]


selectorApp :
    { a
        | clickedTileType : Int -> msg
        , changeTileSearch : String -> msg
    }
    -> String
    -> TileConfig
    -> Sprite
    -> Html msg
selectorApp events tileSearchInput tileConfig { columns, path, texture } =
    let
        tileWidth =
            (WebGL.Texture.size texture
                |> Tuple.first
            )
                // columns

        searchMatchesKeyword keyword =
            String.contains (String.toLower tileSearchInput) (String.toLower keyword)

        indexToY index =
            index // columns

        indexToX index =
            index - indexToY index * columns

        renderKeyedTile index metadata =
            let
                tileId =
                    index + 1
            in
            ( String.fromInt index
            , if List.any searchMatchesKeyword metadata.keywords then
                Html.WithContext.li
                    [ Events.onClick (events.clickedTileType tileId)
                    , class "bg-no-repeat"
                    , style "width" <| String.fromInt tileWidth ++ "px"
                    , style "height" <| String.fromInt tileWidth ++ "px"
                    , style "background-position" <|
                        String.fromInt -(indexToX index * tileWidth)
                            ++ "px "
                            ++ String.fromInt -(indexToY index * tileWidth)
                            ++ "px"
                    , style "background-image" <| "url(" ++ path ++ ")"
                    ]
                    []

              else
                Atoms.Html.none
            )
    in
    div []
        [ Atoms.Input.text
            { id = "tile-selector"
            , onChange = events.changeTileSearch
            , label = Translations.mapCreatorDetailTileSearchLabel
            , text = tileSearchInput
            , error = Nothing
            }
        , div []
            [ Dict.values tileConfig
                |> List.indexedMap renderKeyedTile
                |> Html.WithContext.Keyed.ul [ class "flex flex-wrap" ]
            ]
        ]
