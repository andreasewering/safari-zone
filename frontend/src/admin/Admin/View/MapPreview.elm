module Admin.View.MapPreview exposing (Props, view)

import Admin.Html exposing (Html)
import Data.Units exposing (TileUnit(..))
import Domain.Coordinate exposing (Coordinate)
import Html.Attributes
import Html.WithContext exposing (div, p, text)
import Html.WithContext.Attributes exposing (class, style)
import MapId exposing (MapId)
import Textures.Store exposing (Sprite, Tilemaps)
import WebGL
import WebGL.Map


type alias Props =
    { name : String
    , id : MapId
    , tilemaps : Maybe Tilemaps
    , tileset : Maybe Sprite
    , mapSize : Coordinate Int
    }


view : Props -> Html msg
view props =
    div [ class "flex flex-col items-center justify-center" ]
        (case ( props.tileset, props.tilemaps ) of
            ( Just tileset, Just tilemaps ) ->
                [ mapImage props.mapSize tileset tilemaps, p [ class "text-center" ] [ text props.name ] ]

            _ ->
                [ div [ style "width" <| String.fromInt props.mapSize.x ++ "px", style "height" <| String.fromInt props.mapSize.y ++ "px" ] []
                , p [ class "text-center" ] [ text props.name ]
                ]
        )


mapImage : Coordinate Int -> Sprite -> Tilemaps -> Html msg
mapImage mapSize tileset tilemaps =
    let
        layersInTotal =
            List.length tilemaps.layers
    in
    List.indexedMap
        (\_ layerIndex ->
            WebGL.Map.entity
                { x =
                    { player = TileUnit 5
                    , screenSizePx = 1200
                    , tileMapSize = TileUnit 16
                    , offset = TileUnit 0
                    }
                , y =
                    { player = TileUnit 5
                    , screenSizePx = 1200
                    , tileMapSize = TileUnit 16
                    , offset = TileUnit 0
                    }
                }
                1
                { x = 0, y = 0 }
                tileset.texture
                tilemaps.texture
                layersInTotal
                layerIndex
        )
        tilemaps.layers
        |> WebGL.toHtml [ Html.Attributes.width mapSize.x, Html.Attributes.height mapSize.y ]
        |> Html.WithContext.html
