module Admin.View.DragAndDrop exposing (Msg, Position, State, draggable, droppable, init, isDragging, isDraggingOver, update)

import Atoms.Html
import Html.WithContext
import Html.WithContext.Attributes
import Html.WithContext.Events
import Json.Decode as JD
import List.Extra


type Msg dragId dropId
    = DragStart dragId
    | DragEnd dragId
    | DragEnter dropId
    | DragLeave dropId
    | DragOver dropId Int Position
    | Drop dropId Position


type State dragId dropId
    = NotDragging
    | Dragging dragId
    | DraggingOver dragId dropId Int Position


init : State dragId dropId
init =
    NotDragging


isDragging : State dragId dropId -> Maybe dragId
isDragging state =
    case state of
        NotDragging ->
            Nothing

        Dragging dragId ->
            Just dragId

        DraggingOver dragId _ _ _ ->
            Just dragId


isDraggingOver : State dragId dropId -> Maybe ( dragId, dropId )
isDraggingOver state =
    case state of
        NotDragging ->
            Nothing

        Dragging _ ->
            Nothing

        DraggingOver dragId dropId _ _ ->
            Just ( dragId, dropId )


type alias Position =
    { width : Int
    , height : Int
    , x : Int
    , y : Int
    }


draggable : State dragId dropId -> (Msg dragId dropId -> msg) -> dragId -> List (Html.WithContext.Attribute { ctx | disabled : Bool } msg)
draggable state lift dragId =
    [ Html.WithContext.Attributes.draggable "true"
    , Html.WithContext.Events.custom "dragstart"
        (JD.succeed
            { message = lift <| DragStart dragId
            , stopPropagation = True
            , preventDefault = False
            }
        )
    , Html.WithContext.Events.custom "dragend"
        (JD.succeed
            { message = lift <| DragEnd dragId
            , stopPropagation = True
            , preventDefault = False
            }
        )
    , Atoms.Html.noTabindex
    ]
        ++ (case state of
                NotDragging ->
                    [ Html.WithContext.Events.onClick (DragStart dragId |> lift), Html.WithContext.Events.on "keydown" (keyDecoder [ ( "Enter", DragStart dragId |> lift ) ]) ]

                _ ->
                    []
           )


droppable : State dragId dropId -> (Msg dragId dropId -> msg) -> dropId -> List (Html.WithContext.Attribute { ctx | disabled : Bool } msg)
droppable state lift dropId =
    [ Html.WithContext.Events.custom "dragenter"
        (JD.succeed
            { message = lift <| DragEnter dropId
            , stopPropagation = True
            , preventDefault = True
            }
        )
    , Html.WithContext.Events.custom "dragleave"
        (JD.succeed
            { message = lift <| DragLeave dropId
            , stopPropagation = True
            , preventDefault = True
            }
        )
    , Html.WithContext.Events.custom "dragover"
        (JD.map2
            (\timestamp position ->
                { message = lift <| DragOver dropId timestamp position

                -- We don't stop propagation for dragover events because this will trigger redraw,
                -- and we get a lot of dragover events.
                , stopPropagation = False
                , preventDefault = True
                }
            )
            timestampDecoder
            positionDecoder
        )
    , Html.WithContext.Events.custom "drop"
        (JD.map
            (\position ->
                { message = lift <| Drop dropId position
                , stopPropagation = True
                , preventDefault = True
                }
            )
            positionDecoder
        )
    , Atoms.Html.noTabindex
    ]
        ++ (case state of
                NotDragging ->
                    []

                _ ->
                    [ Html.WithContext.Events.on "click" (JD.map (\position -> Drop dropId position |> lift) positionDecoder)
                    , Html.WithContext.Events.on "keydown" (keyDecoder [ ( "Enter", Drop dropId { width = 0, height = 0, x = 0, y = 0 } |> lift ) ])
                    ]
           )


update :
    Msg dragId dropId
    -> State dragId dropId
    -> ( State dragId dropId, Maybe ( dragId, dropId, Position ) )
update msg model =
    case ( msg, model ) of
        ( DragStart dragId, _ ) ->
            ( Dragging dragId, Nothing )

        ( DragEnd _, _ ) ->
            ( NotDragging, Nothing )

        ( DragEnter _, DraggingOver dragId _ _ _ ) ->
            ( Dragging dragId, Nothing )

        -- Only handle DragLeave if it is for the current dropId.
        -- DragLeave and DragEnter sometimes come in the wrong order
        -- when two droppables are next to each other.
        ( DragLeave dropId_, DraggingOver dragId dropId _ _ ) ->
            if dropId_ == dropId then
                ( Dragging dragId, Nothing )

            else
                ( model, Nothing )

        ( DragOver dropId timeStamp pos, Dragging dragId ) ->
            ( DraggingOver dragId dropId timeStamp pos, Nothing )

        ( DragOver dropId timeStamp pos, DraggingOver dragId _ currentTimeStamp _ ) ->
            if timeStamp == currentTimeStamp then
                -- Handle dragover bubbling, if we already have handled this event
                -- (by looking at the timeStamp), do nothing. Also, this does some rate limiting
                -- if multiple events occur in the same time stamp.
                ( model, Nothing )

            else
                -- Update coordinates
                ( DraggingOver dragId dropId timeStamp pos, Nothing )

        ( Drop dropId pos, Dragging dragId ) ->
            ( NotDragging, Just ( dragId, dropId, pos ) )

        ( Drop dropId pos, DraggingOver dragId _ _ _ ) ->
            ( NotDragging, Just ( dragId, dropId, pos ) )

        _ ->
            ( model, Nothing )


timestampDecoder : JD.Decoder Int
timestampDecoder =
    JD.at [ "timeStamp" ] JD.float |> JD.map round


positionDecoder : JD.Decoder Position
positionDecoder =
    JD.map4 Position
        (JD.at [ "currentTarget", "clientWidth" ] JD.int)
        (JD.at [ "currentTarget", "clientHeight" ] JD.int)
        (JD.at [ "offsetX" ] JD.float |> JD.map round)
        (JD.at [ "offsetY" ] JD.float |> JD.map round)


keyDecoder : List ( String, a ) -> JD.Decoder a
keyDecoder keysToReactTo =
    JD.field "key" JD.string
        |> JD.andThen
            (\key ->
                case List.Extra.find (\( k, _ ) -> k == key) keysToReactTo of
                    Just ( _, a ) ->
                        JD.succeed a

                    Nothing ->
                        JD.fail ""
            )
