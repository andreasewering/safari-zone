module Admin.View.TileApp exposing (view)

import Admin.Detail.Types.TileDict as TileDict exposing (TileDict)
import Admin.Html exposing (Html)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Html
import Html.Attributes
import Html.WithContext
import Json.Encode as E
import List.Extra
import Textures.Store exposing (Sprite)
import WebGL.Texture


type alias TileApp s =
    { offset : Coordinate Int
    , tileDict : TileDict
    , viewPort : { s | width : Int, height : Int }
    , tileset : Sprite
    , tileSize : Int
    , layerNumberInFocus : LayerNumber
    }


view : TileApp s -> List (Html msg)
view { tileDict, tileSize, offset, viewPort, tileset, layerNumberInFocus } =
    let
        screenX =
            viewPort.width

        screenY =
            viewPort.height

        tileWidth =
            (WebGL.Texture.size tileset.texture
                |> Tuple.first
            )
                // tileset.columns

        encodeTile { position, tileId } =
            let
                { x, y } =
                    tilePosToPx position
            in
            E.object [ ( "x", E.int x ), ( "y", E.int y ), ( "id", E.int tileId ) ]

        filterVisible : Coordinate Int -> Bool
        filterVisible coord =
            let
                pixelX =
                    coord.x * tileSize

                pixelY =
                    coord.y * tileSize
            in
            (pixelX + tileSize > -1 * offset.x)
                && (pixelX - tileSize < screenX - offset.x)
                && (pixelY + tileSize > -1 * offset.y)
                && (pixelY - tileSize < screenY - offset.y)

        tilePosToPx : Coordinate Int -> Coordinate Int
        tilePosToPx =
            Coordinate.times tileSize >> Coordinate.plus offset

        tilesGroupedByLayerNumber =
            TileDict.getEntries tileDict
                |> List.filter (.position >> filterVisible)
                |> List.sortBy .layerNumber
                |> List.Extra.groupWhile (\tile1 tile2 -> tile1.layerNumber == tile2.layerNumber)
    in
    List.map
        (\( firstTile, tiles ) ->
            let
                opacity =
                    if firstTile.layerNumber == layerNumberInFocus then
                        "1"

                    else
                        "0.5"
            in
            Html.node "tile-app"
                [ Html.Attributes.class "fixed left-0 top-0 -z-10"
                , Html.Attributes.height screenY
                , Html.Attributes.width screenX
                , Html.Attributes.style "opacity" opacity
                , Html.Attributes.property "tiles" <| E.list encodeTile (firstTile :: tiles)
                , Html.Attributes.property "tile_size" <| E.int tileSize
                , Html.Attributes.attribute "tileset_path" tileset.path
                , Html.Attributes.property "tiles_in_row" <| E.int tileset.columns
                , Html.Attributes.property "src_tile_size" <| E.int tileWidth
                ]
                []
                |> Html.WithContext.html
        )
        tilesGroupedByLayerNumber
