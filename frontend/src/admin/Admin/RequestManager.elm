module Admin.RequestManager exposing
    ( ApiCall(..)
    , Events
    , Msg
    , RequestManager
    , Status(..)
    , init
    , invalidate
    , onLanguageChange
    , register
    , status
    , unregister
    , update
    )

import Admin.ErrorHandling as ErrorHandling
import Admin.Language as Language exposing (ConfigurableLanguage)
import Admin.Route exposing (Route)
import Admin.Translations as Translations exposing (I18n)
import Auth
import Base64
import Dict exposing (Dict)
import Grpc
import Http
import Item exposing (Item)
import Lens exposing (Lens)
import Maybe.Extra
import PokemonNumber exposing (PokemonNumber)
import Process
import Proto.Arceus as Arceus exposing (CreateAreaRequest, CreateAreaResponse, EditAreaRequest, EditAreaResponse, GetAreaDetailRequest, GetAreaDetailResponse, GetAreasRequest, GetAreasResponse, GetAudioTracksRequest, GetAudioTracksResponse, TranslateRequest, TranslateResponse)
import Proto.Arceus.ArceusService as ArceusService
import Proto.Gardevoir.GardevoirService as GardevoirService
import Proto.Google.Protobuf as Protobuf
import Proto.Kangaskhan as Kangaskhan exposing (AddWarpRequest, AddWarpResponse, DeleteWarpRequest, DeleteWarpResponse, GetAllPokemonRequest, GetAllPokemonResponse)
import Proto.Kangaskhan.KangaskhanService as KangaskhanService
import Proto.Smeargle as Smeargle exposing (AddStartTileRequest, AddStartTileResponse, CreateMapRequest, CreateMapResponse, EditMapRequest, EditMapResponse, FindFittingTilesRequest, FindFittingTilesResponse, GetMapsRequest, GetMapsResponse, GetStartTilesRequest, GetStartTilesResponse, GetTextureMetadataRequest, GetTextureMetadataResponse, GetTileConfigRequest, GetTileConfigResponse, GetTileDetailsRequest, GetTileDetailsResponse, GetTilesetConfigRequest, GetTilesetConfigResponse)
import Proto.Smeargle.SmeargleService as SmeargleService
import Protobuf.Encode
import Result.Extra
import Shared.Types.Error as Error
import Shared.Types.TrainerSprite as TrainerSprite
import Shared.UpdateUtil as UpdateUtil
import Tagged exposing (untag)
import Task
import Textures.Api
import Textures.Store exposing (Sprite, Tilemaps)
import Time
import TrainerNumber exposing (TrainerNumber)
import WebGL.Texture as Texture exposing (nonPowerOfTwoOptions)


type ApiCall
    = -- Kangaskhan
      GetAllPokemon GetAllPokemonRequest
    | AddWarp AddWarpRequest
    | DeleteWarp DeleteWarpRequest
      -- Smeargle
    | GetStartTiles GetStartTilesRequest
    | GetTileDetails GetTileDetailsRequest
    | GetTilesetConfig GetTilesetConfigRequest
    | GetTextureMetadata GetTextureMetadataRequest
    | GetTileConfig GetTileConfigRequest
    | GetMaps GetMapsRequest
    | CreateMap CreateMapRequest
    | FindFittingTiles FindFittingTilesRequest
    | EditMap EditMapRequest
    | AddStartTile AddStartTileRequest
      -- Arceus
    | GetAudioTracks GetAudioTracksRequest
    | GetAreas GetAreasRequest
    | GetAreaDetail GetAreaDetailRequest
    | CreateArea CreateAreaRequest
    | EditArea EditAreaRequest
    | Translate TranslateRequest
      -- Textures
    | LoadTilesetTexture GetTilesetConfigResponse -- Do not construct in "user space", instead use `GetTilesetConfig`
    | LoadPokemonTexture PokemonNumber
    | LoadTrainerTexture TrainerNumber
    | LoadItemTexture Item
    | LoadTilemapTexture GetTextureMetadataRequest GetTextureMetadataResponse
      -- Gardevoir
    | RefreshAuth


init : RequestManager
init =
    RequestManager { registered = [], grpc = Dict.empty }


type ApiResponse
    = -- Kangaskhan
      GetAllPokemonRes GetAllPokemonRequest (Result Grpc.Error GetAllPokemonResponse)
    | AddWarpRes AddWarpRequest (Result Grpc.Error AddWarpResponse)
    | DeleteWarpRes DeleteWarpRequest (Result Grpc.Error DeleteWarpResponse)
      -- Smeargle
    | GetMapsRes GetMapsRequest (Result Grpc.Error GetMapsResponse)
    | GetStartTilesRes GetStartTilesRequest (Result Grpc.Error GetStartTilesResponse)
    | GetTileDetailsRes GetTileDetailsRequest (Result Grpc.Error GetTileDetailsResponse)
    | GetTilesetConfigRes GetTilesetConfigRequest (Result Grpc.Error GetTilesetConfigResponse)
    | GetTextureMetadataRes GetTextureMetadataRequest (Result Grpc.Error GetTextureMetadataResponse)
    | GetTileConfigRes GetTileConfigRequest (Result Grpc.Error GetTileConfigResponse)
    | CreateMapRes CreateMapRequest (Result Grpc.Error CreateMapResponse)
    | FindFittingTilesRes FindFittingTilesRequest (Result Grpc.Error FindFittingTilesResponse)
    | EditMapRes EditMapRequest (Result Grpc.Error EditMapResponse)
    | AddStartTileRes AddStartTileRequest (Result Grpc.Error AddStartTileResponse)
      -- Arceus
    | GetAudioTracksRes GetAudioTracksRequest (Result Grpc.Error GetAudioTracksResponse)
    | GetAreasRes GetAreasRequest (Result Grpc.Error GetAreasResponse)
    | GetAreaDetailRes GetAreaDetailRequest (Result Grpc.Error GetAreaDetailResponse)
    | CreateAreaRes CreateAreaRequest (Result Grpc.Error CreateAreaResponse)
    | EditAreaRes EditAreaRequest (Result Grpc.Error EditAreaResponse)
    | TranslateRes TranslateRequest (Result Grpc.Error TranslateResponse)
      -- Textures
    | LoadTilesetTextureRes GetTilesetConfigResponse (Result Texture.Error Sprite)
    | LoadPokemonTextureRes PokemonNumber (Result Texture.Error Sprite)
    | LoadTrainerTextureRes TrainerNumber (Result Texture.Error Sprite)
    | LoadItemTextureRes Item (Result Texture.Error Sprite)
    | LoadTilemapTextureRes GetTextureMetadataRequest GetTextureMetadataResponse (Result Texture.Error Tilemaps)
    | RefreshAuthRes (Result Error.Error Auth.Authentification)


status : ApiCall -> RequestManager -> Status Error.Error
status call manager =
    let
        identifier =
            serializeApiCall call

        get id =
            id
                |> grpcRequestState
                |> .get
                |> (|>) { requestManager = manager }
    in
    get identifier
        -- The status of the most recent subrequest (if any) is the status we are interested in
        |> Maybe.map (\state -> List.head state.subrequests |> Maybe.andThen get |> Maybe.withDefault state)
        |> Maybe.map (toExternalStatus identifier.path)
        |> Maybe.withDefault NotAsked


type Status e
    = Loading
    | Failure { path : String, error : e }
    | Success
    | NotAsked


type alias Events msg =
    { -- Internal messages
      onCallUpdate : Msg -> msg
    , -- one event per http request, just signal success
      -- Kangaskhan
      onGetAllPokemon : GetAllPokemonRequest -> GetAllPokemonResponse -> msg
    , onAddWarp : AddWarpRequest -> AddWarpResponse -> msg
    , onDeleteWarp : DeleteWarpRequest -> DeleteWarpResponse -> msg
    , onGetMaps : GetMapsRequest -> GetMapsResponse -> msg

    -- Smeargle
    , onGetStartTiles : GetStartTilesRequest -> GetStartTilesResponse -> msg
    , onGetTileDetails : GetTileDetailsRequest -> GetTileDetailsResponse -> msg
    , onGetTileConfig : GetTileConfigRequest -> GetTileConfigResponse -> msg
    , onCreateMap : CreateMapRequest -> CreateMapResponse -> msg
    , onEditMap : EditMapRequest -> EditMapResponse -> msg
    , onFindFittingTiles : FindFittingTilesRequest -> FindFittingTilesResponse -> msg
    , onAddStartTile : AddStartTileRequest -> AddStartTileResponse -> msg

    -- Arceus
    , onGetAudioTracks : GetAudioTracksRequest -> GetAudioTracksResponse -> msg
    , onGetAreas : GetAreasRequest -> GetAreasResponse -> msg
    , onGetAreaDetail : GetAreaDetailRequest -> GetAreaDetailResponse -> msg
    , onCreateArea : CreateAreaRequest -> CreateAreaResponse -> msg
    , onEditArea : EditAreaRequest -> EditAreaResponse -> msg
    , onTranslate : TranslateRequest -> TranslateResponse -> msg

    -- Textures
    , onLoadTilesetTexture : GetTilesetConfigResponse -> Sprite -> msg
    , onLoadPokemonTexture : PokemonNumber -> Sprite -> msg
    , onLoadTrainerTexture : TrainerNumber -> Sprite -> msg
    , onLoadItemTexture : Item -> Sprite -> msg
    , onLoadTilemapTexture : GetTextureMetadataRequest -> GetTextureMetadataResponse -> Tilemaps -> msg
    }


handleError : ApiCall -> Error.Error -> Session s -> ( Session s, Cmd Msg )
handleError apiCall err session =
    case
        ErrorHandling.handleWebError session
            { languageToString = Language.toString }
            { error = err, path = serializeApiCall apiCall |> .path }
    of
        ErrorHandling.AutomaticallyFixable _ ErrorHandling.Retry ->
            ( session
            , Process.sleep 1000
                |> Task.andThen (\_ -> Task.succeed apiCall)
                |> Task.perform Retry
            )

        ErrorHandling.AutomaticallyFixable _ ErrorHandling.RefreshToken ->
            let
                ( manager, cmds ) =
                    register [ RefreshAuth ] session.requestManager
            in
            ( { session | requestManager = manager }, cmds )

        ErrorHandling.Blocking _ ->
            ( session, Cmd.none )


type alias CacheStrategy =
    { forMillis : Int
    , refreshIfLanguageChanges : Bool
    }


defaultCacheStrategy : CacheStrategy
defaultCacheStrategy =
    { forMillis = 300000 -- 5 Minutes
    , refreshIfLanguageChanges = True
    }


mutationCacheStrategy : CacheStrategy
mutationCacheStrategy =
    { forMillis = 0, refreshIfLanguageChanges = False }


type alias RetryStrategy =
    { maxRetries : Int, initialBackoffInMillis : Int }


defaultRetryStrategy : RetryStrategy
defaultRetryStrategy =
    { maxRetries = 6, initialBackoffInMillis = 500 }



-- | General idea:
-- This module manages http/grpc requests in the sense that it
-- - Supports tracking progress via Http.track
-- - Tracks starting and finishing times of requests
-- - Allows caching via different strategies
-- - Can automatically resend requests if a token is updated or the preferred language is changed
-- - Automatically handles retries according to a specified error handling strategy
--
-- Most of the code in here should be generic and should not need adjustment when new calls are added
-- The external interface should be slim and consist of the following parts:
-- - Registering/Unregistering requests
-- - Customizing Error Handling and Caching
-- - Getting the status of requests (Loading/Success/Failure)


type RequestManager
    = RequestManager
        { grpc : Dict ( String, String ) GrpcRequestState
        , registered : List ApiCall
        }


type alias GrpcRequestState =
    { cacheStrategy : CacheStrategy
    , retryStrategy : RetryStrategy
    , error : Maybe Error.Error
    , loadingSince : Maybe Time.Posix
    , retryCount : Int
    , lastResponseAt : Maybe Time.Posix
    , language : Maybe Translations.Language
    , subrequests : List RequestIdentifier
    }


initialRequestState : CacheStrategy -> GrpcRequestState
initialRequestState cacheStrat =
    { cacheStrategy = cacheStrat
    , retryStrategy = defaultRetryStrategy
    , error = Nothing
    , loadingSince = Nothing
    , retryCount = 0
    , lastResponseAt = Nothing
    , language = Nothing
    , subrequests = []
    }


toExternalStatus : String -> GrpcRequestState -> Status Error.Error
toExternalStatus path { error, lastResponseAt, loadingSince } =
    case ( error, lastResponseAt, loadingSince ) of
        ( Just e, _, _ ) ->
            Failure { path = path, error = e }

        ( Nothing, Nothing, _ ) ->
            Loading

        ( Nothing, Just _, Nothing ) ->
            Success

        ( Nothing, Just lastResponseAt_, Just loadingSince_ ) ->
            if Time.posixToMillis lastResponseAt_ >= Time.posixToMillis loadingSince_ then
                Success

            else
                Loading


grpcRequestState : RequestIdentifier -> Lens (Maybe GrpcRequestState) { s | requestManager : RequestManager }
grpcRequestState identifier =
    let
        grpcLens : Lens (Dict ( String, String ) GrpcRequestState) RequestManager
        grpcLens =
            { get = \(RequestManager r) -> r.grpc, set = \v (RequestManager r) -> RequestManager { r | grpc = v } }

        requestManagerLens =
            { get = \{ requestManager } -> requestManager, set = \r v -> { v | requestManager = r } }
    in
    requestManagerLens
        |> Lens.compose grpcLens
        |> Lens.compose (Lens.dict <| identifierToComparable identifier)


type Msg
    = TriggerSubrequest RequestIdentifier ApiCall
    | RequestStart ApiCall Time.Posix
    | RequestEnd RequestIdentifier Time.Posix
    | AuthRefreshed Auth.Authentification
    | GotResponse ApiCall ApiResponse
    | Retry ApiCall


type alias RequestIdentifier =
    { serializedRequest : String, path : String }


type alias Session s =
    { s
        | auth : Auth.AuthManager
        , i18n : I18n
        , language : ConfigurableLanguage
        , requestManager : RequestManager
        , timezone : Time.Zone
        , version : String
        , route : Route
    }


register : List ApiCall -> RequestManager -> ( RequestManager, Cmd Msg )
register calls manager =
    ( List.foldl registerInternal manager calls
    , List.map begin calls |> Cmd.batch
    )


invalidate : List ApiCall -> RequestManager -> ( RequestManager, Cmd Msg )
invalidate calls =
    UpdateUtil.chain [ unregister calls, register calls ]


unregister : List ApiCall -> RequestManager -> ( RequestManager, Cmd msg )
unregister calls manager =
    ( List.foldl unregisterInternal manager calls
    , List.map (serializeApiCall >> identifierToTracker >> Http.cancel) calls |> Cmd.batch
    )


begin : ApiCall -> Cmd Msg
begin call =
    Time.now |> Task.map (RequestStart call) |> Task.perform identity


update : Events msg -> Msg -> Session s -> ( Session s, Cmd msg )
update events msg session =
    case msg of
        TriggerSubrequest mainRequestIdentifier apiCall ->
            ( addSubrequest apiCall
                |> Maybe.map
                |> Lens.update (grpcRequestState mainRequestIdentifier)
                |> (|>) session
            , begin apiCall |> Cmd.map events.onCallUpdate
            )

        RequestStart apiCall time ->
            let
                lens =
                    grpcRequestState <| serializeApiCall apiCall

                existing =
                    lens.get session

                activeLanguage =
                    Language.getActive session.language

                shouldCacheBasedOnLanguage =
                    Maybe.map2
                        (\state language ->
                            activeLanguage == language || not state.cacheStrategy.refreshIfLanguageChanges
                        )
                        existing
                        (existing |> Maybe.andThen .language)
                        |> Maybe.withDefault False

                shouldCacheBasedOnTime =
                    Maybe.map2
                        (\state lastResponseAt ->
                            Time.posixToMillis time <= Time.posixToMillis lastResponseAt + state.cacheStrategy.forMillis
                        )
                        existing
                        (existing |> Maybe.andThen .lastResponseAt)
                        |> Maybe.withDefault False
            in
            if shouldCacheBasedOnTime && shouldCacheBasedOnLanguage then
                ( session, Cmd.none )

            else
                ( (setRequestStart time
                    >> setLanguage activeLanguage
                  )
                    |> Maybe.map
                    |> Lens.update lens
                    |> (|>) session
                , executeRequest session apiCall |> Cmd.map (GotResponse apiCall >> events.onCallUpdate)
                )

        RequestEnd identifier time ->
            ( setRequestEnd time
                |> Maybe.map
                |> Lens.update (grpcRequestState identifier)
                |> (|>) session
            , Cmd.none
            )

        GotResponse apiCall response ->
            let
                identifier =
                    serializeApiCall apiCall

                handleErrorResult =
                    Maybe.map (handleError apiCall)
                        >> Maybe.withDefault UpdateUtil.none

                ( newSession, cmds ) =
                    setResponse response
                        |> Maybe.map
                        |> Lens.update (grpcRequestState identifier)
                        |> (|>) session
                        |> handleErrorResult (responseToError response)
            in
            ( newSession
            , Cmd.batch
                [ Time.now |> Task.perform (RequestEnd identifier >> events.onCallUpdate)
                , emitSuccessEvent events response
                , cmds |> Cmd.map events.onCallUpdate
                ]
            )

        Retry call ->
            ( session, begin call |> Cmd.map events.onCallUpdate )

        AuthRefreshed _ ->
            let
                retries =
                    retrieveRegistered session.requestManager
                        |> List.filter (\( _, state ) -> Maybe.Extra.isJust state.error)
                        |> List.map (Tuple.first >> begin)
                        |> Cmd.batch
                        |> Cmd.map events.onCallUpdate
            in
            ( session, retries )


onLanguageChange : RequestManager -> Cmd Msg
onLanguageChange manager =
    let
        executeWithChangedLanguage =
            retrieveRegistered manager
                |> List.filter (\( _, state ) -> state.cacheStrategy.refreshIfLanguageChanges)
                |> List.map (Tuple.first >> begin)
                |> Cmd.batch
    in
    executeWithChangedLanguage


serializeApiCall : ApiCall -> RequestIdentifier
serializeApiCall call =
    let
        make :
            (req -> Protobuf.Encode.Encoder)
            -> req
            -> Grpc.Rpc req res
            -> { serializedRequest : String, path : String }
        make enc req rpc =
            { serializedRequest = enc req |> encoderToString, path = Grpc.rpcPath rpc }
    in
    case call of
        GetAllPokemon req ->
            make Kangaskhan.encodeGetAllPokemonRequest req KangaskhanService.getAllPokemon

        AddWarp req ->
            make Kangaskhan.encodeAddWarpRequest req KangaskhanService.addWarp

        DeleteWarp req ->
            make Kangaskhan.encodeDeleteWarpRequest req KangaskhanService.deleteWarp

        GetMaps req ->
            make Smeargle.encodeGetMapsRequest req SmeargleService.getMaps

        GetStartTiles req ->
            make Smeargle.encodeGetStartTilesRequest req SmeargleService.getStartTiles

        GetTileDetails req ->
            make Smeargle.encodeGetTileDetailsRequest req SmeargleService.getTileDetails

        GetTextureMetadata req ->
            make Smeargle.encodeGetTextureMetadataRequest req SmeargleService.getTextureMetadata

        GetTilesetConfig req ->
            make Smeargle.encodeGetTilesetConfigRequest req SmeargleService.getTilesetConfig

        GetTileConfig req ->
            make Smeargle.encodeGetTileConfigRequest req SmeargleService.getTileConfig

        CreateMap req ->
            make Smeargle.encodeCreateMapRequest req SmeargleService.createMap

        EditMap req ->
            make Smeargle.encodeEditMapRequest req SmeargleService.editMap

        AddStartTile req ->
            make Smeargle.encodeAddStartTileRequest req SmeargleService.addStartTile

        FindFittingTiles req ->
            make Smeargle.encodeFindFittingTilesRequest req SmeargleService.findFittingTiles

        GetAudioTracks req ->
            make Arceus.encodeGetAudioTracksRequest req ArceusService.getAudioTracks

        GetAreas req ->
            make Arceus.encodeGetAreasRequest req ArceusService.getAreas

        GetAreaDetail req ->
            make Arceus.encodeGetAreaDetailRequest req ArceusService.getAreaDetail

        CreateArea req ->
            make Arceus.encodeCreateAreaRequest req ArceusService.createArea

        EditArea req ->
            make Arceus.encodeEditAreaRequest req ArceusService.editArea

        Translate req ->
            make Arceus.encodeTranslateRequest req ArceusService.translate

        LoadTilesetTexture config ->
            tilesetIdentifier config

        LoadPokemonTexture pokemonNumber ->
            { serializedRequest = String.fromInt <| untag pokemonNumber, path = "pokemonTexture" }

        LoadTrainerTexture trainerNumber ->
            { serializedRequest = String.fromInt <| untag trainerNumber, path = "trainerTexture" }

        LoadItemTexture item ->
            { serializedRequest = "", path = Item.toSpritePath item }

        LoadTilemapTexture _ metadataRes ->
            { serializedRequest = "", path = metadataRes.path }

        RefreshAuth ->
            make Protobuf.encodeEmpty {} GardevoirService.refresh


executeRequest : Session s -> ApiCall -> Cmd ApiResponse
executeRequest session call =
    let
        make rpc req toRes =
            grpc
                { rpc = rpc
                , body = req
                , session = session
                , identifier = serializeApiCall call
                }
                |> Cmd.map (toRes req)
    in
    case call of
        GetAllPokemon req ->
            make KangaskhanService.getAllPokemon req GetAllPokemonRes

        AddWarp req ->
            make KangaskhanService.addWarp req AddWarpRes

        DeleteWarp req ->
            make KangaskhanService.deleteWarp req DeleteWarpRes

        GetMaps req ->
            make SmeargleService.getMaps req GetMapsRes

        GetStartTiles req ->
            make SmeargleService.getStartTiles req GetStartTilesRes

        GetTileDetails req ->
            make SmeargleService.getTileDetails req GetTileDetailsRes

        GetTilesetConfig req ->
            make SmeargleService.getTilesetConfig req GetTilesetConfigRes

        GetTextureMetadata req ->
            make SmeargleService.getTextureMetadata req GetTextureMetadataRes

        GetTileConfig req ->
            make SmeargleService.getTileConfig req GetTileConfigRes

        CreateMap req ->
            make SmeargleService.createMap req CreateMapRes

        EditMap req ->
            make SmeargleService.editMap req EditMapRes

        AddStartTile req ->
            make SmeargleService.addStartTile req AddStartTileRes

        FindFittingTiles req ->
            make SmeargleService.findFittingTiles req FindFittingTilesRes

        GetAudioTracks req ->
            make ArceusService.getAudioTracks req GetAudioTracksRes

        GetAreas req ->
            make ArceusService.getAreas req GetAreasRes

        GetAreaDetail req ->
            make ArceusService.getAreaDetail req GetAreaDetailRes

        CreateArea req ->
            make ArceusService.createArea req CreateAreaRes

        EditArea req ->
            make ArceusService.editArea req EditAreaRes

        Translate req ->
            make ArceusService.translate req TranslateRes

        LoadTilesetTexture config ->
            Texture.loadWith
                { nonPowerOfTwoOptions | magnify = Texture.nearest, minify = Texture.nearest }
                config.tilesetPath
                |> Task.map
                    (\texture ->
                        let
                            ( _, height ) =
                                Texture.size texture

                            rows =
                                height // config.tileSizeInPx
                        in
                        { texture = texture
                        , rows = rows
                        , columns = config.tilesPerRow
                        , path = config.tilesetPath
                        }
                    )
                |> Task.attempt (LoadTilesetTextureRes config)

        LoadPokemonTexture pokemonNumber ->
            let
                path =
                    Textures.Api.pokemonTexturePath pokemonNumber

                fromTexture texture =
                    { texture = texture, columns = 4, rows = 4, path = path }
            in
            Texture.load path
                |> Task.map fromTexture
                |> Task.attempt (LoadPokemonTextureRes pokemonNumber)

        LoadTrainerTexture trainerNumber ->
            let
                path =
                    TrainerSprite.trainerNumberToPath trainerNumber

                fromTexture texture =
                    { texture = texture, columns = 4, rows = 4, path = path }
            in
            Texture.load path
                |> Task.map fromTexture
                |> Task.attempt (LoadTrainerTextureRes trainerNumber)

        LoadItemTexture item ->
            let
                path =
                    Item.toSpritePath item

                fromTexture texture =
                    { texture = texture, columns = 1, rows = 17, path = path }
            in
            Texture.load path
                |> Task.map fromTexture
                |> Task.attempt (LoadItemTextureRes item)

        LoadTilemapTexture metadataReq metadataRes ->
            Texture.loadWith { nonPowerOfTwoOptions | magnify = Texture.nearest } metadataRes.path
                |> Task.map
                    (\texture ->
                        { texture = texture
                        , offset = { x = metadataRes.offsetX, y = metadataRes.offsetY }
                        , layers = metadataRes.layers
                        }
                    )
                |> Task.attempt (LoadTilemapTextureRes metadataReq metadataRes)

        RefreshAuth ->
            Auth.refreshAuth |> Cmd.map RefreshAuthRes


emitSuccessEvent : Events msg -> ApiResponse -> Cmd msg
emitSuccessEvent events response =
    let
        make event req res =
            case res of
                Ok ok ->
                    dispatch (event req ok)

                Err _ ->
                    Cmd.none
    in
    case response of
        GetAllPokemonRes req res ->
            make events.onGetAllPokemon req res

        AddWarpRes req res ->
            make events.onAddWarp req res

        DeleteWarpRes req res ->
            make events.onDeleteWarp req res

        GetMapsRes req res ->
            make events.onGetMaps req res

        GetStartTilesRes req res ->
            make events.onGetStartTiles req res

        GetTileDetailsRes req res ->
            make events.onGetTileDetails req res

        GetTilesetConfigRes req res ->
            make
                (\_ r ->
                    TriggerSubrequest (serializeApiCall <| GetTilesetConfig req)
                        (LoadTilesetTexture r)
                        |> events.onCallUpdate
                )
                req
                res

        GetTextureMetadataRes req res ->
            make
                (\re r ->
                    TriggerSubrequest (serializeApiCall <| GetTextureMetadata req)
                        (LoadTilemapTexture re r)
                        |> events.onCallUpdate
                )
                req
                res

        GetTileConfigRes req res ->
            make events.onGetTileConfig req res

        CreateMapRes req res ->
            make events.onCreateMap req res

        EditMapRes req res ->
            make events.onEditMap req res

        FindFittingTilesRes req res ->
            make events.onFindFittingTiles req res

        AddStartTileRes req res ->
            make events.onAddStartTile req res

        GetAudioTracksRes req res ->
            make events.onGetAudioTracks req res

        GetAreasRes req res ->
            make events.onGetAreas req res

        GetAreaDetailRes req res ->
            make events.onGetAreaDetail req res

        CreateAreaRes req res ->
            make events.onCreateArea req res

        EditAreaRes req res ->
            make events.onEditArea req res

        TranslateRes req res ->
            make events.onTranslate req res

        LoadTilesetTextureRes req res ->
            make events.onLoadTilesetTexture req res

        LoadTrainerTextureRes req res ->
            make events.onLoadTrainerTexture req res

        LoadItemTextureRes req res ->
            make events.onLoadItemTexture req res

        LoadPokemonTextureRes req res ->
            make events.onLoadPokemonTexture req res

        LoadTilemapTextureRes metadataReq metadataRes res ->
            make (events.onLoadTilemapTexture metadataReq) metadataRes res

        RefreshAuthRes (Ok auth) ->
            AuthRefreshed auth |> events.onCallUpdate |> dispatch

        RefreshAuthRes (Err _) ->
            Cmd.none


setRequestStart : Time.Posix -> GrpcRequestState -> GrpcRequestState
setRequestStart time state =
    { state | loadingSince = Just time }


setLanguage : Translations.Language -> GrpcRequestState -> GrpcRequestState
setLanguage lang state =
    { state | language = Just lang }


setRequestEnd : Time.Posix -> GrpcRequestState -> GrpcRequestState
setRequestEnd time state =
    { state | lastResponseAt = Just time }


setResponse : ApiResponse -> GrpcRequestState -> GrpcRequestState
setResponse res state =
    { state | error = responseToError res }


addSubrequest : ApiCall -> GrpcRequestState -> GrpcRequestState
addSubrequest call state =
    { state | subrequests = serializeApiCall call :: state.subrequests }


responseToError : ApiResponse -> Maybe Error.Error
responseToError res =
    case res of
        GetAllPokemonRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        AddWarpRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        DeleteWarpRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetMapsRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetStartTilesRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTileDetailsRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTilesetConfigRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTextureMetadataRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetTileConfigRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        CreateMapRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        EditMapRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        FindFittingTilesRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        AddStartTileRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetAreasRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetAreaDetailRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        GetAudioTracksRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        CreateAreaRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        EditAreaRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        TranslateRes _ r ->
            Result.Extra.error r |> Maybe.map Error.fromGrpcError

        LoadTilesetTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadPokemonTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadTrainerTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadItemTextureRes _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        LoadTilemapTextureRes _ _ r ->
            Result.Extra.error r |> Maybe.map Error.TextureError

        RefreshAuthRes r ->
            Result.Extra.error r


cacheStrategy : ApiCall -> CacheStrategy
cacheStrategy call =
    case call of
        CreateMap _ ->
            mutationCacheStrategy

        GetTileDetails _ ->
            -- Has extra cache invalidation mechanism via websocket
            { defaultCacheStrategy | forMillis = 10000000, refreshIfLanguageChanges = False }

        GetTextureMetadata _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        GetTilesetConfig _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadTilesetTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadPokemonTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadTrainerTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadItemTexture _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        LoadTilemapTexture _ _ ->
            { defaultCacheStrategy | refreshIfLanguageChanges = False }

        _ ->
            defaultCacheStrategy


retrieveRegistered : RequestManager -> List ( ApiCall, GrpcRequestState )
retrieveRegistered (RequestManager manager) =
    manager.registered
        |> List.filterMap
            (\apiCall ->
                Dict.get
                    (serializeApiCall apiCall
                        |> identifierToComparable
                    )
                    manager.grpc
                    |> Maybe.map (Tuple.pair apiCall)
            )


registerInternal : ApiCall -> RequestManager -> RequestManager
registerInternal call (RequestManager manager) =
    RequestManager
        { manager
            | registered = appendNonDuplicate call manager.registered
            , grpc =
                Dict.update (serializeApiCall call |> identifierToComparable)
                    (Maybe.withDefault (initialRequestState (cacheStrategy call)) >> Just)
                    manager.grpc
        }


unregisterInternal : ApiCall -> RequestManager -> RequestManager
unregisterInternal call (RequestManager manager) =
    RequestManager { manager | registered = List.filter ((/=) call) manager.registered }


tilesetIdentifier : GetTilesetConfigResponse -> RequestIdentifier
tilesetIdentifier response =
    { serializedRequest =
        String.fromInt response.tileSizeInPx ++ "_" ++ String.fromInt response.tilesPerRow
    , path = response.tilesetPath
    }


identifierToTracker : RequestIdentifier -> String
identifierToTracker { serializedRequest, path } =
    -- use some seperator like % that is not in the base64 alphabet and should not be in the URL either
    path ++ "%" ++ serializedRequest


identifierToComparable : RequestIdentifier -> ( String, String )
identifierToComparable { path, serializedRequest } =
    ( path, serializedRequest )


type alias GrpcRequestConfig s req res =
    { rpc : Grpc.Rpc req res
    , session : Session s
    , body : req
    , identifier : RequestIdentifier
    }


{-| Perform a gRPC request. The rpc is expected to be generated via `protoc-gen-elm`
and imported from the `generated` folder.
Since this returns a task, you may chain other tasks after it without
having to go through the `update` function.
-}
grpc : GrpcRequestConfig a req res -> Cmd (Result Grpc.Error res)
grpc { rpc, session, body, identifier } =
    let
        acceptLanguage =
            Translations.currentLanguage session.i18n
                |> Translations.languageToString
    in
    Grpc.new rpc body
        |> Auth.addHeaders session.auth
        |> Grpc.addHeader "accept-language" acceptLanguage
        |> Grpc.setTracker (identifierToTracker identifier)
        |> Grpc.toCmd identity


encoderToString : Protobuf.Encode.Encoder -> String
encoderToString =
    Protobuf.Encode.encode
        >> Base64.fromBytes
        >> Maybe.withDefault ""


appendNonDuplicate : a -> List a -> List a
appendNonDuplicate a list =
    case list of
        [] ->
            [ a ]

        first :: rest ->
            if first == a then
                list

            else
                first :: appendNonDuplicate a rest


dispatch : msg -> Cmd msg
dispatch =
    Task.succeed >> Task.perform identity
