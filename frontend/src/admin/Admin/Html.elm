module Admin.Html exposing (Attribute, Context, Document, Html)

import Admin.Translations exposing (I18n)
import Html.WithContext
import Shared.Background exposing (BackgroundImage)
import Time


type alias Document msg =
    { title : I18n -> String
    , body : List (Html msg)
    }


type alias Context =
    { i18n : I18n
    , background : BackgroundImage
    , timezone : Time.Zone
    , disabled : Bool
    , fontSize : Int
    }


type alias Html msg =
    Html.WithContext.Html Context msg


type alias Attribute msg =
    Html.WithContext.Attribute Context msg
