module Admin.Route exposing (..)

import Admin.AreaEditor.Route as AreaEditor
import AreaId exposing (AreaId)
import Browser.Navigation as Nav
import MapId exposing (MapId)
import Url exposing (Url)
import UrlCodec exposing (UrlCodec, andThen, buildUnion, buildUrl, chain, parseUrl, s, top, union, variant0, variant1, variant3)


type KnownRoute
    = Overview
    | Detail MapId
    | EditArea MapId AreaId AreaEditor.Route
    | Logout


type alias Route =
    Result Url KnownRoute


routeCodec : UrlCodec (KnownRoute -> a) a KnownRoute
routeCodec =
    s "admin"
        |> chain
            (union
                (\overview detail editArea logout value ->
                    case value of
                        Overview ->
                            overview

                        Detail mapId ->
                            detail mapId

                        EditArea mapId areaId editAreaRoute ->
                            editArea mapId areaId editAreaRoute

                        Logout ->
                            logout
                )
                |> variant0 Overview top
                |> variant1 Detail MapId.urlCodec
                |> variant3 EditArea (MapId.urlCodec |> chain (s "area") |> andThen AreaId.urlCodec |> andThen AreaEditor.codec)
                |> variant0 Logout (s "logout")
                |> buildUnion
            )


navigate : Nav.Key -> KnownRoute -> Cmd msg
navigate key r =
    fromRoute r |> Nav.pushUrl key


toRoute : Url -> Result Url KnownRoute
toRoute url =
    parseUrl routeCodec url
        |> Result.fromMaybe url


fromRoute : KnownRoute -> String
fromRoute r =
    buildUrl routeCodec r
