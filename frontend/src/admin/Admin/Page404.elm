module Admin.Page404 exposing (view)

import Admin.Html exposing (Document)
import Admin.Translations as Translations
import Atoms.Html exposing (text)


view : m -> Document msg
view _ =
    { title = Translations.notFoundTitle
    , body = [ text Translations.notFoundTitle ]
    }
