module Admin.Detail.Types.Area exposing (..)

import Admin.Detail.Types.ItemProbabilities exposing (ItemProbabilities)
import Admin.Detail.Types.Selection as Selection exposing (Selection)
import AreaId exposing (AreaId)
import AudioTrackNumber exposing (AudioTrackNumber)
import Dict exposing (Dict)
import ItemDict
import Proto.Arceus
import Shared.Types.Cuboid as Cuboid
import Tagged exposing (tag)


type alias Area =
    { selection : Selection
    , name : String
    , audioTrackNumber : Maybe AudioTrackNumber
    , id : AreaId
    , encounterProbabilities : Dict Int Float
    , itemProbabilities : ItemProbabilities
    }


encounterProbabilitiesFromProto : List Proto.Arceus.Encounter -> Dict Int Float
encounterProbabilitiesFromProto =
    List.map (\{ pokemonNumber, probability } -> ( pokemonNumber, probability ))
        >> Dict.fromList


singleFromProto : Proto.Arceus.Area -> Area
singleFromProto { id, name, areaParts, backgroundMusicTrack } =
    { id = tag id
    , name = name
    , audioTrackNumber = Maybe.map tag backgroundMusicTrack
    , selection = List.map Cuboid.fromProto areaParts |> Cuboid.fromCuboids |> Selection.fromCoords
    , encounterProbabilities = Dict.empty
    , itemProbabilities = { probabilities = ItemDict.empty }
    }


fromProto : List Proto.Arceus.Area -> List Area
fromProto =
    List.map singleFromProto
