module Admin.Detail.Types.Encounters exposing (Encounters, fromProto, get, getBySpawnKind, remove, toInputId, toList, toProto, upsertEncounter)

import Dict exposing (Dict)
import PokemonNumber exposing (PokemonNumber)
import Proto.Arceus
import Proto.Arceus.SpawnKind as SpawnKind exposing (SpawnKind)
import Tagged exposing (tag, untag)


type alias Encounters =
    Dict ( Int, Int ) Float


toInputId : SpawnKind -> PokemonNumber -> String
toInputId spawnKind pokemonNumber =
    String.fromInt (serializeSpawnKind spawnKind)
        ++ "-"
        ++ String.fromInt (untag pokemonNumber)


addPlaceholder : Encounters -> Encounters
addPlaceholder =
    Dict.insert ( serializeSpawnKind SpawnKind.Grass, placeholderPokemonNumber ) 0


upsertEncounter : SpawnKind -> PokemonNumber -> Float -> Encounters -> Encounters
upsertEncounter spawnKind pokemonNumber probability =
    Dict.insert ( serializeSpawnKind spawnKind, untag pokemonNumber ) probability
        >> addPlaceholder


remove : SpawnKind -> PokemonNumber -> Encounters -> Encounters
remove spawnKind pokemonId =
    Dict.remove ( serializeSpawnKind spawnKind, untag pokemonId )


get : SpawnKind -> Int -> Encounters -> Maybe Float
get spawnKind pokemonId =
    Dict.get ( serializeSpawnKind spawnKind, pokemonId )


getBySpawnKind : SpawnKind -> Encounters -> List ( PokemonNumber, Float )
getBySpawnKind spawnKind =
    toList
        >> List.filterMap
            (\( kind, pokemonNumber, probability ) ->
                if kind == spawnKind then
                    Just ( pokemonNumber, probability )

                else
                    Nothing
            )


toList : Encounters -> List ( SpawnKind, PokemonNumber, Float )
toList =
    Dict.toList >> List.map (\( ( serializedSpawnKind, pokemonId ), probability ) -> ( deserializeSpawnKind serializedSpawnKind, tag pokemonId, probability ))


fromProto : List Proto.Arceus.Encounter -> Encounters
fromProto =
    List.map
        (\{ pokemonNumber, probability, spawnKind } ->
            ( ( serializeSpawnKind spawnKind
              , pokemonNumber
              )
            , probability
            )
        )
        >> Dict.fromList
        >> addPlaceholder


toProto : Encounters -> List Proto.Arceus.Encounter
toProto encs =
    let
        singleToProto ( ( serializedSpawnKind, pokemonNumber ), probability ) =
            { pokemonNumber = pokemonNumber
            , probability = probability
            , spawnKind = deserializeSpawnKind serializedSpawnKind
            }

        isPlaceholder ( _, pokemonNumber ) =
            pokemonNumber == placeholderPokemonNumber
    in
    Dict.toList encs |> List.filter (not << isPlaceholder << Tuple.first) |> List.map singleToProto


serializeSpawnKind : SpawnKind -> Int
serializeSpawnKind kind =
    case kind of
        SpawnKind.Grass ->
            0

        SpawnKind.Water ->
            1

        SpawnKind.SpawnKindUnrecognized_ _ ->
            2


deserializeSpawnKind : Int -> SpawnKind
deserializeSpawnKind int =
    case int of
        0 ->
            SpawnKind.Grass

        1 ->
            SpawnKind.Water

        n ->
            SpawnKind.SpawnKindUnrecognized_ n


placeholderPokemonNumber : Int
placeholderPokemonNumber =
    -1
