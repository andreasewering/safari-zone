module Admin.Detail.Types.TileConfig exposing (..)

import Dict exposing (Dict)
import Proto.Smeargle as Proto


type alias TileMetadata =
    { layer : Int
    , keywords : List String
    }


type alias TileConfig =
    Dict Int TileMetadata


tileIdToLayer : TileConfig -> Int -> Maybe Int
tileIdToLayer config tileId =
    Dict.get tileId config |> Maybe.map .layer


fromProto : List Proto.TileConfiguration -> TileConfig
fromProto =
    List.indexedMap
        (\i { layer, keywords } ->
            ( i + 1
            , { layer = layer, keywords = keywords }
            )
        )
        >> Dict.fromList
