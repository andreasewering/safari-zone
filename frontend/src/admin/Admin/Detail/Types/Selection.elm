module Admin.Detail.Types.Selection exposing
    ( Selection
    , checkSingle
    , fromCoords
    , fromCorners
    , getSelected
    , init
    , isEmpty
    , isOnEdge
    , isSelected
    , resetSaved
    , save
    , topLeft
    )

import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Maybe.Extra
import Set exposing (Set)


type alias Selection =
    { ranges : Maybe { left : Int, right : Int, top : Int, bottom : Int, layerNumber : LayerNumber }
    , savedSelection : Set ( LayerNumber, Int, Int )
    }


init : Selection
init =
    { ranges = Nothing, savedSelection = Set.empty }


fromCoords : Set ( LayerNumber, Int, Int ) -> Selection
fromCoords coords =
    { ranges = Nothing, savedSelection = coords }


fromCorners : LayerNumber -> Coordinate Int -> Coordinate Int -> Selection -> Selection
fromCorners layerNumber a b selection =
    let
        ( left, right ) =
            if a.x < b.x then
                ( a.x, b.x )

            else
                ( b.x, a.x )

        ( top, bottom ) =
            if a.y < b.y then
                ( a.y, b.y )

            else
                ( b.y, a.y )
    in
    { ranges = Just { left = left, right = right, top = top, bottom = bottom, layerNumber = layerNumber }
    , savedSelection = selection.savedSelection
    }


resetSaved : Selection -> Selection
resetSaved selection =
    { selection | savedSelection = Set.empty }


isSelected : LayerNumber -> Coordinate Int -> Selection -> Bool
isSelected layerNumber { x, y } selection =
    let
        inRange =
            case selection.ranges of
                Just { left, right, top, bottom } ->
                    left <= x && right >= x && top <= y && bottom >= y

                Nothing ->
                    False
    in
    inRange || Set.member ( layerNumber, x, y ) selection.savedSelection


topLeft : LayerNumber -> Selection -> Maybe (Coordinate Int)
topLeft layerNumber =
    getSelected
        >> Set.toList
        >> List.filter (\( l, _, _ ) -> l == layerNumber)
        >> List.head
        >> Maybe.map (\( _, x, y ) -> { x = x, y = y })


isEmpty : Selection -> Bool
isEmpty selection =
    Maybe.Extra.isNothing selection.ranges && Set.isEmpty selection.savedSelection


checkSingle : Selection -> Maybe (Coordinate Int)
checkSingle selection =
    let
        checkRangeSingle { left, right, top, bottom } =
            if left == right && top == bottom then
                Just { x = left, y = top }

            else
                Nothing
    in
    case ( Set.isEmpty selection.savedSelection, selection.ranges ) of
        ( True, Just ranges ) ->
            checkRangeSingle ranges

        _ ->
            Nothing


isOnEdge : LayerNumber -> Coordinate Int -> Selection -> { left : Bool, right : Bool, top : Bool, bottom : Bool }
isOnEdge layerNumber ({ x, y } as coord) selection =
    let
        isSelectedItself =
            isSelected layerNumber coord selection
    in
    { left = isSelectedItself && not (isSelected layerNumber { coord | x = x - 1 } selection)
    , right = isSelectedItself && not (isSelected layerNumber { coord | x = x + 1 } selection)
    , top = isSelectedItself && not (isSelected layerNumber { coord | y = y - 1 } selection)
    , bottom = isSelectedItself && not (isSelected layerNumber { coord | y = y + 1 } selection)
    }


getSelected : Selection -> Set ( LayerNumber, Int, Int )
getSelected selection =
    let
        coordsInRange { left, right, top, bottom, layerNumber } =
            Coordinate.cartesianProduct (List.range left right) (List.range top bottom)
                |> List.map (\{ x, y } -> ( layerNumber, x, y ))
                |> Set.fromList
    in
    selection.ranges
        |> Maybe.map coordsInRange
        |> Maybe.withDefault Set.empty
        |> Set.union selection.savedSelection


save : Selection -> Selection
save selection =
    { savedSelection = getSelected selection, ranges = Nothing }
