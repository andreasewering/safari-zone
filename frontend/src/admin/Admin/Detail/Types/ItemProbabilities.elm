module Admin.Detail.Types.ItemProbabilities exposing (ItemProbabilities, fromProto, get, init, remove, toList, toProto, upsertItem)

import Item exposing (Item)
import ItemDict exposing (ItemDict)
import Proto.Arceus exposing (ItemConfig)


type alias ItemProbabilities =
    { probabilities : ItemDict Float }


init : ItemProbabilities
init =
    { probabilities = ItemDict.empty }


get : Item -> ItemProbabilities -> Maybe Float
get item { probabilities } =
    ItemDict.get item probabilities


upsertItem : Item -> Float -> ItemProbabilities -> ItemProbabilities
upsertItem item probability itemProbabilities =
    { itemProbabilities | probabilities = ItemDict.insert item probability itemProbabilities.probabilities }


remove : Item -> ItemProbabilities -> ItemProbabilities
remove item itemProbabilities =
    { itemProbabilities | probabilities = ItemDict.remove item itemProbabilities.probabilities }


toList : ItemProbabilities -> List ( Item, Float )
toList { probabilities } =
    ItemDict.toList probabilities


fromProto : List ItemConfig -> ItemProbabilities
fromProto itemConfigs =
    let
        singleFromProto { item, probability } =
            Maybe.map (\i -> ( i, probability ))
                (Item.fromProto item)
    in
    { probabilities = List.filterMap singleFromProto itemConfigs |> ItemDict.fromList }


toProto : ItemProbabilities -> List ItemConfig
toProto { probabilities } =
    ItemDict.toList probabilities
        |> List.map
            (\( item, probability ) ->
                { item = Item.toProto item, probability = probability }
            )
