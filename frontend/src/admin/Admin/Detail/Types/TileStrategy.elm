module Admin.Detail.Types.TileStrategy exposing (..)


type TileStrategy
    = Manual
    | AskServer


next : TileStrategy -> TileStrategy
next strategy =
    case strategy of
        Manual ->
            AskServer

        AskServer ->
            Manual
