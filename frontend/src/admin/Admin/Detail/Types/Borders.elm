module Admin.Detail.Types.Borders exposing (Borders, combine, each, fold, map, toAttribute)

import Html.WithContext
import Html.WithContext.Attributes


type alias Borders =
    BorderLike Int


type alias BorderLike a =
    { bottom : a, left : a, right : a, top : a }


map : (a -> b) -> BorderLike a -> BorderLike b
map f b =
    { bottom = f b.bottom, left = f b.left, right = f b.right, top = f b.top }


fold : List (BorderLike number) -> BorderLike number
fold =
    List.foldl combine (each 0)


each : a -> BorderLike a
each n =
    { bottom = n, left = n, right = n, top = n }


combine : BorderLike comparable -> BorderLike comparable -> BorderLike comparable
combine b1 b2 =
    { bottom = max b1.bottom b2.bottom
    , left = max b1.left b2.left
    , right = max b1.right b2.right
    , top = max b1.top b2.top
    }


toAttribute : BorderLike Float -> Html.WithContext.Attribute ctx msg
toAttribute { bottom, left, right, top } =
    Html.WithContext.Attributes.style "border-width" <|
        String.join " " <|
            List.map (\px -> String.fromFloat px ++ "px") [ top, right, bottom, left ]
