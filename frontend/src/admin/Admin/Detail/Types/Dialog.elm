module Admin.Detail.Types.Dialog exposing (ChoosePokemonDialogProps, CreateAreaDialogProps, Dialog(..), isCreateAreaDialog)

import Domain.Coordinate exposing (Coordinate)
import PokemonNumber exposing (PokemonNumber)


type Dialog
    = CreateArea CreateAreaDialogProps
    | ChooseMapForWarp (Coordinate Int)


isCreateAreaDialog : Dialog -> Maybe CreateAreaDialogProps
isCreateAreaDialog dialog =
    case dialog of
        CreateArea props ->
            Just props

        _ ->
            Nothing


type alias CreateAreaDialogProps =
    { name : String
    }


type alias ChoosePokemonDialogProps =
    { useChoice : PokemonNumber -> Maybe Dialog
    }
