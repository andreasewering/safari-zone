module Admin.Detail.Types.TileDict exposing (Tile, TileDict, TileId, addTile, addTiles, centerAndScaleMap, empty, fromProto, getEntries, removeTiles, singleTileFromProto, toProto)

import Dict exposing (Dict)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Proto.Smeargle as Proto


type alias TileId =
    Int


{-| Important invariant: the list of tiles is ordered by LayerNumber and LayerType!
-}
type alias TileDict =
    Dict ( Int, Int ) (List TileMember)


empty : TileDict
empty =
    Dict.empty


type alias TileMember =
    HasLayer
        { tileId : Int
        }


type alias HasLayer a =
    { a
        | layerNumber : LayerNumber
        , layer : Int
    }


type alias HasPosition a =
    HasLayer
        { a
            | coord : Coordinate Int
        }


toDictRepresentation : Tile -> TileMember
toDictRepresentation { tileId, layerNumber, layer } =
    { tileId = tileId
    , layerNumber = layerNumber
    , layer = layer
    }


type alias Tile =
    HasPosition
        { tileId : Int
        }


addTile : Tile -> TileDict -> TileDict
addTile tile =
    let
        newTile =
            toDictRepresentation tile
    in
    Dict.update (Coordinate.toTuple tile.coord) (Maybe.map (addTileToList newTile) >> Maybe.withDefault [ newTile ] >> Just)


addTiles : List Tile -> TileDict -> TileDict
addTiles tiles tileDict =
    List.foldl addTile tileDict tiles


removeTile :
    { a | coord : Coordinate Int, layerNumber : Int }
    -> TileDict
    -> TileDict
removeTile tile =
    Dict.update (Coordinate.toTuple tile.coord)
        (Maybe.map (deleteTileFromList tile))


removeTiles : List { a | coord : Coordinate Int, layerNumber : Int } -> TileDict -> TileDict
removeTiles tiles tileDict =
    List.foldl removeTile tileDict tiles


getEntries : TileDict -> List { position : Coordinate Int, layerNumber : LayerNumber, tileId : Int }
getEntries =
    Dict.toList
        >> List.concatMap
            (\( ( x, y ), tiles ) ->
                List.map
                    (\{ layerNumber, tileId } -> { position = { x = x, y = y }, layerNumber = layerNumber, tileId = tileId })
                    tiles
            )


fromProto : List Proto.TileDetail -> TileDict
fromProto =
    let
        addToDict tile =
            Dict.update (Coordinate.toTuple tile.coord) <|
                \mayPrevTiles ->
                    case mayPrevTiles of
                        Just prevTiles ->
                            Just <| addTileToList (toDictRepresentation tile) prevTiles

                        Nothing ->
                            Just [ toDictRepresentation tile ]
    in
    List.map singleTileFromProto
        >> List.foldl addToDict empty


singleTileFromProto : Proto.TileDetail -> Tile
singleTileFromProto tile =
    { tileId = tile.tileId
    , layerNumber = tile.layerNumber
    , layer = tile.layer
    , coord = { x = tile.x, y = tile.y }
    }


toProto : TileDict -> List Proto.ChosenTile
toProto current =
    let
        convertSingle : ( Int, Int ) -> TileMember -> Proto.ChosenTile
        convertSingle ( x, y ) tileMember =
            { layerNumber = tileMember.layerNumber
            , tileId = tileMember.tileId
            , x = x
            , y = y
            }
    in
    Dict.toList current |> List.concatMap (\( pos, members ) -> List.map (convertSingle pos) members)


addTileToList : TileMember -> List TileMember -> List TileMember
addTileToList tile =
    List.filter (\{ layerNumber, layer } -> layerNumber /= tile.layerNumber || layer /= tile.layer)
        >> (::) tile
        >> List.sortWith orderTiles


deleteTileFromList : { a | layerNumber : Int } -> List TileMember -> List TileMember
deleteTileFromList tile =
    List.filter
        (\{ layerNumber } ->
            layerNumber /= tile.layerNumber
        )


orderTiles : TileMember -> TileMember -> Order
orderTiles t1 t2 =
    compare t1.layer t2.layer


{-| Perfectly center and zoom out map so that all tiles are on screen
-}
centerAndScaleMap : { s | width : Int, height : Int } -> TileDict -> { offset : Coordinate Int, tileSize : Int }
centerAndScaleMap screen tileDict =
    let
        { topLeftX, topLeftY, bottomRightX, bottomRightY } =
            Dict.foldl
                (\( x, y ) _ acc ->
                    { topLeftX = min x acc.topLeftX
                    , topLeftY = min y acc.topLeftY
                    , bottomRightX = max x acc.bottomRightX
                    , bottomRightY = max y acc.bottomRightY
                    }
                )
                { topLeftX = 0, topLeftY = 0, bottomRightX = 0, bottomRightY = 0 }
                tileDict
                |> minSize 20

        minSize size corners =
            { topLeftX = corners.topLeftX - max (size - (corners.bottomRightX - corners.topLeftX)) 0 // 2
            , topLeftY = corners.topLeftY - max (size - (corners.bottomRightY - corners.topLeftY)) 0 // 2
            , bottomRightX = corners.bottomRightX + max (size - (corners.bottomRightY - corners.topLeftY)) 0 // 2
            , bottomRightY = corners.bottomRightY + max (size - (corners.bottomRightY - corners.topLeftY)) 0 // 2
            }

        offset =
            { x = -topLeftX * tileSize, y = -topLeftY * tileSize }

        tileSizeX =
            screen.width // (bottomRightX - topLeftX + 1)

        tileSizeY =
            screen.height // (bottomRightY - topLeftY + 1)

        tileSize =
            min tileSizeX tileSizeY
    in
    { offset = offset
    , tileSize = tileSize
    }
