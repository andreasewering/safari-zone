module Admin.Detail.Grid exposing (DragState(..), GridMode(..), GridModel, drag, release, touch, view)

import Admin.Detail.Types.Selection as Selection exposing (Selection)
import Admin.Html exposing (Attribute, Html)
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Html
import Html.Events.Extra.Pointer as Pointer
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class, style)
import Shared.Types.Generic exposing (Setter)


type DragState
    = WaitingForDrag
    | DraggingFrom (Coordinate Int)


type GridMode
    = Drag
    | SelectSingle
    | SelectMulti


type alias GridModel model =
    { model
        | dragState : DragState
        , gridMode : GridMode
        , selection : Selection
        , tileSize : Int
        , offset : Coordinate Int
    }


touch : LayerNumber -> Coordinate Int -> Setter (GridModel model)
touch layerNumber coord model =
    case model.gridMode of
        Drag ->
            { model | dragState = DraggingFrom (Coordinate.minus coord model.offset) }

        SelectSingle ->
            let
                selectedTile =
                    Coordinate.minus coord model.offset
                        |> Coordinate.map
                            (\dim -> floor <| toFloat dim / toFloat model.tileSize)
            in
            { model
                | dragState = DraggingFrom selectedTile
                , selection = Selection.fromCorners layerNumber selectedTile selectedTile model.selection |> Selection.resetSaved
            }

        SelectMulti ->
            let
                selectedTile =
                    Coordinate.minus coord model.offset
                        |> Coordinate.map
                            (\dim -> floor <| toFloat dim / toFloat model.tileSize)
            in
            { model
                | dragState = DraggingFrom selectedTile
                , selection = Selection.fromCorners layerNumber selectedTile selectedTile model.selection
            }


drag : LayerNumber -> Coordinate Int -> Setter (GridModel model)
drag layerNumber coord model =
    case ( model.gridMode, model.dragState ) of
        ( Drag, DraggingFrom start ) ->
            { model | offset = Coordinate.minus coord start }

        ( SelectSingle, DraggingFrom fromTile ) ->
            let
                toTile =
                    Coordinate.minus coord model.offset
                        |> Coordinate.map
                            (\dim -> floor <| toFloat dim / toFloat model.tileSize)
            in
            { model | selection = Selection.fromCorners layerNumber fromTile toTile model.selection }

        ( SelectMulti, DraggingFrom fromTile ) ->
            let
                toTile =
                    Coordinate.minus coord model.offset
                        |> Coordinate.map
                            (\dim -> floor <| toFloat dim / toFloat model.tileSize)
            in
            { model | selection = Selection.fromCorners layerNumber fromTile toTile model.selection }

        _ ->
            model


release : Setter (GridModel model)
release model =
    let
        onRelease =
            case model.gridMode of
                SelectMulti ->
                    Selection.save

                _ ->
                    identity
    in
    { model | dragState = WaitingForDrag, selection = onRelease model.selection }


{-|

    The way this works:
    There is a certain offset the map is at, which is the top left coordinate of the screen.
    We can figure out what the difference of the top left coordinate of the top left tile is (assuming the grid is "infinite"):
        topLeft = remainder (offset / tileSize)

    With that info we can just render screenWidth / tileSize many tiles with the offset given by `topLeft`.

-}
view :
    { onDown : Coordinate Int -> msg
    , onMove : Coordinate Int -> msg
    , onUp : Coordinate Int -> msg
    }
    -> (Coordinate Int -> List (Attribute msg))
    -> { s | height : Int, width : Int }
    -> GridModel model
    -> Html msg
view events tileAttrs { width, height } model =
    let
        cursorType =
            case ( model.gridMode, model.dragState ) of
                ( Drag, WaitingForDrag ) ->
                    class "cursor-grab"

                ( Drag, DraggingFrom _ ) ->
                    class "cursor-grabbing"

                _ ->
                    class "cursor-crosshair"

        pxToMoveLeft =
            model.tileSize - remainderBy model.tileSize model.offset.x

        pxToMoveUp =
            model.tileSize - remainderBy model.tileSize model.offset.y

        gridX =
            ceiling <| toFloat width / toFloat model.tileSize

        gridY =
            ceiling <| toFloat height / toFloat model.tileSize

        singleRow coordY =
            List.range -1 gridX
                |> List.map
                    (\coordX ->
                        let
                            tileCoord =
                                Coordinate.minus { x = coordX, y = coordY }
                                    (Coordinate.map (\dim -> dim // model.tileSize) model.offset)
                        in
                        emptyTile model.tileSize <| tileAttrs tileCoord
                    )
                |> div [ class "flex w-full", style "height" <| String.fromInt model.tileSize ++ "px" ]
    in
    div
        ([ class "fixed flex flex-col"
         , cursorType
         , bindEvent Pointer.onDown events.onDown
         , style "top" <| String.fromInt -pxToMoveUp ++ "px"
         , style "left" <| String.fromInt -pxToMoveLeft ++ "px"
         , style "height" <| String.fromInt height ++ "px"
         ]
            ++ (case model.dragState of
                    DraggingFrom _ ->
                        [ bindEvent Pointer.onMove events.onMove
                        , bindEvent Pointer.onUp events.onUp
                        , bindEvent Pointer.onLeave events.onUp
                        ]

                    _ ->
                        []
               )
        )
    <|
        List.map singleRow <|
            List.range -1 gridY


emptyTile : Int -> List (Attribute msg) -> Html msg
emptyTile tileSize extraAttrs =
    div ([ style "width" <| String.fromInt tileSize ++ "px", style "height" <| String.fromInt tileSize ++ "px" ] ++ extraAttrs) []


bindEvent : ((Pointer.Event -> msg) -> Html.Attribute msg) -> (Coordinate Int -> msg) -> Html.WithContext.Attribute ctx msg
bindEvent ev callback =
    Html.WithContext.htmlAttribute <| ev (eventToTuple >> callback)


eventToTuple : Pointer.Event -> Coordinate Int
eventToTuple { pointer } =
    let
        ( x, y ) =
            pointer.clientPos
    in
    { x = floor x, y = floor y }
