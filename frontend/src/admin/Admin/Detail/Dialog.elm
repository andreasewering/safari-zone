module Admin.Detail.Dialog exposing (view)

import Admin.Detail.Model exposing (PageModel)
import Admin.Detail.Types.Dialog as Dialog exposing (CreateAreaDialogProps, Dialog)
import Admin.Html exposing (Html)
import Admin.Session exposing (Session)
import Admin.Translations as Translations
import Admin.View.MapPreview as Map
import Atoms.Button
import Atoms.Html
import Atoms.Input
import Atoms.Modal
import BoundedDict
import Domain.Coordinate exposing (Coordinate)
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events as Events
import MapId exposing (MapId)
import Tagged exposing (tag)


type alias Model m =
    Session { m | detailModel : PageModel }


type alias Events msg =
    { onClose : msg
    , noOp : msg
    , createArea : CreateAreaDialogProps -> msg
    , openInnerMap : MapId -> Coordinate Int -> msg
    , setDialog : Dialog -> msg
    }


view : Events msg -> Model m -> Html msg
view events model =
    let
        modalContent dialog =
            case dialog of
                Dialog.CreateArea props ->
                    createAreaDialog events props

                Dialog.ChooseMapForWarp warpFrom ->
                    mapSelection events model warpFrom
    in
    case model.detailModel.dialog of
        Just dialog ->
            Atoms.Modal.modal { onClose = events.onClose, noOp = events.noOp } (modalContent dialog)

        Nothing ->
            Atoms.Html.none


createAreaDialog : Events msg -> Dialog.CreateAreaDialogProps -> List (Html msg)
createAreaDialog events props =
    [ Atoms.Input.text
        { id = "create_area"
        , onChange = \name -> events.setDialog <| Dialog.CreateArea { props | name = name }
        , text = props.name
        , label = Translations.mapCreatorDetailAreaNameLabel
        , error = Nothing
        }
    , Atoms.Html.text Translations.mapCreatorDetailAreaMusicTrackLabel
    , Atoms.Button.button
        { onPress = events.createArea props |> Just
        , label = Atoms.Html.text Translations.mapCreatorDetailAreaDialogSubmit
        , loading = False
        }
        []
    ]


mapSelection : Events msg -> Model m -> Coordinate Int -> List (Html msg)
mapSelection events model warpFrom =
    let
        mapSize =
            { x = 100, y = 60 }

        namedMapToMapProps { mapName, mapId } =
            { id = tag mapId
            , name = mapName
            , tileset = model.tileset
            , tilemaps = BoundedDict.get (tag mapId) model.tilemaps
            , mapSize = mapSize
            }

        showMap map =
            div [ class "rounded-lg bg-white p-2", Events.onClick <| events.openInnerMap map.id warpFrom ]
                [ Map.view map ]
    in
    List.map (namedMapToMapProps >> showMap) model.maps
