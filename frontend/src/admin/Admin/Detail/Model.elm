module Admin.Detail.Model exposing
    ( InnerMapMode(..)
    , InnerMapModel
    , PageModel
    , Pokemon
    , init
    , initInner
    , modifyInnerMapModel
    , setInnerMapModel
    )

import Admin.Detail.Grid as Grid
import Admin.Detail.Types.Dialog exposing (Dialog)
import Admin.Detail.Types.Selection as Selection exposing (Selection)
import Admin.Detail.Types.TileDict as TileDict exposing (TileDict)
import Admin.Detail.Types.TileStrategy as TileStrategy exposing (TileStrategy)
import Config
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber, initialLayerNumber)
import MapId exposing (MapId)
import PokemonNumber exposing (PokemonNumber)
import Proto.Kangaskhan.Type exposing (Type)
import Shared.Types.Generic exposing (Lift, Setter)


type alias PageModel =
    { gridMode : Grid.GridMode
    , dragState : Grid.DragState
    , offset : Coordinate Int
    , selection : Selection
    , currentTileDict : TileDict
    , innerMapModel : Maybe InnerMapModel
    , layer : LayerNumber
    , tileStrategy : TileStrategy
    , dialog : Maybe Dialog
    , tileSize : Int
    , tileSearchInput : String
    }


type InnerMapMode
    = InnerMapWaitingForDrag
    | InnerMapDraggingFrom (Coordinate Int)
    | SelectingWarpEnd


type alias InnerMapModel =
    { mapId : MapId
    , warpFrom : Coordinate Int
    , offset : Coordinate Int
    , selection : Selection
    , gridMode : Grid.GridMode
    , dragState : Grid.DragState
    , layer : LayerNumber
    , tileSize : Int
    }


type alias Pokemon =
    { pokemonNumber : PokemonNumber, name : String, primaryType : Type, secondaryType : Maybe Type }


init : PageModel
init =
    { gridMode = Grid.Drag
    , dragState = Grid.WaitingForDrag
    , offset = { x = 0, y = 0 }
    , selection = Selection.init
    , currentTileDict = TileDict.empty
    , innerMapModel = Nothing
    , layer = initialLayerNumber
    , tileStrategy = TileStrategy.AskServer
    , dialog = Nothing
    , tileSize = Config.tileSize
    , tileSearchInput = ""
    }


initInner : MapId -> Coordinate Int -> InnerMapModel
initInner mapId warpFrom =
    { mapId = mapId
    , warpFrom = warpFrom
    , offset = { x = 0, y = 0 }
    , selection = Selection.init
    , gridMode = Grid.Drag
    , dragState = Grid.WaitingForDrag
    , layer = 0
    , tileSize = Config.tileSize
    }


modifyInnerMapModel : Lift InnerMapModel PageModel
modifyInnerMapModel f model =
    { model | innerMapModel = Maybe.map f model.innerMapModel }


setInnerMapModel : InnerMapModel -> Setter PageModel
setInnerMapModel m model =
    { model | innerMapModel = Just m }
