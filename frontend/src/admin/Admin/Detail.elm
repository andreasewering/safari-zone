module Admin.Detail exposing (Context, Msg, PageModel, areaLabel, init, onPageLoad, tileAttributes, update, updateShared, view)

import Admin.AreaEditor.Route
import Admin.Detail.Dialog as Dialog
import Admin.Detail.Grid as Grid
import Admin.Detail.Model as Model
import Admin.Detail.Types.Area exposing (Area)
import Admin.Detail.Types.Borders as Borders
import Admin.Detail.Types.Dialog as Dialog
import Admin.Detail.Types.Selection as Selection exposing (Selection)
import Admin.Detail.Types.TileConfig as TileConfig exposing (TileConfig)
import Admin.Detail.Types.TileDict as TileDict exposing (TileId)
import Admin.Detail.Types.TileStrategy as TileStrategy exposing (TileStrategy)
import Admin.Html exposing (Attribute, Document, Html)
import Admin.LanguageDict as LanguageDict
import Admin.RequestManager as RequestManager
import Admin.Route
import Admin.Session as Session exposing (Session)
import Admin.Translations as Translations
import Admin.View.TileApp as TileApp
import Admin.View.Toolbar as Toolbar
import Atoms.Html
import BoundedDict
import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Html.WithContext exposing (div)
import Html.WithContext.Attributes exposing (class, href, style)
import List.Extra
import MapId exposing (MapId)
import Proto.Smeargle
import Set
import Shared.Types.Cuboid as Cuboid
import Tagged exposing (untag)


type alias PageModel =
    Model.PageModel


init : PageModel
init =
    Model.init


type Msg
    = ClickedTileType TileId
    | DeleteSelectedTiles
    | TouchedTile (Coordinate Int)
    | MovedTile (Coordinate Int)
    | ReleasedTile (Coordinate Int)
    | SwitchDragMode Grid.GridMode
    | ChangeLayer LayerNumber
    | SwitchTileStrategy TileStrategy
    | ChangedTileSearch String
    | SaveTiles
    | OpenAreaDialog
    | CloseDialog
    | SetDialog Dialog.Dialog
    | CreateArea Dialog.CreateAreaDialogProps
    | OpenWarpDialog (Coordinate Int)
    | ConfirmWarp { from : Coordinate Int, to : Coordinate Int, toMap : MapId, toLayer : LayerNumber }
    | OpenInnerMap MapId (Coordinate Int)
    | TouchedInnerMapTile (Coordinate Int)
    | MovedInnerMapTile (Coordinate Int)
    | ReleasedInnerMapTile (Coordinate Int)
    | MarkStartTile (Coordinate Int)
    | GoUpLayer
    | GoDownLayer
    | ZoomBy Float


type alias Context msg =
    { mapId : MapId, lift : Msg -> msg, liftShared : Session.Msg -> msg }


type alias Model m =
    Session { m | detailModel : PageModel }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        -- Maybe it would make more sense to only reset if the map is the last opened map?
        pageModel =
            Model.init

        currentTileDict =
            model.savedTiles
                |> BoundedDict.get ctx.mapId
                |> Maybe.withDefault TileDict.empty

        { offset, tileSize } =
            TileDict.centerAndScaleMap model currentTileDict

        newDetailModel : PageModel
        newDetailModel =
            { pageModel
                | currentTileDict = currentTileDict
                , offset = offset
                , tileSize = tileSize
            }

        ( requestManager, requestManagerCmds ) =
            RequestManager.register (requests ctx.mapId) model.requestManager
    in
    ( { model | requestManager = requestManager, detailModel = newDetailModel }
    , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
    )


requests : MapId -> List RequestManager.ApiCall
requests mapId =
    [ RequestManager.GetTileDetails { mapId = untag mapId }
    , RequestManager.GetTileConfig {}
    , RequestManager.GetAreas { mapId = untag mapId }
    , RequestManager.GetStartTiles { mapId = untag mapId }
    , RequestManager.GetTilesetConfig {}
    , RequestManager.GetAudioTracks {}
    ]


updateShared : Context msg -> Session.Msg -> Model m -> ( Model m, Cmd msg )
updateShared ctx msg ({ detailModel } as model) =
    case msg of
        Session.GotTiles mapId tiles ->
            if mapId == ctx.mapId then
                let
                    currentTileDict =
                        TileDict.fromProto tiles.tileDetails

                    { offset, tileSize } =
                        TileDict.centerAndScaleMap model currentTileDict
                in
                ( { model
                    | detailModel =
                        { detailModel
                            | currentTileDict = currentTileDict
                            , offset = offset
                            , tileSize = tileSize
                        }
                  }
                , Cmd.none
                )

            else
                ( model, Cmd.none )

        Session.GotSaveTileResponse mapId _ ->
            ( { model
                | savedTiles =
                    model.savedTiles
                        |> BoundedDict.insert mapId detailModel.currentTileDict
              }
            , Cmd.none
            )

        Session.GotTilesChosenByStrategy response ->
            ( { model
                | detailModel =
                    { detailModel
                        | currentTileDict =
                            TileDict.addTiles
                                (chosenTilesToTiles model.tileConfig response.chosenTiles)
                                detailModel.currentTileDict
                    }
              }
            , Cmd.none
            )

        Session.GotEditAreaResponse _ _ ->
            ( { model
                | detailModel =
                    { detailModel
                        | dialog = Nothing
                        , selection = Selection.init
                    }
              }
            , Cmd.none
            )

        Session.GotCreateAreaResponse _ _ ->
            ( { model
                | detailModel =
                    { detailModel
                        | dialog = Nothing
                        , selection = Selection.init
                    }
              }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ detailModel } as model) =
    case msg of
        ClickedTileType tileType ->
            selectTileId ctx tileType model

        DeleteSelectedTiles ->
            ( { model | detailModel = deleteSelectedTiles detailModel }, Cmd.none )

        TouchedTile coord ->
            ( { model | detailModel = Grid.touch detailModel.layer coord detailModel }, Cmd.none )

        TouchedInnerMapTile coord ->
            ( { model | detailModel = { detailModel | innerMapModel = Maybe.map (Grid.touch detailModel.layer coord) detailModel.innerMapModel } }, Cmd.none )

        MovedTile coord ->
            ( { model | detailModel = Grid.drag detailModel.layer coord detailModel }, Cmd.none )

        MovedInnerMapTile coord ->
            ( { model | detailModel = { detailModel | innerMapModel = Maybe.map (Grid.drag detailModel.layer coord) detailModel.innerMapModel } }, Cmd.none )

        ReleasedTile _ ->
            ( { model | detailModel = Grid.release detailModel }, Cmd.none )

        ReleasedInnerMapTile _ ->
            ( { model | detailModel = { detailModel | innerMapModel = Maybe.map Grid.release detailModel.innerMapModel } }, Cmd.none )

        SwitchDragMode mode ->
            ( { model
                | detailModel =
                    case detailModel.innerMapModel of
                        Just innerMapModel ->
                            Model.setInnerMapModel { innerMapModel | gridMode = mode } detailModel

                        Nothing ->
                            { detailModel | gridMode = mode, selection = Selection.init }
              }
            , Cmd.none
            )

        ChangeLayer newLayerNumber ->
            ( { model | detailModel = { detailModel | layer = newLayerNumber } }, Cmd.none )

        SwitchTileStrategy newStrategy ->
            ( { model | detailModel = { detailModel | tileStrategy = newStrategy } }, Cmd.none )

        SaveTiles ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.EditMap
                            { mapId = untag ctx.mapId, chosenTiles = TileDict.toProto detailModel.currentTileDict }
                        ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        CloseDialog ->
            ( { model | detailModel = { detailModel | dialog = Nothing } }, Cmd.none )

        OpenAreaDialog ->
            ( { model
                | detailModel =
                    { detailModel
                        | dialog =
                            Just <|
                                Dialog.CreateArea
                                    { name = "" }
                    }
              }
            , Cmd.none
            )

        SetDialog newDialog ->
            ( { model | detailModel = { detailModel | dialog = Just newDialog } }, Cmd.none )

        CreateArea dialog ->
            let
                cuboids =
                    Selection.getSelected detailModel.selection
                        |> Cuboid.toCuboids

                areaParts =
                    List.map Cuboid.toProto cuboids

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.CreateArea
                            { mapId = untag ctx.mapId
                            , name = Just <| LanguageDict.constant dialog.name
                            , areaParts = areaParts
                            }
                        ]
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        OpenWarpDialog warpFrom ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.GetMaps {} ] model.requestManager
            in
            ( { model
                | requestManager = requestManager
                , detailModel =
                    { detailModel | dialog = Just (Dialog.ChooseMapForWarp warpFrom) }
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        ConfirmWarp { from, to, toMap, toLayer } ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.AddWarp
                            { fromLayerNumber = detailModel.layer
                            , fromMapId = untag ctx.mapId
                            , fromX = from.x
                            , fromY = from.y
                            , toLayerNumber = toLayer
                            , toMapId = untag toMap
                            , toX = to.x
                            , toY = to.y
                            }
                        ]
                        model.requestManager
            in
            ( { model
                | requestManager = requestManager
                , detailModel = { detailModel | innerMapModel = Nothing }
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        OpenInnerMap mapId warpFrom ->
            let
                innerMapModel =
                    Model.initInner mapId warpFrom

                { offset, tileSize } =
                    BoundedDict.get mapId model.savedTiles
                        |> Maybe.withDefault TileDict.empty
                        |> TileDict.centerAndScaleMap model

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.GetTileDetails { mapId = untag mapId } ]
                        model.requestManager
            in
            ( { model
                | detailModel =
                    { detailModel
                        | innerMapModel = Just { innerMapModel | tileSize = tileSize, offset = offset }
                    }
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        MarkStartTile startTile ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.AddStartTile
                            { mapId = untag ctx.mapId
                            , x = startTile.x
                            , y = startTile.y
                            , layerNumber = detailModel.layer
                            }
                        ]
                        model.requestManager
            in
            ( { model
                | detailModel = { detailModel | selection = Selection.init }
                , requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        GoUpLayer ->
            ( { model | detailModel = { detailModel | layer = detailModel.layer + 1 } }, Cmd.none )

        GoDownLayer ->
            ( { model | detailModel = { detailModel | layer = detailModel.layer - 1 } }, Cmd.none )

        ZoomBy multiplier ->
            let
                tileSize =
                    round <| multiplier * toFloat detailModel.tileSize

                adjustment =
                    if multiplier > 1 then
                        detailModel.tileSize * 4

                    else
                        -4 * tileSize

                -- Just changing the tile size without changing the offset will essentially zoom in the top-left corner
                -- which is probably not what the user expects.
                -- So we instead adjust the offset to get a zoom into the middle of the screen instead.
                -- This logic is flawed since it contains a hardcoded 4 but works pretty well in practice.
                smartOffset =
                    { x = detailModel.offset.x - adjustment
                    , y = detailModel.offset.y - round (toFloat adjustment * (toFloat model.height / toFloat model.width))
                    }
            in
            ( { model | detailModel = { detailModel | tileSize = tileSize, offset = smartOffset } }, Cmd.none )

        ChangedTileSearch newTileSearch ->
            ( { model | detailModel = { detailModel | tileSearchInput = newTileSearch } }, Cmd.none )


view : Context msg -> Model m -> Document msg
view ctx ({ detailModel } as model) =
    let
        areaLabels =
            model.areas
                |> BoundedDict.get ctx.mapId
                |> Maybe.withDefault []
                |> List.map
                    (areaLabel ctx
                        { layerNumber = detailModel.layer
                        , moveLeft = detailModel.offset.x
                        , moveUp = detailModel.offset.y
                        , tileSize = detailModel.tileSize
                        }
                    )

        dialogEvents =
            { noOp = Session.NoOp |> ctx.liftShared
            , onClose = CloseDialog |> ctx.lift
            , createArea = CreateArea >> ctx.lift
            , openInnerMap = \mapId -> OpenInnerMap mapId >> ctx.lift
            , setDialog = SetDialog >> ctx.lift
            }

        configureToolbar =
            case detailModel.innerMapModel of
                Just innerMapModel ->
                    Toolbar.addGridTools
                        { currentDragMode = innerMapModel.gridMode
                        , switchDragMode = SwitchDragMode >> ctx.lift
                        }
                        >> Toolbar.addLayerTools
                            { goUpLayer = GoUpLayer |> ctx.lift
                            , goDownLayer = GoDownLayer |> ctx.lift
                            }
                        >> Toolbar.addZoom { zoomBy = ZoomBy >> ctx.lift }
                        >> Toolbar.maybe (Selection.checkSingle innerMapModel.selection)
                            (\coord ->
                                Toolbar.addWarpConfirmation
                                    { confirmWarp =
                                        ConfirmWarp
                                            { from = innerMapModel.warpFrom
                                            , to = coord
                                            , toMap = innerMapModel.mapId
                                            , toLayer = innerMapModel.layer
                                            }
                                            |> ctx.lift
                                    }
                            )

                Nothing ->
                    Toolbar.addGridTools
                        { currentDragMode = detailModel.gridMode
                        , switchDragMode = SwitchDragMode >> ctx.lift
                        }
                        >> Toolbar.addLayerTools
                            { goUpLayer = GoUpLayer |> ctx.lift
                            , goDownLayer = GoDownLayer |> ctx.lift
                            }
                        >> Toolbar.addZoom { zoomBy = ZoomBy >> ctx.lift }
                        >> Toolbar.addSaveButton { save = SaveTiles |> ctx.lift }
                        >> Toolbar.unless (Selection.isEmpty detailModel.selection)
                            (Toolbar.addEraserButton
                                { delete = DeleteSelectedTiles |> ctx.lift
                                }
                            )
                        >> Toolbar.addManualTileSelectionButton
                            { current = detailModel.tileStrategy
                            , switchTileStrategy = SwitchTileStrategy >> ctx.lift
                            }
                        >> Toolbar.unless (Selection.isEmpty detailModel.selection)
                            (Toolbar.addAreaCreation
                                { dialog = detailModel.dialog
                                , openAreaDialog = OpenAreaDialog |> ctx.lift
                                }
                            )
                        >> Toolbar.maybe (Selection.checkSingle detailModel.selection)
                            (\startTile ->
                                Toolbar.addStartTileCreation
                                    { markStartTile = MarkStartTile startTile |> ctx.lift
                                    }
                            )
                        >> Toolbar.maybe model.tileset
                            (\tileset ->
                                Toolbar.addTileSelector
                                    { tileConfig = model.tileConfig
                                    , tileSearchInput = detailModel.tileSearchInput
                                    , tileset = tileset
                                    , changeTileSearch = ChangedTileSearch >> ctx.lift
                                    , clickedTileType = ClickedTileType >> ctx.lift
                                    }
                            )
    in
    { title = Translations.mapCreatorDetailTitle
    , body =
        [ div [ class "flex h-full w-full flex-col" ]
            []
        , Dialog.view dialogEvents model
        , Toolbar.builder |> configureToolbar |> Toolbar.build
        , Html.WithContext.map ctx.lift <|
            case detailModel.innerMapModel of
                Just innerMapModel ->
                    Grid.view { onDown = TouchedInnerMapTile, onMove = MovedInnerMapTile, onUp = ReleasedInnerMapTile }
                        (tileAttributes
                            { areas = model.areas |> BoundedDict.get innerMapModel.mapId |> Maybe.withDefault []
                            , startTiles = model.startTiles |> BoundedDict.get innerMapModel.mapId |> Maybe.withDefault Dict.empty
                            , layer = innerMapModel.layer
                            , selection = innerMapModel.selection
                            }
                        )
                        model
                        innerMapModel

                Nothing ->
                    Grid.view { onDown = TouchedTile, onMove = MovedTile, onUp = ReleasedTile }
                        (tileAttributes
                            { areas = model.areas |> BoundedDict.get ctx.mapId |> Maybe.withDefault []
                            , layer = detailModel.layer
                            , selection = detailModel.selection
                            , startTiles = model.startTiles |> BoundedDict.get ctx.mapId |> Maybe.withDefault Dict.empty
                            }
                        )
                        model
                        detailModel
        ]
            ++ areaLabels
            ++ (case ( detailModel.innerMapModel, model.tileset ) of
                    ( Just innerMapModel, Just tileset ) ->
                        TileApp.view
                            { tileDict = model.savedTiles |> BoundedDict.get innerMapModel.mapId |> Maybe.withDefault Dict.empty
                            , offset = innerMapModel.offset
                            , viewPort = model
                            , tileset = tileset
                            , layerNumberInFocus = innerMapModel.layer
                            , tileSize = innerMapModel.tileSize
                            }

                    ( Nothing, Just tileset ) ->
                        TileApp.view
                            { tileDict = detailModel.currentTileDict
                            , offset = detailModel.offset
                            , viewPort = model
                            , tileset = tileset
                            , layerNumberInFocus = detailModel.layer
                            , tileSize = detailModel.tileSize
                            }

                    _ ->
                        []
               )
    }


selectTileId : Context msg -> TileDict.TileId -> Model m -> ( Model m, Cmd msg )
selectTileId ctx tileId ({ detailModel } as model) =
    let
        selected =
            Selection.getSelected detailModel.selection
                |> Set.toList
    in
    case detailModel.tileStrategy of
        TileStrategy.Manual ->
            let
                addTiles =
                    TileDict.addTiles (selectManualTileId model.tileConfig tileId selected)
            in
            ( { model
                | detailModel =
                    { detailModel
                        | currentTileDict =
                            addTiles detailModel.currentTileDict
                    }
              }
            , Cmd.none
            )

        TileStrategy.AskServer ->
            let
                groupedByLayer : List ( LayerNumber, List { x : Int, y : Int } )
                groupedByLayer =
                    List.Extra.groupWhile (\( layerNumber1, _, _ ) ( layerNumber2, _, _ ) -> layerNumber1 == layerNumber2) selected
                        |> List.map
                            (\( first, rest ) ->
                                let
                                    ( layerNumber, _, _ ) =
                                        first

                                    coords =
                                        (first :: rest) |> List.map (\( _, x, y ) -> { x = x, y = y })
                                in
                                ( layerNumber, coords )
                            )

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        (List.map
                            (\( layerNumber, positions ) ->
                                RequestManager.FindFittingTiles
                                    { tileId = tileId, layerNumber = layerNumber, positions = positions }
                            )
                            groupedByLayer
                        )
                        model.requestManager
            in
            ( { model | requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )


selectManualTileId : TileConfig -> TileDict.TileId -> List ( LayerNumber, Int, Int ) -> List TileDict.Tile
selectManualTileId cfg tileId coords =
    let
        toTile layer ( layerNumber, x, y ) =
            { tileId = tileId, coord = { x = x, y = y }, layerNumber = layerNumber, layer = layerNumber * 100 + layer }
    in
    case TileConfig.tileIdToLayer cfg tileId of
        Just layer ->
            List.map (toTile layer) coords

        Nothing ->
            []


chosenTilesToTiles : TileConfig -> List Proto.Smeargle.ChosenTile -> List TileDict.Tile
chosenTilesToTiles cfg =
    List.filterMap
        (\tile ->
            Maybe.map
                (\layer ->
                    { tileId = tile.tileId
                    , layer = layer
                    , layerNumber = tile.layerNumber
                    , coord = { x = tile.x, y = tile.y }
                    }
                )
                (TileConfig.tileIdToLayer cfg tile.tileId)
        )


deleteSelectedTiles : PageModel -> PageModel
deleteSelectedTiles model =
    let
        tilesToRemove =
            Selection.getSelected model.selection
                |> Set.toList
                |> List.map
                    (\( layerNumber, x, y ) ->
                        { layerNumber = layerNumber, coord = { x = x, y = y } }
                    )
    in
    { model | currentTileDict = TileDict.removeTiles tilesToRemove model.currentTileDict }


tileAttributes :
    { model
        | areas : List Area
        , layer : LayerNumber
        , startTiles : Dict LayerNumber (List (Coordinate Int))
        , selection : Selection
    }
    -> Coordinate Int
    -> List (Attribute msg)
tileAttributes model coord =
    let
        areaBorder =
            List.map
                (\area ->
                    Selection.isOnEdge model.layer coord area.selection
                        |> Borders.map
                            (\onEdge ->
                                if onEdge then
                                    5

                                else
                                    0
                            )
                )
                model.areas
                |> Borders.fold

        isStartTile =
            Dict.get model.layer model.startTiles
                |> Maybe.withDefault []
                |> List.any ((==) coord)

        borderColor =
            if Selection.isSelected model.layer coord model.selection then
                class "border-green-600"

            else if isStartTile then
                class "border-amber-600"

            else
                class "border-gray-400/70"

        borderWidth =
            if Selection.isSelected model.layer coord model.selection then
                Borders.each 4

            else
                Borders.fold
                    [ areaBorder
                    , startTileBorder
                    , Borders.each 1
                    ]

        startTileBorder =
            if isStartTile then
                Borders.each 5

            else
                Borders.each 0
    in
    [ class "border-solid", Borders.toAttribute borderWidth, borderColor ]


areaLabel : { ctx | mapId : MapId } -> { layerNumber : LayerNumber, moveLeft : Int, moveUp : Int, tileSize : Int } -> Area -> Html msg
areaLabel ctx { layerNumber, moveLeft, moveUp, tileSize } area =
    case Selection.topLeft layerNumber area.selection of
        Nothing ->
            Atoms.Html.none

        Just { x, y } ->
            Atoms.Html.a
                [ href
                    (Admin.AreaEditor.Route.Layout
                        |> Admin.Route.EditArea ctx.mapId area.id
                        |> Admin.Route.fromRoute
                    )
                , class "absolute z-10 cursor-default bg-white p-1"
                , style "left" <| String.fromInt (x * tileSize + moveLeft) ++ "px"
                , style "top" <| String.fromInt (y * tileSize + moveUp) ++ "px"
                ]
                [ Html.WithContext.text area.name
                ]
