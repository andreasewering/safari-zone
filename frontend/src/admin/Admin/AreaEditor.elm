module Admin.AreaEditor exposing (..)

import Admin.AreaEditor.Model as Model exposing (PageModel)
import Admin.AreaEditor.Route as Route exposing (Route)
import Admin.Detail as Detail
import Admin.Detail.Grid as Grid
import Admin.Detail.Model exposing (Pokemon)
import Admin.Detail.Types.Area exposing (Area)
import Admin.Detail.Types.Encounters as Encounters exposing (Encounters)
import Admin.Detail.Types.ItemProbabilities as ItemProbabilities exposing (ItemProbabilities)
import Admin.Detail.Types.Selection as Selection
import Admin.Detail.Types.TileDict as TileDict exposing (TileDict)
import Admin.Html exposing (Document, Html)
import Admin.Interop.Audio
import Admin.Language as Language
import Admin.LanguageDict as LanguageDict exposing (LanguageDict)
import Admin.RequestManager as RequestManager
import Admin.Route
import Admin.Session as Session exposing (Session)
import Admin.Translations as Translations
import Admin.View.DragAndDrop as DragAndDrop
import Admin.View.Tabs as Tabs
import Admin.View.TileApp as TileApp
import Admin.View.Toolbar as Toolbar
import AreaId exposing (AreaId)
import Atoms.Html
import Atoms.Input
import Atoms.Select
import AudioTrackNumber exposing (AudioTrackNumber)
import BoundedDict
import Config
import Dict exposing (Dict)
import Domain.Coordinate exposing (Coordinate)
import Domain.Layer exposing (LayerNumber)
import Html.WithContext exposing (div, main_, withContext)
import Html.WithContext.Attributes exposing (class, style)
import Html.WithContext.Events as Events
import Interop.Audio.Out
import Item exposing (Item)
import ItemDict
import List.NonEmpty as NonEmpty exposing (NonEmpty)
import MapId exposing (MapId)
import Maybe.Extra
import PokemonNumber exposing (PokemonNumber)
import PokemonNumberDict exposing (PokemonNumberDict)
import Proto.Arceus.SpawnKind as SpawnKind exposing (SpawnKind)
import Proto.Kangaskhan.Type as Type exposing (Type)
import Shared.Types.Audio as Audio exposing (AudioConfig)
import Shared.Types.Cuboid as Cuboid
import Shared.View.Icon exposing (iconHtmlWith)
import Shared.View.PokemonImage as PokemonImage
import Simple.Fuzzy
import Tagged exposing (untag)
import Textures.Store exposing (Sprite)


type Msg
    = SetAreaName Translations.Language String
    | AutoTranslateAreaName { source : Translations.Language, target : List Translations.Language }
    | SetBackgroundMusicTrack AudioTrackNumber
    | PlayMusicTrack AudioTrackNumber
    | TouchedTile (Coordinate Int)
    | MovedTile (Coordinate Int)
    | ReleasedTile (Coordinate Int)
    | SwitchedGridMode Grid.GridMode
    | ZoomBy Float
    | ChangeLayer Int
    | SaveAreaPartChanges
    | EncounterDragAndDrop (DragAndDrop.Msg Model.EncounterDragId Model.EncounterDropId)
    | ChangeEncounterProbability PokemonNumber SpawnKind Float
    | ChangeEncounterSearchInput String
    | ChangeEncounterSearchType (Maybe Type)
    | SaveEncounterChanges
    | ItemDragAndDrop (DragAndDrop.Msg Model.ItemDragId Model.ItemDropId)
    | SaveItemChanges
    | ChangeItemProbability Item Float


type alias Context msg =
    { areaId : AreaId
    , mapId : MapId
    , route : Route
    , lift : Msg -> msg
    , liftShared : Session.Msg -> msg
    }


type alias Model m =
    Session { m | areaEditorModel : PageModel }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx ({ areaEditorModel } as model) =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register
                (requests ctx)
                model.requestManager

        { offset, tileSize } =
            case BoundedDict.get ctx.mapId model.savedTiles of
                Just tileDict ->
                    TileDict.centerAndScaleMap model tileDict

                Nothing ->
                    { offset = { x = 0, y = 0 }, tileSize = Config.tileSize }

        newAreaEditorModel =
            { areaEditorModel | offset = offset, tileSize = tileSize }
    in
    ( { model | requestManager = requestManager, areaEditorModel = newAreaEditorModel }
        |> Model.setAreaDetail ctx.areaId
    , requestManagerCmds
        |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
    )


requests : Context msg -> List RequestManager.ApiCall
requests ctx =
    [ RequestManager.GetAreaDetail { areaId = untag ctx.areaId }
    , RequestManager.GetAudioTracks {}
    , RequestManager.GetTileDetails { mapId = untag ctx.mapId }
    , RequestManager.GetTileConfig {}
    , RequestManager.GetAreas { mapId = untag ctx.mapId }
    , RequestManager.GetStartTiles { mapId = untag ctx.mapId }
    , RequestManager.GetTilesetConfig {}
    , RequestManager.GetAllPokemon {}
    ]


updateShared : Context msg -> Session.Msg -> Model m -> ( Model m, Cmd msg )
updateShared ctx msg ({ areaEditorModel } as model) =
    case msg of
        Session.GotAreaDetail areaId _ ->
            if areaId == ctx.areaId then
                ( Model.setAreaDetail areaId model
                , Cmd.none
                )

            else
                ( model, Cmd.none )

        Session.GotTiles mapId response ->
            if mapId /= ctx.mapId then
                ( model, Cmd.none )

            else
                let
                    tileDict =
                        TileDict.fromProto response.tileDetails

                    { offset, tileSize } =
                        TileDict.centerAndScaleMap model tileDict

                    newAreaEditorModel =
                        { areaEditorModel | offset = offset, tileSize = tileSize }
                in
                ( { model | areaEditorModel = newAreaEditorModel }, Cmd.none )

        Session.GotTranslation req res ->
            case
                areaEditorModel.requestedTranslation
                    |> Maybe.Extra.filter
                        (\( lang, text ) ->
                            req.sourceLanguage == Language.toProto lang && req.text == text
                        )
            of
                Just _ ->
                    let
                        newName =
                            res.translations
                                |> List.filterMap (\translation -> Language.fromProto translation.language |> Maybe.map (Tuple.pair translation.text))
                                |> List.foldl (\( text, language ) -> LanguageDict.insert language text) areaEditorModel.name
                    in
                    ( { model | areaEditorModel = { areaEditorModel | name = newName, requestedTranslation = Nothing } }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ areaEditorModel } as model) =
    case msg of
        SetAreaName language name ->
            let
                newName =
                    LanguageDict.insert language name areaEditorModel.name

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.EditArea
                            { areaId = untag ctx.areaId
                            , name = Just newName
                            , areaParts = Nothing
                            , backgroundMusicTrack = Nothing
                            , encounters = Nothing
                            , items = Nothing
                            }
                        ]
                        model.requestManager
            in
            ( { model | areaEditorModel = { areaEditorModel | name = newName }, requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        AutoTranslateAreaName { source, target } ->
            let
                text =
                    areaEditorModel.name |> LanguageDict.get source

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.Translate
                            { sourceLanguage = Language.toProto source
                            , targetLanguages = target |> List.map Language.toProto
                            , text = text
                            }
                        ]
                        model.requestManager
            in
            ( { model
                | requestManager = requestManager
                , areaEditorModel = { areaEditorModel | requestedTranslation = Just ( source, text ) }
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        SetBackgroundMusicTrack backgroundMusicTrack ->
            let
                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.EditArea
                            { areaId = untag ctx.areaId
                            , name = Nothing
                            , areaParts = Nothing
                            , backgroundMusicTrack = Just <| untag backgroundMusicTrack
                            , encounters = Nothing
                            , items = Nothing
                            }
                        ]
                        model.requestManager
            in
            ( { model | areaEditorModel = { areaEditorModel | audioTrackNumber = Just backgroundMusicTrack }, requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        PlayMusicTrack audioTrackNumber ->
            let
                -- TODO extract this somewhere, this is also used in Safari.Audio
                audioTrackToLoop : AudioTrackNumber -> Maybe Interop.Audio.Out.AudioLoop
                audioTrackToLoop number =
                    model.audioConfig
                        |> Audio.getLoop number
                        |> Maybe.map (\loop -> { filename = loop.filename, loopStart = loop.start, loopEnd = loop.end })
            in
            ( model, audioTrackToLoop audioTrackNumber |> Maybe.map Admin.Interop.Audio.loop |> Maybe.withDefault Cmd.none )

        TouchedTile coord ->
            ( { model | areaEditorModel = Grid.touch areaEditorModel.layerNumber coord areaEditorModel }, Cmd.none )

        MovedTile coord ->
            ( { model | areaEditorModel = Grid.drag areaEditorModel.layerNumber coord areaEditorModel }, Cmd.none )

        ReleasedTile _ ->
            ( { model | areaEditorModel = Grid.release areaEditorModel }, Cmd.none )

        SwitchedGridMode gridMode ->
            ( { model | areaEditorModel = { areaEditorModel | gridMode = gridMode } }, Cmd.none )

        ZoomBy zoom ->
            let
                tileSize =
                    round <| zoom * toFloat areaEditorModel.tileSize

                adjustment =
                    if zoom > 1 then
                        areaEditorModel.tileSize * 4

                    else
                        -4 * tileSize

                -- Just changing the tile size without changing the offset will essentially zoom in the top-left corner
                -- which is probably not what the user expects.
                -- So we instead adjust the offset to get a zoom into the middle of the screen instead.
                -- This logic is flawed since it contains a hardcoded 4 but works pretty well in practice.
                smartOffset =
                    { x = areaEditorModel.offset.x - adjustment
                    , y = areaEditorModel.offset.y - round (toFloat adjustment * (toFloat model.height / toFloat model.width))
                    }
            in
            ( { model
                | areaEditorModel =
                    { areaEditorModel
                        | tileSize = tileSize
                        , offset = smartOffset
                    }
              }
            , Cmd.none
            )

        EncounterDragAndDrop dragAndDropMsg ->
            let
                ( encounterDragState, drop ) =
                    DragAndDrop.update dragAndDropMsg areaEditorModel.encounterDragState
            in
            case drop of
                Just ( ( pokemonNumber, Just previousSpawnKind ), Just spawnKind, _ ) ->
                    ( { model
                        | areaEditorModel =
                            { areaEditorModel
                                | encounterDragState = encounterDragState
                                , encounterProbabilities =
                                    Encounters.upsertEncounter spawnKind pokemonNumber 1 areaEditorModel.encounterProbabilities
                                        |> Encounters.remove previousSpawnKind pokemonNumber
                            }
                      }
                    , Cmd.none
                    )

                Just ( ( pokemonNumber, Just previousSpawnKind ), Nothing, _ ) ->
                    ( { model
                        | areaEditorModel =
                            { areaEditorModel
                                | encounterDragState = encounterDragState
                                , encounterProbabilities = Encounters.remove previousSpawnKind pokemonNumber areaEditorModel.encounterProbabilities
                            }
                      }
                    , Cmd.none
                    )

                Just ( ( pokemonNumber, Nothing ), Just spawnKind, _ ) ->
                    ( { model
                        | areaEditorModel =
                            { areaEditorModel
                                | encounterDragState = encounterDragState
                                , encounterProbabilities = Encounters.upsertEncounter spawnKind pokemonNumber 1 areaEditorModel.encounterProbabilities
                            }
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | areaEditorModel = { areaEditorModel | encounterDragState = encounterDragState } }
                    , Cmd.none
                    )

        ChangeEncounterProbability pokemonNumber spawnKind probability ->
            ( { model
                | areaEditorModel =
                    { areaEditorModel
                        | encounterProbabilities = Encounters.upsertEncounter spawnKind pokemonNumber probability areaEditorModel.encounterProbabilities
                    }
              }
            , Cmd.none
            )

        ChangeEncounterSearchInput newValue ->
            ( { model
                | areaEditorModel =
                    { areaEditorModel
                        | encounterSearchInput = newValue
                    }
              }
            , Cmd.none
            )

        ChangeEncounterSearchType newValue ->
            ( { model
                | areaEditorModel =
                    { areaEditorModel
                        | encounterSearchType = newValue
                    }
              }
            , Cmd.none
            )

        ChangeLayer diff ->
            ( { model
                | areaEditorModel =
                    { areaEditorModel
                        | layerNumber = areaEditorModel.layerNumber + diff
                    }
              }
            , Cmd.none
            )

        SaveAreaPartChanges ->
            let
                newAreaParts =
                    areaEditorModel.selection
                        |> Selection.getSelected
                        |> Cuboid.toCuboids
                        |> List.map Cuboid.toProto

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.EditArea
                            { areaId = untag ctx.areaId
                            , name = Nothing
                            , areaParts = Just { list = newAreaParts }
                            , encounters = Nothing
                            , items = Nothing
                            , backgroundMusicTrack = Nothing
                            }
                        ]
                        model.requestManager
            in
            ( { model
                | requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        ItemDragAndDrop dragAndDropMsg ->
            let
                ( itemDragState, drop ) =
                    DragAndDrop.update dragAndDropMsg areaEditorModel.itemDragState
            in
            case drop of
                Just ( ( item, Just () ), Nothing, _ ) ->
                    ( { model
                        | areaEditorModel =
                            { areaEditorModel
                                | itemDragState = itemDragState
                                , itemProbabilities = ItemProbabilities.remove item areaEditorModel.itemProbabilities
                            }
                      }
                    , Cmd.none
                    )

                Just ( ( item, Nothing ), Just (), _ ) ->
                    ( { model
                        | areaEditorModel =
                            { areaEditorModel
                                | itemDragState = itemDragState
                                , itemProbabilities = ItemProbabilities.upsertItem item 1 areaEditorModel.itemProbabilities
                            }
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | areaEditorModel = { areaEditorModel | itemDragState = itemDragState } }
                    , Cmd.none
                    )

        ChangeItemProbability item probability ->
            ( { model
                | areaEditorModel =
                    { areaEditorModel
                        | itemProbabilities = ItemProbabilities.upsertItem item probability areaEditorModel.itemProbabilities
                    }
              }
            , Cmd.none
            )

        SaveEncounterChanges ->
            let
                newEncounters =
                    areaEditorModel.encounterProbabilities |> Encounters.toProto

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.EditArea
                            { areaId = untag ctx.areaId
                            , name = Nothing
                            , areaParts = Nothing
                            , encounters = Just { list = newEncounters }
                            , items = Nothing
                            , backgroundMusicTrack = Nothing
                            }
                        ]
                        model.requestManager
            in
            ( { model
                | requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )

        SaveItemChanges ->
            let
                newItems =
                    areaEditorModel.itemProbabilities |> ItemProbabilities.toProto

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register
                        [ RequestManager.EditArea
                            { areaId = untag ctx.areaId
                            , name = Nothing
                            , areaParts = Nothing
                            , encounters = Nothing
                            , items = Just { list = newItems }
                            , backgroundMusicTrack = Nothing
                            }
                        ]
                        model.requestManager
            in
            ( { model
                | requestManager = requestManager
              }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )


view : Context msg -> Model m -> Document msg
view ctx model =
    let
        tabs =
            [ ( Route.Layout, Translations.areaEditorLayoutTitle )
            , ( Route.Metadata, Translations.areaEditorMetadataTitle )
            , ( Route.Encounters, Translations.areaEditorEncountersTitle )
            , ( Route.Items, Translations.areaEditorItemsTitle )
            ]
                |> List.map
                    (\( route, translation ) ->
                        { route = Admin.Route.EditArea ctx.mapId ctx.areaId route, label = translation }
                    )
    in
    { title =
        case ctx.route of
            Route.Layout ->
                Translations.areaEditorLayoutTitle

            Route.Metadata ->
                Translations.areaEditorMetadataTitle

            Route.Encounters ->
                Translations.areaEditorEncountersTitle

            Route.Items ->
                Translations.areaEditorItemsTitle
    , body =
        [ Tabs.view model.route tabs
        , main_ [ class "relative flex flex-col items-center justify-center gap-2" ]
            (case ( ctx.route, RequestManager.status (RequestManager.GetAreaDetail { areaId = untag ctx.areaId }) model.requestManager ) of
                ( Route.Layout, RequestManager.Success ) ->
                    Maybe.map2
                        (viewLayout ctx
                            model
                            model.areaEditorModel
                            (BoundedDict.get ctx.mapId model.areas
                                |> Maybe.withDefault []
                                |> List.filter (\area -> area.id /= ctx.areaId)
                            )
                            (BoundedDict.get ctx.mapId model.startTiles |> Maybe.withDefault Dict.empty)
                        )
                        model.tileset
                        (BoundedDict.get ctx.mapId model.savedTiles)
                        |> Maybe.withDefault []

                ( Route.Metadata, RequestManager.Success ) ->
                    viewMetadata ctx model.areaEditorModel model.audioConfig

                ( Route.Encounters, RequestManager.Success ) ->
                    viewEncounters ctx model.areaEditorModel model.pokemon model.areaEditorModel.encounterDragState

                ( Route.Items, RequestManager.Success ) ->
                    viewItems ctx model.areaEditorModel model.areaEditorModel.itemDragState

                _ ->
                    []
            )
        ]
    }


viewLayout :
    Context msg
    -> { v | width : Int, height : Int }
    -> Grid.GridModel { m | layerNumber : LayerNumber }
    -> List Area
    -> Dict LayerNumber (List (Coordinate Int))
    -> Sprite
    -> TileDict
    -> List (Html msg)
viewLayout ctx viewport gridModel areas startTiles tileset tileDict =
    let
        coordinateStyles =
            Detail.tileAttributes
                { areas = areas
                , layer = gridModel.layerNumber
                , startTiles = startTiles
                , selection = gridModel.selection
                }

        areaLabels =
            areas
                |> List.map
                    (Detail.areaLabel ctx
                        { layerNumber = gridModel.layerNumber
                        , moveLeft = gridModel.offset.x
                        , moveUp = gridModel.offset.y
                        , tileSize = gridModel.tileSize
                        }
                    )

        grid =
            Grid.view
                { onDown = TouchedTile >> ctx.lift
                , onMove = MovedTile >> ctx.lift
                , onUp = ReleasedTile >> ctx.lift
                }
                coordinateStyles
                viewport
                gridModel

        toolbar =
            Toolbar.builder
                |> Toolbar.addGridTools
                    { currentDragMode = gridModel.gridMode
                    , switchDragMode = SwitchedGridMode >> ctx.lift
                    }
                |> Toolbar.addLayerTools
                    { goUpLayer = ChangeLayer 1 |> ctx.lift
                    , goDownLayer = ChangeLayer -1 |> ctx.lift
                    }
                |> Toolbar.addZoom { zoomBy = ZoomBy >> ctx.lift }
                |> Toolbar.addSaveButton { save = SaveAreaPartChanges |> ctx.lift }
                |> Toolbar.build

        tileApp =
            TileApp.view
                { tileDict = tileDict
                , offset = gridModel.offset
                , viewPort = viewport
                , tileset = tileset
                , layerNumberInFocus = gridModel.layerNumber
                , tileSize = gridModel.tileSize
                }
    in
    grid
        :: toolbar
        :: tileApp
        ++ areaLabels


viewMetadata :
    Context msg
    -> { a | name : LanguageDict String, audioTrackNumber : Maybe AudioTrackNumber }
    -> AudioConfig
    -> List (Html msg)
viewMetadata ctx { name, audioTrackNumber } audioConfig =
    let
        backgroundMusicTrackSelect =
            audioConfig
                |> Audio.getLoopNames
                |> NonEmpty.fromList
                |> Maybe.map
                    (\options ->
                        Atoms.Select.view
                            { options = options
                            , onSelect = SetBackgroundMusicTrack >> ctx.lift
                            , selectedOption = audioTrackNumber
                            , id = "music_track"
                            , label = Translations.mapCreatorDetailAreaMusicTrackLabel
                            }
                    )
                |> Maybe.withDefault Atoms.Html.none

        nameInputForLang lang languageName =
            Atoms.Input.text
                { id = "name_" ++ Translations.languageToString lang
                , onChange = SetAreaName lang >> ctx.lift
                , label = Translations.areaEditorNameInputLabel languageName
                , text = LanguageDict.get lang name
                , error = Nothing
                }

        nameInput =
            withContext <|
                \{ i18n } ->
                    let
                        currentLang =
                            Translations.currentLanguage i18n

                        languageName lang =
                            translationForLanguage lang i18n
                    in
                    div []
                        [ nameInputForLang currentLang (languageName currentLang)
                        , Translations.languages
                            |> List.filter ((/=) currentLang)
                            |> List.map (\lang -> nameInputForLang lang (languageName lang))
                            |> div []
                        ]

        autoTranslateButton =
            withContext <|
                \{ i18n } ->
                    let
                        currentLang =
                            Translations.currentLanguage i18n
                    in
                    Atoms.Html.button
                        [ Events.onClick
                            (AutoTranslateAreaName
                                { source = currentLang
                                , target =
                                    Translations.languages
                                        |> List.filter ((/=) currentLang)
                                }
                                |> ctx.lift
                            )
                        ]
                        [ Html.WithContext.text "Autotranslate" ]
    in
    [ nameInput
    , backgroundMusicTrackSelect
    , autoTranslateButton

    -- TODO make this actually useful (start/stop/switch when drop down switches etc.)
    , case audioTrackNumber of
        Just audioTrack ->
            Atoms.Html.button [ Events.onClick (PlayMusicTrack audioTrack |> ctx.lift) ] [ Html.WithContext.text "Play" ]

        Nothing ->
            Atoms.Html.none
    ]


viewEncounters :
    Context msg
    -> { a | encounterProbabilities : Encounters, encounterSearchInput : String, encounterSearchType : Maybe Type }
    -> PokemonNumberDict Pokemon
    -> Model.EncounterDragState
    -> List (Html msg)
viewEncounters ctx { encounterProbabilities, encounterSearchInput, encounterSearchType } pokemons dragState =
    let
        isDragging =
            Maybe.Extra.isJust <| DragAndDrop.isDragging dragState

        spawnKindArea ( spawnKind, attributes, hoverAttributes ) =
            let
                attrs =
                    case DragAndDrop.isDragging dragState of
                        Just _ ->
                            hoverAttributes

                        _ ->
                            attributes
            in
            div
                (class "box-border flex min-h-10 w-full flex-wrap rounded-lg"
                    :: attrs
                    ++ DragAndDrop.droppable dragState (EncounterDragAndDrop >> ctx.lift) (Just spawnKind)
                )
                (Encounters.getBySpawnKind spawnKind encounterProbabilities
                    |> List.filterMap (\( pokemonNumber, probability ) -> PokemonNumberDict.get pokemonNumber pokemons |> Maybe.map (Tuple.pair probability))
                    |> List.map (showEncounter spawnKind)
                )
                |> Atoms.Html.disable (not isDragging)

        showEncounter spawnKind ( probability, pokemon ) =
            div [ class "flex flex-col gap-2" ]
                [ showSingle 80 (Just spawnKind) pokemon
                , Atoms.Input.slider
                    { value = probability
                    , id = Encounters.toInputId spawnKind pokemon.pokemonNumber
                    , onChange = ChangeEncounterProbability pokemon.pokemonNumber spawnKind >> ctx.lift
                    , label = Translations.areaEditorProbabilityLabel
                    , min = 1
                    , max = 100
                    , step = 1
                    }
                ]
                |> Atoms.Html.disable isDragging

        showSingle size spawnKind pokemon =
            PokemonImage.showImageHtml
                (style "width" (String.fromInt size ++ "px")
                    :: (case DragAndDrop.isDragging dragState of
                            Just dragId ->
                                if dragId == ( pokemon.pokemonNumber, spawnKind ) then
                                    class ""

                                else
                                    class "opacity-50"

                            Nothing ->
                                class ""
                       )
                    :: DragAndDrop.draggable dragState (EncounterDragAndDrop >> ctx.lift) ( pokemon.pokemonNumber, spawnKind )
                )
                pokemon
                PokemonImage.Normal
                |> Atoms.Html.disable isDragging

        pokemonListStyling =
            class "box-border flex w-full max-w-4xl flex-wrap rounded-lg border-4 border-solid p-2"
                :: (case DragAndDrop.isDragging dragState of
                        Just _ ->
                            class "border-gray-200 bg-gray-100"

                        _ ->
                            class "border-transparent bg-white"
                   )
                :: DragAndDrop.droppable dragState (EncounterDragAndDrop >> ctx.lift) Nothing

        pokemonFilter : Pokemon -> Bool
        pokemonFilter pokemon =
            let
                trimmedInput =
                    String.trim encounterSearchInput
            in
            (if trimmedInput /= "" then
                Simple.Fuzzy.match trimmedInput pokemon.name

             else
                True
            )
                && (case encounterSearchType of
                        Nothing ->
                            True

                        Just type_ ->
                            pokemon.primaryType == type_ || pokemon.secondaryType == Just type_
                   )
    in
    [ div [ class "flex w-full gap-2 px-4 pt-2" ]
        (List.map spawnKindArea
            [ ( SpawnKind.Grass, [ class "border-4 border-green-300 bg-green-300" ], [ class "border-4 border-solid border-green-500 bg-green-400" ] )
            , ( SpawnKind.Water, [ class "border-4 border-sky-300 bg-sky-300" ], [ class "border-4 border-solid border-sky-500 bg-sky-400" ] )
            ]
        )
    , div [ class "flex gap-4" ]
        [ Atoms.Input.text
            { onChange = ChangeEncounterSearchInput >> ctx.lift
            , text = encounterSearchInput
            , id = "name_or_number"
            , label =
                Translations.areaEditorEncountersSearchLabel
            , error = Nothing
            }
        , Html.WithContext.withContext <|
            \{ i18n } ->
                Atoms.Select.view
                    { onSelect = ChangeEncounterSearchType >> ctx.lift
                    , options = typeSelectOptions i18n
                    , selectedOption = Just encounterSearchType
                    , id = "type"
                    , label = Translations.areaEditorEncountersTypeLabel
                    }
        , iconHtmlWith { name = "save", width = 3, height = 3, description = Translations.mapCreatorDetailSaveDescription }
            [ Events.onClick (ctx.lift SaveEncounterChanges), class "flex items-center justify-center p-2" ]
        ]
    , PokemonNumberDict.toList pokemons
        |> List.filter (Tuple.second >> pokemonFilter)
        |> List.sortBy (Tuple.second >> .pokemonNumber >> untag)
        |> List.map Tuple.second
        |> List.map (showSingle 50 Nothing)
        |> div pokemonListStyling
        |> Atoms.Html.disable (not isDragging)
    ]
        |> List.map (Atoms.Html.disable isDragging)


viewItems :
    Context msg
    -> { a | itemProbabilities : ItemProbabilities }
    -> Model.ItemDragState
    -> List (Html msg)
viewItems ctx { itemProbabilities } dragState =
    let
        isDragging =
            Maybe.Extra.isJust <| DragAndDrop.isDragging dragState

        selectedItemsArea =
            let
                attrs =
                    case DragAndDrop.isDragging dragState of
                        Just _ ->
                            [ class "border-solid border-green-500 bg-green-400" ]

                        _ ->
                            [ class "border-green-300 bg-green-300" ]
            in
            div
                (class "box-border flex min-h-10 w-full flex-wrap rounded-lg border-4"
                    :: attrs
                    ++ DragAndDrop.droppable dragState (ItemDragAndDrop >> ctx.lift) (Just ())
                )
                (itemProbabilities.probabilities
                    |> ItemDict.toList
                    |> List.map showItem
                )
                |> Atoms.Html.disable (not isDragging)

        showItem ( item, probability ) =
            div [ class "flex flex-col items-center gap-2" ]
                [ showSingle (Just ()) item
                , Atoms.Input.slider
                    { value = probability
                    , id = "item" ++ (Item.toComparable item |> String.fromInt)
                    , onChange = ChangeItemProbability item >> ctx.lift
                    , label = Translations.areaEditorProbabilityLabel
                    , min = 1
                    , max = 100
                    , step = 1
                    }
                ]
                |> Atoms.Html.disable isDragging

        showSingle active item =
            -- TODO description
            Atoms.Html.image (class "w-16" :: DragAndDrop.draggable dragState (ItemDragAndDrop >> ctx.lift) ( item, active ))
                { description = \_ -> "", src = Item.toImagePath item }
    in
    [ iconHtmlWith { name = "save", width = 3, height = 3, description = Translations.mapCreatorDetailSaveDescription }
        [ Events.onClick (ctx.lift SaveItemChanges), class "flex items-center justify-center p-2" ]
    , div [ class "flex w-full gap-2 px-4 pt-2" ]
        [ selectedItemsArea ]
    , Item.allItems
        |> List.map (showSingle Nothing)
        |> div
            (class "box-border flex w-full max-w-4xl flex-wrap rounded-lg border-4 border-solid p-2"
                :: (case DragAndDrop.isDragging dragState of
                        Just _ ->
                            class "border-gray-200 bg-gray-100"

                        _ ->
                            class "border-white bg-white"
                   )
                :: DragAndDrop.droppable dragState (ItemDragAndDrop >> ctx.lift) Nothing
            )
    ]


typeSelectOptions : Translations.I18n -> NonEmpty ( Maybe Type, String )
typeSelectOptions i18n =
    ( ( Nothing, Translations.areaEditorEncountersSearchNone )
    , [ ( Just Type.Fire, Translations.pokemonTypeFire )
      , ( Just Type.Grass, Translations.pokemonTypeGrass )
      , ( Just Type.Flying, Translations.pokemonTypeFlying )
      , ( Just Type.Normal, Translations.pokemonTypeNormal )
      , ( Just Type.Poison, Translations.pokemonTypePoison )
      , ( Just Type.Water, Translations.pokemonTypeWater )
      , ( Just Type.Fighting, Translations.pokemonTypeFighting )
      , ( Just Type.Psychic, Translations.pokemonTypePsychic )
      , ( Just Type.Ice, Translations.pokemonTypeIce )
      , ( Just Type.Ground, Translations.pokemonTypeGround )
      , ( Just Type.Rock, Translations.pokemonTypeRock )
      , ( Just Type.Electric, Translations.pokemonTypeElectric )
      , ( Just Type.Steel, Translations.pokemonTypeSteel )
      , ( Just Type.Fairy, Translations.pokemonTypeFairy )
      , ( Just Type.Bug, Translations.pokemonTypeBug )
      , ( Just Type.Dragon, Translations.pokemonTypeDragon )
      , ( Just Type.Ghost, Translations.pokemonTypeGhost )
      ]
    )
        |> NonEmpty.map (\( type_, translation ) -> ( type_, translation i18n ))
        |> (\( empty, typeFilters ) -> ( empty, List.sortBy Tuple.second typeFilters ))


translationForLanguage : Translations.Language -> Translations.I18n -> String
translationForLanguage lang =
    case lang of
        Translations.De ->
            Translations.languageGerman

        Translations.En ->
            Translations.languageEnglish

        Translations.Fr ->
            Translations.languageFrench


itemTranslation : Item -> Translations.I18n -> String
itemTranslation item =
    case item of
        Item.Pokeball ->
            Translations.itemPokeball

        Item.Superball ->
            Translations.itemSuperball

        Item.Hyperball ->
            Translations.itemHyperball

        Item.Quickball ->
            Translations.itemQuickball

        Item.Diveball ->
            Translations.itemDiveball
