module Admin.Interop.Audio exposing (loop)

import Admin.InteropDefinitions as InteropDefinitions
import Admin.InteropPorts as InteropPorts
import Interop.Audio.Out as Out


loop : Out.AudioLoop -> Cmd msg
loop =
    Out.Loop >> send


send : Out.Msg -> Cmd msg
send =
    InteropDefinitions.AudioOut >> InteropPorts.fromElm
