module Admin.Overview exposing (Context, Msg, PageModel, init, onPageLoad, update, updateShared, view)

import Admin.Html exposing (Attribute, Document, Html)
import Admin.RequestManager as RequestManager
import Admin.Route as Route
import Admin.Session as Session exposing (Session)
import Admin.Translations as Translations
import Atoms.Button
import Atoms.Html
import Atoms.Input
import Atoms.MapPreview
import BoundedDict
import Domain.Coordinate exposing (Coordinate)
import Html.WithContext exposing (a, div, p)
import Html.WithContext.Attributes exposing (class, href, style)
import MapId exposing (MapId)
import Tagged exposing (tag)
import Textures.Store exposing (Sprite, Tilemaps)
import WebGL.Texture


type alias PageModel =
    { newMapName : String
    }


type Msg
    = ChangedNewMapName String
    | SubmitNewMap


init : PageModel
init =
    { newMapName = ""
    }


type alias Model m =
    Session
        { m
            | overviewModel : PageModel
        }


type alias Context msg =
    { lift : Msg -> msg, liftShared : Session.Msg -> msg }


onPageLoad : Context msg -> Model m -> ( Model m, Cmd msg )
onPageLoad ctx model =
    let
        ( requestManager, requestManagerCmds ) =
            RequestManager.register (requests model) model.requestManager
    in
    ( { model | requestManager = requestManager }
    , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
    )


requests : Model m -> List RequestManager.ApiCall
requests model =
    [ RequestManager.GetTilesetConfig {}, RequestManager.GetMaps {} ]
        ++ List.map (\map -> RequestManager.GetTextureMetadata { mapId = map.mapId }) model.maps


updateShared : Context msg -> Session.Msg -> Model m -> ( Model m, Cmd msg )
updateShared ctx msg model =
    case msg of
        Session.ReceivedMaps _ ->
            onPageLoad ctx model

        _ ->
            ( model, Cmd.none )


update : Context msg -> Msg -> Model m -> ( Model m, Cmd msg )
update ctx msg ({ overviewModel } as model) =
    case msg of
        ChangedNewMapName newName ->
            ( { model | overviewModel = { overviewModel | newMapName = newName } }, Cmd.none )

        SubmitNewMap ->
            let
                props =
                    { mapName = overviewModel.newMapName }

                ( requestManager, requestManagerCmds ) =
                    RequestManager.register [ RequestManager.CreateMap props ] model.requestManager
            in
            ( { model | overviewModel = { overviewModel | newMapName = "" }, requestManager = requestManager }
            , requestManagerCmds |> Cmd.map (Session.RequestManagerMessage >> ctx.liftShared)
            )


view : Context msg -> Model m -> Document msg
view ctx model =
    let
        namedMapToMapProps { mapName, mapId } =
            { id = tag mapId
            , name = mapName
            , tileset = model.tileset
            , tilemaps = BoundedDict.get (tag mapId) model.tilemaps
            }

        imageDimensions { mapId } =
            let
                center =
                    BoundedDict.get (tag mapId) model.tilemaps
                        |> Maybe.map
                            (\{ offset, texture, layers } ->
                                -- Perfect Centering by using offset, texture size and layer amount
                                -- Offset is the coordinate of the top-left-most tile in the map
                                -- Texture Size is not the total amount of tiles since we cannot encode arbitrarily many
                                -- layers into a single pixel. Instead we encode 2 layers into one pixel
                                -- and put the pixel for the next layers to the right (x-Dimension)
                                let
                                    layerBasedScaling =
                                        List.length layers // 2

                                    ( texX, texY ) =
                                        WebGL.Texture.size texture
                                in
                                { x = offset.x + texX // (2 * layerBasedScaling), y = offset.y + texY // 2 }
                            )
                        |> Maybe.withDefault { x = 0, y = 0 }
            in
            { mapSize = { width = 300, height = 200 }, center = center, zoom = 0.2 }
    in
    { title = Translations.mapCreatorOverviewTitle
    , body =
        [ div [ class "flex flex-wrap gap-2 p-2" ]
            (addMapForm ctx model.overviewModel.newMapName
                :: List.map (\map -> showMap (namedMapToMapProps map) (imageDimensions map))
                    model.maps
            )
        ]
    }


addMapForm : Context msg -> String -> Html msg
addMapForm ctx mapName =
    div [ class "flex flex-col gap-2", wrappedRowElementStyling ]
        [ Atoms.Input.text
            { id = "map"
            , onChange = ChangedNewMapName >> ctx.lift
            , label = Translations.mapCreatorOverviewAddMapNameLabel
            , text = mapName
            , error = Nothing
            }
        , addMapButton ctx
        ]


wrappedRowElementStyling : Attribute msg
wrappedRowElementStyling =
    class "h-[250px] w-[350px] rounded-lg bg-gray-200 p-2 dark:bg-gray-800"


addMapButton : Context msg -> Html msg
addMapButton ctx =
    Atoms.Button.button
        { onPress = ctx.lift SubmitNewMap |> Just
        , label = Atoms.Html.text Translations.mapCreatorOverviewAddMapSubmit
        , loading = False
        }
        []


showMap : { id : MapId, name : String, tileset : Maybe Sprite, tilemaps : Maybe Tilemaps } -> { mapSize : { height : Int, width : Int }, center : Coordinate Int, zoom : Float } -> Html msg
showMap props imageDimensions =
    a [ href (Route.Detail props.id |> Route.fromRoute), class "flex flex-col items-center justify-center" ]
        (case ( props.tileset, props.tilemaps ) of
            ( Just tileset, Just tilemaps ) ->
                [ Atoms.MapPreview.image imageDimensions tileset tilemaps, p [ class "text-center" ] [ Html.WithContext.text props.name ] ]

            _ ->
                [ div
                    [ style "width" <| String.fromInt imageDimensions.mapSize.width ++ "px"
                    , style "height" <| String.fromInt imageDimensions.mapSize.height ++ "px"
                    ]
                    []
                , p [ class "text-center" ] [ Html.WithContext.text props.name ]
                ]
        )
