module Admin.LanguageDict exposing (..)

import Admin.Translations as Translations exposing (Language)


type alias LanguageDict v =
    { de : v, en : v, fr : v }


constant : v -> LanguageDict v
constant v =
    { de = v, en = v, fr = v }


get : Language -> LanguageDict v -> v
get lang dict =
    case lang of
        Translations.De ->
            dict.de

        Translations.En ->
            dict.en

        Translations.Fr ->
            dict.fr


insert : Language -> v -> LanguageDict v -> LanguageDict v
insert lang v dict =
    case lang of
        Translations.De ->
            { dict | de = v }

        Translations.En ->
            { dict | en = v }

        Translations.Fr ->
            { dict | fr = v }
