-repository-url = https://gitlab.com/andreasewering/safari-zone
-issue-url = { -repository-url }/-/issues/new?issue[title]={ $title }&issue[description]={ $description }

# Generic
backButtonDescription = Back
cancelLabel = Cancel
navIconDescription = Navigation
safariIconLabel = Discover
dexIconLabel = Pokédex
settingsIconLabel = Settings
trainerSelectIconLabel = Trainer Selection
groupsIconLabel = Contacts
mapEditorIconLabel = Map Editor
logoutIconLabel = Logout
languageGerman = German
languageEnglish = English
languageFrench = French

# Errors
notFoundTitle = Not found
notFoundImageDescription = A slowbro
deserializationErrorTitle = Unexpected data from server
unauthorizedError = Your login has expired. Trying to get a new authentication token...
websocketConnectionErrorNotification = Lost connection to server. Trying to reconnect...
unexpectedDataImageDescription = A psyduck with a headache
urlErrorTitle = Requested invalid url
serverDownTitle = A server is down
serverDownText = If the problem persists, please write a <a href="mailto:support@safari-zone.app">mail</a> to the development team.
invalidRequestTitle = The server failed to understand a request.
unknownErrorTitle = Unknown error
loadTextureErrorNotification = Failed to load texture. The request will be retried automatically.
timeoutNotification = Request to server timed out. The request will be retried automatically.
timeoutImageDescription = A slowpoke
offlineNotification = You appear to be offline. Trying to reconnect...
genericErrorText = This shouldn't have happened. Please open an issue <a href="{-issue-url}">here</a>.
copyErrorButton = Copy error message
retryButton = Try again

# Pokemon types
pokemonTypeBug = Bug
pokemonTypeDragon = Dragon
pokemonTypeElectric = Electric
pokemonTypeFairy = Fairy
pokemonTypeFighting = Fighting
pokemonTypeFire = Fire
pokemonTypeFlying = Flying
pokemonTypeGhost = Ghost
pokemonTypeGrass = Grass
pokemonTypeGround = Ground
pokemonTypeIce = Ice
pokemonTypeNormal = Normal
pokemonTypePoison = Poison
pokemonTypePsychic = Psychic
pokemonTypeRock = Rock
pokemonTypeSteel = Steel
pokemonTypeWater = Water

# Items
itemPokeball = Poké Ball
itemSuperball = Great Ball
itemHyperball = Ultra Ball
itemDiveball = Dive Ball
itemQuickball = Quick Ball

# Map Creator
mapCreatorDetailTitle = Create map
mapCreatorDetailAreaDialogOpen = Create area
mapCreatorDetailAreaDialogSubmit= Save area
mapCreatorDetailCreateWarp = Create Warp
mapCreatorDetailCompleteWarp = Complete Warp
mapCreatorDetailCreateStartTile = Mark as start tile
mapCreatorDetailAreaNameLabel = Area name
mapCreatorDetailAreaMusicTrackLabel = Music track
mapCreatorDetailAreaProbabilityLabel = Probability
mapCreatorDetailAreaAddEncounter = Add encounter
mapCreatorDetailAreaAddItem = Add item
mapCreatorDetailDragToolDescription = Drag tool
mapCreatorDetailSelectToolDescription = Select tool
mapCreatorDetailSelectMultiToolDescription = Multiselect tool
mapCreatorDetailGoUpLayerDescription = Go up layer
mapCreatorDetailGoDownLayerDescription = Go down layer
mapCreatorDetailZoomInDescription = Zoom in
mapCreatorDetailZoomOutDescription = Zoom out
mapCreatorDetailDeleteDescription = Delete
mapCreatorDetailSaveDescription = Save
mapCreatorDetailEnableAutomaticTileSelection = Enable automatic tile selection
mapCreatorDetailDisableAutomaticTileSelection = Disable automatic tile selection
mapCreatorDetailTileSearchLabel = Tile search

mapCreatorOverviewTitle = Your maps
mapCreatorOverviewAddMapSubmit = Add new map
mapCreatorOverviewAddMapNameLabel = Map name

# Area Editor
areaEditorLayoutTitle = Layout
areaEditorMetadataTitle = Metadata
areaEditorNameInputLabel = Name ({ $language })
areaEditorEncountersTitle = Encounters
areaEditorItemsTitle = Items
areaEditorProbabilityLabel = Probability
areaEditorEncountersSearchLabel = Name/No
areaEditorEncountersSearchNone = No Filter
areaEditorEncountersTypeLabel = Typ
