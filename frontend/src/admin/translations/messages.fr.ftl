-repository-url = https://gitlab.com/andreasewering/safari-zone
-issue-url = { -repository-url }/-/issues/new?issue[title]={ $title }&issue[description]={ $description }

# Générique
backButtonDescription = Retour
cancelLabel = Annuler
navIconDescription = Navigation
safariIconLabel = Découvrir
dexIconLabel = Pokédex
settingsIconLabel = Paramètres
trainerSelectIconLabel = Sélection du dresseur
groupsIconLabel = Contacts
mapEditorIconLabel = Éditeur de carte
logoutIconLabel = Déconnexion
languageGerman = Allemand
languageEnglish = Anglais
languageFrench = Français

# Erreurs
notFoundTitle = Non trouvé
notFoundImageDescription = Un flagadoss
deserializationErrorTitle = Données inattendues du serveur
unauthorizedError = Votre connexion a expiré. Tentative d'obtention d'un nouveau jeton d'authentification...
websocketConnectionErrorNotification = Connexion au serveur perdue. Tentative de reconnexion...
unexpectedDataImageDescription = Un psykokwak avec un mal de tête
urlErrorTitle = URL demandée invalide
serverDownTitle = Un serveur est en panne
serverDownText = Si le problème persiste, veuillez envoyer un <a href="mailto:support@safari-zone.app">mail</a> à l'équipe de développement.
invalidRequestTitle = Le serveur n'a pas compris une requête.
unknownErrorTitle = Erreur inconnue
loadTextureErrorNotification = Échec du chargement de la texture. La requête sera réessayée automatiquement.
timeoutNotification = La requête au serveur a expiré. La requête sera réessayée automatiquement.
timeoutImageDescription = Un ramoloss
offlineNotification = Vous semblez être hors ligne. Tentative de reconnexion...
genericErrorText = Cela n'aurait pas dû arriver. Veuillez ouvrir un problème <a href="{-issue-url}">ici</a>.
copyErrorButton = Copier le message d'erreur
retryButton = Réessayer

# Types de Pokémon
pokemonTypeBug = Insecte
pokemonTypeDragon = Dragon
pokemonTypeElectric = Électrique
pokemonTypeFairy = Fée
pokemonTypeFighting = Combat
pokemonTypeFire = Feu
pokemonTypeFlying = Vol
pokemonTypeGhost = Spectre
pokemonTypeGrass = Plante
pokemonTypeGround = Sol
pokemonTypeIce = Glace
pokemonTypeNormal = Normal
pokemonTypePoison = Poison
pokemonTypePsychic = Psy
pokemonTypeRock = Roche
pokemonTypeSteel = Acier
pokemonTypeWater = Eau

# Objets
itemPokeball = Poké Ball
itemSuperball = Super Ball
itemHyperball = Hyper Ball
itemDiveball = Scuba Ball
itemQuickball = Rapide Ball

# Créateur de carte
mapCreatorDetailTitle = Créer une carte
mapCreatorDetailAreaDialogOpen = Créer une zone
mapCreatorDetailAreaDialogSubmit= Enregistrer la zone
mapCreatorDetailCreateWarp = Créer un Warp
mapCreatorDetailCompleteWarp = Compléter le Warp
mapCreatorDetailCreateStartTile = Marquer comme case de départ
mapCreatorDetailAreaNameLabel = Nom de la zone
mapCreatorDetailAreaMusicTrackLabel = Piste musicale
mapCreatorDetailAreaProbabilityLabel = Probabilité
mapCreatorDetailAreaAddEncounter = Ajouter une rencontre
mapCreatorDetailAreaAddItem = Ajouter un objet
mapCreatorDetailDragToolDescription = Outil de déplacement
mapCreatorDetailSelectToolDescription = Outil de sélection
mapCreatorDetailSelectMultiToolDescription = Outil de sélection multiple
mapCreatorDetailGoUpLayerDescription = Monter d'un niveau
mapCreatorDetailGoDownLayerDescription = Descendre d'un niveau
mapCreatorDetailZoomInDescription = Zoomer
mapCreatorDetailZoomOutDescription = Dézoomer
mapCreatorDetailDeleteDescription = Supprimer
mapCreatorDetailSaveDescription = Enregistrer
mapCreatorDetailEnableAutomaticTileSelection = Activer la sélection automatique de cases
mapCreatorDetailDisableAutomaticTileSelection = Désactiver la sélection automatique de cases
mapCreatorDetailTileSearchLabel = Recherche de cases

mapCreatorOverviewTitle = Vos cartes
mapCreatorOverviewAddMapSubmit = Ajouter une nouvelle carte
mapCreatorOverviewAddMapNameLabel = Nom de la carte

# Éditeur de zone
areaEditorLayoutTitle = Disposition
areaEditorMetadataTitle = Métadonnées
areaEditorNameInputLabel = Nom ({ $language })
areaEditorEncountersTitle = Rencontres
areaEditorItemsTitle = Objets
areaEditorProbabilityLabel = Probabilité
areaEditorEncountersSearchLabel = Nom/No
areaEditorEncountersSearchNone = Pas de filtre
areaEditorEncountersTypeLabel = Type
