-repository-url = https://gitlab.com/andreasewering/safari-zone
-issue-url = { -repository-url }/-/issues/new?issue[title]={ $title }&issue[description]={ $description }

# Generic
backButtonDescription = Zurück
cancelLabel = Abbrechen
navIconDescription = Navigation
safariIconLabel = Erkunden
dexIconLabel = Pokédex
settingsIconLabel = Einstellungen
trainerSelectIconLabel = Trainerauswahl
groupsIconLabel = Kontakte
mapEditorIconLabel = Karteneditor
logoutIconLabel = Ausloggen
languageGerman = Deutsch
languageEnglish = Englisch
languageFrench = Französisch

# Errors
notFoundTitle = Nichts zu finden
notFoundImageDescription = Ein Lahmus
deserializationErrorTitle = Unerwartete Daten vom Server
unauthorizedError = Dein Login ist abgelaufen. Eine neues Token wird angefragt...
websocketConnectionErrorNotification = Verbindung zum Server abgebrochen. Eine neue Verbindung wird aufgebaut...
unexpectedDataImageDescription = Ein Enton mit Kopfschmerzen
urlErrorTitle = Invalide Url angefragt
serverDownTitle = Ein Server ist down
serverDownText = Falls das Problem länger bestehen bleibt, schreibe eine <a href="mailto:support@safari-zone.app">Mail</a> an das Entwicklerteam.
invalidRequestTitle = Der Server hat die Anfrage nicht verstanden.
unknownErrorTitle = Unbekannter Fehler
loadTextureErrorNotification = Konnte Textur nicht laden. Die Anfrage wird automatisiert neu gestartet.
timeoutNotification = Anfrage an Server hat zu lange gedauert. Die Anfrage wird automatisiert neu gestartet.
timeoutImageDescription = Ein Flegmon
offlineNotification = Du scheinst offline zu sein. Verbindung wird wiederhergestellt...
genericErrorText = Das sollte nicht passiert sein. Bitte erstelle ein Ticket <a href="{-issue-url}">hier</a>.
copyErrorButton = Fehlermeldung kopieren
retryButton = Nochmal versuchen

# Pokemon Types
pokemonTypeBug = Käfer
pokemonTypeDragon = Drache
pokemonTypeElectric = Elektro
pokemonTypeFairy = Fee
pokemonTypeFighting = Kampf
pokemonTypeFire = Feuer
pokemonTypeFlying = Flug
pokemonTypeGhost = Geist
pokemonTypeGrass = Pflanze
pokemonTypeGround = Boden
pokemonTypeIce = Eis
pokemonTypeNormal = Normal
pokemonTypePoison = Gift
pokemonTypePsychic = Psycho
pokemonTypeRock = Gestein
pokemonTypeSteel = Stahl
pokemonTypeWater = Wasser

# Items
itemPokeball = Pokéball
itemSuperball = Superball
itemHyperball = Hyperball
itemDiveball = Tauchball
itemQuickball = Flottball


# Map Creator
mapCreatorDetailTitle = Karte erstellen
mapCreatorDetailAreaDialogOpen = Bereich erstellen
mapCreatorDetailAreaDialogSubmit= Bereich speichern
mapCreatorDetailCreateWarp = Warppunkt erstellen
mapCreatorDetailCompleteWarp = Warppunkt vervollständigen
mapCreatorDetailCreateStartTile = Als Startpunkt markieren
mapCreatorDetailAreaNameLabel = Name des Bereichs
mapCreatorDetailAreaMusicTrackLabel = Hintergrundmusik
mapCreatorDetailAreaProbabilityLabel = Wahrscheinlichkeit
mapCreatorDetailAreaAddEncounter = Begegnung hinzufügen
mapCreatorDetailAreaAddItem = Item hinzufügen
mapCreatorDetailDragToolDescription = Kartenbewegungs-Werkzeug
mapCreatorDetailSelectToolDescription = Auswahl-Werkzeug
mapCreatorDetailSelectMultiToolDescription = Mehrfachauswahl-Werkzeug
mapCreatorDetailGoUpLayerDescription = Ebene hoch
mapCreatorDetailGoDownLayerDescription = Ebene runter
mapCreatorDetailZoomInDescription = Reinzoomen
mapCreatorDetailZoomOutDescription = Rauszoomen
mapCreatorDetailDeleteDescription = Löschen
mapCreatorDetailSaveDescription = Speichern
mapCreatorDetailEnableAutomaticTileSelection = Automatische Kachelauswahl anschalten
mapCreatorDetailDisableAutomaticTileSelection = Automatische Kachelauswahl ausschalten
mapCreatorDetailTileSearchLabel = Kachelsuche

mapCreatorOverviewTitle = Deine Karten
mapCreatorOverviewAddMapSubmit = Neue Karte erstellen
mapCreatorOverviewAddMapNameLabel = Kartenname

# Area Editor
areaEditorLayoutTitle = Grundriss
areaEditorMetadataTitle = Metadaten
areaEditorNameInputLabel = Name ({ $language })
areaEditorEncountersTitle = Begegnungen
areaEditorItemsTitle = Items
areaEditorProbabilityLabel = Wahrscheinlichkeit
areaEditorEncountersSearchLabel = Name/Nr
areaEditorEncountersSearchNone = Kein Filter
areaEditorEncountersTypeLabel = Typ
