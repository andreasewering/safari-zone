import { customHttpInterceptorRules } from './custom-rules';

/**
 * This module implements a monkeypatch for the Browsers `XMLHttpRequest`
 * by wrapping the methods on its prototype.
 *
 * This is done via the following pattern:
 *
 * 1. Save original method to local variable
 *  ```
 *   const original = XMLHttpRequest.prototype.theMethodName;
 *    ```
 * 2. Overwrite method with a new function
 *  ```
 *  XMLHttpRequest.prototype.theMethodName = function(...args) {
 *    return original.bind(this)(...args);
 *  }
 *  ```
 *
 * 3. Add custom logic to the function.
 *
 * The patch allows us to modify responses in various ways, including:
 *  - Introducing a delay
 *  - Changing the HTTP status code
 *  - Changing the response body
 *  - Changing response headers
 *  - Pausing requests
 *  - Resuming paused requests via the DevTools Browser Console
 *  - Registering custom callbacks
 *
 * By default, the interceptor does nothing. You can add hardcoded rules by modifying
 * `custom-rules.ts`, or you can add rules at runtime using `window.hi.rules.push({ ... })`.
 *
 * This is really handy for testing edge cases like loading spinners, out of order responses,
 * and unlikely errors or errors that cannot exist right now,
 * but can exist in the future (e.g. serialization errors).
 */

class HttpInterceptor {
  /**
   * @type {import('./http-interceptor-rule').HttpInterceptorRule[]}
   */
  rules = customHttpInterceptorRules;

  pausedRequests = [];

  #reminderTimerId;

  constructor() {
    this.startReminder();
  }

  startReminder() {
    if (this.#reminderTimerId !== undefined) {
      clearInterval(this.#reminderTimerId);
    }
    this.#reminderTimerId = setInterval(() => {
      if (this.pausedRequests.length > 0) {
        console.warn(`There are still paused requests to the following urls:
${this.pausedRequests
  .map(([requestParams]) => `${requestParams.method} ${requestParams.url}`)
  .join('\n')}

You may resume them using window.hi.resume()`);
      }
    }, 5000);
  }

  /**
   *
   * @param {RequestParams} requestParams
   * @returns {import('./http-interceptor-rule').HttpInterceptorRule[]}
   */
  getRules(requestParams) {
    return this.rules
      .map((rule) => {
        const priority = ruleMatchesParams(rule, requestParams);
        return [priority, rule];
      })
      .filter(([prio]) => prio > 0)
      .sort(([prioA], [prioB]) => prioB - prioA)
      .map(([, rule]) => rule);
  }

  /**
   *
   * @param {RequestParams} requestParams
   * @returns {import('./http-interceptor-rule').HttpInterceptorConfig}
   */
  getInterceptorConfig(requestParams) {
    return this.getRules(requestParams).reduce(combineGeneric, {});
  }

  resume(matcherOrMethod) {
    this.startReminder();

    function shouldUnpause(requestParams) {
      if (!matcherOrMethod) {
        return true;
      }
      if (matcherOrMethod instanceof RegExp) {
        return !!matcherOrMethod.exec(requestParams.url);
      }
      if (['POST', 'PUT', 'GET', 'DELETE'].includes(matcherOrMethod)) {
        return requestParams.method === matcherOrMethod;
      }
      return requestParams.url.includes(matcherOrMethod);
    }

    const stillPausedRequests = [];
    const resumedRequests = [];

    for (const pausedRequest of this.pausedRequests) {
      const [requestParams, resolve] = pausedRequest;
      if (shouldUnpause(requestParams)) {
        resolve();
        resumedRequests.push(requestParams);
      } else {
        stillPausedRequests.push(pausedRequest);
      }
    }

    this.pausedRequests = stillPausedRequests;
    return resumedRequests;
  }
}

/**
 * @param {import('./http-interceptor-rule').HttpInterceptorRule} rule
 * @param {RequestParams} requestParams
 * @returns {number} priority of the rule, higher is a better match.
 *                   priority <= 0 means no match.
 */
function ruleMatchesParams(rule, requestParams) {
  let priority = 0;

  if (rule.match) {
    const doesMatch = rule.match.exec(requestParams.url);
    if (!doesMatch) {
      return 0;
    }
    priority += rule.match.source.length;
  }

  if (rule.method) {
    if (rule.method !== requestParams.method) {
      return 0;
    }
    priority += 10;
  }

  return priority;
}

/**
 * Combine two objects. The former object is deemed more important.
 * @template T
 * @param {T} a
 * @param {T} b
 * @returns {T} merged object
 */
function combineGeneric(a, b) {
  const merged = { ...a };
  for (const [k, v] of Object.entries(b)) {
    if (!(k in a)) {
      merged[k] = v;
    }
  }

  return merged;
}

window.hi = new HttpInterceptor();

// Save a reference to the original methods
const originalOpen = window.XMLHttpRequest.prototype.open;
const originalSend = window.XMLHttpRequest.prototype.send;
const originalGetResponseHeader =
  window.XMLHttpRequest.prototype.getResponseHeader;
const originalGetAllResponseHeaders =
  window.XMLHttpRequest.prototype.getAllResponseHeaders;

// Override open method to capture parameters
window.XMLHttpRequest.prototype.open = function (...args) {
  const [method, url] = args;
  this.requestParams = { method, url: url.toString() };
  originalOpen.apply(this, args);
};

// Override send method to control responses
window.XMLHttpRequest.prototype.send = function (...args) {
  const xhr = this;
  const interceptorConfig = window.hi.getInterceptorConfig(xhr.requestParams);

  if (interceptorConfig.body) {
    Object.defineProperty(xhr, 'response', {
      get() {
        return interceptorConfig.body;
      },
    });
  }

  if (interceptorConfig.status) {
    Object.defineProperty(xhr, 'status', {
      get() {
        return interceptorConfig.status;
      },
    });
  }

  if (interceptorConfig.headers) {
    xhr.customHeaders = interceptorConfig.headers;
  }

  setTimeout(async function () {
    if (interceptorConfig.pause) {
      console.warn(
        `Paused ${xhr.requestParams.method} request to ${xhr.requestParams.url}.
You may resume the request at any point using window.hi.resume('${xhr.requestParams.url}')`
      );
      await new Promise((resolve) => {
        window.hi.pausedRequests.push([xhr.requestParams, resolve]);
        window.hi.startReminder();
      });
    }
    originalSend.apply(xhr, args);
    if (interceptorConfig.callback) {
      interceptorConfig.callback();
    }
  }, interceptorConfig.delay ?? 0);
};

window.XMLHttpRequest.prototype.getResponseHeader = function (name) {
  if (this.customHeaders) {
    const headerValue = this.customHeaders[name];
    if (headerValue) {
      return headerValue;
    }
  }
  return originalGetResponseHeader.bind(this)(name);
};

window.XMLHttpRequest.prototype.getAllResponseHeaders = function () {
  const headers = originalGetAllResponseHeaders.bind(this)();

  if (!this.customHeaders) {
    return headers;
  }

  const customHeadersToGo = { ...this.customHeaders };

  const previousHeaders = headers
    .split('\r\n')
    .filter((v) => !!v)
    .map((header) => header.split(': '))
    .map(([headerName, ...headerValue]) => {
      const overriddenHeaderValue = this.customHeaders[headerName];
      delete customHeadersToGo[headerName];
      return [
        headerName,
        ...(overriddenHeaderValue ? [overriddenHeaderValue] : headerValue),
      ];
    });
  return (
    [...previousHeaders, ...Object.entries(customHeadersToGo)]
      .map((headerParts) => headerParts.join(': '))
      .join('\r\n') + '\r\n'
  );
};

/**
 * @typedef RequestParams
 * @property {string} url
 * @property {string} method
 */

export {};
