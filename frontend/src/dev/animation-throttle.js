// Proof of concept animation throttle to achieve a consistent FPS.
// Might be worth using if it reduces memory usage/increases performance if it still looks "smooth enough".

const DT_LIMIT = 1000 / 60;
const RAF = window.requestAnimationFrame;
const CAF = window.cancelAnimationFrame;

const rafIdMap = new Map();

window.requestAnimationFrame = (callback) => {
  let lastTimestamp = performance.now();
  let originalRId;
  const wrappedCallback = (timestamp) => {
    const deltaTime = timestamp - lastTimestamp;
    if (deltaTime > DT_LIMIT) {
      return callback(timestamp);
    }
    const rId = RAF(wrappedCallback);
    rafIdMap.set(originalRId, rId);
  };
  originalRId = RAF(wrappedCallback);
  return originalRId;
};

window.cancelAnimationFrame = (rId) => {
  const currentRId = rafIdMap.get(rId);
  if (currentRId) {
    rafIdMap.delete(rId);
    return CAF(currentRId);
  }
};
