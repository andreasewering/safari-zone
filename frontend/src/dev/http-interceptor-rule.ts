export type HttpInterceptorRule = {
  match?: RegExp;
  method?: 'POST' | 'GET' | 'PUT' | 'DELETE';
} & HttpInterceptorConfig;

export type HttpInterceptorConfig = {
  delay?: number;
  pause?: boolean;
  status?: number;
  headers?: Record<string, string>;
  body?: string | ArrayBuffer;
  callback?: () => void;
};
