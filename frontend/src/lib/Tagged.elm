module Tagged exposing (Tagged, tag, untag)

{-| Simple tagging mechanism for type-safe primitive types.

Problem statement:
Imagine the following function:

    someFunction : Int -> Int -> ReturnType
    someFunction mapId userId = ...

The IDs are NOT interchangeable, but the type system cannot help us here.
The naive solution is to create custom data types `type MapId = MapId Int` and `type UserId = UserId Int` to differentiate the two,
but this is annoying to use - methods on top of these types have to be implemented for each custom data type.

`Tagged` uses a phantom type parameter instead, which means that `tag` and `untag` works regardless of the `tag` type.
So you can define

    type UserIdTag
        = UserIdTag

    type alias UserId =
        Tagged Int UserIdTag

    type MapIdTag
        = MapIdTag

    type alias MapId =
        Tagged Int MapIdTag

-}


type Tagged t tag
    = Tagged t


tag : t -> Tagged t tag
tag =
    Tagged


untag : Tagged t tag -> t
untag (Tagged t) =
    t
