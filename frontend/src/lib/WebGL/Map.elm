module WebGL.Map exposing (entity)

import Camera exposing (Camera)
import Config
import Data.Units exposing (TileUnit(..), unWebGLUnit)
import Domain.Coordinate exposing (Coordinate)
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 exposing (Vec2, vec2)
import Math.Vector4 exposing (Vec4, vec4)
import WebGL
import WebGL.Common
import WebGL.Texture as Texture exposing (Texture)


entity : Camera -> Float -> Coordinate Int -> Texture -> Texture -> Int -> Int -> WebGL.Entity
entity camera zoom offset tileSet tileMap layersInTotal layerIndex =
    let
        ( totalTilesX, totalTilesY ) =
            Texture.size tileMap |> Tuple.mapBoth toFloat toFloat

        layerBasedScaling =
            (layersInTotal + 1) // 2

        adjustedTilesX =
            totalTilesX / toFloat layerBasedScaling

        scalingX =
            adjustedTilesX * Config.tileSize / toFloat (Camera.getScreenWidth camera)

        scalingY =
            totalTilesY * Config.tileSize / toFloat (Camera.getScreenHeight camera)

        trans =
            Camera.calcWebGLCoord camera { x = TileUnit <| toFloat offset.x, y = TileUnit <| toFloat offset.y }

        translateX =
            (unWebGLUnit trans.x - pixelFixX) / scalingX

        translateY =
            (unWebGLUnit trans.y + pixelFixY) / scalingY

        pixelFixX =
            (Config.trainerSize + 20) * zoom / (2 * toFloat (Camera.getScreenWidth camera))

        pixelFixY =
            (Config.trainerSize - 80) * zoom / (2 * toFloat (Camera.getScreenWidth camera))

        tilesetPicker =
            -- Inverse to the backend encoding of tile ids. Even layers occupy the red and green channels, odd layers blue and alpha.
            if modBy 2 layerIndex == 0 then
                vec4 1 256 0 0

            else
                vec4 0 0 1 256

        entityForFragmentShader fragShader =
            WebGL.entityWith WebGL.Common.settings
                WebGL.Common.vertexShader
                fragShader
                WebGL.Common.mesh
                { perspective =
                    Mat4.identity
                        |> Mat4.scale3 (scalingX * zoom) (scalingY * zoom) 1
                        |> Mat4.translate3 (translateX + 1) (translateY - 1) 0
                , texture = tileSet
                , spriteSheetSize = WebGL.Common.textureSize tileSet
                , spriteSize = vec2 16 16
                , encodedMap = tileMap
                , tileSize = vec2 Config.tileSize Config.tileSize
                , encodedMapSize = vec2 adjustedTilesX totalTilesY
                , xOffset = toFloat (layerIndex // 2)
                , xScale = toFloat layerBasedScaling
                , tilesetPicker = tilesetPicker
                }
    in
    entityForFragmentShader fragmentShader


type alias Uniforms =
    { perspective : Mat4
    , texture : Texture
    , encodedMap : Texture
    , encodedMapSize : Vec2
    , spriteSheetSize : Vec2
    , spriteSize : Vec2
    , tileSize : Vec2
    , xOffset : Float
    , xScale : Float
    , tilesetPicker : Vec4
    }


fragmentShader : WebGL.Shader {} Uniforms { vcoord : Vec2 }
fragmentShader =
    [glsl|
        precision mediump float;
        uniform sampler2D texture;
        uniform sampler2D encodedMap;
        uniform vec2 encodedMapSize;
        uniform vec2 spriteSheetSize;   // In px
        uniform vec2 spriteSize;        // In px
        uniform vec2 tileSize;
        uniform vec4 tilesetPicker;
        uniform float xOffset;
        uniform float xScale;
        varying vec2 vcoord;

        vec2 getIndex(in vec2 coord) {
          vec2 tile = floor(vec2(coord.x, 1.0 - coord.y) * encodedMapSize);
          float indexX = (tile.x + (xOffset + 0.5) / xScale) / encodedMapSize.x;
          float indexY = 1.0 - ((tile.y + 0.5) / encodedMapSize.y);
          return vec2(indexX, indexY); 
        }
        // -- Example: coord is in (0/1 space): so something like (0.302, 0.506)
        // -- We scale this to the map size and floor. Say the map size is 200/50px then
        // -- the coord would be mapped to (60, 10).
        // -- This is where the offset for the encoding of multiple layers comes in
        // -- Say there are two layers, the map is (in spirit) 100/50px large
        // -- and the actual coord we are right now in user space is (30, 10)
        // -- We now need to add the offset for the layer index to go to (61, 10) for example
        // -- add 0.5 to go precisely in the "middle" of the pixel and divide through the map size to go back to (0/1 space)

        void main () {
          vec2 indexXY = getIndex(vcoord);
          vec4 encodedColor = texture2D(encodedMap, indexXY);

          float index = floor(dot(tilesetPicker, encodedColor)) - 1.0;

          if (index == -1.0) {
            discard;
          }

          float w = spriteSheetSize.x;
          float h = spriteSheetSize.y;
          //-- Normalize sprite size (0.0-1.0) - Percentage a sprite takes up of the sprite sheet
          float dx = spriteSize.x / w;
          float dy = spriteSize.y / h;
          float cols = w / spriteSize.x;
          //-- From linear index to row/col pair
          float col = mod(index, cols);
          float row = floor(index / cols);
          //-- magic constants fix issue with overlapping tiles due to floating point precision
          float xxx = clamp(fract(vcoord.x * encodedMapSize.x), 0.01, 0.99);
          float yyy = clamp(fract((1.0 - vcoord.y) * encodedMapSize.y), 0.01, 0.99);
          vec2 uv = vec2(dx * (col + xxx), 1.0 - dy * (row + yyy));
          gl_FragColor = texture2D(texture, uv);
        }
    |]
