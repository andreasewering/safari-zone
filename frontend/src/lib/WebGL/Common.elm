module WebGL.Common exposing (..)

import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 exposing (Vec2, vec2)
import Math.Vector3 exposing (Vec3, vec3)
import WebGL
import WebGL.Settings
import WebGL.Settings.Blend as Blend
import WebGL.Texture exposing (Texture)


settings : List WebGL.Settings.Setting
settings =
    [ Blend.add Blend.srcAlpha Blend.oneMinusSrcAlpha ]


type alias Vertex =
    { position : Vec3
    , coord : Vec2
    }


textureSize : Texture -> Vec2
textureSize =
    WebGL.Texture.size >> (\( x, y ) -> vec2 (toFloat x) (toFloat y))


mesh : WebGL.Mesh Vertex
mesh =
    WebGL.triangles square


perspective : Mat4
perspective =
    Mat4.scale3 2 2 1 (Mat4.makeTranslate3 1 1 0)


vertexShader : WebGL.Shader Vertex { a | perspective : Mat4 } { vcoord : Vec2 }
vertexShader =
    [glsl|
        attribute vec3 position;
        attribute vec2 coord;
        uniform mat4 perspective;
        varying vec2 vcoord;
        void main () {
          gl_Position = perspective * vec4(position.xy, 0.0, 1.0);
          vcoord = coord.xy;
        }
    |]


square : List ( Vertex, Vertex, Vertex )
square =
    let
        topLeft =
            Vertex (vec3 -1 1 1) (vec2 0 1)

        topRight =
            Vertex (vec3 1 1 1) (vec2 1 1)

        bottomLeft =
            Vertex (vec3 -1 -1 1) (vec2 0 0)

        bottomRight =
            Vertex (vec3 1 -1 1) (vec2 1 0)
    in
    [ ( topLeft, topRight, bottomLeft )
    , ( bottomLeft, topRight, bottomRight )
    ]


type alias Uniforms =
    { perspective : Mat4
    , spriteSheet : Texture
    , spritePercent : Vec2
    , row : Float
    , col : Float
    , opacity : Float
    }


spriteSheetFragmentShader : WebGL.Shader {} Uniforms { vcoord : Vec2 }
spriteSheetFragmentShader =
    [glsl|
        precision mediump float;
        uniform sampler2D spriteSheet;
        uniform vec2 spritePercent;
        uniform float row;
        uniform float col;
        uniform float opacity;
        varying vec2 vcoord;

        void main() {
          vec2 uv = vec2(spritePercent.x * (col + vcoord.x), 1.0 - spritePercent.y * (row + 1.0 - vcoord.y));
          gl_FragColor = texture2D(spriteSheet, uv) * vec4(1, 1, 1, opacity);
        }
    |]
