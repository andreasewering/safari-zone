module EnumSetMembers exposing (EnumSetMembers, contiguous, get, getIndex, toIndexedList)

import List.Extra


type EnumSetMembers a
    = EnumSetMembers (List (Maybe a))


contiguous : List a -> EnumSetMembers a
contiguous =
    List.map Just >> EnumSetMembers


getIndex : a -> EnumSetMembers a -> Maybe Int
getIndex a (EnumSetMembers members) =
    List.Extra.findIndex ((==) (Just a)) members


get : Int -> EnumSetMembers a -> Maybe a
get index (EnumSetMembers members) =
    List.Extra.getAt index members |> Maybe.andThen identity


toIndexedList : EnumSetMembers a -> List ( Int, a )
toIndexedList (EnumSetMembers members) =
    List.indexedMap (\index member -> ( index, member )) members
        |> List.filterMap (\( index, member ) -> Maybe.map (Tuple.pair index) member)
