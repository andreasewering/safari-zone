module Auth exposing
    ( AuthManager, Authentification, Permissions
    , manageSingle, decodeAuthentification
    , getUserId, getMapId, getToken, headers, addHeaders
    , addToken, switchMap, setLifespan, updateTime
    , refreshReminder, refreshAuth, logout
    , decodedTokenToAuth, getMapIdMaybe
    )

{-| Module centered around Authentication.
Tokens contain metadata encoded into them, one of which is the current map id.
There may be two reasons to refresh the token:

  - The token expired

  - The user switched the map

Since in the latter case, we may be able to save a call if the token is still valid,
this module provides caching functionality in form of the
`AuthManager` construct.


# Main data types

@docs AuthManager, Authentification, Permissions


# Ways to create them

@docs manageSingle, decodeAuthentification


# Readonly operations

@docs getUserId, getUserName, getMapId, getToken, getPermissions, headers, addHeaders


# Update operations

@docs addToken, switchMap, setLifespan, updateTime


# Effects

@docs refreshReminder, refreshAuth, logout

-}

import Dict exposing (Dict)
import Grpc
import Http
import MapId exposing (MapId)
import Maybe.Extra
import Proto.Gardevoir
import Proto.Gardevoir.GardevoirService as GardevoirService
import Proto.Gardevoir.Permission as Permission
import Protobuf.Token
import Protobuf.Types.Int64 as Int64
import Shared.Types.Error as Error
import Tagged exposing (tag)
import Time
import TsJson.Decode as TsDecode
import TsJson.Decode.Pipeline as TsDecode
import UserId exposing (UserId)


{-| An access token, its lifespan (in seconds) and its encoded metadata
-}
type alias Authentification =
    { token : String
    , lifespan : Int
    , permissions : Permissions
    , userId : UserId
    , mapId : Maybe MapId
    }


{-| A container for possibly one or more access tokens which differ by their map id.
This upholds the invariant that at least one token, the one which matches the currentMapId is contained.
-}
type AuthManager
    = AuthManager
        { currentMapId : Maybe MapId
        , userId : UserId
        , permissions : Permissions
        , timestamp : Time.Posix
        , tokens : Dict ( Int, Int ) { token : String, lifespan : Int }
        }


{-| Create an `AuthManager` from a single token and a timestamp.
-}
manageSingle : Authentification -> Time.Posix -> AuthManager
manageSingle { token, lifespan, permissions, userId, mapId } timestamp =
    AuthManager
        { currentMapId = mapId
        , userId = userId
        , permissions = permissions
        , timestamp = timestamp
        , tokens =
            let
                key =
                    mapId
                        |> Maybe.withDefault noMapIdDefault
                        |> MapId.toComparable
            in
            Dict.singleton key { token = token, lifespan = lifespan }
        }


{-| Generates a correct `authorization` header with the active token.
-}
headers : AuthManager -> List Http.Header
headers (AuthManager manager) =
    manager.currentMapId
        |> Maybe.andThen (\mapId -> Dict.get (MapId.toComparable mapId) manager.tokens)
        |> Maybe.map (.token >> tokenToHeader)
        |> Maybe.Extra.toList


addHeaders : AuthManager -> Grpc.RpcRequest req res -> Grpc.RpcRequest req res
addHeaders manager req =
    List.foldl (\( key, value ) -> Grpc.addHeader key value) req (headersKeyValuePairs manager)


headersKeyValuePairs : AuthManager -> List ( String, String )
headersKeyValuePairs (AuthManager manager) =
    let
        key =
            manager.currentMapId |> Maybe.withDefault noMapIdDefault |> MapId.toComparable

        xMapIdHeader =
            case manager.currentMapId of
                Just mapId ->
                    [ ( "x-map-id", MapId.toHeader mapId ) ]

                Nothing ->
                    []
    in
    Dict.get key manager.tokens
        |> Maybe.map (.token >> tokenToHeaderTuple)
        |> Maybe.Extra.toList
        |> (++) xMapIdHeader


{-| Access the `user_id` field from the token metadata.
-}
getUserId : AuthManager -> UserId
getUserId (AuthManager { userId }) =
    userId


{-| Access the `map_id` field from the token metadata and default to map\_id 0.
The map\_id will only be Nothing when the user is not initialized or if
the users map got deleted.
-}
getMapId : AuthManager -> MapId
getMapId (AuthManager { currentMapId }) =
    currentMapId |> Maybe.withDefault noMapIdDefault


{-| Access the `map_id` field from the token metadata.
-}
getMapIdMaybe : AuthManager -> Maybe MapId
getMapIdMaybe (AuthManager { currentMapId }) =
    currentMapId


getToken : AuthManager -> String
getToken (AuthManager manager) =
    let
        key =
            manager.currentMapId |> Maybe.withDefault noMapIdDefault |> MapId.toComparable
    in
    Dict.get key manager.tokens
        |> Maybe.map .token
        -- Should never because of the invariants of this module
        |> Maybe.withDefault ""


{-| Attempt to switch the active map id. This can either use a cached token, which is still valid,
or trigger a refresh request to get a new one.
-}
switchMap : MapId -> AuthManager -> ( AuthManager, Cmd (Result Error.Error Authentification) )
switchMap mapId (AuthManager manager) =
    case Dict.get (MapId.toComparable mapId) manager.tokens of
        Just _ ->
            ( AuthManager { manager | currentMapId = Just mapId }, Cmd.none )

        Nothing ->
            ( AuthManager manager, refreshAuth )


{-| Add a token to an `AuthManager`. This does not automatically switch the currentMapId,
since that would complicate switching maps with an animation delay.
-}
addToken : Authentification -> AuthManager -> AuthManager
addToken auth (AuthManager manager) =
    let
        doesPreviousTokenMetadataMatch =
            manager.userId
                == auth.userId
                && manager.permissions
                == auth.permissions

        handlePreviousToken =
            if doesPreviousTokenMetadataMatch then
                identity

            else
                always Dict.empty

        newKey =
            auth.mapId |> Maybe.withDefault noMapIdDefault |> MapId.toComparable

        addNewToken =
            Dict.insert newKey { token = auth.token, lifespan = auth.lifespan }
    in
    AuthManager
        { manager
            | userId = auth.userId
            , permissions = auth.permissions
            , tokens =
                handlePreviousToken manager.tokens
                    |> addNewToken
        }


{-| Set the lifespan of the current token in seconds.
This may be useful to trigger refreshes quickly if the server was temporarily down.
-}
setLifespan : Int -> AuthManager -> AuthManager
setLifespan lifespanInSeconds (AuthManager manager) =
    let
        key =
            manager.currentMapId |> Maybe.withDefault noMapIdDefault |> MapId.toComparable
    in
    AuthManager
        { manager
            | tokens =
                Dict.update key
                    (Maybe.map (\token -> { token | lifespan = lifespanInSeconds }))
                    manager.tokens
        }


refreshReminder : AuthManager -> Sub Time.Posix
refreshReminder (AuthManager { currentMapId, tokens }) =
    let
        lifespanInSeconds =
            Dict.get key tokens
                |> Maybe.map .lifespan
                -- Note: this never happens because of the invariants preserved by this module
                |> Maybe.withDefault 0

        key =
            currentMapId |> Maybe.withDefault noMapIdDefault |> MapId.toComparable
    in
    Time.every (toFloat lifespanInSeconds * 800) identity


{-| Update the timestamp used to discard cached tokens once their validity runs out.
-}
updateTime : Time.Posix -> AuthManager -> AuthManager
updateTime timestamp (AuthManager manager) =
    let
        diffInSeconds =
            (Time.posixToMillis timestamp - Time.posixToMillis manager.timestamp) // 1000
    in
    AuthManager
        { manager
            | timestamp = timestamp
            , tokens =
                manager.tokens
                    |> Dict.map (\_ auth -> { auth | lifespan = auth.lifespan - diffInSeconds })
                    |> Dict.filter (\_ auth -> auth.lifespan > 0)
        }


refreshAuth : Cmd (Result Error.Error Authentification)
refreshAuth =
    let
        parseAuthResult : Result Grpc.Error Proto.Gardevoir.AccessTokenData -> Result Error.Error Authentification
        parseAuthResult =
            Result.mapError Error.fromGrpcError
                >> Result.andThen (parseAccessTokenData >> Result.mapError Error.TokenDecodingFailed)
    in
    Grpc.new GardevoirService.refresh {}
        |> Grpc.toCmd parseAuthResult


decodeAuthentification : TsDecode.Decoder Authentification
decodeAuthentification =
    (TsDecode.succeed Tuple.pair
        |> TsDecode.required "access_token" TsDecode.string
        |> TsDecode.required "lifespan" TsDecode.int
    )
        |> TsDecode.andThen
            (TsDecode.andThenInit <|
                \( token, validForSeconds ) ->
                    case parseAccessTokenData { accessToken = token, validForSeconds = validForSeconds } of
                        Ok auth ->
                            TsDecode.succeed auth

                        Err _ ->
                            TsDecode.fail "todo add string conversion in pwt lib"
            )


logout : (Result Grpc.Error {} -> msg) -> Cmd msg
logout callback =
    Grpc.new GardevoirService.logout {}
        |> Grpc.toCmd callback


tokenToHeader : String -> Http.Header
tokenToHeader token =
    Http.header "authorization" token


tokenToHeaderTuple : String -> ( String, String )
tokenToHeaderTuple token =
    ( "authorization", token )


type alias Permissions =
    List Permission.Permission


parseAccessTokenData : Proto.Gardevoir.AccessTokenData -> Result Protobuf.Token.Error Authentification
parseAccessTokenData tokenData =
    Protobuf.Token.decode Proto.Gardevoir.decodeAccessTokenClaims tokenData.accessToken
        |> Result.map (.claims >> decodedTokenToAuth tokenData)


decodedTokenToAuth : Proto.Gardevoir.AccessTokenData -> Proto.Gardevoir.AccessTokenClaims -> Authentification
decodedTokenToAuth { accessToken, validForSeconds } claims =
    { token = accessToken
    , lifespan = validForSeconds
    , userId = tag claims.userId
    , mapId = claims.mapId |> Maybe.map tag
    , permissions = claims.permissions
    }


{-| Users always have a mapId except when they are first created and still need to choose a pokemon
and a starting location. We can get away not treating this as a special case since a token with no mapId will never
overwrite a token with a mapId.
-}
noMapIdDefault : MapId
noMapIdDefault =
    tag <| Int64.fromInts 0 0
