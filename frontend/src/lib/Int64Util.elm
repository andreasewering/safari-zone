module Int64Util exposing (int64ToUrlSegment, parseInt64FromUrl, urlCodec, urlParser)

import Base64
import Bytes
import Bytes.Decode
import Bytes.Encode
import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Url.Parser
import UrlCodec exposing (UrlCodec)


urlParser : Url.Parser.Parser (Int64 -> b) b
urlParser =
    Url.Parser.custom "Int64" parseInt64FromUrl


urlCodec : UrlCodec (Int64 -> b) b Int64
urlCodec =
    UrlCodec.custom "Int64" { parse = parseInt64FromUrl, build = int64ToUrlSegment }


int64ToUrlSegment : Int64 -> String
int64ToUrlSegment int64 =
    let
        ( high, low ) =
            Int64.toInts int64

        bytes =
            Bytes.Encode.encode
                (Bytes.Encode.sequence
                    [ Bytes.Encode.unsignedInt32 Bytes.BE high
                    , Bytes.Encode.unsignedInt32 Bytes.BE low
                    ]
                )

        base64 =
            Base64.fromBytes bytes |> Maybe.withDefault ""

        urlSafeBase64 =
            String.foldr
                (\char acc ->
                    case char of
                        '/' ->
                            String.cons '_' acc

                        '+' ->
                            String.cons '-' acc

                        '=' ->
                            acc

                        other ->
                            String.cons other acc
                )
                ""
                base64
    in
    urlSafeBase64


parseInt64FromUrl : String -> Maybe Int64
parseInt64FromUrl urlSafeBase64 =
    let
        base64 =
            String.foldr
                (\char acc ->
                    case char of
                        '_' ->
                            String.cons '/' acc

                        '-' ->
                            String.cons '+' acc

                        other ->
                            String.cons other acc
                )
                ""
                urlSafeBase64

        bytes =
            Base64.toBytes base64

        int64Decoder =
            Bytes.Decode.map2 Int64.fromInts (Bytes.Decode.unsignedInt32 Bytes.BE) (Bytes.Decode.unsignedInt32 Bytes.BE)
    in
    bytes |> Maybe.andThen (Bytes.Decode.decode int64Decoder)
