module Atoms.Button exposing (..)

import Atoms.Html
import Atoms.Loading exposing (loadingIndicator)
import Html.WithContext exposing (Attribute, Html)
import Html.WithContext.Attributes exposing (class, disabled)
import Html.WithContext.Events


button :
    { onPress : Maybe msg
    , label : Html { ctx | disabled : Bool } msg
    , loading : Bool
    }
    -> List (Attribute { ctx | disabled : Bool } msg)
    -> Html { ctx | disabled : Bool } msg
button { onPress, label, loading } extraAttrs =
    let
        color =
            if loading then
                class "bg-gray-400 dark:bg-gray-600"

            else
                class "bg-red-600"
    in
    Html.WithContext.button
        ([ case onPress of
            Just handler ->
                Html.WithContext.Events.onClick handler

            Nothing ->
                disabled True
         , class "relative h-fit rounded-md px-4 py-2 text-white transition-colors"
         , color
         ]
            ++ extraAttrs
        )
        [ if loading then
            loadingIndicator

          else
            Atoms.Html.none
        , label
        ]
