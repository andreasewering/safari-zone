module Atoms.Icon exposing (icon, iconWith)

import Html.WithContext as Html exposing (Attribute, Html)
import Html.WithContext.Attributes as Attributes


icon :
    { name : String, height : Int, width : Int, description : i18n -> String }
    -> Html { ctx | i18n : i18n } msg
icon options =
    iconWith options []


iconWith :
    { name : String, height : Int, width : Int, description : i18n -> String }
    -> List (Attribute { ctx | i18n : i18n } msg)
    -> Html { ctx | i18n : i18n } msg
iconWith { name, height, width, description } extraAttrs =
    Html.img
        ([ Attributes.width width
         , Attributes.height height
         , Attributes.src <| nameToSrc name
         , Html.withContextAttribute <| \{ i18n } -> Attributes.alt (description i18n)
         ]
            ++ extraAttrs
        )
        []


nameToSrc : String -> String
nameToSrc name =
    "/icons/" ++ name ++ ".svg"
