module Atoms.Html exposing
    ( a
    , alt
    , button
    , disable
    , disabled
    , formatTime
    , image
    , input
    , noTabindex
    , none
    , placeholder
    , text
    , translatedHtml
    , truncatedText
    )

import DateFormat
import Html
import Html.WithContext exposing (Attribute, Html)
import Html.WithContext.Attributes
import Time


text : (i18n -> String) -> Html.WithContext.Html { ctx | i18n : i18n } msg
text key =
    Html.WithContext.withContext
        (\{ i18n } ->
            let
                value =
                    key i18n
            in
            -- value is empty if the translations are still loading
            -- We use a zero-width whitespace as a replacement, since that will
            -- still take up one line of vertical space, resulting in less content-shift.
            if value == "" then
                Html.WithContext.text "\u{200B}"

            else
                Html.WithContext.text value
        )


truncatedText : String -> Html.WithContext.Html ctx msg
truncatedText str =
    [ Html.WithContext.text str ]
        |> Html.WithContext.div
            [ Html.WithContext.Attributes.class "w-full flex-auto overflow-hidden text-ellipsis whitespace-nowrap"
            ]


placeholder : (i18n -> String) -> Html.WithContext.Attribute { ctx | i18n : i18n } msg
placeholder key =
    Html.WithContext.withContextAttribute (\{ i18n } -> key i18n |> Html.WithContext.Attributes.placeholder)


alt : (i18n -> String) -> Html.WithContext.Attribute { ctx | i18n : i18n } msg
alt key =
    Html.WithContext.withContextAttribute <| \{ i18n } -> Html.WithContext.Attributes.alt (key i18n)


none : Html ctx msg
none =
    Html.WithContext.text ""


formatTime : List DateFormat.Token -> Time.Posix -> Html { ctx | timezone : Time.Zone } msg
formatTime tokens time =
    Html.WithContext.withContext (\{ timezone } -> DateFormat.format tokens timezone time |> Html.WithContext.text)


disable : Bool -> Html.WithContext.Html { ctx | disabled : Bool } msg -> Html.WithContext.Html { ctx | disabled : Bool } msg
disable disabled_ =
    withContext (\ctx -> { ctx | disabled = disabled_ })


button :
    List (Attribute { ctx | disabled : Bool } msg)
    -> List (Html { ctx | disabled : Bool } msg)
    -> Html { ctx | disabled : Bool } msg
button attrs =
    Html.WithContext.button (disabled :: attrs)


input :
    List (Html.WithContext.Attribute { ctx | disabled : Bool } msg)
    -> List (Html.WithContext.Html { ctx | disabled : Bool } msg)
    -> Html.WithContext.Html { ctx | disabled : Bool } msg
input attrs =
    Html.WithContext.input (disabled :: attrs)


a :
    List (Attribute { ctx | disabled : Bool } msg)
    -> List (Html { ctx | disabled : Bool } msg)
    -> Html { ctx | disabled : Bool } msg
a attrs =
    Html.WithContext.a (noTabindex :: attrs)


translatedHtml :
    List (Html.WithContext.Attribute { ctx | i18n : i18n } msg)
    -> (i18n -> List (Html.Html Never))
    -> Html.WithContext.Html { ctx | i18n : i18n } msg
translatedHtml attrs key =
    Html.WithContext.withContext
        (.i18n
            >> key
            >> List.map (Html.map never >> Html.WithContext.html)
            >> Html.WithContext.p attrs
        )


image :
    List (Html.WithContext.Attribute { ctx | i18n : i18n } msg)
    -> { description : i18n -> String, src : String }
    -> Html.WithContext.Html { ctx | i18n : i18n } msg
image attrs { description, src } =
    Html.WithContext.withContext
        (\{ i18n } ->
            Html.WithContext.img
                ([ Html.WithContext.Attributes.alt (description i18n)
                 , Html.WithContext.Attributes.src src
                 ]
                    ++ attrs
                )
                []
        )


disabled : Html.WithContext.Attribute { ctx | disabled : Bool } msg
disabled =
    Html.WithContext.withContextAttribute <|
        \ctx -> Html.WithContext.Attributes.disabled ctx.disabled


noTabindex : Attribute { ctx | disabled : Bool } msg
noTabindex =
    Html.WithContext.withContextAttribute <|
        \ctx ->
            Html.WithContext.Attributes.tabindex <|
                if ctx.disabled then
                    -1

                else
                    0


withContext : (a -> b) -> Html.WithContext.Html b msg -> Html.WithContext.Html a msg
withContext f hb =
    Html.WithContext.withContext
        (\ca ->
            let
                cb =
                    f ca
            in
            Html.WithContext.html <| Html.WithContext.toHtml cb hb
        )
