module Atoms.MapPreview exposing (..)

import Camera
import Data.Units exposing (TileUnit(..))
import Domain.Coordinate as Coordinate exposing (Coordinate)
import Html.Attributes
import Html.WithContext
import Textures.Store exposing (Sprite, Tilemaps)
import WebGL
import WebGL.Map
import WebGL.Texture as Texture


image :
    { mapSize : { height : Int, width : Int }, center : Coordinate Int, zoom : Float }
    -> Sprite
    -> Tilemaps
    -> Html.WithContext.Html ctx msg
image { mapSize, center, zoom } tileset tilemaps =
    let
        layersInTotal =
            List.length tilemaps.layers

        player =
            Coordinate.map (toFloat >> TileUnit) center

        offset =
            Coordinate.map (toFloat >> TileUnit) tilemaps.offset

        tilemapSize =
            Texture.size tilemaps.texture |> (\( x, y ) -> { x = TileUnit <| toFloat x, y = TileUnit <| toFloat y })
    in
    List.indexedMap
        (\layerIndex _ ->
            WebGL.Map.entity
                (Camera.mkCamera player mapSize { offset = offset, tileMapSize = tilemapSize })
                zoom
                tilemaps.offset
                tileset.texture
                tilemaps.texture
                layersInTotal
                layerIndex
        )
        tilemaps.layers
        |> WebGL.toHtml [ Html.Attributes.width mapSize.width, Html.Attributes.height mapSize.height ]
        |> Html.WithContext.html
