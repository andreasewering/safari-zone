module Atoms.Modal exposing (modal)

import Html.WithContext exposing (Html, div)
import Html.WithContext.Attributes exposing (class)
import Html.WithContext.Events exposing (onClick, stopPropagationOn)
import Json.Decode


modal : { noOp : msg, onClose : msg } -> List (Html ctx msg) -> Html ctx msg
modal { noOp, onClose } contents =
    div
        [ class "fixed left-0 top-0 z-10 flex h-screen w-screen max-w-full items-center justify-center backdrop-blur-sm"
        , onClick onClose
        ]
        [ div
            [ class "flex flex-col items-center gap-4 rounded-lg bg-gray-300 p-4 dark:bg-gray-700"
            , onClickStopPropagation noOp
            ]
            contents
        ]


onClickStopPropagation : msg -> Html.WithContext.Attribute ctx msg
onClickStopPropagation msg =
    stopPropagationOn "click" <| Json.Decode.succeed ( msg, True )
