module Atoms.Select exposing (view)

import Atoms.Html
import Html.WithContext exposing (Html, div, option, select)
import Html.WithContext.Attributes as Attributes exposing (class)
import Html.WithContext.Events as Events
import List.NonEmpty as NonEmpty exposing (NonEmpty)


view :
    { onSelect : a -> msg
    , options : NonEmpty ( a, String )
    , selectedOption : Maybe a
    , label : i18n -> String
    , id : String
    }
    -> Html { ctx | disabled : Bool, i18n : i18n } msg
view { onSelect, options, selectedOption, id, label } =
    let
        renderOption ( optA, optValue ) =
            option
                [ Attributes.value optValue
                , Attributes.selected (Just optA == selectedOption)
                ]
                [ Html.WithContext.text optValue ]

        optionForValue value =
            NonEmpty.filter (\( _, v ) -> value == v) options
                |> Maybe.map NonEmpty.head
                |> Maybe.withDefault (NonEmpty.head options)
                |> Tuple.first
    in
    div [ class "relative mt-3" ]
        [ NonEmpty.map renderOption options
            |> NonEmpty.toList
            |> select [ Events.onInput (optionForValue >> onSelect), Atoms.Html.disabled, Attributes.id id, class "peer" ]
        , Html.WithContext.label [ Attributes.for id, class "absolute left-0 -translate-y-4 text-xs peer-focus:text-sky-700 dark:peer-focus:text-sky-400" ] [ Atoms.Html.text label ]
        ]
