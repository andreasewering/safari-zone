module Atoms.Input exposing (slider, text)

import Atoms.Html exposing (input, placeholder)
import Html.WithContext exposing (div, label)
import Html.WithContext.Attributes as Attributes exposing (class, for, id, value)
import Html.WithContext.Events


text :
    { id : String
    , onChange : String -> msg
    , label : i18n -> String
    , text : String
    , error : Maybe (i18n -> String)
    }
    -> Html.WithContext.Html { ctx | i18n : i18n, disabled : Bool } msg
text props =
    div [ class "relative mt-3" ]
        [ input
            [ class "peer w-full appearance-none border-b border-gray-500 bg-transparent px-0 py-1 placeholder:text-transparent focus:border-sky-700 focus:outline-none dark:border-gray-300 dark:text-white dark:focus:border-sky-400"
            , placeholder props.label
            , id props.id
            , Html.WithContext.Events.onInput props.onChange
            , Attributes.value props.text
            ]
            []
        , label [ for props.id, class "absolute left-0 -translate-y-4 bg-transparent text-xs text-gray-700 duration-100 ease-linear peer-placeholder-shown:translate-y-1 peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-500 peer-focus:-translate-y-4 peer-focus:text-xs peer-focus:text-sky-700 dark:text-gray-300 dark:peer-placeholder-shown:text-gray-400 dark:peer-focus:text-sky-400" ]
            [ Atoms.Html.text props.label ]

        , case props.error of
            Just error ->
                div [ class "mt-1 text-xs text-red-600", Attributes.attribute "role" "alert", Attributes.attribute "aria-live" "polite" ] [ Atoms.Html.text error ]
            Nothing ->
                Atoms.Html.none
        ]


slider :
    { id : String
    , onChange : Float -> msg
    , label : i18n -> String
    , value : Float
    , min : Float
    , max : Float
    , step : Float
    }
    -> Html.WithContext.Html { ctx | i18n : i18n, disabled : Bool } msg
slider props =
    input
        [ Attributes.type_ "range"
        , Attributes.min <| String.fromFloat props.min
        , Attributes.max <| String.fromFloat props.max
        , Attributes.step <| String.fromFloat props.step
        , value <| String.fromFloat props.value
        , Html.WithContext.Events.onInput (String.toFloat >> Maybe.withDefault props.min >> props.onChange)
        ]
        [ label [] [ Atoms.Html.text props.label ] ]
