module Atoms.Loading exposing (loadingIndicator)

import Html.WithContext


loadingIndicator : Html.WithContext.Html ctx msg
loadingIndicator =
    Html.WithContext.node "loading-indicator" [] []
