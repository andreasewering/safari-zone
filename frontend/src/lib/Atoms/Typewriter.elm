module Atoms.Typewriter exposing (typewriter)

import Html.WithContext as Html exposing (Attribute, Html)
import Html.WithContext.Attributes exposing (attribute, class)
import Html.WithContext.Events as Events
import Json.Decode as Decode


typewriter :
    { text : i18n -> String, speed : Int, lines : Int, onFinish : msg }
    -> List (Attribute { ctx | i18n : i18n } msg)
    -> Html { ctx | i18n : i18n } msg
typewriter config extraAttrs =
    Html.node "type-writer"
        ([ class "w-full bg-gray-300 p-2 dark:bg-gray-800"
         , Html.withContextAttribute <| \{ i18n } -> attribute "text" (config.text i18n)
         , attribute "speed" (String.fromInt config.speed)
         , attribute "lines" (String.fromInt config.lines)
         , Events.on "animationfinished" (Decode.succeed config.onFinish)
         ]
            ++ extraAttrs
        )
        []
