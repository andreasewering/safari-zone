module Camera exposing (Camera, calcScreenCoord, calcTileOffset, calcWebGLCoord, getPlayerCoord, getScreenHeight, getScreenWidth, mkCamera, zoom)

import Config
import Data.Units exposing (TileUnit(..), WebGLUnit(..), unTileUnit)
import Domain.Coordinate as Coordinate exposing (Coordinate)



{-
   There are three different coordinate systems we have to work with here.
   This module attempts to make it easy to distinguish them.

   1. Screen Pixels. Something like 1200x800. Is relevant because we want to render entities at consistent sizes regardless of screen size.
      All entities have a size given in pixels.

   2. Tile Coordinates. Something like 12x15. Is relevant because players and pokemon cannot stop between tiles. Also moveability is determined
     on tile basis.

   3. WebGL Canvas Coordinates. Something like 0.6x0.3. Is relevant because this is how the graphics are rendered in the end. Contrary to
     the other 2 coordinte systems, 0x0 is not in the top left but in the middle. Goes from -1 to +1 in both dimensions,
     x from left to right, y from down to up.

   The camera is always centered on the player unless the player approaches the edge of the map.
   Then, the camera stops following the player until he leaves the edge again.
-}


type alias OneDCamera =
    { player : TileUnit
    , screenSizePx : Int
    , tileMapSize : TileUnit
    , offset : TileUnit
    }


type alias Camera =
    Coordinate OneDCamera


mkCamera : Coordinate TileUnit -> { session | width : Int, height : Int } -> { tileMapSize : Coordinate TileUnit, offset : Coordinate TileUnit } -> Camera
mkCamera player { height, width } { tileMapSize, offset } =
    { x = { player = player.x, screenSizePx = width, tileMapSize = tileMapSize.x, offset = offset.x }
    , y = { player = player.y, screenSizePx = height, tileMapSize = tileMapSize.y, offset = offset.y }
    }


zoom : Float -> Camera -> Camera
zoom scale =
    Coordinate.map
        (\{ player, screenSizePx, tileMapSize, offset } ->
            { player = unTileUnit player * scale |> TileUnit
            , screenSizePx = toFloat screenSizePx * scale |> round
            , tileMapSize = unTileUnit tileMapSize * scale |> TileUnit
            , offset = unTileUnit offset * scale |> TileUnit
            }
        )


getPlayerCoord : Camera -> Coordinate TileUnit
getPlayerCoord =
    Coordinate.map .player


calcTileOffset : Camera -> Coordinate TileUnit
calcTileOffset =
    Coordinate.map calcTileOffset1d


getScreenWidth : Camera -> Int
getScreenWidth =
    .x >> .screenSizePx


getScreenHeight : Camera -> Int
getScreenHeight =
    .y >> .screenSizePx


calcWebGLCoord : Camera -> Coordinate TileUnit -> Coordinate WebGLUnit
calcWebGLCoord camera tile =
    let
        tileOffset =
            calcTileOffset camera

        withOffset =
            { x = addTile tileOffset.x tile.x, y = addTile tileOffset.y tile.y }
    in
    { x = tileToWebGL camera.x.screenSizePx withOffset.x
    , y = tileToWebGL camera.y.screenSizePx withOffset.y |> invertWebGL
    }


calcScreenCoord : Camera -> Coordinate TileUnit -> Coordinate Float
calcScreenCoord camera tile =
    let
        tileOffset =
            calcTileOffset camera

        withOffset =
            { x = addTile tileOffset.x tile.x, y = addTile tileOffset.y tile.y }
    in
    { x = Config.tileSize * unTileUnit withOffset.x, y = Config.tileSize * unTileUnit withOffset.y }


tileToWebGL : Int -> TileUnit -> WebGLUnit
tileToWebGL screenSizePx (TileUnit tu) =
    let
        tilesOnScreen =
            toFloat screenSizePx / Config.tileSize
    in
    WebGLUnit <| 2 * tu / tilesOnScreen - 1


calcTileOffset1d : OneDCamera -> TileUnit
calcTileOffset1d { player, screenSizePx, tileMapSize, offset } =
    let
        tilesOnScreen =
            toFloat screenSizePx / Config.tileSize

        (TileUnit off) =
            offset

        (TileUnit pTile) =
            player

        (TileUnit mapSize) =
            tileMapSize

        tileOffset =
            clamp (tilesOnScreen - mapSize - 2) (1 - off) (tilesOnScreen / 2 - pTile)
    in
    TileUnit tileOffset


addTile : TileUnit -> TileUnit -> TileUnit
addTile (TileUnit t1) (TileUnit t2) =
    TileUnit (t1 + t2)


invertWebGL : WebGLUnit -> WebGLUnit
invertWebGL (WebGLUnit w) =
    WebGLUnit -w
