/**
 * Utility to wrap a type into a lazy container, which runs the initialization function
 * the first time the variable is actually used.
 */
export class Lazy<T> {
  #init: () => T;
  #value: T | undefined;

  get value(): T {
    if (this.#value !== undefined) {
      return this.#value;
    }
    this.#value = this.#init();
    return this.#value;
  }

  constructor(init: () => T) {
    this.#init = init;
  }

  derive<U>(f: (t: T) => U): Lazy<U> {
    return new Lazy(() => f(this.value));
  }
}

export function lazy<T>(init: () => T): Lazy<T> {
  return new Lazy(init);
}
