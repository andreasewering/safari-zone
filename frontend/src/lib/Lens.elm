module Lens exposing (Lens, compose, dict, set, update)

import Dict exposing (Dict)



-- Very basic lens implementation for composition purposes
-- Do not use this for performance critical sections, but instead
-- boilerplate heavy code that benefits from this API.


type alias Lens a b =
    { get : b -> a, set : a -> b -> b }


update : Lens a b -> (a -> a) -> b -> b
update lens f b =
    lens.set (f (lens.get b)) b


set : Lens a b -> b -> a -> b
set lens b a =
    lens.set a b


dict : comparable -> Lens (Maybe v) (Dict comparable v)
dict k =
    { get = Dict.get k, set = always >> Dict.update k }


compose : Lens a b -> Lens b c -> Lens a c
compose aInB bInC =
    Lens (bInC.get >> aInB.get)
        (\a c ->
            let
                b =
                    aInB.set a (bInC.get c)
            in
            bInC.set b c
        )
