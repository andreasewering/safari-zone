module EnumSet exposing (EnumSet(..), contains, fromInt, fromList, intToBits, intersect, isEmpty, map, toList)

import Bitwise
import EnumSetMembers exposing (EnumSetMembers)


type EnumSet a
    = EnumSet Int


fromInt : Int -> EnumSet a
fromInt =
    EnumSet


fromList : EnumSetMembers a -> List a -> EnumSet a
fromList members actualMembers =
    List.foldl
        (\member ((EnumSet repr) as set) ->
            case EnumSetMembers.getIndex member members of
                Just index ->
                    EnumSet (setBit index repr)

                Nothing ->
                    set
        )
        (fromInt 0)
        actualMembers


contains : EnumSetMembers a -> a -> EnumSet a -> Bool
contains members member (EnumSet repr) =
    EnumSetMembers.getIndex member members
        |> Maybe.map (\index -> bitIsSet index repr)
        |> Maybe.withDefault False


{-| Intersect two `EnumSet`s.
-}
intersect : EnumSet a -> EnumSet a -> EnumSet a
intersect (EnumSet repr1) (EnumSet repr2) =
    EnumSet (Bitwise.and repr1 repr2)


isEmpty : EnumSet a -> Bool
isEmpty (EnumSet repr) =
    repr == 0


toList : EnumSetMembers a -> EnumSet a -> List a
toList members (EnumSet repr) =
    let
        rec indexedMembers leftoverBits lastIterationIndex =
            case indexedMembers of
                [] ->
                    []

                ( firstIndex, firstMember ) :: restMembers ->
                    case List.drop (firstIndex - lastIterationIndex) leftoverBits of
                        firstBit :: restBits ->
                            (if firstBit then
                                (::) firstMember

                             else
                                identity
                            )
                                (rec restMembers restBits (firstIndex + 1))

                        [] ->
                            []
    in
    rec (EnumSetMembers.toIndexedList members) (intToBits repr) 0


map : (a -> b) -> EnumSetMembers a -> EnumSetMembers b -> EnumSet a -> EnumSet b
map f membersA membersB setA =
    toList membersA setA
        |> List.map f
        |> fromList membersB


bitIsSet : Int -> Int -> Bool
bitIsSet index m =
    Bitwise.and 1 (Bitwise.shiftRightBy index m) == 1


setBit : Int -> Int -> Int
setBit index m =
    Bitwise.or m (Bitwise.shiftLeftBy index 1)


intToBits : Int -> List Bool
intToBits number =
    let
        -- Helper function to recursively extract bits
        intToBoolBitsHelper : Int -> Int -> List Bool -> List Bool
        intToBoolBitsHelper num bitCount bits =
            if bitCount == 0 then
                bits

            else
                let
                    leastSignificantBit =
                        Bitwise.and num 1

                    isBitSet =
                        leastSignificantBit == 1

                    remainingBits =
                        Bitwise.shiftRightBy 1 num
                in
                intToBoolBitsHelper remainingBits (bitCount - 1) (isBitSet :: bits)
    in
    List.reverse (intToBoolBitsHelper number 32 [])
