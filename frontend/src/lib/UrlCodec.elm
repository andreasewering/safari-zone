module UrlCodec exposing
    ( UrlCodec, top, s, string, int, custom
    , chain, andThen
    , union, variant0, variant1, variant2, buildUnion
    , parseUrl, buildUrl
    , map, qString, variant3
    )

{-| A bidirectional mapping between elm/url and a custom data type of your choice.
Combines a `Url.Parser.Parser` and a `Route -> String` function to make parse/stringify operations automatically inverses of each other.


# Basic UrlCodecs

@docs UrlCodec, top, s, string, int, custom


# Chaining Codecs

@docs chain, andThen


# Handling Union Types

@docs union, variant0, variant1, variant2, buildUnion


# Using the UrlCodec

@docs parseUrl, buildUrl

-}

import Dict exposing (Dict)
import List
import Url exposing (Url)
import Url.Builder
import Url.Parser exposing ((</>))
import Url.Parser.Query


type UrlCodec a b c
    = UrlCodec
        { parser : Url.Parser.Parser a b
        , builder : c -> UrlParts
        }


type alias UrlParts =
    { pathSegments : List String
    , queryParams : Dict String (List String)
    , fragment : String
    }


emptyUrl : UrlParts
emptyUrl =
    { pathSegments = [], queryParams = Dict.empty, fragment = "" }


append : UrlParts -> UrlParts -> UrlParts
append p1 p2 =
    { pathSegments = p1.pathSegments ++ p2.pathSegments
    , queryParams =
        Dict.merge Dict.insert
            (\k a b -> Dict.insert k (a ++ b))
            Dict.insert
            p1.queryParams
            p2.queryParams
            Dict.empty
    , fragment = p1.fragment ++ p2.fragment
    }


parseUrl : UrlCodec (a -> a) a a -> Url -> Maybe a
parseUrl (UrlCodec codec) url =
    Url.Parser.parse codec.parser url


buildUrl : UrlCodec a b c -> c -> String
buildUrl (UrlCodec codec) a =
    let
        { pathSegments, queryParams, fragment } =
            codec.builder a

        builderQueryParams =
            Dict.foldr (\key values acc -> List.map (Url.Builder.string key) values ++ acc) [] queryParams

        nonEmptyFragment =
            if String.isEmpty fragment then
                Nothing

            else
                Just fragment
    in
    Url.Builder.custom Url.Builder.Absolute pathSegments builderQueryParams nonEmptyFragment


top : UrlCodec a a c
top =
    UrlCodec { parser = Url.Parser.top, builder = \_ -> emptyUrl }


s : String -> UrlCodec a a c
s pathSegment =
    UrlCodec
        { parser = Url.Parser.s pathSegment
        , builder = \_ -> { emptyUrl | pathSegments = [ pathSegment ] }
        }


custom : String -> { parse : String -> Maybe a, build : a -> String } -> UrlCodec (a -> b) b a
custom name { parse, build } =
    UrlCodec
        { parser = Url.Parser.custom name parse
        , builder = \a -> { emptyUrl | pathSegments = [ build a ] }
        }


string : UrlCodec (String -> b) b String
string =
    UrlCodec
        { parser = Url.Parser.string
        , builder = \pathSegment -> { emptyUrl | pathSegments = [ pathSegment ] }
        }


map : a -> UrlCodec a b c -> UrlCodec (b -> d) d c
map a (UrlCodec codec) =
    UrlCodec
        { parser = Url.Parser.map a codec.parser
        , builder = codec.builder
        }


qString : String -> UrlCodec (Maybe String -> b) b (Maybe String)
qString key =
    UrlCodec
        { parser = Url.Parser.query (Url.Parser.Query.string key)
        , builder =
            \mayValue ->
                case mayValue of
                    Just value ->
                        { emptyUrl | queryParams = Dict.singleton key [ value ] }

                    Nothing ->
                        emptyUrl
        }


int : UrlCodec (Int -> b) b Int
int =
    custom "Int" { parse = String.toInt, build = String.fromInt }


chain : UrlCodec a b c -> UrlCodec d a c -> UrlCodec d b c
chain (UrlCodec c1) (UrlCodec c2) =
    UrlCodec
        { parser = c2.parser </> c1.parser
        , builder =
            \z ->
                append (c2.builder z) (c1.builder z)
        }


andThen : UrlCodec b c x -> UrlCodec a b y -> UrlCodec a c ( y, x )
andThen (UrlCodec c1) (UrlCodec c2) =
    UrlCodec
        { parser = c2.parser </> c1.parser
        , builder =
            \( y, x ) ->
                append (c2.builder y) (c1.builder x)
        }


type CustomCodec match a b
    = CustomCodec
        { match : match
        , parser : List (Url.Parser.Parser a b)
        }


union : match -> CustomCodec match a b
union match =
    CustomCodec
        { match = match
        , parser = []
        }


variant0 :
    a
    -> UrlCodec a b a
    -> CustomCodec (UrlParts -> c) (b -> d) d
    -> CustomCodec c (b -> d) d
variant0 ctor (UrlCodec codec) (CustomCodec am) =
    CustomCodec
        { match = am.match (codec.builder ctor)
        , parser = Url.Parser.map ctor codec.parser :: am.parser
        }


variant1 :
    a
    -> UrlCodec a b c
    -> CustomCodec ((c -> UrlParts) -> d) (b -> e) e
    -> CustomCodec d (b -> e) e
variant1 ctor (UrlCodec codec) (CustomCodec am) =
    CustomCodec
        { match = am.match codec.builder
        , parser = Url.Parser.map ctor codec.parser :: am.parser
        }


variant2 :
    a
    -> UrlCodec a b ( c, d )
    -> CustomCodec ((c -> d -> UrlParts) -> e) (b -> f) f
    -> CustomCodec e (b -> f) f
variant2 ctor (UrlCodec codec) (CustomCodec am) =
    CustomCodec
        { match = am.match (\c d -> codec.builder ( c, d ))
        , parser = Url.Parser.map ctor codec.parser :: am.parser
        }


variant3 :
    a
    -> UrlCodec a b ( ( c, d ), e )
    -> CustomCodec ((c -> d -> e -> UrlParts) -> f) (b -> g) g
    -> CustomCodec f (b -> g) g
variant3 ctor (UrlCodec codec) (CustomCodec am) =
    CustomCodec
        { match = am.match (\c d e -> codec.builder ( ( c, d ), e ))
        , parser = Url.Parser.map ctor codec.parser :: am.parser
        }


buildUnion : CustomCodec (c -> UrlParts) a b -> UrlCodec a b c
buildUnion (CustomCodec am) =
    UrlCodec
        { builder = \v -> am.match v
        , parser = Url.Parser.oneOf am.parser
        }
