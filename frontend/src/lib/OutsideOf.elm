module OutsideOf exposing (isOutsideOf)

import Json.Decode as Decode


isOutsideOf : String -> String -> Decode.Decoder ()
isOutsideOf property value =
    let
        rec p v =
            Decode.oneOf
                [ Decode.field p Decode.string
                    |> Decode.andThen
                        (\propValue ->
                            if propValue == v then
                                -- found match
                                Decode.succeed False

                            else
                                -- try next decoder
                                Decode.fail "continue"
                        )
                , Decode.lazy
                    (\_ -> rec p v |> Decode.field "parentNode")

                -- fallback if all previous decoders failed
                , Decode.succeed True
                ]
    in
    Decode.field "target" (rec property value)
        |> Decode.andThen
            (\isOutside ->
                if isOutside then
                    Decode.succeed ()

                else
                    Decode.fail <| "inside target " ++ property ++ ":" ++ value
            )
