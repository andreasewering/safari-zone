module BoundedDict exposing (BoundedDict, get, insert, remove, update, withCapacity)

import List.Extra


type BoundedDict k v
    = BoundedDict { maxCapacity : Int, length : Int, entries : List ( k, v ) }


withCapacity : Int -> BoundedDict k v
withCapacity maxCapacity =
    BoundedDict { maxCapacity = maxCapacity, length = 0, entries = [] }


capacity : BoundedDict k v -> Int
capacity (BoundedDict dict) =
    dict.maxCapacity


get : k -> BoundedDict k v -> Maybe v
get k (BoundedDict dict) =
    List.Extra.find (\( dk, _ ) -> dk == k) dict.entries
        |> Maybe.map Tuple.second


insert : k -> v -> BoundedDict k v -> BoundedDict k v
insert k v bdict =
    remove k bdict
        |> prepend k v
        |> limitLength (capacity bdict)


update : k -> (Maybe v -> Maybe v) -> BoundedDict k v -> BoundedDict k v
update k f (BoundedDict dict) =
    let
        ( removed, kept ) =
            removeInternal k dict.entries

        updated =
            f removed
    in
    BoundedDict
        { dict
            | entries =
                case updated of
                    Just stillPresent ->
                        ( k, stillPresent ) :: kept

                    Nothing ->
                        kept
            , length = dict.length - lengthOfMaybe removed + lengthOfMaybe updated
        }
        |> limitLength dict.maxCapacity


remove : k -> BoundedDict k v -> BoundedDict k v
remove k (BoundedDict dict) =
    let
        ( removed, kept ) =
            removeInternal k dict.entries
    in
    BoundedDict
        { dict
            | entries = kept
            , length = dict.length - lengthOfMaybe removed
        }


limitLength : Int -> BoundedDict k v -> BoundedDict k v
limitLength n (BoundedDict dict) =
    let
        actualLength =
            min n dict.length
    in
    BoundedDict { dict | length = actualLength, entries = List.take actualLength dict.entries }


prepend : k -> v -> BoundedDict k v -> BoundedDict k v
prepend k v (BoundedDict dict) =
    BoundedDict { dict | length = dict.length + 1, entries = ( k, v ) :: dict.entries }


removeInternal : k -> List ( k, v ) -> ( Maybe v, List ( k, v ) )
removeInternal k list =
    let
        ( kept, removed ) =
            List.partition (\( dk, _ ) -> dk /= k) list
    in
    ( List.head removed |> Maybe.map Tuple.second, kept )


lengthOfMaybe : Maybe a -> Int
lengthOfMaybe maybe =
    case maybe of
        Just _ ->
            1

        Nothing ->
            0
