/* eslint-disable @typescript-eslint/consistent-type-definitions */
type BarcodeDetectorOptions = {
  formats: string[];
};

type DetectedBarcode = {
  rawValue: string;
  format: string;
  boundingBox: DOMRectReadOnly;
  cornerPoints: DOMPointReadOnly[];
};

declare class BarcodeDetector {
  constructor(options?: BarcodeDetectorOptions);

  static getSupportedFormats(): Promise<string[]>;

  detect(image: ImageBitmapSource): Promise<DetectedBarcode[]>;
}

type MediaStreamTrackProcessorOptions = {
  track: MediaStreamTrack;
};

type MediaStreamTrackGeneratorOptions = {
  kind: 'audio' | 'video';
};

declare class MediaStreamTrackProcessor<T> {
  constructor(options: MediaStreamTrackProcessorOptions);

  readonly readable: ReadableStream<T>;
}

declare class MediaStreamTrackGenerator<T> {
  constructor(options: MediaStreamTrackGeneratorOptions);

  readonly writable: WritableStream<T>;
  readonly kind: 'audio' | 'video';
}

declare global {
  interface MediaStream {
    addTrack(track: MediaStreamTrackGenerator<unknown>): void;
  }
  interface Window {
    BarcodeDetector: typeof BarcodeDetector;
    MediaStreamTrackProcessor?: typeof MediaStreamTrackProcessor;
    MediaStreamTrackGenerator?: typeof MediaStreamTrackGenerator;
  }
}

export class QRCodeScanner extends HTMLElement {
  #video!: HTMLVideoElement;
  #select!: HTMLSelectElement;
  #canvas!: OffscreenCanvas;
  #ctx!: OffscreenCanvasRenderingContext2D;
  #currentStream: MediaStream | undefined;
  #didManualCameraSelection = false;

  /**
   * @method connectedCallback
   * @description Lifecycle method called when the element is connected to the DOM.
   * Renders the DOM inside of this element.
   */
  async connectedCallback() {
    this.#video = document.createElement('video');
    this.#select = document.createElement('select');
    this.#canvas = new OffscreenCanvas(1, 1);
    // Should never be null since the error case mentions getting two different contexts for the same context
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.#ctx = this.#canvas.getContext('2d')!;
    this.appendChild(this.#video);
    this.appendChild(this.#select);

    const devices = await navigator.mediaDevices.enumerateDevices();
    this.listDevices(devices);

    this.#select.addEventListener('change', () => {
      this.#didManualCameraSelection = true;
      void this.scan();
    });

    await this.scan();
  }

  private listDevices(mediaDevices: MediaDeviceInfo[]) {
    this.#select.innerHTML = '';
    mediaDevices.forEach((mediaDevice, i) => {
      if (mediaDevice.kind === 'videoinput') {
        const option = document.createElement('option');
        option.selected = i === 0 ? true : false;
        option.value = mediaDevice.deviceId;
        option.textContent = mediaDevice.label || `Camera ${i.toString()}`;
        this.#select.append(option);
      }
    });
  }

  private async mediaStream(): Promise<MediaStream> {
    if (this.#currentStream) {
      for (const track of this.#currentStream.getTracks()) {
        track.stop();
      }
    }

    const constraints: MediaTrackConstraints =
      this.#select.value && this.#didManualCameraSelection
        ? { deviceId: { exact: this.#select.value } }
        : { facingMode: 'environment' };
    const stream = await navigator.mediaDevices.getUserMedia({
      video: constraints,
    });
    this.#currentStream = stream;
    return stream;
  }

  async scan() {
    const mediaStream = await this.mediaStream();
    const barcodeDetector = new BarcodeDetector({
      formats: ['qr_code'],
    });
    const videoTrack = mediaStream.getVideoTracks()[0];
    if (!videoTrack) {
      return;
    }
    const { width, height } = videoTrack.getSettings();
    if (!width || !height) {
      return;
    }
    this.#video.width = width;
    this.#video.height = height;
    this.#canvas.width = width;
    this.#canvas.height = height;
    this.#ctx.strokeStyle = 'red';
    this.#ctx.fillStyle = 'red';
    const trackProcessor = new MediaStreamTrackProcessor({ track: videoTrack });
    const trackGenerator = new MediaStreamTrackGenerator({ kind: 'video' });

    const highlightBarcode = this.highlightBarcode.bind(this);
    const transformer = new TransformStream<
      ImageBitmapSource & { timestamp: number; close: () => void }
    >({
      async transform(videoFrame, controller) {
        const bitmap = await createImageBitmap(videoFrame);
        const detectedBarcodes = await barcodeDetector.detect(bitmap);
        if (!detectedBarcodes.length) {
          bitmap.close();
          controller.enqueue(videoFrame);
          return;
        }
        const timestamp = videoFrame.timestamp;
        videoFrame.close();
        const newFrame = await highlightBarcode(
          bitmap,
          timestamp,
          detectedBarcodes
        );
        controller.enqueue(newFrame);
      },
      flush(controller) {
        controller.terminate();
      },
    });

    void trackProcessor.readable
      .pipeThrough(transformer)
      .pipeTo(trackGenerator.writable);

    const processedStream = new MediaStream();
    processedStream.addTrack(trackGenerator);
    this.#video.addEventListener('loadedmetadata', () => {
      void this.#video.play();
    });
    this.#video.srcObject = processedStream;
  }

  async highlightBarcode(
    bitmap: ImageBitmap,
    timestamp: number,
    detectedBarcodes: DetectedBarcode[]
  ): Promise<VideoFrame> {
    const floor = Math.floor;
    this.#ctx.drawImage(bitmap, 0, 0, this.#canvas.width, this.#canvas.height);
    bitmap.close();
    detectedBarcodes.map((detectedBarcode) => {
      const { x, y, width, height } = detectedBarcode.boundingBox;
      this.#ctx.strokeRect(floor(x), floor(y), floor(width), floor(height));
      this.dispatchEvent(
        new CustomEvent('detect', { detail: detectedBarcode.rawValue })
      );
    });
    const newBitmap = await createImageBitmap(this.#canvas);
    return new VideoFrame(newBitmap, { timestamp });
  }
}

customElements.define('qr-code-scanner', QRCodeScanner);
