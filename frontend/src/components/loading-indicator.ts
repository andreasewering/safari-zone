class LoadingIndicator extends HTMLElement {
  /**
   * @method connectedCallback
   * @description Lifecycle method called when the element is connected to the DOM.
   * Renders the DOM inside of this element.
   */
  connectedCallback() {
    this.innerHTML = `
<style>
.dot {
    width: 20%;
    aspect-ratio: 1 / 1;
    background-color: black;
    margin: 0 5%;
    border-radius: 50%;
    animation: bounce 1.4s infinite ease-in-out both;
}
    
.dot:nth-child(1) { animation-delay: -0.32s; background-color: rgb(200, 150, 150) }
.dot:nth-child(2) { animation-delay: -0.16s; background-color: rgb(210, 100, 100) }
.dot:nth-child(3) { animation-delay: 0s; background-color: rgb(220, 50, 50) }

@keyframes bounce {
    0%, 80%, 100% {
        transform: scale(0);
    }
    40% {
        transform: scale(1.0);
    }
}  
</style>
<div class="flex justify-center items-center absolute w-full h-full left-0 right-0 top-0 bottom-0" role="status" aria-label="Loading">
    <div class="dot"></div>
    <div class="dot"></div>
    <div class="dot"></div>
</div>    
    `;
  }
}

customElements.define('loading-indicator', LoadingIndicator);
