export function loadImage(src: string): Promise<CanvasImageSource> {
  const image = new Image();
  const p = new Promise<CanvasImageSource>((resolve, reject) => {
    image.onload = () => {
      resolve(image);
    };
    image.onerror = reject;
  });
  image.src = src;
  return p;
}
