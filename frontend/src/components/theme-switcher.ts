import { getCurrentThemeName, saveThemeSelection, Theme } from './theme.js';

/**
 * WebComponent for switching between light and dark theme.
 * Usage: `<theme-switcher></theme-switcher>`
 */
class ThemeSwitcher extends HTMLElement {
  #selectedTheme: Theme = getCurrentThemeName();

  /**
   * @method toggle
   * @description Toggles the theme between light and dark.
   */
  toggle() {
    const newTheme = this.#selectedTheme === 'dark' ? 'light' : 'dark';

    document.documentElement.setAttribute('theme', newTheme);

    saveThemeSelection(newTheme);
    this.#selectedTheme = newTheme;
    this.dispatchEvent(
      new CustomEvent<Theme>('theme-change', { detail: newTheme })
    );
  }

  /**
   * @method connectedCallback
   * @description Lifecycle method called when the element is connected to the DOM.
   * Renders the DOM inside of this element.
   */
  connectedCallback() {
    this.innerHTML = `
<div class="w-20 h-12 rounded-full border-[0.25rem] border-solid relative transition-all duration-300 bg-sky-400 border-sky-500 dark:bg-sky-900 dark:border-sky-700">
    <div
        class="w-10 h-10 rounded-full absolute p-1 left-0 transition-all duration-300 bg-sky-300 dark:bg-sky-800 dark:left-8"
        role="button"
        tabindex="0"
    >
        <svg
        class="absolute transition-all duration-150 dark:opacity-0 dark:delay-150"
        id="sun"
        fill="#FFFFFF"
        height="2rem"
        width="2rem"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        viewBox="0 0 485.34 485.34"
        xml:space="preserve"
        >
        <path fill="#FFFF00" d=""></path>
        <circle cx="150" cy="200" r="150" fill="#FFFF00" />
        <!-- d="M89.472,275.122c-0.08-0.637-0.049-1.274-0.112-1.91c-33.377-15.01-56.732-48.514-56.732-87.418 c0-52.846,42.999-95.844,95.846-95.844c30.397,0,57.496,14.274,75.07,36.435c6.658-1.019,13.429-1.705,20.375-1.705 c6.149,0,12.203,0.557,18.21,1.385c-21.539-40.815-64.363-68.743-113.655-68.743C57.624,57.322,0,114.946,0,185.794 c0,47.779,26.271,89.471,65.064,111.602C71.932,288.776,80.072,281.144,89.472,275.122z" -->
        <path
            d="M429.945,292.87c1.18-5.226,1.912-10.611,1.912-16.202c0-40.784-33.074-73.843-73.843-73.843 c-15.422,0-29.73,4.747-41.581,12.84c-16.537-34.49-51.699-58.356-92.516-58.356c-56.717,0-102.68,45.978-102.68,102.679 c0,11.917,2.119,23.291,5.863,33.934c-29.489,7.535-51.379,34.046-51.379,65.878c0,37.677,30.556,68.219,68.219,68.219h273.18 c37.678,0,68.219-30.542,68.219-68.219C485.34,326.516,461.475,298.892,429.945,292.87z"
        ></path>
        </svg>
        <svg
        class="absolute transition-all duration-150 opacity-0 delay-150 dark:opacity-100 dark:delay-0"
        id="moon"
        viewBox="0 0 24 24"
        fill="#FFFFFF"
        width="2rem"
        height="2rem"
        xmlns="http://www.w3.org/2000/svg"
        >
        <path
            d="M19.9001 2.30719C19.7392 1.8976 19.1616 1.8976 19.0007 2.30719L18.5703 3.40247C18.5212 3.52752 18.4226 3.62651 18.298 3.67583L17.2067 4.1078C16.7986 4.26934 16.7986 4.849 17.2067 5.01054L18.298 5.44252C18.4226 5.49184 18.5212 5.59082 18.5703 5.71587L19.0007 6.81115C19.1616 7.22074 19.7392 7.22074 19.9001 6.81116L20.3305 5.71587C20.3796 5.59082 20.4782 5.49184 20.6028 5.44252L21.6941 5.01054C22.1022 4.849 22.1022 4.26934 21.6941 4.1078L20.6028 3.67583C20.4782 3.62651 20.3796 3.52752 20.3305 3.40247L19.9001 2.30719Z"
        ></path>
        <path
            d="M16.0328 8.12967C15.8718 7.72009 15.2943 7.72009 15.1333 8.12967L14.9764 8.52902C14.9273 8.65407 14.8287 8.75305 14.7041 8.80237L14.3062 8.95987C13.8981 9.12141 13.8981 9.70107 14.3062 9.86261L14.7041 10.0201C14.8287 10.0694 14.9273 10.1684 14.9764 10.2935L15.1333 10.6928C15.2943 11.1024 15.8718 11.1024 16.0328 10.6928L16.1897 10.2935C16.2388 10.1684 16.3374 10.0694 16.462 10.0201L16.8599 9.86261C17.268 9.70107 17.268 9.12141 16.8599 8.95987L16.462 8.80237C16.3374 8.75305 16.2388 8.65407 16.1897 8.52902L16.0328 8.12967Z"
        ></path>
        <path
            fill="#FFFF00"
            d="M12 22C17.5228 22 22 17.5228 22 12C22 11.5373 21.3065 11.4608 21.0672 11.8568C19.9289 13.7406 17.8615 15 15.5 15C11.9101 15 9 12.0899 9 8.5C9 6.13845 10.2594 4.07105 12.1432 2.93276C12.5392 2.69347 12.4627 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"
        ></path>
        </svg>
    </div>
</div>
    `;
    // This is fine because we can just manually check that the selectors will work given the HTML above.
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const thumb = this.querySelector('[role="button"]')!;

    thumb.addEventListener('click', () => {
      this.toggle();
    });
    thumb.addEventListener('keypress', () => {
      this.toggle();
    });
  }
}

customElements.define('theme-switcher', ThemeSwitcher);
