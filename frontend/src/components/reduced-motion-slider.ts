import {
  prefersReducedMotion,
  savePrefersReducedMotion,
} from './reduced-motion';

class ReducedMotionSlider extends HTMLElement {
  #reducedMotion: boolean = prefersReducedMotion();

  /**
   * @method connectedCallback
   * @description Lifecycle method called when the element is connected to the DOM.
   * Renders the DOM inside of this element.
   */
  connectedCallback() {
    this.innerHTML = `<style>
  .reduced-motion-slider-thumb {
    background-image: url('/images/waves.svg');
    background-size: 4rem;
    background-position: 0 0.5rem;
    animation: MOVE-BG 2s linear infinite;
  }
  @keyframes MOVE-BG {
    0% {
     background-position: 0 0.5rem;
    }
    100% { 
      background-position: -4rem 0.5rem;
    }
  }
</style>
<div class="slider w-20 h-12 rounded-full relative border-solid border-[0.25rem] transition-all bg-sky-300 dark:bg-sky-900 border-sky-600 dark:border-sky-700 duration-500">
    <div
        class="reduced-motion-slider-thumb w-10 h-10 rounded-full transition-all duration-500 left-8 motion-reduced:left-0 absolute p-1 top-0 bg-sky-400 dark:bg-sky-800 bg-repeat"
        role="button"
        tabindex="0"
    >
    </div>
</div>
    `;
    // This is fine because we can just manually check that the selectors will work given the HTML above.
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const thumb = this.querySelector('[role="button"]')!;

    thumb.addEventListener('click', () => {
      this.toggle();
    });
    thumb.addEventListener('keypress', () => {
      this.toggle();
    });
  }

  /**
   * @method toggle
   * @description Toggles whether or not reduced motion is active.
   */
  toggle() {
    const reduced = !this.#reducedMotion;

    savePrefersReducedMotion(reduced);
    this.#reducedMotion = reduced;

    this.dispatchEvent(
      new CustomEvent<boolean>('reduced-motion-change', {
        detail: reduced,
      })
    );
  }
}

window.customElements.define('reduced-motion-slider', ReducedMotionSlider);
