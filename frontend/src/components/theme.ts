export const themes = ['light', 'dark'] as const;

export type Theme = (typeof themes)[number];

const chooseThemeFromName = (name: string): Theme =>
  name === 'dark' ? 'dark' : 'light';

export const getCurrentThemeName = (): Theme => {
  const savedTheme = window.localStorage.getItem('preferred_color_theme');
  if (savedTheme) {
    return chooseThemeFromName(savedTheme);
  }
  return window.matchMedia('(prefers-color-scheme: dark)').matches
    ? 'dark'
    : 'light';
};

export const saveThemeSelection = (theme: Theme): void => {
  window.localStorage.setItem('preferred_color_theme', theme);
};
