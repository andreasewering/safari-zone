/**
 * @class Typewriter
 * @extends HTMLElement
 * @description Custom web component that simulates a typewriter effect. If you want to react to the component finishing typing the given text, you may listen on the "animationfinished" event.
 */
class Typewriter extends HTMLElement {
  /**
   * @description The text to be displayed by the typewriter.
   */
  text = '';

  /**
   * @description The typing speed in milliseconds.
   */
  speed = 50;

  /**
   * @description Lines of output before pausing.
   */
  lines = 2;

  /**
   * @description Number of lines that should be skipped from the start of the text.
   */
  lineoffset = 0;

  /**
   * @description The current index of the text being typed.
   */
  #index = 0;

  /**
   * @description The indices in the text where a line break should occur.
   */
  #lineBreakIndices: number[] = [];

  /**
   * @description The number of lines that the text currently occupies on screen.
   */
  #currentLineNumber = 0;

  /**
   * @description Timer for controlling the typing animation.
   */
  #cancelAnimationFrame: CancelAnimationFrame = () => {
    // By default do nothing
  };

  /**
   * @description Index used to determine how far the pre-renderer has proceeded.
   */
  #prerenderIndex = 0;

  #cleanupFunctions: (() => void)[] = [];

  #shadowRoot: ShadowRoot;

  /**
   * @constructor
   * @description Creates a Typewriter instance and attaches a shadow DOM.
   */
  constructor() {
    super();
    this.#shadowRoot = this.attachShadow({ mode: 'open' });
  }

  /**
   * @method connectedCallback
   * @description Lifecycle method called when the element is connected to the DOM.
   * Renders the shadow DOM and starts the typing animation.
   */
  connectedCallback() {
    this.render();
    const resizeListener = debounce(() => {
      this.prerender();
      this.startTyping();
    }, 100);
    window.addEventListener('resize', resizeListener);
    this.#cleanupFunctions.push(() => {
      window.removeEventListener('resize', resizeListener);
    });
  }

  /**
   * @method disconnectedCallback
   * @description Lifecycle method called when the element is disconnected from the DOM.
   * Clears the typing timer.
   */
  disconnectedCallback() {
    this.cancelAnimationFrame();
    this.#cleanupFunctions.forEach((cleanup) => {
      cleanup();
    });
  }

  /**
   * @method attributeChangedCallback
   * @param {string} name - The name of the attribute that changed.
   * @param {string} oldValue - The previous value of the attribute.
   * @param {string} newValue - The new value of the attribute.
   * @description Lifecycle method called when observed attributes change.
   * Updates the corresponding properties and restarts the typing animation.
   */
  attributeChangedCallback(name: string, oldValue: string, newValue: string) {
    if (name === 'text' && oldValue !== newValue) {
      this.text = newValue || '';
      this.prerender();
      this.startTyping();
    }

    if (name === 'speed' && oldValue !== newValue) {
      this.speed = parseInt(newValue, 10) || this.speed;
      this.startTyping();
    }

    if (name === 'lines' && oldValue !== newValue) {
      this.lines = parseInt(newValue, 10) || this.lines;
      const output = this.#shadowRoot.getElementById('output');
      const testOutput = this.#shadowRoot.getElementById('test');
      if (output) {
        output.style.height = `${(1.5 * this.lines).toString()}rem`;
      }
      if (testOutput) {
        testOutput.style.height = `${(1.5 * this.lines).toString()}rem`;
      }
      this.prerender();
      this.startTyping();
    }

    if (name === 'lineoffset' && oldValue !== newValue) {
      const lineoffset = parseInt(newValue, 10);
      if (Number.isNaN(lineoffset)) {
        return;
      }
      this.lineoffset = lineoffset;
      this.startTyping();
    }
  }

  /**
   * @method observedAttributes
   * @return {string[]} An array of attribute names to observe for changes.
   * @description Static method that defines the observed attributes for the element.
   */
  static get observedAttributes(): string[] {
    return ['text', 'speed', 'lines', 'lineoffset'];
  }

  /**
   * @private
   * @method render
   * @description Renders the shadow DOM structure for the typewriter.
   */
  render() {
    this.#shadowRoot.innerHTML = `
      <style>
        :host {
          position: relative;
        }
        span {
          display: inline-block;
        }
        #test {
          position: absolute;
          left: 0;
          top: 0;
          padding: inherit;
          opacity: 0;
        }
      </style>
      <span id="output" style="height: ${(
        1.5 * this.lines
      ).toString()}rem"></span>
      <span id="test" style="height: ${(
        1.5 * this.lines
      ).toString()}rem"></span>
    `;
  }

  /**
   * @private
   * @method startTyping
   * @description Starts the typing animation by initializing the timer.
   */
  startTyping() {
    this.cancelAnimationFrame();
    this.resetContent();
    if (this.lineoffset === 0) {
      this.#index = 0;
      this.#cancelAnimationFrame = rafInterval(() => {
        this.type();
      }, this.speed);
      return;
    }
    const theoreticalIndex = this.#lineBreakIndices[this.lineoffset];
    if (
      theoreticalIndex === undefined &&
      this.#prerenderIndex === this.text.length - 1
    ) {
      // There is no content and we prerendered completely so there will be no content in the future either
      return;
    }
    if (theoreticalIndex === undefined) {
      // There could still be content coming when prerendering completes
      requestAnimationFrame(() => {
        this.startTyping();
      });
      return;
    }
    this.#index = theoreticalIndex;
    this.#cancelAnimationFrame = rafInterval(() => {
      this.type();
    }, this.speed);
  }

  resetContent() {
    const output = this.#shadowRoot.getElementById('output');
    if (output) {
      output.textContent = '';
    }
  }

  /**
   * @description Should only be set internally but can be used to observe whether the text is completely visible or not.
   */
  setDone() {
    this.setAttribute('done', '');
  }

  /**
   * @description Should only be set internally but can be used to observe whether the text is completely visible or not.
   */
  setUndone() {
    this.removeAttribute('done');
  }

  /**
   * @private
   * @method prerender
   * @description Render the given text into an invisible span to detect where line breaks occur.
   */
  prerender() {
    this.#prerenderIndex = 0;
    this.#lineBreakIndices = [];
    const words = this.text.split(/\s/);

    requestAnimationFrame(() => {
      const testOutput = this.#shadowRoot.getElementById('test');
      if (!testOutput) {
        throw new Error(
          'Inner HTML of Typewriter Component is inconsistent, element with id "test" not found.'
        );
      }
      testOutput.textContent = '';

      let index = 0;
      const spans: [number, HTMLSpanElement][] = [];
      for (const word of words) {
        const span = document.createElement('span');
        span.textContent = word;
        testOutput.appendChild(span);
        testOutput.append(' ');
        spans.push([index, span]);
        index = index + word.length + 1;
      }
      requestAnimationFrame(() => {
        let prevHeight = 0;
        for (const [wordIndex, span] of spans) {
          const rect = span.getBoundingClientRect();
          if (rect.top > prevHeight) {
            this.#lineBreakIndices.push(wordIndex);
          }
          this.#prerenderIndex = wordIndex;
          prevHeight = rect.top;
        }
        this.#prerenderIndex = index;
      });
    });
  }

  /**
   * @private
   * @method type
   * @description Types the next character of the text into the output span.
   */
  type() {
    this.setUndone();
    const output = this.#shadowRoot.getElementById('output');
    if (!output) {
      throw new Error(
        'Inner HTML of Typewriter Component is inconsistent, element with id "output" not found.'
      );
    }

    if (this.#index >= this.text.length) {
      this.cancelAnimationFrame();
      return;
    }
    if (this.#index >= this.#prerenderIndex) {
      // Wait for prerendering to catch up
      return;
    }
    const nextLineBreak = this.#lineBreakIndices[this.#currentLineNumber];
    if (nextLineBreak && this.#index >= nextLineBreak) {
      if (this.#currentLineNumber >= this.lines) {
        this.cancelAnimationFrame();
        return;
      }

      this.#currentLineNumber += 1;
    }

    if (output.textContent !== null) {
      output.textContent += this.text.charAt(this.#index);
    }
    this.#index++;
  }

  /**
   * @private
   * @method clearTimer
   * @description Clears the typing timer.
   */
  cancelAnimationFrame() {
    this.#cancelAnimationFrame();
    if (!this.hasAttribute('done')) {
      this.dispatchEvent(new CustomEvent('animationfinished'));
      this.setDone();
    }
    this.#cancelAnimationFrame = () => {
      // reset to default (do nothing)
    };
  }
}

type CancelAnimationFrame = () => void;

const rafInterval = (
  callback: () => void,
  delayInMs: number
): CancelAnimationFrame => {
  let delayOverlap = 0;
  let handle: number;
  let lastTimestamp: number | undefined;
  let cancelled = false;

  const run = (timestamp: number) => {
    if (cancelled) {
      return;
    }
    const dt = lastTimestamp ? timestamp - lastTimestamp : 0;
    lastTimestamp = timestamp;
    delayOverlap += dt;
    while (delayOverlap > delayInMs) {
      delayOverlap -= delayInMs;
      callback();
    }
    handle = requestAnimationFrame(run);
  };
  handle = requestAnimationFrame(run);
  return () => {
    cancelled = true;
    cancelAnimationFrame(handle);
  };
};

function debounce(func: () => void, waitMs: number) {
  let timeout: number | null = null;
  return () => {
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = window.setTimeout(() => {
      timeout = null;
      func();
    }, waitMs);
  };
}

customElements.define('type-writer', Typewriter);
