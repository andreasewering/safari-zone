import { loadImage } from './common.js';

/**
 * WebComponent for rendering a dynamic set of tiles to a canvas using a static tileset.
 * In the future we might instead generate a dynamic tilemap and load it via a service worker or something similar
 * - that would have the advantage of seeing the "real deal" as in "rendered by the WebGL fragment shader used on the safari page".
 *
 * For now, this seems simpler and allows for some customization i.e. render one tile-app per layer and reduce the opacity of
 * non-selected layers.
 *
 * This component works via HTML properties and not attributes, i.e. in the browser this element will always look like this:
 * `<tile-app></tile-app>` since properties get applied via JS (or in our case, Elm via `Html.Attributes.property`).
 */
class TileApp extends HTMLElement {
  set width(width: number) {
    this.#width = width;
    if (this.#canvas) {
      this.#canvas.width = width;
    }
    this.rerender();
  }
  set height(height: number) {
    this.#height = height;
    if (this.#canvas) {
      this.#canvas.height = height;
    }
    this.rerender();
  }
  set tile_size(tile_size: number) {
    this.#tile_size = tile_size;
    this.rerender();
  }
  set tiles(tiles: Tile[]) {
    this.#tiles = tiles;
    this.rerender();
  }
  set src_tile_size(src_tile_size: number) {
    this.#src_tile_size = src_tile_size;
    this.rerender();
  }
  set tiles_in_row(tiles_in_row: number) {
    this.#tiles_in_row = tiles_in_row;
    this.rerender();
  }
  set tileset_path(tileset_path: string) {
    void loadImage(tileset_path).then((image) => {
      this.#tilesetImage = image;
      this.rerender();
    });
  }

  #width = 0;
  #height = 0;
  #tile_size = 0;
  #tiles: Tile[] = [];
  #src_tile_size = 0;
  #tiles_in_row = 0;

  #canvas: HTMLCanvasElement | undefined;
  #tilesetImage: CanvasImageSource | undefined;
  #shadowRoot: ShadowRoot;

  constructor() {
    super();
    this.#shadowRoot = this.attachShadow({ mode: 'open' });
  }

  /**
   * @method connectedCallback
   * @description Lifecycle method called when the element is connected to the DOM.
   * Renders the shadow DOM.
   */
  connectedCallback() {
    this.#canvas = document.createElement('canvas');
    this.#canvas.height = this.#height;
    this.#canvas.width = this.#width;
    this.#shadowRoot.appendChild(this.#canvas);
    this.rerender();
  }

  /**
   * @method attributeChangedCallback
   * @param {string} name - The name of the attribute that changed.
   * @param {string} oldValue - The previous value of the attribute.
   * @param {string} newValue - The new value of the attribute.
   * @description Lifecycle method called when observed attributes change.
   * Updates the corresponding properties and restarts the typing animation.
   */
  attributeChangedCallback(name: string, oldValue: string, newValue: string) {
    if (oldValue == newValue) {
      return;
    }
    switch (name) {
      case 'height': {
        this.height = parseInt(newValue, 10);
        return;
      }
      case 'width': {
        this.width = parseInt(newValue, 10);
        return;
      }
      case 'tileset_path': {
        this.tileset_path = newValue;
        return;
      }
    }
  }

  indexToCoordinate(index: number) {
    // avoid dividing by 0
    if (this.#tiles_in_row === 0) {
      return { x: 0, y: 0 };
    }
    const y = Math.floor(index / this.#tiles_in_row);
    const x = index - y * this.#tiles_in_row;
    return {
      x: x * this.#src_tile_size,
      y: y * this.#src_tile_size,
    };
  }

  rerender() {
    const tilesetImage = this.#tilesetImage;
    if (!tilesetImage) {
      return;
    }
    const context = this.#canvas?.getContext('2d');
    if (!context) {
      return;
    }
    context.clearRect(0, 0, this.#width, this.#height);
    this.#tiles.forEach(({ x, y, id }) => {
      const sourceCoord = this.indexToCoordinate(id - 1);
      context.drawImage(
        tilesetImage,
        sourceCoord.x,
        sourceCoord.y,
        this.#src_tile_size,
        this.#src_tile_size,
        x,
        y,
        this.#tile_size,
        this.#tile_size
      );
    });
  }

  /**
   * @method observedAttributes
   * @return {string[]} An array of attribute names to observe for changes.
   * @description Static method that defines the observed attributes for the element.
   */
  static get observedAttributes(): string[] {
    return ['height', 'width', 'tileset_path'];
  }
}

type Tile = {
  x: number;
  y: number;
  id: number;
};

customElements.define('tile-app', TileApp);
