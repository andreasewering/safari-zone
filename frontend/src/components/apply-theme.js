import { getCurrentThemeName } from './theme';

document.body.classList.add(getCurrentThemeName());
document
  .querySelectorAll('[data-js]')
  .forEach((element) => element.removeAttribute('data-js'));
