function determineTimeOfDay(hours) {
  if (hours >= 5 && hours <= 10) {
    return 'morning';
  }
  if (hours >= 11 && hours <= 16) {
    return 'day';
  }
  if (hours >= 17 && hours <= 22) {
    return 'dusk';
  }
  return 'night';
}
function setBackground() {
  const src = `/images/safari-zone-${determineTimeOfDay(
    new Date().getHours()
  )}.png`;
  const background = document.getElementById('background');
  if (background) {
    background.src = src;
    background.classList.remove('hidden');
  }
}

setBackground();
setInterval(setBackground, 10000);
