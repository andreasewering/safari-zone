const localStorageKey = 'prefers_reduced_motion';

export function prefersReducedMotion(): boolean {
  return (
    window.localStorage.getItem(localStorageKey) !== null ||
    window.matchMedia(`(prefers-reduced-motion: reduce)`).matches
  );
}

export function applyPrefersReducedMotion(reduced: boolean) {
  if (reduced) {
    document.documentElement.setAttribute('motion', 'reduced');
  } else {
    document.documentElement.removeAttribute('motion');
  }
}

export function savePrefersReducedMotion(reduced: boolean): void {
  applyPrefersReducedMotion(reduced);
  if (reduced) {
    window.localStorage.setItem(localStorageKey, '');
  } else {
    window.localStorage.removeItem(localStorageKey);
  }
}
