import intl_proxy from 'intl-proxy';
import { refreshLocalToken } from './login/login-handler.js';
import './components/loading-indicator.js';
import './components/reduced-motion-slider.js';
import './components/qr-code-scanner.js';
import './components/Typewriter.js';
import { AudioMsg, audioPort } from './main/Interop/audio.js';
import { handleClipboardMsg } from './main/Interop/clipboard.js';
import { handleLocalStorageMsg } from './main/Interop/localStorage.js';
import { WsMsg, websocketPort } from './main/Interop/websocket.js';
import { Authentification, hashToMap, redirectTo } from './shared.js';
import { getCurrentThemeName } from './components/theme.js';
import { Elm as ElmBundle } from 'Main.elm';
import { handleEventMsg } from './main/Interop/event.js';
import { registerScrollbarAnalyzer } from './scrollbar.js';
import {
  applyPrefersReducedMotion,
  prefersReducedMotion,
} from './components/reduced-motion.js';

// Will be provided by vite
declare const __VERSION__: string;
declare const __DEV__: boolean;

if (__DEV__) {
  await import('./generated/Proto/dev-tools.mjs').then(() => {
    console.warn('Grpc Dev Tools enabled.');
  });
  await import('./dev/http-interceptor.js').then(() => {
    console.warn(
      'Http interceptor enabled. You may mock requests by manipulating window.hi'
    );
  });
  await import('./dev/event-listener.js').then(() => {
    console.warn(
      'Event listener enabled. This will log events emitted by the application.'
    );
  });
}

registerScrollbarAnalyzer();

const preferredLanguage =
  window.localStorage.getItem('preferred_language') ?? null;
const language =
  window.navigator.language || (window.navigator.languages[0] ?? 'en');
const reducedMotion = prefersReducedMotion();

const initialPath = window.location.pathname + window.location.search;

export const { lifespan, access_token } = hashToMap(window.location.hash);

const authenticateViaToken = async (): Promise<
  Authentification | undefined
> => {
  const intLifespan = Number.parseInt(lifespan ?? '');
  if (!Number.isNaN(intLifespan) && access_token) {
    return { access_token, lifespan: intLifespan };
  }
  return refreshLocalToken();
};

const handleAuthentification = async (): Promise<Authentification> => {
  const auth = await authenticateViaToken();
  if (auth) {
    return auth;
  }
  return redirectTo(`/login/?r=${initialPath}`);
};

void (async () => {
  const auth = await handleAuthentification();
  const audioOn = window.localStorage.getItem('audio_on') !== null;
  const fontSize = parseInt(
    getComputedStyle(document.documentElement).fontSize,
    10
  );

  const main = ElmBundle.Main;

  const app = main.init({
    flags: {
      auth,
      intl: intl_proxy,
      language,
      fontSize,
      preferredLanguage: preferredLanguage ?? null,
      preferredColorTheme: getCurrentThemeName(),
      reducedMotion,
      audioOn,
      now: new Date().getTime(),
      x: window.innerWidth,
      y: window.innerHeight,
      version: __VERSION__,
    },
  });

  const { interopFromElm, interopToElm } = app.ports;

  const sendWs = (wsMsg: WsMsg) => {
    interopToElm.send({ tag: 'ws', data: wsMsg });
  };

  const handleWsMsg = websocketPort(sendWs);

  const sendAudio = (audioMsg: AudioMsg) => {
    interopToElm.send({ tag: 'audio', data: audioMsg });
  };

  const handleAudioMsg = audioPort(sendAudio);

  // Use null (even though it is not used) for typescript to catch exhaustiveness errors
  const ports = (fromElm: ElmBundle.FromElm): null => {
    switch (fromElm.tag) {
      case 'clipboard':
        handleClipboardMsg(fromElm.data);
        return null;
      case 'localStorage':
        handleLocalStorageMsg(fromElm.data);
        return null;
      case 'ws':
        handleWsMsg(fromElm.data);
        return null;
      case 'audio':
        handleAudioMsg(fromElm.data);
        return null;
      case 'event':
        handleEventMsg(fromElm.data);
        return null;
    }
  };

  interopFromElm.subscribe(ports);

  document.documentElement.setAttribute('theme', getCurrentThemeName());
  applyPrefersReducedMotion(reducedMotion);
})();
