// When using GitHub's OAuth APIs, the state parameter is used as a CSRF token to prevent forgery attacks
// This module implements generating a random string to use as this "state" param.
// We save the string to session storage so that when we come back via a redirect,
// we can verify we came from Github.

export function generateState(): string {
  const array = new Uint8Array(16);
  window.crypto.getRandomValues(array);
  const base64 = toBase64(array);
  window.sessionStorage.setItem(SESSION_STORAGE_STATE_KEY, base64);
  return encodeURIComponent(base64);
}

function toBase64(uint8Array: Uint8Array): string {
  return btoa(String.fromCharCode.apply(null, Array.from(uint8Array)));
}

const SESSION_STORAGE_STATE_KEY = 'state';

export function verifyState(state: string): boolean {
  const savedState = window.sessionStorage.getItem(SESSION_STORAGE_STATE_KEY);
  window.sessionStorage.removeItem(SESSION_STORAGE_STATE_KEY);

  return savedState == decodeURIComponent(state);
}
