import { GrpcWebFetchTransport } from '@protobuf-ts/grpcweb-transport';
import { GardevoirServiceClient } from '../generated/gardevoir.client.js';
import { Authentification, hashToMap } from '../shared.js';

export async function attemptAndrenaLogin(): Promise<
  Authentification | undefined
> {
  const { access_token } = hashToMap(window.location.hash);

  if (!access_token) {
    return;
  }

  const rpc = new GrpcWebFetchTransport({ baseUrl: window.location.origin });
  const gardevoir = new GardevoirServiceClient(rpc);
  const { accessToken, validForSeconds } = await gardevoir
    .login({
      payload: {
        oneofKind: 'andrenaAccessToken',
        andrenaAccessToken: access_token,
      },
    })
    .then(({ response }) => response);
  return {
    access_token: accessToken,
    lifespan: validForSeconds,
  };
}
