import { GrpcWebFetchTransport } from '@protobuf-ts/grpcweb-transport';
import { GardevoirServiceClient } from '../generated/gardevoir.client.js';
import { Authentification } from '../shared.js';
import { verifyState } from './state.js';

export async function attemptGithubLogin(): Promise<
  Authentification | undefined
> {
  const queryParams = new URLSearchParams(window.location.search);
  const githubCode = queryParams.get('code');

  if (!githubCode) {
    return;
  }

  const githubState = queryParams.get('state');
  if (!githubState || !verifyState(githubState)) {
    return;
  }

  const rpc = new GrpcWebFetchTransport({ baseUrl: window.location.origin });
  const gardevoir = new GardevoirServiceClient(rpc);
  try {
    const { accessToken, validForSeconds } = await gardevoir
      .login({
        payload: {
          oneofKind: 'githubCode',
          githubCode,
        },
      })
      .then(({ response }) => response);
    return {
      access_token: accessToken,
      lifespan: validForSeconds,
    };
  } catch (e) {
    console.warn(e);
    return;
  }
}
