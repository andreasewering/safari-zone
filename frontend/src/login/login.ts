import { GardevoirServiceClient } from '../generated/gardevoir.client.js';
import { mapToHash } from '../shared.js';
import { attemptAndrenaLogin } from './andrena.js';
import { attemptGithubLogin } from './github.js';
import { generateState } from './state.js';
import { GrpcWebFetchTransport } from '@protobuf-ts/grpcweb-transport';

declare const __DEV__: boolean;

async function firstSuccess<T>(
  attempts: (() => Promise<T | undefined>)[]
): Promise<T | undefined> {
  for (const attempt of attempts) {
    const t = await attempt();
    if (t !== undefined) {
      return t;
    }
  }
}
let activeErrorMessageEl: HTMLElement | null = null;

const rpc = new GrpcWebFetchTransport({ baseUrl: window.location.origin });
const gardevoir = new GardevoirServiceClient(rpc);
const configPromise = gardevoir.getConfig({});

async function login() {
  const auth = await firstSuccess([attemptAndrenaLogin, attemptGithubLogin]);
  if (auth) {
    window.location.href = `/${mapToHash(auth)}`;
  }

  const { response } = await configPromise;
  const githubLink = document.querySelector<HTMLAnchorElement>('#github-login');
  if (githubLink) {
    const state = generateState();
    const redirectUri = __DEV__
      ? `http://localhost:3000/login/`
      : `${window.location.origin}/login/`;
    githubLink.href = `https://github.com/login/oauth/authorize?client_id=${response.githubClientId}&state=${state}&redirect_uri=${redirectUri}`;
  }

  document.querySelector('form')?.addEventListener('submit', (event) => {
    event.preventDefault();
    void onSubmit();
  });

  document.getElementById('andrena-login')?.addEventListener('click', () => {
    void onAndrenaLogin();
  });
}

async function onSubmit() {
  const password = document.querySelector<HTMLInputElement>('#password')?.value;
  const identifier =
    document.querySelector<HTMLInputElement>('#username')?.value;

  if (!password || !identifier) {
    return;
  }
  const loginButton = document.querySelector('form > button');
  loginButton?.setAttribute('disabled', '');
  const loadingIndicator = document.querySelector('loading-indicator');
  loadingIndicator?.classList.toggle('hidden');

  const errorMessageEls = document.querySelectorAll('[id^="error_"]');
  errorMessageEls.forEach((el) => {
    el.classList.add('h-0');
    el.classList.remove('px-4', 'py-2');
  });
  const transitionEnd = activeErrorMessageEl
    ? waitForTransition(activeErrorMessageEl)
    : Promise.resolve();

  const request = gardevoir.login({
    payload: {
      oneofKind: 'safarizoneCredentials',
      safarizoneCredentials: {
        password,
        identifier,
      },
    },
  });
  const [response] = await Promise.allSettled([request, transitionEnd]);
  loadingIndicator?.classList.toggle('hidden');
  loginButton?.removeAttribute('disabled');
  if (response.status === 'rejected') {
    const reason: unknown = response.reason;
    const errorMessageEl = getErrorMessageElement(reason);

    errorMessageEl?.classList.remove('h-0');
    errorMessageEl?.classList.add('px-4', 'py-2');
    activeErrorMessageEl = errorMessageEl;
    return;
  }
  const { accessToken, validForSeconds } = response.value.response;
  activeErrorMessageEl = null;
  window.location.href = `/${mapToHash({
    access_token: accessToken,
    lifespan: validForSeconds,
  })}`;
}

async function onAndrenaLogin() {
  const { default: Keycloak } = await import('keycloak-js');
  const config = (await configPromise).response;
  const keycloak = Keycloak({
    realm: config.andrenaRealm,
    clientId: config.andrenaClientId,
    url: 'https://auth.apps.andrena.de/auth/',
  });

  await keycloak.init({
    onLoad: 'login-required',
    flow: 'implicit',
    redirectUri: `${window.location.origin}/login/`,
  });
}

function getErrorMessageElement(reason: unknown): HTMLElement | null {
  const defaultErrorMessageEl = document.getElementById('error_DEFAULT');
  if (
    typeof reason !== 'object' ||
    reason === null ||
    !('code' in reason) ||
    typeof reason.code !== 'string'
  ) {
    return defaultErrorMessageEl;
  }
  return (
    document.getElementById(`error_${reason.code}`) ?? defaultErrorMessageEl
  );
}

function waitForTransition(element: HTMLElement): Promise<void> {
  return new Promise((resolve) => {
    element.addEventListener(
      'transitionend',
      () => {
        resolve();
      },
      { once: true }
    );
  });
}

void login();
