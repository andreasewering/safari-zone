import { Authentification } from '../shared.js';
import { GardevoirServiceClient } from '../generated/gardevoir.client.js';
import { GrpcWebFetchTransport } from '@protobuf-ts/grpcweb-transport';

const rpc = new GrpcWebFetchTransport({ baseUrl: window.location.origin });
const gardevoir = new GardevoirServiceClient(rpc);

export const refreshLocalToken = async (): Promise<
  Authentification | undefined
> =>
  gardevoir
    .refresh({})
    .then(({ response }) => ({
      access_token: response.accessToken,
      lifespan: response.validForSeconds,
    }))
    .catch(() => undefined);
