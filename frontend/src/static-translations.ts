const de = {
  login_title: 'Login | Safari-Zone',
  signup_title: 'Anmelden | Safari-Zone',
  background_alt: 'Safari-Zone Hintergrund',
  warning_alt: 'Warnsymbol',
  no_js_message:
    'Bitte schalte Javascript in deinen Browsereinstellungen an um diese Website zu benutzen.',
  login_headline: 'Willkommen zur Safari-Zone',
  signup_headline: 'Neuen Nutzer erstellen',
  username_or_email: 'Nutzername/E-Mail',
  username: 'Nutzername',
  password: 'Passwort',
  email: 'E-Mail',
  login: 'Einloggen',
  github_login: 'Mit Github einloggen',
  andrena_login: 'Mit Andrena einloggen',
  signup: 'Anmelden',
  no_account_message: 'Du hast noch keinen Account?',
  existing_account_message: 'Du hast schon einen Nutzer?',
  login_servererror:
    'Du konntest aus einem unbekannten Grund nicht angemeldet werden. Probiere es später noch einmal.',
  login_unauthorized: 'Inkorrekter Nutzername/E-Mail oder Passwort.',
  login_notfound: 'Nutzername/E-Mail existiert nicht.',
  signup_success:
    'Dein Nutzer wurde erfolgreich angelegt. Du solltest eine E-Mail bekommen um deinen Account zu verifizieren. Danach kannst du dich einloggen.',
  signup_conflict: 'Dieser Nutzer existiert bereits.',
  signup_servererror:
    'Die Anmeldung ist aus einem unbekannten Grund fehlgeschlagen. Probiere es später noch einmal oder nutze einen alternativen Login.',
  verification_title: 'Verifizierung | Safari-Zone',
  verification_headline: 'Verifizierung',
  verification_success: 'Dein Nutzer wurde erfolgreich verifiziert.',
  verification_conflict: 'Dieser Nutzer ist bereits verifiziert.',
  verification_error: 'Nutzer-ID und Verifizierungs-ID passen nicht zusammen.',
  verification_servererror:
    'Verifizierung aus einem unbekannten Grund fehlgeschlagen. Probiere es später noch einmal.',
  verification_expired:
    'Die Verifizierungs-ID ist abgelaufen. Wir haben dir automatisch eine neue ID zugeschickt.',
  verification_verifying: 'Verifizierung läuft...',
} as const;

const en = {
  login_title: 'Login | Safari-Zone',
  signup_title: 'Sign up | Safari-Zone',
  background_alt: 'Safari-Zone Background',
  warning_alt: 'Warning sign',
  login_headline: 'Welcome to the Safari-Zone',
  signup_headline: 'Create a new account',
  no_js_message:
    'Please enable Javascript in your Browser Settings to use this website.',
  username_or_email: 'Username/Email',
  username: 'Username',
  password: 'Password',
  email: 'Email',
  login: 'Sign in',
  github_login: 'Sign in with Github',
  andrena_login: 'Sign in with Andrena',
  signup: 'Sign up',
  no_account_message: "Don't have an account yet?",
  existing_account_message: 'You already have an account?',
  login_servererror:
    'Login failed for an unknown reason. Try again in a few minutes.',
  login_unauthorized: 'Incorrect username/email or password.',
  login_notfound: 'Username/Email does not exist.',
  signup_success:
    'Your account has been created successfully. You should get an email to verify your account, after which you are able to login.',
  signup_conflict: 'This username has been already taken.',
  signup_servererror:
    'Sign up failed for an unknown reason. Try again in a few minutes or use an alternative login.',
  verification_title: 'Verification | Safari-Zone',
  verification_headline: 'Verification',
  verification_success: 'Your user has been successfully verified.',
  verification_conflict: 'This user is already verified.',
  verification_error: 'User ID and verification ID do not match up.',
  verification_servererror:
    'Verification failed for an unknown reason. Try again in a few minutes.',
  verification_expired:
    'The verification ID is expired. We sent you a new verification ID automatically.',
  verification_verifying: 'Verifying...',
};

export default { de, en };
