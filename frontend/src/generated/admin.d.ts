export type JsonObject = { [Key in string]?: JsonValue };
export type JsonArray = JsonValue[];

/**
Matches any valid JSON value.
Source: https://github.com/sindresorhus/type-fest/blob/master/source/basic.d.ts
*/
export type JsonValue =
  | string
  | number
  | boolean
  | null
  | JsonObject
  | JsonArray;

export interface ElmApp {
  ports: {
    interopFromElm: PortFromElm<FromElm>;
    interopToElm: PortToElm<ToElm>;
    [key: string]: UnknownPort;
  };
}

export type FromElm = { data : boolean; tag : "unsaved_changes" } | { data : { tag : "stop" } | { tag : "fadeOut" } | { data : { end : number | null; filename : string; start : number | null }; tag : "loop" } | { data : string; tag : "start" } | { tag : "resume" }; tag : "audio" };

export type ToElm = { data : { tag : "ended"; url : string } | { error : string; tag : "error" }; tag : "audio" } | { tag : "unload_page" };

export type Flags = { audioOn : boolean; auth : { access_token : string; lifespan : number }; fontSize : number; intl : JsonValue; language : string; now : number; preferredColorTheme : string | null; preferredLanguage : string | null; version : string; x : number; y : number };

export namespace Admin {
  function init(options: { node?: HTMLElement | null; flags: Flags }): ElmApp;
}

export as namespace ElmAdmin;

export { ElmAdmin as Elm };

export type UnknownPort = PortFromElm<unknown> | PortToElm<unknown> | undefined;

export type PortFromElm<Data> = {
  subscribe(callback: (fromElm: Data) => void): void;
  unsubscribe(callback: (fromElm: Data) => void): void;
};

export type PortToElm<Data> = { send(data: Data): void };
