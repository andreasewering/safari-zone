module ItemIdDict exposing
    ( ItemIdDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs ItemIdDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import ItemId
import ManualDict


{-| A Dict using ItemId as key. -}
type ItemIdDict v
    = ItemIdDict (ManualDict.Dict ( Int, Int ) ItemId.ItemId v)


{-| Construct an empty ItemIdDict -}
empty : ItemIdDict v
empty =
    ItemIdDict ManualDict.empty


{-| Convert a ItemIdDict into the list of its entries -}
fromList : List ( ItemId.ItemId, v ) -> ItemIdDict v
fromList list =
    ItemIdDict (ManualDict.fromList ItemId.toComparable list)


{-| Convert a list of entries to a ItemIdDict -}
toList : ItemIdDict v -> List ( ItemId.ItemId, v )
toList dict =
    case dict of
        ItemIdDict d ->
            ManualDict.toList d


{-| Insert an entry into the ItemIdDict, overriding the previous value if it existed. -}
insert : ItemId.ItemId -> v -> ItemIdDict v -> ItemIdDict v
insert k v dict =
    ItemIdDict
        (case dict of
             ItemIdDict d ->
                 ManualDict.insert ItemId.toComparable k v d
        )


{-| Remove an entry from the ItemIdDict by ItemId. -}
remove : ItemId.ItemId -> ItemIdDict v -> ItemIdDict v
remove k dict =
    ItemIdDict
        (case dict of
             ItemIdDict d ->
                 ManualDict.remove ItemId.toComparable k d
        )


{-| Updates an entry from the ItemIdDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update : ItemId.ItemId -> (Maybe v -> Maybe v) -> ItemIdDict v -> ItemIdDict v
update k f dict =
    ItemIdDict
        (case dict of
             ItemIdDict d ->
                 ManualDict.update ItemId.toComparable k f d
        )


{-| Filters entries in a ItemIdDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter : (ItemId.ItemId -> v -> Bool) -> ItemIdDict v -> ItemIdDict v
filter f dict =
    ItemIdDict
        (case dict of
             ItemIdDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a ItemIdDict. The function receives both key and value of each entry as its arguments. -}
map : (ItemId.ItemId -> unpack -> b) -> ItemIdDict unpack -> ItemIdDict b
map f dict =
    ItemIdDict
        (case dict of
             ItemIdDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a ItemIdDict -}
values : ItemIdDict v -> List v
values dict =
    case dict of
        ItemIdDict d ->
            ManualDict.values d


{-| Iterate over the entries of a ItemIdDict to construct a new value -}
foldl :
    (ItemId.ItemId -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> ItemIdDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        ItemIdDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a ItemIdDict -}
get : ItemId.ItemId -> ItemIdDict v -> Maybe v
get k dict =
    case dict of
        ItemIdDict d ->
            ManualDict.get ItemId.toComparable k d