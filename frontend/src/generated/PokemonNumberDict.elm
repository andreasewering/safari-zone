module PokemonNumberDict exposing
    ( PokemonNumberDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs PokemonNumberDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import ManualDict
import PokemonNumber


{-| A Dict using PokemonNumber as key. -}
type PokemonNumberDict v
    = PokemonNumberDict (ManualDict.Dict Int PokemonNumber.PokemonNumber v)


{-| Construct an empty PokemonNumberDict -}
empty : PokemonNumberDict v
empty =
    PokemonNumberDict ManualDict.empty


{-| Convert a PokemonNumberDict into the list of its entries -}
fromList : List ( PokemonNumber.PokemonNumber, v ) -> PokemonNumberDict v
fromList list =
    PokemonNumberDict (ManualDict.fromList PokemonNumber.toComparable list)


{-| Convert a list of entries to a PokemonNumberDict -}
toList : PokemonNumberDict v -> List ( PokemonNumber.PokemonNumber, v )
toList dict =
    case dict of
        PokemonNumberDict d ->
            ManualDict.toList d


{-| Insert an entry into the PokemonNumberDict, overriding the previous value if it existed. -}
insert :
    PokemonNumber.PokemonNumber
    -> v
    -> PokemonNumberDict v
    -> PokemonNumberDict v
insert k v dict =
    PokemonNumberDict
        (case dict of
             PokemonNumberDict d ->
                 ManualDict.insert PokemonNumber.toComparable k v d
        )


{-| Remove an entry from the PokemonNumberDict by PokemonNumber. -}
remove :
    PokemonNumber.PokemonNumber -> PokemonNumberDict v -> PokemonNumberDict v
remove k dict =
    PokemonNumberDict
        (case dict of
             PokemonNumberDict d ->
                 ManualDict.remove PokemonNumber.toComparable k d
        )


{-| Updates an entry from the PokemonNumberDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update :
    PokemonNumber.PokemonNumber
    -> (Maybe v -> Maybe v)
    -> PokemonNumberDict v
    -> PokemonNumberDict v
update k f dict =
    PokemonNumberDict
        (case dict of
             PokemonNumberDict d ->
                 ManualDict.update PokemonNumber.toComparable k f d
        )


{-| Filters entries in a PokemonNumberDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter :
    (PokemonNumber.PokemonNumber -> v -> Bool)
    -> PokemonNumberDict v
    -> PokemonNumberDict v
filter f dict =
    PokemonNumberDict
        (case dict of
             PokemonNumberDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a PokemonNumberDict. The function receives both key and value of each entry as its arguments. -}
map :
    (PokemonNumber.PokemonNumber -> unpack -> b)
    -> PokemonNumberDict unpack
    -> PokemonNumberDict b
map f dict =
    PokemonNumberDict
        (case dict of
             PokemonNumberDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a PokemonNumberDict -}
values : PokemonNumberDict v -> List v
values dict =
    case dict of
        PokemonNumberDict d ->
            ManualDict.values d


{-| Iterate over the entries of a PokemonNumberDict to construct a new value -}
foldl :
    (PokemonNumber.PokemonNumber -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> PokemonNumberDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        PokemonNumberDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a PokemonNumberDict -}
get : PokemonNumber.PokemonNumber -> PokemonNumberDict v -> Maybe v
get k dict =
    case dict of
        PokemonNumberDict d ->
            ManualDict.get PokemonNumber.toComparable k d