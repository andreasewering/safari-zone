module UserIdDict exposing
    ( UserIdDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs UserIdDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import ManualDict
import UserId


{-| A Dict using UserId as key. -}
type UserIdDict v
    = UserIdDict (ManualDict.Dict ( Int, Int ) UserId.UserId v)


{-| Construct an empty UserIdDict -}
empty : UserIdDict v
empty =
    UserIdDict ManualDict.empty


{-| Convert a UserIdDict into the list of its entries -}
fromList : List ( UserId.UserId, v ) -> UserIdDict v
fromList list =
    UserIdDict (ManualDict.fromList UserId.toComparable list)


{-| Convert a list of entries to a UserIdDict -}
toList : UserIdDict v -> List ( UserId.UserId, v )
toList dict =
    case dict of
        UserIdDict d ->
            ManualDict.toList d


{-| Insert an entry into the UserIdDict, overriding the previous value if it existed. -}
insert : UserId.UserId -> v -> UserIdDict v -> UserIdDict v
insert k v dict =
    UserIdDict
        (case dict of
             UserIdDict d ->
                 ManualDict.insert UserId.toComparable k v d
        )


{-| Remove an entry from the UserIdDict by UserId. -}
remove : UserId.UserId -> UserIdDict v -> UserIdDict v
remove k dict =
    UserIdDict
        (case dict of
             UserIdDict d ->
                 ManualDict.remove UserId.toComparable k d
        )


{-| Updates an entry from the UserIdDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update : UserId.UserId -> (Maybe v -> Maybe v) -> UserIdDict v -> UserIdDict v
update k f dict =
    UserIdDict
        (case dict of
             UserIdDict d ->
                 ManualDict.update UserId.toComparable k f d
        )


{-| Filters entries in a UserIdDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter : (UserId.UserId -> v -> Bool) -> UserIdDict v -> UserIdDict v
filter f dict =
    UserIdDict
        (case dict of
             UserIdDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a UserIdDict. The function receives both key and value of each entry as its arguments. -}
map : (UserId.UserId -> unpack -> b) -> UserIdDict unpack -> UserIdDict b
map f dict =
    UserIdDict
        (case dict of
             UserIdDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a UserIdDict -}
values : UserIdDict v -> List v
values dict =
    case dict of
        UserIdDict d ->
            ManualDict.values d


{-| Iterate over the entries of a UserIdDict to construct a new value -}
foldl :
    (UserId.UserId -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> UserIdDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        UserIdDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a UserIdDict -}
get : UserId.UserId -> UserIdDict v -> Maybe v
get k dict =
    case dict of
        UserIdDict d ->
            ManualDict.get UserId.toComparable k d