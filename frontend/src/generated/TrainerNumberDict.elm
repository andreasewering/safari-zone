module TrainerNumberDict exposing
    ( TrainerNumberDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs TrainerNumberDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import ManualDict
import TrainerNumber


{-| A Dict using TrainerNumber as key. -}
type TrainerNumberDict v
    = TrainerNumberDict (ManualDict.Dict Int TrainerNumber.TrainerNumber v)


{-| Construct an empty TrainerNumberDict -}
empty : TrainerNumberDict v
empty =
    TrainerNumberDict ManualDict.empty


{-| Convert a TrainerNumberDict into the list of its entries -}
fromList : List ( TrainerNumber.TrainerNumber, v ) -> TrainerNumberDict v
fromList list =
    TrainerNumberDict (ManualDict.fromList TrainerNumber.toComparable list)


{-| Convert a list of entries to a TrainerNumberDict -}
toList : TrainerNumberDict v -> List ( TrainerNumber.TrainerNumber, v )
toList dict =
    case dict of
        TrainerNumberDict d ->
            ManualDict.toList d


{-| Insert an entry into the TrainerNumberDict, overriding the previous value if it existed. -}
insert :
    TrainerNumber.TrainerNumber
    -> v
    -> TrainerNumberDict v
    -> TrainerNumberDict v
insert k v dict =
    TrainerNumberDict
        (case dict of
             TrainerNumberDict d ->
                 ManualDict.insert TrainerNumber.toComparable k v d
        )


{-| Remove an entry from the TrainerNumberDict by TrainerNumber. -}
remove :
    TrainerNumber.TrainerNumber -> TrainerNumberDict v -> TrainerNumberDict v
remove k dict =
    TrainerNumberDict
        (case dict of
             TrainerNumberDict d ->
                 ManualDict.remove TrainerNumber.toComparable k d
        )


{-| Updates an entry from the TrainerNumberDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update :
    TrainerNumber.TrainerNumber
    -> (Maybe v -> Maybe v)
    -> TrainerNumberDict v
    -> TrainerNumberDict v
update k f dict =
    TrainerNumberDict
        (case dict of
             TrainerNumberDict d ->
                 ManualDict.update TrainerNumber.toComparable k f d
        )


{-| Filters entries in a TrainerNumberDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter :
    (TrainerNumber.TrainerNumber -> v -> Bool)
    -> TrainerNumberDict v
    -> TrainerNumberDict v
filter f dict =
    TrainerNumberDict
        (case dict of
             TrainerNumberDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a TrainerNumberDict. The function receives both key and value of each entry as its arguments. -}
map :
    (TrainerNumber.TrainerNumber -> unpack -> b)
    -> TrainerNumberDict unpack
    -> TrainerNumberDict b
map f dict =
    TrainerNumberDict
        (case dict of
             TrainerNumberDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a TrainerNumberDict -}
values : TrainerNumberDict v -> List v
values dict =
    case dict of
        TrainerNumberDict d ->
            ManualDict.values d


{-| Iterate over the entries of a TrainerNumberDict to construct a new value -}
foldl :
    (TrainerNumber.TrainerNumber -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> TrainerNumberDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        TrainerNumberDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a TrainerNumberDict -}
get : TrainerNumber.TrainerNumber -> TrainerNumberDict v -> Maybe v
get k dict =
    case dict of
        TrainerNumberDict d ->
            ManualDict.get TrainerNumber.toComparable k d