module ItemDict exposing
    ( ItemDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs ItemDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import Item
import ManualDict


{-| A Dict using Item as key. -}
type ItemDict v
    = ItemDict (ManualDict.Dict Int Item.Item v)


{-| Construct an empty ItemDict -}
empty : ItemDict v
empty =
    ItemDict ManualDict.empty


{-| Convert a ItemDict into the list of its entries -}
fromList : List ( Item.Item, v ) -> ItemDict v
fromList list =
    ItemDict (ManualDict.fromList Item.toComparable list)


{-| Convert a list of entries to a ItemDict -}
toList : ItemDict v -> List ( Item.Item, v )
toList dict =
    case dict of
        ItemDict d ->
            ManualDict.toList d


{-| Insert an entry into the ItemDict, overriding the previous value if it existed. -}
insert : Item.Item -> v -> ItemDict v -> ItemDict v
insert k v dict =
    ItemDict
        (case dict of
             ItemDict d ->
                 ManualDict.insert Item.toComparable k v d
        )


{-| Remove an entry from the ItemDict by Item. -}
remove : Item.Item -> ItemDict v -> ItemDict v
remove k dict =
    ItemDict
        (case dict of
             ItemDict d ->
                 ManualDict.remove Item.toComparable k d
        )


{-| Updates an entry from the ItemDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update : Item.Item -> (Maybe v -> Maybe v) -> ItemDict v -> ItemDict v
update k f dict =
    ItemDict
        (case dict of
             ItemDict d ->
                 ManualDict.update Item.toComparable k f d
        )


{-| Filters entries in a ItemDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter : (Item.Item -> v -> Bool) -> ItemDict v -> ItemDict v
filter f dict =
    ItemDict
        (case dict of
             ItemDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a ItemDict. The function receives both key and value of each entry as its arguments. -}
map : (Item.Item -> unpack -> b) -> ItemDict unpack -> ItemDict b
map f dict =
    ItemDict
        (case dict of
             ItemDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a ItemDict -}
values : ItemDict v -> List v
values dict =
    case dict of
        ItemDict d ->
            ManualDict.values d


{-| Iterate over the entries of a ItemDict to construct a new value -}
foldl :
    (Item.Item -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> ItemDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        ItemDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a ItemDict -}
get : Item.Item -> ItemDict v -> Maybe v
get k dict =
    case dict of
        ItemDict d ->
            ManualDict.get Item.toComparable k d