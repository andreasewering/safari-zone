module AreaIdDict exposing
    ( AreaIdDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs AreaIdDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import AreaId
import ManualDict


{-| A Dict using AreaId as key. -}
type AreaIdDict v
    = AreaIdDict (ManualDict.Dict ( Int, Int ) AreaId.AreaId v)


{-| Construct an empty AreaIdDict -}
empty : AreaIdDict v
empty =
    AreaIdDict ManualDict.empty


{-| Convert a AreaIdDict into the list of its entries -}
fromList : List ( AreaId.AreaId, v ) -> AreaIdDict v
fromList list =
    AreaIdDict (ManualDict.fromList AreaId.toComparable list)


{-| Convert a list of entries to a AreaIdDict -}
toList : AreaIdDict v -> List ( AreaId.AreaId, v )
toList dict =
    case dict of
        AreaIdDict d ->
            ManualDict.toList d


{-| Insert an entry into the AreaIdDict, overriding the previous value if it existed. -}
insert : AreaId.AreaId -> v -> AreaIdDict v -> AreaIdDict v
insert k v dict =
    AreaIdDict
        (case dict of
             AreaIdDict d ->
                 ManualDict.insert AreaId.toComparable k v d
        )


{-| Remove an entry from the AreaIdDict by AreaId. -}
remove : AreaId.AreaId -> AreaIdDict v -> AreaIdDict v
remove k dict =
    AreaIdDict
        (case dict of
             AreaIdDict d ->
                 ManualDict.remove AreaId.toComparable k d
        )


{-| Updates an entry from the AreaIdDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update : AreaId.AreaId -> (Maybe v -> Maybe v) -> AreaIdDict v -> AreaIdDict v
update k f dict =
    AreaIdDict
        (case dict of
             AreaIdDict d ->
                 ManualDict.update AreaId.toComparable k f d
        )


{-| Filters entries in a AreaIdDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter : (AreaId.AreaId -> v -> Bool) -> AreaIdDict v -> AreaIdDict v
filter f dict =
    AreaIdDict
        (case dict of
             AreaIdDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a AreaIdDict. The function receives both key and value of each entry as its arguments. -}
map : (AreaId.AreaId -> unpack -> b) -> AreaIdDict unpack -> AreaIdDict b
map f dict =
    AreaIdDict
        (case dict of
             AreaIdDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a AreaIdDict -}
values : AreaIdDict v -> List v
values dict =
    case dict of
        AreaIdDict d ->
            ManualDict.values d


{-| Iterate over the entries of a AreaIdDict to construct a new value -}
foldl :
    (AreaId.AreaId -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> AreaIdDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        AreaIdDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a AreaIdDict -}
get : AreaId.AreaId -> AreaIdDict v -> Maybe v
get k dict =
    case dict of
        AreaIdDict d ->
            ManualDict.get AreaId.toComparable k d