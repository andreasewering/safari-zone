export type JsonObject = { [Key in string]?: JsonValue };
export type JsonArray = JsonValue[];

/**
Matches any valid JSON value.
Source: https://github.com/sindresorhus/type-fest/blob/master/source/basic.d.ts
*/
export type JsonValue =
  | string
  | number
  | boolean
  | null
  | JsonObject
  | JsonArray;

export interface ElmApp {
  ports: {
    interopFromElm: PortFromElm<FromElm>;
    interopToElm: PortToElm<ToElm>;
    [key: string]: UnknownPort;
  };
}

export type FromElm = { data : { args : JsonValue[]; name : string }; tag : "event" } | { data : { tag : "stop" } | { tag : "fadeOut" } | { data : { end : number | null; filename : string; start : number | null }; tag : "loop" } | { data : string; tag : "start" } | { tag : "resume" }; tag : "audio" } | { data : { data : { key : string }; tag : "delete" } | { data : { key : string; value : string }; tag : "set" }; tag : "localStorage" } | { data : string; tag : "clipboard" } | { data : { data : { key : string; msg : number[] }; tag : "send" } | { data : { key : string }; tag : "disconnect" } | { data : { key : string; url : string }; tag : "connect" }; tag : "ws" };

export type ToElm = { data : (({ key : string; tag : "connected" } | { key : string; tag : "closed" } | { error : string; key : string; tag : "error" } | { key : string; msg : number[]; tag : "msg" }) & { key : string }); tag : "ws" } | { data : { tag : "ended"; url : string } | { error : string; tag : "error" }; tag : "audio" };

export type Flags = { audioOn : boolean; auth : { access_token : string; lifespan : number }; fontSize : number; intl : JsonValue; language : string; now : number; preferredColorTheme : string | null; preferredLanguage : string | null; reducedMotion : boolean; version : string; x : number; y : number };

export namespace Main {
  function init(options: { node?: HTMLElement | null; flags: Flags }): ElmApp;
}

export as namespace Elm;

export { Elm };

export type UnknownPort = PortFromElm<unknown> | PortToElm<unknown> | undefined;

export type PortFromElm<Data> = {
  subscribe(callback: (fromElm: Data) => void): void;
  unsubscribe(callback: (fromElm: Data) => void): void;
};

export type PortToElm<Data> = { send(data: Data): void };
