module GroupIdDict exposing
    ( GroupIdDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs GroupIdDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import GroupId
import ManualDict


{-| A Dict using GroupId as key. -}
type GroupIdDict v
    = GroupIdDict (ManualDict.Dict ( Int, Int ) GroupId.GroupId v)


{-| Construct an empty GroupIdDict -}
empty : GroupIdDict v
empty =
    GroupIdDict ManualDict.empty


{-| Convert a GroupIdDict into the list of its entries -}
fromList : List ( GroupId.GroupId, v ) -> GroupIdDict v
fromList list =
    GroupIdDict (ManualDict.fromList GroupId.toComparable list)


{-| Convert a list of entries to a GroupIdDict -}
toList : GroupIdDict v -> List ( GroupId.GroupId, v )
toList dict =
    case dict of
        GroupIdDict d ->
            ManualDict.toList d


{-| Insert an entry into the GroupIdDict, overriding the previous value if it existed. -}
insert : GroupId.GroupId -> v -> GroupIdDict v -> GroupIdDict v
insert k v dict =
    GroupIdDict
        (case dict of
             GroupIdDict d ->
                 ManualDict.insert GroupId.toComparable k v d
        )


{-| Remove an entry from the GroupIdDict by GroupId. -}
remove : GroupId.GroupId -> GroupIdDict v -> GroupIdDict v
remove k dict =
    GroupIdDict
        (case dict of
             GroupIdDict d ->
                 ManualDict.remove GroupId.toComparable k d
        )


{-| Updates an entry from the GroupIdDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update :
    GroupId.GroupId -> (Maybe v -> Maybe v) -> GroupIdDict v -> GroupIdDict v
update k f dict =
    GroupIdDict
        (case dict of
             GroupIdDict d ->
                 ManualDict.update GroupId.toComparable k f d
        )


{-| Filters entries in a GroupIdDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter : (GroupId.GroupId -> v -> Bool) -> GroupIdDict v -> GroupIdDict v
filter f dict =
    GroupIdDict
        (case dict of
             GroupIdDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a GroupIdDict. The function receives both key and value of each entry as its arguments. -}
map : (GroupId.GroupId -> unpack -> b) -> GroupIdDict unpack -> GroupIdDict b
map f dict =
    GroupIdDict
        (case dict of
             GroupIdDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a GroupIdDict -}
values : GroupIdDict v -> List v
values dict =
    case dict of
        GroupIdDict d ->
            ManualDict.values d


{-| Iterate over the entries of a GroupIdDict to construct a new value -}
foldl :
    (GroupId.GroupId -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> GroupIdDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        GroupIdDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a GroupIdDict -}
get : GroupId.GroupId -> GroupIdDict v -> Maybe v
get k dict =
    case dict of
        GroupIdDict d ->
            ManualDict.get GroupId.toComparable k d