module MapIdDict exposing
    ( MapIdDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs MapIdDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import ManualDict
import MapId


{-| A Dict using MapId as key. -}
type MapIdDict v
    = MapIdDict (ManualDict.Dict ( Int, Int ) MapId.MapId v)


{-| Construct an empty MapIdDict -}
empty : MapIdDict v
empty =
    MapIdDict ManualDict.empty


{-| Convert a MapIdDict into the list of its entries -}
fromList : List ( MapId.MapId, v ) -> MapIdDict v
fromList list =
    MapIdDict (ManualDict.fromList MapId.toComparable list)


{-| Convert a list of entries to a MapIdDict -}
toList : MapIdDict v -> List ( MapId.MapId, v )
toList dict =
    case dict of
        MapIdDict d ->
            ManualDict.toList d


{-| Insert an entry into the MapIdDict, overriding the previous value if it existed. -}
insert : MapId.MapId -> v -> MapIdDict v -> MapIdDict v
insert k v dict =
    MapIdDict
        (case dict of
             MapIdDict d ->
                 ManualDict.insert MapId.toComparable k v d
        )


{-| Remove an entry from the MapIdDict by MapId. -}
remove : MapId.MapId -> MapIdDict v -> MapIdDict v
remove k dict =
    MapIdDict
        (case dict of
             MapIdDict d ->
                 ManualDict.remove MapId.toComparable k d
        )


{-| Updates an entry from the MapIdDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update : MapId.MapId -> (Maybe v -> Maybe v) -> MapIdDict v -> MapIdDict v
update k f dict =
    MapIdDict
        (case dict of
             MapIdDict d ->
                 ManualDict.update MapId.toComparable k f d
        )


{-| Filters entries in a MapIdDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter : (MapId.MapId -> v -> Bool) -> MapIdDict v -> MapIdDict v
filter f dict =
    MapIdDict
        (case dict of
             MapIdDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a MapIdDict. The function receives both key and value of each entry as its arguments. -}
map : (MapId.MapId -> unpack -> b) -> MapIdDict unpack -> MapIdDict b
map f dict =
    MapIdDict
        (case dict of
             MapIdDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a MapIdDict -}
values : MapIdDict v -> List v
values dict =
    case dict of
        MapIdDict d ->
            ManualDict.values d


{-| Iterate over the entries of a MapIdDict to construct a new value -}
foldl :
    (MapId.MapId -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> MapIdDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        MapIdDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a MapIdDict -}
get : MapId.MapId -> MapIdDict v -> Maybe v
get k dict =
    case dict of
        MapIdDict d ->
            ManualDict.get MapId.toComparable k d