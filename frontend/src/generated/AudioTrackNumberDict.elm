module AudioTrackNumberDict exposing
    ( AudioTrackNumberDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs AudioTrackNumberDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import AudioTrackNumber
import ManualDict


{-| A Dict using AudioTrackNumber as key. -}
type AudioTrackNumberDict v
    = AudioTrackNumberDict
        (ManualDict.Dict Int AudioTrackNumber.AudioTrackNumber v)


{-| Construct an empty AudioTrackNumberDict -}
empty : AudioTrackNumberDict v
empty =
    AudioTrackNumberDict ManualDict.empty


{-| Convert a AudioTrackNumberDict into the list of its entries -}
fromList :
    List ( AudioTrackNumber.AudioTrackNumber, v ) -> AudioTrackNumberDict v
fromList list =
    AudioTrackNumberDict
        (ManualDict.fromList AudioTrackNumber.toComparable list)


{-| Convert a list of entries to a AudioTrackNumberDict -}
toList : AudioTrackNumberDict v -> List ( AudioTrackNumber.AudioTrackNumber, v )
toList dict =
    case dict of
        AudioTrackNumberDict d ->
            ManualDict.toList d


{-| Insert an entry into the AudioTrackNumberDict, overriding the previous value if it existed. -}
insert :
    AudioTrackNumber.AudioTrackNumber
    -> v
    -> AudioTrackNumberDict v
    -> AudioTrackNumberDict v
insert k v dict =
    AudioTrackNumberDict
        (case dict of
             AudioTrackNumberDict d ->
                 ManualDict.insert AudioTrackNumber.toComparable k v d
        )


{-| Remove an entry from the AudioTrackNumberDict by AudioTrackNumber. -}
remove :
    AudioTrackNumber.AudioTrackNumber
    -> AudioTrackNumberDict v
    -> AudioTrackNumberDict v
remove k dict =
    AudioTrackNumberDict
        (case dict of
             AudioTrackNumberDict d ->
                 ManualDict.remove AudioTrackNumber.toComparable k d
        )


{-| Updates an entry from the AudioTrackNumberDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update :
    AudioTrackNumber.AudioTrackNumber
    -> (Maybe v -> Maybe v)
    -> AudioTrackNumberDict v
    -> AudioTrackNumberDict v
update k f dict =
    AudioTrackNumberDict
        (case dict of
             AudioTrackNumberDict d ->
                 ManualDict.update AudioTrackNumber.toComparable k f d
        )


{-| Filters entries in a AudioTrackNumberDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter :
    (AudioTrackNumber.AudioTrackNumber -> v -> Bool)
    -> AudioTrackNumberDict v
    -> AudioTrackNumberDict v
filter f dict =
    AudioTrackNumberDict
        (case dict of
             AudioTrackNumberDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a AudioTrackNumberDict. The function receives both key and value of each entry as its arguments. -}
map :
    (AudioTrackNumber.AudioTrackNumber -> unpack -> b)
    -> AudioTrackNumberDict unpack
    -> AudioTrackNumberDict b
map f dict =
    AudioTrackNumberDict
        (case dict of
             AudioTrackNumberDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a AudioTrackNumberDict -}
values : AudioTrackNumberDict v -> List v
values dict =
    case dict of
        AudioTrackNumberDict d ->
            ManualDict.values d


{-| Iterate over the entries of a AudioTrackNumberDict to construct a new value -}
foldl :
    (AudioTrackNumber.AudioTrackNumber -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> AudioTrackNumberDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        AudioTrackNumberDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a AudioTrackNumberDict -}
get : AudioTrackNumber.AudioTrackNumber -> AudioTrackNumberDict v -> Maybe v
get k dict =
    case dict of
        AudioTrackNumberDict d ->
            ManualDict.get AudioTrackNumber.toComparable k d