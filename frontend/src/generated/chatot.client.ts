// @generated by protobuf-ts 2.9.5 with parameter optimize_code_size
// @generated from protobuf file "chatot.proto" (package "chatot", syntax proto3)
// tslint:disable
import type { RpcTransport } from "@protobuf-ts/runtime-rpc";
import type { ServiceInfo } from "@protobuf-ts/runtime-rpc";
import { ChatotService } from "./chatot";
import type { ParseChatMessageResponse } from "./chatot";
import type { ParseChatMessageRequest } from "./chatot";
import type { GetChatMessagesResponse } from "./chatot";
import type { GetChatMessagesRequest } from "./chatot";
import type { MarkGroupAsFavoriteResponse } from "./chatot";
import type { MarkGroupAsFavoriteRequest } from "./chatot";
import type { RemoveFromGroupResponse } from "./chatot";
import type { RemoveFromGroupRequest } from "./chatot";
import type { RequestGroupMembershipResponse } from "./chatot";
import type { RequestGroupMembershipRequest } from "./chatot";
import type { AddToGroupResponse } from "./chatot";
import type { AddToGroupRequest } from "./chatot";
import type { GetGroupDetailResponse } from "./chatot";
import type { GetGroupDetailRequest } from "./chatot";
import type { GetGroupsResponse } from "./chatot";
import type { GetGroupsRequest } from "./chatot";
import type { RemoveGroupResponse } from "./chatot";
import type { RemoveGroupRequest } from "./chatot";
import type { CreateGroupResponse } from "./chatot";
import type { CreateGroupRequest } from "./chatot";
import type { MarkFriendAsFavoriteResponse } from "./chatot";
import type { MarkFriendAsFavoriteRequest } from "./chatot";
import type { RemoveFriendResponse } from "./chatot";
import type { RemoveFriendRequest } from "./chatot";
import type { AddFriendResponse } from "./chatot";
import type { AddFriendRequest } from "./chatot";
import type { GetFriendRequestsResponse } from "./chatot";
import type { GetFriendRequestsRequest } from "./chatot";
import { stackIntercept } from "@protobuf-ts/runtime-rpc";
import type { GetFriendsResponse } from "./chatot";
import type { GetFriendsRequest } from "./chatot";
import type { UnaryCall } from "@protobuf-ts/runtime-rpc";
import type { RpcOptions } from "@protobuf-ts/runtime-rpc";
/**
 * gRPC
 *
 * @generated from protobuf service chatot.ChatotService
 */
export interface IChatotServiceClient {
    /**
     * Friends
     *
     * @generated from protobuf rpc: GetFriends(chatot.GetFriendsRequest) returns (chatot.GetFriendsResponse);
     */
    getFriends(input: GetFriendsRequest, options?: RpcOptions): UnaryCall<GetFriendsRequest, GetFriendsResponse>;
    /**
     * @generated from protobuf rpc: GetFriendRequests(chatot.GetFriendRequestsRequest) returns (chatot.GetFriendRequestsResponse);
     */
    getFriendRequests(input: GetFriendRequestsRequest, options?: RpcOptions): UnaryCall<GetFriendRequestsRequest, GetFriendRequestsResponse>;
    /**
     * @generated from protobuf rpc: AddFriend(chatot.AddFriendRequest) returns (chatot.AddFriendResponse);
     */
    addFriend(input: AddFriendRequest, options?: RpcOptions): UnaryCall<AddFriendRequest, AddFriendResponse>;
    /**
     * @generated from protobuf rpc: RemoveFriend(chatot.RemoveFriendRequest) returns (chatot.RemoveFriendResponse);
     */
    removeFriend(input: RemoveFriendRequest, options?: RpcOptions): UnaryCall<RemoveFriendRequest, RemoveFriendResponse>;
    /**
     * @generated from protobuf rpc: MarkFriendAsFavorite(chatot.MarkFriendAsFavoriteRequest) returns (chatot.MarkFriendAsFavoriteResponse);
     */
    markFriendAsFavorite(input: MarkFriendAsFavoriteRequest, options?: RpcOptions): UnaryCall<MarkFriendAsFavoriteRequest, MarkFriendAsFavoriteResponse>;
    /**
     * Groups
     *
     * @generated from protobuf rpc: CreateGroup(chatot.CreateGroupRequest) returns (chatot.CreateGroupResponse);
     */
    createGroup(input: CreateGroupRequest, options?: RpcOptions): UnaryCall<CreateGroupRequest, CreateGroupResponse>;
    /**
     * @generated from protobuf rpc: RemoveGroup(chatot.RemoveGroupRequest) returns (chatot.RemoveGroupResponse);
     */
    removeGroup(input: RemoveGroupRequest, options?: RpcOptions): UnaryCall<RemoveGroupRequest, RemoveGroupResponse>;
    /**
     * @generated from protobuf rpc: GetGroups(chatot.GetGroupsRequest) returns (chatot.GetGroupsResponse);
     */
    getGroups(input: GetGroupsRequest, options?: RpcOptions): UnaryCall<GetGroupsRequest, GetGroupsResponse>;
    /**
     * @generated from protobuf rpc: GetGroupDetail(chatot.GetGroupDetailRequest) returns (chatot.GetGroupDetailResponse);
     */
    getGroupDetail(input: GetGroupDetailRequest, options?: RpcOptions): UnaryCall<GetGroupDetailRequest, GetGroupDetailResponse>;
    /**
     * @generated from protobuf rpc: AddToGroup(chatot.AddToGroupRequest) returns (chatot.AddToGroupResponse);
     */
    addToGroup(input: AddToGroupRequest, options?: RpcOptions): UnaryCall<AddToGroupRequest, AddToGroupResponse>;
    /**
     * @generated from protobuf rpc: RequestGroupMembership(chatot.RequestGroupMembershipRequest) returns (chatot.RequestGroupMembershipResponse);
     */
    requestGroupMembership(input: RequestGroupMembershipRequest, options?: RpcOptions): UnaryCall<RequestGroupMembershipRequest, RequestGroupMembershipResponse>;
    /**
     * @generated from protobuf rpc: RemoveFromGroup(chatot.RemoveFromGroupRequest) returns (chatot.RemoveFromGroupResponse);
     */
    removeFromGroup(input: RemoveFromGroupRequest, options?: RpcOptions): UnaryCall<RemoveFromGroupRequest, RemoveFromGroupResponse>;
    /**
     * @generated from protobuf rpc: MarkGroupAsFavorite(chatot.MarkGroupAsFavoriteRequest) returns (chatot.MarkGroupAsFavoriteResponse);
     */
    markGroupAsFavorite(input: MarkGroupAsFavoriteRequest, options?: RpcOptions): UnaryCall<MarkGroupAsFavoriteRequest, MarkGroupAsFavoriteResponse>;
    /**
     * @generated from protobuf rpc: GetChatMessages(chatot.GetChatMessagesRequest) returns (chatot.GetChatMessagesResponse);
     */
    getChatMessages(input: GetChatMessagesRequest, options?: RpcOptions): UnaryCall<GetChatMessagesRequest, GetChatMessagesResponse>;
    /**
     * @generated from protobuf rpc: ParseChatMessage(chatot.ParseChatMessageRequest) returns (chatot.ParseChatMessageResponse);
     */
    parseChatMessage(input: ParseChatMessageRequest, options?: RpcOptions): UnaryCall<ParseChatMessageRequest, ParseChatMessageResponse>;
}
/**
 * gRPC
 *
 * @generated from protobuf service chatot.ChatotService
 */
export class ChatotServiceClient implements IChatotServiceClient, ServiceInfo {
    typeName = ChatotService.typeName;
    methods = ChatotService.methods;
    options = ChatotService.options;
    constructor(private readonly _transport: RpcTransport) {
    }
    /**
     * Friends
     *
     * @generated from protobuf rpc: GetFriends(chatot.GetFriendsRequest) returns (chatot.GetFriendsResponse);
     */
    getFriends(input: GetFriendsRequest, options?: RpcOptions): UnaryCall<GetFriendsRequest, GetFriendsResponse> {
        const method = this.methods[0], opt = this._transport.mergeOptions(options);
        return stackIntercept<GetFriendsRequest, GetFriendsResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: GetFriendRequests(chatot.GetFriendRequestsRequest) returns (chatot.GetFriendRequestsResponse);
     */
    getFriendRequests(input: GetFriendRequestsRequest, options?: RpcOptions): UnaryCall<GetFriendRequestsRequest, GetFriendRequestsResponse> {
        const method = this.methods[1], opt = this._transport.mergeOptions(options);
        return stackIntercept<GetFriendRequestsRequest, GetFriendRequestsResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: AddFriend(chatot.AddFriendRequest) returns (chatot.AddFriendResponse);
     */
    addFriend(input: AddFriendRequest, options?: RpcOptions): UnaryCall<AddFriendRequest, AddFriendResponse> {
        const method = this.methods[2], opt = this._transport.mergeOptions(options);
        return stackIntercept<AddFriendRequest, AddFriendResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: RemoveFriend(chatot.RemoveFriendRequest) returns (chatot.RemoveFriendResponse);
     */
    removeFriend(input: RemoveFriendRequest, options?: RpcOptions): UnaryCall<RemoveFriendRequest, RemoveFriendResponse> {
        const method = this.methods[3], opt = this._transport.mergeOptions(options);
        return stackIntercept<RemoveFriendRequest, RemoveFriendResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: MarkFriendAsFavorite(chatot.MarkFriendAsFavoriteRequest) returns (chatot.MarkFriendAsFavoriteResponse);
     */
    markFriendAsFavorite(input: MarkFriendAsFavoriteRequest, options?: RpcOptions): UnaryCall<MarkFriendAsFavoriteRequest, MarkFriendAsFavoriteResponse> {
        const method = this.methods[4], opt = this._transport.mergeOptions(options);
        return stackIntercept<MarkFriendAsFavoriteRequest, MarkFriendAsFavoriteResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * Groups
     *
     * @generated from protobuf rpc: CreateGroup(chatot.CreateGroupRequest) returns (chatot.CreateGroupResponse);
     */
    createGroup(input: CreateGroupRequest, options?: RpcOptions): UnaryCall<CreateGroupRequest, CreateGroupResponse> {
        const method = this.methods[5], opt = this._transport.mergeOptions(options);
        return stackIntercept<CreateGroupRequest, CreateGroupResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: RemoveGroup(chatot.RemoveGroupRequest) returns (chatot.RemoveGroupResponse);
     */
    removeGroup(input: RemoveGroupRequest, options?: RpcOptions): UnaryCall<RemoveGroupRequest, RemoveGroupResponse> {
        const method = this.methods[6], opt = this._transport.mergeOptions(options);
        return stackIntercept<RemoveGroupRequest, RemoveGroupResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: GetGroups(chatot.GetGroupsRequest) returns (chatot.GetGroupsResponse);
     */
    getGroups(input: GetGroupsRequest, options?: RpcOptions): UnaryCall<GetGroupsRequest, GetGroupsResponse> {
        const method = this.methods[7], opt = this._transport.mergeOptions(options);
        return stackIntercept<GetGroupsRequest, GetGroupsResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: GetGroupDetail(chatot.GetGroupDetailRequest) returns (chatot.GetGroupDetailResponse);
     */
    getGroupDetail(input: GetGroupDetailRequest, options?: RpcOptions): UnaryCall<GetGroupDetailRequest, GetGroupDetailResponse> {
        const method = this.methods[8], opt = this._transport.mergeOptions(options);
        return stackIntercept<GetGroupDetailRequest, GetGroupDetailResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: AddToGroup(chatot.AddToGroupRequest) returns (chatot.AddToGroupResponse);
     */
    addToGroup(input: AddToGroupRequest, options?: RpcOptions): UnaryCall<AddToGroupRequest, AddToGroupResponse> {
        const method = this.methods[9], opt = this._transport.mergeOptions(options);
        return stackIntercept<AddToGroupRequest, AddToGroupResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: RequestGroupMembership(chatot.RequestGroupMembershipRequest) returns (chatot.RequestGroupMembershipResponse);
     */
    requestGroupMembership(input: RequestGroupMembershipRequest, options?: RpcOptions): UnaryCall<RequestGroupMembershipRequest, RequestGroupMembershipResponse> {
        const method = this.methods[10], opt = this._transport.mergeOptions(options);
        return stackIntercept<RequestGroupMembershipRequest, RequestGroupMembershipResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: RemoveFromGroup(chatot.RemoveFromGroupRequest) returns (chatot.RemoveFromGroupResponse);
     */
    removeFromGroup(input: RemoveFromGroupRequest, options?: RpcOptions): UnaryCall<RemoveFromGroupRequest, RemoveFromGroupResponse> {
        const method = this.methods[11], opt = this._transport.mergeOptions(options);
        return stackIntercept<RemoveFromGroupRequest, RemoveFromGroupResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: MarkGroupAsFavorite(chatot.MarkGroupAsFavoriteRequest) returns (chatot.MarkGroupAsFavoriteResponse);
     */
    markGroupAsFavorite(input: MarkGroupAsFavoriteRequest, options?: RpcOptions): UnaryCall<MarkGroupAsFavoriteRequest, MarkGroupAsFavoriteResponse> {
        const method = this.methods[12], opt = this._transport.mergeOptions(options);
        return stackIntercept<MarkGroupAsFavoriteRequest, MarkGroupAsFavoriteResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: GetChatMessages(chatot.GetChatMessagesRequest) returns (chatot.GetChatMessagesResponse);
     */
    getChatMessages(input: GetChatMessagesRequest, options?: RpcOptions): UnaryCall<GetChatMessagesRequest, GetChatMessagesResponse> {
        const method = this.methods[13], opt = this._transport.mergeOptions(options);
        return stackIntercept<GetChatMessagesRequest, GetChatMessagesResponse>("unary", this._transport, method!, opt, input);
    }
    /**
     * @generated from protobuf rpc: ParseChatMessage(chatot.ParseChatMessageRequest) returns (chatot.ParseChatMessageResponse);
     */
    parseChatMessage(input: ParseChatMessageRequest, options?: RpcOptions): UnaryCall<ParseChatMessageRequest, ParseChatMessageResponse> {
        const method = this.methods[14], opt = this._transport.mergeOptions(options);
        return stackIntercept<ParseChatMessageRequest, ParseChatMessageResponse>("unary", this._transport, method!, opt, input);
    }
}
