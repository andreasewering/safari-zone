module ChatChannelDict exposing
    ( ChatChannelDict
    , empty
    , fromList, toList
    , insert, remove, update, filter, map
    , values, foldl, get
    )

{-|
@docs ChatChannelDict

@docs empty

@docs fromList, toList

@docs insert, remove, update, filter, map

@docs values, foldl, get
-}


import ChatChannel
import ManualDict


{-| A Dict using ChatChannel as key. -}
type ChatChannelDict v
    = ChatChannelDict
        (ManualDict.Dict ( Int, ( Int, Int ) ) ChatChannel.ChatChannel v)


{-| Construct an empty ChatChannelDict -}
empty : ChatChannelDict v
empty =
    ChatChannelDict ManualDict.empty


{-| Convert a ChatChannelDict into the list of its entries -}
fromList : List ( ChatChannel.ChatChannel, v ) -> ChatChannelDict v
fromList list =
    ChatChannelDict (ManualDict.fromList ChatChannel.toComparable list)


{-| Convert a list of entries to a ChatChannelDict -}
toList : ChatChannelDict v -> List ( ChatChannel.ChatChannel, v )
toList dict =
    case dict of
        ChatChannelDict d ->
            ManualDict.toList d


{-| Insert an entry into the ChatChannelDict, overriding the previous value if it existed. -}
insert : ChatChannel.ChatChannel -> v -> ChatChannelDict v -> ChatChannelDict v
insert k v dict =
    ChatChannelDict
        (case dict of
             ChatChannelDict d ->
                 ManualDict.insert ChatChannel.toComparable k v d
        )


{-| Remove an entry from the ChatChannelDict by ChatChannel. -}
remove : ChatChannel.ChatChannel -> ChatChannelDict v -> ChatChannelDict v
remove k dict =
    ChatChannelDict
        (case dict of
             ChatChannelDict d ->
                 ManualDict.remove ChatChannel.toComparable k d
        )


{-| Updates an entry from the ChatChannelDict via the given function. The argument for the function will be `Nothing` when the key is not in the map yet. -}
update :
    ChatChannel.ChatChannel
    -> (Maybe v -> Maybe v)
    -> ChatChannelDict v
    -> ChatChannelDict v
update k f dict =
    ChatChannelDict
        (case dict of
             ChatChannelDict d ->
                 ManualDict.update ChatChannel.toComparable k f d
        )


{-| Filters entries in a ChatChannelDict via the given function. The function receives both key and value of each entry as its arguments. -}
filter :
    (ChatChannel.ChatChannel -> v -> Bool)
    -> ChatChannelDict v
    -> ChatChannelDict v
filter f dict =
    ChatChannelDict
        (case dict of
             ChatChannelDict d ->
                 ManualDict.filter f d
        )


{-| Applies a given function to each entry in a ChatChannelDict. The function receives both key and value of each entry as its arguments. -}
map :
    (ChatChannel.ChatChannel -> unpack -> b)
    -> ChatChannelDict unpack
    -> ChatChannelDict b
map f dict =
    ChatChannelDict
        (case dict of
             ChatChannelDict d ->
                 ManualDict.map f d
        )


{-| Get the list of values from a ChatChannelDict -}
values : ChatChannelDict v -> List v
values dict =
    case dict of
        ChatChannelDict d ->
            ManualDict.values d


{-| Iterate over the entries of a ChatChannelDict to construct a new value -}
foldl :
    (ChatChannel.ChatChannel -> unpack -> unpack0 -> unpack0)
    -> unpack0
    -> ChatChannelDict unpack
    -> unpack0
foldl f acc dict =
    case dict of
        ChatChannelDict d ->
            ManualDict.foldl f acc d


{-| Get the value of a given key in a ChatChannelDict -}
get : ChatChannel.ChatChannel -> ChatChannelDict v -> Maybe v
get k dict =
    case dict of
        ChatChannelDict d ->
            ManualDict.get ChatChannel.toComparable k d