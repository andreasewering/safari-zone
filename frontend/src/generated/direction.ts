// @generated by protobuf-ts 2.9.5 with parameter optimize_code_size
// @generated from protobuf file "direction.proto" (package "data", syntax proto3)
// tslint:disable
/**
 * @generated from protobuf enum data.Direction
 */
export enum Direction {
    /**
     * @generated from protobuf enum value: L = 0;
     */
    L = 0,
    /**
     * @generated from protobuf enum value: R = 1;
     */
    R = 1,
    /**
     * @generated from protobuf enum value: U = 2;
     */
    U = 2,
    /**
     * @generated from protobuf enum value: D = 3;
     */
    D = 3
}
