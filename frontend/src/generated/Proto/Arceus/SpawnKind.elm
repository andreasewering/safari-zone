{- !!! DO NOT EDIT THIS FILE MANUALLY !!! -}

module Proto.Arceus.SpawnKind exposing (SpawnKind(..), decodeSpawnKind, defaultSpawnKind, encodeSpawnKind, fieldNumbersSpawnKind, jsonEncodeSpawnKind)

{-| 
This file was automatically generated by
- [`protoc-gen-elm`](https://www.npmjs.com/package/protoc-gen-elm) 4.0.4
- `protoc` 4.25.3
- the following specification files: `arceus.proto`

To run it, add a dependency via `elm install` on [`elm-protocol-buffers`](https://package.elm-lang.org/packages/eriktim/elm-protocol-buffers/1.2.0) version latest or higher.

@docs SpawnKind, decodeSpawnKind, defaultSpawnKind, encodeSpawnKind, fieldNumbersSpawnKind, jsonEncodeSpawnKind

-}

import Json.Encode
import Protobuf.Decode
import Protobuf.Encode
import String


{-| Encode a `SpawnKind` to JSON. Uses the canonical encoding described here: https://protobuf.dev/programming-guides/proto3/#json

-}
jsonEncodeSpawnKind : SpawnKind -> Json.Encode.Value
jsonEncodeSpawnKind value =
    Json.Encode.string <|
        case value of
            Grass ->
                "Grass"

            Water ->
                "Water"

            SpawnKindUnrecognized_ i ->
                "_UNRECOGNIZED_" ++ String.fromInt i


{-| The field numbers for the fields of `SpawnKind`. This is mostly useful for internals, like documentation generation.

-}
fieldNumbersSpawnKind : SpawnKind -> Int
fieldNumbersSpawnKind n_ =
    case n_ of
        Grass ->
            0

        Water ->
            1

        SpawnKindUnrecognized_ m_ ->
            m_


{-| Default for SpawnKind. Should only be used for 'required' decoders as an initial value.

-}
defaultSpawnKind : SpawnKind
defaultSpawnKind =
    Grass


{-| Declares how to encode a `SpawnKind` to Bytes. To actually perform the conversion to Bytes, you need to use Protobuf.Encode.encode from eriktim/elm-protocol-buffers.

-}
encodeSpawnKind : SpawnKind -> Protobuf.Encode.Encoder
encodeSpawnKind value =
    Protobuf.Encode.int32 <|
        case value of
            Grass ->
                0

            Water ->
                1

            SpawnKindUnrecognized_ i ->
                i


{-| Declares how to decode a `SpawnKind` from Bytes. To actually perform the conversion from Bytes, you need to use Protobuf.Decode.decode from eriktim/elm-protocol-buffers.

-}
decodeSpawnKind : Protobuf.Decode.Decoder SpawnKind
decodeSpawnKind =
    Protobuf.Decode.int32
        |> Protobuf.Decode.map
            (\i ->
                case i of
                    0 ->
                        Grass

                    1 ->
                        Water

                    _ ->
                        SpawnKindUnrecognized_ i
            )


{-| `SpawnKind` enumeration

-}
type SpawnKind
    = Grass
    | Water
    | SpawnKindUnrecognized_ Int
