{- !!! DO NOT EDIT THIS FILE MANUALLY !!! -}

module Proto.Error.ErrorCode exposing (ErrorCode(..), decodeErrorCode, defaultErrorCode, encodeErrorCode, fieldNumbersErrorCode, jsonEncodeErrorCode)

{-| 
This file was automatically generated by
- [`protoc-gen-elm`](https://www.npmjs.com/package/protoc-gen-elm) 4.0.4
- `protoc` 4.25.3
- the following specification files: `error_code.proto`

To run it, add a dependency via `elm install` on [`elm-protocol-buffers`](https://package.elm-lang.org/packages/eriktim/elm-protocol-buffers/1.2.0) version latest or higher.

@docs ErrorCode, decodeErrorCode, defaultErrorCode, encodeErrorCode, fieldNumbersErrorCode, jsonEncodeErrorCode

-}

import Json.Encode
import Protobuf.Decode
import Protobuf.Encode
import String


{-| Encode a `ErrorCode` to JSON. Uses the canonical encoding described here: https://protobuf.dev/programming-guides/proto3/#json

-}
jsonEncodeErrorCode : ErrorCode -> Json.Encode.Value
jsonEncodeErrorCode value =
    Json.Encode.string <|
        case value of
            Unspecified ->
                "Unspecified"

            EntityAlreadyExists ->
                "EntityAlreadyExists"

            EntityDoesNotExist ->
                "EntityDoesNotExist"

            DatabaseAccessFailed ->
                "DatabaseAccessFailed"

            NotEnoughPermissions ->
                "NotEnoughPermissions"

            AuthenticationTokenCouldNotBeParsed ->
                "AuthenticationTokenCouldNotBeParsed"

            AuthenticationTokenNotPresent ->
                "AuthenticationTokenNotPresent"

            AuthenticationTokenExpired ->
                "AuthenticationTokenExpired"

            BadRequest ->
                "BadRequest"

            UpstreamUnavailable ->
                "UpstreamUnavailable"

            UpstreamFailedExpectation ->
                "UpstreamFailedExpectation"

            UpstreamUnknownError ->
                "UpstreamUnknownError"

            InconsistentDatabaseEntry ->
                "InconsistentDatabaseEntry"

            ErrorCodeUnrecognized_ i ->
                "_UNRECOGNIZED_" ++ String.fromInt i


{-| The field numbers for the fields of `ErrorCode`. This is mostly useful for internals, like documentation generation.

-}
fieldNumbersErrorCode : ErrorCode -> Int
fieldNumbersErrorCode n_ =
    case n_ of
        Unspecified ->
            0

        EntityAlreadyExists ->
            1

        EntityDoesNotExist ->
            2

        DatabaseAccessFailed ->
            3

        NotEnoughPermissions ->
            4

        AuthenticationTokenCouldNotBeParsed ->
            5

        AuthenticationTokenNotPresent ->
            6

        AuthenticationTokenExpired ->
            7

        BadRequest ->
            8

        UpstreamUnavailable ->
            9

        UpstreamFailedExpectation ->
            10

        UpstreamUnknownError ->
            11

        InconsistentDatabaseEntry ->
            13

        ErrorCodeUnrecognized_ m_ ->
            m_


{-| Default for ErrorCode. Should only be used for 'required' decoders as an initial value.

-}
defaultErrorCode : ErrorCode
defaultErrorCode =
    Unspecified


{-| Declares how to encode a `ErrorCode` to Bytes. To actually perform the conversion to Bytes, you need to use Protobuf.Encode.encode from eriktim/elm-protocol-buffers.

-}
encodeErrorCode : ErrorCode -> Protobuf.Encode.Encoder
encodeErrorCode value =
    Protobuf.Encode.int32 <|
        case value of
            Unspecified ->
                0

            EntityAlreadyExists ->
                1

            EntityDoesNotExist ->
                2

            DatabaseAccessFailed ->
                3

            NotEnoughPermissions ->
                4

            AuthenticationTokenCouldNotBeParsed ->
                5

            AuthenticationTokenNotPresent ->
                6

            AuthenticationTokenExpired ->
                7

            BadRequest ->
                8

            UpstreamUnavailable ->
                9

            UpstreamFailedExpectation ->
                10

            UpstreamUnknownError ->
                11

            InconsistentDatabaseEntry ->
                13

            ErrorCodeUnrecognized_ i ->
                i


{-| Declares how to decode a `ErrorCode` from Bytes. To actually perform the conversion from Bytes, you need to use Protobuf.Decode.decode from eriktim/elm-protocol-buffers.

-}
decodeErrorCode : Protobuf.Decode.Decoder ErrorCode
decodeErrorCode =
    Protobuf.Decode.int32
        |> Protobuf.Decode.map
            (\i ->
                case i of
                    0 ->
                        Unspecified

                    1 ->
                        EntityAlreadyExists

                    2 ->
                        EntityDoesNotExist

                    3 ->
                        DatabaseAccessFailed

                    4 ->
                        NotEnoughPermissions

                    5 ->
                        AuthenticationTokenCouldNotBeParsed

                    6 ->
                        AuthenticationTokenNotPresent

                    7 ->
                        AuthenticationTokenExpired

                    8 ->
                        BadRequest

                    9 ->
                        UpstreamUnavailable

                    10 ->
                        UpstreamFailedExpectation

                    11 ->
                        UpstreamUnknownError

                    13 ->
                        InconsistentDatabaseEntry

                    _ ->
                        ErrorCodeUnrecognized_ i
            )


{-| `ErrorCode` enumeration

-}
type ErrorCode
    = Unspecified
    | EntityAlreadyExists
    | EntityDoesNotExist
    | DatabaseAccessFailed
    | NotEnoughPermissions
    | AuthenticationTokenCouldNotBeParsed
    | AuthenticationTokenNotPresent
    | AuthenticationTokenExpired
    | BadRequest
    | UpstreamUnavailable
    | UpstreamFailedExpectation
    | UpstreamUnknownError
    | InconsistentDatabaseEntry
    | ErrorCodeUnrecognized_ Int
