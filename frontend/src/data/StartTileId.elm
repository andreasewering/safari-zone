module StartTileId exposing (StartTileId, toComparable)

import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Tagged exposing (Tagged, untag)


type alias StartTileId =
    Tagged Int64 StartTileIdTag


toComparable : StartTileId -> ( Int, Int )
toComparable =
    untag >> Int64.toInts


type StartTileIdTag
    = StartTileIdTag
