module ItemId exposing (ItemId, toComparable)

import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Tagged exposing (Tagged, untag)


type alias ItemId =
    Tagged Int64 ItemIdTag


toComparable : ItemId -> ( Int, Int )
toComparable =
    untag >> Int64.toInts


type ItemIdTag
    = ItemIdTag
