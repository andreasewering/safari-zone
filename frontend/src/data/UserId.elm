module UserId exposing (UserId, fromShortString, toComparable, toShortString)

import Int64Util
import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Tagged exposing (Tagged, tag, untag)


type alias UserId =
    Tagged Int64 UserIdTag


toComparable : UserId -> ( Int, Int )
toComparable =
    untag >> Int64.toInts


toShortString : UserId -> String
toShortString =
    untag >> Int64Util.int64ToUrlSegment


fromShortString : String -> Maybe UserId
fromShortString =
    Int64Util.parseInt64FromUrl >> Maybe.map tag


type UserIdTag
    = UserIdTag
