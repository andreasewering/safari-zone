module Item exposing (Item(..), allItems, fromProto, toComparable, toImagePath, toProto, toSpritePath)

import Proto.Data.Item


type Item
    = Pokeball
    | Superball
    | Hyperball
    | Quickball
    | Diveball


allItems : List Item
allItems =
    [ Pokeball, Superball, Hyperball, Quickball, Diveball ]


toSpritePath : Item -> String
toSpritePath item =
    case item of
        Pokeball ->
            "/images/items/pokeball-sprites.png"

        Superball ->
            "/images/items/superball-sprites.png"

        Hyperball ->
            "/images/items/hyperball-sprites.png"

        Diveball ->
            "/images/items/diveball-sprites.png"

        Quickball ->
            "/images/items/quickball-sprites.png"


toImagePath : Item -> String
toImagePath item =
    case item of
        Pokeball ->
            "/images/items/pokeball.png"

        Superball ->
            "/images/items/superball.png"

        Hyperball ->
            "/images/items/hyperball.png"

        Diveball ->
            "/images/items/diveball.png"

        Quickball ->
            "/images/items/quickball.png"


toComparable : Item -> Int
toComparable item =
    case item of
        Pokeball ->
            0

        Superball ->
            1

        Hyperball ->
            2

        Quickball ->
            3

        Diveball ->
            4


fromProto : Proto.Data.Item.Item -> Maybe Item
fromProto item =
    case item of
        Proto.Data.Item.Pokeball ->
            Just Pokeball

        Proto.Data.Item.Superball ->
            Just Superball

        Proto.Data.Item.Hyperball ->
            Just Hyperball

        Proto.Data.Item.Diveball ->
            Just Diveball

        Proto.Data.Item.Quickball ->
            Just Quickball

        Proto.Data.Item.ItemUnrecognized_ _ ->
            Nothing


toProto : Item -> Proto.Data.Item.Item
toProto item =
    case item of
        Pokeball ->
            Proto.Data.Item.Pokeball

        Superball ->
            Proto.Data.Item.Superball

        Hyperball ->
            Proto.Data.Item.Hyperball

        Diveball ->
            Proto.Data.Item.Diveball

        Quickball ->
            Proto.Data.Item.Quickball
