module AudioTrackNumber exposing (AudioTrackNumber, toComparable)

import Tagged exposing (Tagged, untag)


type alias AudioTrackNumber =
    Tagged Int AudioTrackNumberTag


type AudioTrackNumberTag
    = AudioTrackNumberTag


toComparable : AudioTrackNumber -> Int
toComparable =
    untag
