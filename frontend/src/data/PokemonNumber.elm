module PokemonNumber exposing (PokemonNumber, toComparable, urlCodec)

import Tagged exposing (Tagged, tag, untag)
import UrlCodec exposing (UrlCodec)


type alias PokemonNumber =
    Tagged Int PokemonNumberTag


type PokemonNumberTag
    = PokemonNumberTag


toComparable : PokemonNumber -> Int
toComparable =
    untag


urlCodec : UrlCodec (PokemonNumber -> a) a PokemonNumber
urlCodec =
    UrlCodec.custom "PokemonNumber" { build = untag >> String.fromInt, parse = String.toInt >> Maybe.map tag }
