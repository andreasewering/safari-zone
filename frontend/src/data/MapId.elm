module MapId exposing (MapId, fromComparable, fromHeader, toComparable, toHeader, urlCodec)

import Int64Util
import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Protobuf.Utils.Int64 as Int64
import Tagged exposing (Tagged, tag, untag)
import UrlCodec exposing (UrlCodec)


type alias MapId =
    Tagged Int64 MapIdTag


toComparable : MapId -> ( Int, Int )
toComparable =
    untag >> Int64.toInts


toHeader : MapId -> String
toHeader =
    untag >> Int64.toSignedString


fromHeader : String -> Maybe MapId
fromHeader =
    Int64.fromSignedString >> Maybe.map tag


fromComparable : ( Int, Int ) -> MapId
fromComparable ( high, low ) =
    Int64.fromInts high low |> Tagged.tag


urlCodec : UrlCodec (MapId -> a) a MapId
urlCodec =
    UrlCodec.custom "GroupId" { build = untag >> Int64Util.int64ToUrlSegment, parse = Int64Util.parseInt64FromUrl >> Maybe.map tag }


type MapIdTag
    = MapIdTag
