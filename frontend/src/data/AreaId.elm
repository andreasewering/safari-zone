module AreaId exposing (AreaId, toComparable, urlCodec)

import Int64Util
import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Tagged exposing (Tagged, tag, untag)
import UrlCodec exposing (UrlCodec)


type alias AreaId =
    Tagged Int64 AreaIdTag


toComparable : AreaId -> ( Int, Int )
toComparable =
    untag >> Int64.toInts


urlCodec : UrlCodec (AreaId -> a) a AreaId
urlCodec =
    UrlCodec.custom "AreaId" { build = untag >> Int64Util.int64ToUrlSegment, parse = Int64Util.parseInt64FromUrl >> Maybe.map tag }


type AreaIdTag
    = AreaIdTag
