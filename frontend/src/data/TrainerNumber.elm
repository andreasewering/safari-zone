module TrainerNumber exposing (TrainerNumber, toComparable, urlCodec)

import Tagged exposing (Tagged, tag, untag)
import UrlCodec exposing (UrlCodec)


type alias TrainerNumber =
    Tagged Int TrainerNumberTag


type TrainerNumberTag
    = TrainerNumberTag


toComparable : TrainerNumber -> Int
toComparable =
    untag


urlCodec : UrlCodec (TrainerNumber -> a) a TrainerNumber
urlCodec =
    UrlCodec.custom "TrainerNumber" { build = untag >> String.fromInt, parse = String.toInt >> Maybe.map tag }
