module ChatChannel exposing (ChatChannel(..), toComparable, toString)

import GroupId exposing (GroupId)
import MapId exposing (MapId)
import UserId exposing (UserId)


type ChatChannel
    = MapChannel MapId
    | GroupChannel GroupId
    | PrivateChannel UserId


toComparable : ChatChannel -> ( Int, ( Int, Int ) )
toComparable channel =
    case channel of
        MapChannel mapId ->
            ( 0, MapId.toComparable mapId )

        GroupChannel groupId ->
            ( 0, GroupId.toComparable groupId )

        PrivateChannel userId ->
            ( 0, UserId.toComparable userId )


toString : ChatChannel -> String
toString channel =
    let
        ( channelTag, ( id1, id2 ) ) =
            toComparable channel
    in
    [ channelTag, id1, id2 ]
        |> List.map String.fromInt
        |> String.join "_"
