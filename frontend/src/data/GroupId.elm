module GroupId exposing (GroupId, toComparable, urlCodec)

import Int64Util
import Protobuf.Types.Int64 as Int64 exposing (Int64)
import Tagged exposing (Tagged, tag, untag)
import UrlCodec exposing (UrlCodec)


type alias GroupId =
    Tagged Int64 GroupIdTag


toComparable : GroupId -> ( Int, Int )
toComparable =
    untag >> Int64.toInts


urlCodec : UrlCodec (GroupId -> a) a GroupId
urlCodec =
    UrlCodec.custom "GroupId" { build = untag >> Int64Util.int64ToUrlSegment, parse = Int64Util.parseInt64FromUrl >> Maybe.map tag }


type GroupIdTag
    = GroupIdTag
