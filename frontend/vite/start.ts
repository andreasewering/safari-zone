import { createServer } from 'vite';
import os from 'node:os';
import qrcode from 'qrcode';
import terminalLink from 'terminal-link';
import open from 'open';

const NGINX_PORT = 3443;

const server = await createServer({});
await server.listen();

const [ip] = Object.entries(os.networkInterfaces())
  .flatMap(([, iface]) => iface ?? [])
  .filter((ip) => ip.family === 'IPv4' && ip.address !== '127.0.0.1');

if (!ip) {
  throw new Error('Could not determine public IP address');
}

const baseUrl = `https://${ip.address}:${NGINX_PORT.toString()}/`;

// Log the custom messages
console.log(`\n  ➜  Network: ${terminalLink(baseUrl, baseUrl)}`);

const urlAsQrCode = new Promise<string>((resolve, reject) => {
  qrcode.toString(baseUrl, { type: 'terminal', small: true }, (err, url) => {
    if (err) {
      reject(err);
    }
    console.log(`\n${url}\n`);
    resolve(url);
  });
});

server.bindCLIShortcuts({
  print: true,
  customShortcuts: [
    {
      key: 'o',
      description: 'to open in browser',
      action: async () => {
        await open(baseUrl);
      },
    },
    {
      key: 'qr',
      description: 'print QR code',
      action: async () => {
        console.log(`\n${await urlAsQrCode}\n`);
      },
    },
  ],
});
