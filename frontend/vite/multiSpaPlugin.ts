import { existsSync, readFileSync } from 'fs';
import path from 'path';
import { Connect, Plugin, ViteDevServer, send } from 'vite';

export type Options = Record<string, string>;

function transformHtml(
  options: Options,
  server: ViteDevServer
): Connect.NextHandleFunction {
  return function middleware(req: Connect.IncomingMessage, res, next) {
    const url = req.url;
    if (req.headers['sec-fetch-dest'] !== 'document' || !url) {
      next();
      return;
    }

    let [entry] = Object.entries(options).filter(([k]) => url.startsWith(k));
    if (!entry) {
      next();
      return;
    }
    let [baseUrl, filePath] = entry;
    filePath = path.join(__dirname, '..', filePath);
    if (!existsSync(filePath)) {
      console.error(new Error(`Cannot find ${filePath}.`));
      next();
      return;
    }
    const html = readFileSync(filePath, 'utf-8');
    server
      .transformIndexHtml(baseUrl, html, url)
      .then((transformedHtml) => {
        send(req, res, transformedHtml, 'html', {});
      })
      .catch((err: unknown) => {
        console.error(err);
        next(err);
      });
  };
}

export function multiSpa(options: Options): Plugin {
  return {
    name: 'multiSpa',
    configureServer(server: ViteDevServer): void {
      server.middlewares.use(transformHtml(options, server));
    },
  };
}
