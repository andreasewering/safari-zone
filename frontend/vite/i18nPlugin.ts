import handlebars from 'handlebars';
import { IndexHtmlTransformContext, Plugin } from 'vite';

type Options<T extends Record<string, string>> = {
  defaultLanguage: string;
  translations: Record<string, T>;
  paths: string[];
};

export const staticI18nPlugin = <T extends Record<string, string>>(
  options: Options<T>
): Plugin => {
  const htmls = new Map<{ path: string; language: string }, string>();
  return {
    name: 'i18n-static-plugin',
    enforce: 'post',
    transformIndexHtml: {
      handler(html: string, ctx: IndexHtmlTransformContext): string {
        let path = ctx.path;
        if (path.startsWith('/')) {
          path = path.substring(1);
        }
        if (!options.paths.includes(path)) {
          return html;
        }

        let defaultHtml = '';

        const template = handlebars.compile(html);
        Object.entries(options.translations).forEach(
          ([language, translations]) => {
            const translated = template(translations);
            if (language === options.defaultLanguage) {
              defaultHtml = translated;
            } else {
              htmls.set({ language, path }, translated);
            }
          }
        );

        return defaultHtml;
      },
    },
    generateBundle(this) {
      htmls.forEach((html, { language, path }) => {
        this.emitFile({
          type: 'asset',
          fileName: path.replace('index.html', `index.${language}.html`),
          source: html,
        });
      });
    },
  };
};
