import { writeFileSync } from 'node:fs';
import { IndexHtmlTransformContext, Plugin } from 'vite';

export const linkPlugin = (): Plugin => {
  let lax = false;
  return {
    name: 'link-plugin',
    configureServer() {
      lax = true;
    },
    transformIndexHtml(html: string, ctx: IndexHtmlTransformContext) {
      return html.replace(
        /chunk_file_path\((.+)\)/g,
        (_substr, match: string) => {
          const filepath = Object.values(ctx.bundle ?? {}).find(
            (asset) => asset.name === match
          )?.fileName;
          if (!filepath && !lax) {
            const availableChunkNames = Object.values(ctx.bundle ?? {})
              .map((asset) => asset.name)
              .filter((name) => !!name);
            throw new Error(
              `Could not find chunk with name ${match}. Available chunks are: ${availableChunkNames.join(
                ', '
              )}`
            );
          }

          return filepath ? `/${filepath}` : '';
        }
      );
    },
  };
};
