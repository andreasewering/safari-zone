import autoprefixer from 'autoprefixer';
import { elmReviewTailwindCssPlugin } from 'elm-review-tailwindcss-postcss-plugin';
import tailwind from 'tailwindcss';
import tailwindMainConfig from './tailwind.main.config.js';
import tailwindMinimalConfig from './tailwind.minimal.config.js';

export default {
  plugins: [
    tailwind(tailwindMainConfig),
    tailwind(tailwindMinimalConfig),
    elmReviewTailwindCssPlugin({ tailwindConfig: tailwindMainConfig }),
    autoprefixer,
  ],
};
